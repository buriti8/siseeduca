<!DOCTYPE html>
<!--
Landing page based on Pratt: http://blacktie.co/demo/pratt/
-->
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Adminlte-laravel - {{ trans('adminlte_lang::message.landingdescription') }} ">
    <meta name="author" content="Sergi Tur Badenas - acacha.org">

    <meta property="og:title" content="Adminlte-laravel" />
    <meta property="og:type" content="website" />
    <meta property="og:description" content="Adminlte-laravel - {{ trans('adminlte_lang::message.landingdescription') }}" />
    <meta property="og:url" content="http://demo.adminlte.acacha.org/" />
    <meta property="og:image" content="http://demo.adminlte.acacha.org/img/AcachaAdminLTE.png" />
    <meta property="og:image" content="http://demo.adminlte.acacha.org/img/AcachaAdminLTE600x600.png" />
    <meta property="og:image" content="http://demo.adminlte.acacha.org/img/AcachaAdminLTE600x314.png" />
    <meta property="og:sitename" content="demo.adminlte.acacha.org" />
    <meta property="og:url" content="http://demo.adminlte.acacha.org" />

    <meta name="twitter:card" content="summary_large_image" />
    <meta name="twitter:site" content="@acachawiki" />
    <meta name="twitter:creator" content="@acacha1" />

    <title>{{ trans('adminlte_lang::message.landingdescriptionpratt') }}</title>

    <!-- Custom styles for this template -->
    <link href="{{ asset('/css/all-landing.css') }}" rel="stylesheet">

    <link href='http://fonts.googleapis.com/css?family=Lato:300,400,700,300italic,400italic' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Raleway:400,300,700' rel='stylesheet' type='text/css'>

</head>

<body data-spy="scroll" data-offset="0" data-target="#navigation">

<div id="app">
    <!-- Fixed navbar -->
    <div id="navigation" class="navbar navbar-default navbar-fixed-top">
      <div style="background-color:#190B07;">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>


            </div>
            <div class="navbar-collapse collapse">

                <ul class="nav navbar-nav navbar-right" >
                    @if (Auth::guest())
                         <h2><li><a href="{{ url('/login') }}">Iniciar Sesión</a></li></h2>

                    @else
                        <li><a href="/home">{{ Auth::user()->name }}</a></li>
                    @endif
                </ul>
            </div><!--/.nav-collapse -->
        </div>
    </div>

</div>
</div>
<!--   <section id="showcase" name="showcase"></section >
    <div id="showcase"> -->

  <div id="features">

      <div style="background-color:#BDB7B0;">
        <div class="container">

            <div class="row">
                <h1 class="centered" style="color:white" >Bienvenido A Sie  </h1>
                <br>
                <div class="col-lg-8 col-lg-offset-2">
                    <div id="carousel-example-generic" class="carousel slide" >

                        <!-- Indicators -->
                        <ol class="carousel-indicators">
                            <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                            <li data-target="#carousel-example-generic" data-slide-to="1"></li>
                            <li data-target="#carousel-example-generic" data-slide-to="2"></li>
                            <li data-target="#carousel-example-generic" data-slide-to="3"></li>
                        </ol>

                        <!-- Wrapper for slides -->
                        <div class="carousel-inner">
                            <div class="item active">
                                <img src="{{ asset('/img/asd.png') }}" alt="">
                            </div>
                            <div class="item">
                                <img src="{{ asset('/img/asd.png') }}" alt="">
                            </div>
                            <div class="item">
                                <img src="{{ asset('/img/asd.png') }}" alt="">
                            </div>
                            <div class="item">
                                <img src="{{ asset('/img/PANTALLA.png') }}" alt="">
                            </div>
                            <!-- Controls -->
                           <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
                          <span class="glyphicon glyphicon-chevron-left"></span>
                          </a>
                         <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
                         <span class="glyphicon glyphicon-chevron-right"></span>
                         </a>
                         <!-- Carousel -->
                        </div>
                    </div>
                </div>
            </div>
            <br>
            <br>
            <br>
        </div><!-- /container -->
 </div>

   </div>




    <section id="desc" name="desc"></section>
    <!-- INTRO WRAP -->
    <div id="intro">
        <div class="container">
            <div class="row centered">
                <h1>Quienes Somos</h1>
                <br>
                <br>
                <div class="col-lg-4">
                    <img src="{{ asset('/img/alpujarra.png') }}" alt=""height="250" width="250">
                    <h3>Gobernacion de Antioquia </h3>
                    <p>Software exclusivo de la <a href="http://antioquia.gov.co/index.php/2014-01-03-13-49-44/2016-02-21-02-28-53">Gobernación de Antioquia</a>  </p>
                </div>
                <div class="col-lg-4" >
                    <img src="{{ asset('/img/nerd.png') }}" alt="" height="250" width="250">
                    <h3>Equipo desarrollador</h3>
                    <p>El equipo esta conformado por cinco profesionales en formación, los cuales cuenta con motivación y ganas de aprender</p>
                </div>
                <div class="col-lg-4">
                    <img src="{{ asset('/img/periodico.png') }}" alt="" height="250" width="250">
                    <h3>Temas de Interes</h3>
                    <p>Actualmente en la gobernación de Antioquia se esta llevando a cabo muchos proyectos con la inteción de mejorar la calidad de vida de las personas.</p>
                </div>
            </div>
            <br>
            <hr>
        </div> <!--/ .container -->
    </div><!--/ #introwrap -->

    <!-- FEATURES WRAP -->

    <div id="features">
        <div style="background-color:#3B170B;">
        <div class="container">

            <div class="row">
                <h1 class="centered" style ="color:white">¿En que consiste este sistema? </h1>
                <br>
                <br>
                <div class="col-lg-6 centered">
                    <img class="centered" src="{{ asset('/img/sistema6.png') }}" alt="" >
                </div>

                <div class="col-lg-6" style="color:white">

                    <br>
                    <!-- ACCORDION -->
                    <div class="accordion ac" id="accordion2">
                        <div class="accordion-group">
                            <div class="accordion-heading">
                                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne">
                                    ¿Cual es su finalidad?
                                </a>
                            </div><!-- /accordion-heading -->
                            <div id="collapseOne" class="accordion-body collapse in">
                                <div class="accordion-inner">
                                    <p> la finalidad de este gran proyecto es contribuir al cumplimiento del plan de desarrolo de la gobernación, secretaria de educación con la estrategia de ecosistemas de innovacion </p>
                                </div><!-- /accordion-inner -->
                            </div><!-- /collapse -->
                        </div><!-- /accordion-group -->
                        <br>

                        <div class="accordion-group">
                            <div class="accordion-heading">
                                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseTwo">
                                    ¿En que consiste?
                                </a>
                            </div>
                            <div id="collapseTwo" class="accordion-body collapse">
                                <div class="accordion-inner">
                                    <p>Consiste en desarrollar un sistema modular que inytedre las diferentes fuentes de informacion de los 117 municipios no certificado, mejorando el flujo entre las diferentes areas de la secretaria por medio de herramientas de manejo de informacion y desarrollo de interfaces graficas </p>
                                </div><!-- /accordion-inner -->
                            </div><!-- /collapse -->
                        </div><!-- /accordion-group -->
                        <br>

                        <div class="accordion-group">
                            <div class="accordion-heading">
                                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseThree">
                                    ¿Hacia quien esta dirigido?
                                </a>
                            </div>
                            <div id="collapseThree" class="accordion-body collapse">
                                <div class="accordion-inner">
                                    <p>Este Proyecto esta dirigido ha todos los funcionarios publicos del area de la gobernación que necesiten realizar consultas o reportes </p>
                                </div><!-- /accordion-inner -->
                            </div><!-- /collapse -->
                        </div><!-- /accordion-group -->
                        <br>

                        <div class="accordion-group">
                            <div class="accordion-heading">
                                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseFour">
                                    ¡Esta es la etapa 1!
                                </a>
                            </div>
                            <div id="collapseFour" class="accordion-body collapse">
                                <div class="accordion-inner">
                                    <p>Actualmente se esta desarrollando la primera etapa, la cual consiste en realizar algunos modulos de carga de informacion y de consulta </p>
                                </div><!-- /accordion-inner -->
                            </div><!-- /collapse -->
                        </div><!-- /accordion-group -->
                        <br>
                    </div><!-- Accordion -->
                </div>
            </div>
        </div><!--/ .container -->
    </div><!--/ #features -->

</div>


    <div id="features">


        <div class="container">

            <div class="row centered">
                <div class="col-lg-12">

                </div>
                <div class="col-lg-2">

                    <h5>Linea de atención</h5>

                    <p>01 8000 415 221</p>
                    <img class="hidden-xs hidden-sm hidden-md" src="{{ asset('/img/arrow1.png') }}">
                </div>
                <div class="col-lg-8">
                    <img class="img-responsive" src="{{ asset('/img/contactanos2.png') }}" alt="">
                </div>
                <div class="col-lg-2">
                    <br>
                    <img class="hidden-xs hidden-sm hidden-md" src="{{ asset('/img/arrow2.png') }}">
                    <h5>Atencion Virtual </h5>
                    <p> Envianos tus sugerencias o dudas <a href="http://antioquia.gov.co/index.php/atencion-a-la-ciudadania">Pincha Aqui</a> </p>
                </div>
            </div>
        </div> <!--/ .container -->
    <!--/ #headerwrap </div>-->
</div>







      <div id="c">
        <div style="background-color:#BDB7B0">
        <div class="container">
            <p>
                <a href="http://www.antioquia.gov.co/"></a><b>Gobernación de Antioquia </b></a>.Secretaria de Educación<br/>
                Creado por <a href="http://www.antioquia.gov.co">Equipo De Desarrollo Gobernación de Antioquia</a>
                <br/>

                 2018 <a href="http://www.antioquia.gov.co">.</a>
            </p>

        </div>

</div>
</div>

<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="{{ asset('/js/app.js') }}"></script>
<script src="{{ asset('/js/smoothscroll.js') }}"></script>
<script>
    $('.carousel').carousel({
        interval: 3500
    })
</script>
</body>
</html>
