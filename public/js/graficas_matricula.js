google.charts.load("current", {
  packages: ["bar"]
});

var anioselect = [];

function anios(element) {
  if (element.checked) {
    anioselect.push(element.value);
  } else {
    anioselect.splice(anioselect.indexOf(element.value), 1);
  }
};

var estableselect = [];

function establematricula(element) {
  estableselect = [];
  if (element.checked) {
    estableselect = element.value;
    sedesselect = ['0'];
  } else {
    estableselect = ['0'];
  }
};

var sedesselect = [];

function sedesmatricula(element) {
  sedesselect = [];
  if (element.checked) {
    sedesselect = element.value;
  } else {
  sedesselect = ['0'];
  }
};

var sectorselect = [];

function sector(element) {
  sectorselect = [];
  if (element.checked) {
    sectorselect.push(element.value);
  } else {
    sectorselect.splice(sectorselect.indexOf(element.value), 1);
  }
};

var calendarioselect = [];

function calendario(element) {
  calendarioselect = [];
  if (element.checked) {
    calendarioselect.push(element.value);
  } else {
    calendarioselect.splice(calendarioselect.indexOf(element.value), 1);
  }
};

var zonaresselect = [];

function zonasresi(element) {
  zonaresselect = [];
  if (element.checked) {
    zonaresselect.push(element.value);
  } else {
    zonaresselect.splice(zonaresselect.indexOf(element.value), 1);
  }
};

var contrataselect = [];

function contrataciones(element) {
  contrataselect = [];
  if (element.checked) {
    contrataselect.push(element.value);
  } else {
    contrataselect.splice(contrataselect.indexOf(element.value), 1);
  }
};

var gradosselect = [];

function grados(element) {
  gradosselect = [];
  if (element.checked) {
    gradosselect.push(element.value);
  } else {
    gradosselect.splice(gradosselect.indexOf(element.value), 1);
  }
};

var victimasselect = [];

function victimas(element) {
  victimasselect = [];
  if (element.checked) {
    victimasselect.push(element.value);
  } else {
    victimasselect.splice(victimasselect.indexOf(element.value), 1);
  }
};
var _titulografica1 = "Numero de matriculas totales ";
var subregiontext = "";
var municipiotext = "";
var sectortext = "";


$('.dynamic').change(function() {
  estableselect = ['0'];
  sedesselect = ['0'];
  subregiontext = this.options[this.selectedIndex].innerText;
  municipiotext = "TODOS";
  if (subregiontext=="TODOS") {
    _titulografica1 = "Numero de matriculas ";
    _titulografica1 = _titulografica1 + "de todas las regiones";
  }else{
    _titulografica1 = "Numero de matriculas ";
    _titulografica1 = _titulografica1 + "de la region de: " + subregiontext;
  }

});
$('.dynamicmuni').change(function() {
  estableselect = ['0'];
  sedesselect = ['0'];
  municipiotext = this.options[this.selectedIndex].innerText;
  if (municipiotext=="TODOS") {
    _titulografica1 = "Numero de matriculas ";
    _titulografica1 = _titulografica1 +"de todos los municipios de la region de: " + subregiontext;
  }else{
    _titulografica1 = "Numero de matriculas ";
    _titulografica1 = _titulografica1 + " de la region de: " + subregiontext+" del municipio de: "+municipiotext;
  }
});



$('.cbsectores').change(function() {
  sectortext = this.value;

  if (subregiontext=="") {
_titulografica1 = "Numero de matriculas ";
      _titulografica1 = _titulografica1 +"del sector: " + sectortext;
  }else{
    _titulografica1 = "Numero de matriculas ";
      _titulografica1 = _titulografica1 + " de la region de: " + subregiontext+" del sector: " + sectortext;
  }
  if (subregiontext!="" && municipiotext!="") {
    _titulografica1 = "Numero de matriculas ";
        _titulografica1 = _titulografica1 + " de la region de: " + subregiontext+" del municipio de: "+municipiotext+" del sector: " + sectortext;
  }

});


//Funcion para calcular el año

function _cambiargrafica() {
  if (sectorselect == '') {
    sectorselect = ['OFICIAL,NO OFICIAL'];
  }
  if (calendarioselect == '') {
    calendarioselect = ['A,B,OTRO'];
  }
  if (estableselect == '') {
    estableselect = ['0'];
  }
  if (sedesselect == '') {
    sedesselect = ['0'];
  }




  /*Obtener valor de Combo SUBREGION*/
  var subregion = 0;
  var municipio = 0;
  var selectsu = document.getElementById('CodigoSubregion');
  subregion = selectsu.value;
  var selectmu = document.getElementById('NombreMunicipio');
  municipio = selectmu.value;

  var sector = 'NULL';
  _cargargraficamatriculas(anioselect, sectorselect, calendarioselect, subregion,municipio,estableselect,sedesselect);



}

function _cambiarzonasgrafica() {
  if (zonaresselect == '') {
    zonaresselect = ['1,2'];
  }

  if (contrataselect == '') {
    contrataselect = ['S,N'];
  }

  if (gradosselect == '') {
    gradosselect = ['-2,-1,0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,21,22,23,24,25,26,99,999'];
  }

  if (victimasselect == '') {
    victimasselect = ['1,2,3,4,5,9'];
  }

  if (estableselect == '') {
    estableselect = ['0'];
  }
  if (sedesselect == '') {
    sedesselect = ['0'];
  }

  /*Obtener valor de Combo SUBREGION*/
  var subregion = 0;
  var municipio = 0;
  var selectsu = document.getElementById('CodigoSubregion');
  subregion = selectsu.value;
  var selectmu = document.getElementById('NombreMunicipio');
  municipio = selectmu.value;
  _vistasmatriculaszona(zonaresselect, contrataselect, gradosselect, victimasselect, anioselect, subregion, municipio,estableselect,sedesselect);
}

function _vistasmatriculaszona(zona, contra, grado, vict, anio, subregion, municipio,estableselect, sedesselect) {
  var url = "grafica_matriculas_zona" + "/" + zona + "/" + contra + "/" + grado + "/" + vict + "/" + anio + "/" + subregion + "/" + municipio + "/" + estableselect + "/" + sedesselect + "";
  $.get(url, function(resul) {
    var data = jQuery.parseJSON(resul);
    if (data == 0) {
      data =  [{"anioTtile":"No se encontraron datos",
      "anioData":[0,0,0,0,0,0,0,0,0,0,0,0]}];
    }
    var mes = {"Mes": ['Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre']};
var nuevoArray = [["Mes"]];

var groupBy = function (data, val) {
return data.reduce(function(grupo, pos) {
    var valor = pos[val];
    grupo[valor] = grupo[valor] || {anioTtile: pos.anioTtile, anioData: []};
    grupo[valor].anioData = pos.anioData;
    return grupo;
}, {});
}
var newdatas = groupBy(data,"anioTtile");
jQuery.each(newdatas, function(index, value){
    nuevoArray[0].push(value.anioTtile.toString());
});

mes.Mes.forEach(function(month) {
  nuevoArray.push([month])
});

jQuery.each(newdatas, function(index, anio){
  var i = 0;
  anio.anioData.forEach(function(dato) {
    nuevoArray[i + 1].push(Number(dato));
    i++;
  });
});
    var info = google.visualization.arrayToDataTable(nuevoArray);
    var options = {
      chart: {
        title: 'Matriculas por Mes ',
        subtitle: 'En los meses ',
        legend: {
          position: 'bottom'
        }
      },

      vAxis: {
        title: 'Matriculas',
        format: 'decimal'
      },
      bars: 'vertical'
    };

    var chart = new google.charts.Bar(document.getElementById('piechart_3d'));
    chart.draw(info, google.charts.Bar.convertOptions(options));
  });
};

function seleccion() {
  $('#boton').toggleClass("btn-danger btn-success");
  var capa = document.getElementById('capa');
  capa.innerHTML += "<button type='button' id='boton' class='btn btn-primary' >Primary</button>";
}



function _cargargraficamatriculas(anio, sector, calendario, subregion, municipio,estableselect,sedeselect) {
  var url = "grafica_matriculas" + "/" + anio + "/" + sector + "/" + calendario + "/" + subregion + "/" + municipio + "/" + estableselect + "/" + sedeselect + "";
  $.get(url, function(resul) {
    var data = jQuery.parseJSON(resul);
    if (data == 0) {
      data =  [{"anioTtile":"No se encontraron datos",
      "anioData":[0,0,0,0,0,0,0,0,0,0,0,0]}];
    }
    var groupBy = function (data, val) {
    return data.reduce(function(grupo, pos) {
        var valor = pos[val];
        grupo[valor] = grupo[valor] || {anioTtile: pos.anioTtile, anioData: []};
        grupo[valor].anioData = pos.anioData;
        return grupo;
    }, {});
}
var datas = groupBy(data,'anioTtile');
/*Funcion para generar colores random*/
var randomColorGenerator = function () {
    return '#' + (Math.random().toString(16) + '0000000').slice(2, 8);
};
/*Funcion para generar colores random segun el array de colores*/
Array.prototype.getRandom= function(cut){
    var i= Math.floor(Math.random()*this.length);
    if(cut && i in this){
        return this.splice(i, 1)[0];
    }
    return this[i];
}

var colors= ['aqua', 'black', 'blue', 'fuchsia', 'gray', 'green',
'lime', 'maroon', 'navy', 'olive', 'orange', 'purple', 'red',
'silver', 'teal', 'white', 'yellow'];


var arrayDataSets = [];
var dataSetAux = {};
jQuery.each(datas, function(index, value){
  var color = colors.getRandom();//Si no quiere los colores de la lista, puede llmara la funcion randomColorGenerator();
   dataSetAux = {
      label: value.anioTtile, //Dependiendo de como este tu objeto de datos, aquí iría el nombre del año.
      backgroundColor: color,
      borderColor: color,
      pointBorderColor: color,
      pointBackgroundColor: color,
      pointHoverBackgroundColor: color,
      pointHoverBorderColor: "rgba(8, 171, 171, 1)",
      borderWidth: 5,
      lineTension: 0,
      fill: false,
      data: value.anioData//Dependiendo de como este tu objeto de datos, aquí iría el arreglo de los datos de ese año.
   }
   arrayDataSets.push(dataSetAux);
});

var buyerData = {
      labels: ['Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'],
      datasets: arrayDataSets
    };
    var ctx = document.getElementById("myChartma").getContext('2d');
    if (window.grafica) {
      window.grafica.clear();
      window.grafica.destroy();
    }
    window.grafica = new Chart(ctx, {
      type: 'line',
      data: buyerData,
      options: {
        responsive: true,
        title: {
          display: true,
          text: _titulografica1
        },
        tooltips: {
          mode: 'index',
          intersect: false
        },
        hover: {
          mode: 'nearest',
          intersect: true
        },
        scales: {
          xAxes: [{
            display: true,
            scaleLabel: {
              display: true,
              labelString: 'Mes'
            }
          }],
          yAxes: [{
            display: true,
            scaleLabel: {
              display: true,
              labelString: 'Numero de matriculas'
            },
            ticks: {
              min: 0

            }
          }]

        }
      }

    });

  })
}
