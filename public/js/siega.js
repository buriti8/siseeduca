

function borrar_anexo(ano_info, mes_corte){

  var urlraiz=$("#url_raiz_proyecto").val();
  $("#capa_modal").show();
  $("#capa_formularios").show();
  var screenTop = $(document).scrollTop();
  $("#capa_formularios").css('top', screenTop);
  $("#capa_formularios").html($("#cargador_empresa").html());
  var miurl=urlraiz+"/form_borrado_anexo/"+ano_info+"/"+mes_corte+"";


  $.ajax({
    url: miurl
  }).done( function(resul)
  {
    $("#capa_formularios").html(resul);
  }).fail( function()
  {
    $("#capa_formularios").html('<span>...Ha ocurrido un error, revise su conexión y vuelva a intentarlo...</span>');
  }) ;


}

function borrar_archivos_due(ano_info, mes_corte){

  var urlraiz=$("#url_raiz_proyecto").val();
  $("#capa_modal").show();
  $("#capa_formularios").show();
  var screenTop = $(document).scrollTop();
  $("#capa_formularios").css('top', screenTop);
  $("#capa_formularios").html($("#cargador_empresa").html());
  var miurl=urlraiz+"/form_borrado_archivos_due/"+ano_info+"/"+mes_corte+"";


  $.ajax({
    url: miurl
  }).done( function(resul)
  {
    $("#capa_formularios").html(resul);
  }).fail( function()
  {
    $("#capa_formularios").html('<span>...Ha ocurrido un error, revise su conexión y vuelva a intentarlo...</span>');
  }) ;


}

function borrar_archivos_due_nacional(ano_info, mes_corte){

  var urlraiz=$("#url_raiz_proyecto").val();
  $("#capa_modal").show();
  $("#capa_formularios").show();
  var screenTop = $(document).scrollTop();
  $("#capa_formularios").css('top', screenTop);
  $("#capa_formularios").html($("#cargador_empresa").html());
  var miurl=urlraiz+"/form_borrado_archivos_due_nacional/"+ano_info+"/"+mes_corte+"";


  $.ajax({
    url: miurl
  }).done( function(resul)
  {
    $("#capa_formularios").html(resul);
  }).fail( function()
  {
    $("#capa_formularios").html('<span>...Ha ocurrido un error, revise su conexión y vuelva a intentarlo...</span>');
  }) ;


}

function borrar_archivos_mesa(ano_info, mes_corte){

  var urlraiz=$("#url_raiz_proyecto").val();
  $("#capa_modal").show();
  $("#capa_formularios").show();
  var screenTop = $(document).scrollTop();
  $("#capa_formularios").css('top', screenTop);
  $("#capa_formularios").html($("#cargador_empresa").html());
  var miurl=urlraiz+"/form_borrado_archivos_mesa/"+ano_info+"/"+mes_corte+"";


  $.ajax({
    url: miurl
  }).done( function(resul)
  {
    $("#capa_formularios").html(resul);
  }).fail( function()
  {
    $("#capa_formularios").html('<span>...Ha ocurrido un error, revise su conexión y vuelva a intentarlo...</span>');
  }) ;


}


function borrar_archivos_conectividad(ano_info, mes_corte){

  var urlraiz=$("#url_raiz_proyecto").val();
  $("#capa_modal").show();
  $("#capa_formularios").show();
  var screenTop = $(document).scrollTop();
  $("#capa_formularios").css('top', screenTop);
  $("#capa_formularios").html($("#cargador_empresa").html());
  var miurl=urlraiz+"/form_borrado_archivos_conectividad/"+ano_info+"/"+mes_corte+"";


  $.ajax({
    url: miurl
  }).done( function(resul)
  {
    $("#capa_formularios").html(resul);
  }).fail( function()
  {
    $("#capa_formularios").html('<span>...Ha ocurrido un error, revise su conexión y vuelva a intentarlo...</span>');
  }) ;


}

function borrar_archivos_calidad(ano_info, mes_corte){

  var urlraiz=$("#url_raiz_proyecto").val();
  $("#capa_modal").show();
  $("#capa_formularios").show();
  var screenTop = $(document).scrollTop();
  $("#capa_formularios").css('top', screenTop);
  $("#capa_formularios").html($("#cargador_empresa").html());
  var miurl=urlraiz+"/form_borrado_archivos_calidad/"+ano_info+"/"+mes_corte+"";


  $.ajax({
    url: miurl
  }).done( function(resul)
  {
    $("#capa_formularios").html(resul);
  }).fail( function()
  {
    $("#capa_formularios").html('<span>...Ha ocurrido un error, revise su conexión y vuelva a intentarlo...</span>');
  }) ;


}

function borrar_archivos_saber(periodo){

  var urlraiz=$("#url_raiz_proyecto").val();
  $("#capa_modal").show();
  $("#capa_formularios").show();
  var screenTop = $(document).scrollTop();
  $("#capa_formularios").css('top', screenTop);
  $("#capa_formularios").html($("#cargador_empresa").html());
  var miurl=urlraiz+"/form_borrado_archivos_saber/"+periodo+"";


  $.ajax({
    url: miurl
  }).done( function(resul)
  {
    $("#capa_formularios").html(resul);
  }).fail( function()
  {
    $("#capa_formularios").html('<span>Ha ocurrido un error, revise su conexión y vuelva a intentarlo.</span>');
  }) ;


}

function borrar_archivos_clasificaciones(periodo){

  var urlraiz=$("#url_raiz_proyecto").val();
  $("#capa_modal").show();
  $("#capa_formularios").show();
  var screenTop = $(document).scrollTop();
  $("#capa_formularios").css('top', screenTop);
  $("#capa_formularios").html($("#cargador_empresa").html());
  var miurl=urlraiz+"/form_borrado_archivos_clasificaciones/"+periodo+"";


  $.ajax({
    url: miurl
  }).done( function(resul)
  {
    $("#capa_formularios").html(resul);
  }).fail( function()
  {
    $("#capa_formularios").html('<span>Ha ocurrido un error, revise su conexión y vuelva a intentarlo.</span>');
  }) ;


}

function borrar_carga_dispositivo(num_carga)
{
  var urlraiz=$("#url_raiz_proyecto").val();
  $("#capa_modal").show();
  $("#capa_formularios").show();
  var screenTop = $(document).scrollTop();
  $("#capa_formularios").css('top', screenTop);
  $("#capa_formularios").html($("#cargador_empresa").html());
  var miurl=urlraiz+"/form_borrado_dispositivos/"+num_carga+"";

  $.ajax({
    url: miurl
  }).done( function(resul)
  {
    $("#capa_formularios").html(resul);
  }).fail( function()
  {
    $("#capa_formularios").html('<span>...Ha ocurrido un error, revise su conexión y vuelva a intentarlo...</span>');
  }) ;
}

function  verinfo_usuario(arg){
  var urlraiz=$("#url_raiz_proyecto").val();
  var miurl =urlraiz+"/form_editar_usuario/"+arg+"";
  $("#capa_modal").show();
  $("#capa_formularios").show();
  var screenTop = $(document).scrollTop();
  $("#capa_formularios").css('top', screenTop);
  $("#capa_formularios").html($("#cargador_empresa").html());

  $.ajax({
    url: miurl
  }).done( function(resul)
  {
    $("#capa_formularios").html(resul);

  }).fail( function()
  {
    $("#capa_formularios").html('<span>...Ha ocurrido un error, revise su conexión y vuelva a intentarlo...</span>');
  }) ;

}


function  verinfo_comiteJunta(arg){
  var urlraiz=$("#url_raiz_proyecto").val();
  var miurl =urlraiz+"/form_editar_comiteJunta/"+arg+"";
  $("#capa_modal").show();
  $("#capa_formularios").show();
  var screenTop = $(document).scrollTop();
  $("#capa_formularios").css('top', screenTop);
  $("#capa_formularios").html($("#cargador_empresa").html());

  $.ajax({
    url: miurl
  }).done( function(resul)
  {
    $("#capa_formularios").html(resul);

  }).fail( function()
  {
    $("#capa_formularios").html('<span>...Ha ocurrido un error, revise su conexión y vuelva a intentarlo...</span>');
  }) ;

}


function  verinfo_comiteDocumento(arg){
  var urlraiz=$("#url_raiz_proyecto").val();
  var miurl =urlraiz+"/form_editar_documento_estructura/"+arg+"";
  $("#capa_modal").show();
  $("#capa_formularios").show();
  var screenTop = $(document).scrollTop();
  $("#capa_formularios").css('top', screenTop);
  $("#capa_formularios").html($("#cargador_empresa").html());

  $.ajax({
    url: miurl
  }).done( function(resul)
  {
    $("#capa_formularios").html(resul);

  }).fail( function()
  {
    $("#capa_formularios").html('<span>...Ha ocurrido un error, revise su conexión y vuelva a intentarlo...</span>');
  }) ;

}



function  verinfo_usuario2(arg){
  var urlraiz=$("#url_raiz_proyecto").val();
  var miurl =urlraiz+"/form_editar_usuario2/"+arg+"";
  $("#capa_modal").show();
  $("#capa_formularios").show();
  var screenTop = $(document).scrollTop();
  $("#capa_formularios").css('top', screenTop);
  $("#capa_formularios").html($("#cargador_empresa").html());

  $.ajax({
    url: miurl
  }).done( function(resul)
  {
    $("#capa_formularios").html(resul);

  }).fail( function()
  {
    $("#capa_formularios").html('<span>...Ha ocurrido un error, revise su conexión y vuelva a intentarlo...</span>');
  }) ;

}

function  verinfo_departamento(arg){
  var urlraiz=$("#url_raiz_proyecto").val();
  var miurl =urlraiz+"/form_editar_departamento/"+arg+"";
  $("#capa_modal").show();
  $("#capa_formularios").show();
  var screenTop = $(document).scrollTop();
  $("#capa_formularios").css('top', screenTop);
  $("#capa_formularios").html($("#cargador_empresa").html());

  $.ajax({
    url: miurl
  }).done( function(resul)
  {
    $("#capa_formularios").html(resul);

  }).fail( function()
  {
    $("#capa_formularios").html('<span>...Ha ocurrido un error, revise su conexión y vuelva a intentarlo...</span>');
  }) ;

}


//FUNCIÓN FORMULARIO EDITAR SUBREGIÓN
function  verinfo_subregion(arg){


  var urlraiz=$("#url_raiz_proyecto").val();
  var miurl =urlraiz+"/form_editar_subregion/"+arg+"";
  $("#capa_modal").show();
  $("#capa_formularios").show();
  var screenTop = $(document).scrollTop();
  $("#capa_formularios").css('top', screenTop);
  $("#capa_formularios").html($("#cargador_empresa").html());

  $.ajax({
    url: miurl
  }).done( function(resul)
  {
    $("#capa_formularios").html(resul);

  }).fail( function()
  {
    $("#capa_formularios").html('<span>...Ha ocurrido un error, revise su conexión y vuelva a intentarlo...</span>');
  }) ;

}


//FUNCIÓN FORMULARIO EDITAR Municipio
function  verinfo_municipio(arg){
  var urlraiz=$("#url_raiz_proyecto").val();
  var miurl =urlraiz+"/form_editar_municipio/"+arg+"";
  $("#capa_modal").show();
  $("#capa_formularios").show();
  var screenTop = $(document).scrollTop();
  $("#capa_formularios").css('top', screenTop);
  $("#capa_formularios").html($("#cargador_empresa").html());

  $.ajax({
    url: miurl
  }).done( function(resul)
  {
    $("#capa_formularios").html(resul);

  }).fail( function()
  {
    $("#capa_formularios").html('<span>...Ha ocurrido un error, revise su conexión y vuelva a intentarlo...</span>');
  }) ;

}


function  verinfo_grado(arg){
  var urlraiz=$("#url_raiz_proyecto").val();
  var miurl =urlraiz+"/form_editar_grado/"+arg+"";
  $("#capa_modal").show();
  $("#capa_formularios").show();
  var screenTop = $(document).scrollTop();
  $("#capa_formularios").css('top', screenTop);
  $("#capa_formularios").html($("#cargador_empresa").html());

  $.ajax({
    url: miurl
  }).done( function(resul)
  {
    $("#capa_formularios").html(resul);

  }).fail( function()
  {
    $("#capa_formularios").html('<span>...Ha ocurrido un error, revise su conexión y vuelva a intentarlo...</span>');
  }) ;

}


function  verinfo_tipodocumento(arg){


  var urlraiz=$("#url_raiz_proyecto").val();
  var miurl =urlraiz+"/form_editar_tipodocumento/"+arg+"";
  $("#capa_modal").show();
  $("#capa_formularios").show();
  var screenTop = $(document).scrollTop();
  $("#capa_formularios").css('top', screenTop);
  $("#capa_formularios").html($("#cargador_empresa").html());

  $.ajax({
    url: miurl
  }).done( function(resul)
  {
    $("#capa_formularios").html(resul);

  }).fail( function()
  {
    $("#capa_formularios").html('<span>...Ha ocurrido un error, revise su conexión y vuelva a intentarlo...</span>');
  }) ;

}

function  verinfo_conalunmant(arg){


  var urlraiz=$("#url_raiz_proyecto").val();
  var miurl =urlraiz+"/form_editar_conalunmant/"+arg+"";
  $("#capa_modal").show();
  $("#capa_formularios").show();
  var screenTop = $(document).scrollTop();
  $("#capa_formularios").css('top', screenTop);
  $("#capa_formularios").html($("#cargador_empresa").html());

  $.ajax({
    url: miurl
  }).done( function(resul)
  {
    $("#capa_formularios").html(resul);

  }).fail( function()
  {
    $("#capa_formularios").html('<span>...Ha ocurrido un error, revise su conexión y vuelva a intentarlo...</span>');
  }) ;

}

function  verinfo_sitacaant(arg){


  var urlraiz=$("#url_raiz_proyecto").val();
  var miurl =urlraiz+"/form_editar_sitacaant/"+arg+"";
  $("#capa_modal").show();
  $("#capa_formularios").show();
  var screenTop = $(document).scrollTop();
  $("#capa_formularios").css('top', screenTop);
  $("#capa_formularios").html($("#cargador_empresa").html());

  $.ajax({
    url: miurl
  }).done( function(resul)
  {
    $("#capa_formularios").html(resul);

  }).fail( function()
  {
    $("#capa_formularios").html('<span>...Ha ocurrido un error, revise su conexión y vuelva a intentarlo...</span>');
  }) ;

}

function  verinfo_zona(arg){


  var urlraiz=$("#url_raiz_proyecto").val();
  var miurl =urlraiz+"/form_editar_zona/"+arg+"";
  $("#capa_modal").show();
  $("#capa_formularios").show();
  var screenTop = $(document).scrollTop();
  $("#capa_formularios").css('top', screenTop);
  $("#capa_formularios").html($("#cargador_empresa").html());

  $.ajax({
    url: miurl
  }).done( function(resul)
  {
    $("#capa_formularios").html(resul);

  }).fail( function()
  {
    $("#capa_formularios").html('<span>...Ha ocurrido un error, revise su conexión y vuelva a intentarlo...</span>');
  }) ;

}

function  verinfo_tipodiscapacidad(arg){


  var urlraiz=$("#url_raiz_proyecto").val();
  var miurl =urlraiz+"/form_editar_tipodiscapacidad/"+arg+"";
  $("#capa_modal").show();
  $("#capa_formularios").show();
  var screenTop = $(document).scrollTop();
  $("#capa_formularios").css('top', screenTop);
  $("#capa_formularios").html($("#cargador_empresa").html());

  $.ajax({
    url: miurl
  }).done( function(resul)
  {
    $("#capa_formularios").html(resul);

  }).fail( function()
  {
    $("#capa_formularios").html('<span>...Ha ocurrido un error, revise su conexión y vuelva a intentarlo...</span>');
  }) ;

}

function  verinfo_metodologia(arg){


  var urlraiz=$("#url_raiz_proyecto").val();
  var miurl =urlraiz+"/form_editar_metodologia/"+arg+"";
  $("#capa_modal").show();
  $("#capa_formularios").show();
  var screenTop = $(document).scrollTop();
  $("#capa_formularios").css('top', screenTop);
  $("#capa_formularios").html($("#cargador_empresa").html());

  $.ajax({
    url: miurl
  }).done( function(resul)
  {
    $("#capa_formularios").html(resul);

  }).fail( function()
  {
    $("#capa_formularios").html('<span>...Ha ocurrido un error, revise su conexión y vuelva a intentarlo...</span>');
  }) ;

}

function  verinfo_estrato(arg){


  var urlraiz=$("#url_raiz_proyecto").val();
  var miurl =urlraiz+"/form_editar_estrato/"+arg+"";
  $("#capa_modal").show();
  $("#capa_formularios").show();
  var screenTop = $(document).scrollTop();
  $("#capa_formularios").css('top', screenTop);
  $("#capa_formularios").html($("#cargador_empresa").html());

  $.ajax({
    url: miurl
  }).done( function(resul)
  {
    $("#capa_formularios").html(resul);

  }).fail( function()
  {
    $("#capa_formularios").html('<span>...Ha ocurrido un error, revise su conexión y vuelva a intentarlo...</span>');
  }) ;

}

function  verinfo_resguardo(arg){


  var urlraiz=$("#url_raiz_proyecto").val();
  var miurl =urlraiz+"/form_editar_resguardo/"+arg+"";
  $("#capa_modal").show();
  $("#capa_formularios").show();
  var screenTop = $(document).scrollTop();
  $("#capa_formularios").css('top', screenTop);
  $("#capa_formularios").html($("#cargador_empresa").html());

  $.ajax({
    url: miurl
  }).done( function(resul)
  {
    $("#capa_formularios").html(resul);

  }).fail( function()
  {
    $("#capa_formularios").html('<span>...Ha ocurrido un error, revise su conexión y vuelva a intentarlo...</span>');
  }) ;

}

function  verinfo_jornada(arg){


  var urlraiz=$("#url_raiz_proyecto").val();
  var miurl =urlraiz+"/form_editar_jornada/"+arg+"";
  $("#capa_modal").show();
  $("#capa_formularios").show();
  var screenTop = $(document).scrollTop();
  $("#capa_formularios").css('top', screenTop);
  $("#capa_formularios").html($("#cargador_empresa").html());

  $.ajax({
    url: miurl
  }).done( function(resul)
  {
    $("#capa_formularios").html(resul);

  }).fail( function()
  {
    $("#capa_formularios").html('<span>...Ha ocurrido un error, revise su conexión y vuelva a intentarlo...</span>');
  }) ;

}
//function TIPO VICTIMA CONFLICTO
function  verinfo_victimaconflicto(arg){

  var urlraiz=$("#url_raiz_proyecto").val();
  var miurl =urlraiz+"/form_editar_victimaconflicto/"+arg+"";
  $("#capa_modal").show();
  $("#capa_formularios").show();
  var screenTop = $(document).scrollTop();
  $("#capa_formularios").css('top', screenTop);
  $("#capa_formularios").html($("#cargador_empresa").html());

  $.ajax({
    url: miurl
  }).done( function(resul)
  {
    $("#capa_formularios").html(resul);

  }).fail( function()
  {
    $("#capa_formularios").html('<span>...Ha ocurrido un error, revise su conexión y vuelva a intentarlo...</span>');
  }) ;

}

//function TIPO CAPACIDAD EXCEPCIONAL
function  verinfo_capacidadexcepcional(arg){

  var urlraiz=$("#url_raiz_proyecto").val();
  var miurl =urlraiz+"/form_editar_capacidadexcepcional/"+arg+"";
  $("#capa_modal").show();
  $("#capa_formularios").show();
  var screenTop = $(document).scrollTop();
  $("#capa_formularios").css('top', screenTop);
  $("#capa_formularios").html($("#cargador_empresa").html());

  $.ajax({
    url: miurl
  }).done( function(resul)
  {
    $("#capa_formularios").html(resul);

  }).fail( function()
  {
    $("#capa_formularios").html('<span>...Ha ocurrido un error, revise su conexión y vuelva a intentarlo...</span>');
  }) ;

}
//function TIPO ETNIA
function  verinfo_etnia(arg){

  var urlraiz=$("#url_raiz_proyecto").val();
  var miurl =urlraiz+"/form_editar_etnia/"+arg+"";
  $("#capa_modal").show();
  $("#capa_formularios").show();
  var screenTop = $(document).scrollTop();
  $("#capa_formularios").css('top', screenTop);
  $("#capa_formularios").html($("#cargador_empresa").html());

  $.ajax({
    url: miurl
  }).done( function(resul)
  {
    $("#capa_formularios").html(resul);

  }).fail( function()
  {
    $("#capa_formularios").html('<span>...Ha ocurrido un error, revise su conexión y vuelva a intentarlo...</span>');
  }) ;

}
function  verinfo_nivel(arg){

  var urlraiz=$("#url_raiz_proyecto").val();
  var miurl =urlraiz+"/form_editar_nivel/"+arg+"";
  $("#capa_modal").show();
  $("#capa_formularios").show();
  var screenTop = $(document).scrollTop();
  $("#capa_formularios").css('top', screenTop);
  $("#capa_formularios").html($("#cargador_empresa").html());

  $.ajax({
    url: miurl
  }).done( function(resul)
  {
    $("#capa_formularios").html(resul);

  }).fail( function()
  {
    $("#capa_formularios").html('<span>...Ha ocurrido un error, revise su conexión y vuelva a intentarlo...</span>');
  }) ;

}
function  verinfo_nivelmediatotal(arg){

  var urlraiz=$("#url_raiz_proyecto").val();
  var miurl =urlraiz+"/form_editar_nivelmediatotal/"+arg+"";
  $("#capa_modal").show();
  $("#capa_formularios").show();
  var screenTop = $(document).scrollTop();
  $("#capa_formularios").css('top', screenTop);
  $("#capa_formularios").html($("#cargador_empresa").html());

  $.ajax({
    url: miurl
  }).done( function(resul)
  {
    $("#capa_formularios").html(resul);

  }).fail( function()
  {
    $("#capa_formularios").html('<span>...Ha ocurrido un error, revise su conexión y vuelva a intentarlo...</span>');
  }) ;

}

//function TIPO NIVEL CINE
function  verinfo_nivelcine(arg){

  var urlraiz=$("#url_raiz_proyecto").val();
  var miurl =urlraiz+"/form_editar_nivelcine/"+arg+"";
  $("#capa_modal").show();
  $("#capa_formularios").show();
  var screenTop = $(document).scrollTop();
  $("#capa_formularios").css('top', screenTop);
  $("#capa_formularios").html($("#cargador_empresa").html());

  $.ajax({
    url: miurl
  }).done( function(resul)
  {
    $("#capa_formularios").html(resul);

  }).fail( function()
  {
    $("#capa_formularios").html('<span>...Ha ocurrido un error, revise su conexión y vuelva a intentarlo...</span>');
  }) ;

}

//function RectorDirector
function  verinfo_rectordirector(arg){

  var urlraiz=$("#url_raiz_proyecto").val();
  var miurl =urlraiz+"/form_editar_rectordirector/"+arg+"";
  $("#capa_modal").show();
  $("#capa_formularios").show();
  var screenTop = $(document).scrollTop();
  $("#capa_formularios").css('top', screenTop);
  $("#capa_formularios").html($("#cargador_empresa").html());

  $.ajax({
    url: miurl
  }).done( function(resul)
  {
    $("#capa_formularios").html(resul);

  }).fail( function()
  {
    $("#capa_formularios").html('<span>...Ha ocurrido un error, revise su conexión y vuelva a intentarlo...</span>');
  }) ;
}

//Origen dispositivo
function  verinfo_origen(arg){
  var urlraiz=$("#url_raiz_proyecto").val();
  var miurl =urlraiz+"/form_editar_origen/"+arg+"";
  $("#capa_modal").show();
  $("#capa_formularios").show();
  var screenTop = $(document).scrollTop();
  $("#capa_formularios").css('top', screenTop);
  $("#capa_formularios").html($("#cargador_empresa").html());

  $.ajax({
    url: miurl
  }).done( function(resul)
  {
    $("#capa_formularios").html(resul);

  }).fail( function()
  {
    $("#capa_formularios").html('<span>...Ha ocurrido un error, revise su conexión y vuelva a intentarlo...</span>');
  }) ;
}
function borrado_origen(idori){
  var urlraiz=$("#url_raiz_proyecto").val();
  $("#capa_modal").show();
  $("#capa_formularios").show();
  var screenTop = $(document).scrollTop();
  $("#capa_formularios").css('top', screenTop);
  $("#capa_formularios").html($("#cargador_empresa").html());
  var miurl=urlraiz+"/form_borrado_origen/"+idori+"";

  $.ajax({
    url: miurl
  }).done( function(resul)
  {
    $("#capa_formularios").html(resul);

  }).fail( function()
  {
    $("#capa_formularios").html('<span>...Ha ocurrido un error, revise su conexión y vuelva a intentarlo...</span>');
  }) ;
}

//Tipo de dispositivo
function  verinfo_tipodispositivo(arg){
  var urlraiz=$("#url_raiz_proyecto").val();
  var miurl =urlraiz+"/form_editar_tipodispositivo/"+arg+"";
  $("#capa_modal").show();
  $("#capa_formularios").show();
  var screenTop = $(document).scrollTop();
  $("#capa_formularios").css('top', screenTop);
  $("#capa_formularios").html($("#cargador_empresa").html());

  $.ajax({
    url: miurl
  }).done( function(resul)
  {
    $("#capa_formularios").html(resul);

  }).fail( function()
  {
    $("#capa_formularios").html('<span>...Ha ocurrido un error, revise su conexión y vuelva a intentarlo...</span>');
  }) ;
}
function borrado_tipodispositivo(idtip){
  var urlraiz=$("#url_raiz_proyecto").val();
  $("#capa_modal").show();
  $("#capa_formularios").show();
  var screenTop = $(document).scrollTop();
  $("#capa_formularios").css('top', screenTop);
  $("#capa_formularios").html($("#cargador_empresa").html());
  var miurl=urlraiz+"/form_borrado_tipodispositivo/"+idtip+"";

  $.ajax({
    url: miurl
  }).done( function(resul)
  {
    $("#capa_formularios").html(resul);

  }).fail( function()
  {
    $("#capa_formularios").html('<span>...Ha ocurrido un error, revise su conexión y vuelva a intentarlo...</span>');
  }) ;
}

//Proyecto
function  verinfo_proyecto(arg){
  var urlraiz=$("#url_raiz_proyecto").val();
  var miurl =urlraiz+"/form_editar_proyecto/"+arg+"";
  $("#capa_modal").show();
  $("#capa_formularios").show();
  var screenTop = $(document).scrollTop();
  $("#capa_formularios").css('top', screenTop);
  $("#capa_formularios").html($("#cargador_empresa").html());

  $.ajax({
    url: miurl
  }).done( function(resul)
  {
    $("#capa_formularios").html(resul);

  }).fail( function()
  {
    $("#capa_formularios").html('<span>...Ha ocurrido un error, revise su conexión y vuelva a intentarlo...</span>');
  }) ;
}
function borrado_proyecto(idpro){
  var urlraiz=$("#url_raiz_proyecto").val();
  $("#capa_modal").show();
  $("#capa_formularios").show();
  var screenTop = $(document).scrollTop();
  $("#capa_formularios").css('top', screenTop);
  $("#capa_formularios").html($("#cargador_empresa").html());
  var miurl=urlraiz+"/form_borrado_proyecto/"+idpro+"";

  $.ajax({
    url: miurl
  }).done( function(resul)
  {
    $("#capa_formularios").html(resul);

  }).fail( function()
  {
    $("#capa_formularios").html('<span>...Ha ocurrido un error, revise su conexión y vuelva a intentarlo...</span>');
  }) ;
}
//Contrato
function  verinfo_contrato(arg){
  var urlraiz=$("#url_raiz_proyecto").val();
  var miurl =urlraiz+"/form_editar_contrato/"+arg+"";
  $("#capa_modal").show();
  $("#capa_formularios").show();
  var screenTop = $(document).scrollTop();
  $("#capa_formularios").css('top', screenTop);
  $("#capa_formularios").html($("#cargador_empresa").html());

  $.ajax({
    url: miurl
  }).done( function(resul)
  {
    $("#capa_formularios").html(resul);

  }).fail( function()
  {
    $("#capa_formularios").html('<span>...Ha ocurrido un error, revise su conexión y vuelva a intentarlo...</span>');
  }) ;
}
function borrado_contrato(idcon){
  var urlraiz=$("#url_raiz_proyecto").val();
  $("#capa_modal").show();
  $("#capa_formularios").show();
  var screenTop = $(document).scrollTop();
  $("#capa_formularios").css('top', screenTop);
  $("#capa_formularios").html($("#cargador_empresa").html());
  var miurl=urlraiz+"/form_borrado_contrato/"+idcon+"";

  $.ajax({
    url: miurl
  }).done( function(resul)
  {
    $("#capa_formularios").html(resul);

  }).fail( function()
  {
    $("#capa_formularios").html('<span>...Ha ocurrido un error, revise su conexión y vuelva a intentarlo...</span>');
  }) ;
}

$(document).on("click",".div_modal", function(e){
  $(this).hide();
  $("#capa_formularios").hide();
  $("#capa_formularios").html("");
})


function ocultar_informacion(){

  //función encargada de ocualtar o cerrar el modal de información que se activa al ingresar al sistema.

  document.getElementById('verInformacion').style.display = 'none';
}

function cerrar_modal() {

  //función encargada de cerrar el modal o ventanas emergentes de los módulos o súbmodulos.

  var x = document.getElementById('capa_modal');
  if (x.style.display === 'block') {
    x.style.display = 'none';
  } else {
    x.style.display = 'block';
  }

  var z = document.getElementById('capa_formularios');
  if (z.style.display === 'block') {
    z.style.display = 'none';
  } else {
    z.style.display = 'block';
  }

}


//infraestructura fisica educativa, actualizacion

function  verinfo_sedesinfra(arg){

  var urlraiz=$("#url_raiz_proyecto").val();
  var miurl =urlraiz+"/form_editar_sedesinfra/"+arg+"";
  $("#capa_modal").show();
  $("#capa_formularios").show();
  var screenTop = $(document).scrollTop();
  $("#capa_formularios").css('top', screenTop);
  $("#capa_formularios").html($("#cargador_empresa").html());

  $.ajax({
    url: miurl
  }).done( function(resul)
  {
    $("#capa_formularios").html(resul);

  }).fail( function()
  {
    $("#capa_formularios").html('<span>...Ha ocurrido un error, revise su conexión y vuelva a intentarlo...</span>');
  }) ;

}

function borrado_sedesinfra(iddept){

  var urlraiz=$("#url_raiz_proyecto").val();
  $("#capa_modal").show();
  $("#capa_formularios").show();
  var screenTop = $(document).scrollTop();
  $("#capa_formularios").css('top', screenTop);
  $("#capa_formularios").html($("#cargador_empresa").html());
  var miurl=urlraiz+"/form_borrado_sedesinfra/"+iddept+"";


  $.ajax({
    url: miurl
  }).done( function(resul)
  {
    $("#capa_formularios").html(resul);

  }).fail( function()
  {
    $("#capa_formularios").html('<span>...Ha ocurrido un error, revise su conexión y vuelva a intentarlo...</span>');
  }) ;
}

function  verinfo_alcaldes(arg){

  var urlraiz=$("#url_raiz_proyecto").val();
  var miurl =urlraiz+"/form_editar_alcaldes/"+arg+"";
  $("#capa_modal").show();
  $("#capa_formularios").show();
  var screenTop = $(document).scrollTop();
  $("#capa_formularios").css('top', screenTop);
  $("#capa_formularios").html($("#cargador_empresa").html());
  var d = new Date();
  var strDate = d.getFullYear() + "/" + (d.getMonth()+1) + "/" + d.getDate();
  $("#fechaescritura").attr('max', strDate);
  $.ajax({
    url: miurl
  }).done( function(resul)
  {
    $("#capa_formularios").html(resul);

  }).fail( function()
  {
    $("#capa_formularios").html('<span>Ha ocurrido un error, revise su conexión y vuelva a intentarlo.</span>');
  }) ;

}


function borrar_alcalde(iddept){

  var urlraiz=$("#url_raiz_proyecto").val();
  $("#capa_modal").show();
  $("#capa_formularios").show();
  var screenTop = $(document).scrollTop();
  $("#capa_formularios").css('top', screenTop);
  $("#capa_formularios").html($("#cargador_empresa").html());
  var miurl=urlraiz+"/form_borrar_alcalde/"+iddept+"";


  $.ajax({
    url: miurl
  }).done( function(resul)
  {
    $("#capa_formularios").html(resul);

  }).fail( function()
  {
    $("#capa_formularios").html('<span>...Ha ocurrido un error, revise su conexión y vuelva a intentarlo...</span>');
  }) ;
}


function  verinfo_secretarios(arg){

  var urlraiz=$("#url_raiz_proyecto").val();
  var miurl =urlraiz+"/form_editar_secretarios/"+arg+"";
  $("#capa_modal").show();
  $("#capa_formularios").show();
  var screenTop = $(document).scrollTop();
  $("#capa_formularios").css('top', screenTop);
  $("#capa_formularios").html($("#cargador_empresa").html());
  var d = new Date();
  var strDate = d.getFullYear() + "/" + (d.getMonth()+1) + "/" + d.getDate();
  $("#fechaescritura").attr('max', strDate);
  $.ajax({
    url: miurl
  }).done( function(resul)
  {
    $("#capa_formularios").html(resul);

  }).fail( function()
  {
    $("#capa_formularios").html('<span>Ha ocurrido un error, revise su conexión y vuelva a intentarlo.</span>');
  }) ;

}


function  verinfo_rectores(arg){

  var urlraiz=$("#url_raiz_proyecto").val();
  var miurl =urlraiz+"/form_editar_rectores/"+arg+"";
  $("#capa_modal").show();
  $("#capa_formularios").show();
  var screenTop = $(document).scrollTop();
  $("#capa_formularios").css('top', screenTop);
  $("#capa_formularios").html($("#cargador_empresa").html());
  var d = new Date();
  var strDate = d.getFullYear() + "/" + (d.getMonth()+1) + "/" + d.getDate();
  $("#fechaescritura").attr('max', strDate);
  $.ajax({
    url: miurl
  }).done( function(resul)
  {
    $("#capa_formularios").html(resul);

  }).fail( function()
  {
    $("#capa_formularios").html('<span>Ha ocurrido un error, revise su conexión y vuelva a intentarlo.</span>');
  }) ;

}

function borrar_rectores(iddept){

  var urlraiz=$("#url_raiz_proyecto").val();
  $("#capa_modal").show();
  $("#capa_formularios").show();
  var screenTop = $(document).scrollTop();
  $("#capa_formularios").css('top', screenTop);
  $("#capa_formularios").html($("#cargador_empresa").html());
  var miurl=urlraiz+"/form_borrar_rectores/"+iddept+"";


  $.ajax({
    url: miurl
  }).done( function(resul)
  {
    $("#capa_formularios").html(resul);

  }).fail( function()
  {
    $("#capa_formularios").html('<span>...Ha ocurrido un error, revise su conexión y vuelva a intentarlo...</span>');
  }) ;
}


function  verinfo_adminsimat(arg){

  var urlraiz=$("#url_raiz_proyecto").val();
  var miurl =urlraiz+"/form_editar_adminsimat/"+arg+"";
  $("#capa_modal").show();
  $("#capa_formularios").show();
  var screenTop = $(document).scrollTop();
  $("#capa_formularios").css('top', screenTop);
  $("#capa_formularios").html($("#cargador_empresa").html());
  var d = new Date();
  var strDate = d.getFullYear() + "/" + (d.getMonth()+1) + "/" + d.getDate();
  $("#fechaescritura").attr('max', strDate);
  $.ajax({
    url: miurl
  }).done( function(resul)
  {
    $("#capa_formularios").html(resul);

  }).fail( function()
  {
    $("#capa_formularios").html('<span>Ha ocurrido un error, revise su conexión y vuelva a intentarlo.</span>');
  }) ;

}


function  verinfo_enlacetic(arg){

  var urlraiz=$("#url_raiz_proyecto").val();
  var miurl =urlraiz+"/form_editar_enlacestic/"+arg+"";
  $("#capa_modal").show();
  $("#capa_formularios").show();
  var screenTop = $(document).scrollTop();
  $("#capa_formularios").css('top', screenTop);
  $("#capa_formularios").html($("#cargador_empresa").html());
  var d = new Date();
  var strDate = d.getFullYear() + "/" + (d.getMonth()+1) + "/" + d.getDate();
  $("#fechaescritura").attr('max', strDate);
  $.ajax({
    url: miurl
  }).done( function(resul)
  {
    $("#capa_formularios").html(resul);

  }).fail( function()
  {
    $("#capa_formularios").html('<span>Ha ocurrido un error, revise su conexión y vuelva a intentarlo.</span>');
  }) ;

}


function borrar_enlacestic(iddept){

  var urlraiz=$("#url_raiz_proyecto").val();
  $("#capa_modal").show();
  $("#capa_formularios").show();
  var screenTop = $(document).scrollTop();
  $("#capa_formularios").css('top', screenTop);
  $("#capa_formularios").html($("#cargador_empresa").html());
  var miurl=urlraiz+"/form_borrado_enlacestic/"+iddept+"";


  $.ajax({
    url: miurl
  }).done( function(resul)
  {
    $("#capa_formularios").html(resul);

  }).fail( function()
  {
    $("#capa_formularios").html('<span>...Ha ocurrido un error, revise su conexión y vuelva a intentarlo...</span>');
  }) ;
}


function  verinfo_jefesnucleos(arg){

  var urlraiz=$("#url_raiz_proyecto").val();
  var miurl =urlraiz+"/form_editar_directornucleo/"+arg+"";
  $("#capa_modal").show();
  $("#capa_formularios").show();
  var screenTop = $(document).scrollTop();
  $("#capa_formularios").css('top', screenTop);
  $("#capa_formularios").html($("#cargador_empresa").html());
  var d = new Date();
  var strDate = d.getFullYear() + "/" + (d.getMonth()+1) + "/" + d.getDate();
  $("#fechaescritura").attr('max', strDate);
  $.ajax({
    url: miurl
  }).done( function(resul)
  {
    $("#capa_formularios").html(resul);

  }).fail( function()
  {
    $("#capa_formularios").html('<span>Ha ocurrido un error, revise su conexión y vuelva a intentarlo.</span>');
  }) ;

}


function borrar_directornucleo(iddept){

  var urlraiz=$("#url_raiz_proyecto").val();
  $("#capa_modal").show();
  $("#capa_formularios").show();
  var screenTop = $(document).scrollTop();
  $("#capa_formularios").css('top', screenTop);
  $("#capa_formularios").html($("#cargador_empresa").html());
  var miurl=urlraiz+"/form_borrado_directornucleo/"+iddept+"";


  $.ajax({
    url: miurl
  }).done( function(resul)
  {
    $("#capa_formularios").html(resul);

  }).fail( function()
  {
    $("#capa_formularios").html('<span>...Ha ocurrido un error, revise su conexión y vuelva a intentarlo...</span>');
  }) ;
}


function  verinfo_jefesplaneacion(arg){

  var urlraiz=$("#url_raiz_proyecto").val();
  var miurl =urlraiz+"/form_editar_secretarioplaneacion/"+arg+"";
  $("#capa_modal").show();
  $("#capa_formularios").show();
  var screenTop = $(document).scrollTop();
  $("#capa_formularios").css('top', screenTop);
  $("#capa_formularios").html($("#cargador_empresa").html());
  var d = new Date();
  var strDate = d.getFullYear() + "/" + (d.getMonth()+1) + "/" + d.getDate();
  $("#fechaescritura").attr('max', strDate);
  $.ajax({
    url: miurl
  }).done( function(resul)
  {
    $("#capa_formularios").html(resul);

  }).fail( function()
  {
    $("#capa_formularios").html('<span>Ha ocurrido un error, revise su conexión y vuelva a intentarlo.</span>');
  }) ;

}


function borrar_jefesplaneacion(iddept){

  var urlraiz=$("#url_raiz_proyecto").val();
  $("#capa_modal").show();
  $("#capa_formularios").show();
  var screenTop = $(document).scrollTop();
  $("#capa_formularios").css('top', screenTop);
  $("#capa_formularios").html($("#cargador_empresa").html());
  var miurl=urlraiz+"/form_borrado_secretarioplaneacion/"+iddept+"";


  $.ajax({
    url: miurl
  }).done( function(resul)
  {
    $("#capa_formularios").html(resul);

  }).fail( function()
  {
    $("#capa_formularios").html('<span>...Ha ocurrido un error, revise su conexión y vuelva a intentarlo...</span>');
  }) ;
}


function  verinfo_comitestic(arg){

  var urlraiz=$("#url_raiz_proyecto").val();
  var miurl =urlraiz+"/form_editar_comitestic/"+arg+"";
  $("#capa_modal").show();
  $("#capa_formularios").show();
  var screenTop = $(document).scrollTop();
  $("#capa_formularios").css('top', screenTop);
  $("#capa_formularios").html($("#cargador_empresa").html());
  var d = new Date();
  var strDate = d.getFullYear() + "/" + (d.getMonth()+1) + "/" + d.getDate();
  $("#fechaescritura").attr('max', strDate);
  $.ajax({
    url: miurl
  }).done( function(resul)
  {
    $("#capa_formularios").html(resul);

  }).fail( function()
  {
    $("#capa_formularios").html('<span>Ha ocurrido un error, revise su conexión y vuelva a intentarlo.</span>');
  }) ;

}


function borrado_comitestic(iddept){

  var urlraiz=$("#url_raiz_proyecto").val();
  $("#capa_modal").show();
  $("#capa_formularios").show();
  var screenTop = $(document).scrollTop();
  $("#capa_formularios").css('top', screenTop);
  $("#capa_formularios").html($("#cargador_empresa").html());
  var miurl=urlraiz+"/form_borrado_comitestic/"+iddept+"";


  $.ajax({
    url: miurl
  }).done( function(resul)
  {
    $("#capa_formularios").html(resul);

  }).fail( function()
  {
    $("#capa_formularios").html('<span>...Ha ocurrido un error, revise su conexión y vuelva a intentarlo...</span>');
  }) ;
}


function  verinfo_jume(arg){

  var urlraiz=$("#url_raiz_proyecto").val();
  var miurl =urlraiz+"/form_editar_jume/"+arg+"";
  $("#capa_modal").show();
  $("#capa_formularios").show();
  var screenTop = $(document).scrollTop();
  $("#capa_formularios").css('top', screenTop);
  $("#capa_formularios").html($("#cargador_empresa").html());
  var d = new Date();
  var strDate = d.getFullYear() + "/" + (d.getMonth()+1) + "/" + d.getDate();
  $("#fechaescritura").attr('max', strDate);
  $.ajax({
    url: miurl
  }).done( function(resul)
  {
    $("#capa_formularios").html(resul);

  }).fail( function()
  {
    $("#capa_formularios").html('<span>Ha ocurrido un error, revise su conexión y vuelva a intentarlo.</span>');
  }) ;

}


function borrado_jume(iddept){

  var urlraiz=$("#url_raiz_proyecto").val();
  $("#capa_modal").show();
  $("#capa_formularios").show();
  var screenTop = $(document).scrollTop();
  $("#capa_formularios").css('top', screenTop);
  $("#capa_formularios").html($("#cargador_empresa").html());
  var miurl=urlraiz+"/form_borrado_jume/"+iddept+"";


  $.ajax({
    url: miurl
  }).done( function(resul)
  {
    $("#capa_formularios").html(resul);

  }).fail( function()
  {
    $("#capa_formularios").html('<span>...Ha ocurrido un error, revise su conexión y vuelva a intentarlo...</span>');
  }) ;
}

function  verinfo_comitescupos(arg){

  var urlraiz=$("#url_raiz_proyecto").val();
  var miurl =urlraiz+"/form_editar_comitescupos/"+arg+"";
  $("#capa_modal").show();
  $("#capa_formularios").show();
  var screenTop = $(document).scrollTop();
  $("#capa_formularios").css('top', screenTop);
  $("#capa_formularios").html($("#cargador_empresa").html());
  var d = new Date();
  var strDate = d.getFullYear() + "/" + (d.getMonth()+1) + "/" + d.getDate();
  $("#fechaescritura").attr('max', strDate);
  $.ajax({
    url: miurl
  }).done( function(resul)
  {
    $("#capa_formularios").html(resul);

  }).fail( function()
  {
    $("#capa_formularios").html('<span>Ha ocurrido un error, revise su conexión y vuelva a intentarlo.</span>');
  }) ;

}


function borrado_comitescupos(iddept){

  var urlraiz=$("#url_raiz_proyecto").val();
  $("#capa_modal").show();
  $("#capa_formularios").show();
  var screenTop = $(document).scrollTop();
  $("#capa_formularios").css('top', screenTop);
  $("#capa_formularios").html($("#cargador_empresa").html());
  var miurl=urlraiz+"/form_borrado_comitescupos/"+iddept+"";


  $.ajax({
    url: miurl
  }).done( function(resul)
  {
    $("#capa_formularios").html(resul);

  }).fail( function()
  {
    $("#capa_formularios").html('<span>...Ha ocurrido un error, revise su conexión y vuelva a intentarlo...</span>');
  }) ;
}

function  verinfo_documentos(arg){

  var urlraiz=$("#url_raiz_proyecto").val();
  var miurl =urlraiz+"/form_editar_documentos/"+arg+"";
  $("#capa_modal").show();
  $("#capa_formularios").show();
  var screenTop = $(document).scrollTop();
  $("#capa_formularios").css('top', screenTop);
  $("#capa_formularios").html($("#cargador_empresa").html());
  var d = new Date();
  var strDate = d.getFullYear() + "/" + (d.getMonth()+1) + "/" + d.getDate();
  $("#fechaescritura").attr('max', strDate);
  $.ajax({
    url: miurl
  }).done( function(resul)
  {
    $("#capa_formularios").html(resul);

  }).fail( function()
  {
    $("#capa_formularios").html('<span>Ha ocurrido un error, revise su conexión y vuelva a intentarlo.</span>');
  }) ;

}


function borrado_documentos(iddept){

  var urlraiz=$("#url_raiz_proyecto").val();
  $("#capa_modal").show();
  $("#capa_formularios").show();
  var screenTop = $(document).scrollTop();
  $("#capa_formularios").css('top', screenTop);
  $("#capa_formularios").html($("#cargador_empresa").html());
  var miurl=urlraiz+"/form_borrado_documentos/"+iddept+"";


  $.ajax({
    url: miurl
  }).done( function(resul)
  {
    $("#capa_formularios").html(resul);

  }).fail( function()
  {
    $("#capa_formularios").html('<span>...Ha ocurrido un error, revise su conexión y vuelva a intentarlo...</span>');
  }) ;
}


function  verinfo_sedeslegal(arg){

  var urlraiz=$("#url_raiz_proyecto").val();
  var miurl =urlraiz+"/form_editar_sedeslegal/"+arg+"";
  $("#capa_modal").show();
  $("#capa_formularios").show();
  var screenTop = $(document).scrollTop();
  $("#capa_formularios").css('top', screenTop);
  $("#capa_formularios").html($("#cargador_empresa").html());
  var d = new Date();
  var strDate = d.getFullYear() + "/" + (d.getMonth()+1) + "/" + d.getDate();
  $("#fechaescritura").attr('max', strDate);
  $.ajax({
    url: miurl
  }).done( function(resul)
  {
    $("#capa_formularios").html(resul);

  }).fail( function()
  {
    $("#capa_formularios").html('<span>...Ha ocurrido un error, revise su conexión y vuelva a intentarlo...</span>');
  }) ;

}


function  verinfo_sedesriesgos(arg){

  var urlraiz=$("#url_raiz_proyecto").val();
  var miurl =urlraiz+"/form_editar_sedesriesgos/"+arg+"";
  $("#capa_modal").show();
  $("#capa_formularios").show();
  var screenTop = $(document).scrollTop();
  $("#capa_formularios").css('top', screenTop);
  $("#capa_formularios").html($("#cargador_empresa").html());
  var d = new Date();
  var strDate = d.getFullYear() + "/" + (d.getMonth()+1) + "/" + d.getDate();
  $("#fechaescritura").attr('max', strDate);
  $.ajax({
    url: miurl
  }).done( function(resul)
  {
    $("#capa_formularios").html(resul);

  }).fail( function()
  {
    $("#capa_formularios").html('<span>...Ha ocurrido un error, revise su conexión y vuelva a intentarlo...</span>');
  }) ;

}





function borrado_sedeslegal(iddept){

  var urlraiz=$("#url_raiz_proyecto").val();
  $("#capa_modal").show();
  $("#capa_formularios").show();
  var screenTop = $(document).scrollTop();
  $("#capa_formularios").css('top', screenTop);
  $("#capa_formularios").html($("#cargador_empresa").html());
  var miurl=urlraiz+"/form_borrado_sedeslegal/"+iddept+"";


  $.ajax({
    url: miurl
  }).done( function(resul)
  {
    $("#capa_formularios").html(resul);

  }).fail( function()
  {
    $("#capa_formularios").html('<span>...Ha ocurrido un error, revise su conexión y vuelva a intentarlo...</span>');
  }) ;
}





function  verinfo_sedesservicio(arg){

  var urlraiz=$("#url_raiz_proyecto").val();
  var miurl =urlraiz+"/form_editar_sedesservicio/"+arg+"";
  $("#capa_modal").show();
  $("#capa_formularios").show();
  var screenTop = $(document).scrollTop();
  $("#capa_formularios").css('top', screenTop);
  $("#capa_formularios").html($("#cargador_empresa").html());

  $.ajax({
    url: miurl
  }).done( function(resul)
  {
    $("#capa_formularios").html(resul);

  }).fail( function()
  {
    $("#capa_formularios").html('<span>...Ha ocurrido un error, revise su conexión y vuelva a intentarlo...</span>');
  }) ;

}

function borrado_sedesservicio(iddept){

  var urlraiz=$("#url_raiz_proyecto").val();
  $("#capa_modal").show();
  $("#capa_formularios").show();
  var screenTop = $(document).scrollTop();
  $("#capa_formularios").css('top', screenTop);
  $("#capa_formularios").html($("#cargador_empresa").html());
  var miurl=urlraiz+"/form_borrado_sedesservicio/"+iddept+"";


  $.ajax({
    url: miurl
  }).done( function(resul)
  {
    $("#capa_formularios").html(resul);

  }).fail( function()
  {
    $("#capa_formularios").html('<span>...Ha ocurrido un error, revise su conexión y vuelva a intentarlo...</span>');
  }) ;
}

function borrado_sedesriesgos(iddept){

  var urlraiz=$("#url_raiz_proyecto").val();
  $("#capa_modal").show();
  $("#capa_formularios").show();
  var screenTop = $(document).scrollTop();
  $("#capa_formularios").css('top', screenTop);
  $("#capa_formularios").html($("#cargador_empresa").html());
  var miurl=urlraiz+"/form_borrado_sedesriesgos/"+iddept+"";


  $.ajax({
    url: miurl
  }).done( function(resul)
  {
    $("#capa_formularios").html(resul);

  }).fail( function()
  {
    $("#capa_formularios").html('<span>Ha ocurrido un error, revise su conexión y vuelva a intentarlo</span>');
  }) ;
}

function borrado_sedesespacios(iddept){

  var urlraiz=$("#url_raiz_proyecto").val();
  $("#capa_modal").show();
  $("#capa_formularios").show();
  var screenTop = $(document).scrollTop();
  $("#capa_formularios").css('top', screenTop);
  $("#capa_formularios").html($("#cargador_empresa").html());
  var miurl=urlraiz+"/form_borrado_sedesespacios/"+iddept+"";


  $.ajax({
    url: miurl
  }).done( function(resul)
  {
    $("#capa_formularios").html(resul);

  }).fail( function()
  {
    $("#capa_formularios").html('<span>Ha ocurrido un error, revise su conexión y vuelva a intentarlo</span>');
  }) ;
}



function  verinfo_sedesaulas(arg){

  var urlraiz=$("#url_raiz_proyecto").val();
  var miurl =urlraiz+"/form_editar_sedesaulas/"+arg+"";

  $("#capa_modal").show();
  $("#capa_formularios").show();
  var screenTop = $(document).scrollTop();
  $("#capa_formularios").css('top', screenTop);
  $("#capa_formularios").html($("#cargador_empresa").html());

  $.ajax({
    url: miurl
  }).done( function(resul)
  {
    $("#capa_formularios").html(resul);

  }).fail( function()
  {
    $("#capa_formularios").html('<span>...Ha ocurrido un error, revise su conexión y vuelva a intentarlo...</span>');
  }) ;

}

function  verinfo_sedesbaterias(arg){

  var urlraiz=$("#url_raiz_proyecto").val();
  var miurl =urlraiz+"/form_editar_sedesbaterias/"+arg+"";

  $("#capa_modal").show();
  $("#capa_formularios").show();
  var screenTop = $(document).scrollTop();
  $("#capa_formularios").css('top', screenTop);
  $("#capa_formularios").html($("#cargador_empresa").html());

  $.ajax({
    url: miurl
  }).done( function(resul)
  {
    $("#capa_formularios").html(resul);

  }).fail( function()
  {
    $("#capa_formularios").html('<span>...Ha ocurrido un error, revise su conexión y vuelva a intentarlo...</span>');
  }) ;

}


function  verinfo_sedesotros(arg){

  var urlraiz=$("#url_raiz_proyecto").val();
  var miurl =urlraiz+"/form_editar_sedesotros/"+arg+"";

  $("#capa_modal").show();
  $("#capa_formularios").show();
  var screenTop = $(document).scrollTop();
  $("#capa_formularios").css('top', screenTop);
  $("#capa_formularios").html($("#cargador_empresa").html());

  $.ajax({
    url: miurl
  }).done( function(resul)
  {
    $("#capa_formularios").html(resul);

  }).fail( function()
  {
    $("#capa_formularios").html('<span>...Ha ocurrido un error, revise su conexión y vuelva a intentarlo...</span>');
  }) ;

}

function borrado_sedesespacio(iddept){

  var urlraiz=$("#url_raiz_proyecto").val();
  $("#capa_modal").show();
  $("#capa_formularios").show();
  var screenTop = $(document).scrollTop();
  $("#capa_formularios").css('top', screenTop);
  $("#capa_formularios").html($("#cargador_empresa").html());
  var miurl=urlraiz+"/form_borrado_sedesespacio/"+iddept+"";


  $.ajax({
    url: miurl
  }).done( function(resul)
  {
    $("#capa_formularios").html(resul);

  }).fail( function()
  {
    $("#capa_formularios").html('<span>...Ha ocurrido un error, revise su conexión y vuelva a intentarlo...</span>');
  }) ;
}

function  verinfo_sedesconectividad(arg){

  var urlraiz=$("#url_raiz_proyecto").val();
  var miurl =urlraiz+"/form_editar_sedesconectividad/"+arg+"";
  $("#capa_modal").show();
  $("#capa_formularios").show();
  var screenTop = $(document).scrollTop();
  $("#capa_formularios").css('top', screenTop);
  $("#capa_formularios").html($("#cargador_empresa").html());

  $.ajax({
    url: miurl
  }).done( function(resul)
  {
    $("#capa_formularios").html(resul);

  }).fail( function()
  {
    $("#capa_formularios").html('<span>...Ha ocurrido un error, revise su conexión y vuelva a intentarlo...</span>');
  }) ;

}

function borrado_sedesconectividad(iddept){

  var urlraiz=$("#url_raiz_proyecto").val();
  $("#capa_modal").show();
  $("#capa_formularios").show();
  var screenTop = $(document).scrollTop();
  $("#capa_formularios").css('top', screenTop);
  $("#capa_formularios").html($("#cargador_empresa").html());
  var miurl=urlraiz+"/form_borrado_sedesconectividad/"+iddept+"";


  $.ajax({
    url: miurl
  }).done( function(resul)
  {
    $("#capa_formularios").html(resul);

  }).fail( function()
  {
    $("#capa_formularios").html('<span>...Ha ocurrido un error, revise su conexión y vuelva a intentarlo...</span>');
  }) ;
}

function  verinfo_sedesescritura(arg){

  var urlraiz=$("#url_raiz_proyecto").val();
  var miurl =urlraiz+"/form_editar_sedesescritura/"+arg+"";
  $("#capa_modal").show();
  $("#capa_formularios").show();
  var screenTop = $(document).scrollTop();
  $("#capa_formularios").css('top', screenTop);
  $("#capa_formularios").html($("#cargador_empresa").html());
  var d = new Date();
  var strDate = d.getFullYear() + "/" + (d.getMonth()+1) + "/" + d.getDate();
  $("#fechaescritura").attr('max', strDate);
  $.ajax({
    url: miurl
  }).done( function(resul)
  {
    $("#capa_formularios").html(resul);

  }).fail( function()
  {
    $("#capa_formularios").html('<span>...Ha ocurrido un error, revise su conexión y vuelva a intentarlo...</span>');
  }) ;

}

function borrado_sedesescritura(iddept){

  var urlraiz=$("#url_raiz_proyecto").val();
  $("#capa_modal").show();
  $("#capa_formularios").show();
  var screenTop = $(document).scrollTop();
  $("#capa_formularios").css('top', screenTop);
  $("#capa_formularios").html($("#cargador_empresa").html());
  var miurl=urlraiz+"/form_borrado_sedesescritura/"+iddept+"";


  $.ajax({
    url: miurl
  }).done( function(resul)
  {
    $("#capa_formularios").html(resul);

  }).fail( function()
  {
    $("#capa_formularios").html('<span>...Ha ocurrido un error, revise su conexión y vuelva a intentarlo...</span>');
  }) ;
}



function cargar_formulario(arg)
{
  var urlraiz=$("#url_raiz_proyecto").val();
  $("#capa_modal").show();
  $("#capa_formularios").show();
  var screenTop = $(document).scrollTop();
  $("#capa_formularios").css('top', screenTop);
  $("#capa_formularios").html($("#cargador_empresa").html());
  if(arg==1){ var miurl=urlraiz+"/form_nuevo_usuario"; }
  if(arg==2){ var miurl=urlraiz+"/form_nuevo_rol"; }
  if(arg==3){ var miurl=urlraiz+"/form_nuevo_permiso"; }
  if(arg==4){ var miurl=urlraiz+"/form_nuevo_departamento"; }
  if(arg==5){ var miurl=urlraiz+"/form_nuevo_tipodocumento"; }
  if(arg==6){ var miurl=urlraiz+"/form_nuevo_conalunmant"; }
  if(arg==7){ var miurl=urlraiz+"/form_nuevo_sitacaant"; }
  if(arg==8){ var miurl=urlraiz+"/form_nuevo_zona"; }
  if(arg==9){ var miurl=urlraiz+"/form_nuevo_tipodiscapacidad"; }
  if(arg==10){ var miurl=urlraiz+"/form_nuevo_metodologia"; }
  if(arg==11){ var miurl=urlraiz+"/form_nuevo_estrato"; }
  if(arg==12){ var miurl=urlraiz+"/form_nuevo_resguardo"; }
  if(arg==13){ var miurl=urlraiz+"/form_nuevo_jornada"; }
  if(arg==14){ var miurl=urlraiz+"/form_nuevo_victimaconflicto"; }
  if(arg==15){ var miurl=urlraiz+"/form_nuevo_capacidadexcepcional"; }
  if(arg==16){ var miurl=urlraiz+"/form_nuevo_etnia"; }
  if(arg==17){ var miurl=urlraiz+"/form_nuevo_nivel"; }
  if(arg==18){ var miurl=urlraiz+"/form_nuevo_nivelmediatotal"; }
  if(arg==19){ var miurl=urlraiz+"/form_nuevo_nivelcine"; }
  if(arg==20){ var miurl=urlraiz+"/form_nuevo_subregion"; }
  if(arg==21){ var miurl=urlraiz+"/form_nuevo_grado"; }
  if(arg==22){ var miurl=urlraiz+"/form_nuevo_municipio"; }
  if(arg==23){ var miurl=urlraiz+"/form_nuevo_rectordirector"; }
  //
  if(arg==24){ var miurl=urlraiz+"/form_nuevo_origen"; }
  if(arg==25){ var miurl=urlraiz+"/form_nuevo_tipodispositivo"; }
  if(arg==26){ var miurl=urlraiz+"/form_nuevo_proyecto"; }
  if(arg==27){ var miurl=urlraiz+"/form_nuevo_contrato"; }
  if(arg==30){ var miurl=urlraiz+"/form_nuevo_sedesinfra"; }
  if(arg==31){ var miurl=urlraiz+"/form_nuevo_sedeslegal"; }
  if(arg==32){ var miurl=urlraiz+"/form_nuevo_sedesservicio"; }
  if(arg==33){ var miurl=urlraiz+"/form_nuevo_sedesconectividad"; }
  if(arg==34){ var miurl=urlraiz+"/form_nuevo_sedesotros"; }
  if(arg==35){ var miurl=urlraiz+"/form_nuevo_sedesaulas"; }
  if(arg==36){ var miurl=urlraiz+"/form_nuevo_sedesbaterias"; }
  if(arg==40){ var miurl=urlraiz+"/form_nuevo_sedesescritura"; }
  if(arg==41){ var miurl=urlraiz+"/form_nuevo_riesgo"; }
  if(arg==42){ var miurl=urlraiz+"/form_ingresar_riesgo"; }
  if(arg==43){ var miurl=urlraiz+"/form_ingresar_alcaldes"; }
  if(arg==44){ var miurl=urlraiz+"/form_ingresar_rectores"; }
  if(arg==45){ var miurl=urlraiz+"/form_ingresar_enlacetic"; }
  if(arg==46){ var miurl=urlraiz+"/form_ingresar_secretarioplaneacion"; }
  if(arg==47){ var miurl=urlraiz+"/form_ingresar_directornucleo"; }
  if(arg==48){ var miurl=urlraiz+"/form_ingresar_comites"; }
  if(arg==49){ var miurl=urlraiz+"/form_ingresar_sedesservicio"; }
  if(arg==50){ var miurl=urlraiz+"/form_nuevo_modal"; }
  if(arg==51){ var miurl=urlraiz+"/form_ingresar_documento"; }
  if(arg==52){ var miurl=urlraiz+"/form_nuevo_comiteJunta"; }
  if(arg==53){ var miurl=urlraiz+"/form_nuevo_documento"; }


  $.ajax({
    url: miurl
  }).done( function(resul)
  {
    $("#capa_formularios").html(resul);
  }).fail( function()
  {
    $("#capa_formularios").html('<span>...Ha ocurrido un error, revise su conexión y vuelva a intentarlo...</span>');
  }) ;

}

//Hace referencia a id y class de los form.
$(document).on("submit",".formentrada",function(e){
  e.preventDefault();
  var quien=$(this).attr("id");
  var formu=$(this);
  var varurl="";
  if(quien=="f_crear_usuario"){  var varurl=$(this).attr("action");  var div_resul="capa_formularios";  }
  if(quien=="f_crear_departamento"){  var varurl=$(this).attr("action");  var div_resul="capa_formularios";  }
  if(quien=="f_crear_tipodocumento"){  var varurl=$(this).attr("action");  var div_resul="capa_formularios";  }
  if(quien=="f_crear_conalunmant"){  var varurl=$(this).attr("action");  var div_resul="capa_formularios";  }
  if(quien=="f_crear_sitacaant"){  var varurl=$(this).attr("action");  var div_resul="capa_formularios";  }
  if(quien=="f_crear_zona"){  var varurl=$(this).attr("action");  var div_resul="capa_formularios";  }
  if(quien=="f_crear_tipodiscapacidad"){  var varurl=$(this).attr("action");  var div_resul="capa_formularios";  }
  if(quien=="f_crear_metodologia"){  var varurl=$(this).attr("action");  var div_resul="capa_formularios";  }
  if(quien=="f_crear_estrato"){  var varurl=$(this).attr("action");  var div_resul="capa_formularios";  }
  if(quien=="f_crear_resguardo"){  var varurl=$(this).attr("action");  var div_resul="capa_formularios";  }
  if(quien=="f_crear_jornada"){  var varurl=$(this).attr("action");  var div_resul="capa_formularios";  }
  if(quien=="f_crear_victimaconflicto"){  var varurl=$(this).attr("action");  var div_resul="capa_formularios";  }

  if(quien=="f_crear_rol"){  var varurl=$(this).attr("action");  var div_resul="capa_formularios";  }
  if(quien=="f_crear_permiso"){  var varurl=$(this).attr("action");  var div_resul="capa_formularios";  }

  if(quien=="f_editar_usuario"){  var varurl=$(this).attr("action");  var div_resul="notificacion_E2";  }
  if(quien=="f_editar_usuario2"){  var varurl=$(this).attr("action");  var div_resul="notificacion_E2";  }
  if(quien=="f_editar_departamento"){  var varurl=$(this).attr("action");  var div_resul="notificacion_E2";  }
  if(quien=="f_editar_tipodocumento"){  var varurl=$(this).attr("action");  var div_resul="notificacion_E2";  }
  if(quien=="f_editar_conalunmant"){  var varurl=$(this).attr("action");  var div_resul="notificacion_E2";  }
  if(quien=="f_editar_sitacaant"){  var varurl=$(this).attr("action");  var div_resul="notificacion_E2";  }
  if(quien=="f_editar_zona"){  var varurl=$(this).attr("action");  var div_resul="notificacion_E2";  }
  if(quien=="f_editar_tipodiscapacidad"){  var varurl=$(this).attr("action");  var div_resul="notificacion_E2";  }
  if(quien=="f_editar_metodologia"){  var varurl=$(this).attr("action");  var div_resul="notificacion_E2";  }
  if(quien=="f_editar_resguardo"){  var varurl=$(this).attr("action");  var div_resul="notificacion_E2";  }
  if(quien=="f_editar_estrato"){  var varurl=$(this).attr("action");  var div_resul="notificacion_E2";  }
  if(quien=="f_editar_jornada"){  var varurl=$(this).attr("action");  var div_resul="notificacion_E2";  }
  if(quien=="f_editar_victimaconflicto"){  var varurl=$(this).attr("action");  var div_resul="notificacion_E2";  }
  if(quien=="f_editar_acceso"){  var varurl=$(this).attr("action");  var div_resul="notificacion_E3";  }


  if(quien=="f_borrar_usuario"){  var varurl=$(this).attr("action");  var div_resul="capa_formularios";  }
  if(quien=="f_borrar_departamento"){  var varurl=$(this).attr("action");  var div_resul="capa_formularios";  }
  if(quien=="f_borrar_tipodocumento"){  var varurl=$(this).attr("action");  var div_resul="capa_formularios";  }
  if(quien=="f_borrar_conalunmant"){  var varurl=$(this).attr("action");  var div_resul="capa_formularios";  }
  if(quien=="f_borrar_sitacaant"){  var varurl=$(this).attr("action");  var div_resul="capa_formularios";  }
  if(quien=="f_borrar_zona"){  var varurl=$(this).attr("action");  var div_resul="capa_formularios";  }
  if(quien=="f_borrar_tipodiscapacidad"){  var varurl=$(this).attr("action");  var div_resul="capa_formularios";  }
  if(quien=="f_borrar_metodologia"){  var varurl=$(this).attr("action");  var div_resul="capa_formularios";  }
  if(quien=="f_borrar_estrato"){  var varurl=$(this).attr("action");  var div_resul="capa_formularios";  }
  if(quien=="f_borrar_resguardo"){  var varurl=$(this).attr("action");  var div_resul="capa_formularios";  }
  if(quien=="f_borrar_jornada"){  var varurl=$(this).attr("action");  var div_resul="capa_formularios";  }
  if(quien=="f_borrar_victimaconflicto"){  var varurl=$(this).attr("action");  var div_resul="capa_formularios";  }
  if(quien=="f_borrar_anexo"){  var varurl=$(this).attr("action");  var div_resul="capa_formularios";  }
  if(quien=="f_borrar_archivos_due"){  var varurl=$(this).attr("action");  var div_resul="capa_formularios";  }
  if(quien=="f_borrar_archivos_mesa"){  var varurl=$(this).attr("action");  var div_resul="capa_formularios";  }
  if(quien=="f_borrar_archivos_calidad"){  var varurl=$(this).attr("action");  var div_resul="capa_formularios";  }
  if(quien=="f_borrar_archivos_sedesconectividad"){  var varurl=$(this).attr("action");  var div_resul="capa_formularios";  }
  if(quien=="f_borrar_archivos_conectividad"){  var varurl=$(this).attr("action");  var div_resul="capa_formularios";  }
  if(quien=="elimnasolicitudif"){  var varurl=$(this).attr("action");  var div_resul="capa_formularios";  }
  if(quien=="f_borrar_archivos_saber"){  var varurl=$(this).attr("action");  var div_resul="capa_formularios";  }
  if(quien=="f_borrar_archivos_clasificaciones"){  var varurl=$(this).attr("action");  var div_resul="capa_formularios";  }
  if(quien=="f_borrar_archivos_due_nacional"){  var varurl=$(this).attr("action");  var div_resul="capa_formularios";  }


  if(quien=="f_asignar_permiso"){  var varurl=$(this).attr("action");  var div_resul="capa_formularios";  }

  //NEW.............................................................................................................
  //IF CREAR CAPACIDAD EXCEPCIONAL
  if(quien=="f_crear_capacidadexcepcional"){  var varurl=$(this).attr("action");  var div_resul="capa_formularios";  }
  //IF EDITAR CAPACIDAD EXCEPCIONAL
  if(quien=="f_editar_capacidadexcepcional"){  var varurl=$(this).attr("action");  var div_resul="notificacion_E2";  }
  //IF BORRAR VICTIMA CONFLICT
  if(quien=="f_borrar_capacidadexcepcional"){  var varurl=$(this).attr("action");  var div_resul="capa_formularios";  }

  //NEW.............................................................................................................
  //IF CREAR ETNIA
  if(quien=="f_crear_etnia"){  var varurl=$(this).attr("action");  var div_resul="capa_formularios";  }
  //IF EDITAR CAPACIDAD EXCEPCIONAL
  if(quien=="f_editar_etnia"){  var varurl=$(this).attr("action");  var div_resul="notificacion_E2";  }
  //IF BORRAR VICTIMA CONFLICT
  if(quien=="f_borrar_etnia"){  var varurl=$(this).attr("action");  var div_resul="capa_formularios";  }


  //IF CREAR SUBREGIÓN..................................................................................
  if(quien=="f_crear_subregion"){  var varurl=$(this).attr("action");  var div_resul="capa_formularios";  }
  //IF EDITAR SUBREGIÓN
  if(quien=="f_editar_subregion"){  var varurl=$(this).attr("action");  var div_resul="notificacion_E2";  }
  //IF BORRAR SUBREGIÓN
  if(quien=="f_borrar_subregion"){  var varurl=$(this).attr("action");  var div_resul="capa_formularios";  }

  //NEW.............................................................................................................
  //IF CREAR NIVEL
  if(quien=="f_crear_nivel"){  var varurl=$(this).attr("action");  var div_resul="capa_formularios";  }
  //IF EDITAR CAPACIDAD EXCEPCIONAL
  if(quien=="f_editar_nivel"){  var varurl=$(this).attr("action");  var div_resul="notificacion_E2";  }
  //IF BORRAR VICTIMA CONFLICT
  if(quien=="f_borrar_nivel"){  var varurl=$(this).attr("action");  var div_resul="capa_formularios";  }

  //NEW.............................................................................................................
  //IF CREAR NIVEL
  if(quien=="f_crear_nivelmediatotal"){  var varurl=$(this).attr("action");  var div_resul="capa_formularios";  }
  //IF EDITAR CAPACIDAD EXCEPCIONAL
  if(quien=="f_editar_nivelmediatotal"){  var varurl=$(this).attr("action");  var div_resul="notificacion_E2";  }
  //IF BORRAR VICTIMA CONFLICT
  if(quien=="f_borrar_nivelmediatotal"){  var varurl=$(this).attr("action");  var div_resul="capa_formularios";  }

  //NEW.............................................................................................................
  //IF CREAR NIVEL CINE
  if(quien=="f_crear_nivelcine"){  var varurl=$(this).attr("action");  var div_resul="capa_formularios";  }
  //IF EDITAR CAPACIDAD EXCEPCIONAL
  if(quien=="f_editar_nivelcine"){  var varurl=$(this).attr("action");  var div_resul="notificacion_E2";  }
  //IF BORRAR VICTIMA CONFLICT
  if(quien=="f_borrar_nivelcine"){  var varurl=$(this).attr("action");  var div_resul="capa_formularios";  }


  if(quien=="f_crear_grado"){  var varurl=$(this).attr("action");  var div_resul="capa_formularios";  }
  //IF EDITAR NIVEL CINE
  if(quien=="f_editar_grado"){  var varurl=$(this).attr("action");  var div_resul="notificacion_E2";  }
  //IF BORRAR NIVEL CINE
  if(quien=="f_borrar_grado"){  var varurl=$(this).attr("action");  var div_resul="capa_formularios";  }

  if(quien=="f_crear_municipio"){  var varurl=$(this).attr("action");  var div_resul="capa_formularios";  }
  //IF EDITAR NIVEL CINE
  if(quien=="f_editar_municipio"){  var varurl=$(this).attr("action");  var div_resul="notificacion_E2";  }
  //IF BORRAR NIVEL CINE
  if(quien=="f_borrar_municipio"){  var varurl=$(this).attr("action");  var div_resul="capa_formularios";  }


  //NEW.............................................................................................................
  //IF CREAR RectorDirector
  if(quien=="f_crear_rectordirector"){  var varurl=$(this).attr("action");  var div_resul="capa_formularios";  }
  //IF Editar RectorDirector
  if(quien=="f_editar_rectordirector"){  var varurl=$(this).attr("action");  var div_resul="notificacion_E2";  }
  //IF BORRAR RectorDirector
  if(quien=="f_borrar_rectordirector"){  var varurl=$(this).attr("action");  var div_resul="capa_formularios";  }


  if(quien=="f_crear_sedesinfra"){  var varurl=$(this).attr("action");  var div_resul="capa_formularios";  }

  if(quien=="f_editar_sedesinfra"){  var varurl=$(this).attr("action");  var div_resul="notificacion_E2";  }

  if(quien=="f_borrar_sedesinfra"){  var varurl=$(this).attr("action");  var div_resul="capa_formularios";  }


  if(quien=="f_crear_sedeslegal"){  var varurl=$(this).attr("action");  var div_resul="capa_formularios";  }

  if(quien=="f_editar_sedeslegal"){  var varurl=$(this).attr("action");  var div_resul="notificacion_E2";  }

  if(quien=="f_borrar_sedeslegal"){  var varurl=$(this).attr("action");  var div_resul="capa_formularios";  }



  if(quien=="f_crear_sedesservicio"){  var varurl=$(this).attr("action");  var div_resul="capa_formularios";  }

  if(quien=="f_crear_riesgo"){  var varurl=$(this).attr("action");  var div_resul="capa_formularios";  }

  if(quien=="f_ingresar_riesgo"){  var varurl=$(this).attr("action");  var div_resul="capa_formularios";  }

  if(quien=="f_ingresar_sedesservicios"){  var varurl=$(this).attr("action");  var div_resul="capa_formularios";  }

  if(quien=="f_editar_sedesservicio"){  var varurl=$(this).attr("action");  var div_resul="notificacion_E2";  }

  if(quien=="f_borrar_sedesservicio"){  var varurl=$(this).attr("action");  var div_resul="capa_formularios";  }

  if(quien=="f_borrar_sedesriesgos"){  var varurl=$(this).attr("action");  var div_resul="capa_formularios";  }

  if(quien=="f_crear_sedesotros"){  var varurl=$(this).attr("action");  var div_resul="capa_formularios";  }

  if(quien=="f_editar_sedesotros"){  var varurl=$(this).attr("action");  var div_resul="notificacion_E2";  }



  if(quien=="f_crear_sedesaulas"){  var varurl=$(this).attr("action");  var div_resul="capa_formularios";  }

  if(quien=="f_editar_sedesaulas"){  var varurl=$(this).attr("action");  var div_resul="notificacion_E2";  }



  if(quien=="f_crear_sedesbaterias"){  var varurl=$(this).attr("action");  var div_resul="capa_formularios";  }

  if(quien=="f_editar_sedesbaterias"){  var varurl=$(this).attr("action");  var div_resul="notificacion_E2";  }

  if(quien=="f_borrar_sedesespacio"){  var varurl=$(this).attr("action");  var div_resul="capa_formularios";  }



  //Origen dispositivo
  if(quien=="f_crear_origen"){  var varurl=$(this).attr("action");  var div_resul="capa_formularios";  }
  if(quien=="f_editar_origen"){  var varurl=$(this).attr("action");  var div_resul="notificacion_E2";  }
  if(quien=="f_borrar_origen"){  var varurl=$(this).attr("action");  var div_resul="capa_formularios";  }
  //Tipo dispositivo
  if(quien=="f_crear_tipodispositivo"){  var varurl=$(this).attr("action");  var div_resul="capa_formularios";  }
  if(quien=="f_editar_tipodispositivo"){  var varurl=$(this).attr("action");  var div_resul="notificacion_E2";  }
  if(quien=="f_borrar_tipodispositivo"){  var varurl=$(this).attr("action");  var div_resul="capa_formularios";  }
  //Proyecto
  if(quien=="f_crear_proyecto"){  var varurl=$(this).attr("action");  var div_resul="capa_formularios";  }
  if(quien=="f_editar_proyecto"){  var varurl=$(this).attr("action");  var div_resul="notificacion_E2";  }
  if(quien=="f_borrar_proyecto"){  var varurl=$(this).attr("action");  var div_resul="capa_formularios";  }
  //Contrato
  if(quien=="f_crear_contrato"){  var varurl=$(this).attr("action");  var div_resul="capa_formularios";  }
  if(quien=="f_editar_contrato"){  var varurl=$(this).attr("action");  var div_resul="notificacion_E2";  }
  if(quien=="f_borrar_contrato"){  var varurl=$(this).attr("action");  var div_resul="capa_formularios";  }
  //Borrado carga dispositivos
  if(quien=="f_borrar_archivos_dispositivos"){  var varurl=$(this).attr("action");  var div_resul="capa_formularios";  }
  if(quien=="f_crear_sedesconectividad"){  var varurl=$(this).attr("action");  var div_resul="capa_formularios";  }

  if(quien=="f_editar_sedesconectividad"){  var varurl=$(this).attr("action");  var div_resul="notificacion_E2";  }

  if(quien=="f_borrar_sedesconectividad"){  var varurl=$(this).attr("action");  var div_resul="capa_formularios";  }

  //Borrado dispositivos
  if(quien=="f_borrar_dispositivo"){  var varurl=$(this).attr("action");  var div_resul="capa_formularios";  }
  //Borrado traslados
  if(quien=="f_borrar_traslado"){  var varurl=$(this).attr("action");  var div_resul="capa_formularios";  }
  //Borrado solicitudes
  if(quien=="f_borrar_solicitud"){  var varurl=$(this).attr("action");  var div_resul="capa_formularios";  }

  if(quien=="f_ingresar_alcalde"){  var varurl=$(this).attr("action");  var div_resul="capa_formularios";  }

  if(quien=="f_ingresar_documentos"){  var varurl=$(this).attr("action");  var div_resul="capa_formularios";  }

  if(quien=="f_crear_sedesescritura"){  var varurl=$(this).attr("action");  var div_resul="capa_formularios";  }

  if(quien=="f_editar_sedesescritura"){  var varurl=$(this).attr("action");  var div_resul="notificacion_E2";  }

  if(quien=="f_borrar_sedesescritura"){  var varurl=$(this).attr("action");  var div_resul="capa_formularios";  }

  if(quien=="f_crear_modal"){  var varurl=$(this).attr("action");  var div_resul="capa_formularios";  }


  $("#"+div_resul+"").html( $("#cargador_empresa").html());
  $.ajax({
    // la URL para la petición
    url : varurl,
    data : formu.serialize(),
    type : 'POST',
    dataType : 'html',

    success : function(resul) {
      $("#"+div_resul+"").html(resul);

    },
    error : function(xhr, status) {
      $("#"+div_resul+"").html('Ha ocurrido un error, revise su conexión e intentelo nuevamente');
    }

  });


})



function asignar_rol(idusu){
  var idrol=$("#rol1").val();
  var urlraiz=$("#url_raiz_proyecto").val();
  $("#zona_etiquetas_roles").html($("#cargador_empresa").html());
  var miurl=urlraiz+"/asignar_rol/"+idusu+"/"+idrol+"";

  $.ajax({
    url: miurl
  }).done( function(resul)
  {
    var etiquetas="";
    var roles=$.parseJSON(resul);
    $.each(roles,function(index, value) {
      etiquetas+= '<span class="label label-warning">'+value+'</span> ';
    })

    $("#zona_etiquetas_roles").html(etiquetas);

  }).fail( function()
  {
    $("#zona_etiquetas_roles").html('<span style="color:red;">...Error: Aun no ha agregado roles o revise su conexión...</span>');
  }) ;

}


function quitar_rol(idusu){
  var idrol=$("#rol2").val();
  var urlraiz=$("#url_raiz_proyecto").val();
  $("#zona_etiquetas_roles").html($("#cargador_empresa").html());
  var miurl=urlraiz+"/quitar_rol/"+idusu+"/"+idrol+"";

  $.ajax({
    url: miurl
  }).done( function(resul)
  {
    var etiquetas="";
    var roles=$.parseJSON(resul);
    $.each(roles,function(index, value) {
      etiquetas+= '<span class="label label-warning" style="margin-left:10px;" >'+value+'</span> ';
    })

    $("#zona_etiquetas_roles").html(etiquetas);

  }).fail( function()
  {
    $("#zona_etiquetas_roles").html('<span style="color:red;">...Error: Aun no ha agregado roles  o revise su conexión...</span>');
  }) ;

}

function borrado_documento_estructura(idusu){

  var urlraiz=$("#url_raiz_proyecto").val();
  $("#capa_modal").show();
  $("#capa_formularios").show();
  var screenTop = $(document).scrollTop();
  $("#capa_formularios").css('top', screenTop);
  $("#capa_formularios").html($("#cargador_empresa").html());
  var miurl=urlraiz+"/form_borrado_documento_estructura/"+idusu+"";


  $.ajax({
    url: miurl
  }).done( function(resul)
  {
    $("#capa_formularios").html(resul);

  }).fail( function()
  {
    $("#capa_formularios").html('<span>...Ha ocurrido un error, revise su conexión y vuelva a intentarlo...</span>');
  }) ;


}


function borrado_usuario(idusu){

  var urlraiz=$("#url_raiz_proyecto").val();
  $("#capa_modal").show();
  $("#capa_formularios").show();
  var screenTop = $(document).scrollTop();
  $("#capa_formularios").css('top', screenTop);
  $("#capa_formularios").html($("#cargador_empresa").html());
  var miurl=urlraiz+"/form_borrado_usuario/"+idusu+"";


  $.ajax({
    url: miurl
  }).done( function(resul)
  {
    $("#capa_formularios").html(resul);

  }).fail( function()
  {
    $("#capa_formularios").html('<span>...Ha ocurrido un error, revise su conexión y vuelva a intentarlo...</span>');
  }) ;


}



function borrado_departamento(iddept){

  var urlraiz=$("#url_raiz_proyecto").val();
  $("#capa_modal").show();
  $("#capa_formularios").show();
  var screenTop = $(document).scrollTop();
  $("#capa_formularios").css('top', screenTop);
  $("#capa_formularios").html($("#cargador_empresa").html());
  var miurl=urlraiz+"/form_borrado_departamento/"+iddept+"";


  $.ajax({
    url: miurl
  }).done( function(resul)
  {
    $("#capa_formularios").html(resul);

  }).fail( function()
  {
    $("#capa_formularios").html('<span>...Ha ocurrido un error, revise su conexión y vuelva a intentarlo...</span>');
  }) ;


}

function borrado_subregion(iddept){

  var urlraiz=$("#url_raiz_proyecto").val();
  $("#capa_modal").show();
  $("#capa_formularios").show();
  var screenTop = $(document).scrollTop();
  $("#capa_formularios").css('top', screenTop);
  $("#capa_formularios").html($("#cargador_empresa").html());
  var miurl=urlraiz+"/form_borrado_subregion/"+iddept+"";


  $.ajax({
    url: miurl
  }).done( function(resul)
  {
    $("#capa_formularios").html(resul);

  }).fail( function()
  {
    $("#capa_formularios").html('<span>...Ha ocurrido un error, revise su conexión y vuelva a intentarlo...</span>');
  }) ;


}

function borrado_municipio(iddept){

  var urlraiz=$("#url_raiz_proyecto").val();
  $("#capa_modal").show();
  $("#capa_formularios").show();
  var screenTop = $(document).scrollTop();
  $("#capa_formularios").css('top', screenTop);
  $("#capa_formularios").html($("#cargador_empresa").html());
  var miurl=urlraiz+"/form_borrado_municipio/"+iddept+"";


  $.ajax({
    url: miurl
  }).done( function(resul)
  {
    $("#capa_formularios").html(resul);

  }).fail( function()
  {
    $("#capa_formularios").html('<span>...Ha ocurrido un error, revise su conexión y vuelva a intentarlo...</span>');
  }) ;


}





function borrado_grado(iddept){

  var urlraiz=$("#url_raiz_proyecto").val();
  $("#capa_modal").show();
  $("#capa_formularios").show();
  var screenTop = $(document).scrollTop();
  $("#capa_formularios").css('top', screenTop);
  $("#capa_formularios").html($("#cargador_empresa").html());
  var miurl=urlraiz+"/form_borrado_grado/"+iddept+"";


  $.ajax({
    url: miurl
  }).done( function(resul)
  {
    $("#capa_formularios").html(resul);

  }).fail( function()
  {
    $("#capa_formularios").html('<span>...Ha ocurrido un error, revise su conexión y vuelva a intentarlo...</span>');
  }) ;


}

function borrado_tipodocumento(iddept){

  var urlraiz=$("#url_raiz_proyecto").val();
  $("#capa_modal").show();
  $("#capa_formularios").show();
  var screenTop = $(document).scrollTop();
  $("#capa_formularios").css('top', screenTop);
  $("#capa_formularios").html($("#cargador_empresa").html());
  var miurl=urlraiz+"/form_borrado_tipodocumento/"+iddept+"";


  $.ajax({
    url: miurl
  }).done( function(resul)
  {
    $("#capa_formularios").html(resul);

  }).fail( function()
  {
    $("#capa_formularios").html('<span>...Ha ocurrido un error, revise su conexión y vuelva a intentarlo...</span>');
  }) ;


}


function borrado_comiteJunta(iddept){

  var urlraiz=$("#url_raiz_proyecto").val();
  $("#capa_modal").show();
  $("#capa_formularios").show();
  var screenTop = $(document).scrollTop();
  $("#capa_formularios").css('top', screenTop);
  $("#capa_formularios").html($("#cargador_empresa").html());
  var miurl=urlraiz+"/form_borrado_comiteJunta/"+iddept+"";


  $.ajax({
    url: miurl
  }).done( function(resul)
  {
    $("#capa_formularios").html(resul);

  }).fail( function()
  {
    $("#capa_formularios").html('<span>...Ha ocurrido un error, revise su conexión y vuelva a intentarlo...</span>');
  }) ;


}


function borrado_conalunmant(iddept){

  var urlraiz=$("#url_raiz_proyecto").val();
  $("#capa_modal").show();
  $("#capa_formularios").show();
  var screenTop = $(document).scrollTop();
  $("#capa_formularios").css('top', screenTop);
  $("#capa_formularios").html($("#cargador_empresa").html());
  var miurl=urlraiz+"/form_borrado_conalunmant/"+iddept+"";


  $.ajax({
    url: miurl
  }).done( function(resul)
  {
    $("#capa_formularios").html(resul);

  }).fail( function()
  {
    $("#capa_formularios").html('<span>...Ha ocurrido un error, revise su conexión y vuelva a intentarlo...</span>');
  }) ;


}

function borrado_sitacaant(iddept){

  var urlraiz=$("#url_raiz_proyecto").val();
  $("#capa_modal").show();
  $("#capa_formularios").show();
  var screenTop = $(document).scrollTop();
  $("#capa_formularios").css('top', screenTop);
  $("#capa_formularios").html($("#cargador_empresa").html());
  var miurl=urlraiz+"/form_borrado_sitacaant/"+iddept+"";


  $.ajax({
    url: miurl
  }).done( function(resul)
  {
    $("#capa_formularios").html(resul);

  }).fail( function()
  {
    $("#capa_formularios").html('<span>...Ha ocurrido un error, revise su conexión y vuelva a intentarlo...</span>');
  }) ;


}

function borrado_zona(iddept){

  var urlraiz=$("#url_raiz_proyecto").val();
  $("#capa_modal").show();
  $("#capa_formularios").show();
  var screenTop = $(document).scrollTop();
  $("#capa_formularios").css('top', screenTop);
  $("#capa_formularios").html($("#cargador_empresa").html());
  var miurl=urlraiz+"/form_borrado_zona/"+iddept+"";


  $.ajax({
    url: miurl
  }).done( function(resul)
  {
    $("#capa_formularios").html(resul);

  }).fail( function()
  {
    $("#capa_formularios").html('<span>...Ha ocurrido un error, revise su conexión y vuelva a intentarlo...</span>');
  }) ;


}
function borrado_tipodiscapacidad(iddept){

  var urlraiz=$("#url_raiz_proyecto").val();
  $("#capa_modal").show();
  $("#capa_formularios").show();
  var screenTop = $(document).scrollTop();
  $("#capa_formularios").css('top', screenTop);
  $("#capa_formularios").html($("#cargador_empresa").html());
  var miurl=urlraiz+"/form_borrado_tipodiscapacidad/"+iddept+"";


  $.ajax({
    url: miurl
  }).done( function(resul)
  {
    $("#capa_formularios").html(resul);

  }).fail( function()
  {
    $("#capa_formularios").html('<span>...Ha ocurrido un error, revise su conexión y vuelva a intentarlo...</span>');
  }) ;


}

function borrado_metodologia(iddept){

  var urlraiz=$("#url_raiz_proyecto").val();
  $("#capa_modal").show();
  $("#capa_formularios").show();
  var screenTop = $(document).scrollTop();
  $("#capa_formularios").css('top', screenTop);
  $("#capa_formularios").html($("#cargador_empresa").html());
  var miurl=urlraiz+"/form_borrado_metodologia/"+iddept+"";


  $.ajax({
    url: miurl
  }).done( function(resul)
  {
    $("#capa_formularios").html(resul);

  }).fail( function()
  {
    $("#capa_formularios").html('<span>...Ha ocurrido un error, revise su conexión y vuelva a intentarlo...</span>');
  }) ;


}

function borrado_estrato(iddept){

  var urlraiz=$("#url_raiz_proyecto").val();
  $("#capa_modal").show();
  $("#capa_formularios").show();
  var screenTop = $(document).scrollTop();
  $("#capa_formularios").css('top', screenTop);
  $("#capa_formularios").html($("#cargador_empresa").html());
  var miurl=urlraiz+"/form_borrado_estrato/"+iddept+"";


  $.ajax({
    url: miurl
  }).done( function(resul)
  {
    $("#capa_formularios").html(resul);

  }).fail( function()
  {
    $("#capa_formularios").html('<span>...Ha ocurrido un error, revise su conexión y vuelva a intentarlo...</span>');
  }) ;


}

function borrado_resguardo(iddept){

  var urlraiz=$("#url_raiz_proyecto").val();
  $("#capa_modal").show();
  $("#capa_formularios").show();
  var screenTop = $(document).scrollTop();
  $("#capa_formularios").css('top', screenTop);
  $("#capa_formularios").html($("#cargador_empresa").html());
  var miurl=urlraiz+"/form_borrado_resguardo/"+iddept+"";


  $.ajax({
    url: miurl
  }).done( function(resul)
  {
    $("#capa_formularios").html(resul);

  }).fail( function()
  {
    $("#capa_formularios").html('<span>...Ha ocurrido un error, revise su conexión y vuelva a intentarlo...</span>');
  }) ;


}
/*-------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------*/
function filtroSubregionMunicipios(){
  var subregion = $("#selSubregion").val();
  $( "#selSede" ).empty();
  $( "#selEstablecimiento" ).empty();
  $( "#selMunicipio" ).empty();
  if(subregion == ""){
    return;
  }

  $.ajaxSetup({
    headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
  });

  $.ajax({
    type: "POST",
    url: "filtraMunicipios",
    data: {"subregion": subregion}
  }).done( function(info ){
    $('#selMunicipio').empty();
    $( "#selMunicipio" ).append( "<option value=''>Seleccione uno</option>");
    var json_info = JSON.parse( info );
    $.each( json_info, function( key, value ) {
      var codigo = value.CodigoDaneMunicipio;
      var name =  value.NombreMunicipio;
      $( "#selMunicipio" ).append( "<option value='"+codigo+"'>"+name+"</option>");
    });


  });


}
function encode_utf8(s) {
  return unescape(encodeURIComponent(s));
}
function decode_utf8(s) {
  return decodeURIComponent(escape(s));
}

function filtroMunicipioEstablecimiento(){
  var municipio = $("#selMunicipio").val();
  $( "#selSede" ).empty();
  $( "#selEstablecimiento" ).empty();
  if(municipio == ""){
    return;
  }

  $.ajaxSetup({
    headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
  });

  $.ajax({
    type: "POST",
    url: "filtraMunicipiosEstablecimientos",
    data: {"municipio": municipio}
  }).done( function(info ){
    $( "#selEstablecimiento" ).append( "<option value=''>Elige uno</option>");
    var json_info = JSON.parse( info );
    $.each( json_info, function( key, value ) {
      var codigo = value.codigo_establecimiento;
      var name =  value.nombre_establecimiento;
      $( "#selEstablecimiento" ).append( "<option value='"+codigo+"'>"+name+"</option>");
    });


  });


}




$(document).ready(function () {

  $("#selEstablecimiento").on("change", function(e){
    $( "#selSede" ).empty();

    var establecimiento = $(this).val();

    if(establecimiento == ""){
      return;
    }

    $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
    });
    var urlraiz=$("#url_raiz_proyecto").val();
    var miurl=urlraiz+"/filtSede";

    $.ajax({
      type: "POST",
      url: miurl,
      data: {"selEstablecimiento": establecimiento}
    }).done( function(info ){
      // console.log(info);
      var json_info = JSON.parse( info );
      $( "#selSede" ).append( "<option value=''>Elige una</option>");
      $.each( json_info, function( key, value ) {
        var codigo = value.codigo_sede;
        var name = value.nombre_sede;
        $( "#selSede" ).append( "<option value='"+codigo+"'>"+name+"</option>");
      });
    });
  });

  $("#selSede").on("change", function(e){
    var sede = $("#selSede").val();

    if(sede == ""){
      limpiaHiddenEspaciosSedes();
      return;
    }

    $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
    });
    var urlraiz=$("#url_raiz_proyecto").val();
    var miurl=urlraiz+"/EspaciosSede";

    $.ajax({
      type: "POST",
      url: miurl,
      data: {"selSede": sede}
    }).done( function(info){
      if(info =="[]"){
        limpiaHiddenEspaciosSedes();
        return;
      }

      var json_info = JSON.parse( info );
      $("#tableAulasPreescolar").val(json_info[0].AulasPreescolar);
      $("#tableAulasPrimaria").val(json_info[0].AulasPrimaria);
      $("#tableAulasSecundaria").val(json_info[0].AulasSecundaria);
      $("#tableAulasEspaciales").val(json_info[0].AulasEspaciales);
      $("#tableBiblioteca").val(json_info[0].Biblioteca);
      $("#tableAulasSistemas").val(json_info[0].AulasSistemas);
      $("#tableAulasBilinguismo").val(json_info[0].AulasBilinguismo);
      $("#tableLaboratorio").val(json_info[0].Laboratorio);
      $("#tableAulasTalleres").val(json_info[0].AulasTalleres);
      $("#tableAulasMultiples").val(json_info[0].AulasMultiples);
      $("#tableCocinas").val(json_info[0].Cocinas);
      $("#tableComedores").val(json_info[0].Comedores);
      $("#tableSantiriosHombres").val(json_info[0].SantiriosHombres);
      $("#tableSanitariosMujeres").val(json_info[0].SanitariosMujeres);
      $("#tableLavamanosHombres").val(json_info[0].LavamanosHombres);
      $("#tableLavamanosMujeres").val(json_info[0].LavamanosMujeres);
      $("#tableBReducida").val(json_info[0].SanitariosReducida);
      $("#tableOrinales").val(json_info[0].Orinales);
      $("#tableVivienda").val(json_info[0].Vivienda);
      $("#tableCanchas").val(json_info[0].Canchas);
      $("#tablePlacasMulti").val(json_info[0].PlacasMulti);
      $("#tableJuegosInfantiles").val(json_info[0].JuegosInfantiles);
    });
  });

  $("#anadirEfi").on("click", function(e){
    var agregarCantidad = $("#CantidadEfi").val();

    if(agregarCantidad<=0 || agregarCantidad>99){
      alert("Sólo se permiten añadir espacios con cantidades entre 1 y 99.");
      $("#CantidadEfi").focus();
      return;
    }

    var sede = $("#selSede").val();
    if(sede ==""){
      alert("Debe seleccionar primero la sede educativa en la que realizará el diagnóstico de necesidades.");
      return;
    }

    $("#info").hide(300);
    $("#infoTipoSolicitud").hide(300);
    $("#añadir_requerimiento").hide(300);

    var item = {};
    item.espacio = $('#EFisicoe').val();
    item.espacioT = $('#EFisicoe  option:selected').text();
    item.cantidad = $('#CantidadEfi').val();
    item.estado = $('#EstadoActualefi').val();
    item.estadoT = $('#EstadoActualefi  option:selected').text();
    item.tiposolicitud = $('#TipoSolicitudefi').val();
    item.tiposolicitudT = $('#TipoSolicitudefi  option:selected').text();
    if(item.cantidad == ""){
      item.cantidad = 1;
    }


    if(item.tiposolicitud == ""){
      $("#infoTipoSolicitud").show(500);
      return;
    }

    var agregar=true;
    var esp = document.getElementsByName("rEspacio[]");
    var est = document.getElementsByName("rEstado[]");
    var cont=0;
    for(var i=0; i<esp.length;i++){
      if(esp[i].value == item.espacio){
        if(item.estado == est[i].value){
          $("#info").show(500);
          return;
        }
        cont++;
      }

      if(cont == 3){
        agregar = false;
        break;
      }
    }
    if(agregar){
      var validaCantidadEspacio = validarEspaciosCantidad(item.espacio, item.cantidad);
      if(item.espacio != 23 && item.tiposolicitud !=4){
        if(validaCantidadEspacio){
          muestraMensajeExceder(item.espacioT, item.cantidad, item.estadoT);
          return;
        }
      }

      var reqvarios = "";
      var rcount = 0;
      var values = [];
      $("#reqVarios input[type='checkbox']").each(function() {
        if($(this).prop('checked')) {
          $("#añadir_requerimiento").show(500);
          values.push($(this).attr('value'));
          if(rcount>=1){
            reqvarios = reqvarios + " - ";
          }
          rcount++;
          reqvarios = reqvarios + $(this).attr('title');
        }else{
          $("#añadir_requerimiento").show(500);
          values.push(false);
        }

      });

      item.reqvarios = reqvarios;
      item.cubierta = values.pop();
      item.cerramiento = values.pop();
      item.puertas = values.pop();
      item.paredes = values.pop();
      item.pisos = values.pop();
      item.alcantarillado = values.pop();
      item.acueducto = values.pop();
      item.electricidad = values.pop();

      agregaEspacioSolicitud(item);
      $("#reqVarios input[type='checkbox']").prop("checked",false);
      $("#reqVarios input[type='checkbox']").removeAttr("disabled");
      item.estado = $('#EstadoActualefi').val(1);
      item.tiposolicitud = $('#TipoSolicitudefi').val('');

    }
    $('#CantidadEfi').val(1);
  });
});

function limpiaHiddenEspaciosSedes(){
  $("#ControlEspaciosCantidad input[type='hidden']").attr("value","0");
}

function validarEspaciosCantidad(espacio, cantidad){
  switch (espacio) {
    case "1":
    var registro = $("#tableAulasPreescolar").val();
    break;
    case "2":
    var registro = $("#tableAulasPrimaria").val();
    break;
    case "3":
    var registro = $("#tableAulasSecundaria").val();
    break;
    case "4":
    var registro = $("#tableAulasEspaciales").val();
    break;
    case "5":
    var registro = $("#tableBiblioteca").val();
    break;
    case "6":
    var registro = $("#tableAulasSistemas").val();
    break;
    case "7":
    var registro = $("#tableAulasBilinguismo").val();
    break;
    case "8":
    var registro = $("#tableLaboratorio").val();
    break;
    case "9":
    var registro = $("#tableAulasTalleres").val();
    break;
    case "10":
    var registro = $("#tableAulasMultiples").val();
    break;
    case "11":
    var registro = $("#tableCocinas").val();
    break;
    case "12":
    var registro = $("#tableComedores").val();
    break;
    case "13":
    var registro = $("#tableSanitariosMujeres").val();
    break;
    case "14":
    var registro = $("#tableLavamanosHombres").val();
    break;
    case "15":
    var registro = $("#tableBReducida").val();
    break;
    case "16":
    var registro = $("#tableLavamanosMujeres").val();
    break;
    case "17":
    var registro = $("#tableLavamanosHombres").val();
    break;
    case "18":
    var registro = $("#tableOrinales").val();
    break;
    case "19":
    var registro = $("#tableVivienda").val();
    break;
    case "20":
    var registro = $("#tableCanchas").val();
    break;
    case "21":
    var registro = $("#tablePlacasMulti").val();
    break;
    case "22":
    var registro = $("#tableJuegosInfantiles").val();
    break;
    default:
    break;
  }

  if(cantidad > registro){
    $("#tableCantidadRegistrada").val(registro);
    return true;
  }

  return false;
}

function muestraMensajeExceder(espacio, cantidad, estado){
  alert("Las "+cantidad+" "+espacio+", que desea reportar, en estado actual: "+estado+", exceden la cantidad de "+$("#tableCantidadRegistrada").val() +" "+espacio+" registrados(as) actualmente para la sede seleccionada.");
}


function agregaEspacioSolicitud(item){


  $("#tdReqEspacios").append('<tr role="row" class="odd"><td>'+item.espacioT+
  '<input type="hidden" name="rEspacio[]" value="'+item.espacio+'"></td><td>'+item.cantidad+
  '<input type="hidden" name="rCantidad[]" value="'+item.cantidad+'"></td><td>'+item.estadoT+
  '<input type="hidden" name="rEstado[]" value="'+item.estado+'"></td><td>'+item.tiposolicitudT+
  '<input type="hidden" name="rTipo[]" value="'+item.tiposolicitud+
  '"></td><td>'+item.reqvarios+
  '<input type="hidden" name="rElectricidad[]" value="'+item.electricidad+'"/>'+
  '<input type="hidden" name="rAcueducto[]" value="'+item.acueducto+'"/>'+
  '<input type="hidden" name="rAlcantarillado[]" value="'+item.alcantarillado+'"/>'+
  '<input type="hidden" name="rPisos[]" value="'+item.pisos+'"/>'+
  '<input type="hidden" name="rParedes[]" value="'+item.paredes+'"/>'+
  '<input type="hidden" name="rPuertas[]" value="'+item.puertas+'"/>'+
  '<input type="hidden" name="rCerramiento[]" value="'+item.cerramiento+'"/>'+
  '<input type="hidden" name="rCubierta[]" value="'+item.cubierta+'"/>'+
  '</td><td><button type="button" class="btn  btn-danger btn-xs eliminaEspacio"><i class="fa fa-fw fa-trash" title="Borrar"></i></button></td></tr>');
}

$(function () {
  $(document).on('click', '.eliminaEspacio', function (event) {
    event.preventDefault();
    $(this).closest('tr').remove();
  });
});

function elimnaSolicitudIF(id){

  var urlraiz=$("#url_raiz_proyecto").val();
  $("#capa_modal").show();
  $("#capa_formularios").show();
  var screenTop = $(document).scrollTop();
  $("#capa_formularios").css('top', screenTop);
  $("#capa_formularios").html($("#cargador_empresa").html());
  var miurl=urlraiz+"/elimnaSolicitudIF/"+id;


  $.ajax({
    url: miurl
  }).done( function(resul)
  {
    $("#capa_formularios").html(resul);
  }).fail( function()
  {
    $("#capa_formularios").html('<span>...Ha ocurrido un error, revise su conexión y vuelva a intentarlo...</span>');
  }) ;


}
function eliminaSolicitud(id){

  var urlraiz=$("#url_raiz_proyecto").val();
  $("#capa_modal").show();
  $("#capa_formularios").show();
  var screenTop = $(document).scrollTop();
  $("#capa_formularios").css('top', screenTop);
  $("#capa_formularios").html($("#cargador_empresa").html());
  var miurl=urlraiz+"/eliminaSolicitud/"+id;


  $.ajax({
    url: miurl
  }).done( function(resul)
  {
    $("#capa_formularios").html(resul);
  }).fail( function()
  {
    $("#capa_formularios").html('<span>...Ha ocurrido un error, revise su conexión y vuelva a intentarlo...</span>');
  }) ;


}

//Funcion que hace desaparecer el div transcurridos 3000 milisegundos!
$(document).ready(function() {
  setTimeout(function() {
    // Declaramos la capa mediante una clase para ocultarlo
    $(".msjInfo").fadeOut(6000);
  },20000);

  $("#clickme").click(function() {
    $(".msjInfo").toggle("slow", function() {

    });
  });
});

$(document).ready(function() {
  $("#EstadoActualefi").on("change",function(event){
    var valEstado = $('#EstadoActualefi').val()
    if(valEstado == 3){
      $("#reqVarios input[type='checkbox']").attr("disabled","true");
      $("#reqVarios input[type='checkbox']").prop("checked",false);
      $("#TipoSolicitudefi").val(4);
    }else{
      $("#reqVarios input[type='checkbox']").removeAttr("disabled");
      if($("#TipoSolicitudefi").val() == 4){
        $("#TipoSolicitudefi").val('');
      }
    }
  });
});

$(document).ready(function(){
  $("#TipoSolicitudefi").on("change",function(event){
    var vaTipo = $('#TipoSolicitudefi').val()
    if(vaTipo == 4){
      $("#reqVarios input[type='checkbox']").attr("disabled","true");
      $("#reqVarios input[type='checkbox']").prop("checked",false);
      $("#EstadoActualefi").val(3);
    }else{
      $("#reqVarios input[type='checkbox']").removeAttr("disabled");
      if( $('#EstadoActualefi').val() == 3){
        $("#EstadoActualefi").val(1);
      }
    }
  });
});

$(document).ready(function(){
  _.delay(function() { $(".msjNotificacion").hide(500); }, 5000);
});


$(document).ready(function(){
  var existeRespuesta = $("#exisresponse").val();
  var pathname = window.location.pathname;
  // /requerimientosIFisica/1/view
  var urlDividida = pathname.split("/");
  if(!(urlDividida[1] == "requerimientosIFisica" && urlDividida[3] == "view")){
    return;
  }

  $("#urlActual").val(pathname);

  if(existeRespuesta == 0 || existeRespuesta ==""){
    $("#Respuesta").hide();
  }else{

    var RespuestaId = $("#RespuestaId").val();
    $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
    });
    var urlraiz=$("#url_raiz_proyecto").val();
    var miurl=urlraiz+"/vistoRequerimiento";

    $.ajax({
      type: "POST",
      url: miurl,
      data: {"RespuestaId": RespuestaId}
    });
  }


  $("#btnRegRespSolicitud").on("click", function(e){
    $("#Respuesta").show(100);
    $("#RespuestaSolicitudIF").removeAttr("readonly");
    $("#btnConfirmEditRespuestaSolicitud").show(100);
    $("#btnCancelaRespuestSIF").show(100);
    $("#tipoForm").val(1);
  });

  $("#btnEdtRespSolicitud").on("click", function(e){
    $("#RespuestaSolicitudIF").removeAttr("readonly");
    $("#btnConfirmEditRespuestaSolicitud").show(100);
    $("#btnCancelaRespuestSIF").show(100);
    $("#tipoForm").val(2);
  });

  $("#btnCancelaRespuestSIF").on("click", function(e){
    var tipoForm = $("#tipoForm").val();

    if(tipoForm == 1){
      $("#Respuesta").hide(100);
    }else{
      $("#RespuestaSolicitud").attr("readonly", "readonly");
    }
    $("#btnConfirmEditRespuestaSolicitud").hide(100);
    $("#btnCancelaRespuestSIF").hide(100);
  });
});

$(document).ready(function(){

  $('#formRegReqSolIF').bind('submit', function (event) {
    var anexo = $("#anexoEvidencia").val();

    if(anexo != ""){
      var extensiones = anexo.substring(anexo.lastIndexOf("."));

      if(extensiones != ".jpg" && extensiones != ".JPG" && extensiones != ".pdf" && extensiones != ".PDF" && extensiones != ".png" && extensiones != ".PNG" && extensiones != ".jepg" && extensiones != ".JEPG" && extensiones != ".gif" && extensiones != ".GIF")
      {
        alert("El archivo de tipo " + extensiones + " no es válido");
        return false;
      }
    }

  });

});
/*-------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------*/
//Infraestructura Tecnológica
//
$(document).ready(function(){
  var existeRespuesta = $("#exisresponsee").val();
  var pathname = window.location.pathname;
  // /requerimientosIFisica/1/view
  var urlDividida = pathname.split("/");
  if(!(urlDividida[1] == "requerimientos" && urlDividida[3] == "view")){
    return;
  }

  $("#urlActuall").val(pathname);

  if(existeRespuesta == 0 || existeRespuesta ==""){
    $("#Respuestaa").hide();
  }else{

    var RespuestaId = $("#RespuestaId").val();
        $.ajaxSetup({
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
        });
        var urlraiz=$("#url_raiz_proyecto").val();
        var miurl=urlraiz+"/vistoRequerimientoo";

      $.ajax({
        type: "POST",
        url: miurl,
        data: {"RespuestaId": RespuestaId}
      });
  }

$("#btnRegRespSolicitudt").on("click", function(e){
  $("#Respuestaa").show(100);
  $("#RespuestaSolicitudd").removeAttr("readonly");
  $("#btnConfirmEditRespuestaSolicitudd").show(100);
  $("#btnCancelaRespuestt").show(100);
  $("#tipoFormn").val(1);
});

$("#btnEdtRespSolicitudt").on("click", function(e){
  $("#RespuestaSolicitudd").removeAttr("readonly");
  $("#btnConfirmEditRespuestaSolicitudd").show(100);
  $("#btnCancelaRespuestt").show(100);
  $("#tipoFormn").val(2);
});

$("#btnCancelaRespuestt").on("click", function(e){
  var tipoFormn = $("#tipoFormn").val();

  if(tipoFormn == 1){
    $("#Respuestaa").hide(100);
  }else{
    $("#RespuestaSolicitudd").attr("readonly", "readonly");
  }
  $("#btnConfirmEditRespuestaSolicitudd").hide(100);
  $("#btnCancelaRespuestt").hide(100);
});
});
//


function borrado_jornada(iddept){

  var urlraiz=$("#url_raiz_proyecto").val();
  $("#capa_modal").show();
  $("#capa_formularios").show();
  var screenTop = $(document).scrollTop();
  $("#capa_formularios").css('top', screenTop);
  $("#capa_formularios").html($("#cargador_empresa").html());
  var miurl=urlraiz+"/form_borrado_jornada/"+iddept+"";


  $.ajax({
    url: miurl
  }).done( function(resul)
  {
    $("#capa_formularios").html(resul);

  }).fail( function()
  {
    $("#capa_formularios").html('<span>...Ha ocurrido un error, revise su conexión y vuelva a intentarlo...</span>');
  }) ;


}

function borrado_victimaconflicto(iddept){

  var urlraiz=$("#url_raiz_proyecto").val();
  $("#capa_modal").show();
  $("#capa_formularios").show();
  var screenTop = $(document).scrollTop();
  $("#capa_formularios").css('top', screenTop);
  $("#capa_formularios").html($("#cargador_empresa").html());
  var miurl=urlraiz+"/form_borrado_victimaconflicto/"+iddept+"";


  $.ajax({
    url: miurl
  }).done( function(resul)
  {
    $("#capa_formularios").html(resul);

  }).fail( function()
  {
    $("#capa_formularios").html('<span>...Ha ocurrido un error, revise su conexión y vuelva a intentarlo...</span>');
  }) ;

}

//BORRADO CAPACIDAD EXCEPCIONAL
function borrado_capacidadexcepcional(iddept){

  var urlraiz=$("#url_raiz_proyecto").val();
  $("#capa_modal").show();
  $("#capa_formularios").show();
  var screenTop = $(document).scrollTop();
  $("#capa_formularios").css('top', screenTop);
  $("#capa_formularios").html($("#cargador_empresa").html());
  var miurl=urlraiz+"/form_borrado_capacidadexcepcional/"+iddept+"";


  $.ajax({
    url: miurl
  }).done( function(resul)
  {
    $("#capa_formularios").html(resul);

  }).fail( function()
  {
    $("#capa_formularios").html('<span>...Ha ocurrido un error, revise su conexión y vuelva a intentarlo...</span>');
  }) ;
}

//BORRADO CAPACIDAD ETNIA
function borrado_etnia(iddept){

  var urlraiz=$("#url_raiz_proyecto").val();
  $("#capa_modal").show();
  $("#capa_formularios").show();
  var screenTop = $(document).scrollTop();
  $("#capa_formularios").css('top', screenTop);
  $("#capa_formularios").html($("#cargador_empresa").html());
  var miurl=urlraiz+"/form_borrado_etnia/"+iddept+"";


  $.ajax({
    url: miurl
  }).done( function(resul)
  {
    $("#capa_formularios").html(resul);

  }).fail( function()
  {
    $("#capa_formularios").html('<span>...Ha ocurrido un error, revise su conexión y vuelva a intentarlo...</span>');
  }) ;
}

//BORRADO NIVEL
function borrado_nivel(iddept){

  var urlraiz=$("#url_raiz_proyecto").val();
  $("#capa_modal").show();
  $("#capa_formularios").show();
  var screenTop = $(document).scrollTop();
  $("#capa_formularios").css('top', screenTop);
  $("#capa_formularios").html($("#cargador_empresa").html());
  var miurl=urlraiz+"/form_borrado_nivel/"+iddept+"";


  $.ajax({
    url: miurl
  }).done( function(resul)
  {
    $("#capa_formularios").html(resul);

  }).fail( function()
  {
    $("#capa_formularios").html('<span>...Ha ocurrido un error, revise su conexión y vuelva a intentarlo...</span>');
  }) ;
}

//BORRADO NIVEL MEDIA TOTAL
function borrado_nivelmediatotal(iddept){

  var urlraiz=$("#url_raiz_proyecto").val();
  $("#capa_modal").show();
  $("#capa_formularios").show();
  var screenTop = $(document).scrollTop();
  $("#capa_formularios").css('top', screenTop);
  $("#capa_formularios").html($("#cargador_empresa").html());
  var miurl=urlraiz+"/form_borrado_nivelmediatotal/"+iddept+"";


  $.ajax({
    url: miurl
  }).done( function(resul)
  {
    $("#capa_formularios").html(resul);

  }).fail( function()
  {
    $("#capa_formularios").html('<span>...Ha ocurrido un error, revise su conexión y vuelva a intentarlo...</span>');
  }) ;
}


function borrado_nivelcine(iddept){

  var urlraiz=$("#url_raiz_proyecto").val();
  $("#capa_modal").show();
  $("#capa_formularios").show();
  var screenTop = $(document).scrollTop();
  $("#capa_formularios").css('top', screenTop);
  $("#capa_formularios").html($("#cargador_empresa").html());
  var miurl=urlraiz+"/form_borrado_nivelcine/"+iddept+"";


  $.ajax({
    url: miurl
  }).done( function(resul)
  {
    $("#capa_formularios").html(resul);

  }).fail( function()
  {
    $("#capa_formularios").html('<span>...Ha ocurrido un error, revise su conexión y vuelva a intentarlo...</span>');
  }) ;
}



function borrado_rectordirector(iddept){

  var urlraiz=$("#url_raiz_proyecto").val();
  $("#capa_modal").show();
  $("#capa_formularios").show();
  var screenTop = $(document).scrollTop();
  $("#capa_formularios").css('top', screenTop);
  $("#capa_formularios").html($("#cargador_empresa").html());
  var miurl=urlraiz+"/form_borrado_rectordirector/"+iddept+"";


  $.ajax({
    url: miurl
  }).done( function(resul)
  {
    $("#capa_formularios").html(resul);

  }).fail( function()
  {
    $("#capa_formularios").html('<span>...Ha ocurrido un error, revise su conexión y vuelva a intentarlo...</span>');
  }) ;
}





function borrar_permiso(idrol,idper){

  var urlraiz=$("#url_raiz_proyecto").val();
  var miurl=urlraiz+"/quitar_permiso/"+idrol+"/"+idper+"";
  $("#filaP_"+idper+"").html($("#cargador_empresa").html() );
  $.ajax({
    url: miurl
  }).done( function(resul)
  {
    $("#filaP_"+idper+"").hide();

  }).fail( function()
  {
    alert("No se borro correctamente, intentalo nuevamente o revisa tu conexión");
  }) ;



}



function borrar_rol(idrol){

  var urlraiz=$("#url_raiz_proyecto").val();
  var miurl=urlraiz+"/borrar_rol/"+idrol+"";
  $("#filaR_"+idrol+"").html($("#cargador_empresa").html() );
  $.ajax({
    url: miurl
  }).done( function(resul)
  {
    $("#filaR_"+idrol+"").hide();

  }).fail( function()
  {
    alert("No se borro correctamente, intentalo nuevamente o revisa tu conexión");
  }) ;



}


//Subir Anexos Matricula

$(document).on("submit",".formarchivo",function(e){


  e.preventDefault();

  //disable the submit button
  $("#btnsubmit").attr("disabled", true);



  var formu=$(this);
  var nombreform=$(this).attr("id");

  if(nombreform=="f_subir_anexo" ){ var miurl="subir_anexo";  var divresul="notificacion_resul_fci"}
  //información del formulario
  var formData = new FormData($("#"+nombreform+"")[0]);


  //hacemos la petición ajax
  $.ajax({

    url: miurl,
    type: 'POST',

    // Form data
    //datos del formulario
    data: formData,
    //necesario para subir archivos via ajax
    cache: false,
    contentType: false,
    processData: false,
    //mientras enviamos el archivo
    beforeSend: function(){
      $("#"+divresul+"").html($("#cargador_empresa").html());

    },
    //una vez finalizado correctamente
    success: function(data){
      $("#"+divresul+"").html(data);
      $("#loading").hide();
      $("#anexos").load(" #anexos");
      $("#formulario_anexos").load(" #formulario_anexos");

    },
    //si ha ocurrido un error
    error: function(data){
      alert("ha ocurrido un error") ;
    }
  });



});


//Subir Archivos DUE

$(document).on("submit",".formarchivodue",function(e){


  e.preventDefault();

  //disable the submit button
  $("#btnsubmit").attr("disabled", true);



  var formu=$(this);
  var nombreform=$(this).attr("id");

  if(nombreform=="f_subir_archivos_due" ){ var miurl="subir_archivos_due";  var divresul="notificacion_resul_fci"}
  //información del formulario
  var formData = new FormData($("#"+nombreform+"")[0]);


  //hacemos la petición ajax
  $.ajax({

    url: miurl,
    type: 'POST',

    // Form data
    //datos del formulario
    data: formData,
    //necesario para subir archivos via ajax
    cache: false,
    contentType: false,
    processData: false,
    //mientras enviamos el archivo
    beforeSend: function(){
      $("#"+divresul+"").html($("#cargador_empresa").html());

    },
    //una vez finalizado correctamente
    success: function(data){
      $("#"+divresul+"").html(data);
      $("#loading").hide();
      $("#anexos").load(" #anexos");
      $("#formulario_archivos_due").load(" #formulario_archivos_due");

    },
    //si ha ocurrido un error
    error: function(data){
      alert("ha ocurrido un error") ;

    }
  });



});

//Subir Archivos DUE Nacional

$(document).on("submit",".formarchivoduenacional",function(e){


  e.preventDefault();

  //disable the submit button
  $("#btnsubmit").attr("disabled", true);



  var formu=$(this);
  var nombreform=$(this).attr("id");

  if(nombreform=="f_subir_archivos_due_nacional" ){ var miurl="subir_archivos_due_nacional";  var divresul="notificacion_resul_fci"}
  //información del formulario
  var formData = new FormData($("#"+nombreform+"")[0]);


  //hacemos la petición ajax
  $.ajax({

    url: miurl,
    type: 'POST',

    // Form data
    //datos del formulario
    data: formData,
    //necesario para subir archivos via ajax
    cache: false,
    contentType: false,
    processData: false,
    //mientras enviamos el archivo
    beforeSend: function(){
      $("#"+divresul+"").html($("#cargador_empresa").html());

    },
    //una vez finalizado correctamente
    success: function(data){
      $("#"+divresul+"").html(data);
      $("#loading").hide();
      $("#anexos").load(" #anexos");
      $("#formulario_archivos_due_nacional").load(" #formulario_archivos_due_nacional");

    },
    //si ha ocurrido un error
    error: function(data){
      alert("Ha ocurrido un error") ;

    }
  });



});

// Subir Archivos Pruebas Saber

$(document).on("submit",".formarchivopruebas",function(e){


  e.preventDefault();

  //disable the submit button
  $("#btnsubmit").attr("disabled", true);



  var formu=$(this);
  var nombreform=$(this).attr("id");

  if(nombreform=="f_subir_archivos_pruebas" ){ var miurl="subir_archivos_pruebas";  var divresul="notificacion_resul_fci"}
  //información del formulario
  var formData = new FormData($("#"+nombreform+"")[0]);


  //hacemos la petición ajax
  $.ajax({

    url: miurl,
    type: 'POST',

    // Form data
    //datos del formulario
    data: formData,
    //necesario para subir archivos via ajax
    cache: false,
    contentType: false,
    processData: false,
    //mientras enviamos el archivo
    beforeSend: function(){
      $("#"+divresul+"").html($("#cargador_empresa").html());

    },
    //una vez finalizado correctamente
    success: function(data){
      $("#"+divresul+"").html(data);
      $("#loading").hide();
      $("#anexos").load(" #anexos");
      $("#formulario_archivos_pruebas").load(" #formulario_archivos_pruebas");

    },
    //si ha ocurrido un error
    error: function(data){
      alert("Ha ocurrido un error. Valide el tipo de datos en la plantilla") ;

    }
  });



});

// Inicio subida de archivo de clasificaciones

$(document).on("submit",".formarchivoclasificaciones",function(e){


  e.preventDefault();

  //disable the submit button
  $("#btnsubmit").attr("disabled", true);



  var formu=$(this);
  var nombreform=$(this).attr("id");

  if(nombreform=="f_subir_archivos_clasificaciones" ){ var miurl="subir_archivos_clasificaciones";  var divresul="notificacion_resul_fci"}
  //información del formulario
  var formData = new FormData($("#"+nombreform+"")[0]);


  //hacemos la petición ajax
  $.ajax({

    url: miurl,
    type: 'POST',

    // Form data
    //datos del formulario
    data: formData,
    //necesario para subir archivos via ajax
    cache: false,
    contentType: false,
    processData: false,
    //mientras enviamos el archivo
    beforeSend: function(){
      $("#"+divresul+"").html($("#cargador_empresa").html());

    },
    //una vez finalizado correctamente
    success: function(data){
      $("#"+divresul+"").html(data);
      $("#loading").hide();
      $("#anexos").load(" #anexos");
      $("#formulario_archivos_clasificaciones").load(" #formulario_archivos_clasificaciones");

    },
    //si ha ocurrido un error
    error: function(data){
      alert("Ha ocurrido un error.") ;

    }
  });



});

//Subir Archivos Docentes

$(document).on("submit",".formarchivodocentes",function(e){


  e.preventDefault();

  //disable the submit button
  $("#btnsubmit").attr("disabled", true);



  var formu=$(this);
  var nombreform=$(this).attr("id");

  if(nombreform=="f_subir_archivos_docentes" ){ var miurl="subir_archivos_docentes";  var divresul="notificacion_resul_fci"}
  //información del formulario
  var formData = new FormData($("#"+nombreform+"")[0]);


  //hacemos la petición ajax
  $.ajax({

    url: miurl,
    type: 'POST',

    // Form data
    //datos del formulario
    data: formData,
    //necesario para subir archivos via ajax
    cache: false,
    contentType: false,
    processData: false,
    //mientras enviamos el archivo
    beforeSend: function(){
      $("#"+divresul+"").html($("#cargador_empresa").html());

    },
    //una vez finalizado correctamente
    success: function(data){
      $("#"+divresul+"").html(data);
      $("#loading").hide();
      $("#anexos").load(" #anexos");
      $("#formulario_archivos_docentes").load(" #formulario_archivos_docentes");

    },
    //si ha ocurrido un error
    error: function(data){
      alert("ha ocurrido un error") ;

    }
  });



});







//Subir Archivos Mesa de Ayuda sedesconectividad

$(document).on("submit",".formarchivomesa",function(e){


  e.preventDefault();

  //disable the submit button
  $("#btnsubmit").attr("disabled", true);



  var formu=$(this);
  var nombreform=$(this).attr("id");

  if(nombreform=="f_subir_archivos_mesa" ){ var miurl="subir_archivos_mesa";  var divresul="notificacion_resul_fci"}
  //información del formulario
  var formData = new FormData($("#"+nombreform+"")[0]);


  //hacemos la petición ajax
  $.ajax({

    url: miurl,
    type: 'POST',

    // Form data
    //datos del formulario
    data: formData,
    //necesario para subir archivos via ajax
    cache: false,
    contentType: false,
    processData: false,
    //mientras enviamos el archivo
    beforeSend: function(){
      $("#"+divresul+"").html($("#cargador_empresa").html());

    },
    //una vez finalizado correctamente
    success: function(data){
      $("#"+divresul+"").html(data);
      $("#loading").hide();
      $("#anexos").load(" #anexos");
      $("#formulario_archivos_mesa").load(" #formulario_archivos_mesa");

    },
    //si ha ocurrido un error
    error: function(data){
      alert("ha ocurrido un error") ;

    }
  });



});

//Subir Archivos calidad sedesconectividad



$(document).on("submit",".formarchivoconectividad",function(e){


  e.preventDefault();

  //disable the submit button
  $("#btnsubmit").attr("disabled", true);



  var formu=$(this);
  var nombreform=$(this).attr("id");

  if(nombreform=="f_subir_archivos_conectividad" ){ var miurl="subir_archivos_conectividad";  var divresul="notificacion_resul_fci"}
  //información del formulario
  var formData = new FormData($("#"+nombreform+"")[0]);


  //hacemos la petición ajax
  $.ajax({

    url: miurl,
    type: 'POST',

    // Form data
    //datos del formulario
    data: formData,
    //necesario para subir archivos via ajax
    cache: false,
    contentType: false,
    processData: false,
    //mientras enviamos el archivo
    beforeSend: function(){
      $("#"+divresul+"").html($("#cargador_empresa").html());

    },
    //una vez finalizado correctamente
    success: function(data){
      $("#"+divresul+"").html(data);
      $("#loading").hide();
      $("#anexos").load(" #anexos");
      $("#formulario_archivos_conectividad").load(" #formulario_archivos_conectividad");

    },
    //si ha ocurrido un error
    error: function(data){
      alert("ha ocurrido un error") ;

    }
  });



});





$(document).on("submit",".formarchivocalidad",function(e){


  e.preventDefault();

  //disable the submit button
  $("#btnsubmit").attr("disabled", true);



  var formu=$(this);
  var nombreform=$(this).attr("id");

  if(nombreform=="f_subir_archivos_calidad" ){ var miurl="subir_archivos_calidad";  var divresul="notificacion_resul_fci"}
  //información del formulario
  var formData = new FormData($("#"+nombreform+"")[0]);


  //hacemos la petición ajax
  $.ajax({

    url: miurl,
    type: 'POST',

    // Form data
    //datos del formulario
    data: formData,
    //necesario para subir archivos via ajax
    cache: false,
    contentType: false,
    processData: false,
    //mientras enviamos el archivo
    beforeSend: function(){
      $("#"+divresul+"").html($("#cargador_empresa").html());

    },
    //una vez finalizado correctamente
    success: function(data){
      $("#"+divresul+"").html(data);
      $("#loading").hide();
      $("#anexos").load(" #anexos");
      $("#formulario_archivos_calidad").load(" #formulario_archivos_calidad");

    },
    //si ha ocurrido un error
    error: function(data){
      alert("ha ocurrido un error") ;

    }
  });



});

//Subir Archivos Dotacion

$(document).on("submit",".formarchivodotacion",function(e){


  e.preventDefault();

  //disable the submit button
  $("#btnsubmit").attr("disabled", true);



  var formu=$(this);
  var nombreform=$(this).attr("id");

  if(nombreform=="f_subir_archivos_dotacion" ){ var miurl="subir_archivos_dotacion";  var divresul="notificacion_resul_fci"}
  //información del formulario
  var formData = new FormData($("#"+nombreform+"")[0]);


  //hacemos la petición ajax
  $.ajax({

    url: miurl,
    type: 'POST',

    // Form data
    //datos del formulario
    data: formData,
    //necesario para subir archivos via ajax
    cache: false,
    contentType: false,
    processData: false,
    //mientras enviamos el archivo
    beforeSend: function(){
      $("#"+divresul+"").html($("#cargador_empresa").html());

    },
    //una vez finalizado correctamente
    success: function(data){
      $("#"+divresul+"").html(data);
      $("#loading").hide();
      $("#anexos").load(" #anexos");
      $("#formulario_archivos_dotacion").load(" #formulario_archivos_dotacion");

    },
    //si ha ocurrido un error
    error: function(data){
      alert("ha ocurrido un error") ;

    }
  });



});

//Subir Archivos Capacitacion Docentes

$(document).on("submit",".formarchivocapacitacion",function(e){


  e.preventDefault();

  //disable the submit button
  $("#btnsubmit").attr("disabled", true);



  var formu=$(this);
  var nombreform=$(this).attr("id");

  if(nombreform=="f_subir_archivos_capacitacion" ){ var miurl="subir_archivos_capacitacion";  var divresul="notificacion_resul_fci"}
  //información del formulario
  var formData = new FormData($("#"+nombreform+"")[0]);


  //hacemos la petición ajax
  $.ajax({

    url: miurl,
    type: 'POST',

    // Form data
    //datos del formulario
    data: formData,
    //necesario para subir archivos via ajax
    cache: false,
    contentType: false,
    processData: false,
    //mientras enviamos el archivo
    beforeSend: function(){
      $("#"+divresul+"").html($("#cargador_empresa").html());

    },
    //una vez finalizado correctamente
    success: function(data){
      $("#"+divresul+"").html(data);
      $("#loading").hide();
      $("#anexos").load(" #anexos");
      $("#formulario_archivos_capacitacion").load(" #formulario_archivos_capacitacion");

    },
    //si ha ocurrido un error
    error: function(data){
      alert("ha ocurrido un error") ;

    }
  });



});

//Subir Archivos Docentes

$(document).on("submit",".formarchivodocentes",function(e){


  e.preventDefault();

  //disable the submit button
  $("#btnsubmit").attr("disabled", true);

  var formu=$(this);
  var nombreform=$(this).attr("id");

  if(nombreform=="f_subir_archivos_docentes" ){ var miurl="subir_archivos_docentes";  var divresul="notificacion_resul_fci"}
  //información del formulario
  var formData = new FormData($("#"+nombreform+"")[0]);


  //hacemos la petición ajax
  $.ajax({

    url: miurl,
    type: 'POST',

    // Form data
    //datos del formulario
    data: formData,
    //necesario para subir archivos via ajax
    cache: false,
    contentType: false,
    processData: false,
    //mientras enviamos el archivo
    beforeSend: function(){
      $("#"+divresul+"").html($("#cargador_empresa").html());

    },
    //una vez finalizado correctamente
    success: function(data){
      $("#"+divresul+"").html(data);
      $("#loading").hide();

      $("#formulario_archivos_docentes").load(" #formulario_archivos_docentes");

    },
    //si ha ocurrido un error
    error: function(data){
      alert("ha ocurrido un error") ;

    }
  });
});



//Dispositivos
$(document).ready(function(){
  if($("#cbfuncionamiento").val() == "1")
  {
    $("#div_estado").show();
  }
  else
  {
    $("#div_estado").hide(500);
    $("#evidencia").val('');
  }
});

//Dispositivos
$(document).ready(function(){
  $("#cbfuncionamiento").on("change", function() {
    if($("#cbfuncionamiento").val() === "1")
    {
      $("#div_estado").show(500);
    }
    else
    {
      $("#div_estado").hide(500);
      $("#evidencia").val('');
    }
  });
});



//Asignamos como valor principal al "select", el valor 1. Es el que se mostrará sin haber seleccionado nada.
if ($('#tipo_solicitud').val() == 1)
{
  $(".div_dispositivos").css("display","block");
  $(".div_conectividad").css("display","none");
};

//Detectamos los cambios y mostramos uno u otro form
$('#tipo_solicitud').change(function() {

  if ($('#tipo_solicitud').val() == 1)
  {
    $(".div_dispositivos").css("display","block");
    $(".div_conectividad").css("display","none");
  };

  if ($('#tipo_solicitud').val() == 2)
  {
    $(".div_dispositivos").css("display","none");
    $(".div_conectividad").css("display","block");
  };

});




function ver_editar_dispositivo(arg)
{
  var urlraiz=$("#url_raiz_proyecto").val();
  var miurl =urlraiz+"/form_editar_dispositivo/"+arg+"";
  location.href = miurl;
}

function confirmacion_borrado_dispositivo(id){
  var urlraiz=$("#url_raiz_proyecto").val();
  $("#capa_modal").show();
  $("#capa_formularios").show();
  var screenTop = $(document).scrollTop();
  $("#capa_formularios").css('top', screenTop);
  $("#capa_formularios").html($("#cargador_empresa").html());
  var miurl=urlraiz+"/form_borrado_dispositivo_individual/"+id+"";

  $.ajax({
    url: miurl
  }).done( function(resul)
  {
    $("#capa_formularios").html(resul);

  }).fail( function()
  {
    $("#capa_formularios").html('<span>...Ha ocurrido un error, revise su conexión y vuelva a intentarlo...</span>');
  }) ;
}

function ver_editar_traslado(arg)
{
  var urlraiz=$("#url_raiz_proyecto").val();
  var miurl =urlraiz+"/form_editar_traslado/"+arg+"";
  location.href = miurl;
}

function nuevo_traslado_dispositivo(arg)
{
  var urlraiz=$("#url_raiz_proyecto").val();
  var miurl =urlraiz+"/form_nuevo_traslado/"+arg+"";
  location.href = miurl;
}

function confirmacion_borrado_traslado(id){
  var urlraiz=$("#url_raiz_proyecto").val();
  $("#capa_modal").show();
  $("#capa_formularios").show();
  var screenTop = $(document).scrollTop();
  $("#capa_formularios").css('top', screenTop);
  $("#capa_formularios").html($("#cargador_empresa").html());
  var miurl=urlraiz+"/form_borrado_traslado/"+id+"";

  $.ajax({
    url: miurl
  }).done( function(resul)
  {
    $("#capa_formularios").html(resul);

  }).fail( function()
  {
    $("#capa_formularios").html('<span>...Ha ocurrido un error, revise su conexión y vuelva a intentarlo...</span>');
  });
}

//function ver_editar_solicitud(arg)
//{
    //var urlraiz=$("#url_raiz_proyecto").val();
    //var miurl =urlraiz+"/form_editar_solicitud/"+arg+"";
  //  location.href = miurl;
//}

//Subir Archivos dispositivos
$(document).on("submit",".formarchivodispositivos",function(e){
  if(confirm('¿Desea realizar la carga?') == false)
  {
    return false;
  }
  e.preventDefault();

  //disable the submit button
  $("#btnsubmit").attr("disabled", true);

  var formu=$(this);
  var nombreform=$(this).attr("id");

  if(nombreform=="f_subir_archivos_dispositivos" ){ var miurl="subir_carga_dispositivos";  var divresul="notificacion_resul_fci"}
  //información del formulario
  var formData = new FormData($("#"+nombreform+"")[0]);


  //hacemos la petición ajax
  $.ajax({

    url: miurl,
    type: 'POST',

    // Form data
    //datos del formulario
    data: formData,
    //necesario para subir archivos via ajax
    cache: false,
    contentType: false,
    processData: false,
    //mientras enviamos el archivo
    beforeSend: function(){
      $("#"+divresul+"").html($("#cargador_empresa").html());

    },
    //una vez finalizado correctamente
    success: function(data){

      $("#"+divresul+"").html(data);
      $("#loading").hide();


      $("#formulario_archivos_dispositivos").load(" #formulario_archivos_dispositivos");

    },
    //si ha ocurrido un error
    error: function(data){
      alert("No fue posible realizar la carga. Debe convertir el archivo a Utf-8.");
    }
  });

});
//Fin carga dispositivos

$(document).ready(function(){
  $('[data-toggle="tooltip"]').tooltip();
});

//function ver_editar_solicitud(arg)
//{
    //var urlraiz=$("#url_raiz_proyecto").val();
    //var miurl =urlraiz+"/form_editar_solicitud/"+arg+"";
    //location.href = miurl;
//}

function confirmacion_borrado_solicitud(id){
  var urlraiz=$("#url_raiz_proyecto").val();
  $("#capa_modal").show();
  $("#capa_formularios").show();
  var screenTop = $(document).scrollTop();
  $("#capa_formularios").css('top', screenTop);
  $("#capa_formularios").html($("#cargador_empresa").html());
  var miurl=urlraiz+"/form_borrado_solicitud/"+id+"";

  $.ajax({
    url: miurl
  }).done( function(resul)
  {
    $("#capa_formularios").html(resul);

  }).fail( function()
  {
    $("#capa_formularios").html('<span>...Ha ocurrido un error, revise su conexión y vuelva a intentarlo...</span>');
  }) ;
}





//
//  $("#anadirEfii").on("click", function(){


//  $("#añadir_requerimientoo").hide(300);

  // var item = {};
   //item.tipo = $('#tipo_solicitud').val();
  //   item.tipoT = $('#tipo_solicitud  option:selected').text();
  //   item.dispo = $('#tipo_id').val();
  //   item.dispoT = $('#tipo_id  option:selected').text();
    //  item.cantidad = $('#cantidad_d').val();
    //  item.uso = $('#tipo_uso').val();
     //item.usoT = $('#tipo_uso  option:selected').text();



    //   var agregar=true;
      //var values = [];

    // if(agregar){

      //  agregaSolicitud(item);
      //  item.tipo = $('#tipo_solicitud').val(1);
      //  item.dispo = $('#tipo_id').val('');
        //  item.cantidad = $('#cantidad_d').val('');
         //item.uso = $('#tipo_uso').val('');


      // }


  //  });


  //function agregaSolicitud(item){


    // $("#tdReqEspacioss").append('<tr role="row" class="odd"><td>'+item.tipoT+
      //'<input type="hidden" name="rTipo[]" value="'+item.tipo+'"></td><td>'+item.dispoT+
      //'<input type="hidden" name="rDispo[]" value="'+item.dispo+'"></td><td>'+item.cantidad+
      //'<input type="hidden" name="rCanti[]" value="'+item.cantidad+'"></td><td>'+item.uso+
    //  '<input type="hidden" name="rUso[]" value="'+item.usoT+'"/>'+
    // '</td><td><button type="button" class="btn  btn-danger btn-xs eliminaEspacio"><i class="fa fa-fw fa-trash" title="Borrar"></i></button></td></tr>');
  //  }

  //  $(function () {
    //    $(document).on('click', '.eliminaEspacio', function (event) {
      //     event.preventDefault();
        //    $(this).closest('tr').remove();
        //});
    //});

//
