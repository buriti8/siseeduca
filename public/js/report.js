$(document).ready(function(){
  var table;
  $("#genReport").on("click", function(e){
    $('#info').hide(100);
    try {
      var contador=0;
      $("#cargaSelCampos :checkbox").each(function(){
        if($(this).is(":checked"))
          contador++;
      });

      if(contador == 0)
        return;


    var dataString = $('#fmrSelCampos').serialize();

    if (table) {
      table.destroy();
      $('#dt_cliente tbody').remove();
    }
    defineEncabezados();

      e.preventDefault();

      $.ajax({
        type: "POST",
        url: "reportesGenerator",
        data: dataString
      }).done( function(info ){

        $.ajaxSetup({
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
        });
        var json_info = JSON.parse( info );
        var my_columns = [];

      $.each( json_info[0], function( key, value ) {
              var my_item = {};
              my_item.data = key;
              my_columns.push(my_item);
              if(value == 0 && key=='nfilas'){
                $("#info").show(100);
                return;
              }
      });

            table = $("#dt_cliente").DataTable({
              "destroy":true,
              "data": json_info,
              "columns":my_columns,
              "language": idioma_espanol,
              "dom": "<'row'<'form-inline' <'col-sm-offset-5'B>>>"
                 +"<'row' <'form-inline' <'col-sm-1'f>>>"
                 +"<rt>"
                 +"<'row'<'form-inline'"
                 +" <'col-sm-6 col-md-6 col-lg-6'l>"
                 +"<'col-sm-6 col-md-6 col-lg-6'p>>>",
          "buttons": [
            {
                 extend: 'collection',
                 text: 'Export',
                 buttons: [
                     'excel',
                     'csv'
                 ]
             }
          ]
            });
            $( "#dt_cliente_filter" ).parent().parent().prepend("<div class='col-sm-1'></div>");
            var padre = $( "#dt_cliente_filter" ).parent().parent();
            $(".dt-buttons").parent().appendTo(padre);
            $(".dt-buttons").parent().removeClass( "col-sm-offset-5" );
            $(".dt-buttons").parent().addClass( "col-sm-1" );
      });

    } catch (e) {
      $("#danger").show(100);
      console.log(e);
      $('#dt_cliente tbody').remove();
    } finally {
    }
  });

});

function defineEncabezados(){
      var campos = [];
      $('#cargaSelCampos input:checked').each(function() {
          if ($(this).attr('value') != '') {
            $( "#encabezados" ).append( "<th>"+$(this).attr('title')+"</th>")
            campos.push($(this).attr('value'));
          }
      });

      var x = 0;
      $("#dt_cliente th").each(function(){
        x++;
      });

       if(x>campos.length){
          var limit = x-campos.length;
           for (var i = 0; i < limit; i++) {
               $("#dt_cliente th").remove(':first');
           }
       }
}

function matricula(){
  var items = [
    ["matricula_actual.sector","Sector"],
    ["matricula_actual.calendario","Calendario"],
    ["matricula_actual.ano_inf","Año"],
    ["municipios.NombreMunicipio","Municipio"],
    ["matricula_actual.codigo_dane","Dane"],
    ["matricula_actual.dane_anterior","Dane anterior"],
    ["matricula_actual.cons_sede","Consecutivo sede"],
    ["tipo_documentos.Abreviado","Tipo de documento"],
    ["matricula_actual.nro_documento","Documento"],
    ["matricula_actual.exp_depto","Depto de expedición"],
    ["matricula_actual.exp_mun","Municipio de expedición"],
    ["matricula_actual.apellido1","Primer apellido"],
    ["matricula_actual.apellido2","Segundo apellido"],
    ["matricula_actual.nombre1","Primer nombre"],
    ["matricula_actual.nombre2","Segundo nombre"],
    ["matricula_actual.direccion_residencia","Dirección"],
    ["matricula_actual.tel" ,"Teléfono"],
    ["matricula_actual.estrato","Estrato"],
    ["matricula_actual.sisben","Sisbén"],
    ["matricula_actual.fecha_nacimiento","Nacimiento"],
    ["departamentos.NombreDepartamento","Departamento"],
    ["matricula_actual.nac_mun","Luda de nacimiento"],
    ["matricula_actual.genero","Genero"],
    ["victima_conflictos.Descripcion","Descripción"],
    ["matricula_actual.dpto_exp","Dpto expulsor "],
    ["matricula_actual.mun_exp","Último municipio expulsor"],
    ["matricula_actual.proviene_sector_priv","Proviene de sector privado"],
    ["matricula_actual.proviene_otr_mun","Proviene de otro municipio"],
    ["matricula_actual.cap_exc","Capacidad exepcional"],
    ["matricula_actual.etnia","Etnia"],
    ["matricula_actual.res","Resguardo"],
    ["matricula_actual.ins_familiar","Institución bienestar de origen"],
    ["jornadas.Descripcion","Jornada"],
    ["matricula_actual.caracter","Carácter"],
    ["matricula_actual.especialidad","Especialidad"],
    ["matricula_actual.grado","Grado"],
    ["matricula_actual.grupo","Grupo"],
    ["matricula_actual.metodologia","Metodología"],
    ["matricula_actual.matricula_contratada","Matricula contratada"],
    ["matricula_actual.repitente","Repitente"],
    ["matricula_actual.nuevo","Nuevo"],
    ["matricula_actual.sit_acad_ano_ant","Situación académica anterior"],
    ["matricula_actual.con_alum_ano_ant","Condición alumno año anterior"],
    ["matricula_actual.fue_recu","Fuentes de recursos"],
    ["zonas.Descripcion","Zona"],
    ["matricula_actual.cab_familia","Alumno cabeza de familia"],
    ["matricula_actual.ben_mad_flia","Ben hijos dependientes de mad cab de flia"],
    ["matricula_actual.ben_vet_fp","Beneficiario vetenaros fuerza pública"],
    ["matricula_actual.ben_her_nac","Beneficiario héroes de la nación"],
    ["matricula_actual.codigo_internado","Código internado"],
    ["matricula_actual.codigo_valoracion_1","Código valoración #1"],
    ["matricula_actual.codigo_valoracion_2","Código valoración #2"],
    ["matricula_actual.num_convenio","Número de convenio"],
    ["matricula_actual.apoyo_academico_especial","Apoyo académico especial"],
    ["matricula_actual.srpa","Sisbén de responsabilidad social"],
    ["matricula_actual.pais_origen","País de origen"]
  ];

  var keys = [
    ["matricula_actual.mun_codigo","municipios.CodigoMunicipio", "municipios"],
    ["matricula_actual.tipo_documento","tipo_documentos.IdTipoDocumento", "tipo_documentos"],
    ["matricula_actual.nac_depto","departamentos.CodigoDepartamento", "departamentos"],
    ["matricula_actual.pob_vict_conf","victima_conflictos.IdVictimaConflicto", "victima_conflictos"],
    ["matricula_actual.tipo_jornada","jornadas.IdJornada", "jornadas"],
    ["matricula_actual.zon_alu","zonas.IdZona", "zonas"],
    ["municipios.CodigoSubregion","subregions.CodigoSubregion", "subregions"]
  ];

  var tablaPrincipal="matricula_actual";

  var datos=[keys, items, tablaPrincipal];
  return datos;
}

function matriculapublica(){
  var items = [
    ["historico_sedes.codigo_establecimiento", "Dane institución" ],
    ["historico_sedes.nombre_establecimiento", "Nombre establecimiento"],
    ["matricula_actual.dane_anterior","Dane anterior"],
    ["historico_sedes.nombre_sede", "Nombre sede" ],
    ["historico_sedes.zona", "Zona sede" ],
    ["historico_establecimientos.sector", "Naturaleza IE"],
    ["historico_sedes.nombre_municipio", "Municipio" ],
    ["subregions.NombreSubregion", "Subregión" ],
    ["historico_establecimientos.tipo_establecimiento", "Tipo de establecimiento"],
    ["zonas.Descripcion","Zona"],
    ["matricula_actual.grado","Grado"],
    ["matricula_actual.metodologia","Metodología"],
    ["matricula_actual.genero","Genero"],
    ["matricula_actual.estrato","Estrato"],
    ["matricula_actual.sisben","Sisbén"],
    ["matricula_actual.repitente","Repitente"],
    ["matricula_actual.nuevo","Nuevo"],
    ["matricula_actual.sector","Sector"],
    ["matricula_actual.matricula_contratada","Matricula contratada"],
    ["matricula_actual.sit_acad_ano_ant","Situación académica anterior"],
    ["matricula_actual.con_alum_ano_ant","Condición alumno año anterior"],
    ["victima_conflictos.Descripcion","Descripción"],
    ["matricula_actual.cap_exc","Capacidad excepcional"],
    ["matricula_actual.etnia","Etnia"],
    ["matricula_actual.res","Resguardo"],
    ["matricula_actual.caracter","Carácter"],
    ["matricula_actual.especialidad","Especialidad"]
  ];

  var keys = [
    ["matricula_actual.mun_codigo","municipios.CodigoMunicipio", "municipios"],
    ["matricula_actual.tipo_documento","tipo_documentos.IdTipoDocumento", "tipo_documentos"],
    ["matricula_actual.codigo_dane","historico_establecimientos.codigo_establecimiento", "historico_establecimientos"],
    ["historico_establecimientos.codigo_establecimiento","historico_sedes.codigo_establecimiento", "historico_sedes"],
    ["matricula_actual.pob_vict_conf","victima_conflictos.IdVictimaConflicto", "victima_conflictos"],
    ["matricula_actual.tipo_jornada","jornadas.IdJornada", "jornadas"],
    ["matricula_actual.zon_alu","zonas.IdZona", "zonas"],
    ["municipios.CodigoSubregion","subregions.CodigoSubregion", "subregions"]
  ];

  var tablaPrincipal="matricula_actual";

  var datos=[keys, items, tablaPrincipal];
  return datos;
}

function dueSedes(){
  var items = [
    ["meses.mes", "Mes corte" ],
    ["anio_corte", "Año corte" ],
    ["nombre_departamento", "Departamento" ],
    ["codigo_dane_municipio", "Código Municipio"],
    ["nombre_municipio", "Municipio" ],
    ["historico_sedes.codigo_establecimiento", "Código estableciminto"],
    ["nombre_establecimiento", "Nombre estableciminto"],
    ["codigo_sede", "Código sede"],
    ["nombre_sede", "Nombre sede" ],
    ["zona", "Zona" ],
    ["direccion", "Dirección" ],
    ["telefono", "Teléfono" ],
    ["estado_sede", "Estado"],
    ["niveles", "Niveles" ],
    ["modelos", "Modelos" ],
    ["grados", "Grados" ]
  ];

  var keys = [
    ["historico_sedes.codigo_dane_municipio","municipios.CodigoDaneMunicipio", "municipios"],
    ["historico_sedes.mes_corte","meses.id", "meses"],
    ["municipios.CodigoSubregion","subregions.CodigoSubregion", "subregions"]
  ];

  var tablaPrincipal="historico_sedes";

  var datos=[keys, items, tablaPrincipal];
  return datos;
}

function historicoConectividad(){
  var items = [
  ["meses.mes", "Mes corte" ],
  ["historico_conectividad.anio_corte", "Año corte"],
  ["historico_conectividad.codigo_dane_sede", "Dane sede"],
  ["historico_conectividad.nombre_sede_educativa", "Nombre sede"],
  ["historico_conectividad.direccion", "Dirección"],
  ["historico_conectividad.telefono", "Teléfono"],
  ["historico_conectividad.zona", "Zona"],
  ["historico_conectividad.departamento", "Departamento"],
  ["historico_conectividad.municipio", "Municipio"],
  ["historico_conectividad.programa_origen_de_los_recursos", "Programa origen de recursos"],
  ["historico_conectividad.numero_de_contrato", "Número de contrato"],
  ["historico_conectividad.operador", "Operador"],
  ["historico_conectividad.tecnologia_ultima_milla", "Tec. última milla"],
  ["historico_conectividad.ancho_de_banda", "Ancho de banda"],
  ["historico_conectividad.meses_de_servicio", "meses de servicio"],
  ["historico_conectividad.fecha_inicio_servicio", "Fecha inicio"],
  ["historico_conectividad.fecha_fin_servicio", "Fecha fin"]
  ];

  var keys = [
    ["historico_conectividad.codigo_dane_sede", "temp_historico_sedes.codigo_sede", "temp_historico_sedes"],
    ["temp_historico_sedes.codigo_dane_municipio","municipios.CodigoDaneMunicipio", "municipios"],
    ["historico_conectividad.mes_corte","meses.id", "meses"],
    ["municipios.CodigoSubregion","subregions.CodigoSubregion", "subregions"]
  ];

  var tablaPrincipal="historico_conectividad";

  var datos=[keys, items, tablaPrincipal];
  return datos;
}

function infoInfraFisica(){
  var items = [
  ["subregions.NombreSubregion", "Subregión"],
  ["temp_historico_sedes.nombre_municipio", "Municipio"],
  ["temp_historico_sedes.codigo_establecimiento", "Dane establecimiento"],
  ["temp_historico_sedes.nombre_establecimiento", "Nombre establecimiento"],
  ["temp_historico_sedes.codigo_sede", "Código sede"],
  ["temp_historico_sedes.nombre_sede", "Nombre sede"],
  ["temp_historico_sedes.zona", "Zona"],
  ["temp_historico_sedes.direccion", "Dirección"],
  ["temp_historico_establecimientos.telefono", "Teléfono"],
  ["sedes_legals.TipoPropietario", "Tipo de Propietario"],
  ["sedes_legals.Modalidad", "Modalidad"],
  ["sedes_legals.AreaLote", "Área lote"],
  ["sedes_legals.AreaConstruida", "Área Construida"],
  ["sedes_legals.Plano", "Cuentan Con Plano"],
  ["sedes_legals.Foto", "Cuentan Con Foto"],
  ["sedes_legals.Latitud", "Latitud"],
  ["sedes_legals.Coordenadas", "Longitud"],
  ["sedes_legals.Distancia", "Distancia de la cab Municipal"],
  ["sedes_legals.TipoVia", "Tipo de vía"],
  ["sedes_servicios.TipoEnergia", "Tipo energía"],
  ["historico_riesgos.riesgos", "Historico riesgos"],
  ["historico_riesgos.created_at", "Fecha riesgos"],
  ["sedes_servicios.Acueducto", "Acueducto"],
  ["sedes_servicios.Alcantarillado", "Alcantarillado"],
  ["sedes_servicios.PozoSeptico", "Pozo séptico"],
  ["sedes_espacios.AulasPreescolar", "Aulas de preescolar"],
  ["sedes_espacios.AulasPrimaria", "Aulas de primaria"],
  ["sedes_espacios.AulasSecundaria", "Aulas de Secundaria"],
  ["sedes_espacios.AulasEspaciales", "Aulas de espaciales"],
  ["sedes_espacios.Biblioteca", "Bibliotecas"],
  ["sedes_espacios.AulasSistemas", "Aulas de sistemas"],
  ["sedes_espacios.AulasBilinguismo", "Aulas de bilingüismo"],
  ["sedes_espacios.Laboratorio", "Laboratorios"],
  ["sedes_espacios.AulasTalleres", "Aulas de Talleres"],
  ["sedes_espacios.AulasMultiples", "Aulas múltiples"],
  ["sedes_espacios.Cocinas", "Cocinas"],
  ["sedes_espacios.Comedores", "Comedores"],
  ["sedes_espacios.SantiriosHombres", "Santirios hombres"],
  ["sedes_espacios.SanitariosMujeres", "Sanitarios mujeres"],
  ["sedes_espacios.SanitariosReducida", "Sanitarios movilidad reducida"],
  ["sedes_espacios.LavamanosHombres", "Lavamanos hombres"],
  ["sedes_espacios.LavamanosMujeres", "Lavamanos mujeres"],
  ["sedes_espacios.Orinales", "Orinales"],
  ["sedes_espacios.Vivienda", "Vivienda para docente"],
  ["sedes_espacios.Canchas", "Canchas"],
  ["sedes_espacios.PlacasMulti", "Placas multifuncionales"],
  ["sedes_espacios.JuegosInfantiles", "Juegos infantiles"]
  ];

  var keys = [
    ["temp_historico_sedes.codigo_dane_municipio","municipios.CodigoDaneMunicipio", "municipios"],
    ["municipios.CodigoSubregion","subregions.CodigoSubregion", "subregions"],
    ["temp_historico_sedes.codigo_sede","temp_historico_establecimientos.codigo_establecimiento", "temp_historico_establecimientos"],
    ["temp_historico_sedes.codigo_sede","sedes_legals.DaneSede", "sedes_legals"],
    ["temp_historico_sedes.codigo_sede","sedes_servicios.DaneSede", "sedes_servicios"],
    ["temp_historico_sedes.codigo_sede","historico_riesgos.codigo_dane", "historico_riesgos"],
    ["temp_historico_sedes.codigo_sede","sedes_espacios.DaneSede", "sedes_espacios"]

  ];

  var tablaPrincipal="temp_historico_sedes";

  var datos=[keys, items, tablaPrincipal];
  return datos;

}

function historicoCalidad(){
  var items = [
  ["meses.mes", "Mes corte" ],
  ["historico_calidad.anio_corte", "Año corte"],
  ["municipios.NombreMunicipio", "Municipio"],
  ["historico_calidad.codigo_dane_sede", "Dane sede"],
  ["historico_calidad.sede_educativa", "Nombre sede"],
  ["historico_calidad.tipo_de_canal", "Tipo canal"],
  ["historico_calidad.disponibilidad_del_servicio", "disponibilidad del servicio"],
  ["historico_calidad.descuento_disponibilidad_del_servicio", "% descuento disponibilidad"],
  ["historico_calidad.latencia", "Latencia"],
  ["historico_calidad.descuento_latencia", "% descuento latencia"],
  ["historico_calidad.efectividad_instalacion", "Efectividad de instalación"],
  ["historico_calidad.descuento_efectividad_instalacion", "%  Descuento efectividad de instalación"],
  ["historico_calidad.velocidad_de_transferencia", "velocidad de transferencia"],
  ["historico_calidad.descuento_velocidad_de_transferencia", "% Descuento velocidad de transferencia"]
  ];

  var keys = [
    ["historico_calidad.codigo_dane_sede", "temp_historico_sedes.codigo_sede", "temp_historico_sedes"],
    ["temp_historico_sedes.codigo_dane_municipio","municipios.CodigoDaneMunicipio", "municipios"],
    ["historico_calidad.mes_corte","meses.id", "meses"],
    ["municipios.CodigoSubregion","subregions.CodigoSubregion", "subregions"]
  ];

  var tablaPrincipal="historico_calidad";

  var datos=[keys, items, tablaPrincipal];
  return datos;
}

function dueEstablecimientos(){
  var items = [

    ["meses.mes", "Mes corte" ],
    ["anio_corte", "Año corte"],
    ["departamento", "Departamento"],
    ["historico_establecimientos.codigo_dane_municipio", "Código dane municipio"],
    ["municipio", "Municipio"],
    ["codigo_establecimiento", "Código estableciminto"],
    ["nombre_establecimiento", "Nombre estableciminto"],
    ["direccion", "Dirección"],
    ["telefono", "Teléfono"],
    ["nombre_rector", "Rector"],
    ["tipo_establecimiento", "Tipo"],
    ["etnias", "Etnias"],
    ["sector", "Sector"],
    ["genero", "Genero"],
    ["zona", "Zona"],
    ["niveles", "Niveles"],
    ["jornadas", "Jornadas"],
    ["caracter", "Catácter"],
    ["especialidad", "Especialidad"],
    ["licencia", "Licencia"],
    ["grados", "Grados"],
    ["modelos_educativos", "Modelos educativos"],
    ["capacidades_excepcionales", "Capaciadades excepcionales"],
    ["discapacidades", "Discapacidades"],
    ["idiomas", "Idiomas"],
    ["numero_de_sedes", "Número de sedes"],
    ["estado", "Estado"],
    ["prestador_de_servicio", "Prestador del servicio"],
    ["propiedad_de_la_planta_fisica", "Propiedad planta fisíca"],
    ["resguardo", "Resguardo"],
    ["matricula_contratada", "Matrícula contratada"],
    ["calendario", "Calendario"],
    ["internado", "Internado"],
    ["estrato_socio_economico", "Estrato socio económico"],
    ["correo_electronico", "Correo"]
  ];

  var keys = [
    ["historico_establecimientos.codigo_dane_municipio","municipios.CodigoDaneMunicipio", "municipios"],
    ["historico_establecimientos.mes_corte","meses.id", "meses"],
    ["municipios.CodigoSubregion","subregions.CodigoSubregion", "subregions"]
  ];


  var tablaPrincipal="historico_establecimientos";

  var datos=[keys, items, tablaPrincipal];
  return datos;
}

function Solicitudes(){
  var items = [

  ["solicitudes.tipo_solicitud", "Tipo solicitud" ],
  ["solicitudes.sede_id", "Sede"],
  ["solicitudes.DaneEstablecimiento", "Dane Establecimiento"],
  ["solicitudes.tipodispositivo_id", "Tipo dispositivo"],
  ["solicitudes.descripcion", "Descripción"],
  ["solicitudes.respuesta", "Respuesta"],
  ["solicitudes.solicitud_fisica", "Archivo PDF"],
  ["solicitudes.cantidad_d", "Cantidad de dispositivo"],
  ["solicitudes.tipo_uso", "Tipo uso"],

  ];

  var keys = [

   ["solicitudes.sede_id", "temp_historico_sedes.codigo_sede", "temp_historico_sedes"]
   ["temp_historico_sedes.sede_id", "temp_historico_sedes.codigo_sede", "solicitudes"]






  ];

  var tablaPrincipal="solicitudes";

  var datos=[keys, items, tablaPrincipal];
  return datos;
}

function checkGenerator(){
  $("#hdnaniocorte").val("");
  $("#hdnmescorte").val("");
  $("#selAnioCorte").empty();
  $("#selMesCorte").empty();
  $("#selMesContenedor").hide();
  $('#info').hide(100);
  $("#infofiltroSeleccinando").hide(100);
  $("#infofiltroMatricula").hide(100);
  $("#cargaResultados").empty();
  $("#cargaSelCampos").empty();
  $("#selAnioContenedor").show();
  $("#CasoEspecialMatricula").hide();

  var selArea=document.getElementById("selArea").value;
  var area=null;
  switch(selArea){
    case "matricula":
    area = matricula();
    $("#CasoEspecialMatricula").show();
    break;
    case "dueEst":
    area = dueEstablecimientos();
    break;
    case "dueSed":
    area = dueSedes();
    break;
    case "matriculapublica":
    area = matriculapublica();
    $("#CasoEspecialMatricula").show();
    break;
    case "historicoConectividad":
    area = historicoConectividad();
    break;
    case "historicoCalidad":
    area = historicoCalidad();
    break;
    case "InfraFisica":
    $("#selAnioContenedor").hide();
    area = infoInfraFisica();
    break;
    case "Solicitudes":
    $("#selAnioContenedor").hide();
    area = Solicitudes();
    break;
    default:
      return;
    break;
  }

  var keys = area[0];
  var items = area[1];
  var tPrincipal = area[2];
  genColums(items, tPrincipal);
  asignarkeys(keys);
  cargaAnios(tPrincipal);
}

function genColums(items, tPrincipal){
  $("#cargaSelCampos").append("<div class='form-check col-xs-4'><label class='checkbox-formulario'><input type='checkbox' value='' id='checkTodos' onchange='selTodos()'/><span class='label-text'>Marcar/Desmarcar Todos</span></label></div>");
  for (var i = 0; i < items.length; i++) {
    $("#cargaSelCampos").append("<div class='form-check col-xs-4'><label class='checkbox-formulario'><input type='checkbox' name='colums[]' title='"+items[i][1]+"' value='"+items[i][0]+"'/><span class='label-text'>"+items[i][1]+"</span></label></div>");
  }
    $("#cargaSelCampos").append("<input type='hidden' id='tPrincipal' name='tPrincipal' value='"+tPrincipal+"'/>");
}

function asignarkeys(keys){
  for (var i = 0; i < keys.length; i++) {
    $("#cargaSelCampos").append("<input type='hidden' name='local[]' value='"+keys[i][0]+"'/>");
    $("#cargaSelCampos").append("<input type='hidden' name='foraneo[]' value='"+keys[i][1]+"'/>");
    $("#cargaSelCampos").append("<input type='hidden' name='tforanea[]' value='"+keys[i][2]+"'/>");
  }
}

function selTodos(data){
  var check = document.getElementById("checkTodos").checked;
  $("#cargaSelCampos :checkbox").attr("checked",check);
}

function cargaAnios(tPrincipal){
  $.ajaxSetup({
  headers: {
    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
  });


  $("#loader").show(100);
$.ajax({
  type: "POST",
  url: "filtraAnio",
  data: {"tPrincipal": tPrincipal}
}).done( function(info ){
  $('#selAnioCorte').empty();
  $( "#selAnioCorte" ).append( "<option value=''>TODOS</option>");
  var json_info = JSON.parse( info );
  $.each( json_info, function( key, value ) {
        if(tPrincipal == 'matricula_actual'){
          var anio_corte = value.ano_inf;
        }else {
          var anio_corte = value.anio_corte;
        }
          $( "#selAnioCorte" ).append( "<option value='"+anio_corte+"'>"+anio_corte+"</option>");
          if(anio_corte == 0){
            $("#infofiltroSeleccinando").show(100);
            if(tPrincipal == 'matricula_actual'){
              $("#infofiltroMatricula").show(100);
            }
          }
  });
  $("#loader").hide();
});
}

function filtroMeses(){
  var anio_corte = $("#selAnioCorte").val();
  $("#hdnaniocorte").val(anio_corte);

  if(anio_corte == ""){
    $('#selMesContenedor').hide(100);
    return;
  }

  var tPrincipal = $('#tPrincipal').val();
  $("#loader").show(100);

  $.ajaxSetup({
  headers: {
    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
  });

  $.ajax({
    type: "POST",
    url: "filtraMes",
    data: {"tPrincipal": tPrincipal, "hdnaniocorte":anio_corte}
  }).done( function(info ){
    $('#selMesCorte').empty();
    $( "#selMesCorte" ).append( "<option value=''>TODOS</option>");
    var json_info = JSON.parse( info );
    $.each( json_info, function( key, value ) {
            var codigo = value.mes_corte;
            var name = value.mes;

            $( "#selMesCorte" ).append( "<option value='"+codigo+"'>"+name+"</option>");
    });
    $("#loader").hide();

    $('#selMesContenedor').show(100);
  });
}

var idioma_espanol = {
		    "sProcessing":     "Procesando...",
		    "sLengthMenu":     "Mostrar _MENU_ registros",
		    "sZeroRecords":    "No se encontraron resultados",
		    "sEmptyTable":     "Ningún dato disponible en esta tabla",
		    "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
		    "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
		    "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
		    "sInfoPostFix":    "",
		    "sSearch":         "",
		    "sUrl":            "",
		    "sInfoThousands":  ",",
		    "sLoadingRecords": "Cargando...",
		    "oPaginate": {
		        "sFirst":    "Primero",
		        "sLast":     "Último",
		        "sNext":     "Siguiente",
		        "sPrevious": "Anterior"
		    },
		    "oAria": {
		        "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
		        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
		    }
		}

    function filtroSubregionMunicipios(){
        var subregion = $("#selSubregion").val();
        if(subregion == ""){
          $('#selMunicipioContenedor').hide(100);
          $("#hdnsubregion").val("");
          $("#hdnmunicipio").val("");
          return;
        }

          $.ajaxSetup({
          headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
          });

        $.ajax({
          type: "POST",
          url: "filtraMunicipios",
          data: {"subregion": subregion}
        }).done( function(info ){
          $('#selMunicipio').empty();
          $( "#selMunicipio" ).append( "<option value=''>TODOS</option>");
          var json_info = JSON.parse( info );
          $.each( json_info, function( key, value ) {
                  var codigo = value.CodigoDaneMunicipio;
                  var name =  value.NombreMunicipio;
                  $( "#selMunicipio" ).append( "<option value='"+codigo+"'>"+name+"</option>");
          });
          $('#selMunicipioContenedor').show(100);
        });
        $("#hdnsubregion").val(subregion);
        $("#hdnmunicipio").val("");

    }

    function filtroMunicipios(){
      var subregion = $("#selMunicipio").val();
      $("#hdnmunicipio").val(subregion);
    }

    function selecMeses(){
      var mescorte = $("#selMesCorte").val();
      $("#hdnmescorte").val(mescorte);
    }

function encode_utf8(s) {
  return unescape(encodeURIComponent(s));
}
function decode_utf8(s) {
  return decodeURIComponent(escape(s));
}


function  verinfo_usuario2(arg){
  var urlraiz=$("#url_raiz_proyecto").val();
  var miurl =urlraiz+"/form_editar_usuario2/"+arg+"";
  $("#capa_modal").show();
  $("#capa_formularios").show();
  var screenTop = $(document).scrollTop();
  $("#capa_formularios").css('top', screenTop);
  $("#capa_formularios").html($("#cargador_empresa").html());

    $.ajax({
    url: miurl
    }).done( function(resul)
    {
     $("#capa_formularios").html(resul);

    }).fail( function()
   {
    $("#capa_formularios").html('<span>...Ha ocurrido un error, revise su conexión y vuelva a intentarlo...</span>');
   }) ;

}

$(document).on("submit",".formentrada",function(e){
  e.preventDefault();
  var quien=$(this).attr("id");
  var formu=$(this);
  var varurl="";

  if(quien=="f_editar_usuario2"){  var varurl=$(this).attr("action");  var div_resul="notificacion_E2";  }

  $("#"+div_resul+"").html( $("#cargador_empresa").html());
  $.ajax({
    // la URL para la petición
    url : varurl,
    data : formu.serialize(),
    type : 'POST',
    dataType : 'html',

    success : function(resul) {
      $("#"+div_resul+"").html(resul);

    },
    error : function(xhr, status) {
        $("#"+div_resul+"").html('ha ocurrido un error, revise su conexión e intentelo nuevamente');
    }

  });


});
