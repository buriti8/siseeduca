<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::group(['middleware' => 'auth'], function () {
//->middleware('roleshinobi:administrador_sistema')
  Route::get('/home', 'HomeController@index');
    Route::get('/listado_usuarios', 'UsuariosController@listado_usuarios');
    Route::get('/cargar_datos', 'MatriculaController@index');
    Route::post('subir_archivos_due', 'DueController@subir_archivos');

    Route::post('subir_archivos_due_nacional', 'DueNacionalController@subir_archivos');
    Route::get('form_borrado_archivos_due_nacional/{ano_info}/{mes_corte}', 'DueNacionalController@form_borrado_archivos_due_nacional');

//-- Ruta para carga masiva y ver arhivos cargados pruebas saber -->
    Route::get('/cargar_datos_saber', 'PruebaSaberController@index');


//-- Ruta post para carga masiva pruebas saber -->
    Route::post('subir_archivos_pruebas', 'PruebaSaberController@subir_archivos');

//-- Ruta post para carga masiva clasificaciones saber -->
    Route::post('subir_archivos_clasificaciones', 'ClasificacionesController@subir_archivos');
    Route::get('/clasificaciones_saber', 'ClasificacionesController@clasificaciones');

//-- Ruta para consultar reportes pruebas saber -->
    Route::get('/consulta_basica_saber_antioquia', 'PruebaSaberController@antioquia');
//-- Fin rutas para consultar reportes pruebas saber -->

    Route::get('/descargar_plantilla_saber', 'PruebaSaberController@DescargarPlantillaSaber');
    Route::get('/descargar_plantilla_clasificaciones', 'ClasificacionesController@DescargarPlantillaClasificaciones');

//--/ Inicio Rutas para eliminar carga masiva pruebas saber -->
    Route::get('form_borrado_archivos_saber/{periodo}', 'PruebaSaberController@form_borrado_archivos_saber');
    Route::post('borrar_archivos_saber', 'PruebaSaberController@borrar_archivos_saber');
//--/ Fin Rutas para eliminar carga masiva pruebas saber -->

//--/ Inicio Rutas para eliminar carga masiva clasificaciones saber -->
    Route::get('form_borrado_archivos_clasificaciones/{periodo}', 'ClasificacionesController@form_borrado_archivos_clasificaciones');
    Route::post('borrar_archivos_clasificaciones', 'ClasificacionesController@borrar_archivos_clasificaciones');
//--/ Fin Rutas para eliminar carga masiva clasificaciones saber -->

    Route::get('/descargar_plantilla_sedes', 'DueController@DescargarPlantillaSedes');
    Route::get('/descargar_plantilla_establecimientos', 'DueController@DescargarPlantillaEstablecimientos');
    Route::get('/descargar_plantilla_matricula', 'MatriculaController@DescargarPlantillaMatricula');

    Route::post('subir_archivos_mesa', 'MesaController@subir_archivos');

    Route::post('subir_archivos_docentes', 'DocentesController@subir_archivos');
    Route::post('subir_archivos_mesa', 'MesaController@subir_archivos');
    Route::post('subir_archivos_calidad', 'CalidadConectividadController@subir_archivos');
    Route::post('subir_archivos_dotacion', 'DotacionController@subir_archivos');
    Route::post('subir_archivos_capacitacion', 'CapacitacionController@subir_archivos');

    Route::get('/editar_estructura', 'EstructuraController@listado_estructura');
    Route::get('/editar_informacion', 'InformacionController@listado_informacion');
    Route::get('/consulta_avanzada', 'EstructuraController@listado_avanzada');
    //Inicio de enrutamiento de vistas de consulta basica de matricula y due
    Route::get('/consulta_basica_matricula', 'ConsultaBasicaMatriculaController@index');
    Route::get('/consulta_basica_due', 'ConsultaBasicaDueController@index');
    //Fin de enrutamiento de vistas de consulta basica de matricula y due

    // Rutas DataTable para editar estructura del módulo matrícula.
    Route::get('/listado_departamentos', 'EstructuraController@listado_departamentos')->name('datatable.departamentos');
    Route::get('/listado_municipios', 'EstructuraController@listado_municipios')->name('datatable.municipios');
    Route::get('/listado_subregiones', 'EstructuraController@listado_subregiones')->name('datatable.subregiones');
    Route::get('/listado_tipodoc', 'EstructuraController@listado_tipodoc')->name('datatable.tipodoc');
    Route::get('/listado_comoiteJunta', 'EstructuraController@listado_comiteJunta')->name('datatable.comiteJunta');
    Route::get('/listado_documento_editar_estructura', 'EstructuraController@listado_documento')->name('datatable.documento_editarEstructura');
    Route::get('/listado_conaluant', 'EstructuraController@listado_conaluant')->name('datatable.conaluant');
    Route::get('/listado_sitacant', 'EstructuraController@listado_sitacant')->name('datatable.sitacant');
    Route::get('/listado_zonas', 'EstructuraController@listado_zonas')->name('datatable.zonas');
    Route::get('/listado_tipodis', 'EstructuraController@listado_tipodis')->name('datatable.tipodis');
    Route::get('/listado_metodologia', 'EstructuraController@listado_metodologia')->name('datatable.metodologia');
    Route::get('/listado_estrato', 'EstructuraController@listado_estrato')->name('datatable.estrato');
    Route::get('/listado_resguardos', 'EstructuraController@listado_resguardos')->name('datatable.resguardos');
    Route::get('/listado_jornada', 'EstructuraController@listado_jornada')->name('datatable.jornada');
    Route::get('/listado_victcon', 'EstructuraController@listado_victcon')->name('datatable.victcon');
    Route::get('/listado_capacidadexcepcionals', 'EstructuraController@listado_capacidadexcepcionals')->name('datatable.capacidadexcepcionals');
    Route::get('/listado_etnias', 'EstructuraController@listado_etnias')->name('datatable.etnias');
    Route::get('/listado_nivel', 'EstructuraController@listado_nivel')->name('datatable.nivel');
    Route::get('/listado_nivelmedia', 'EstructuraController@listado_nivelmedia')->name('datatable.nivelmedia');
    Route::get('/listado_nivelcine', 'EstructuraController@listado_nivelcine')->name('datatable.nivelcine');
    Route::get('/listado_grado', 'EstructuraController@listado_grado')->name('datatable.grado');
    // Fin rutas DataTable para editar estructura del módulo matrícula.

    Route::get('/cargar_datos_due', 'DueController@index');
    Route::get('/cargar_datos_due_nacional', 'DueNacionalController@index');
      Route::get('/cargar_datos_mesa', 'MesaController@index');
      Route::get('/cargar_datos_docentes', 'DocentesController@index');
      Route::get('/cargar_datos_calidad', 'CalidadConectividadController@index');
      Route::get('/cargar_datos_dotacion', 'DotacionController@index');
        Route::get('/cargar_datos_capacitacion', 'CapacitacionController@index');





    Route::post('crear_usuario', 'UsuariosController@crear_usuario');
    Route::post('crear_departamento', 'EstructuraController@crear_departamento');
    Route::post('crear_tipodocumento', 'EstructuraController@crear_tipodocumento');
    Route::post('crear_conalunmant', 'EstructuraController@crear_conalunmant');
    Route::post('crear_sitacaant', 'EstructuraController@crear_sitacaant');
    Route::post('crear_zona', 'EstructuraController@crear_zona');
    Route::post('crear_tipodiscapacidad', 'EstructuraController@crear_tipodiscapacidad');
    Route::post('crear_metodologia', 'EstructuraController@crear_metodologia');
    Route::post('crear_estrato', 'EstructuraController@crear_estrato');
    Route::post('crear_resguardo', 'EstructuraController@crear_resguardo');
    Route::post('crear_jornada', 'EstructuraController@crear_jornada');


    Route::get('form_editar_departamento/{id}', 'EstructuraController@form_editar_departamento');
    Route::get('form_editar_tipodocumento/{id}', 'EstructuraController@form_editar_tipodocumento');
    Route::get('form_editar_comiteJunta/{id}', 'EstructuraController@form_editar_comiteJunta');
    Route::get('form_editar_documento_estructura/{id}', 'EstructuraController@form_editar_documento');
    Route::get('form_editar_conalunmant/{id}', 'EstructuraController@form_editar_conalunmant');
    Route::get('form_editar_sitacaant/{id}', 'EstructuraController@form_editar_sitacaant');
    Route::get('form_editar_zona/{id}', 'EstructuraController@form_editar_zona');
    Route::get('form_editar_tipodiscapacidad/{id}', 'EstructuraController@form_editar_tipodiscapacidad');
    Route::get('form_editar_metodologia/{id}', 'EstructuraController@form_editar_metodologia');
    Route::get('form_editar_estrato/{id}', 'EstructuraController@form_editar_estrato');
    Route::get('form_editar_resguardo/{id}', 'EstructuraController@form_editar_resguardo');
    Route::get('form_editar_jornada/{id}', 'EstructuraController@form_editar_jornada');



    Route::get('form_borrado_departamento/{iddept}', 'EstructuraController@form_borrado_departamento');
    Route::get('form_borrado_tipodocumento/{iddept}', 'EstructuraController@form_borrado_tipodocumento');
    Route::get('form_borrado_comiteJunta/{iddept}', 'EstructuraController@form_borrado_comiteJunta');
    Route::get('form_borrado_documento_estructura/{iddept}', 'EstructuraController@form_borrado_documento');
    Route::get('form_borrado_conalunmant/{iddept}', 'EstructuraController@form_borrado_conalunmant');
    Route::get('form_borrado_sitacaant/{iddept}', 'EstructuraController@form_borrado_sitacaant');
    Route::get('form_borrado_zona/{iddept}', 'EstructuraController@form_borrado_zona');
    Route::get('form_borrado_tipodiscapacidad/{iddept}', 'EstructuraController@form_borrado_tipodiscapacidad');
    Route::get('form_borrado_metodologia/{iddept}', 'EstructuraController@form_borrado_metodologia');
    Route::get('form_borrado_estrato/{iddept}', 'EstructuraController@form_borrado_estrato');
    Route::get('form_borrado_resguardo/{iddept}', 'EstructuraController@form_borrado_resguardo');
    Route::get('form_borrado_jornada/{iddept}', 'EstructuraController@form_borrado_jornada');

    Route::post('borrar_departamento', 'EstructuraController@borrar_departamento');
    Route::post('borrar_tipodocumento', 'EstructuraController@borrar_tipodocumento');
    Route::post('borrar_comiteJunta', 'EstructuraController@borrar_comiteJunta');
    Route::post('borrar_documento_estructura', 'EstructuraController@borrar_documento');
    Route::post('borrar_conalunmant', 'EstructuraController@borrar_conalunmant');
    Route::post('borrar_sitacaant', 'EstructuraController@borrar_sitacaant');
    Route::post('borrar_zona', 'EstructuraController@borrar_zona');
    Route::post('borrar_tipodiscapacidad', 'EstructuraController@borrar_tipodiscapacidad');
    Route::post('borrar_metodologia', 'EstructuraController@borrar_metodologia');
    Route::post('borrar_estrato', 'EstructuraController@borrar_estrato');
    Route::post('borrar_resguardo', 'EstructuraController@borrar_resguardo');
    Route::post('borrar_jornada', 'EstructuraController@borrar_jornada');


    Route::post('editar_departamento	', 'EstructuraController@editar_departamento');
    Route::post('editar_tipodocumento	', 'EstructuraController@editar_tipodocumento');
    Route::post('editar_comiteJunta', 'EstructuraController@editar_comiteJunta');
    Route::post('editar_conalunmant	', 'EstructuraController@editar_conalunmant');
    Route::post('editar_sitacaant	', 'EstructuraController@editar_sitacaant');
    Route::post('editar_zona ', 'EstructuraController@editar_zona');
    Route::post('editar_tipodiscapacidad ', 'EstructuraController@editar_tipodiscapacidad');
    Route::post('editar_metodologia ', 'EstructuraController@editar_metodologia');
    Route::post('editar_estrato ', 'EstructuraController@editar_estrato');
    Route::post('editar_resguardo ', 'EstructuraController@editar_resguardo');
    Route::post('editar_jornada ', 'EstructuraController@editar_jornada');

    Route::post('editar_usuario', 'UsuariosController@editar_usuario');
    Route::post('editar_usuario2', 'UsuariosController@editar_usuario2');

    Route::post('buscar_usuario', 'UsuariosController@buscar_usuario');
    Route::post('borrar_usuario', 'UsuariosController@borrar_usuario');
    Route::post('editar_acceso', 'UsuariosController@editar_acceso');
    Route::post('subir_anexo', 'MatriculaController@subir_anexo');

    Route::post('crear_rol', 'UsuariosController@crear_rol');
    Route::post('crear_permiso', 'UsuariosController@crear_permiso');
    Route::post('asignar_permiso', 'UsuariosController@asignar_permiso');
    Route::get('quitar_permiso/{idrol}/{idper}', 'UsuariosController@quitar_permiso');


    Route::get('form_nuevo_usuario', 'UsuariosController@form_nuevo_usuario');
    Route::get('form_nuevo_rol', 'UsuariosController@form_nuevo_rol');
    Route::get('form_nuevo_permiso', 'UsuariosController@form_nuevo_permiso');
    Route::get('form_nuevo_departamento', 'EstructuraController@form_nuevo_departamento');
    Route::get('form_nueva_subregion', 'EstructuraController@form_nueva_subregion');
    Route::get('form_nuevo_tipodocumento', 'EstructuraController@form_nuevo_tipodocumento');
    Route::get('form_nuevo_comiteJunta', 'EstructuraController@form_nuevo_comiteJunta');
    Route::get('form_nuevo_documento', 'EstructuraController@form_nuevo_documento');
    Route::get('form_nuevo_conalunmant', 'EstructuraController@form_nuevo_conalunmant');
    Route::get('form_nuevo_sitacaant', 'EstructuraController@form_nuevo_sitacaant');
    Route::get('form_nuevo_zona', 'EstructuraController@form_nuevo_zona');
    Route::get('form_nuevo_tipodiscapacidad', 'EstructuraController@form_nuevo_tipodiscapacidad');
    Route::get('form_nuevo_metodologia', 'EstructuraController@form_nuevo_metodologia');
    Route::get('form_nuevo_estrato', 'EstructuraController@form_nuevo_estrato');
    Route::get('form_nuevo_resguardo', 'EstructuraController@form_nuevo_resguardo');
    Route::get('form_nuevo_jornada', 'EstructuraController@form_nuevo_jornada');




    Route::get('form_editar_usuario/{id}', 'UsuariosController@form_editar_usuario');
    Route::get('form_editar_usuario2/{id}', 'UsuariosController@form_editar_usuario2');
    Route::get('confirmacion_borrado_usuario/{idusuario}', 'UsuariosController@confirmacion_borrado_usuario');
    Route::get('asignar_rol/{idusu}/{idrol}', 'UsuariosController@asignar_rol');
    Route::get('quitar_rol/{idusu}/{idrol}', 'UsuariosController@quitar_rol');
    Route::get('form_borrado_usuario/{idusu}', 'UsuariosController@form_borrado_usuario');

    Route::get('borrar_rol/{idrol}', 'UsuariosController@borrar_rol');



    //NEW VICTIMA CONFLICTO...................................................................................................
//->middleware('shinobi:administrador_sistema/RUTA A VISTAS')
    Route::post('crear_victimaconflicto', 'EstructuraController@crear_victimaconflicto');
    Route::post('editar_victimaconflicto	', 'EstructuraController@editar_victimaconflicto');
    Route::post('borrar_victimaconflicto', 'EstructuraController@borrar_victimaconflicto');

//NEW comite o junta editar estructura
Route::post('crear_comiteJunta', 'EstructuraController@crear_comiteJunta');
Route::post('editar_comiteJunta	', 'EstructuraController@editar_comiteJunta');
Route::post('borrar_comiteJunta', 'EstructuraController@borrar_comiteJunta');
Route::post('crear_documento', 'EstructuraController@crear_documento');
Route::post('editar_documento_estructura', 'EstructuraController@editar_documento');
Route::post('borrar_documento', 'EstructuraController@borrar_documento');

//->middleware('shinobi:administrador_sistema/RUTA A FORMULARIOS')
    Route::get ('form_nuevo_victimaconflicto', 'EstructuraController@form_nuevo_victimaconflicto');
    Route::get ('form_editar_victimaconflicto/{id}', 'EstructuraController@form_editar_victimaconflicto');
    Route::get ('form_borrado_victimaconflicto/{iddept}', 'EstructuraController@form_borrado_victimaconflicto');
    Route::post('f_editar_victimaconflicto', 'EstructuraController@f_editar_victimaconflicto');

    //NEW CAPACIDAD EXCEPCIONAL...................................................................................................
    //->middleware('shinobi:administrador_sistema/RUTA A VISTAS')
    Route::post('crear_capacidadexcepcional', 'EstructuraController@crear_capacidadexcepcional');
    Route::post('editar_capacidadexcepcional	', 'EstructuraController@editar_capacidadexcepcional');
    Route::post('borrar_capacidadexcepcional', 'EstructuraController@borrar_capacidadexcepcional');
    //->middleware('shinobi:administrador_sistema/RUTA A FORMULARIOS')
    Route::get ('form_nuevo_capacidadexcepcional', 'EstructuraController@form_nuevo_capacidadexcepcional');
    Route::get ('form_editar_capacidadexcepcional/{id}', 'EstructuraController@form_editar_capacidadexcepcional');
    Route::get ('form_borrado_capacidadexcepcional/{iddept}', 'EstructuraController@form_borrado_capacidadexcepcional');
//    Route::post('f_editar_capacidadexcepcional', 'EstructuraController@f_editar_capacidadexcepcional');

    //NEW CAPACIDAD ETNIA...................................................................................................
       //->middleware('shinobi:administrador_sistema/RUTA A VISTAS')
    Route::post('crear_etnia', 'EstructuraController@crear_etnia');
    Route::post('editar_etnia	', 'EstructuraController@editar_etnia');
    Route::post('borrar_etnia', 'EstructuraController@borrar_etnia');
   //->middleware('shinobi:administrador_sistema/RUTA A FORMULARIOS')
    Route::get ('form_nuevo_etnia', 'EstructuraController@form_nuevo_etnia');
    Route::get ('form_editar_etnia/{id}', 'EstructuraController@form_editar_etnia');
    Route::get ('form_borrado_etnia/{iddept}', 'EstructuraController@form_borrado_etnia');
  //  Route::post('f_editar_etnia', 'EstructuraController@f_editar_etnia');

    //NEW NIVEL Que hará foreignkeys con la tabla Grado...................................................................................................
   //->middleware('shinobi:administrador_sistema/RUTA A VISTAS')
    Route::post('crear_nivel', 'EstructuraController@crear_nivel');
    Route::post('editar_nivel	', 'EstructuraController@editar_nivel');
    Route::post('borrar_nivel', 'EstructuraController@borrar_nivel');
   //->middleware('shinobi:administrador_sistema/RUTA A FORMULARIOS')
    Route::get ('form_nuevo_nivel', 'EstructuraController@form_nuevo_nivel');
    Route::get ('form_editar_nivel/{id}', 'EstructuraController@form_editar_nivel');
    Route::get ('form_borrado_nivel/{iddept}', 'EstructuraController@form_borrado_nivel');
  //  Route::post('f_editar_nivel', 'EstructuraController@f_editar_nivel');


    //NEW NivelMediaTotal Que hará foreignkeys con la tabla Grado...................................................................................................
   //->middleware('shinobi:administrador_sistema/RUTA A VISTAS')
    Route::post('crear_nivelmediatotal', 'EstructuraController@crear_nivelmediatotal');
    Route::post('editar_nivelmediatotal	', 'EstructuraController@editar_nivelmediatotal');
    Route::post('borrar_nivelmediatotal', 'EstructuraController@borrar_nivelmediatotal');
   //->middleware('shinobi:administrador_sistema/RUTA A FORMULARIOS')
    Route::get ('form_nuevo_nivelmediatotal', 'EstructuraController@form_nuevo_nivelmediatotal');
    Route::get ('form_editar_nivelmediatotal/{id}', 'EstructuraController@form_editar_nivelmediatotal');
    Route::get ('form_borrado_nivelmediatotal/{iddept}', 'EstructuraController@form_borrado_nivelmediatotal');
  //  Route::post('f_editar_nivelmediatotal', 'EstructuraController@f_editar_nivelmediatotal');


    //NEW NivelCine Que hará foreignkeys con la tabla Grado...................................................................................................
   //->middleware('shinobi:administrador_sistema/RUTA A VISTAS')
    Route::post('crear_nivelcine', 'EstructuraController@crear_nivelcine');
    Route::post('editar_nivelcine	', 'EstructuraController@editar_nivelcine');
    Route::post('borrar_nivelcine', 'EstructuraController@borrar_nivelcine');
   //->middleware('shinobi:administrador_sistema/RUTA A FORMULARIOS')
    Route::get ('form_nuevo_nivelcine', 'EstructuraController@form_nuevo_nivelcine');
    Route::get ('form_editar_nivelcine/{id}', 'EstructuraController@form_editar_nivelcine');
    Route::get ('form_borrado_nivelcine/{iddept}', 'EstructuraController@form_borrado_nivelcine');
  //  Route::post('f_editar_nivelcine', 'EstructuraController@f_editar_nivelcine');


  //NEW Grado Que hará foreignkeys con la tabla nivel, nivelmediatotal, nivelcine
  Route::post('crear_grado', 'EstructuraController@crear_grado');
  Route::post('editar_grado	', 'EstructuraController@editar_grado');
  Route::post('borrar_grado', 'EstructuraController@borrar_grado');
  //->middleware('shinobi:administrador_sistema/RUTA A FORMULARIOS')
  Route::get ('form_nuevo_grado', 'EstructuraController@form_nuevo_grado');
  Route::get ('form_editar_grado/{id}', 'EstructuraController@form_editar_grado');
  Route::get ('form_borrado_grado/{iddept}', 'EstructuraController@form_borrado_grado');

  //NEW SUBREGIÓN.......................................................................................
  //->middleware('shinobi:administrador_sistema/RUTA A VISTAS')
  Route::post('crear_subregion', 'EstructuraController@crear_subregion');
  Route::post('editar_subregion ', 'EstructuraController@editar_subregion');
  Route::post('borrar_subregion', 'EstructuraController@borrar_subregion');
  //->middleware('shinobi:administrador_sistema/RUTA A FORMULARIOS')
  Route::get('form_nuevo_subregion', 'EstructuraController@form_nuevo_subregion');
  Route::get('form_editar_subregion/{id}', 'EstructuraController@form_editar_subregion');
  Route::get('form_borrado_subregion/{iddept}', 'EstructuraController@form_borrado_subregion');


  //NEW Municipio.......................................................................................
  //->middleware('shinobi:administrador_sistema/RUTA A VISTAS')
  Route::post('crear_municipio', 'EstructuraController@crear_municipio');
  Route::post('editar_municipio ', 'EstructuraController@editar_municipio');
  Route::post('borrar_municipio', 'EstructuraController@borrar_municipio');
  //->middleware('shinobi:administrador_sistema/RUTA A FORMULARIOS')
  Route::get('form_nuevo_municipio', 'EstructuraController@form_nuevo_municipio');
  Route::get('form_editar_municipio/{id}', 'EstructuraController@form_editar_municipio');
  Route::get('form_borrado_municipio/{iddept}', 'EstructuraController@form_borrado_municipio');

  Route::get('form_borrado_anexo/{ano_info}/{mes_corte}', 'MatriculaController@form_borrado_anexo');
  Route::post('borrar_anexo', 'MatriculaController@borrar_anexo');
  Route::get('form_borrado_archivos_due/{ano_info}/{mes_corte}', 'DueController@form_borrado_archivos_due');
  Route::post('borrar_archivos_due', 'DueController@borrar_archivos_due');
  Route::post('borrar_archivos_due_nacional', 'DueNacionalController@borrar_archivos_due_nacional');

  Route::post('borrar_archivos_mesa', 'MesaController@borrar_archivos_mesa');
  Route::get('form_borrado_archivos_mesa/{ano_info}/{mes_corte}', 'MesaController@form_borrado_archivos_mesa');
  //Graficas de Consulta Consulta Basica Matricula
  Route::get('grafica_matriculas/{anio}/{sector}/{calendario}/{subregion}/{municipio}/{estableselect}/{sedesselect}', 'ConsultaBasicaMatriculaController@registro_matriculas');
  Route::get('grafica_matriculas_zona/{zona}/{contra}/{grado}/{victi}/{anio}/{subregion}/{municipio}/{estableselect}/{sedesselect}', 'ConsultaBasicaMatriculaController@vistas_matriculas_zona');
  Route::post('ConsultaBasica.fetch', 'GraficasController@fetch')->name('ConsultaBasica.fetch');

  Route::get('vistas_matriculas_zona', 'ConsultaBasicaMatriculaController@vistas_matriculas_zona');

  //Graficas de Consulta Consulta Basica DUE
  Route::post('ConsultaBasica.esta', 'GraficasController@_fetchesta')->name('ConsultaBasica.esta');
  Route::post('ConsultaBasica.sedes', 'GraficasController@_fetchsedes')->name('ConsultaBasica.sedes');
  Route::post('ConsultaBasica.meses', 'GraficasController@_fetchmeses')->name('ConsultaBasica.meses');
  Route::get('grafica_due1/{estable}/{anio_corte}/{mes_corte}', 'ConsultaBasicaDueController@vistas_due_grafica1');
  Route::get('grafica_due2/{sede}/{estableselect}/{anio_corte}/{mes_corte}', 'ConsultaBasicaDueController@vistas_due_grafica2');
  Route::get('datatable.sedes', 'ConsultaBasicaDueController@vistas_due_listasedes')->name('datatable.sedes');


  //NEW RectorDirectors.......................................................................................
  //->middleware('shinobi:administrador_sistema/RUTA A VISTAS')
  Route::post('crear_rectordirector', 'InformacionController@crear_rectordirector');
  Route::post('editar_rectordirector ', 'InformacionController@editar_rectordirector');
  Route::post('borrar_rectordirector', 'InformacionController@borrar_rectordirector');
  //->middleware('shinobi:administrador_sistema/RUTA A FORMULARIOS')
  Route::get('form_nuevo_rectordirector', 'InformacionController@form_nuevo_rectordirector');
  Route::get('form_editar_rectordirector/{id}', 'InformacionController@form_editar_rectordirector');
  Route::get('form_borrado_rectordirector/{iddept}', 'InformacionController@form_borrado_rectordirector');
    //->middleware('shinobi:administrador_sistema/RUTA A Listado de rectordirector')
  Route::get('/listado_rectordirector', 'InformacionController@listado_rectordirector')->name('datatable.rectordirector');

  //Origen dispositivo
  Route::post('crear_origen', 'EstructuraInventarioController@crear_origen');
  Route::post('editar_origen', 'EstructuraInventarioController@editar_origen');
  Route::post('borrar_origen', 'EstructuraInventarioController@borrar_origen');

  Route::get('form_nuevo_origen', 'EstructuraInventarioController@form_nuevo_origen');
  Route::get('form_editar_origen/{id}', 'EstructuraInventarioController@form_editar_origen');
  Route::get('form_borrado_origen/{idorigen}', 'EstructuraInventarioController@form_borrado_origen');

  //Tipo de dispositivo
  Route::post('crear_tipodispositivo', 'EstructuraInventarioController@crear_tipodispositivo');
  Route::post('editar_tipodispositivo', 'EstructuraInventarioController@editar_tipodispositivo');
  Route::post('borrar_tipodispositivo', 'EstructuraInventarioController@borrar_tipodispositivo');

  Route::get('form_nuevo_tipodispositivo', 'EstructuraInventarioController@form_nuevo_tipodispositivo');
  Route::get('form_editar_tipodispositivo/{id}', 'EstructuraInventarioController@form_editar_tipodispositivo');
  Route::get('form_borrado_tipodispositivo/{idtipo}', 'EstructuraInventarioController@form_borrado_tipodispositivo');

  //Proyecto
  Route::post('crear_proyecto', 'EstructuraInventarioController@crear_proyecto');
  Route::post('editar_proyecto', 'EstructuraInventarioController@editar_proyecto');
  Route::post('borrar_proyecto', 'EstructuraInventarioController@borrar_proyecto');

  Route::get('form_nuevo_proyecto', 'EstructuraInventarioController@form_nuevo_proyecto');
  Route::get('form_editar_proyecto/{id}', 'EstructuraInventarioController@form_editar_proyecto');
  Route::get('form_borrado_proyecto/{idproyecto}', 'EstructuraInventarioController@form_borrado_proyecto');

  //Contrato
  Route::post('crear_contrato', 'EstructuraInventarioController@crear_contrato');
  Route::post('editar_contrato', 'EstructuraInventarioController@editar_contrato');
  Route::post('borrar_contrato', 'EstructuraInventarioController@borrar_contrato');

  Route::get('form_nuevo_contrato', 'EstructuraInventarioController@form_nuevo_contrato');
  Route::get('form_editar_contrato/{id}', 'EstructuraInventarioController@form_editar_contrato');
  Route::get('form_borrado_contrato/{idcontrato}', 'EstructuraInventarioController@form_borrado_contrato');

  //Dispositivos
  Route::get('/listado_dispositivos', 'DispositivoController@listado_dispositivos');
  Route::get('/lista_dispositivos', 'DispositivoController@lista_dispositivos');
  Route::get('/lista_dispositivos_existentes', 'DispositivoController@lista_dispositivos_existentes')->name('datatable.dispositivos');

  Route::post('crear_dispositivo', 'DispositivoController@crear_dispositivo');
  Route::post('editar_dispositivo/{id}', 'DispositivoController@editar_dispositivo');
  Route::post('borrar_dispositivo', 'DispositivoController@borrar_dispositivo');

  Route::get('form_nuevo_dispositivo', 'DispositivoController@form_nuevo_dispositivo');
  Route::get('form_editar_dispositivo/{id}', 'DispositivoController@form_editar_dispositivo');
  Route::get('form_borrado_dispositivo_individual/{id}', 'DispositivoController@form_borrado_dispositivo');

  Route::get('/down/evidencias/{evidencia}', 'DispositivoController@descargarEvidencia');

  //Carga masiva dispositivos
  Route::get('carga_masiva_dispositivos', 'DispositivoController@form_carga_masiva_dispositivos');
  Route::post('subir_carga_dispositivos', 'DispositivoController@subir_archivos');

  Route::get('form_borrado_dispositivos/{num_carga}', 'DispositivoController@form_borrado_dispositivos');
  Route::post('borrar_archivos_dispositivos', 'DispositivoController@borrar_archivos_dispositivos');

  Route::get('/descargar_plantilla_dispositivos', 'DispositivoController@DescargarPlantillaDispositivos');

  //Traslado dispositivos
  Route::get ('traslado_dispositivos', 'TrasladoController@index');
  Route::get('/lista_data_traslados', 'TrasladoController@lista_data_traslados')->name('datatable.traslados');

  Route::post('crear_traslado', 'TrasladoController@crear_traslado');
  Route::post('editar_traslado/{id}', 'TrasladoController@editar_traslado');
  Route::delete('borrar_traslado/{id}', 'TrasladoController@borrar_traslado');

  Route::get('form_nuevo_traslado/{id_dispositivo}', 'TrasladoController@form_nuevo_traslado');
  Route::post('borrar_traslado', 'TrasladoController@borrar_traslado');

  Route::get('form_nuevo_traslado/{id_traslado}', 'TrasladoController@form_nuevo_traslado');
  Route::get('form_editar_traslado/{id_traslado}', 'TrasladoController@form_editar_traslado');
  Route::get('form_borrado_traslado/{id_traslado}', 'TrasladoController@form_borrado_traslado');



  Route::post('crear_solicitud', 'SolicitudesController@crear_solicitud');

  Route::post('borrar_solicitud', 'SolicitudesController@borrar_solicitud');
  Route::post('eliminaSolicitud', 'SolicitudesController@eliminaSolicitud');

  Route::get('form_nueva_solicitud', 'SolicitudesController@form_nueva_solicitud');

  Route::get('form_borrado_solicitud/{id_solicitud}', 'SolicitudesController@form_borrado_solicitud');

  Route::get('/down/solicitudes/{solicitud}', 'SolicitudesController@descargarSolicitud');

//
  Route::get('/requerimientos/{codigo_establecimiento}/establecimiento', 'SolicitudesController@RegEstablecimiento');
  Route::get('/requerimientos/{codigo_establecimiento}/municipio', 'SolicitudesController@RegMunicipio');
  Route::get('/requerimientos/consult', 'SolicitudesController@RegGen');
  Route::get('/requerimientos/{codigo_sede}/sede', 'SolicitudesController@RegSede');
//


  Route::post('FiltroSede', 'SolicitudesController@FiltroSede');
  Route::post('filtraMunicipiosEstablecimientos', 'SolicitudesController@filtrarMunicipiosEstablecimientos');
  Route::post('form_ingresar_sedesservicio', 'SolicitudesController@form_ingresar_sedesservicio');
  Route::get('form_ingresar_sedesservicio', 'SolicitudesController@form_ingresar_sedesservicio');
  Route::post('/form_nueva_solicitud/Create', 'SolicitudesController@crear_solicitud');


  Route::get('form_nuevo_modal', 'SolicitudesController@form_nuevo_modal');
  Route::post('crear_modal', 'SolicitudesController@crear_modal');
  Route::post('/modal/Create', 'SolicitudesController@crear_modal');
  Route::post('/modal/ingresar_sedesservicios', 'SolicitudesController@ingresar_sedesservicios');
  Route::get('/requerimientos/{id}/view', 'SolicitudesController@verDetalleSolicitud');
  Route::post('/update/solicitudt', 'SolicitudesController@updateSolicitud');
  Route::post('/requerimientos/Respuesta', 'SolicitudesController@RegistrarRespuesta');
  Route::post('vistoRequerimientoo', 'SolicitudesController@vistoRequerimientoo');



//ingraestructura fisica educativa

  Route::get('/informacion', 'ActualizacionFisicaController@listado_informacion');


  Route::get('form_nuevo_sedeslegal', 'ActualizacionFisicaController@form_nuevo_sedeslegal');
  Route::get('form_editar_sedeslegal/{id}', 'ActualizacionFisicaController@form_editar_sedeslegal');
  Route::get('form_borrado_sedeslegal/{iddept}', 'ActualizacionFisicaController@form_borrado_sedeslegal');

  Route::post('crear_sedeslegal', 'ActualizacionFisicaController@crear_sedeslegal');
  Route::post('editar_sedeslegal ', 'ActualizacionFisicaController@editar_sedeslegal');
  Route::post('borrar_sedeslegal', 'ActualizacionFisicaController@borrar_sedeslegal');
  Route::get('/listado_traslados', 'TrasladoController@listado_traslados')->name('datatable.l_traslados');
  Route::get('/listado_sedeslegal', 'ActualizacionFisicaController@listado_sedeslegal')->name('datatable.sedeslegal');


  Route::get('form_nuevo_sedesservicio', 'ActualizacionFisicaController@form_nuevo_sedesservicio');
  Route::get('form_editar_sedesservicio/{id}', 'ActualizacionFisicaController@form_editar_sedesservicio');
  Route::get('form_borrado_sedesservicio/{iddept}', 'ActualizacionFisicaController@form_borrado_sedesservicio');


  Route::get('form_nuevo_riesgo', 'ActualizacionFisicaController@form_nuevo_riesgo');
  Route::get('form_borrado_sedesriesgos/{iddept}', 'ActualizacionFisicaController@form_borrado_sedesriesgos');
  Route::get('form_editar_sedesriesgos/{id}', 'ActualizacionFisicaController@form_editar_sedesriesgos');
  Route::post('borrar_sedesriesgos', 'ActualizacionFisicaController@borrar_sedesriesgos');
  Route::post('actualizar_riesgos', 'ActualizacionFisicaController@editar_sedesriesgos');
  Route::post('crear_riesgos', 'ActualizacionFisicaController@crear_riesgos');







  Route::get('form_nuevo_sedesservicio', 'ActualizacionFisicaController@form_nuevo_sedesservicio');

  Route::post('crear_sedesservicio', 'ActualizacionFisicaController@crear_sedesservicio');
  Route::post('editar_sedesservicio ', 'ActualizacionFisicaController@editar_sedesservicio');
  Route::post('borrar_sedesservicio', 'ActualizacionFisicaController@borrar_sedesservicio');

  Route::get('/listado_sedesservicio', 'ActualizacionFisicaController@listado_sedesservicio')->name('datatable.sedesservicio');
  Route::get('/listado_sedesriesgos', 'ActualizacionFisicaController@listado_sedesriesgos')->name('datatable.sedesriesgos');


  Route::get('form_nuevo_sedesotros', 'ActualizacionFisicaController@form_nuevo_sedesotros');
  Route::get('form_editar_sedesotros/{id}', 'ActualizacionFisicaController@form_editar_sedesotros');
  Route::get('form_borrado_sedesotros/{iddept}', 'ActualizacionFisicaController@form_borrado_sedesotros');

  Route::get('form_nuevo_sedesaulas', 'ActualizacionFisicaController@form_nuevo_sedesaulas');
  Route::get('form_editar_sedesaulas/{id}', 'ActualizacionFisicaController@form_editar_sedesaulas');


  Route::get('form_nuevo_sedesbaterias', 'ActualizacionFisicaController@form_nuevo_sedesbaterias');
  Route::get('form_editar_sedesbaterias/{id}', 'ActualizacionFisicaController@form_editar_sedesbaterias');
  Route::get('form_borrado_sedesbaterias/{iddept}', 'ActualizacionFisicaController@form_borrado_sedesbaterias');



  Route::post('crear_sedesespacio', 'ActualizacionFisicaController@crear_sedesespacio');
  Route::post('editar_sedesaulas ', 'ActualizacionFisicaController@editar_sedesaulas');
  Route::post('editar_sedesbaterias ', 'ActualizacionFisicaController@editar_sedesbaterias');
  Route::post('editar_sedesotros ', 'ActualizacionFisicaController@editar_sedesotros');
  Route::get('form_borrado_sedesespacios/{iddept}', 'ActualizacionFisicaController@form_borrado_sedesespacios');
  Route::post('borrar_sedesespacio', 'ActualizacionFisicaController@borrar_sedesespacio');

  Route::get('/listado_sedesespacio', 'ActualizacionFisicaController@listado_sedesespacio')->name('datatable.sedesespacio');


  Route::get('/individual', 'ConectividadIndividualController@listado_informacion')->name('datatable.conectividadindividual');
  Route::get('/listado_conectividad', 'ConectividadIndividualController@listado_conectividad')->name('datatable.sede_conectividads');
  Route::get('form_nuevo_sedesconectividad', 'ConectividadIndividualController@form_nuevo_sedesconectividad');
  Route::get('form_editar_sedesconectividad/{id}', 'ConectividadIndividualController@form_editar_sedesconectividad');
  Route::get('form_borrado_sedesconectividad/{iddept}', 'ConectividadIndividualController@form_borrado_sedesconectividad');

  Route::post('crear_sedesconectividad', 'ConectividadIndividualController@crear_sedesconectividad');
  Route::post('editar_sedesconectividad ', 'ConectividadIndividualController@editar_sedesconectividad');
  Route::post('borrar_sedesconectividad', 'ConectividadIndividualController@borrar_sedesconectividad');



  Route::get('form_nuevo_sedesescritura', 'ActualizacionFisicaController@form_nuevo_sedesescritura');
  Route::get('form_editar_sedesescritura/{id}', 'ActualizacionFisicaController@form_editar_sedesescritura');
  Route::get('form_borrado_sedesescritura/{iddept}', 'ActualizacionFisicaController@form_borrado_sedesescritura');

  Route::post('crear_sedesescritura', 'ActualizacionFisicaController@crear_sedesescritura');
  Route::post('editar_sedesescritura ', 'ActualizacionFisicaController@editar_sedesescritura');
  Route::post('borrar_sedesescritura', 'ActualizacionFisicaController@borrar_sedesescritura');

  Route::get('/listado_sedesescritura', 'ActualizacionFisicaController@listado_sedesescritura')->name('datatable.sedesescritura');


//Rutas directorio municipal

Route::get('form_ingresar_alcaldes', 'DirectorioMunicipalController@form_ingresar_alcaldes');
Route::post('ingresar_alcalde', 'DirectorioMunicipalController@ingresar_alcalde');
Route::get('form_ingresar_rectores', 'DirectorioMunicipalController@form_ingresar_rectores');
Route::post('ingresar_rectores', 'DirectorioMunicipalController@ingresar_rectores');
Route::get('form_contactos_rector', 'DirectorioMunicipalController@form_contactos_rector');
Route::post('ingresar_contactos_rector', 'DirectorioMunicipalController@ingresar_contactos_rector');
Route::get('form_ingresar_enlacetic', 'DirectorioMunicipalController@form_ingresar_enlacetic');
Route::post('ingresar_enlacetic', 'DirectorioMunicipalController@ingresar_enlacetic');
Route::get('form_ingresar_secretarioplaneacion', 'DirectorioMunicipalController@form_ingresar_secretarioplaneacion');
Route::post('ingresar_secretarioplaneacion', 'DirectorioMunicipalController@ingresar_secretarioplaneacion');
Route::get('form_ingresar_directornucleo', 'DirectorioMunicipalController@form_ingresar_directornucleo');
Route::post('ingresar_directornucleo', 'DirectorioMunicipalController@ingresar_directornucleo');
Route::get('form_ingresar_comites', 'DirectorioMunicipalController@form_ingresar_comites');
Route::post('ingresar_comites', 'DirectorioMunicipalController@ingresar_comites');
Route::get('form_ingresar_documento', 'DirectorioMunicipalController@form_ingresar_documento');
Route::post('ingresar_documentos', 'DirectorioMunicipalController@ingresar_documentos');
Route::get('/documento/{documento}', 'DirectorioMunicipalController@DescargarDocumento');


Route::get('ver_directorio', 'DirectorioMunicipalController@listado_directorio');
Route::get('listado_documentos', 'DirectorioMunicipalController@listado_documentos');
Route::get('actualizar_directorio', 'DirectorioMunicipalController@actualizar_directorio');
Route::get('/listado_alcaldes', 'DirectorioMunicipalController@listado_alcaldes')->name('datatable.alcalde');
Route::get('/listado_secretarios', 'DirectorioMunicipalController@listado_secretarios')->name('datatable.secretarios');
Route::get('/listado_adminsimat', 'DirectorioMunicipalController@listado_adminsimat')->name('datatable.Adminsimat');
Route::get('/listado_rectores', 'DirectorioMunicipalController@listado_rectores')->name('datatable.rectores');
Route::get('/listado_enlaces_tic', 'DirectorioMunicipalController@listado_enlacestic')->name('datatable.enlacestic');
Route::get('/listado_directornucleo', 'DirectorioMunicipalController@listado_directornucleo')->name('datatable.Jefesnucleos');
Route::get('/listado_SecretarioPlaneacion', 'DirectorioMunicipalController@listado_SecretarioPlaneacion')->name('datatable.Jefesplaneacion');
Route::get('/listado_comitestic', 'DirectorioMunicipalController@listado_comitestic')->name('datatable.comitestic');
Route::get('/listado_jume', 'DirectorioMunicipalController@listado_jume')->name('datatable.jumes');
Route::get('/listado_comitecupos', 'DirectorioMunicipalController@listado_comitecupos')->name('datatable.cupos');

Route::get('/listado_actualizar_alcaldes', 'DirectorioMunicipalController@listado_actualizar_alcaldes')->name('actualizar.alcalde');
Route::get('/listado_actualizar_secretarios', 'DirectorioMunicipalController@listado_actualizar_secretarios')->name('actualizar.secretarios');
Route::get('/listado_actualizar_adminsimat', 'DirectorioMunicipalController@listado_actualizar_adminsimat')->name('actualizar.Adminsimat');
Route::get('/listado_actualizar_rectores', 'DirectorioMunicipalController@listado_actualizar_rectores')->name('actualizar.rectores');
Route::get('/listado_actualizar_enlacestic', 'DirectorioMunicipalController@listado_actualizar_enlacestic')->name('actualizar.enlacestic');
Route::get('/listado_actualizar_directornucleo', 'DirectorioMunicipalController@listado_actualizar_directornucleo')->name('actualizar.Jefesnucleos');
Route::get('/listado_actualizar_SecreatrioPlaneacion', 'DirectorioMunicipalController@listado_actualizar_SecreatrioPlaneacion')->name('actualizar.Jefesplaneacion');
Route::get('/listado_actualizar_comitestic', 'DirectorioMunicipalController@listado_actualizar_comitestic')->name('actualizar.comitestic');
Route::get('/listado_actualizar_jume', 'DirectorioMunicipalController@listado_actualizar_jume')->name('actualizar.jumes');
Route::get('/listado_actualizar_comitecupos', 'DirectorioMunicipalController@listado_actualizar_comitecupos')->name('actualizar.cupos');

Route::get('form_editar_alcaldes/{id}', 'DirectorioMunicipalController@form_editar_alcaldes');
Route::post('editar_alcaldes', 'DirectorioMunicipalController@editar_alcaldes');
Route::get('form_editar_secretarios/{id}', 'DirectorioMunicipalController@form_editar_secretarios');
Route::post('editar_secretarios', 'DirectorioMunicipalController@editar_secretarios');
Route::get('form_editar_rectores/{id}', 'DirectorioMunicipalController@form_editar_rectores');
Route::post('editar_rectores', 'DirectorioMunicipalController@editar_rectores');
Route::get('form_editar_adminsimat/{id}', 'DirectorioMunicipalController@form_editar_adminsimat');
Route::post('editar_adminsimat', 'DirectorioMunicipalController@editar_adminsimat');
Route::get('form_editar_enlacestic/{id}', 'DirectorioMunicipalController@form_editar_enlacestic');
Route::post('editar_enlacestic', 'DirectorioMunicipalController@editar_enlacestic');
Route::get('form_editar_directornucleo/{id}', 'DirectorioMunicipalController@form_editar_directornucleo');
Route::post('editar_directornucleo', 'DirectorioMunicipalController@editar_directornucleo');
Route::get('form_editar_secretarioplaneacion/{id}', 'DirectorioMunicipalController@form_editar_secretarioplaneacion');
Route::post('editar_secretarioplaneacion', 'DirectorioMunicipalController@editar_secretarioplaneacion');
Route::get('form_editar_comitestic/{id}', 'DirectorioMunicipalController@form_editar_comitestic');
Route::post('editar_comitestic', 'DirectorioMunicipalController@editar_comitestic');
Route::get('form_editar_jume/{id}', 'DirectorioMunicipalController@form_editar_jume');
Route::post('editar_jume', 'DirectorioMunicipalController@editar_jume');
Route::get('form_editar_comitescupos/{id}', 'DirectorioMunicipalController@form_editar_comitescupos');
Route::post('editar_comitescupos', 'DirectorioMunicipalController@editar_comitescupos');
Route::get('form_editar_documentos/{id}', 'DirectorioMunicipalController@form_editar_documentos');
Route::post('editar_documentos', 'DirectorioMunicipalController@editar_documentos');



Route::get('form_borrar_alcalde/{iddept}', 'DirectorioMunicipalController@form_borrado_alcalde');
Route::post('borrar_alcalde', 'DirectorioMunicipalController@borrar_alcalde');
Route::get('form_borrar_rectores/{iddept}', 'DirectorioMunicipalController@form_borrar_rectores');
Route::post('borrar_rectores', 'DirectorioMunicipalController@borrar_rectores');
Route::get('form_borrado_enlacestic/{iddept}', 'DirectorioMunicipalController@form_borrado_enlacestic');
Route::post('borrado_enlacestic', 'DirectorioMunicipalController@borrar_enlacestic');
Route::get('form_borrado_directornucleo/{iddept}', 'DirectorioMunicipalController@form_borrado_directornucleo');
Route::post('borrado_jefesnucleos', 'DirectorioMunicipalController@borrar_directornucleo');
Route::get('form_borrado_secretarioplaneacion/{iddept}', 'DirectorioMunicipalController@form_borrado_secretarioplaneacion');
Route::post('borrado_SecretarioPlaneacion', 'DirectorioMunicipalController@borrado_SecretarioPlaneacion');
Route::get('form_borrado_comitestic/{iddept}', 'DirectorioMunicipalController@form_borrado_comitestic');
Route::post('borrado_comitestic', 'DirectorioMunicipalController@borrado_comitestic');
Route::get('form_borrado_comitestic/{iddept}', 'DirectorioMunicipalController@form_borrado_comitestic');
Route::post('borrado_comitestic', 'DirectorioMunicipalController@borrado_comitestic');
Route::get('form_borrado_jume/{iddept}', 'DirectorioMunicipalController@form_borrado_jume');
Route::post('borrado_jume', 'DirectorioMunicipalController@borrado_jume');
Route::get('form_borrado_comitescupos/{iddept}', 'DirectorioMunicipalController@form_borrado_comitescupos');
Route::post('borrado_comitescupos', 'DirectorioMunicipalController@borrado_comitescupos');
Route::get('form_borrado_documentos/{iddept}', 'DirectorioMunicipalController@form_borrado_documentos');
Route::post('borrado_documentos', 'DirectorioMunicipalController@borrado_documentos');

  //Acerca de
  Route::get('/acerca_de', 'HomeController@acercade');

  //Rutas de los Reportes
  Route::get('/reportes', 'ReporterConroller@index');
  Route::post('reportesGenerator', 'ReporterConroller@genReport');
  Route::post('filtraMunicipios', 'ReporterConroller@filtrarMunicipios');
  Route::post('filtraAnio', 'ReporterConroller@filtrarAnioCorte');
  Route::post('filtraMes', 'ReporterConroller@filtrarMesCorte');
  Route::get('/reportes1', 'ReporterConroller@reportEnsayo');

  //Rutas solicitudes Infraestructura Fisica Educativa
  Route::get('validacion', 'RequerimientoifController@modal_infrafisica');
  Route::get('/requerimientosIFisica', 'RequerimientoifController@index');

  // Route::post('/requerimientosIFisica', 'RequerimientoifController@validacion_riesgos');
  Route::get('/requerimientosIFisica', 'RequerimientoifController@index');
  Route::get('/requerimientosIFisica/consult', 'RequerimientoifController@RegGen');
  Route::post('/requerimientosIFisica/Create', 'RequerimientoifController@RegistrarReqSol');
  Route::post('/requerimientosIFisica/Respuesta', 'RequerimientoifController@RegistrarRespuesta');
  Route::get('/requerimientosIFisica/{codigo_establecimiento}/establecimiento', 'RequerimientoifController@RegEstablecimiento');
  Route::get('/requerimientosIFisica/{codigo_establecimiento}/municipio', 'RequerimientoifController@RegMunicipio');
  Route::get('/requerimientosIFisica/{codigo_sede}/sede', 'RequerimientoifController@RegSede');
  Route::get('/down/{anexo}', 'RequerimientoifController@DescargaAnexo');
  Route::get('/elimnaSolicitudIF/{id}', 'RequerimientoifController@formEliminaSolicitudIF');
  Route::post('eliminaSolicitudIF', 'RequerimientoifController@eliminaSolicitudIF');
  Route::post('eliminaSolicitudIFDelete', 'RequerimientoifController@eliminaSolicitudIFDelete');
  Route::post('/update/solicitud', 'RequerimientoifController@updateSolicitud');
  Route::get('/requerimientosIFisica/{id}/view', 'RequerimientoifController@verDetalleSolicitud');
  Route::post('form_ingresar_riesgo', 'RequerimientoifController@form_ingresar_riesgo');
  Route::get('form_ingresar_riesgo', 'RequerimientoifController@form_ingresar_riesgo');
  Route::post('/requerimientosIFisica/ingresar_riesgos', 'RequerimientoifController@ingresar_riesgos');
//
Route::get('validar', 'RequerimientoifController@validar');
//
  // Route::get('/filtSede', 'RequerimientoifController@FiltroSede');
  Route::post('filtSede', 'RequerimientoifController@FiltroSede');
  Route::post('vistoRequerimiento', 'RequerimientoifController@vistoRequerimiento');
  Route::post('EspaciosSede', 'RequerimientoifController@EspaciosSede');
  Route::post('filtraMunicipiosEstablecimientos', 'RequerimientoifController@filtrarMunicipiosEstablecimientos');






  // Route::get('/EspaciosSede', 'RequerimientoifController@EspaciosSede');

  Route::post('subir_archivos_conectividad', 'ConectividadController@subir_archivos');
  Route::get('/cargar_datos_conectividad', 'ConectividadController@index');
  Route::get('/conectividad_individual', 'ConectividadController@index');
  Route::post('borrar_archivos_conectividad', 'ConectividadController@borrar_archivos_conectividad');
  Route::get('/form_borrado_archivos_conectividad/{ano_info}/{mes_corte}', 'ConectividadController@form_borrado_archivos_conectividad');

  Route::get('/descargar_plantilla_conectividad', 'ConectividadController@DescargarPlantillaConectividad');

  Route::post('subir_archivos_calidad', 'CalidadController@subir_archivos');
  Route::get('/cargar_datos_calidad', 'CalidadController@index');
  Route::post('borrar_archivos_calidad', 'CalidadController@borrar_archivos_calidad');
  Route::get('/form_borrado_archivos_calidad/{ano_info}/{mes_corte}', 'CalidadController@form_borrado_archivos_calidad');
  Route::get('/editar_estructura_inventario', 'EstructuraInventarioController@listar_estructura_inventario');

  Route::get('/descargar_plantilla_calidad', 'CalidadController@DescargarPlantillaCalidad');

  //Fitros para sedes
  Route::post('/municipios_subregion', 'FiltrosController@municipios_subregion')->name('municipios_subregion');
  Route::post('/establecimientos_municipio', 'FiltrosController@establecimientos_municipio')->name('establecimientos_municipio');
  Route::post('/sedes_establecimientos', 'FiltrosController@sedes_establecimientos')->name('sedes_establecimientos');

});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
