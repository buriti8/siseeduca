<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Contrato;

class Proyecto extends Model
{
    protected $table = "proyectos";
    protected $fillable = ["nombre", "plan_desarrollo", "periodo"];

    public function contratos()
    {
        return $this->hasMany('App\Contrato');
    }

}
