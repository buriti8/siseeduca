<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Dispositivo extends Model
{
    protected $fillable = ['sede_id', 'tipo_id', 'serial', 'origen_id', 'acta', 'radicado', 'inversion', 'proveedor', 'marca', 'referencia', 'fecha_entrega', 'fecha_fin_garantia', 
                          'numero_factura','contrato_id', 'procesador', 'sistema_operativo', 'memoria_ram', 'disco_duro', 'estado', 'observaciones', 'motivo', 'evidencia', 'fecha_baja'];
    
    public function tipodispositivo()
    {
        return $this->belongsTo('App\TipoDispositivo');
    }

    public function origen()
    {
        return $this->belongsTo('App\OrigenDispositivo');
    }

    public function contrato()
    {
        return $this->belongsTo('App\Contrato');
    }

    public function translados()
    {
        return $this->hasMany('App\Translado');
    }

    public function temphistoricosedes()
    {        
        return $this->belongsTo('App\TempHistoricoSede', 'sede_id', 'codigo_sede');
    }


}
