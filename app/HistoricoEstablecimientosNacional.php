<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HistoricoEstablecimientosNacional extends Model
{
  protected $table = 'historico_establecimientos_nacional';

  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
      'codigo_dane_municipio','nombre_establecimiento','codigo_establecimiento',
  ];

public function Sedes()
{
   return $this->hasMany('App\HistoricoSedesNacional', 'codigo_establecimiento', 'codigo_establecimiento');
}
}
