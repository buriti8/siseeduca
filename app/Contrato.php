<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Proyecto;

class Contrato extends Model
{
    protected $table = "contratos";
    protected $fillable = ["numero_contrato",
                            "objeto_contrato",
                            "tipo_contrato",
                            "proyecto_id",
                            "fecha_acta_inicio",
                            "fecha_terminacion_contrato",
                            "nit_contratista",
                            "nombre_contratista",
                            "telefono_contratista",
                            "valor_total_contrato",
                            "dependencia_contratante",
                            "telefono_mesa_ayuda",
                            "observaciones"];

    public function proyecto()
    {
        return $this->belongsTo('App\Proyecto');
    }

    public function dispositivos()
    {
        return $this->hasMany('App\Dispositivo');
    }
}
