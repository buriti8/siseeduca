<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SedesEspacio extends Model
{
  protected $fillable = ['id','DaneSede','NombreSede','AulasPreescolar','AulasPrimaria','AulasSecundaria','AulasEspaciales','Biblioteca','AulasSistemas','AulasBilinguismo','Laboratorio','AulasTalleres','AulasMultiples','Cocinas','Comedores','SantiriosHombres','SanitariosMujeres','LavamanosHombres','LavamanosMujeres','Orinales','Vivienda',
  'Canchas','PlacasMulti','JuegosInfantiles','DaneMunicipio','DaneEstablecimiento','BañosReducida'
  ];

  public function HistoricoSedes()
  {
      return $this->belongsTo('App\temp_historico_sede', 'DaneSede','codigo_sede');
  }
}
