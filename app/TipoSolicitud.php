<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TipoSolicitud extends Model
{
    public function Solicitudes(){
       return $this->hasMany('App\SolicitudEspacio', 'TipoSolicitud', 'id');
   }
}
