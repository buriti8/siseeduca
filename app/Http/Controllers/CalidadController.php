<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Meses;
use App\HistoricoCalidad;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Storage;

/**
 * Class HomeController
 * @package App\Http\Controllers
 */


class CalidadController extends Controller
{
  /**
   * Create a new controller instance.
   *
   * @return void
   */
  public function __construct()
  {
      $this->middleware('auth');
  }
  public function form_borrado_archivos_calidad($ano_info,$mes_corte){

        return view("confirmaciones.form_borrado_archivos_calidad")->with("ano_info",$ano_info)->with("mes_corte",$mes_corte);

      }

      public function borrar_archivos_calidad(Request $request){

            $anio_corte=$request->input("ano_info");
            $mes_corte=$request->input("mes_corte");

            $res_calidad=HistoricoCalidad::where('anio_corte',$anio_corte)->where('mes_corte',$mes_corte)->delete();


            if($res_calidad){
              DB::statement('refresh materialized view registro_archivos_cargados_calidad;');


                 return view("mensajes.msj_borrado");
            }
            else
            {
                return view("mensajes.mensaje_error")->with("msj","..Hubo un error, intentarlo nuevamente..");
            }


    }

    public function validar_header_calidad($archivo){

      $opened_file = fopen($archivo, 'r');
      $header = fgetcsv($opened_file, 0, ';');
      $header_convert= $this->utf8_converter($header);
      $header_convert_trim = array_map('trim', $header_convert);

      $codigo_dane_sede  =  $this->getColumnNameByValue($header_convert_trim,'CODIGO DANE SEDE');
      $sede_educativa  =  $this->getColumnNameByValue($header_convert_trim,'NOMBRE SEDE');
      $tipo_de_canal=$this->getColumnNameByValue($header_convert_trim,'TIPO DE CANAL');
      $disponibilidad_del_servicio=$this->getColumnNameByValue($header_convert_trim,'DISPONIBILIDAD DEL SERVICIO');
      $descuento_disponibilidad_del_servicio=$this->getColumnNameByValue($header_convert_trim,'DESCUENTO DISPONIBILIDAD DEL SERVICIO');
      $latencia =$this->getColumnNameByValue($header_convert_trim,'LATENCIA');
      $descuento_latencia=$this->getColumnNameByValue($header_convert_trim,'DESCUENTO LATENCIA');
      $efectividad_instlacion=$this->getColumnNameByValue($header_convert_trim,'EFECTIVIDAD INSTALACION');
      $descuento_efectividad_instalacion=$this->getColumnNameByValue($header_convert_trim,'DESCUENTO EFECTIVIDAD INSTALACION');
      $velocidad_de_transferencia =$this->getColumnNameByValue($header_convert_trim,'VELOCIDAD TRASFERENCIA');
      $descuento_velocidad_de_transferencia=$this->getColumnNameByValue($header_convert_trim,'DESCUENTO VELOCIDAD TRANSFERENCIA');



      fclose($opened_file);

      $validation_array = [
        'CODIGO DANE SEDE'=>$codigo_dane_sede,
        'NOMBRE SEDE'=>  $sede_educativa,
        'TIPO DE CANAL'=>$tipo_de_canal,
        'DISPONIBILIDAD DEL SERVICIO'=>$disponibilidad_del_servicio,
        'DESCUENTO DISPONIBILIDAD DEL SERVICIO'=>$descuento_disponibilidad_del_servicio,
        'LATENCIA'=>$latencia,
        'DESCUENTO LATENCIA'=>$descuento_latencia,
        'EFECTIVIDAD INSTALACION'=>$efectividad_instlacion,
        'DESCUENTO EFECTIVIDAD INSTALACION'=>$descuento_efectividad_instalacion,
        'VELOCIDAD TRASFERENCIA'=>$velocidad_de_transferencia,
        'DESCUENTO VELOCIDAD TRANSFERENCIA'=>$descuento_velocidad_de_transferencia,


        ];


      return $validation_array;

    }

    public  function utf8_converter($array)
      {
          array_walk_recursive($array, function(&$item, $key){
              if(!mb_detect_encoding($item, 'utf-8', true)){
                      $item = utf8_encode($item);
              }
          });

          return $array;
      }

      public function getColumnNameByValue($array, $value)
          {
              return in_array($value, $array)? $value : '';
          }

    public function carga_masiva_calidad($archivo,$mes,$anio){


      DB::statement("COPY temp_archivo_calidad FROM '$archivo' DELIMITER ';' CSV HEADER;");



                DB::statement("insert into temp_historico_calidad (codigo_dane_sede,sede_educativa,tipo_de_canal,disponibilidad_del_servicio,descuento_disponibilidad_del_servicio,latencia,descuento_latencia,efectividad_instalacion,
                descuento_efectividad_instalacion,velocidad_de_transferencia,descuento_velocidad_de_transferencia) select codigo_dane_sede,sede_educativa,tipo_de_canal,disponibilidad_del_servicio,descuento_disponibilidad_del_servicio,latencia,descuento_latencia,efectividad_instalacion,
                descuento_efectividad_instalacion,velocidad_de_transferencia,descuento_velocidad_de_transferencia from temp_archivo_calidad");

                DB::delete('delete from temp_archivo_calidad');

                DB::statement("update temp_historico_calidad set anio_corte ='$anio', mes_corte = '$mes'  , created_at=now(), updated_at=now();");

                DB::statement('insert into historico_calidad (mes_corte,anio_corte,codigo_dane_sede,sede_educativa,tipo_de_canal,disponibilidad_del_servicio,descuento_disponibilidad_del_servicio,latencia,descuento_latencia,efectividad_instalacion,
                descuento_efectividad_instalacion,velocidad_de_transferencia,descuento_velocidad_de_transferencia,created_at,updated_at) select * from temp_historico_calidad;');

              DB::delete('delete from temp_historico_calidad');
              DB::statement('refresh materialized view registro_archivos_cargados_calidad;');


                 return view("mensajes.mensaje_error")->with("msj","..Hubo un error al agregar ; intentarlo nuevamente..");

  }

      public function subir_archivos(Request $request)
      {

        $reglas=[  'archivo_calidad' => 'required|mimes:csv,txt',



              ];

        $mensajes=[  'archivo_calidad.mimes' => 'El archivo seleccionado calidad no corresponde al formato permitido (CSV,txt) ',


                 ];


        $validator0 = Validator::make( $request->all(),$reglas,$mensajes );
        if( $validator0->fails() ){
            return view("mensajes.mensaje_error")->with("msj","...Existen errores...")
                                                ->withErrors($validator0->errors());
        }

        $mes =$request->input("mes");
        $anio =$request->input("anio");
        $calidad = $request->file('archivo_calidad');


        $nuevo_nombre_calidad ="calidad.csv";


        Storage::disk('temp')->put($nuevo_nombre_calidad,  \File::get($calidad) );
        $archivo_calidad = Storage::disk('temp')->getDriver()->getAdapter()->applyPathPrefix('calidad.csv');

        $reglas_calidad=[

          'CODIGO DANE SEDE'=>'required',
          'NOMBRE SEDE'=>  'required',
          'TIPO DE CANAL'=>'required',
          'DISPONIBILIDAD DEL SERVICIO'=>'required',
          'DESCUENTO DISPONIBILIDAD DEL SERVICIO'=>'required',
          'LATENCIA' => 'required',
          'DESCUENTO LATENCIA'=>'required',
          'EFECTIVIDAD INSTALACION'=>'required',
          'DESCUENTO EFECTIVIDAD INSTALACION'=>'required',
          'VELOCIDAD TRASFERENCIA'=>'required',



         ];


       $mensajes1=[  'required'=>'La columna :attribute no se encuentra en el encabezado del archivo cargado o no está escrito correctamente ',
        ];

       $validator1 = Validator::make( $this->validar_header_calidad($archivo_calidad),$reglas_calidad,$mensajes1 );


   if( $validator1->fails() ){

       return view("mensajes.mensaje_error")->with("msj","...Existen errores en el archivo...")
                                           ->withErrors($validator1->errors());

  }
  $busqueda1= DB::table('registro_archivos_cargados_calidad')->where('anio_corte', $anio)->where('mes_corte', $mes)->count();


  if( $busqueda1>0  ){

    switch($mes) {
        case 1: $mes_letras= "Enero"; break;
        case 2: $mes_letras= "Febrero"; break;
        case 3: $mes_letras= "Marzo"; break;
        case 4: $mes_letras= "Abril"; break;
        case 5: $mes_letras= "Mayo"; break;
        case 6: $mes_letras= "Junio"; break;
        case 7: $mes_letras= "Julio"; break;
        case 8: $mes_letras= "Agosto"; break;
        case 9: $mes_letras= "Septiembre"; break;
        case 10: $mes_letras= "Octubre"; break;
        case 11: $mes_letras= "Noviembre"; break;
        case 12: $mes_letras= "Diciembre"; break;
        default:$mes_letras= "No match!"; break;
    }

      return view("mensajes.mensaje_error")->with("msj","Ya existen registros para el Mes: $mes_letras Año: $anio .");
  }




       $this->carga_masiva_calidad($archivo_calidad,$mes,$anio);
       Storage::disk('temp')->delete( $nuevo_nombre_calidad);



        return view("mensajes.msj_anexo_cargado");

      }

      public function DescargarPlantillaCalidad()
      {
        $filename=public_path().'/plantilla_calidad/calidad.csv';
        $finfo=finfo_open(FILEINFO_MIME_TYPE);
        $mimeType=finfo_file($finfo, $filename);

        header("Content-type:".$mimeType);
        header("Content-Disposition: attachment; filename=calidad.csv");
        echo file_get_contents($filename);
      }

      /**
       * Show the application dashboard.
       *
       * @return Response
       */
      public function index()
      {

          $meses =Meses::all();
          $listado = DB::table('registro_archivos_cargados_calidad')
            ->select('anio_corte', 'mes_corte','fecha','cantidad_calidad')
            ->paginate(12);

          return view("conectividad.cargar_datos_calidad")->with("meses",$meses)->with("listados",$listado);

        }
}
