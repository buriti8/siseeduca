<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use DB;
use App\Http\Controllers\Controller;
use App\User;
use PDO;
use App\HistoricoEstablecimiento;
use App\HistoricoSede;
use Datatables;

class ConsultaBasicaDueController extends Controller
{
  function vistas_due_grafica1($estable,$anio_corte,$mes_corte)
  {
    $establecimiento = HistoricoEstablecimiento::where('codigo_establecimiento', $estable)->where("anio_corte",$anio_corte)->where("mes_corte",$mes_corte)->first();
    // $establecimiento;
      $respuesta1 = '<div class="titleboxes" align="center">
        Información principal de la entidad
      </div>
      <div class="box box-primary" style="border: 1px solid #3c8dbc !important; padding-left:4px;">
        <div class="box-header">
        </div>
      <div class="form-group " name="forms">
        <table class="table table table-hover" style="margin-bottom:0px;">
          <tbody>
            <tr>
              <th>Nombre:</th>
              <td>'.$establecimiento['nombre_establecimiento'].'</td>
            </tr>
          </tbody>
          <tbody>
            <tr>
              <th>Código DANE:</th>
              <td>'.$establecimiento['codigo_establecimiento'].'<td>
            </tr>
          </tbody>
          <tbody>
            <tr>
              <th>Dirección:</th>
              <td>'.$establecimiento['direccion'].'</td>
            </tr>
          </tbody>
          <tbody>
            <tr>
              <th>Teléfono:</th>
              <td>'.$establecimiento['telefono'].'</td>
            </tr>
          </tbody>
          <tbody>
            <tr>
              <th>Departamento:</th>
              <td>'.$establecimiento['departamento'].'</td>
            </tr>
          </tbody>
          <tbody>
            <tr>
              <th>Municipio:</th>
              <td>'.$establecimiento['municipio'].'</td>
            </tr>
          </tbody>
          <tbody>
            <tr>
              <th>Estado:</th>
              <td>'.$establecimiento['estado'].'</td>
            </tr>
          </tbody>
          </table>
          <table class="table table table-hover"  style="margin-bottom:0px;">
            <tbody>
              <tr>
                <th>Calendario:</th>
                <td>'.$establecimiento['calendario'].'</td>
                <th>Sector:</th>
                <td>'.$establecimiento['sector'].'</td>
              </tr>
            </tbody>
          </table>
          <table class="table table table-hover">
              <tbody>
                <tr>
                  <th>Zona EE:</th>
                  <td>'.$establecimiento['zona'].'</td>
                </tr>
              </tbody>
              <tbody>
                <tr>
                  <th>Rector:</th>
                  <td>'.$establecimiento['nombre_rector'].'</td>
                </tr>
              </tbody>
          </table>
      </div>
    </div>';
      $respuesta2 = '<div class="titleboxes">Información complementaria
      </div>
      <div class="box box-primary" style="border: 1px solid #3c8dbc !important; padding-left:4px;">
        <div class="box-header">
        </div>
        <div class="form-group" name="forms">
          <table class="table table table-hover" style="margin-bottom:0px;">
            <tbody>
              <tr>
                <th>Género:</th>
                <td>'.$establecimiento['genero'].'</td>
              </tr>
            </tbody>
            <tbody>
              <tr>
                <th>Carácter:</th>
                <td>'.$establecimiento['caracter'].'<tdh>
              </tr>
            </tbody>
            <tbody>
              <tr>
                <th>Especialidad:</th>
                <td>'.$establecimiento['especialidad'].'</td>
              </tr>
            </tbody>
            <tbody>
              <tr>
                <th>Nivel, Grados:</th>
                <td>'.$establecimiento['grados'].'</td>
              </tr>
            </tbody>
            <tbody>
              <tr>
                <th>Etnias:</th>
                <td>'.$establecimiento['etnias'].'</td>
              </tr>
            </tbody>
            <tbody>
              <tr>
                <th>Discapacidades:</th>
                <td>'.$establecimiento['discapacidades'].'</td>
              </tr>
            </tbody>
            <tbody>
              <tr>
                <th>Capacidad Excepcionales:</th>
                <td>'.$establecimiento['capacidades_excepcionales'].'</td>
              </tr>
            </tbody>
            <tbody>
              <tr>
                <th>Modelos:</th>
                <td>'.$establecimiento['modelos_educativos'].'</td>
              </tr>
            </tbody>
            <tbody>
              <tr>
                <th>Idiomas:</th>
                <td>'.$establecimiento['idiomas'].'</td>
              </tr>
            </tbody>
            <tbody>
              <tr>
                <th>Matricula Contratada:</th>
                <td>'.$establecimiento['matricula_contratada'].'</td>
              </tr>
            </tbody>
            </table>
        </div>
      </div>';
      $datos = array('respuesta1'=>$respuesta1,'respuesta2'=>$respuesta2,);
      return   json_encode($datos,JSON_UNESCAPED_UNICODE);


  }
  function vistas_due_grafica2($sede,$estableselect,$anio_corte,$mes_corte)
  {
    $establecimiento = HistoricoEstablecimiento::where('codigo_establecimiento', $estableselect)->where("anio_corte",$anio_corte)->where("mes_corte",$mes_corte)->first();
    $sede = HistoricoSede::where('codigo_sede', $sede)->first();
    $mensaje;
    // $establecimiento;
      $respuesta1 = '<div class="titleboxes" align="center">
        Información principal de la entidad
      </div>
      <div class="box box-primary" style="border: 1px solid #3c8dbc !important; padding-left:4px;">
        <div class="box-header">
        </div>
      <div class="form-group " name="forms">
        <table class="table table table-hover" style="margin-bottom:0px;">
          <tbody>
            <tr>
              <th>Nombre</th>
              <td>'.$sede['nombre_sede'].'</td>
            </tr>
          </tbody>
          <tbody>
            <tr>
              <th>Código DANE:</th>
              <td>'.$sede['codigo_sede'].'<td>
            </tr>
          </tbody>
          <tbody>
            <tr>
              <th>Dirección:</th>
              <td>'.$sede['direccion'].'</td>
            </tr>
          </tbody>
          <tbody>
            <tr>
              <th>Teléfono:</th>
              <td>'.$sede['telefono'].'</td>
            </tr>
          </tbody>
          <tbody>
            <tr>
              <th>Departamento:</th>
              <td>'.$sede['nombre_departamento'].'</td>
            </tr>
          </tbody>
          <tbody>
            <tr>
              <th>Municipio:</th>
              <td>'.$sede['nombre_municipio'].'</td>
            </tr>
          </tbody>
          <tbody>
            <tr>
              <th>Estado:</th>
              <td>'.$sede['estado_sede'].'</td>
            </tr>
          </tbody>
          </table>
          <table class="table table table-hover"  style="margin-bottom:0px;">
            <tbody>
              <tr>
                <th>Calendario:</th>
                <td>'.$establecimiento['calendario'].'</td>
                <th>Sector:</th>
                <td>'.$establecimiento['sector'].'</td>
              </tr>
            </tbody>
          </table>
          <table class="table table table-hover">
              <tbody>
                <tr>
                  <th>Zona EE:</th>
                  <td>'.$sede['zona'].'</td>
                </tr>
              </tbody>
              <tbody>
                <tr>
                  <th>Rector:</th>
                  <td>'.$establecimiento['nombre_rector'].'</td>
                </tr>
              </tbody>
          </table>
      </div>
    </div>';
      $respuesta2 = '<div class="titleboxes">Información complementaria
      </div>
      <div class="box box-primary" style="border: 1px solid #3c8dbc !important; padding-left:4px;">
        <div class="box-header">
        </div>
        <div class="form-group" name="forms">
          <table class="table table table-hover" style="margin-bottom:0px;">
            <tbody>
              <tr>
                <th>Nivel,Grados:</th>
                <td>'.$sede['grados'].'</td>
              </tr>
            </tbody>
            <tbody>
              <tr>
                <th>Modelos:</th>
                <td>'.$sede['modelos'].'</td>
              </tr>
            </tbody>
            </table>
        </div>
      </div>';
      $datos = array('respuesta1'=>$respuesta1,'respuesta2'=>$respuesta2,);
      return   json_encode($datos,JSON_UNESCAPED_UNICODE);


  }

  function vistas_due_listasedes(Request $request)
  {
      $estable = $request->get('estable');
      $anio = $request->get('anio_corte');
      $mes = $request->get('mes_corte');
      $sedes = DB::table('historico_sedes')
      ->where('codigo_establecimiento',$estable)
      ->where("anio_corte",$anio)
      ->where("mes_corte",$mes)->get();
      $listasedes = '';
      foreach($sedes as $row) {
        $listasedes = '<tr><td><a onclick=" _mostrarsedes('.($row->codigo_sede).')"  class="btn  btn-success btn-xs" data-toggle="tooltip" data-placement="top" title="Ver Información" >'.($row->codigo_sede).'</a></td>
         <td>'.$row->nombre_sede.'</td>
         <td>'.$row->direccion.'</td>
         <td>'.$row->telefono.'</td>
         <td>'.$row->nombre_departamento.'</td>
         <td>'.$row->nombre_municipio.'</td>
         <td>'.$row->estado_sede.'</td></tr>';
         echo $listasedes;
      }



  }



  public function index()
  {
    $anios = DB::table('historico_establecimientos')
                   ->select('anio_corte as anio')
                   ->groupBy('anio_corte')
                   ->orderBy('anio_corte','DESC')
                   ->get();
    $subregions = DB::table('subregions')
                 ->orderBy('id', 'ASC')
                 ->get();
      return view("due.consulta_basica_due")->with("subregions",$subregions)->with("anios",$anios);
  }
}
