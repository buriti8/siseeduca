<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\SedesInfra;
use App\SedesLegal;
use App\SedesServicio;
use App\SedeEscritura;
use App\HistoricoEstablecimiento;
use App\temp_historico_sede;
use App\temp_historico_establecimiento;
use App\SedeConectividad;
use App\SedesEspacio;
use App\Municipio;
use App\HistoricoRiesgos;
use App\Riesgos;
use App\Subregion;
use Illuminate\Support\Facades\Validator;
use Datatables;
use Storage;
use App\SiegaTrait\SedesTrait;
use Illuminate\Support\Facades\Auth;


class ActualizacionFisicaController extends Controller
{


  public function form_nuevo_sedesescritura(){

    $subregiones = Subregion::orderBy('id', 'asc')->get();

    return view("formularios.form_nuevo_sedesescritura")->with("subregiones", $subregiones);

  }


  //función encargada de llamar el "form_nuevo_riesgo" para ingresar la información de los riesgos de la sedes educativas.
  public function form_nuevo_riesgo(){

    $subregiones = Subregion::orderBy('id', 'asc')->get();

    return view("formularios.form_nuevo_riesgo")->with("subregiones", $subregiones);

  }




  //función encargada de guardar en las tablas riesgos y historico_riesgos la información ingresada en el formulario "form_nuevo_riesgo", en la base de datos.
  public function crear_riesgos(Request $request){

    $sede_id=$request->sede_id;
    $existencia = riesgos::select('codigo_dane','nombre_sede')->where('codigo_dane', '=', $sede_id)->get();
    if (count($existencia)==0) {
      $data = new Riesgos;
      $riesgos=$request["riesgos"];
      $array = implode(', ', $riesgos);
      $data->codigo_dane = $request->input("sede_id");
      $nombres = temp_historico_sede::where("codigo_sede","=",$data->codigo_dane)->get(["nombre_sede"]);
      foreach ($nombres as $nombre) {
        $nombre = $nombre->nombre_sede;
      }
      $data->nombre_sede = $nombre;
      $data->riesgos = $array;

      $historico = new HistoricoRiesgos;
      $historico->nombre_sede=$nombre;
      $historico->codigo_dane=$request->input("sede_id");
      $historico->riesgos = $array;
      $historico->save();

      if($data->save())
      {
        return view("mensajes.msj_info_ingresada");
      }
      else
      {
        return view("mensajes.mensaje_error")->with("msj","Hubo un error al guardar, Por favor inténtalo nuevamente.");
      }
    }else {
      return view("mensajes.mensaje_error_actualizacion")->with("nombres",$existencia);
    }
  }


  //función encargada de listar en datatables los riesgos de las sedes según el rol del usuario.

  public function listado_sedesriesgos(){

    if(Auth::user()->isRole('secretarios'))
    {

      $datos = riesgos::join('temp_historico_sedes', 'riesgos.codigo_dane', '=', 'temp_historico_sedes.codigo_sede')
      ->select('temp_historico_sedes.nombre_sede','nombre_establecimiento','riesgos.id','riesgos.riesgos','riesgos.codigo_dane')
      ->where('temp_historico_sedes.codigo_dane_municipio', Auth::user()->name)
      ->get();

    }
    elseif (Auth::user()->isRole('rectores'))
    {
      $datos = riesgos::join('temp_historico_sedes', 'riesgos.codigo_dane', '=', 'temp_historico_sedes.codigo_sede')
      ->select('temp_historico_sedes.nombre_sede','nombre_establecimiento','riesgos.id','riesgos.riesgos','riesgos.codigo_dane')
      ->where('temp_historico_sedes.codigo_establecimiento', Auth::user()->name)
      ->get();
    }
    else
    {

      $datos = riesgos::join('temp_historico_sedes', 'riesgos.codigo_dane', '=', 'temp_historico_sedes.codigo_sede')
      ->select('temp_historico_sedes.nombre_sede','nombre_establecimiento','riesgos.id','riesgos.riesgos','riesgos.codigo_dane')
      ->get();

    }

    return Datatables::of($datos)->make(true);
  }


  //función encargada de mostar  el "form_editar_sedesesriesgos" al usuario,
  //con la respectiva información de los riesgos de la se educativa.

  public function form_editar_sedesriesgos($id){

    $sedes=riesgos::findOrFail($id);
    $nombres = temp_historico_sede::select('nombre_municipio', 'nombre_establecimiento', 'nombre_sede', 'direccion')
    ->where("codigo_sede","=",$sedes->codigo_dane)
    ->get();
    return view("formularios.form_editar_sedesesriesgos")->with("sedes",$sedes)->with("id",$id)->with("nombres",$nombres);
  }


  //Función encargada de guardar en las tablas riesgos y historico_riesgos, la informacion actualizada de los riesgos de la sede educativa.

  public function editar_sedesriesgos(Request $request){

    $idsedesriesgos=$request->input("id_sedesriesgos");
    $data=riesgos::findOrFail($idsedesriesgos);
    $riesgos=$request["riesgos"];
    $array = implode(', ', $riesgos);
    $data->riesgos = $array;

    $historico = new HistoricoRiesgos;
    $historico->nombre_sede=$data->nombre_sede;
    $historico->codigo_dane=$data->codigo_dane;
    $historico->riesgos = $array;
    $historico->save();

    if( $data->save()){
      return view("mensajes.msj_sede_actualizada");
    }
    else
    {
      return view("mensajes.mensaje_error")->with("msj","Hubo un error al guardar, Por favor inténtalo nuevamente.");
    }
  }


  //función encargada de mostrar al usuario mensaje de confirmación "confirmaciones.form_borrado_sedesriesgos"
  public function form_borrado_sedesriesgos($id){
    $riesgos=riesgos::find($id);
    return view("confirmaciones.form_borrado_sedesriesgos")->with("riesgos",$riesgos);

  }


  //función encargada de borrar la información de los riesgos de la sedes educativas.
  public function borrar_sedesriesgos(Request $request){

    $idriesgos=$request->input("id_sedesriesgos");
    $riesgos=riesgos::findOrFail($idriesgos);

    if($riesgos->delete()){
      return view("mensajes.msj_sede_actualizada_borrada");
    }
    else
    {
      return view("mensajes.mensaje_error")->with("msj","Hubo un error al borrar, Por favor inténtalo nuevamente");
    }

  }


  public function crear_sedesescritura(Request $request){

    $sede_id=$request->sede_id;

    $existencia = SedeEscritura::select('DaneSede')->where('DaneSede', '=', $sede_id)->get();

    if(count($existencia) == 0) {
      $sedesescritura=new SedeEscritura;
      $sedesescritura->DaneSede=$request->input("sede_id");
      $sedesescritura->Propietario=$request->input("propietario");
      $nombres = temp_historico_sede::where("codigo_sede","=",$sedesescritura->DaneSede)->get(["nombre_sede"]);
      foreach ($nombres as $nombre) {
        $nombre = $nombre->nombre_sede;
      }
      $sedesescritura->NombreSede=$nombre;
      $sedesescritura->MatriculaInmob=$request->input("matriculainmob");
      $sedesescritura->NumEscritura=$request->input("numescritura");
      $sedesescritura->FechaEscritura=$request->input("fechaescritura");

      if($sedesescritura->save())
      {
        return view("mensajes.msj_info_ingresada");
      }
      else
      {
        return view("mensajes.mensaje_error")->with("msj","Hubo un error al guardar, Por favor inténtalo nuevamente.");
      }

    }else{
      $nombres = temp_historico_sede::select('nombre_sede')
      ->where('codigo_sede', '=', $sede_id)
      ->get();
      return view("mensajes.mensaje_error_actualizacion")->with("nombres", $nombres);
    }

  }


  public function form_editar_sedesescritura($id){

    $sedesescritura=SedeEscritura::findOrFail($id);
    $nombres = temp_historico_sede::select('nombre_municipio', 'nombre_establecimiento', 'nombre_sede', 'direccion')
    ->where('codigo_sede', '=', $sedesescritura->DaneSede)
    ->get();
    return view("formularios.form_editar_sedesescritura")->with("sedesescritura",$sedesescritura)->with("nombres",$nombres);
  }


  public function editar_sedesescritura(Request $request){

    $idsedesescritura=$request->input("id_sedesescritura");
    $sedesescritura=SedeEscritura::find($idsedesescritura);
    $sedesescritura->DaneSede=$request->input("sede_id");
    $sedesescritura->Propietario=$request->input("propietario");
    $sedesescritura->MatriculaInmob=$request->input("matriculainmob");
    $sedesescritura->NumEscritura=$request->input("numescritura");
    $sedesescritura->FechaEscritura=$request->input("fechaescritura");



    if( $sedesescritura->save()){
      return view("mensajes.msj_sede_actualizada")->with("idusuario",$idsedesescritura);
    }
    else
    {
      return view("mensajes.mensaje_error")->with("msj","Hubo un error al guardar, Por favor inténtalo nuevamente.");
    }
  }


  public function form_borrado_sedesescritura($id){
    $sedesescritura=SedeEscritura::find($id);
    return view("confirmaciones.form_borrado_sedesescritura")->with("sedesescritura",$sedesescritura);

  }


  public function borrar_sedesescritura(Request $request){

    $idsedesescritura=$request->input("id_sedesescritura");
    $sedesescritura=SedeEscritura::find($idsedesescritura);

    if($sedesescritura->delete()){
      return view("mensajes.msj_sede_actualizada_borrada");
    }
    else
    {
      return view("mensajes.mensaje_error")->with("msj","Hubo un error al borrar, Por favor inténtalo nuevamente.");
    }

  }

  public function listado_sedesescritura(){

    if(Auth::user()->isRole('secretarios'))
    {

      $datos = SedeEscritura::join('temp_historico_sedes', 'sede_escrituras.DaneSede', '=', 'temp_historico_sedes.codigo_sede')
      ->select('temp_historico_sedes.nombre_sede','nombre_establecimiento','direccion','sede_escrituras.id','DaneSede','Propietario','MatriculaInmob','NumEscritura', 'FechaEscritura')
      ->where('temp_historico_sedes.codigo_dane_municipio', Auth::user()->name)
      ->get();
    }
    elseif (Auth::user()->isRole('rectores'))
    {
      $datos = SedeEscritura::join('temp_historico_sedes', 'sede_escrituras.DaneSede', '=', 'temp_historico_sedes.codigo_sede')
      ->select(['temp_historico_sedes.nombre_sede','nombre_establecimiento','direccion','sede_escrituras.id','DaneSede','Propietario','MatriculaInmob','NumEscritura', 'FechaEscritura'])
      ->where('temp_historico_sedes.codigo_establecimiento', Auth::user()->name)
      ->get();
    }
    else
    {

      $datos = SedeEscritura::join('temp_historico_sedes', 'sede_escrituras.DaneSede', '=', 'temp_historico_sedes.codigo_sede')
      ->select(['temp_historico_sedes.nombre_sede','nombre_establecimiento','direccion','sede_escrituras.id','DaneSede','Propietario','MatriculaInmob','NumEscritura', 'FechaEscritura'])
      ->get();

    }

    return Datatables::of($datos)->make(true);
  }


  public function form_nuevo_sedeslegal(){

    $subregiones = Subregion::orderBy('id', 'asc')->get();

    return view("formularios.form_nuevo_sedeslegal")->with("subregiones", $subregiones);

  }


  public function crear_sedeslegal(Request $request){

    $sede_id=$request->sede_id;

    $existencia = SedesLegal::select('DaneSede')
    ->where('DaneSede', '=', $sede_id)
    ->get();

    if(count($existencia) == 0) {
      $sedeslegal=new SedesLegal;
      $sedeslegal->DaneSede=$request->input("sede_id");
      $nombres = temp_historico_sede::select('nombre_sede')
      ->where('codigo_sede', '=', $sedeslegal->DaneSede)
      ->get();
      foreach ($nombres as $nombre) {
        $nombre = $nombre->nombre_sede;
      }
      $sedeslegal->NombreSede= $nombre;
      $sedeslegal->TipoPropietario=$request->input("tipopropietario");
      $sedeslegal->Modalidad=$request->input("modalidad");
      $sedeslegal->AreaLote=$request->input("arealote");
      $sedeslegal->AreaConstruida=$request->input("areaconstruida");
      $sedeslegal->Plano=$request->input("plano");
      $sedeslegal->Foto=$request->input("foto");
      $sedeslegal->Coordenadas=$request->input("coordenadas");
      $sedeslegal->Distancia=$request->input("distancia");
      $tipovia=$request["tipovia"];
      $array = implode(', ', $tipovia);
      $sedeslegal->TipoVia=$array;
      $sedeslegal->Latitud=$request->input("latitud");


      if($sedeslegal->save())
      {

        return view("mensajes.msj_info_ingresada");
      }
    }

    else{

      $nombres = temp_historico_sede::select('nombre_sede')
      ->where('codigo_sede', '=', $sede_id)
      ->get();
      return view("mensajes.mensaje_error_actualizacion")->with("nombres", $nombres);

    }


  }


  public function form_editar_sedeslegal($id){

    $sedeslegal=SedesLegal::findOrFail($id);

    $nombres = temp_historico_sede::select('nombre_municipio', 'nombre_establecimiento', 'nombre_sede', 'direccion')
    ->where('codigo_sede', '=', $sedeslegal->DaneSede)
    ->get();

    return view("formularios.form_editar_sedeslegal")->with("sedeslegal",$sedeslegal)->with("nombres",$nombres);

  }


  public function editar_sedeslegal(Request $request){

    $idsedeslegal=$request->input("id_sedeslegal");
    $sedeslegal=SedesLegal::find($idsedeslegal);
    $sedeslegal->DaneSede=$request->input("sede_id");
    $sedeslegal->TipoPropietario=$request->input("tipopropietario");
    $sedeslegal->Modalidad=$request->input("modalidad");
    $sedeslegal->AreaLote=$request->input("arealote");
    $sedeslegal->AreaConstruida=$request->input("areaconstruida");
    $sedeslegal->Plano=$request->input("plano");
    $sedeslegal->Foto=$request->input("foto");
    $sedeslegal->Coordenadas=$request->input("coordenadas");
    $sedeslegal->Distancia=$request->input("distancia");
    $tipovia=$request["tipovia"];
    $array = implode(', ', $tipovia);
    $sedeslegal->TipoVia=$array;
    $sedeslegal->Latitud=$request->input("latitud");

    if( $sedeslegal->save()){
      return view("mensajes.msj_sede_actualizada");
    }
    else
    {
      return view("mensajes.mensaje_error_actualizacion");
    }
  }


  public function form_borrado_sedeslegal($id){
    $sedeslegal=SedesLegal::find($id);
    return view("confirmaciones.form_borrado_sedeslegal")->with("sedeslegal",$sedeslegal);

  }

  public function borrar_sedeslegal(Request $request){

    $idsedeslegal=$request->input("id_sedeslegal");
    $sedeslegal=SedesLegal::find($idsedeslegal);

    if($sedeslegal->delete()){
      return view("mensajes.msj_sede_actualizada_borrada");
    }
    else
    {
      return view("mensajes.mensaje_error_actualizacion")->with("msj","Hubo un error al borrar, Por favor inténtalo nuevamente.");
    }


  }

  public function listado_sedeslegal(){

    if(Auth::user()->isRole('secretarios'))
    {

      $datos = SedesLegal::join('temp_historico_sedes', 'sedes_legals.DaneSede', '=', 'temp_historico_sedes.codigo_sede')
      ->select('temp_historico_sedes.nombre_sede','nombre_establecimiento','direccion','sedes_legals.id','DaneSede','TipoPropietario','Modalidad','AreaLote', 'AreaConstruida','Plano','Foto','Coordenadas','Distancia','TipoVia','Latitud')
      ->where('temp_historico_sedes.codigo_dane_municipio', Auth::user()->name)
      ->get();
    }
    elseif (Auth::user()->isRole('rectores'))
    {
      $datos = SedesLegal::join('temp_historico_sedes', 'sedes_legals.DaneSede', '=', 'temp_historico_sedes.codigo_sede')
      ->select(['temp_historico_sedes.nombre_sede','nombre_establecimiento','direccion','sedes_legals.id','DaneSede','TipoPropietario','Modalidad','AreaLote', 'AreaConstruida','Plano','Foto','Coordenadas','Distancia','TipoVia','Latitud'])
      ->where('temp_historico_sedes.codigo_establecimiento', Auth::user()->name)
      ->get();
    }
    else
    {
      $datos = SedesLegal::join('temp_historico_sedes', 'sedes_legals.DaneSede', '=', 'temp_historico_sedes.codigo_sede')
      ->select(['temp_historico_sedes.nombre_sede','nombre_establecimiento','direccion','sedes_legals.id','DaneSede','TipoPropietario','Modalidad','AreaLote', 'AreaConstruida','Plano','Foto','Coordenadas','Distancia','TipoVia','Latitud'])
      ->get();

    }

    return Datatables::of($datos)->make(true);
  }


  public function form_nuevo_sedesservicio(){

    $subregiones = Subregion::orderBy('id', 'asc')->get();

    return view("formularios.form_nuevo_sedesservicio")->with("subregiones", $subregiones);

  }



  public function crear_sedesservicio(Request $request){

    $sede_id=$request->sede_id;
    $existencia = SedesServicio::select('DaneSede')
    ->where('DaneSede', '=', $sede_id)
    ->get();


    if(count($existencia) == 0) {
      $sedesservicio=new SedesServicio;
      $Tipoenergia=$request["tipoenergia"];
      $array = implode(', ', $Tipoenergia);
      $sedesservicio->TipoEnergia=$array;
      $sedesservicio->DaneSede=$request->input("sede_id");
      $sedesservicio->Acueducto=$request->input("acueducto");
      $sedesservicio->AguaPotable=$request->input("aguapotable");
      $sedesservicio->PlantaTratamiento=$request->input("plantatratamiento");
      $sedesservicio->Alcantarillado=$request->input("alcantarillado");
      $sedesservicio->PozoSeptico=$request->input("pozoseptico");

      if($sedesservicio->save())
      {

        return view("mensajes.msj_info_ingresada");
      }

    }

    else{

      $nombres = temp_historico_sede::select('nombre_sede')
      ->where('codigo_sede', '=', $sede_id)
      ->get();
      return view("mensajes.mensaje_error_actualizacion")->with("nombres", $nombres);

    }

  }



  public function form_editar_sedesservicio($id){

    $sedesservicio=SedesServicio::find($id);
    $nombres = temp_historico_sede::select('nombre_municipio', 'nombre_establecimiento', 'nombre_sede', 'direccion')
    ->where('codigo_sede', '=', $sedesservicio->DaneSede)
    ->get();
    return view("formularios.form_editar_sedesservicio")->with("sedesservicio",$sedesservicio)->with("nombres",$nombres);
  }


  public function editar_sedesservicio(Request $request){

    $idsedesservicio=$request->input("id_sedesservicio");
    $sedesservicio=SedesServicio::find($idsedesservicio);
    $sedesservicio->DaneSede=$request->input("sede_id");
    $Tipoenergia=$request["tipoenergia"];
    $array = implode(', ', $Tipoenergia);
    $sedesservicio->TipoEnergia=$array;
    $sedesservicio->Acueducto=$request->input("acueducto");
    $sedesservicio->AguaPotable=$request->input("aguapotable");
    $sedesservicio->PlantaTratamiento=$request->input("plantatratamiento");
    $sedesservicio->Alcantarillado=$request->input("alcantarillado");
    $sedesservicio->PozoSeptico=$request->input("pozoseptico");

    if( $sedesservicio->save()){
      return view("mensajes.msj_sede_actualizada")->with("idusuario",$idsedesservicio) ;
    }
    else
    {
      return view("mensajes.mensaje_error")->with("msj","Hubo un error al guardar, Por favor inténtalo nuevamente.");
    }
  }


  public function form_borrado_sedesservicio($id){
    $sedesservicio=SedesServicio::find($id);
    $nombres = temp_historico_sede::select('nombre_sede')
    ->where('codigo_sede', '=', $sedesservicio->DaneSede)
    ->get();
    foreach ($nombres as $nombre) {
      $nombre = $nombre->nombre_sede;
    }
    return view("confirmaciones.form_borrado_sedesservicio")->with("sedesservicio",$sedesservicio)->with("nombre",$nombre);

  }

  public function borrar_sedesservicio(Request $request){

    $idsedesservicio=$request->input("id_sedesservicio");
    $sedesservicio=SedesServicio::find($idsedesservicio);

    if($sedesservicio->delete()){
      return view("mensajes.msj_sede_actualizada_borrada");
    }
    else
    {
      return view("mensajes.mensaje_error")->with("msj","Hubo un error al borrar, Por favor inténtalo nuevamente.");
    }


  }

  public  function objectToArray($d) {
    if (is_object($d)) {
      $d = get_object_vars($d);
    }

    if (is_array($d)) {
      return array_map('trim', $d);
    }
    else {
      return $d;
    }
  }



  public function listado_sedesservicio(){

    if(Auth::user()->isRole('secretarios'))
    {
      $datos = SedesServicio::join('temp_historico_sedes', 'sedes_servicios.DaneSede', '=', 'temp_historico_sedes.codigo_sede')
      ->select(['temp_historico_sedes.nombre_sede','nombre_establecimiento','direccion','sedes_servicios.id','DaneSede','TipoEnergia','Acueducto','AguaPotable','PlantaTratamiento','Alcantarillado','PozoSeptico'])
      ->where('temp_historico_sedes.codigo_dane_municipio', Auth::user()->name)
      ->get();
    }
    elseif (Auth::user()->isRole('rectores'))
    {
      $datos = SedesServicio::join('temp_historico_sedes', 'sedes_servicios.DaneSede', '=', 'temp_historico_sedes.codigo_sede')
      ->select(['temp_historico_sedes.nombre_sede','nombre_establecimiento','direccion','sedes_servicios.id','DaneSede','TipoEnergia','Acueducto','AguaPotable','PlantaTratamiento','Alcantarillado','PozoSeptico'])
      ->where('temp_historico_sedes.codigo_establecimiento', Auth::user()->name)
      ->get();
    }
    else
    {

      $datos = SedesServicio::join('temp_historico_sedes', 'sedes_servicios.DaneSede', '=', 'temp_historico_sedes.codigo_sede')
      ->select(['temp_historico_sedes.nombre_sede','nombre_establecimiento','direccion','sedes_servicios.id','DaneSede','TipoEnergia','Acueducto','AguaPotable','PlantaTratamiento','Alcantarillado','PozoSeptico'])
      ->get();


    }

    return Datatables::of($datos)->make(true);

  }


  public function form_nuevo_sedesotros(){

    return view("formularios.form_nuevo_sedesotros");

  }

  public function form_nuevo_sedesbaterias(){

    return view("formularios.form_nuevo_sedesbaterias");

  }

  public function form_nuevo_sedesaulas(){

    $subregiones = Subregion::orderBy('id', 'asc')->get();

    return view("formularios.form_nuevo_sedesaulas")->with("subregiones", $subregiones);

  }


  public function crear_sedesespacio(Request $request){

    $sede_id=$request->sede_id;

    $existencia = SedesEspacio::select('DaneSede')
    ->where('DaneSede', '=', $sede_id)
    ->get();

    if(count($existencia) == 0) {

      $sedesespacio=new SedesEspacio;
      $sedesespacio->DaneSede=$request->input("sede_id");
      $sedesespacio->AulasPreescolar=$request->input("aulaspreescolar");
      $sedesespacio->AulasPrimaria=$request->input("aulasprimaria");
      $sedesespacio->AulasSecundaria=$request->input("aulassecundaria");
      $sedesespacio->AulasEspaciales=$request->input("aulasespaciales");
      $sedesespacio->Biblioteca=$request->input("biblioteca");
      $sedesespacio->AulasSistemas=$request->input("aulassistemas");
      $sedesespacio->AulasBilinguismo=$request->input("aulasbilinguismo");
      $sedesespacio->Laboratorio=$request->input("laboratorio");
      $sedesespacio->AulasTalleres=$request->input("aulastalleres");
      $sedesespacio->AulasMultiples=$request->input("aulasmultiples");
      $sedesespacio->Cocinas=$request->input("cocinas");
      $sedesespacio->Comedores=$request->input("comedores");
      $sedesespacio->SantiriosHombres=$request->input("santirioshombres");
      $sedesespacio->SanitariosMujeres=$request->input("sanitariosmujeres");
      $sedesespacio->LavamanosHombres=$request->input("lavamanoshombres");
      $sedesespacio->LavamanosMujeres=$request->input("lavamanosmujeres");
      $sedesespacio->Orinales=$request->input("orinales");
      $sedesespacio->Vivienda=$request->input("vivienda");
      $sedesespacio->Canchas=$request->input("canchas");
      $sedesespacio->PlacasMulti=$request->input("placasmulti");
      $sedesespacio->JuegosInfantiles=$request->input("juegosinfantiles");

      if($sedesespacio->save())
      {

        return view("mensajes.msj_info_ingresada");
      }
    }
    else
    {
      $nombres = temp_historico_sede::select('nombre_sede')
      ->where('codigo_sede', '=', $sede_id)
      ->get();
      return view("mensajes.mensaje_error_actualizacion")->with("nombres", $nombres);
    }

  }




  public function form_editar_sedesotros($id){

    $sedesespacio=SedesEspacio::find($id);
    $nombres = temp_historico_sede::select('nombre_municipio', 'nombre_establecimiento', 'nombre_sede', 'direccion')
    ->where('codigo_sede', '=', $sedesespacio->DaneSede)
    ->get();
    return view("formularios.form_editar_sedesotros")->with("sedesespacio",$sedesespacio)->with("nombres", $nombres);
  }

  public function form_editar_sedesaulas($id){

    $sedesespacio=SedesEspacio::find($id);
    $nombres = temp_historico_sede::select('nombre_municipio', 'nombre_establecimiento', 'nombre_sede', 'direccion')
    ->where('codigo_sede', '=', $sedesespacio->DaneSede)
    ->get();
    return view("formularios.form_editar_sedesaulas")->with("sedesespacio",$sedesespacio)->with("nombres",$nombres);
  }

  public function form_editar_sedesbaterias($id){

    $sedesespacio=SedesEspacio::find($id);
    $nombres = temp_historico_sede::select('nombre_municipio', 'nombre_establecimiento', 'nombre_sede', 'direccion')
    ->where('codigo_sede', '=', $sedesespacio->DaneSede)
    ->get();
    return view("formularios.form_editar_sedesbaterias")->with("sedesespacio",$sedesespacio)->with("nombres",$nombres);
  }


  public function editar_sedesaulas(Request $request){

    $idsedesespacio=$request->input("id_sedesespacio");
    $sedesespacio=SedesEspacio::find($idsedesespacio);
    $sedesespacio->DaneSede=$request->input("sede_id");
    $sedesespacio->AulasPreescolar=$request->input("aulaspreescolar");
    $sedesespacio->AulasPrimaria=$request->input("aulasprimaria");
    $sedesespacio->AulasSecundaria=$request->input("aulassecundaria");
    $sedesespacio->AulasEspaciales=$request->input("aulasespaciales");
    $sedesespacio->Biblioteca=$request->input("biblioteca");
    $sedesespacio->AulasSistemas=$request->input("aulassistemas");
    $sedesespacio->AulasBilinguismo=$request->input("aulasbilinguismo");
    $sedesespacio->Laboratorio=$request->input("laboratorio");
    $sedesespacio->AulasTalleres=$request->input("aulastalleres");
    $sedesespacio->AulasMultiples=$request->input("aulasmultiples");


    if( $sedesespacio->save()){
      return view("mensajes.msj_sede_actualizada")->with("idusuario",$idsedesespacio) ;
    }
    else
    {
      return view("mensajes.mensaje_error")->with("msj","Hubo un error al guardar, Por favor inténtalo nuevamente.");
    }
  }



  public function editar_sedesbaterias(Request $request){

    $idsedesespacio=$request->input("id_sedesespacio");
    $sedesespacio=SedesEspacio::find($idsedesespacio);
    $sedesespacio->DaneSede=$request->input("sede_id");
    $sedesespacio->Cocinas=$request->input("cocinas");
    $sedesespacio->Comedores=$request->input("comedores");
    $sedesespacio->SantiriosHombres=$request->input("santirioshombres");
    $sedesespacio->SanitariosMujeres=$request->input("sanitariosmujeres");
    $sedesespacio->LavamanosHombres=$request->input("lavamanoshombres");
    $sedesespacio->LavamanosMujeres=$request->input("lavamanosmujeres");
    $sedesespacio->Orinales=$request->input("orinales");
    $sedesespacio->SanitariosReducida=$request->input("bañosreducida");



    if( $sedesespacio->save()){
      return view("mensajes.msj_sede_actualizada")->with("idusuario",$idsedesespacio) ;
    }
    else
    {
      return view("mensajes.mensaje_error")->with("msj","Hubo un error al guardar, Por favor inténtalo nuevamente.");
    }

  }

  public function editar_sedesotros(Request $request){

    $idsedesespacio=$request->input("id_sedesespacio");
    $sedesespacio=SedesEspacio::find($idsedesespacio);
    $sedesespacio->DaneSede=$request->input("sede_id");
    $sedesespacio->Cocinas=$request->input("cocinas");
    $sedesespacio->Comedores=$request->input("comedores");
    $sedesespacio->Vivienda=$request->input("vivienda");
    $sedesespacio->Canchas=$request->input("canchas");
    $sedesespacio->PlacasMulti=$request->input("placasmulti");
    $sedesespacio->JuegosInfantiles=$request->input("juegosinfantiles");


    if( $sedesespacio->save()){
      return view("mensajes.msj_sede_actualizada")->with("idusuario",$idsedesespacio) ;
    }
    else
    {
      return view("mensajes.mensaje_error")->with("msj","Hubo un error al guardar, Por favor inténtalo nuevamente.");
    }

  }




  public function form_borrado_sedesespacios($id){
    $sedesespacio=SedesEspacio::find($id);
    $nombres = temp_historico_sede::select('nombre_sede')
    ->where('codigo_sede', '=', $sedesespacio->DaneSede)
    ->get();
    foreach ($nombres as $nombre) {
      $nombre = $nombre->nombre_sede;
    }
    return view("confirmaciones.form_borrado_sedesespacios")->with("sedesespacio",$sedesespacio)->with("nombre",$nombre);

  }

  public function borrar_sedesespacio(Request $request){

    $idsedesespacio=$request->input("id_sedesespacio");
    $sedesespacio=SedesEspacio::find($idsedesespacio);

    if($sedesespacio->delete()){
      return view("mensajes.msj_sede_actualizada_borrada");
    }
    else
    {
      return view("mensajes.mensaje_error")->with("msj","Hubo un error al borrar, Por favor inténtalo nuevamente.");
    }


  }

  public function listado_sedesespacio(){

    if(Auth::user()->isRole('secretarios'))
    {
      $datos = SedesEspacio::join('temp_historico_sedes', 'sedes_espacios.DaneSede', '=', 'temp_historico_sedes.codigo_sede')
      ->select('temp_historico_sedes.nombre_sede','nombre_establecimiento','direccion','sedes_espacios.id','DaneSede','AulasPreescolar','AulasPrimaria','AulasSecundaria','AulasEspaciales','Biblioteca','AulasSistemas','AulasBilinguismo','Laboratorio','AulasTalleres','AulasMultiples','Cocinas','Comedores','SantiriosHombres','SanitariosMujeres','LavamanosHombres','LavamanosMujeres','Orinales','Vivienda',
      'Canchas','PlacasMulti','JuegosInfantiles','SanitariosReducida')
      ->where('temp_historico_sedes.codigo_dane_municipio', Auth::user()->name)
      ->get();
    }
    elseif (Auth::user()->isRole('rectores'))
    {
      $datos = SedesEspacio::join('temp_historico_sedes', 'sedes_espacios.DaneSede', '=', 'temp_historico_sedes.codigo_sede')
      ->select('temp_historico_sedes.nombre_sede','nombre_establecimiento','direccion','sedes_espacios.id','DaneSede','AulasPreescolar','AulasPrimaria','AulasSecundaria','AulasEspaciales','Biblioteca','AulasSistemas','AulasBilinguismo','Laboratorio','AulasTalleres','AulasMultiples','Cocinas','Comedores','SantiriosHombres','SanitariosMujeres','LavamanosHombres','LavamanosMujeres','Orinales','Vivienda',
      'Canchas','PlacasMulti','JuegosInfantiles','SanitariosReducida')
      ->where('temp_historico_sedes.codigo_establecimiento', Auth::user()->name)
      ->get();
    }
    else
    {

      $datos = SedesEspacio::join('temp_historico_sedes', 'sedes_espacios.DaneSede', '=', 'temp_historico_sedes.codigo_sede')
      ->select('temp_historico_sedes.nombre_sede','nombre_establecimiento','direccion','sedes_espacios.id','DaneSede','AulasPreescolar','AulasPrimaria','AulasSecundaria','AulasEspaciales','Biblioteca','AulasSistemas','AulasBilinguismo','Laboratorio','AulasTalleres','AulasMultiples','Cocinas','Comedores','SantiriosHombres','SanitariosMujeres','LavamanosHombres','LavamanosMujeres','Orinales','Vivienda',
      'Canchas','PlacasMulti','JuegosInfantiles','SanitariosReducida')
      ->get();

    }

    return Datatables::of($datos)->make(true);
  }


  public function listado_informacion()
  {
    $temparchivosedes=SedesInfra::paginate(100);
    $municipios=Municipio::paginate(100);
    $sedeslegals=SedesLegal::paginate(100);
    $sedesservicios=SedesServicio::paginate(100);
    $sedesespacios=SedesEspacio::paginate(100);
    $sedeescrituras=SedeEscritura::paginate(100);


    return view("actualizacion.informacion")
    ->with("temparchivosedes",$temparchivosedes)
    ->with("municipios",$municipios)
    ->with("sedeslegals",$sedeslegals)
    ->with("sedesespacios",$sedesespacios)
    ->with("sedesservicios",$sedesservicios)
    ->with("sedeescrituras",$sedeescrituras);

  }


  /**
  * Show the application dashboard.
  *
  * @return Response
  */

  public function index()
  {

  }

}
