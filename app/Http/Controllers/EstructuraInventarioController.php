<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
//
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

use App\OrigenDispositivo;
use App\TipoDispositivo;
use App\Proyecto;
use App\Contrato;
use App\Solicitud;

class EstructuraInventarioController extends Controller
{
    //Fuente de dispositivos, tipo de dispositivos, contratos y proyectos.

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    //LISTAR
    public function listar_estructura_inventario()
    {
        $origenes = OrigenDispositivo::orderBy('id', 'ASC')->paginate(10); //OK!!! Muestra los que NO están con eliminados.
        $tipos = TipoDispositivo::orderBy('id', 'ASC')->paginate(10);
        $proyectos = Proyecto::orderBy('id', 'ASC')->paginate(10);
        $contratos = Contrato::orderBy('id', 'ASC')->paginate(10);
        $solicitudes = Solicitud::orderBy('id', 'ASC')->paginate(10);

        return view("inventario.editar_estructura_inventario")
                    ->with("origenes", $origenes)
                    ->with("tipos", $tipos)
                    ->with("proyectos", $proyectos)
                    ->with("contratos", $contratos)
                    ->with("solicitudes", $solicitudes);
    }

    //FUENTE DE DISPOSITIVO
    public function form_nuevo_origen()
    {
        return view("formularios.form_nuevo_origen");
    }
    public function crear_origen(Request $request)
    {
        $orn = OrigenDispositivo::where("id", $request->input("id_origen"))->first();
        if(!$orn)
        {
            $origen = OrigenDispositivo::create(["nombre" => strtoupper($request->input("nombre_origen")),
            "descripcion" => strtoupper($request->input("descripcion_origen"))]); //OK!!!
        }
        else
        {
            return view("mensajes.mensaje_error")->with("msj", "La fuente de dispositivo con id " . $request->input("id_origen") . " ya existe o no es permitirlo utilizarlo.");
        }

        if($origen)
        {
            return view("mensajes.msj_origen_creado");
        }
        else
        {
            return view("mensajes.mensaje_error")->with("msj",".Hubo un error al agregar ;..") ;
        }
    }

    public function form_editar_origen($id)
    {
        $origen = OrigenDispositivo::find($id);
        return view("formularios.form_editar_origen")->with("origen", $origen);
    }
    public function editar_origen(Request $request){

        $idorigen = $request->input("id_origen");
        $origen = OrigenDispositivo::find($idorigen);
        $origen->nombre = strtoupper($request->input("nombre_origen"));
        $origen->descripcion = strtoupper($request->input("descripcion_origen"));

        if($origen->save())
        {
    		return view("mensajes.msj_actualizado");

        }
        else
        {
    		return view("mensajes.mensaje_error")->with("msj","Hubo un error al agregar ; intentarlo nuevamente.");
        }
    }

    public function form_borrado_origen($id)
    {
        $origen = OrigenDispositivo::find($id); //NO funcionan los que tienen borrado lógico.
        return view("confirmaciones.form_borrado_origen")->with("origen", $origen);
    }
    public function borrar_origen(Request $request)
    {
        $idorigen = $request->input("id_origen");
        $origen = OrigenDispositivo::find($idorigen);

        $dispositivos = DB::table("dispositivos")->where('origen_id', $idorigen)
                                                ->select('id', 'origen_id', 'serial')
                                                ->get();

        if($dispositivos->isEmpty())
        {
            if($origen->delete())
            {
                return view("mensajes.msj_borrado");
            }
            else
            {
                return view("mensajes.mensaje_error")->with("msj","Hubo un error al borrar ; intentarlo nuevamente.");
            }
        }
        else
        {
            return view("mensajes.mensaje_error")->with('msj', 'El fuante de dispositivo "' . $origen->nombre . '" no se puede borrar debido a que está vinculado al menos a un dispositivo');
        }


    }
    //FIN FUENTES DE DISPOSITIVO

    //TIPOS DE DISPOSITIVO
    public function form_nuevo_tipodispositivo()
    {
        return view("formularios.form_nuevo_tipodispositivo");
    }
    public function crear_tipodispositivo(Request $request)
    {
        $tipo = TipoDispositivo::where("id", $request->input("id_tipo"))->first();
        if(!$tipo)
        {
            $tipo_dispositivo = TipoDispositivo::create(["nombre" => strtoupper($request->input("nombre_tipo")),
                                                        "descripcion" => strtoupper($request->input("descripcion_tipo"))]);
        }
        else
        {
            return view("mensajes.mensaje_error")->with("msj", "El tipo de dispositivo con id " . $request->input("id_tipo") . " ya existe o no es permitirlo utilizarlo.");
        }

        if($tipo_dispositivo)
        {
            return view("mensajes.msj_info_ingresada");
        }
        else
        {
            return view("mensajes.mensaje_error")->with("msj",".Hubo un error al agregar ;..") ;
        }
    }

    public function form_editar_tipodispositivo($id)
    {
        $tipo = TipoDispositivo::find($id);
        return view("formularios.form_editar_tipodispositivo")->with("tipo", $tipo);
    }
    public function editar_tipodispositivo(Request $request)
    {
        $idtipo = $request->input("id_tipo");
        $tipo = TipoDispositivo::find($idtipo);

        $tipo->nombre = strtoupper($request->input("nombre_tipo"));
        $tipo->descripcion = strtoupper($request->input("descripcion_tipo"));

        if($tipo->save())
        {
    		return view("mensajes.msj_actualizado");

        }
        else
        {
            return view("mensajes.mensaje_error")->with("msj","Hubo un error al agregar ; intentarlo nuevamente.");
        }
    }

    public function form_borrado_tipodispositivo($id)
    {
        $tipo = TipoDispositivo::find($id);
        return view("confirmaciones.form_borrado_tipodispositivo")->with("tipo", $tipo);
    }
    public function borrar_tipodispositivo(Request $request)
    {
        $idtipo = $request->input("id_tipo");
        $tipo = TipoDispositivo::find($idtipo);

        $dispositivos = DB::table("dispositivos")->where('tipo_id', $idtipo)
                                                 ->select('id', 'tipo_id', 'serial')
                                                 ->get();
        //Verifica en dispositivos
        if($dispositivos->count() == 0)
        {
            //Verifica en solicitudes
            $solicitudes = DB::table("solicitudes")->where('tipodispositivo_id', $idtipo)
                                                   ->select('id')->get();
            if($solicitudes->count() == 0)
            {
                if($tipo->delete())
                {
                    return view("mensajes.msj_borrado");
                }
                else
                {
                    return view("mensajes.mensaje_error")->with("msj","Hubo un error al borrar ; intentarlo nuevamente.");
                }
            }
            else
            {
                return view("mensajes.mensaje_error")->with('msj', 'El tipo de dispositivo "' . $tipo->nombre . '" no se puede borrar debido a que está vinculado al menos a una solicitud.');
            }
        }
        else
        {
            return view("mensajes.mensaje_error")->with('msj', 'El tipo de dispositivo "' . $tipo->nombre . '" no se puede borrar debido a que está vinculado al menos a un dispositivo.');
        }
    }
    //FIN TIPOS DE DISPOSITIVO

    //PROYECTO
     public function form_nuevo_proyecto()
     {
         return view("formularios.form_nuevo_proyecto");
     }
     public function crear_proyecto(Request $request)
     {
         $periodo = $request->input("periodo") . "-" . $request->input("periodofinalizacion");

         $pro = Proyecto::where("id", $request->input("id_proyecto"))->first();
         if(!$pro)
         {
            $proyecto = Proyecto::create(["nombre" => strtoupper($request->input("nombre_proyecto")),
                                           "plan_desarrollo" => strtoupper($request->input("plan_desarrollo")),
                                           "periodo" => $periodo]);
         }
         else
         {
             return view("mensajes.mensaje_error")->with("msj", "El proyecto con id " . $request->input("id_proyecto") . " ya existe o no es permitirlo utilizarlo.");
         }

         if($proyecto)
         {
             return view("mensajes.msj_proyecto_creado")->with("msj","Proyecto agregado correctamente.") ;
         }
         else
         {
             return view("mensajes.mensaje_error")->with("msj",".Hubo un error al agregar ;..") ;
         }
     }

     public function form_editar_proyecto($id)
     {
        $proyecto = Proyecto::find($id);
        $periodo_inicio = substr($proyecto->periodo, 0, 4);
        $periodo_final = substr($proyecto->periodo, 5, 4);
        return view("formularios.form_editar_proyecto")->with("proyecto", $proyecto)
                                                    ->with("periodo_inicio", $periodo_inicio)
                                                    ->with("periodo_final", $periodo_final);
     }
     public function editar_proyecto(Request $request)
     {
         $periodo = $request->input("periodo") . "-" . $request->input("periodofinalizacion");

         $idproyecto = $request->input("id_proyecto");
         $proyecto = Proyecto::find($idproyecto);
         $proyecto->nombre = strtoupper($request->input("nombre_proyecto"));
         $proyecto->plan_desarrollo = strtoupper($request->input("plan_desarrollo"));
         $proyecto->periodo = $periodo;

         if($proyecto->save())
         {
             return view("mensajes.msj_actualizado");
         }
         else
         {
             return view("mensajes.mensaje_error")->with("msj","Hubo un error al agregar ; intentarlo nuevamente.");
         }
     }

     public function form_borrado_proyecto($id)
     {
         $proyecto = Proyecto::find($id);
         return view("confirmaciones.form_borrado_proyecto")->with("proyecto", $proyecto);
     }
     public function borrar_proyecto(Request $request)
     {
         $idproyecto = $request->input("id_proyecto");
         $proyecto = Proyecto::find($idproyecto);
         //Nota: $proyecto->contratos trae la información del contrato que tiene asociado el proyecto en contratos.

         if(($proyecto->contratos)->isEmpty())
         {
            if($proyecto->delete())
            {
                return view("mensajes.msj_borrado");
            }
            else
            {
                return view("mensajes.mensaje_error")->with("msj","Hubo un error al borrar ; intentarlo nuevamente.");
            }
         }
         else
         {
            $contrato = Contrato::where("proyecto_id", $idproyecto)->first();
            return view("mensajes.mensaje_error")->with('msj', 'El proyecto "' . $proyecto->nombre . '" no se puede borrar debido a que está vinculado al menos al contrato "' . $contrato->nombre . '".');
         }
     }
     //FIN PROYECTO

     //CONTRATO
     public function form_nuevo_contrato()
     {
         $proyectos = Proyecto::orderBy('nombre', 'asc')->get();
         return view("formularios.form_nuevo_contrato")->with("proyectos", $proyectos);
     }
     public function crear_contrato(Request $request)
     {
        // return $request;

        $con = Contrato::where("id", $request->input("id_contrato"))->first();
        if(!$con)
        {
            $contrato = Contrato::create([
            "numero_contrato" => strtoupper($request->input("numero_contrato")),
            "objeto_contrato" => strtoupper($request->input("objeto_contrato")),
            "tipo_contrato" => $request->input("tipo_contrato"),
            "fecha_acta_inicio" => $request->input("fecha_del_acta_inicio"),
            "fecha_terminacion_contrato" => $request->input("fecha_terminacion_contrato"),
            "nit_contratista" => $request->input("nit_contratista"),
            "nombre_contratista" => strtoupper($request->input("nombre_contratista")),
            "telefono_contratista" => $request->input("telefono_contratista"),
            "valor_total_contrato" => $request->input("valor_total_contrato"),
            "dependencia_contratante" => strtoupper($request->input("dependencia_contratante")),
            "telefono_mesa_ayuda" => $request->input("mesa_ayuda"),
            "observaciones" => strtoupper($request->input("observaciones")),
            "proyecto_id" => $request->input("id_proyecto")
            ]);
        }
        else
        {
             return view("mensajes.mensaje_error")->with("msj", "El contrato con id " . $request->input("id_contrato") . " ya existe o no es permitirlo utilizarlo.");
        }

        if($contrato)
        {
            return view("mensajes.msj_contrato_creado")->with("msj","Contrato agregado correctamente.") ;
        }
        else
        {
            return view("mensajes.mensaje_error")->with("msj",".Hubo un error al agregar ;..") ;
        }
     }

     public function form_editar_contrato($id)
     {
         $contrato = Contrato::find($id);

         $idproyecto = $contrato->proyecto_id;
         $proy = Proyecto::find($idproyecto);

         $proyectos = Proyecto::all();

         return view("formularios.form_editar_contrato")->with("contrato", $contrato)
                                                        ->with("proy", $proy)
                                                        ->with("proyectos", $proyectos);
     }
     public function editar_contrato(Request $request)
     {
         $idcontrato = $request->input("id_contrato");
         $contrato = Contrato::find($idcontrato);

         $contrato->numero_contrato = strtoupper($request->input("numero_contrato"));
         $contrato->objeto_contrato = strtoupper($request->input("objeto_contrato"));
         $contrato->tipo_contrato = $request->input("tipo_contrato");
         $contrato->fecha_acta_inicio = $request->input("fecha_del_acta_inicio");
         $contrato->fecha_terminacion_contrato = $request->input("fecha_terminacion_contrato");
         $contrato->nit_contratista = $request->input("nit_contratista");
         $contrato->nombre_contratista = strtoupper($request->input("nombre_contratista"));
         $contrato->telefono_contratista = $request->input("telefono_contratista");
         $contrato->valor_total_contrato = $request->input("valor_total_contrato");
         $contrato->dependencia_contratante = strtoupper($request->input("dependencia_contratante"));
         $contrato->telefono_mesa_ayuda = $request->input("mesa_ayuda");
         $contrato->observaciones = strtoupper($request->input("observaciones"));
         $contrato->proyecto_id = $request->input("id_proyecto");

         if($contrato->save())
         {
             return view("mensajes.msj_actualizado");

         }
         else
         {
             return view("mensajes.mensaje_error")->with("msj","Hubo un error al agregar ; intentarlo nuevamente.");
         }
     }

     public function form_borrado_contrato($id)
     {
         $contrato = Contrato::find($id);
         return view("confirmaciones.form_borrado_contrato")->with("contrato", $contrato);
     }
     public function borrar_contrato(Request $request)
     {
         $idcontrato = $request->input("id_contrato");
         $contrato = Contrato::find($idcontrato);

         $dispositivos = DB::table("dispositivos")->where('contrato_id', $idcontrato)
                                                ->select('id', 'contrato_id', 'serial')
                                                ->get();
         if($dispositivos->isEmpty())
         {
            if($contrato->delete())
            {
                return view("mensajes.msj_borrado");
            }
            else
            {
                return view("mensajes.mensaje_error")->with("msj","Hubo un error al borrar ; intentarlo nuevamente.");
            }
         }
         else
         {
            return view("mensajes.mensaje_error")->with('msj', 'El contrato "' . $contrato->nombre . '" no se puede borrar debido a que está vinculado al menos a un dispositivo.');
         }
     }
     //FIN CONTRATO

     //SOLICITUDES
    public function form_nueva_solicitud()
    {
        return view("formularios.form_nueva_solicitud");
    }





}
