<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\TipoEspacio;
use App\TipoSolicitud;
use App\Subregion;
use App\IFisicaEstado;
use App\temp_historico_sede;
use App\temp_historico_establecimiento;
use App\SolicitudIF;
use App\SolicitudEspacio;
use App\RespuestaSolicitudIF;
use App\HistoricoRiesgos;
use App\SedesEspacio;
use App\Riesgos;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;


class RequerimientoifController extends Controller
{
  public function index()
  {

    return $this->muestraFormRequerimientos(0);
  }

  public function muestraFormRequerimientos($mensaje){

    $temp_historico_establecimientos = temp_historico_establecimiento::all()
    ->sortBy('nombre_establecimiento')
    ->where("prestador_de_servicio", "=", "OFICIAL")
    ->where("estado", "<>", 'CIERRE DEFINITIVO')
    ->where("estado", "<>", 'CIERRE TEMPORAL');

    $usuario = Auth::user()->user;
    if(Auth::user()->isRole('secretarios')){
      $temp_historico_establecimientos = $temp_historico_establecimientos ->where("codigo_dane_municipio", "=", Auth::user()->name);
    }elseif (Auth::user()->isRole('rectores')) {
      $temp_historico_establecimientos = $temp_historico_establecimientos ->where("codigo_establecimiento", "=", Auth::user()->name);
    }

    $TipoEspacio = TipoEspacio::all()->sortBy('NombreEspacio');
    $TipoSolicitud = TipoSolicitud::all()->sortBy('Nombre');
    $IFisicaEstado = IFisicaEstado::all()->sortBy('Nombre');
    $regSolicitud = $mensaje;
    return view('requerimientoife.requerimiento', array('temp_historico_establecimientos' => $temp_historico_establecimientos, 'TipoEspacio' => $TipoEspacio, 'TipoSolicitud' => $TipoSolicitud, 'IFisicaEstado' => $IFisicaEstado, 'regSolicitud' =>   $regSolicitud));
  }

  public function filtrarMunicipiosEstablecimientos(Request $request){
    $daneMunicipio = $request->input('municipio');
    $establecimientos = temp_historico_establecimiento::all()
    ->sortBy('nombre_establecimiento')
    ->where("prestador_de_servicio", "=", "OFICIAL")
    ->where("estado", "<>", 'CIERRE DEFINITIVO')
    ->where("estado", "<>", 'CIERRE TEMPORAL')
    ->where("codigo_dane_municipio", "=", $daneMunicipio);

    $arreglo = $this->conversionArray($establecimientos);
    echo json_encode($arreglo);

  }

  public function FiltroSede(Request $request){
    $establecimiento = $request->input('selEstablecimiento');
    $sedes = temp_historico_sede::all()
    ->where("estado_sede", "<>", "CIERRE DEFINITIVO")
    ->where("estado_sede", "<>", "CIERRE TEMPORAL")
    ->where("codigo_establecimiento", "=", $establecimiento);
    $arreglo = $this->conversionArray($sedes);
    echo json_encode($arreglo);

  }

  public function EspaciosSede(Request $request){
    $sede = $request->input('selSede');
    $espacios =  DB::table("sedes_espacios")->where("DaneSede", "=", $sede)->get();
    echo json_encode($espacios);
  }

  public  function conversionArray($rows) {
    foreach ($rows as $row) {
      $stdClass =  $this->objectToArray($row->toArray());
      $arreglo[] = $stdClass;
    }
    return $arreglo;
  }

  //función encargada de llamar el "form_ingresar_riesgo" luego de la validación, para ingresar la información de los riesgos de la sedes educativas.

  public function form_ingresar_riesgo(Request $request){

    $sede_id = SolicitudIF::all()->last();

    $nombres = temp_historico_sede::select('nombre_sede', 'codigo_sede')
      ->where("codigo_sede","=",$sede_id->DaneSede)
      ->get();

    return view("formularios.form_ingresar_riesgo")->with("nombres", $nombres);

  }

  //función encargada de guardar en las tablas riesgos y historico_riesgos la información ingresada en el formulario "form_ingresar_riesgo", en la base de datos.
  public function ingresar_riesgos(Request $request){

    $sede_id=$request->sede_id;
    $existencia = riesgos::select('codigo_dane','nombre_sede')->where('codigo_dane', '=', $sede_id)->get();
    if (count($existencia)==0) {
      $data = new Riesgos;
      $riesgos=$request["riesgos"];
      $array = implode(', ', $riesgos);
      $data->codigo_dane = $request->input("sede_id");
      $nombres = temp_historico_sede::where("codigo_sede","=",$data->codigo_dane)->get(["nombre_sede"]);
      foreach ($nombres as $nombre) {
        $nombre = $nombre->nombre_sede;
      }
      $data->nombre_sede = $nombre;
      $data->riesgos = $array;

      $historico = new HistoricoRiesgos;
      $historico->nombre_sede=$nombre;
      $historico->codigo_dane=$request->input("sede_id");
      $historico->riesgos = $array;
      $historico->save();

      if($data->save())
      {
        return view("mensajes.msj_solicitudRegistrada2");
      }
      else
      {
        return view("mensajes.mensaje_error")->with("msj","Hubo un error al agregar. Por favor, intentalo de nuevo;");
      }
    }else {
      return view("mensajes.mensaje_error_actualizacion2")->with("nombres",$existencia);
    }
  }

  public function RegistrarReqSol(Request $request){
    $establecimiento = $request->input('selEstablecimiento');
    $sede = $request->input('selSede');

    $existencia = riesgos::select('codigo_dane','nombre_sede')->where('codigo_dane', '=', $sede)->get();

    if (count($existencia)==0) {
      $rowsEspacio['rEspacio'] = $request->input('rEspacio');
      $rowsEspacio['rCantidad'] = $request->input('rCantidad');
      $rowsEspacio['rEstado'] = $request->input('rEstado');
      $rowsEspacio['rTipo'] = $request->input('rTipo');
      $rowsEspacio['rElectricidad'] = $request->input('rElectricidad');
      $rowsEspacio['rAcueducto'] = $request->input('rAcueducto');
      $rowsEspacio['rAlcantarillado'] = $request->input('rAlcantarillado');
      $rowsEspacio['rPisos'] = $request->input('rPisos');
      $rowsEspacio['rParedes'] = $request->input('rParedes');
      $rowsEspacio['rPuertas'] = $request->input('rPuertas');
      $rowsEspacio['rCerramiento'] = $request->input('rCerramiento');
      $rowsEspacio['rCubierta'] = $request->input('rCubierta');
      $observacion = $request->input('observacion');
      $lote = $request->input('Lote');
      $estudios = $request->input('EstudiosDT');
      $repototal = $request->input('RepoTotal');
      $terminacion = $request->input('Terminacion');
      $name="";
      if($request->hasFile('anexoEvidencia')){
        $file=$request->file('anexoEvidencia');
        $name=time().$file->getClientOriginalName();
        $file->move(public_path().'/AnexosIF/',$name);
      }
      $user = $request->input('userSolicitud');
      $solicitud=new SolicitudIF();
      $solicitud->DaneEstablecimiento = $establecimiento;
      $solicitud->DaneSede = $sede;
      $now = new \DateTime();
      $now->format('d-m-Y');
      $solicitud->Fecha = $now;
      $solicitud->Observacion = $observacion;
      $solicitud->Usuario = $user;
      $solicitud->Anexo = $name;
      $solicitud->Lote = $lote;
      $solicitud->DisenioTecnico = $estudios;
      $solicitud->ReposicionTotal = $repototal;
      $solicitud->TerminacionInconclusa = $terminacion;
      $solicitud->Atendida = false;

      $solicitud->save();
      if (!empty($request->input('rEspacio'))){
        for ($i=0; $i <count($rowsEspacio['rEspacio']); $i++) {
          $solEstado = new SolicitudEspacio();
          $solEstado->SolicitudId = $solicitud->id;
          $solEstado->TipoEspacio = $rowsEspacio['rEspacio'][$i];
          $solEstado->Cantidad = $rowsEspacio['rCantidad'][$i];
          $solEstado->TipoSolicitud = $rowsEspacio['rTipo'][$i];
          $solEstado->EstadoEspacio = $rowsEspacio['rEstado'][$i];
          $solEstado->RedElectrica = $rowsEspacio['rElectricidad'][$i];
          $solEstado->Acueducto = $rowsEspacio['rAcueducto'][$i];
          $solEstado->Alcantarillado = $rowsEspacio['rAlcantarillado'][$i];
          $solEstado->Piso = $rowsEspacio['rPisos'][$i];
          $solEstado->Pared = $rowsEspacio['rParedes'][$i];
          $solEstado->Puerta = $rowsEspacio['rPuertas'][$i];
          $solEstado->Cerramiento = $rowsEspacio['rCerramiento'][$i];
          $solEstado->Cubierta = $rowsEspacio['rCubierta'][$i];
          $solEstado->save();
        }

      }

      $nombres = temp_historico_sede::where("codigo_sede","=",$sede)->get(["nombre_sede","codigo_sede"]);

      return view("mensajes.mensaje_validacion")->with("nombres",$nombres);

    }else{

      $rowsEspacio['rEspacio'] = $request->input('rEspacio');
      $rowsEspacio['rCantidad'] = $request->input('rCantidad');
      $rowsEspacio['rEstado'] = $request->input('rEstado');
      $rowsEspacio['rTipo'] = $request->input('rTipo');
      $rowsEspacio['rElectricidad'] = $request->input('rElectricidad');
      $rowsEspacio['rAcueducto'] = $request->input('rAcueducto');
      $rowsEspacio['rAlcantarillado'] = $request->input('rAlcantarillado');
      $rowsEspacio['rPisos'] = $request->input('rPisos');
      $rowsEspacio['rParedes'] = $request->input('rParedes');
      $rowsEspacio['rPuertas'] = $request->input('rPuertas');
      $rowsEspacio['rCerramiento'] = $request->input('rCerramiento');
      $rowsEspacio['rCubierta'] = $request->input('rCubierta');
      $observacion = $request->input('observacion');
      $lote = $request->input('Lote');
      $estudios = $request->input('EstudiosDT');
      $repototal = $request->input('RepoTotal');
      $terminacion = $request->input('Terminacion');
      $name="";
      if($request->hasFile('anexoEvidencia')){
        $file=$request->file('anexoEvidencia');
        $name=time().$file->getClientOriginalName();
        $file->move(public_path().'/AnexosIF/',$name);
      }
      $user = $request->input('userSolicitud');
      $solicitud=new SolicitudIF();
      $solicitud->DaneEstablecimiento = $establecimiento;
      $solicitud->DaneSede = $sede;
      $now = new \DateTime();
      $now->format('d-m-Y');
      $solicitud->Fecha = $now;
      $solicitud->Observacion = $observacion;
      $solicitud->Usuario = $user;
      $solicitud->Anexo = $name;
      $solicitud->Lote = $lote;
      $solicitud->DisenioTecnico = $estudios;
      $solicitud->ReposicionTotal = $repototal;
      $solicitud->TerminacionInconclusa = $terminacion;
      $solicitud->Atendida = false;

      $solicitud->save();
      if (!empty($request->input('rEspacio'))){
        for ($i=0; $i <count($rowsEspacio['rEspacio']); $i++) {
          $solEstado = new SolicitudEspacio();
          $solEstado->SolicitudId = $solicitud->id;
          $solEstado->TipoEspacio = $rowsEspacio['rEspacio'][$i];
          $solEstado->Cantidad = $rowsEspacio['rCantidad'][$i];
          $solEstado->TipoSolicitud = $rowsEspacio['rTipo'][$i];
          $solEstado->EstadoEspacio = $rowsEspacio['rEstado'][$i];
          $solEstado->RedElectrica = $rowsEspacio['rElectricidad'][$i];
          $solEstado->Acueducto = $rowsEspacio['rAcueducto'][$i];
          $solEstado->Alcantarillado = $rowsEspacio['rAlcantarillado'][$i];
          $solEstado->Piso = $rowsEspacio['rPisos'][$i];
          $solEstado->Pared = $rowsEspacio['rParedes'][$i];
          $solEstado->Puerta = $rowsEspacio['rPuertas'][$i];
          $solEstado->Cerramiento = $rowsEspacio['rCerramiento'][$i];
          $solEstado->Cubierta = $rowsEspacio['rCubierta'][$i];
          $solEstado->save();
        }

      }

      return view("mensajes.msj_solicitudRegistrada");
    }

  }

  public function RegMunicipio(Request $request, $daneMunicipio){

    $SolicitudIF = SolicitudIF::whereHas('Establecimiento', function($q) {
      $q->where("codigo_dane_municipio", "=", Auth::user()->name);
    })->where("Atendida", "=", "false")->orderBy('Fecha', 'desc')->paginate(25);

    $SolicitudIFAT = SolicitudIF::whereHas('Establecimiento', function($q) {
      $q->where("codigo_dane_municipio", "=", Auth::user()->name);
    })->where("Atendida", "=", "true")->orderBy('Fecha', 'desc')->paginate(25);

    return view('requerimientoife.reqconsultamun',array('SolicitudIF' => $SolicitudIF, 'SolicitudIFAT' => $SolicitudIFAT));

  }



  public function RegEstablecimiento(Request $request, $codigo_establecimiento){
    $establecimiento = temp_historico_establecimiento::all()->where("codigo_establecimiento", "=", $codigo_establecimiento);

    $SolicitudIF = SolicitudIF::where("DaneEstablecimiento", "=", $codigo_establecimiento)->where("Atendida", "=", "false")->orderBy('Fecha', 'desc')->paginate(25);
    $SolicitudIFAT = SolicitudIF::where("DaneEstablecimiento", "=", $codigo_establecimiento)->where("Atendida", "=", "true")->orderBy('Fecha', 'desc')->paginate(25);

    return view('requerimientoife.reqconsultaest',array('SolicitudIF' => $SolicitudIF, 'SolicitudIFAT' => $SolicitudIFAT, 'establecimiento' => $establecimiento));
  }

  public function RegSede(Request $request, $codigo_sede){
    $sede = temp_historico_sede::all()->where("codigo_sede", "=", $codigo_sede);

    $SolicitudIF = SolicitudIF::where("DaneSede", "=", $codigo_sede)->where("Atendida", "=", "false")->orderBy('Fecha', 'desc')->paginate(25);
    $SolicitudIFAT = SolicitudIF::where("DaneSede", "=", $codigo_sede)->where("Atendida", "=", "true")->orderBy('Fecha', 'desc')->paginate(25);

    return view('requerimientoife.reqconsultasede',array('SolicitudIF' => $SolicitudIF, 'SolicitudIFAT' => $SolicitudIFAT, 'sede' => $sede));
  }

  public function RegGen(Request $request){

    $SolicitudIF = SolicitudIF::where("Atendida", "=", "false")->orderBy('Fecha', 'desc');
    $SolicitudIFAT = SolicitudIF::where("Atendida", "=", "true")->orderBy('Fecha', 'desc');

    $SolicitudIFAT = $SolicitudIFAT->paginate(25);
    $SolicitudIF = $SolicitudIF->paginate(25);



    return view('requerimientoife.reqconsultagen',array('SolicitudIF' => $SolicitudIF, 'SolicitudIFAT' => $SolicitudIFAT));
  }

  public function formEliminaSolicitudIF($id){
    return view("confirmaciones.form_borrado_solicitudesif")->with("id",$id);

  }


//Función encargada de eliminar el resgitro del diagnóstico de necesidades, sino se procede a ingresar la información de los riesgos de la sede educativa.
  public function eliminaSolicitudIF(Request $request){
    $id = $request->input('selSede');

    $SolicitudIF = true;
    $SolicitudEspacio = true;
    $RespuestaSolicitud = true;

    $SolicitudIF = SolicitudIF::where("DaneSede", "=", $id)->delete();
    if($SolicitudIF && $SolicitudEspacio && $RespuestaSolicitud){
      return view("home");
    }
    else
    {
      return view("mensajes.mensaje_error")->with("msj","Ocurrió un error, recarga la página e intente nuevamente");
    }

  }


  public function eliminaSolicitudIFDelete(Request $request){
    $id = $request->input('id');
    $solicitud = SolicitudIF::find($id);

    $SolicitudIF = true;
    $SolicitudEspacio = true;
    $RespuestaSolicitud = true;

    $SolicitudIF = SolicitudIF::where("id", "=", $id)->delete();
    if($SolicitudIF && $SolicitudEspacio && $RespuestaSolicitud){
      return view("mensajes.msj_solicitud_eliminada")->with("msj","Solicitud eliminada correctamente");
    }
    else
    {
      return view("mensajes.mensaje_error")->with("msj","Ocurrió un error al eliminar, recarga la página e intente nuevamente");
    }

  }

  public function verDetalleSolicitud($id){
    $SolicitudIF = SolicitudIF::all()->where("id", "=", $id);
    return view("requerimientoife.reqconsultadetalle", array('SolicitudIF' => $SolicitudIF));
  }

  public function updateSolicitud(Request $request){
    $id = $request->input('valueupdate');
    $solicitud = SolicitudIF::find($id);
    $solicitud->Atendida = true;
    $solicitud->save();
    $SolicitudIF = SolicitudIF::all()->where("id", "=", $id);
    return view("requerimientoife.reqconsultadetalle", array('SolicitudIF' => $SolicitudIF));
  }

  public function RegistrarRespuesta(Request $request){
    $form = $request->input('tipoForm');
    $SolicitudId = $request->input('idSolicitud');
    $mensaje = $request->input('RespuestaSolicitudIF');
    $url = $request->input('urlActual');
    if($mensaje == ""){

    }elseif($form == 1){
      $respuesta = new RespuestaSolicitudIF();
      $respuesta->SolicitudId = $SolicitudId;
      $respuesta->Mensaje = $mensaje;
      $respuesta->VistoRector = false;
      $respuesta->VistoSecretario = false;
      $now = new \DateTime();
      $now->format('d-m-Y');
      $respuesta->Fecha = $now;
      $respuesta->save();
    }else{
      $idRespuesta = $request->input('RespuestaId');
      $respuesta = RespuestaSolicitudIF::find($idRespuesta);
      if($respuesta != null){
        $respuesta->Mensaje = $mensaje;
        $respuesta->VistoRector = false;
        $respuesta->VistoSecretario = false;
        $now = new \DateTime();
        $now->format('d-m-Y');
        $respuesta->Fecha = $now;
        $respuesta->save();
      }
    }
    header("Location:$url");
    die();
    //return $this->verDetalleSolicitud($SolicitudId);

  }

  public function vistoRequerimiento(Request $request){
    $idRespuesta = $request->input('RespuestaId');
    $respuesta = RespuestaSolicitudIF::find($idRespuesta);

    if(Auth::user()->isRole('secretarios')){
      $respuesta->VistoSecretario = true;
    }elseif (Auth::user()->isRole('rectores')) {
      $respuesta->VistoRector = true;
    }
    $respuesta->save();
  }

  public function DescargaAnexo(Request $request, $anexo){
    $filename=public_path().'/AnexosIF/'.$anexo;
    $finfo=finfo_open(FILEINFO_MIME_TYPE);
    $mimeType=finfo_file($finfo, $filename);

    header("Content-type:".$mimeType);
    header("Content-Disposition: attachment");
    echo file_get_contents($filename);
  }

  public  function objectToArray($d) {
    if (is_object($d)) {
      $d = get_object_vars($d);
    }

    if (is_array($d)) {
      return array_map('trim', $d);
    }
    else {
      return $d;
    }
  }
}
