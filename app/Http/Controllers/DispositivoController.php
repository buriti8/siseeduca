<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Dispositivo;
use App\TipoDispositivo;
use App\OrigenDispositivo;
use App\Contrato;
use App\User;
use App\HistoricoSede;
use App\TempArchivoDispositivos;
use App\temp_historico_establecimiento;
use App\Subregion;
use App\Traslado;
use App\temp_historico_sede;



use App\SiegaTrait\SedesTrait;

use Illuminate\Support\Facades\Auth;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Storage;
use Datatables;
use Carbon\Carbon;

class DispositivoController extends Controller
{
  use SedesTrait;

  /**
  * Create a new controller instance.
  *
  * @return void
  */
  public function __construct()
  {
    $this->middleware('auth');
  }

  public function listado_dispositivos()
  {
    return view('inventario.listado_dispositivos');
  }

  public function lista_dispositivos_existentes()
  {
    if(Auth::user()->isRole('secretarios'))
    {
      $datos = DB::table('dispositivos')
      ->leftJoin('temp_historico_sedes', 'dispositivos.sede_id', '=', 'temp_historico_sedes.codigo_sede')
      ->join('tipo_dispositivos', 'dispositivos.tipo_id', '=', 'tipo_dispositivos.id')
      ->select('dispositivos.id', 'dispositivos.sede_id', 'temp_historico_sedes.nombre_sede', 'tipo_dispositivos.nombre', 'marca', 'serial', 'estado', 'motivo')
      ->where('temp_historico_sedes.codigo_dane_municipio', Auth::user()->name)
      ->get();
    }
    elseif (Auth::user()->isRole('rectores'))
    {
      $datos = DB::table('dispositivos')
      ->leftJoin('temp_historico_sedes', 'dispositivos.sede_id', '=', 'temp_historico_sedes.codigo_sede')
      ->join('tipo_dispositivos', 'dispositivos.tipo_id', '=', 'tipo_dispositivos.id')
      ->select('dispositivos.id', 'dispositivos.sede_id', 'temp_historico_sedes.nombre_sede', 'tipo_dispositivos.nombre', 'marca', 'serial', 'estado', 'motivo')
      ->where('temp_historico_sedes.codigo_establecimiento', Auth::user()->name)
      ->get();
    }
    else
    {
      $datos = DB::table('dispositivos')
      ->leftJoin('temp_historico_sedes', 'dispositivos.sede_id', '=', 'temp_historico_sedes.codigo_sede')
      ->join('tipo_dispositivos', 'dispositivos.tipo_id', '=', 'tipo_dispositivos.id')
      ->select('dispositivos.id', 'dispositivos.sede_id',  'temp_historico_sedes.nombre_sede' ,  'tipo_dispositivos.nombre', 'marca', 'serial', 'estado', 'motivo')
      ->get();
    }

    return Datatables::of($datos)->make(true);
  }

  public function form_nuevo_dispositivo()
  {
    $tipos = TipoDispositivo::orderBy('id', 'asc')->get();
    $origenes = OrigenDispositivo::orderBy('id', 'asc')->get();
    $contratos = Contrato::orderBy('id', 'asc')->get();
    $subregiones = Subregion::orderBy('id', 'asc')->get();

    return view('formularios.form_nuevo_dispositivo')->with("tipos", $tipos)
    ->with("origenes", $origenes)
    ->with("contratos", $contratos)
    ->with("subregiones", $subregiones);
  }

  public function crear_dispositivo(Request $request)
  {
    //
    $reglasDispo=[ 'inversion' => 'bail|nullable|integer',
    'numero_factura' => 'bail|nullable|integer'];

    $mensajesDispo=[ 'inversion.integer' => 'El valor unitario del equipo no puede contener comas ni puntos',
    'numero_factura.integer' => 'El número de la factura no puede contener comas ni puntos'];

    $validadorDispo = Validator::make($request->all(), $reglasDispo, $mensajesDispo);
    if( $validadorDispo->fails())
    {
      return view("mensajes.mensaje_error_evidencia_dispositivos")->with("msj","Hay valores no permitidos")
      ->withErrors($validadorDispo->errors());
    }
    //

    $dis = Dispositivo::where("serial", $request->input("serial"))->first();
    if(!$dis)
    {
      $dispositivo = Dispositivo::create([
        "sede_id" => $request->input("sede_id"),
        "tipo_id" => $request->input("tipo_id"),
        "serial" => strtoupper($request->input("serial")),
        "origen_id" => $request->input("origen_id"),
        "acta" => $request->input("acta"),
        "radicado" => $request->input("radicado"),
        "inversion" => $request->input("inversion"),
        "proveedor" => strtoupper($request->input("proveedor")),
        "marca" => strtoupper($request->input("marca")),
        "referencia" => strtoupper($request->input("referencia")),
        "fecha_entrega" => $request->input("fecha_entrega"),
        "fecha_fin_garantia" => $request->input("fecha_fin_garantia"),
        "numero_factura" => $request->input("numero_factura"),
        "contrato_id" => $request->input("contrato_id"),
        "procesador" => strtoupper($request->input("procesador")),
        "sistema_operativo" => strtoupper($request->input("sistema_operativo")),
        "memoria_ram" => $request->input("memoria_ram"),
        "disco_duro" => $request->input("disco_duro"),
        "estado" => 0,
        "observaciones" => strtoupper($request->input("observaciones")),
        "motivo" => strtoupper($request->input("motivo")),
        "evidencia" => $request->input("evidencia"),
        "fecha_baja" => $request->input("fecha_baja"),
        "numero_carga" => 0]);

        if($dispositivo)
        {
          return view("mensajes.msj_dispositivo_creado")->with("msj","Dispositivo agregado correctamente.");
        }
        else
        {
          return view("mensajes.mensaje_error_2")->with("msj","...Hubo un error al agregar ;...") ;
        }
      }
      else
      {
        return view("mensajes.mensaje_error_2")->with("msj", "El dispositivo con serial " . $request->input("serial") . " ya existe.");
      }
    }

    public function form_editar_dispositivo($id)
    {
      $dispositivo = Dispositivo::find($id);
      $tipos = TipoDispositivo::orderBy('id', 'asc')->get();
      $origenes = OrigenDispositivo::orderBy('id', 'asc')->get();
      $contratos = Contrato::orderBy('id', 'asc')->get();

      $nombre_sede = $dispositivo->temphistoricosedes->nombre_sede;
      $direccion = $dispositivo->temphistoricosedes->direccion;

      return view("formularios.form_editar_dispositivo")->with("dispositivo", $dispositivo)
      ->with("tipos", $tipos)
      ->with("origenes", $origenes)
      ->with("contratos", $contratos)
      ->with("nombre_sede", $nombre_sede)
      ->with("direccion", $direccion);
    }

    public function editar_dispositivo(Request $request, $id)
    {
      //
      $reglasDispo=[ 'inversion' => 'bail|nullable|integer',
      'numero_factura' => 'bail|nullable|integer'];

      $mensajesDispo=[ 'inversion.integer' => 'El valor unitario del equipo no puede contener comas ni puntos',
      'numero_factura.integer' => 'El número de la factura no puede contener comas ni puntos'];

      $validadorDispo = Validator::make($request->all(), $reglasDispo, $mensajesDispo);
      if( $validadorDispo->fails())
      {
        return view("mensajes.mensaje_error_evidencia_dispositivos")->with("msj","Hay valores no permitidos")
        ->withErrors($validadorDispo->errors());
      }
      //

      $dispositivo = Dispositivo::find($id);

      if($request->input('cbfuncionamiento') == '1')
      {
        $reglas=[  'evidencia' => 'required|mimes:pdf',
      ];

      $mensajes=[  'evidencia.mimes' => 'El archivo seleccionado en Dispositivos no corresponde al formato permitido (pdf)'
    ];

    $validator0 = Validator::make( $request->all(),$reglas,$mensajes );
    if( $validator0->fails() ){
      return view("mensajes.mensaje_error_evidencia_dispositivos")->with("msj","...Existen errores...")
      ->withErrors($validator0->errors());
    }

    $dispositivo->motivo = $request->input("motivo");

    if($request->hasFile('evidencia'))
    {
      $file = $request->file('evidencia');
      $name = $file->getClientOriginalName();
      $file->move(public_path().'/evidencias', $name);

      $dispositivo->evidencia = '/evidencias/' . $name;

      $dispositivo->fecha_baja = Carbon::now();
    }
  }
  else
  {
    $dispositivo->motivo = "";
    $dispositivo->evidencia = "";
    $dispositivo->fecha_baja = null;
  }

  //sede_id no se modifica.
  $dispositivo->tipo_id = $request->input("tipo_id");
  $dispositivo->serial = strtoupper($request->input("serial"));
  $dispositivo->origen_id = $request->input("origen_id");
  $dispositivo->acta = strtoupper($request->input("acta"));
  $dispositivo->radicado = strtoupper($request->input("radicado"));
  $dispositivo->inversion = $request->input("inversion");
  $dispositivo->proveedor = strtoupper($request->input("proveedor"));
  $dispositivo->marca = strtoupper($request->input("marca"));
  $dispositivo->referencia = strtoupper($request->input("referencia"));
  $dispositivo->fecha_entrega = $request->input("fecha_entrega");
  $dispositivo->fecha_fin_garantia = $request->input("fecha_fin_garantia");
  $dispositivo->numero_factura = $request->input("numero_factura");
  $dispositivo->contrato_id = $request->input("contrato_id");
  $dispositivo->procesador = strtoupper($request->input("procesador"));
  $dispositivo->sistema_operativo = strtoupper($request->input("sistema_operativo"));
  $dispositivo->memoria_ram = $request->input("memoria_ram");
  $dispositivo->disco_duro = $request->input("disco_duro");
  $dispositivo->estado = $request->input('cbfuncionamiento');
  $dispositivo->observaciones = strtoupper($request->input("observaciones"));

  if($dispositivo->save())
  {
    return view("mensajes.msj_actualizado_dispositivo")->with("msj","Dispositivo actualizado correctamente");
  }
  else
  {
    return view("mensajes.mensaje_error_2")->with("msj","..Hubo un error al agregar ; intentarlo nuevamente..");
  }
}

public function form_borrado_dispositivo($id)
{
  $dispositivo = Dispositivo::find($id);
  return view("confirmaciones.form_borrado_dispositivo_individual")->with("dispositivo",$dispositivo);
}

public function borrar_dispositivo(Request $request)
{
  $traslados = DB::table('traslados')->where('dispositivo_id', '=', $request->id_dispositivo);
  if($traslados->count() == 0)
  {
    $dispositivo = Dispositivo::find($request->input("id_dispositivo"));

    if($dispositivo->delete()){
      return view("mensajes.msj_borrado");
    }
    else
    {
      return view("mensajes.mensaje_error_2")->with("msj","..Hubo un error al borrar ; intentarlo nuevamente..");
    }
  }
  else
  {
    return view("mensajes.mensaje_error")->with('msj', 'El dispositivo no se puede borrar debido a que está vinculado al menos a un traslado.');
  }
}

public function baja_dispositivo()
{
  return view('formularios.form_dar_baja');
}

public function form_carga_masiva_dispositivos()
{
  DB::statement('refresh materialized view registro_archivos_cargados_dispositivos');
  $lista = DB::table('registro_archivos_cargados_dispositivos')
  ->where('numero_carga', '>' , 0)
  ->select('numero_carga','fecha','cantidad')
  ->paginate(10);
  return view("inventario.carga_masiva_dispositivos")->with("lista",$lista);
}

//Subir
public function subir_archivos(Request $request)
{
  $reglas=['archivo_dispositivos' => 'required|mimes:csv,txt'];

  $mensajes=[  'archivo_dispositivos.mimes' => 'El archivo seleccionado en Dispositivos no corresponde al formato permitido (csv)'
];

$validator0 = Validator::make( $request->all(),$reglas,$mensajes );
if( $validator0->fails() ){
  return view("mensajes.mensaje_error")->with("msj","...Existen errores...")
  ->withErrors($validator0->errors());
}

$dispositivos = $request->file('archivo_dispositivos');

$nuevo_nombre_dispositivos ="dispositivos.csv";

Storage::disk('temp')->put($nuevo_nombre_dispositivos,  \File::get($dispositivos) );
$archivo_dispositivos = Storage::disk('temp')->getDriver()->getAdapter()->applyPathPrefix('dispositivos.csv');

$reglas_dispositivos=[
  'sede_id' => 'required',
  'tipo_id' => 'required',
  'serial'  => 'required',
  'origen_id' => 'required',
  'acta'      => 'required',
  'radicado'  => 'required',
  'inversion' => 'required',
  'proveedor' => 'required',
  'marca' => 'required',
  'referencia' => 'required',
  'fecha_entrega' => 'required',
  'fecha_fin_garantia' => 'required',
  'numero_factura' => 'required',
  'contrato_id' => 'required',
  'procesador' => 'required',
  'sistema_operativo' => 'required',
  'memoria_ram' => 'required',
  'disco_duro' => 'required',
  'estado' => 'required',
  'observaciones' => 'required',
  'motivo' => 'required',
  'evidencia' => 'required',
  'fecha_baja' => 'required',
];

$mensajes1=[  'required'=>'La columna :attribute no se encuentra en el encabezado del archivo cargado o no está escrito correctamente ',
];

$validator1 = Validator::make( $this->validar_header_dispositivos($archivo_dispositivos),$reglas_dispositivos,$mensajes1 );
if( $validator1->fails() )
{
  return view("mensajes.mensaje_error")->with("msj","...Existen errores en el archivo dispositivos...")
  ->withErrors($validator1->errors());
}

$valor = $this->carga_masiva_dispositivos($archivo_dispositivos);

Storage::disk('temp')->delete($nuevo_nombre_dispositivos);

if($valor == 1)
{
  $mensaje = "Carga exitosa";
  return view("mensajes.msj_dispositivos_cargados")->with("msj", $mensaje)
  ->with("valor", 1);
}
else if($valor == 2)
{
  $mensaje = "No es posible hacer la carga debido a que hay valores en la columna tipo_id del archivo que no se relacionan con tipos de dispositivos";
  return view("mensajes.msj_dispositivos_error1")->with("msj", $mensaje)
  ->with("valor", 0);
}
else if($valor == 3)
{
  $mensaje = "No es posible hacer la carga debido a que hay valores en la columna fuente u origen_id del archivo que no se relacionan con fuentes de dspositivos";
  return view("mensajes.msj_dispositivos_error2")->with("msj", $mensaje)
  ->with("valor", 0);
}
else if($valor == 4)
{
  $mensaje = "No es posible hacer la carga debido a que hay valores en la columna contrato_id del archivo que no se relacionan con contratos";
  return view("mensajes.msj_dispositivos_error3")->with("msj", $mensaje)
  ->with("valor", 0);
}
else if($valor == 5)
{
  $mensaje = "No es posible hacer la carga debido a que ya existen dispositivos que están incluidos en el archivo";
  return view("mensajes.msj_dispositivos_error4")->with("msj", $mensaje)
  ->with("valor", 0);
}
else if($valor == 6)
{
  $mensaje = "El archivo para cargar dispositivos no contiene datos";
  return view("mensajes.msj_dispositivos_error5")->with("msj", $mensaje)
  ->with("valor", 0);
}
else if($valor == 7)
{
  $mensaje = "No existen sedes en el archivo a cargar";
  return view("mensajes.msj_dispositivos_error6")->with("msj", $mensaje)
  ->with("valor", 0);
}

else
{
  DB::delete('delete from temp_archivos_dispositivos');
  $mensaje = "No es posible hacer la carga.";
  return view("mensajes.msj_dispositivos_error7")->with("msj", $mensaje)
  ->with("valor", 0);
}
}
// Fin subir

//Carga masiva
public function carga_masiva_dispositivos($archivo)
{
  try
  {
    DB::delete('delete from temp_archivos_dispositivos');
    DB::statement("COPY temp_archivos_dispositivos FROM '$archivo' DELIMITER ';' CSV HEADER;");

    $count_temporal = DB::table('temp_archivos_dispositivos')->count();
    if($count_temporal != 0)
    {
      //VERIFICA QUE LAS SEDES EXISTAN
      $dispositivo_temp = DB::table('temp_archivos_dispositivos')
      ->join('temp_historico_sedes', 'temp_archivos_dispositivos.sede_id', '=', 'temp_historico_sedes.codigo_sede')
      ->select('temp_historico_sedes.codigo_sede')
      ->get()
      ->count();

      if($dispositivo_temp != $count_temporal)
      {
        DB::delete('delete from temp_archivos_dispositivos');
        return 7;
      }

      $lista_disp = DB::table('dispositivos')
      ->join('temp_archivos_dispositivos', 'dispositivos.serial', '=', 'temp_archivos_dispositivos.serial')
      ->select('dispositivos.serial')
      ->get();
      if($lista_disp->count() == 0)
      {



        //Tipos dispositivos OK
        $busqueda_tipos = DB::table('temp_archivos_dispositivos')
        ->whereNotIn('tipo_id',function ($query) {
          $query->select('id')->from('tipo_dispositivos');
        })->get();
        if($busqueda_tipos->count() != 0)
        {
          DB::delete('delete from temp_archivos_dispositivos');
          return 2;
        }
        //Origenes de dispositivos
        $busqueda_origenes = DB::table('temp_archivos_dispositivos')
        ->whereNotIn('origen_id',function ($query) {
          $query->select('id')->from('origen_dispositivos');
        })->get();
        if($busqueda_origenes->count() != 0)
        {
          DB::delete('delete from temp_archivos_dispositivos');
          return 3;
        }
        //Contratos
        $busqueda_contratos = DB::table('temp_archivos_dispositivos')
        ->whereNotIn('contrato_id',function ($query) {
          $query->select('id')->from('contratos');
        })->get();
        if($busqueda_contratos->count() != 0)
        {
          $count_contratos = Contrato::all()->count();
          if($count_contratos != 0)
          {
            DB::delete('delete from temp_archivos_dispositivos');
            return 4;
          }
        }

        $max = DB::table('dispositivos')->max('numero_carga');
        $num_carga = $max + 1;
        //La variable $num_carga es para asignar un número de carga y now() para saber la fecha de la carga.
        DB::table('temp_archivos_dispositivos')->update(['numero_carga' => $num_carga, 'created_at' => Carbon::now()]);

        DB::statement("insert into dispositivos(sede_id,tipo_id,serial,origen_id,acta,radicado,inversion,proveedor,marca,referencia,fecha_entrega,fecha_fin_garantia,
        numero_factura,contrato_id,procesador,sistema_operativo,memoria_ram,disco_duro,estado,observaciones,motivo,evidencia,fecha_baja,numero_carga,created_at,updated_at)
        select * from temp_archivos_dispositivos;");

        DB::delete('delete from temp_archivos_dispositivos');
        DB::statement('refresh materialized view registro_archivos_cargados_dispositivos;');
        return 1;
      }
      else
      {
        DB::delete('delete from temp_archivos_dispositivos');
        return 5;
      }
    }
    else
    {
      return 6;
    }
  }
  catch (Exception $e)
  {
    report($e);
    DB::delete('delete from temp_archivos_dispositivos');
    DB::statement('refresh materialized view registro_archivos_cargados_dispositivos;');
    return 0;
  }
}
//Fin carga masiva

public function validar_header_dispositivos($archivo)
{
  $opened_file = fopen($archivo, 'r');
  $header = fgetcsv($opened_file, 0, ';');
  $header_convert_trim = array_map('trim', $header);

  $sede_id =  $this->getColumnNameByValue($header_convert_trim,'sede_id');
  $tipo_id  =  $this->getColumnNameByValue($header_convert_trim,'tipo_id');
  $serial = $this->getColumnNameByValue($header_convert_trim,'serial');
  $origen_id = $this->getColumnNameByValue($header_convert_trim,'origen_id');
  $acta = $this->getColumnNameByValue($header_convert_trim,'acta');
  $radicado = $this->getColumnNameByValue($header_convert_trim,'radicado');
  $inversion= $this->getColumnNameByValue($header_convert_trim,'inversion');
  $proveedor= $this->getColumnNameByValue($header_convert_trim,'proveedor');
  $marca = $this->getColumnNameByValue($header_convert_trim,'marca');
  $referencia = $this->getColumnNameByValue($header_convert_trim,'referencia');
  $fecha_entrega  = $this->getColumnNameByValue($header_convert_trim,'fecha_entrega');
  $fecha_fin_garantia = $this->getColumnNameByValue($header_convert_trim,'fecha_fin_garantia');
  $numero_factura = $this->getColumnNameByValue($header_convert_trim,'numero_factura');
  $contrato_id = $this->getColumnNameByValue($header_convert_trim,'contrato_id');
  $procesador = $this->getColumnNameByValue($header_convert_trim,'procesador');
  $sistema_operativo = $this->getColumnNameByValue($header_convert_trim,'sistema_operativo');
  $memoria_ram = $this->getColumnNameByValue($header_convert_trim,'memoria_ram');
  $disco_duro = $this->getColumnNameByValue($header_convert_trim,'disco_duro');
  $estado = $this->getColumnNameByValue($header_convert_trim,'estado');
  $observaciones = $this->getColumnNameByValue($header_convert_trim,'observaciones');
  $motivo = $this->getColumnNameByValue($header_convert_trim,'motivo');
  $evidencia = $this->getColumnNameByValue($header_convert_trim,'evidencia');
  $fecha_baja = $this->getColumnNameByValue($header_convert_trim,'fecha_baja');

  fclose($opened_file);

  $validation_array = [
    'sede_id'=>$sede_id,
    'tipo_id'=>$tipo_id,
    'serial'=>$serial,
    'origen_id'=>$origen_id,
    'acta'=>$acta,
    'radicado'=>$radicado,
    'inversion'=>$inversion,
    'proveedor'=>$proveedor,
    'marca'=>$marca,
    'referencia'=>$referencia,
    'fecha_entrega'=>$fecha_entrega,
    'fecha_fin_garantia'=>$fecha_fin_garantia,
    'numero_factura'=>$numero_factura,
    'contrato_id'=>$contrato_id,
    'procesador'=>$procesador,
    'sistema_operativo'=>$sistema_operativo,
    'memoria_ram'=>$memoria_ram,
    'disco_duro'=>$disco_duro,
    'estado'=>$estado,
    'observaciones'=>$observaciones,
    'motivo'=>$motivo,
    'evidencia'=>$evidencia,
    'fecha_baja'=>$fecha_baja,
  ];
  return $validation_array;
}



public function getColumnNameByValue($array, $value)
{
  return in_array($value, $array)? $value : '';
}

public function form_borrado_dispositivos($num_carga)
{
  return view("confirmaciones.form_borrado_dispositivos")->with("num_carga",$num_carga);
}

public function borrar_archivos_dispositivos(Request $request)
{
  $carga = $request->input("numero_carga");
  $lista = DB::table('dispositivos')->where('numero_carga', '=' , $carga)->count();

  if($lista > 0)
  {
    $res = DB::table('dispositivos')->where('numero_carga', '=', $carga)->delete();
    if($res)
    {
      DB::delete('delete from temp_archivos_dispositivos');
      DB::statement('refresh materialized view registro_archivos_cargados_dispositivos;');
      return view("mensajes.msj_borrado");
    }
    else
    {
      return view("mensajes.mensaje_error")->with("msj","..Hubo un error al borrar ; intentarlo nuevamente..");
    }
  }
  else
  {
    return view("mensajes.mensaje_error")->with("msj","La carga " . $carga . " ya fué borrada. Actualize el listado.");
  }
}

public function DescargarPlantillaDispositivos()
{
  $filename=public_path().'/plantilla_dispositivos/dispositivos.csv';
  $finfo=finfo_open(FILEINFO_MIME_TYPE);
  $mimeType=finfo_file($finfo, $filename);

  header("Content-type:".$mimeType);
  header("Content-Disposition: attachment; filename=dispositivos.csv");
  echo file_get_contents($filename);
}

public function descargarEvidencia(Request $request, $archivo)
{
  $filename=public_path().'/evidencias/'.$archivo;
  $finfo=finfo_open(FILEINFO_MIME_TYPE);
  $mimeType=finfo_file($finfo, $filename);

  header("Content-type:".$mimeType);
  header("Content-Disposition: attachment; filename=evidencia.pdf");
  echo file_get_contents($filename);
}

}
