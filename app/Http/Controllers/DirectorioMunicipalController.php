<?php
namespace App\Http\Controllers;
use File;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Documentos;
use App\HistoricoEstablecimiento;
use App\temp_historico_sede;
use App\temp_historico_establecimiento;
use App\SedeConectividad;
use App\DirectorioMunicipal;
use App\SedesEspacio;
use App\Municipio;
use App\Rectores;
use App\ComitesJuntas;
use App\EnlacesTIC;
use App\DirectorNucleo;
use App\SecretarioPlaneacion;
use App\Comites;
use App\DocumentosMunicipios;
use App\Subregion;
use Illuminate\Support\Facades\Validator;
use Datatables;
use Storage;
use Illuminate\Support\Facades\Auth;

class DirectorioMunicipalController extends Controller
{

  //Función encargada de mostrar los documentos por municipio
  public function listado_documentos(){

    $datos = DocumentosMunicipios::select('fecha_documento','numero_documento','nombre_municipio','comite_junta','documento')->get();;

    return view("DirectorioMunicipal.documentos_directorio")->with("datos", $datos);

  }


  //Función encargada de la descarga y visualización de los documentos solicitados
  public function DescargarDocumento(Request $request, $documento){
    $filename=public_path().'/Directorio_municipal/'.$documento;
    $finfo=finfo_open(FILEINFO_MIME_TYPE);
    $mimeType=finfo_file($finfo, $filename);

    header('Content-type: application/pdf');
    header('Content-Disposition: inline; filename="' . $filename . '"');
    header('Content-Transfer-Encoding: binary');
    header('Content-Length: ' . filesize($filename));
    header('Accept-Ranges: bytes');

    readfile($filename);


  }


  //Función encargada de mostrar el formulario para el ingreso de los documentos.
  public function form_ingresar_documento(){

    if(Auth::user()->isRole('secretarios') || Auth::user()->isRole('enlace_tic') ){

      $municipios = Municipio::select('NombreMunicipio')->where('CodigoDaneMunicipio', '=',Auth::user()->name)->get();
      foreach ($municipios as $municipio) {
        $municipios = $municipio->NombreMunicipio;
      }

    }else{
      $municipios = Municipio::select('NombreMunicipio','CodigoDaneMunicipio')->get();
    }

    $datos = Documentos::select('codigo','documento')->get();

    return view("formularios.form_ingresar_documento")->with("municipios", $municipios)->with("datos", $datos);

  }


  //Función encargada del registro de los documentos en la base de datos y la carptea pública.
  public function ingresar_documentos(Request $request){

    if(Auth::user()->isRole('secretarios') || Auth::user()->isRole('enlace_tic') ){
      $dane_municipio=Auth::user()->name;
    }else{
      $dane_municipio=$request->input("selMunicipio");
    }

    $municipios = Municipio::select('NombreMunicipio','CodigoDaneMunicipio')->where('CodigoDaneMunicipio', '=', $dane_municipio)->get();
    foreach ($municipios as $municipio) {
      $CodigoDaneMunicipio = $municipio->CodigoDaneMunicipio;
    }

    $documentos = Documentos::select('documento')->where('codigo', '=', $request->input("selcomite"))->get();
    foreach ($documentos as $documento) {
      $nombredocumento = $documento->documento;
    }

    if(Auth::user()->isRole('enlace_tic')){
      $existencias = DocumentosMunicipios::select('nombre_municipio','comite_junta')
      ->where('codigo_municipio', '=', $CodigoDaneMunicipio)
      ->where('comite_junta', '=', $request->input("selcomite"))
      ->get();
    }else {
      $existencias = DocumentosMunicipios::select('nombre_municipio','comite_junta')
      ->where('codigo_municipio', '=', $CodigoDaneMunicipio)
      ->where('comite_junta', '=', $nombredocumento)
      ->get();
    }

    if (count($existencias)==0) {
      $name="";

      if ($request->hasFile('file')){
        $file=$request->file('file');
        $name=time().$file->getClientOriginalName();
        $file->move(public_path().'/Directorio_municipal/',$name);
      }

      $data = new DocumentosMunicipios;
      foreach ($municipios as $municipio) {
        $data->codigo_municipio = $municipio->CodigoDaneMunicipio;
        $data->nombre_municipio = $municipio->NombreMunicipio;
      }
      $data->documento = $name;
      $data->fecha_documento = $request->input("fecha_documento");
      $data->numero_documento = $request->input("numero_documento");
      if(Auth::user()->isRole('enlace_tic')){
        $data->comite_junta = $request->input("selcomite");
      }else {
        $data->comite_junta = $nombredocumento;
      }

      if($data->save()){
        return view("mensajes.msj_info_directorioingresada");
      }
      else
      {
        return view("mensajes.mensaje_error")->with("msj","Hubo un error al guardar, Por favor inténtalo nuevamente.");
      }
    }else{
      if(Auth::user()->isRole('enlace_tic')){
        $informacion=$request->input("selcomite");
      }else {
        $informacion= $nombredocumento;
      }

      return view("mensajes.msj_info_existenciaComites")->with("municipios",$municipios)->with("informacion",$informacion);
    }


  }


  //Función encarga de mostrar el formulario para la actualización de los documentos de los municipios
  public function form_editar_documentos($id){

    $datos = DocumentosMunicipios::select('id','nombre_municipio','comite_junta','documento')->where('id', '=', $id)->get();
    return view("formularios.form_editar_documentos")->with("datos", $datos);

  }


  //Función encargada de actualizar la información de los documentos de los municipios
  public function editar_documentos(Request $request){

    $id = $request->input("id");
    $name="";
    $data = DocumentosMunicipios::findOrFail($id);

    File::delete(public_path().'/Directorio_municipal/'.$data->documento);

    if ($request->hasFile('file')){
      $file=$request->file('file');
      $name=time().$file->getClientOriginalName();
      $file->move(public_path().'/Directorio_municipal/',$name);
      $data->documento = $name;
      $data->fecha_documento = $request->input("fecha_documento");
      $data->numero_documento = $request->input("numero_documento");
    }

    if($data->save()){
      return view("mensajes.msj_info_directorioingresada");
    }
    else
    {
      return view("mensajes.mensaje_error")->with("msj","Hubo un error al guardar, Por favor inténtalo nuevamente.");
    }
  }


  //Función encargada de mostrar el formulario para el borrado de la información de los documentos.
  public function form_borrado_documentos($value){

    $data = DocumentosMunicipios::findOrFail($value);

    return view("confirmaciones.form_borrado_documentos")->with("data",$data);


  }


  //Función encargada del borrado de la informació de los documentos.
  public function borrado_documentos(Request $request){

    $data = DocumentosMunicipios::findOrFail($request->input("documento_id"));

    File::delete(public_path().'/Directorio_municipal/'.$data->documento);

    if($data->delete()){
      return view("mensajes.msj_sede_actualizada_borrada");
    }
    else
    {
      return view("mensajes.mensaje_error")->with("msj","Hubo un error al borrar, Por favor inténtalo nuevamente");
    }

  }


  //Función encargada de mostrar al usuario la vista principal del directorio municipal.
  public function listado_directorio(){
    $contador=0;
    $DirectorioMunicipal="";
    $rectores="";
    $enlacetic="";
    $jefeplaneacion="";
    $informacionPersonal="";
    $Comitestic="";
    $Comitescupos="";
    $JUME="";
    $countEstablecimientos=0;
    $cantidadEstablecimientos=0;
    $documentoTIC = "";
    $documentoEnlaceTIC = "";
    $documentocupos = "";
    $documentoJUME = "";

    if(Auth::user()->isRole('rectores')){
      $cantidadEstablecimientos = Rectores::where('codigo_establecimiento', '=', Auth::user()->name)->count();

      if($cantidadEstablecimientos == 0){
        $contador+=1;
      }

    }



    if(Auth::user()->isRole('secretarios')){

      $countEstablecimientos = temp_historico_establecimiento::where('codigo_dane_municipio', '=',Auth::user()->name)->where('estado', '=', 'ANTIGUO-ACTIVO')->where('sector', '=', 'OFICIAL')->count();
      $countEstablecimientos += temp_historico_establecimiento::where('codigo_dane_municipio', '=', Auth::user()->name)->where('estado', '=', 'NUEVO-ACTIVO')->where('sector', '=', 'OFICIAL')->count();

      $cantidadEstablecimientos = Rectores::where('codigo_municipio', '=', Auth::user()->name)->count();

      if($countEstablecimientos > $cantidadEstablecimientos){
        $rectores = "rectores(a) de los establecimientos educativos,";
        $contador+=1;
      }

      $dataEnlaceTIC = DocumentosMunicipios::where('codigo_municipio', '=', Auth::user()->name)
      ->where('comite_junta','=','Enlace TIC')
      ->count();

      if($dataEnlaceTIC==0){
        $documentoEnlaceTIC = "documento del enlace TIC(Acta, decreto o carta)";
        $contador+=1;
      }


      $datosenlacetic = EnlacesTIC::where('codigo_municipio', '=', Auth::user()->name)->count();
      if($datosenlacetic==0){
        $enlacetic = "enlace TIC,";
        $contador+=1;
      }

      $datosenlacetic = DirectorioMunicipal::select('id')->where('codigo_municipio', '=', Auth::user()->name)->get();
      if(count($datosenlacetic)==0){
        $DirectorioMunicipal = "alcalde, administrador SIMAT y su información personal,";
        $contador+=1;
      }


      $datosenlacetic = SecretarioPlaneacion::select('id')->where('codigo_municipio', '=', Auth::user()->name)->get();
      if(count($datosenlacetic)==0){
        $jefeplaneacion = "secretario(a) de planeación,";
        $contador+=1;
      }


      $datoscomiteTIC = Comites::where('codigo_municipio', '=', Auth::user()->name)
      ->where('comite','=','TIC')
      ->count();
      if($datoscomiteTIC==0){
        $Comitestic = "comités TIC,";
        $contador+=1;
      }


      $datoscomiteCupos = Comites::where('codigo_municipio', '=', Auth::user()->name)
      ->where('comite','=','Cupos')
      ->count();
      if($datoscomiteCupos==0){
        $Comitescupos = "comités de cupos,";
        $contador+=1;
      }

      $datoscomiteJUME = Comites::where('codigo_municipio', '=', Auth::user()->name)
      ->where('comite','=','JUME')
      ->count();
      if($datoscomiteJUME==0){
        $JUME = "JUME,";
        $contador+=1;
      }


      $dataTIC = DocumentosMunicipios::where('codigo_municipio', '=', Auth::user()->name)
      ->where('comite_junta','=','Comité TIC')
      ->count();

      if($dataTIC==0){
        $documentoTIC = "documento del comité TIC(Acta, decreto o carta),";
        $contador+=1;
      }


      $datacupos = DocumentosMunicipios::where('codigo_municipio', '=', Auth::user()->name)
      ->where('comite_junta','=','Comité de Cupos')
      ->count();

      if($datacupos==0){
        $documentocupos = "documento del comité de Cupos(Acta, decreto o carta),";
        $contador+=1;
      }


      $dataJUME = DocumentosMunicipios::where('codigo_municipio', '=', Auth::user()->name)
      ->where('comite_junta','=','JUME')
      ->count();

      if($dataJUME==0){
        $documentoJUME = "documento de la Junta municipal de educación JUME(Acta, decreto o carta),";
        $contador+=1;
      }


      $dataEnlaceTIC = DocumentosMunicipios::where('codigo_municipio', '=', Auth::user()->name)
      ->where('comite_junta','=','Enlace TIC')
      ->count();

      if($dataEnlaceTIC==0){
        $documentoEnlaceTIC = "documento del enlace TIC(Acta, decreto o carta),";
        $contador+=1;
      }



    }


    if(Auth::user()->isRole('enlace_tic')){

      $datosenlacetic = EnlacesTIC::select('id')->where('codigo_municipio', '=', Auth::user()->name)->get();
      if(count($datosenlacetic)==0){
        $informacionPersonal = "Su información personal,";
        $contador+=1;
      }


      $datosenlacetic = Comites::where('codigo_municipio', '=', Auth::user()->name)->where('comite', '=', 'TIC')->count();
      if($datosenlacetic==0){
        $Comitestic = "comités TIC,";
        $contador+=1;
      }

      $dataEnlaceTIC = DocumentosMunicipios::where('codigo_municipio', '=', Auth::user()->name)
      ->where('comite_junta','=','Enlace TIC')
      ->count();

      if($dataEnlaceTIC==0){
        $documentoEnlaceTIC = "documento del enlace TIC(Acta, decreto o carta)";
        $contador+=1;
      }

      $dataTIC = DocumentosMunicipios::where('codigo_municipio', '=', Auth::user()->name)
      ->where('comite_junta','=','Comité TIC')
      ->count();

      if($dataTIC==0){
        $documentoTIC = "documento del comité TIC(Acta, decreto o carta)";
        $contador+=1;
      }

    }




    return view("DirectorioMunicipal.VerInformarcion")
    ->with("enlacetic",$enlacetic)
    ->with("contador",$contador)
    ->with("rectores",$rectores)
    ->with("DirectorioMunicipal",$DirectorioMunicipal)
    ->with("jefeplaneacion",$jefeplaneacion)
    ->with("Comitestic",$Comitestic)
    ->with("Comitescupos",$Comitescupos)
    ->with("informacionPersonal",$informacionPersonal)
    ->with("JUME",$JUME)
    ->with("documentoTIC",$documentoTIC)
    ->with("documentocupos",$documentocupos)
    ->with("documentoEnlaceTIC",$documentoEnlaceTIC)
    ->with("documentoJUME",$documentoJUME);


  }

  //Función encargada de mostrar al usuario la vista actualizar información del directorio municipal.
  public function actualizar_directorio(){


    if(Auth::user()->isRole('secretarios')){

      $datos = DocumentosMunicipios::select('id','numero_documento','fecha_documento','nombre_municipio','comite_junta','documento')

      ->where('codigo_municipio', '=', Auth::user()->name)->get();

    }elseif(Auth::user()->isRole('enlace_tic')){

      $datos = DocumentosMunicipios::select('id','numero_documento','fecha_documento','nombre_municipio','comite_junta','documento')
      ->where('comite_junta','like','%TIC%')
      ->where('codigo_municipio', '=', Auth::user()->name)
      ->get();

    }else{
      $datos = DocumentosMunicipios::select('id','numero_documento','fecha_documento','nombre_municipio','comite_junta','documento')->get();
    }



    return view("DirectorioMunicipal.ActualizarDirectorio")->with("datos",$datos);

  }


  //Función encargada de mostrar al usuario el formulario solicitado para el ingreso de información.
  public function form_ingresar_alcaldes(){

    if(Auth::user()->isRole('secretarios') || Auth::user()->isRole('enlace_tic') ){
      $municipios = Municipio::select('NombreMunicipio')->where('CodigoDaneMunicipio', '=',Auth::user()->name)->get();
      foreach ($municipios as $municipio) {
        $municipios = $municipio->NombreMunicipio;
      }
    }else{
      $municipios = Municipio::select('NombreMunicipio','CodigoDaneMunicipio')->get();
    }

    return view("formularios.form_ingresar_alcaldes")->with("municipios", $municipios);

  }


  //Función encargada de realizar el registro de la información suministrada por el usuario.
  public function ingresar_alcalde(Request $request){

    if(Auth::user()->isRole('secretarios') || Auth::user()->isRole('enlace_tic') ){
      $dane_municipio=Auth::user()->name;
    }else{
      $dane_municipio=$request->input("selMunicipio");
    }
    $existencia = DirectorioMunicipal::select('codigo_municipio')->where('codigo_municipio', '=', $dane_municipio)->get();
    if (count($existencia)==0) {
      $data = new DirectorioMunicipal;
      if(Auth::user()->isRole('secretarios')){
        $data->codigo_municipio=Auth::user()->name;
      }else{
        $data->codigo_municipio=$request->input("selMunicipio");
      }
      $data->nombre_alcalde=$request->input("nombre_alcalde");
      $data->apellido_alcalde=$request->input("apellido_alcalde");
      $celulares=$request["celularesA"];
      $array = implode(', ', $celulares);
      $data->celular_alcalde=$array;
      $data->telefono_alcalde=$request->input("telefono_alcalde");
      $data->correo_alcalde=$request->input("correo_alcalde");
      $data->correo_alcaldia=$request->input("correo_alcaldia");
      $data->direccion_alcaldia=$request->input("direccion_alcaldia");
      $data->nombre_secretario=$request->input("nombre_secretario");
      $data->apellido_secretario=$request->input("apellido_secretario");
      $celulares=$request["celularesS"];
      $array = implode(', ', $celulares);
      $data->celular_secretario=$array;
      $data->telefono_secretario=$request->input("telefono_secretario");
      $data->correo_secretario=$request->input("correo_secretario");
      $data->correo_secretariaeducacion=$request->input("correo_secretariaeducacion");
      $data->cargo_secretario=$request->input("cargo_secretario");
      $data->direccion_secretario=$request->input("direccion_secretario");
      $data->nombre_adminsimat=$request->input("nombre_adminsimat");
      $data->apellido_adminsimat=$request->input("apellido_adminsimat");
      $data->celular_adminsimat=$request->input("celular_adminsimat");
      $data->telefono_adminsimat=$request->input("telefono_adminsimat");
      $data->correo_adminsimat=$request->input("correo_adminsimat");
      $data->cargo_adminsimat=$request->input("cargo_adminsimat");
      $data->direccion_adminsimat=$request->input("direccion_adminsimat");
    }else{
      $municipios = Municipio::select('NombreMunicipio')->where('CodigoDaneMunicipio', '=', $dane_municipio)->get();
      return view("mensajes.msj_existencia_directorio")->with("municipios",$municipios);
    }

    if($data->save())
    {
      return view("mensajes.msj_infodirectorio_ingresada");
    }
    else
    {
      return view("mensajes.mensaje_error")->with("msj","Hubo un error al guardar, Por favor inténtalo nuevamente.");
    }
  }


  public function form_contactos_rector(){

    if(Auth::user()->isRole('rectores')){
      $informacion=temp_historico_establecimiento::select('codigo_establecimiento','nombre_establecimiento')->where('codigo_establecimiento', '=', Auth::user()->name)->get();
      return view("formularios.form_contactos_rector")->with("informacion", $informacion);
    }

  }

  //Función encargada de realizar el registro de la información suministrada por el usuario.
  public function ingresar_contactos_rector(Request $request){

    $existencia = Rectores::select('codigo_establecimiento')->where('codigo_establecimiento', '=', $request->input("establecimientos"))->get();
    $informaciones=temp_historico_establecimiento::select('codigo_dane_municipio')->where('codigo_establecimiento', '=', $request->input("establecimientos"))->get();

    if (count($existencia)==0) {
      $data = new Rectores;
      foreach ($informaciones as $informacion ) {
        $data->codigo_municipio=$informacion->codigo_dane_municipio;
      }
      $data->codigo_establecimiento=$request->input("establecimientos");
      $data->nombre=$request->input("nombres");
      $data->apellido=$request->input("apellidos");
      $data->celular=$request->input("celular");
      $data->telefono=$request->input("telefono");
      $data->correo=$request->input("correo_personal");
      $data->correo_establecimiento=$request->input("correo_establecimiento");
    }else{
      $municipios = temp_historico_establecimiento::select('codigo_establecimiento','nombre_establecimiento')->where('codigo_establecimiento', '=', $request->input("establecimientos"))->get();
      return view("mensajes.msj_existencia_rector")->with("municipios",$municipios);
    }

    if($data->save())
    {
      return view("mensajes.msj_info_directorioingresada");
    }
    else
    {
      return view("mensajes.mensaje_error")->with("msj","Hubo un error al guardar, Por favor inténtalo nuevamente.");
    }
  }



  //Función encargada de mostrar al usuario el formulario solicitado para el ingreso de información.
  public function form_ingresar_rectores(){

    if(Auth::user()->isRole('rectores')){

      $informacion=temp_historico_establecimiento::select('codigo_establecimiento')->where('codigo_establecimiento', '=', Auth::user()->name)->get();
      return view("formularios.form_ingresar_rector")->with("informacion", $informacion);

    }elseif(Auth::user()->isRole('secretarios') || Auth::user()->isRole('enlace_tic') ){

      $municipios = Municipio::select('NombreMunicipio')->where('CodigoDaneMunicipio', '=',Auth::user()->name)->get();
      foreach ($municipios as $municipio) {
        $municipios = $municipio->NombreMunicipio;
      }
      $subregiones = Subregion::orderBy('id', 'asc')->get();
      return view("formularios.form_ingresar_rectores")->with("subregiones", $subregiones)->with("municipios", $municipios);

    }else{

      $subregiones = Subregion::orderBy('id', 'asc')->get();
      return view("formularios.form_ingresar_rectores")->with("subregiones", $subregiones);
    }


  }



  //Función encargada de realizar el registro de la información suministrada por el usuario.
  public function ingresar_rectores(Request $request){

    $existencia = Rectores::select('codigo_establecimiento')->where('codigo_establecimiento', '=', $request->input("establecimientos"))->get();
    $informaciones=temp_historico_establecimiento::select('codigo_dane_municipio')->where('codigo_establecimiento', '=', $request->input("establecimientos"))->get();


    if (count($existencia)==0) {
      $data = new Rectores;
      foreach ($informaciones as $informacion ) {
        $data->codigo_municipio=$informacion->codigo_dane_municipio;
      }
      $data->codigo_establecimiento=$request->input("establecimientos");
      $data->nombre=$request->input("nombres");
      $data->apellido=$request->input("apellidos");
      $data->celular=$request->input("celular");
      $data->telefono=$request->input("telefono");
      $data->correo=$request->input("correo_personal");
      $data->correo_establecimiento=$request->input("correo_establecimiento");
    }else{
      $municipios = temp_historico_establecimiento::select('codigo_establecimiento','nombre_establecimiento')->where('codigo_establecimiento', '=', $request->input("establecimientos"))->get();
      return view("mensajes.msj_existencia_rector")->with("municipios",$municipios);
    }

    if($data->save())
    {
      return view('mensajes.msj_infodirectorio_ingresada');
    }
    else
    {
      return view("mensajes.mensaje_error")->with("msj","Hubo un error al guardar, Por favor inténtalo nuevamente.");
    }
  }

  //Función encargada de mostrar al usuario el formulario solicitado para el ingreso de información.
  public function form_ingresar_enlacetic(){

    if(Auth::user()->isRole('secretarios') || Auth::user()->isRole('enlace_tic') ){

      $municipios = Municipio::select('NombreMunicipio')->where('CodigoDaneMunicipio', '=',Auth::user()->name)->get();
      foreach ($municipios as $municipio) {
        $municipios = $municipio->NombreMunicipio;
      }

    }else{

    $municipios = Municipio::select('NombreMunicipio','CodigoDaneMunicipio')->get();

    }

    return view("formularios.form_ingresar_enlacetic")->with("municipios", $municipios);
  }


  //Función encargada de realizar el registro de la información suministrada por el usuario.
  public function ingresar_enlacetic(Request $request){

    if(Auth::user()->isRole('secretarios') || Auth::user()->isRole('enlace_tic') ){
      $dane_municipio=Auth::user()->name;
    }else{
      $dane_municipio=$request->input("selMunicipio");
    }
    $existencia = EnlacesTIC::select('codigo_municipio')->where('codigo_municipio', '=', $dane_municipio)->get();

    if (count($existencia)==0) {
      $data = new EnlacesTIC;
      if(Auth::user()->isRole('secretarios') || Auth::user()->isRole('enlace_tic') ){
        $data->codigo_municipio=Auth::user()->name;
      }else{
        $data->codigo_municipio=$request->input("selMunicipio");
      }
      $data->nombre_enlacetic=$request->input("nombres");
      $data->apellido_enlacetic=$request->input("apellidos");
      $data->celular_enlacetic=$request->input("celular");
      $data->telefono_enlacetic=$request->input("telefono");
      $data->correo_enlacetic=$request->input("correo_personal");
      $data->cargo_enlacetic=$request->input("cargo");
      $data->direccion_enlacetic=$request->input("direccion");

    }else{
      $municipios = Municipio::select('NombreMunicipio')->where('CodigoDaneMunicipio', '=', $dane_municipio)->get();
      return view("mensajes.msj_existencia_enlacetic")->with("municipios",$municipios);
    }

    if($data->save())
    {
      return view("mensajes.msj_infodirectorio_ingresada");
    }
    else
    {
      return view("mensajes.mensaje_error")->with("msj","Hubo un error al guardar, Por favor inténtalo nuevamente.");
    }
  }


  //Función encargada de mostrar al usuario el formulario solicitado para el ingreso de información.
  public function form_ingresar_secretarioplaneacion(){

    if(Auth::user()->isRole('secretarios') || Auth::user()->isRole('enlace_tic') ){

      $municipios = Municipio::select('NombreMunicipio')->where('CodigoDaneMunicipio', '=',Auth::user()->name)->get();
      foreach ($municipios as $municipio) {
        $municipios = $municipio->NombreMunicipio;
      }

    }else{

    $municipios = Municipio::select('NombreMunicipio','CodigoDaneMunicipio')->get();

    }

    return view("formularios.form_ingresar_secretarioplaneacion")->with("municipios", $municipios);
  }


  //Función encargada de realizar el registro de la información suministrada por el usuario.
  public function ingresar_secretarioplaneacion(Request $request){

    if(Auth::user()->isRole('secretarios')){
      $dane_municipio=Auth::user()->name;
    }else{
      $dane_municipio=$request->input("selMunicipio");
    }

    $existencia = SecretarioPlaneacion::select('codigo_municipio')->where('codigo_municipio', '=', $dane_municipio)->get();
    if (count($existencia)==0) {
      $data = new SecretarioPlaneacion;
      if(Auth::user()->isRole('secretarios')){
        $data->codigo_municipio=Auth::user()->name;
      }else{
        $data->codigo_municipio=$request->input("selMunicipio");
      }
      $data->nombre=$request->input("nombres");
      $data->apellido=$request->input("apellidos");
      $data->celular=$request->input("celular");
      $data->telefono=$request->input("telefono");
      $data->correo=$request->input("correo_personal");
      $data->cargo=$request->input("cargo");
      $data->direccion=$request->input("direccion");


    }else{
      $municipios = Municipio::select('NombreMunicipio')->where('CodigoDaneMunicipio', '=', $dane_municipio)->get();
      return view("mensajes.msj_existencia_jefeplaneacion")->with("municipios",$municipios);
    }

    if($data->save())
    {
      return view("mensajes.msj_infodirectorio_ingresada");
    }
    else
    {
      return view("mensajes.mensaje_error")->with("msj","Hubo un error al guardar, Por favor inténtalo nuevamente.");
    }
  }


  //Función encargada de mostrar al usuario el formulario solicitado para el ingreso de información.
  public function form_ingresar_directornucleo(){

    $municipios = Municipio::select('NombreMunicipio','CodigoDaneMunicipio')->get();

    return view("formularios.form_ingresar_directornucleo")->with("municipios", $municipios);

  }


  //Función encargada de realizar el registro de la información suministrada por el usuario.
  public function ingresar_directornucleo(Request $request){

    $municipios = $request["municipio"];

    $array_num= count($municipios);
    $guardados= 0;
    $noguardados= 0;
    $acumuladores=[];
    $acumuladores2=[];

    for ($i = 0; $i < $array_num; ++$i){
      $existencia = DirectorNucleo::select('codigo_municipio')->where('codigo_municipio', '=', $municipios[$i])->get();
      if (count($existencia)==0) {
        $data = new DirectorNucleo;
        $nombres = Municipio::select('NombreMunicipio')->where('CodigoDaneMunicipio', '=', $municipios[$i])->get();
        $data->codigo_municipio=$municipios[$i];
        $data->nombre=$request->input("nombres");
        $data->apellido=$request->input("apellidos");
        $data->celular=$request->input("celular");
        $data->telefono=$request->input("telefono");
        $data->correo=$request->input("correo_personal");
        $data->cargo="DIRECTOR(A) DE NÚCLEO";
        $data->direccion=$request->input("direccion");
        foreach ($nombres as $nombre) {
          $acumuladores[]=$nombre->NombreMunicipio;
        }
        $guardados+=1;
        $data->save();
      }else{
        $nombres = Municipio::select('NombreMunicipio')->where('CodigoDaneMunicipio', '=', $municipios[$i])->get();
        foreach ($nombres as $nombre) {
          $acumuladores2[]=$nombre->NombreMunicipio;
        }
        $noguardados+=1;
        continue;
      }

    }

    $nombres  = $acumuladores;
    $array = implode(', ', $nombres);

    $nombres  = $acumuladores2;
    $array2 = implode(', ', $nombres);

    if($guardados>=1 || $noguardados>=1 ){
      return view("mensajes.msj_info_jefenucleo")
      ->with("array",$array)
      ->with("array2",$array2)
      ->with("noguardados",$noguardados)
      ->with("guardados",$guardados);
    }else
    {
      return view("mensajes.mensaje_error")->with("msj","Hubo un error al guardar, Por favor inténtalo nuevamente.");
    }
  }


  //Función encargada de mostrar al usuario el formulario solicitado para el ingreso de información.
  public function form_ingresar_comites(){

    if(Auth::user()->isRole('secretarios') || Auth::user()->isRole('enlace_tic') ){

      $municipios = Municipio::select('NombreMunicipio')->where('CodigoDaneMunicipio', '=',Auth::user()->name)->get();
      foreach ($municipios as $municipio) {
        $municipios = $municipio->NombreMunicipio;
      }

    }else{

    $municipios = Municipio::select('NombreMunicipio','CodigoDaneMunicipio')->get();

    }

    $datos= ComitesJuntas::select('codigo','comite_junta')->get();
    return view("formularios.form_ingresar_comites")
    ->with("datos", $datos)
    ->with("municipios", $municipios);

  }


  //Función encargada de realizar el registro de la información suministrada por el usuario.
  public function ingresar_comites(Request $request){

    $data = new Comites;
    $nombres = ComitesJuntas::select('comite_junta')->where('codigo','=',$request->input("selcomite"))->get();
    if(Auth::user()->isRole('secretarios') || Auth::user()->isRole('enlace_tic') ){
      $data->codigo_municipio=Auth::user()->name;
    }else{
      $data->codigo_municipio=$request->input("selMunicipio");
    }
    $data->nombre=$request->input("nombres");
    $data->apellido=$request->input("apellidos");
    $data->celular=$request->input("celular");
    $data->telefono=$request->input("telefono");
    $data->correo=$request->input("correo_personal");
    $data->sector=$request->input("sector");
    $data->cargo=$request->input("cargo");
    $data->direccion=$request->input("direccion");
    if(Auth::user()->isRole('enlace_tic') ){
      $data->comite=$request->input("selcomite");
    }else{
      foreach ($nombres as $nombre) {
        $data->comite=$nombre->comite_junta;
      }
    }



    if($data->save()){
      return view("mensajes.msj_infodirectorio_ingresada");
    }
    else
    {
      return view("mensajes.mensaje_error")->with("msj","Hubo un error al guardar, Por favor inténtalo nuevamente.");
    }
  }


  //A continuación se encuentran las consultas utlizadas para alimentar cada una de las datatables del módulo.
  public function listado_alcaldes(){

    $datos = DirectorioMunicipal::join('municipios', 'directorio_municipal.codigo_municipio', '=', 'municipios.CodigoDaneMunicipio')
    ->join('subregions', 'municipios.CodigoSubregion', '=', 'subregions.CodigoSubregion')
    ->select('municipios.NombreMunicipio','subregions.NombreSubregion','directorio_municipal.nombre_alcalde','directorio_municipal.id','directorio_municipal.apellido_alcalde','directorio_municipal.celular_alcalde','directorio_municipal.telefono_alcalde',
    'directorio_municipal.correo_alcalde','directorio_municipal.correo_alcaldia','directorio_municipal.direccion_alcaldia')
    ->get();

    return Datatables::of($datos)->make(true);
  }


  public function listado_secretarios(){

    $datos = DirectorioMunicipal::join('municipios', 'directorio_municipal.codigo_municipio', '=', 'municipios.CodigoDaneMunicipio')
    ->join('subregions', 'municipios.CodigoSubregion', '=', 'subregions.CodigoSubregion')
    ->select('municipios.NombreMunicipio','subregions.NombreSubregion','directorio_municipal.nombre_secretario','directorio_municipal.id','directorio_municipal.apellido_secretario','directorio_municipal.celular_secretario','directorio_municipal.telefono_secretario',
    'directorio_municipal.correo_secretario','directorio_municipal.correo_secretariaeducacion','directorio_municipal.cargo_secretario','directorio_municipal.direccion_secretario')
    ->get();

    return Datatables::of($datos)->make(true);
  }


  public function listado_rectores(){

    $datos = Rectores::join('temp_historico_establecimientos', 'rectores.codigo_establecimiento', '=', 'temp_historico_establecimientos.codigo_establecimiento')
    ->join('municipios', 'temp_historico_establecimientos.codigo_dane_municipio', '=', 'municipios.CodigoDaneMunicipio')
    ->join('subregions', 'municipios.CodigoSubregion', '=', 'subregions.CodigoSubregion')
    ->select('temp_historico_establecimientos.nombre_establecimiento','subregions.NombreSubregion','temp_historico_establecimientos.municipio','rectores.nombre','rectores.id','rectores.apellido','rectores.celular','rectores.telefono',
    'rectores.correo','rectores.correo_establecimiento')
    ->get();

    return Datatables::of($datos)->make(true);
  }


  public function listado_adminsimat(){

    $datos = DirectorioMunicipal::join('municipios', 'directorio_municipal.codigo_municipio', '=', 'municipios.CodigoDaneMunicipio')
    ->join('subregions', 'municipios.CodigoSubregion', '=', 'subregions.CodigoSubregion')
    ->select('municipios.NombreMunicipio','subregions.NombreSubregion','directorio_municipal.nombre_adminsimat','directorio_municipal.id','directorio_municipal.apellido_adminsimat','directorio_municipal.celular_adminsimat','directorio_municipal.telefono_adminsimat',
    'directorio_municipal.correo_adminsimat','directorio_municipal.cargo_adminsimat','directorio_municipal.direccion_adminsimat')
    ->get();

    return Datatables::of($datos)->make(true);
  }


  public function listado_enlacestic(){

    $datos = EnlacesTIC::join('municipios', 'enlaces_tic.codigo_municipio', '=', 'municipios.CodigoDaneMunicipio')
    ->join('subregions', 'municipios.CodigoSubregion', '=', 'subregions.CodigoSubregion')
    ->select('municipios.NombreMunicipio','subregions.NombreSubregion','enlaces_tic.nombre_enlacetic','enlaces_tic.id','enlaces_tic.apellido_enlacetic','enlaces_tic.celular_enlacetic','enlaces_tic.telefono_enlacetic',
    'enlaces_tic.correo_enlacetic','enlaces_tic.cargo_enlacetic','enlaces_tic.direccion_enlacetic')
    ->get();

    return Datatables::of($datos)->make(true);
  }


  public function listado_directornucleo(){

    $datos = DirectorNucleo::join('municipios', 'director_nucleo.codigo_municipio', '=', 'municipios.CodigoDaneMunicipio')
    ->join('subregions', 'municipios.CodigoSubregion', '=', 'subregions.CodigoSubregion')
    ->select('municipios.NombreMunicipio','director_nucleo.nombre','subregions.NombreSubregion','director_nucleo.id','director_nucleo.apellido','director_nucleo.celular',
    'director_nucleo.correo','director_nucleo.telefono','director_nucleo.cargo','director_nucleo.direccion')
    ->get();

    return Datatables::of($datos)->make(true);
  }


  public function listado_SecretarioPlaneacion(){

    $datos = SecretarioPlaneacion::join('municipios', 'secreatrios_planeacion.codigo_municipio', '=', 'municipios.CodigoDaneMunicipio')
    ->join('subregions', 'municipios.CodigoSubregion', '=', 'subregions.CodigoSubregion')
    ->select('municipios.NombreMunicipio','subregions.NombreSubregion','secreatrios_planeacion.nombre','secreatrios_planeacion.id','secreatrios_planeacion.apellido','secreatrios_planeacion.celular','secreatrios_planeacion.telefono',
    'secreatrios_planeacion.correo','secreatrios_planeacion.cargo','secreatrios_planeacion.direccion')
    ->get();

    return Datatables::of($datos)->make(true);
  }


  public function listado_comitestic(){

    $datos = Comites::join('municipios', 'comites_juntas_municipios.codigo_municipio', '=', 'municipios.CodigoDaneMunicipio')
    ->join('subregions', 'municipios.CodigoSubregion', '=', 'subregions.CodigoSubregion')
    ->select('municipios.NombreMunicipio','comites_juntas_municipios.nombre','comites_juntas_municipios.id','comites_juntas_municipios.apellido','comites_juntas_municipios.celular','comites_juntas_municipios.telefono','comites_juntas_municipios.sector',
    'comites_juntas_municipios.correo','subregions.NombreSubregion','comites_juntas_municipios.comite','comites_juntas_municipios.direccion','comites_juntas_municipios.cargo')
    ->where('comites_juntas_municipios.comite', 'TIC')
    ->get();

    return Datatables::of($datos)->make(true);
  }


  public function listado_jume(){

    $datos = Comites::join('municipios', 'comites_juntas_municipios.codigo_municipio', '=', 'municipios.CodigoDaneMunicipio')
    ->join('subregions', 'municipios.CodigoSubregion', '=', 'subregions.CodigoSubregion')
    ->select('municipios.NombreMunicipio','comites_juntas_municipios.nombre','comites_juntas_municipios.id','comites_juntas_municipios.apellido','comites_juntas_municipios.celular','comites_juntas_municipios.telefono',
    'comites_juntas_municipios.correo','subregions.NombreSubregion','comites_juntas_municipios.comite','comites_juntas_municipios.sector','comites_juntas_municipios.direccion','comites_juntas_municipios.cargo')
    ->where('comites_juntas_municipios.comite', 'JUME')
    ->get();

    return Datatables::of($datos)->make(true);
  }

  public function listado_comitecupos(){

    $datos = Comites::join('municipios', 'comites_juntas_municipios.codigo_municipio', '=', 'municipios.CodigoDaneMunicipio')
    ->join('subregions', 'municipios.CodigoSubregion', '=', 'subregions.CodigoSubregion')
    ->select('municipios.NombreMunicipio','comites_juntas_municipios.nombre','comites_juntas_municipios.id','comites_juntas_municipios.apellido','comites_juntas_municipios.celular','comites_juntas_municipios.telefono',
    'comites_juntas_municipios.correo','subregions.NombreSubregion','comites_juntas_municipios.sector','comites_juntas_municipios.direccion','comites_juntas_municipios.comite','comites_juntas_municipios.cargo')
    ->where('comites_juntas_municipios.comite', 'Cupos')
    ->get();

    return Datatables::of($datos)->make(true);
  }


  public function listado_actualizar_alcaldes(){

    if(Auth::user()->isRole('secretarios'))
    {
      $datos = DirectorioMunicipal::join('municipios', 'directorio_municipal.codigo_municipio', '=', 'municipios.CodigoDaneMunicipio')
      ->select('municipios.NombreMunicipio','directorio_municipal.nombre_alcalde','directorio_municipal.id','directorio_municipal.apellido_alcalde','directorio_municipal.celular_alcalde','directorio_municipal.telefono_alcalde',
      'directorio_municipal.correo_alcalde','directorio_municipal.correo_alcaldia','directorio_municipal.direccion_alcaldia')
      ->where('municipios.CodigoDaneMunicipio', Auth::user()->name)
      ->get();

    }else{
      $datos = DirectorioMunicipal::join('municipios', 'directorio_municipal.codigo_municipio', '=', 'municipios.CodigoDaneMunicipio')
      ->select('municipios.NombreMunicipio','directorio_municipal.nombre_alcalde','directorio_municipal.id','directorio_municipal.apellido_alcalde','directorio_municipal.celular_alcalde','directorio_municipal.telefono_alcalde',
      'directorio_municipal.correo_alcalde','directorio_municipal.correo_alcaldia','directorio_municipal.direccion_alcaldia')
      ->get();
    }

    return Datatables::of($datos)->make(true);
  }



  public function listado_actualizar_secretarios(){

    if(Auth::user()->isRole('secretarios'))
    {
      $datos = DirectorioMunicipal::join('municipios', 'directorio_municipal.codigo_municipio', '=', 'municipios.CodigoDaneMunicipio')
      ->select('municipios.NombreMunicipio','directorio_municipal.nombre_secretario','directorio_municipal.id','directorio_municipal.apellido_secretario','directorio_municipal.celular_secretario','directorio_municipal.telefono_secretario',
      'directorio_municipal.correo_secretario','directorio_municipal.correo_secretariaeducacion','directorio_municipal.cargo_secretario','directorio_municipal.direccion_secretario')
      ->where('municipios.CodigoDaneMunicipio', Auth::user()->name)
      ->get();
    }else{
      $datos = DirectorioMunicipal::join('municipios', 'directorio_municipal.codigo_municipio', '=', 'municipios.CodigoDaneMunicipio')
      ->select('municipios.NombreMunicipio','directorio_municipal.nombre_secretario','directorio_municipal.id','directorio_municipal.apellido_secretario','directorio_municipal.celular_secretario','directorio_municipal.telefono_secretario',
      'directorio_municipal.correo_secretario','directorio_municipal.correo_secretariaeducacion','directorio_municipal.cargo_secretario','directorio_municipal.direccion_secretario')
      ->get();
    }

    return Datatables::of($datos)->make(true);
  }


  public function listado_actualizar_rectores(){

    if(Auth::user()->isRole('secretarios')){

      $datos = Rectores::join('temp_historico_establecimientos', 'rectores.codigo_establecimiento', '=', 'temp_historico_establecimientos.codigo_establecimiento')
      ->select('temp_historico_establecimientos.nombre_establecimiento','temp_historico_establecimientos.municipio','rectores.nombre','rectores.id','rectores.apellido','rectores.celular','rectores.telefono',
      'rectores.correo', 'rectores.correo_establecimiento')
      ->where('temp_historico_establecimientos.codigo_dane_municipio', Auth::user()->name)
      ->get();

    }elseif (Auth::user()->isRole('rectores')){

      $datos = Rectores::join('temp_historico_establecimientos', 'rectores.codigo_establecimiento', '=', 'temp_historico_establecimientos.codigo_establecimiento')
      ->select('temp_historico_establecimientos.nombre_establecimiento','temp_historico_establecimientos.municipio','rectores.nombre','rectores.id','rectores.apellido','rectores.celular','rectores.telefono',
      'rectores.correo', 'rectores.correo_establecimiento')
      ->where('temp_historico_establecimientos.codigo_establecimiento', Auth::user()->name)
      ->get();

    }else{

      $datos = Rectores::join('temp_historico_establecimientos', 'rectores.codigo_establecimiento', '=', 'temp_historico_establecimientos.codigo_establecimiento')
      ->select('temp_historico_establecimientos.nombre_establecimiento','temp_historico_establecimientos.municipio','rectores.nombre','rectores.id','rectores.apellido','rectores.celular','rectores.telefono',
      'rectores.correo', 'rectores.correo_establecimiento')
      ->get();
    }

    return Datatables::of($datos)->make(true);
  }


  public function listado_actualizar_adminsimat(){

    if(Auth::user()->isRole('secretarios')){

      $datos = DirectorioMunicipal::join('municipios', 'directorio_municipal.codigo_municipio', '=', 'municipios.CodigoDaneMunicipio')
      ->select('municipios.NombreMunicipio','directorio_municipal.nombre_adminsimat','directorio_municipal.id','directorio_municipal.apellido_adminsimat','directorio_municipal.celular_adminsimat','directorio_municipal.telefono_adminsimat',
      'directorio_municipal.correo_adminsimat','directorio_municipal.cargo_adminsimat','directorio_municipal.direccion_adminsimat')
      ->where('municipios.CodigoDaneMunicipio', Auth::user()->name)
      ->get();

    }else{

      $datos = DirectorioMunicipal::join('municipios', 'directorio_municipal.codigo_municipio', '=', 'municipios.CodigoDaneMunicipio')
      ->select('municipios.NombreMunicipio','directorio_municipal.nombre_adminsimat','directorio_municipal.id','directorio_municipal.apellido_adminsimat','directorio_municipal.celular_adminsimat','directorio_municipal.telefono_adminsimat',
      'directorio_municipal.correo_adminsimat','directorio_municipal.cargo_adminsimat','directorio_municipal.direccion_adminsimat')
      ->get();

    }

    return Datatables::of($datos)->make(true);
  }


  public function listado_actualizar_enlacestic(){

    if(Auth::user()->isRole('secretarios') || Auth::user()->isRole('enlace_tic')){

      $datos = EnlacesTIC::join('municipios', 'enlaces_tic.codigo_municipio', '=', 'municipios.CodigoDaneMunicipio')
      ->select('municipios.NombreMunicipio','enlaces_tic.nombre_enlacetic','enlaces_tic.id','enlaces_tic.apellido_enlacetic','enlaces_tic.celular_enlacetic','enlaces_tic.telefono_enlacetic',
      'enlaces_tic.correo_enlacetic','enlaces_tic.cargo_enlacetic','enlaces_tic.direccion_enlacetic')
      ->where('municipios.CodigoDaneMunicipio', Auth::user()->name)
      ->get();

    }else{

      $datos = EnlacesTIC::join('municipios', 'enlaces_tic.codigo_municipio', '=', 'municipios.CodigoDaneMunicipio')
      ->select('municipios.NombreMunicipio','enlaces_tic.nombre_enlacetic','enlaces_tic.id','enlaces_tic.apellido_enlacetic','enlaces_tic.celular_enlacetic','enlaces_tic.telefono_enlacetic',
      'enlaces_tic.correo_enlacetic','enlaces_tic.cargo_enlacetic','enlaces_tic.direccion_enlacetic')
      ->get();

    }

    return Datatables::of($datos)->make(true);
  }


  public function listado_actualizar_directornucleo(){

    if(Auth::user()->isRole('secretarios'))
    {

      $datos = DirectorNucleo::join('municipios', 'director_nucleo.codigo_municipio', '=', 'municipios.CodigoDaneMunicipio')
      ->select('municipios.NombreMunicipio','director_nucleo.nombre','director_nucleo.id','director_nucleo.apellido','director_nucleo.celular',
      'director_nucleo.correo','director_nucleo.cargo','director_nucleo.telefono','director_nucleo.direccion')
      ->where('municipios.CodigoDaneMunicipio', Auth::user()->name)
      ->get();

    }else{

      $datos = DirectorNucleo::join('municipios', 'director_nucleo.codigo_municipio', '=', 'municipios.CodigoDaneMunicipio')
      ->select('municipios.NombreMunicipio','director_nucleo.nombre','director_nucleo.telefono','director_nucleo.id','director_nucleo.apellido','director_nucleo.celular',
      'director_nucleo.correo','director_nucleo.cargo','director_nucleo.direccion')
      ->get();

    }

    return Datatables::of($datos)->make(true);
  }


  public function listado_actualizar_SecreatrioPlaneacion(){

    if(Auth::user()->isRole('secretarios')){

      $datos = SecretarioPlaneacion::join('municipios', 'secreatrios_planeacion.codigo_municipio', '=', 'municipios.CodigoDaneMunicipio')
      ->select('municipios.NombreMunicipio','secreatrios_planeacion.nombre','secreatrios_planeacion.id','secreatrios_planeacion.apellido','secreatrios_planeacion.celular','secreatrios_planeacion.telefono',
      'secreatrios_planeacion.correo','secreatrios_planeacion.cargo','secreatrios_planeacion.direccion')
      ->where('municipios.CodigoDaneMunicipio', Auth::user()->name)
      ->get();

    }else{
      $datos = SecretarioPlaneacion::join('municipios', 'secreatrios_planeacion.codigo_municipio', '=', 'municipios.CodigoDaneMunicipio')
      ->select('municipios.NombreMunicipio','secreatrios_planeacion.nombre','secreatrios_planeacion.id','secreatrios_planeacion.apellido','secreatrios_planeacion.celular','secreatrios_planeacion.telefono',
      'secreatrios_planeacion.correo','secreatrios_planeacion.cargo','secreatrios_planeacion.direccion')
      ->get();
    }

    return Datatables::of($datos)->make(true);
  }


  public function listado_actualizar_comitestic(){

    if(Auth::user()->isRole('secretarios') || Auth::user()->isRole('enlace_tic')){

      $datos = Comites::join('municipios', 'comites_juntas_municipios.codigo_municipio', '=', 'municipios.CodigoDaneMunicipio')
      ->select('municipios.NombreMunicipio','comites_juntas_municipios.nombre','comites_juntas_municipios.id','comites_juntas_municipios.apellido','comites_juntas_municipios.celular','comites_juntas_municipios.telefono','comites_juntas_municipios.sector',
      'comites_juntas_municipios.correo','comites_juntas_municipios.comite','comites_juntas_municipios.direccion','comites_juntas_municipios.cargo')
      ->where('municipios.CodigoDaneMunicipio', Auth::user()->name)
      ->where('comites_juntas_municipios.comite', 'TIC')
      ->get();

    }else{

      $datos = Comites::join('municipios', 'comites_juntas_municipios.codigo_municipio', '=', 'municipios.CodigoDaneMunicipio')
      ->select('municipios.NombreMunicipio','comites_juntas_municipios.nombre','comites_juntas_municipios.id','comites_juntas_municipios.apellido','comites_juntas_municipios.celular','comites_juntas_municipios.telefono','comites_juntas_municipios.sector',
      'comites_juntas_municipios.correo','comites_juntas_municipios.comite','comites_juntas_municipios.direccion','comites_juntas_municipios.cargo')
      ->where('comites_juntas_municipios.comite', 'TIC')
      ->get();

    }

    return Datatables::of($datos)->make(true);
  }


  public function listado_actualizar_jume(){

    if(Auth::user()->isRole('secretarios')){

      $datos = Comites::join('municipios', 'comites_juntas_municipios.codigo_municipio', '=', 'municipios.CodigoDaneMunicipio')
      ->select('municipios.NombreMunicipio','comites_juntas_municipios.nombre','comites_juntas_municipios.id','comites_juntas_municipios.apellido','comites_juntas_municipios.celular','comites_juntas_municipios.telefono',
      'comites_juntas_municipios.correo','comites_juntas_municipios.comite','comites_juntas_municipios.sector','comites_juntas_municipios.cargo','comites_juntas_municipios.direccion')
      ->where('comites_juntas_municipios.comite', 'JUME')
      ->where('municipios.CodigoDaneMunicipio', Auth::user()->name)
      ->get();

    }else{

      $datos = Comites::join('municipios', 'comites_juntas_municipios.codigo_municipio', '=', 'municipios.CodigoDaneMunicipio')
      ->select('municipios.NombreMunicipio','comites_juntas_municipios.nombre','comites_juntas_municipios.id','comites_juntas_municipios.apellido','comites_juntas_municipios.celular','comites_juntas_municipios.telefono',
      'comites_juntas_municipios.correo','comites_juntas_municipios.comite','comites_juntas_municipios.cargo','comites_juntas_municipios.sector','comites_juntas_municipios.direccion')
      ->where('comites_juntas_municipios.comite', 'JUME')
      ->get();

    }

    return Datatables::of($datos)->make(true);
  }


  public function listado_actualizar_comitecupos(){

    if(Auth::user()->isRole('secretarios')){

      $datos = Comites::join('municipios', 'comites_juntas_municipios.codigo_municipio', '=', 'municipios.CodigoDaneMunicipio')
      ->select('municipios.NombreMunicipio','comites_juntas_municipios.nombre','comites_juntas_municipios.id','comites_juntas_municipios.apellido','comites_juntas_municipios.celular','comites_juntas_municipios.telefono',
      'comites_juntas_municipios.correo','comites_juntas_municipios.comite','comites_juntas_municipios.cargo','comites_juntas_municipios.sector','comites_juntas_municipios.direccion')
      ->where('comites_juntas_municipios.comite', 'Cupos')
      ->where('municipios.CodigoDaneMunicipio', Auth::user()->name)
      ->get();

    }else{

      $datos = Comites::join('municipios', 'comites_juntas_municipios.codigo_municipio', '=', 'municipios.CodigoDaneMunicipio')
      ->select('municipios.NombreMunicipio','comites_juntas_municipios.nombre','comites_juntas_municipios.id','comites_juntas_municipios.apellido','comites_juntas_municipios.celular','comites_juntas_municipios.telefono',
      'comites_juntas_municipios.correo','comites_juntas_municipios.comite','comites_juntas_municipios.cargo','comites_juntas_municipios.sector','comites_juntas_municipios.direccion')
      ->where('comites_juntas_municipios.comite', 'Cupos')
      ->get();

    }

    return Datatables::of($datos)->make(true);
  }


  //Función encargada de mostrar el formulario para actualizar la información.
  public function form_editar_alcaldes($id){

    $informacion=DirectorioMunicipal::findOrFail($id);
    $municipios = Municipio::select('NombreMunicipio')->where('CodigoDaneMunicipio', '=',$informacion->codigo_municipio)->get();
    foreach ($municipios as $municipio) {
      $nombre = $municipio->NombreMunicipio;
    }
    return view("formularios.form_editar_alcaldes")->with("informacion",$informacion)->with("nombre",$nombre);
  }


  //Función encargada de actualizar la información de los alcaldes en el directorio municipal.
  public function editar_alcaldes(Request $request){

    $id=$request->input("informacion_id");
    $data=DirectorioMunicipal::findOrFail($id);

    $data->nombre_alcalde=$request->input("nombres");
    $data->apellido_alcalde=$request->input("apellidos");
    $data->celular_alcalde=$request->input("celular");
    $data->telefono_alcalde=$request->input("telefono");
    $data->correo_alcalde=$request->input("correo_personal");
    $data->correo_alcaldia=$request->input("correo_alcaldia");
    $data->direccion_alcaldia=$request->input("direccion");


    if( $data->save()){
      return view("mensajes.msj_directorio_actualizado");
    }
    else
    {
      return view("mensajes.mensaje_error")->with("msj","Hubo un error al guardar, Por favor inténtalo nuevamente.");
    }
  }

  //función encargada de mostrar mensaje de confirmación de borrado al usuario.
  public function form_borrado_alcalde($id){
    $data=DirectorioMunicipal::findOrFail($id);
    $municipios = Municipio::select('NombreMunicipio')->where('CodigoDaneMunicipio', '=',$data->codigo_municipio)->get();
    foreach ($municipios as $municipio) {
      $nombre= $municipio->NombreMunicipio;
    }
    return view("confirmaciones.form_borrado_alcalde")->with("data",$data)->with("nombre",$nombre);
  }


  //función encargada de borrar la información de los alcaldes.
  public function borrar_alcalde(Request $request){

    $id=$request->input("alcalde_id");
    $data=DirectorioMunicipal::findOrFail($id);

    if($data->delete()){
      return view("mensajes.msj_sede_actualizada_borrada");
    }
    else
    {
      return view("mensajes.mensaje_error")->with("msj","Hubo un error al borrar, Por favor inténtalo nuevamente");
    }
  }


  //Función encargada de mostrar el formulario para actualizar la información.
  public function form_editar_secretarios($id){
    $informacion=DirectorioMunicipal::findOrFail($id);
    $municipios = Municipio::select('NombreMunicipio')->where('CodigoDaneMunicipio', '=',$informacion->codigo_municipio)->get();
    foreach ($municipios as $municipio) {
      $nombre = $municipio->NombreMunicipio;
    }
    return view("formularios.form_editar_secretarios")->with("informacion",$informacion)->with("nombre",$nombre);
  }


  //Función encargada de actualizar la información de los secretarios en el directorio municipal.
  public function editar_secretarios(Request $request){

    $id=$request->input("informacion_id");
    $data=DirectorioMunicipal::findOrFail($id);

    $data->nombre_secretario=$request->input("nombres");
    $data->apellido_secretario=$request->input("apellidos");
    $data->celular_secretario=$request->input("celular");
    $data->telefono_secretario=$request->input("telefono");
    $data->correo_secretario=$request->input("correo_personal");
    $data->correo_secretariaeducacion=$request->input("correo_secretaria");
    $data->cargo_secretario=$request->input("cargo");
    $data->direccion_secretario=$request->input("direccion");


    if( $data->save()){
      return view("mensajes.msj_directorio_actualizado");
    }
    else
    {
      return view("mensajes.mensaje_error")->with("msj","Hubo un error al guardar, Por favor inténtalo nuevamente.");
    }
  }


  //Función encargada de mostrar el formulario para actualizar la información.
  public function form_editar_rectores($id){
    $informacion=Rectores::findOrFail($id);
    $municipios = Municipio::select('NombreMunicipio')->where('CodigoDaneMunicipio', '=',$informacion->codigo_municipio)->get();
    foreach ($municipios as $municipio) {
      $nombre = $municipio->NombreMunicipio;
    }
    $datos =temp_historico_establecimiento::select('nombre_establecimiento')->where('codigo_establecimiento', '=', $informacion->codigo_establecimiento)->get();
    return view("formularios.form_editar_rectores")->with("informacion",$informacion)->with("datos",$datos)->with("nombre",$nombre);
  }


  //Función encargada de actualizar la información de los rectores en el directorio municipal.
  public function editar_rectores(Request $request){

    $id=$request->input("informacion_id");
    $data=Rectores::findOrFail($id);

    $data->nombre=$request->input("nombres");
    $data->apellido=$request->input("apellidos");
    $data->celular=$request->input("celular");
    $data->telefono=$request->input("telefono");
    $data->correo=$request->input("correo_personal");
    $data->correo_establecimiento=$request->input("correo_establecimiento");



    if( $data->save()){
      return view("mensajes.msj_directorio_actualizado");
    }
    else
    {
      return view("mensajes.mensaje_error")->with("msj","Hubo un error al guardar, Por favor inténtalo nuevamente.");
    }
  }


  //función encargada de mostrar mensaje de confirmación de borrado al usuario.
  public function form_borrar_rectores($id){
    $data=Rectores::findOrFail($id);
    $municipios = Municipio::select('NombreMunicipio')->where('CodigoDaneMunicipio', '=',$data->codigo_municipio)->get();
    $Establecimientos = temp_historico_establecimiento::select('nombre_establecimiento')->where('codigo_establecimiento', '=', $data->codigo_establecimiento)->get();
    foreach ($municipios as $municipio) {
      $nombre= $municipio->NombreMunicipio;
    }
    foreach ($Establecimientos as $Establecimiento) {
      $Establecimiento= $Establecimiento->nombre_establecimiento;
    }
    return view("confirmaciones.form_borrado_rectores")->with("data",$data)->with("nombre",$nombre)->with("Establecimiento",$Establecimiento);

  }


  //función encargada de borrar la información de los rectores.
  public function borrar_rectores(Request $request){

    $id=$request->input("id");
    $data=Rectores::findOrFail($id);

    if($data->delete()){
      return view("mensajes.msj_sede_actualizada_borrada");
    }
    else
    {
      return view("mensajes.mensaje_error")->with("msj","Hubo un error al borrar, Por favor inténtalo nuevamente");
    }
  }


  //Función encargada de mostrar el formulario para actualizar la información.
  public function form_editar_adminsimat($id){
    $informacion=DirectorioMunicipal::findOrFail($id);
    $municipios = Municipio::select('NombreMunicipio')->where('CodigoDaneMunicipio', '=',$informacion->codigo_municipio)->get();
    foreach ($municipios as $municipio) {
      $nombre = $municipio->NombreMunicipio;
    }

    return view("formularios.form_editar_adminsimat")->with("informacion",$informacion)->with("nombre",$nombre);
  }


  //Función encargada de actualizar la información de los admin simat en el directorio municipal.
  public function editar_adminsimat(Request $request){

    $id=$request->input("informacion_id");
    $data=DirectorioMunicipal::findOrFail($id);

    $data->nombre_adminsimat=$request->input("nombres");
    $data->apellido_adminsimat=$request->input("apellidos");
    $data->celular_adminsimat=$request->input("celular");
    $data->telefono_adminsimat=$request->input("telefono");
    $data->correo_adminsimat=$request->input("correo_personal");
    $data->cargo_adminsimat=$request->input("cargo");
    $data->direccion_adminsimat=$request->input("direccion");


    if( $data->save()){
      return view("mensajes.msj_directorio_actualizado");
    }
    else
    {
      return view("mensajes.mensaje_error")->with("msj","Hubo un error al guardar, Por favor inténtalo nuevamente.");
    }
  }


  //Función encargada de mostrar el formulario para actualizar la información.
  public function form_editar_enlacestic($id){
    $informacion=EnlacesTIC::findOrFail($id);
    $municipios = Municipio::select('NombreMunicipio')->where('CodigoDaneMunicipio', '=',$informacion->codigo_municipio)->get();
    foreach ($municipios as $municipio) {
      $nombre = $municipio->NombreMunicipio;
    }
    return view("formularios.form_editar_enlacestic")->with("informacion",$informacion)->with("nombre",$nombre);
  }


  //Función encargada de actualizar la información de los enlaces tic en el directorio municipal.
  public function editar_enlacestic(Request $request){

    $id=$request->input("informacion_id");
    $data=EnlacesTIC::findOrFail($id);

    $data->nombre_enlacetic=$request->input("nombres");
    $data->apellido_enlacetic=$request->input("apellidos");
    $data->celular_enlacetic=$request->input("celular");
    $data->telefono_enlacetic=$request->input("telefono");
    $data->correo_enlacetic=$request->input("correo_personal");
    $data->cargo_enlacetic=$request->input("cargo");
    $data->direccion_enlacetic=$request->input("direccion");


    if( $data->save()){
      return view("mensajes.msj_directorio_actualizado");
    }
    else
    {
      return view("mensajes.mensaje_error")->with("msj","Hubo un error al guardar, Por favor inténtalo nuevamente.");
    }
  }

  //función encargada de mostrar mensaje de confirmación de borrado al usuario.
  public function form_borrado_enlacestic($id){
    $data=EnlacesTIC::findOrFail($id);
    $municipios = Municipio::select('NombreMunicipio')->where('CodigoDaneMunicipio', '=',$data->codigo_municipio)->get();
    foreach ($municipios as $municipio) {
      $nombre= $municipio->NombreMunicipio;
    }

    return view("confirmaciones.form_borrado_enlacestic")->with("data",$data)->with("nombre",$nombre);

  }


  //función encargada de borrar la información de los enlacestic.
  public function borrar_enlacestic(Request $request){

    $id=$request->input("enlacestic_id");
    $data=EnlacesTIC::findOrFail($id);

    if($data->delete()){
      return view("mensajes.msj_sede_actualizada_borrada");
    }
    else
    {
      return view("mensajes.mensaje_error")->with("msj","Hubo un error al borrar, Por favor inténtalo nuevamente");
    }
  }


  //Función encargada de mostrar el formulario para actualizar la información.
  public function form_editar_directornucleo($id){
    $informacion=DirectorNucleo::findOrFail($id);
    $municipios = Municipio::select('NombreMunicipio')->where('CodigoDaneMunicipio', '=',$informacion->codigo_municipio)->get();
    foreach ($municipios as $municipio) {
      $nombre = $municipio->NombreMunicipio;
    }
    return view("formularios.form_editar_directornucleo")->with("informacion",$informacion)->with("nombre",$nombre);
  }


  //Función encargada de actualizar la información de los directores de núcleo en el directorio municipal.
  public function editar_directornucleo(Request $request){

    $id=$request->input("informacion_id");
    $data=DirectorNucleo::findOrFail($id);

    $data->nombre=$request->input("nombres");
    $data->apellido=$request->input("apellidos");
    $data->celular=$request->input("celular");
    $data->telefono=$request->input("telefono");
    $data->correo=$request->input("correo_personal");
    $data->cargo="DIRECTOR(A) DE NÚCLEO";
    $data->direccion=$request->input("direccion");


    if( $data->save()){
      return view("mensajes.msj_directorio_actualizado");
    }
    else
    {
      return view("mensajes.mensaje_error")->with("msj","Hubo un error al guardar, Por favor inténtalo nuevamente.");
    }
  }

  //función encargada de mostrar mensaje de confirmación de borrado al usuario.
  public function form_borrado_directornucleo($id){
    $data=DirectorNucleo::findOrFail($id);
    $municipios = Municipio::select('NombreMunicipio')->where('CodigoDaneMunicipio', '=',$data->codigo_municipio)->get();
    foreach ($municipios as $municipio) {
      $nombre= $municipio->NombreMunicipio;
    }

    return view("confirmaciones.form_borrado_directornucleo")->with("data",$data)->with("nombre",$nombre);

  }


  //función encargada de borrar la información de los jefes de planeación.
  public function borrar_directornucleo(Request $request){

    $id=$request->input("id");
    $data=DirectorNucleo::findOrFail($id);

    if($data->delete()){
      return view("mensajes.msj_sede_actualizada_borrada");
    }
    else
    {
      return view("mensajes.mensaje_error")->with("msj","Hubo un error al borrar, Por favor inténtalo nuevamente");
    }
  }


  //Función encargada de mostrar el formulario para actualizar la información.
  public function form_editar_secretarioplaneacion($id){
    $informacion=SecretarioPlaneacion::findOrFail($id);
    $municipios = Municipio::select('NombreMunicipio')->where('CodigoDaneMunicipio', '=',$informacion->codigo_municipio)->get();
    foreach ($municipios as $municipio) {
      $nombre = $municipio->NombreMunicipio;
    }
    return view("formularios.form_editar_secretarioplaneacion")->with("informacion",$informacion)->with("nombre",$nombre);
  }


  //Función encargada de actualizar la información de los secretarios(a) de planeación en el directorio municipal.
  public function editar_secretarioplaneacion(Request $request){

    $id=$request->input("informacion_id");
    $data=SecretarioPlaneacion::findOrFail($id);

    $data->nombre=$request->input("nombres");
    $data->apellido=$request->input("apellidos");
    $data->celular=$request->input("celular");
    $data->telefono=$request->input("telefono");
    $data->correo=$request->input("correo_personal");
    $data->cargo=$request->input("cargo");
    $data->direccion=$request->input("direccion");


    if( $data->save()){
      return view("mensajes.msj_directorio_actualizado");
    }
    else
    {
      return view("mensajes.mensaje_error")->with("msj","Hubo un error al guardar, Por favor inténtalo nuevamente.");
    }
  }

  //función encargada de mostrar mensaje de confirmación de borrado al usuario.
  public function form_borrado_secretarioplaneacion($id){
    $data=SecretarioPlaneacion::findOrFail($id);
    $municipios = Municipio::select('NombreMunicipio')->where('CodigoDaneMunicipio', '=',$data->codigo_municipio)->get();
    foreach ($municipios as $municipio) {
      $nombre = $municipio->NombreMunicipio;
    }
    return view("confirmaciones.form_borrado_secretarioplaneacion")->with("data",$data)->with("nombre",$nombre);

  }


  //función encargada de borrar la información de los secretarios(a) de planeación.
  public function borrado_SecretarioPlaneacion(Request $request){

    $id=$request->input("id");
    $data=SecretarioPlaneacion::findOrFail($id);

    if($data->delete()){
      return view("mensajes.msj_sede_actualizada_borrada");
    }
    else
    {
      return view("mensajes.mensaje_error")->with("msj","Hubo un error al borrar, Por favor inténtalo nuevamente");
    }
  }


  //Función encargada de mostrar el formulario para actualizar la información.
  public function form_editar_comitestic($id){
    $informacion=Comites::findOrFail($id);
    $municipios = Municipio::select('NombreMunicipio')->where('CodigoDaneMunicipio', '=',$informacion->codigo_municipio)->get();
    foreach ($municipios as $municipio) {
      $nombre = $municipio->NombreMunicipio;
    }
    return view("formularios.form_editar_comitestic")->with("informacion",$informacion)->with("nombre",$nombre);
  }


  //Función encargada de actualizar la información de los comités tic en el directorio municipal.
  public function editar_comitestic(Request $request){

    $id=$request->input("informacion_id");
    $data=Comites::findOrFail($id);

    $data->nombre=$request->input("nombres");
    $data->apellido=$request->input("apellidos");
    $data->celular=$request->input("celular");
    $data->telefono=$request->input("telefono");
    $data->correo=$request->input("correo_personal");
    $data->sector=$request->input("sector");
    $data->cargo=$request->input("cargo");
    $data->direccion=$request->input("direccion");


    if( $data->save()){
      return view("mensajes.msj_directorio_actualizado");
    }
    else
    {
      return view("mensajes.mensaje_error")->with("msj","Hubo un error al guardar, Por favor inténtalo nuevamente.");
    }
  }

  //función encargada de mostrar mensaje de confirmación de borrado al usuario.
  public function form_borrado_comitestic($id){
    $data=Comites::findOrFail($id);
    $municipios = Municipio::select('NombreMunicipio')->where('CodigoDaneMunicipio', '=',$data->codigo_municipio)->get();
    foreach ($municipios as $municipio) {
      $nombre = $municipio->NombreMunicipio;
    }
    return view("confirmaciones.form_borrado_comitestic")->with("data",$data)->with("nombre",$nombre);

  }


  //función encargada de borrar la información de los comités TIC.
  public function borrado_comitestic(Request $request){

    $id=$request->input("id");
    $data=Comites::findOrFail($id);

    if($data->delete()){
      return view("mensajes.msj_sede_actualizada_borrada");
    }
    else
    {
      return view("mensajes.mensaje_error")->with("msj","Hubo un error al borrar, Por favor inténtalo nuevamente");
    }
  }


  //Función encargada de mostrar el formulario para actualizar la información.
  public function form_editar_jume($id){
    $informacion=Comites::findOrFail($id);
    $municipios = Municipio::select('NombreMunicipio')->where('CodigoDaneMunicipio', '=',$informacion->codigo_municipio)->get();
    foreach ($municipios as $municipio) {
      $nombre = $municipio->NombreMunicipio;
    }
    return view("formularios.form_editar_jume")->with("informacion",$informacion)->with("nombre",$nombre);
  }


  //Función encargada de actualizar la información de la JUME en el directorio municipal.
  public function editar_jume(Request $request){

    $id=$request->input("informacion_id");
    $data=Comites::findOrFail($id);

    $data->nombre=$request->input("nombres");
    $data->apellido=$request->input("apellidos");
    $data->celular=$request->input("celular");
    $data->telefono=$request->input("telefono");
    $data->correo=$request->input("correo_personal");
    $data->sector=$request->input("sector");
    $data->cargo=$request->input("cargo");
    $data->direccion=$request->input("direccion");


    if( $data->save()){
      return view("mensajes.msj_directorio_actualizado");
    }
    else
    {
      return view("mensajes.mensaje_error")->with("msj","Hubo un error al guardar, Por favor inténtalo nuevamente.");
    }
  }

  //función encargada de mostrar mensaje de confirmación de borrado al usuario.
  public function form_borrado_jume($id){
    $data=Comites::findOrFail($id);
    $municipios = Municipio::select('NombreMunicipio')->where('CodigoDaneMunicipio', '=',$data->codigo_municipio)->get();
    foreach ($municipios as $municipio) {
      $nombre = $municipio->NombreMunicipio;
    }
    return view("confirmaciones.form_borrado_jume")->with("data",$data)->with("nombre",$nombre);

  }


  //función encargada de borrar la información de la JUME.
  public function borrado_jume(Request $request){

    $id=$request->input("id");
    $data=Comites::findOrFail($id);

    if($data->delete()){
      return view("mensajes.msj_sede_actualizada_borrada");
    }
    else
    {
      return view("mensajes.mensaje_error")->with("msj","Hubo un error al borrar, Por favor inténtalo nuevamente");
    }
  }


  //Función encargada de mostrar el formulario para actualizar la información.
  public function form_editar_comitescupos($id){
    $informacion=Comites::findOrFail($id);
    $municipios = Municipio::select('NombreMunicipio')->where('CodigoDaneMunicipio', '=',$informacion->codigo_municipio)->get();
    foreach ($municipios as $municipio) {
      $nombre = $municipio->NombreMunicipio;
    }
    return view("formularios.form_editar_comitescupos")->with("informacion",$informacion)->with("nombre",$nombre);
  }


  //Función encargada de actualizar la información de los comités de cupos en el directorio municipal.
  public function editar_comitescupos(Request $request){

    $id=$request->input("informacion_id");
    $data=Comites::findOrFail($id);

    $data->nombre=$request->input("nombres");
    $data->apellido=$request->input("apellidos");
    $data->celular=$request->input("celular");
    $data->telefono=$request->input("telefono");
    $data->correo=$request->input("correo_personal");
    $data->sector=$request->input("sector");
    $data->cargo=$request->input("cargo");
    $data->direccion=$request->input("direccion");


    if( $data->save()){
      return view("mensajes.msj_directorio_actualizado");
    }
    else
    {
      return view("mensajes.mensaje_error")->with("msj","Hubo un error al guardar, Por favor inténtalo nuevamente.");
    }
  }

  //función encargada de mostrar mensaje de confirmación de borrado al usuario.
  public function form_borrado_comitescupos($id){
    $data=Comites::findOrFail($id);
    $municipios = Municipio::select('NombreMunicipio')->where('CodigoDaneMunicipio', '=',$data->codigo_municipio)->get();
    foreach ($municipios as $municipio) {
      $nombre = $municipio->NombreMunicipio;
    }
    return view("confirmaciones.form_borrado_comitescupos")->with("data",$data)->with("nombre",$nombre);
  }


  //función encargada de borrar la información de los comités de cupos.
  public function borrado_comitescupos(Request $request){

    $id=$request->input("id");
    $data=Comites::findOrFail($id);

    if($data->delete()){
      return view("mensajes.msj_sede_actualizada_borrada");
    }
    else
    {
      return view("mensajes.mensaje_error")->with("msj","Hubo un error al borrar, Por favor inténtalo nuevamente");
    }
  }


}
