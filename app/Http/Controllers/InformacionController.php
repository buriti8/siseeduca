<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\RectorDirector;
use App\HistoricoEstablecimiento;
use App\Municipio;
use Datatables;

//EMPIENZAN LAS FUNCIONES PARA CADA TIPO DE VISTA.......
use Illuminate\Support\Facades\Validator;

class InformacionController extends Controller
{




  public function form_nuevo_rectordirector(){
  $municipios= Municipio::all();
  $historico_establecimientos=HistoricoEstablecimiento::all();
  return view("formularios.form_nuevo_rectordirector")->with("historico_establecimientos",$historico_establecimientos)->with("municipios",$municipios);


  }

  public function crear_rectordirector(Request $request){

    $rectordirector=new RectorDirector;
    $rectordirector->CC=$request->input("cc");
    $rectordirector->Nit=$request->input("nit");
    $rectordirector->Nombre=$request->input("nombre");
    $rectordirector->TelefonoFijo=$request->input("telefonofijo");
    $rectordirector->PaginaWeb=$request->input("paginaweb ");
    $rectordirector->Email=$request->input("email");
    if($rectordirector->save())

     {


        return view("mensajes.msj_rectordirector_creado")->with("msj","Rector Director se ha agregado correctamente.") ;
      }
      else
      {
          return view("mensajes.mensaje_error")->with("msj","...Hubo un error al agregar ;...") ;
      }

  }


  public function form_editar_rectordirector($id){
    $rectordirector=RectorDirector::find($id);
    $cod_mun=$rectordirector->CodigoMunicipio;
    $muni=Municipio::find($cod_mun);
    $municipios=Municipio::all();
    return view("formularios.form_editar_rectordirector")->with("rectordirector",$rectordirector)->with("municipios",$municipios)->with("muni",$muni);
  }


  public function editar_rectordirector(Request $request){

      $idrectordirector=$request->input("id_rectordirector");
      $rectordirector=RectorDirector::find($idrectordirector);
      $rectordirector->CC=$request->input("cc");
      $rectordirector->Nit= $request->input("nit");
      $rectordirector->Nombre= $request->input("nombre");
      $rectordirector->TelefonoFijo= $request->input("telefonofijo");
      $rectordirector->PaginaWeb= $request->input("paginaweb");
      $rectordirector->Email=$request->input("email");


      if( $rectordirector->save()){
      return view("mensajes.msj_usuario_actualizado")->with("msj","Rector Director actualizado correctamente")
                                                       ->with("idusuario",$idrectordirector) ;
      }
      else
      {
      return view("mensajes.mensaje_error")->with("msj","..Hubo un error al agregar ; intentarlo nuevamente..");
      }
  }



      public function form_borrado_rectordirector($id){
        $rectordirector=RectorDirector::find($id);
        return view("confirmaciones.form_borrado_rectordirector")->with("rectordirector",$rectordirector);

      }

      public function borrar_rectordirector(Request $request){

              $idrectordirector=$request->input("id_rectordirector");
              $rectordirector=RectorDirector::find($idrectordirector);

              if($rectordirector->delete()){
                   return view("mensajes.msj_rectordirector_borrado")->with("msj","rector director ha sido borrado correctamente.") ;
              }
              else
              {
                  return view("mensajes.mensaje_error")->with("msj","..Hubo un error al agregar ; intentarlo nuevamente..");
              }

      }

 // Esta  función trae los datos y los lista en la aplicación.
       public function listado_rectordirector(){
        $datos = RectorDirector::select(['id','CC','Nit','Nombre','TelefonoFijo','PaginaWeb','Email']);
             return Datatables::of($datos)->make(true);
       }



       public function listado_informacion(){
        //carga el formulario para agregar un nuevo usuario

        $rectordirectors=RectorDirector::paginate(100);
        $municipios=Municipio::paginate(100);
        $historico_establecimientos=HistoricoEstablecimiento::paginate(100);




        return view("conectividad.editar_informacion")
        ->with("rectordirectors",$rectordirectors)
        ->with("municipios",$municipios)
        ->with("historico_establecimientos",$historico_establecimientos);


    }

    /**
     * Show the application dashboard.
     *
     * @return Response
     */
    public function index()
    {

    }

}
