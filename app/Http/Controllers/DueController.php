<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Meses;
use App\HistoricoEstablecimiento;
use App\HistoricoSede;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Storage;

/**
 * Class HomeController
 * @package App\Http\Controllers
 */
class DueController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function form_borrado_archivos_due($ano_info,$mes_corte){

      return view("confirmaciones.form_borrado_archivos_due")->with("ano_info",$ano_info)->with("mes_corte",$mes_corte);

    }


    public function borrar_archivos_due(Request $request){

            $anio_corte=$request->input("ano_info");
            $mes_corte=$request->input("mes_corte");

            $res_establecimiento=HistoricoEstablecimiento::where('anio_corte',$anio_corte)->where('mes_corte',$mes_corte)->delete();
            $res_sede=HistoricoSede::where('anio_corte',$anio_corte)->where('mes_corte',$mes_corte)->delete();

            if($res_establecimiento && $res_sede){
              DB::statement('refresh materialized view registro_archivos_cargados_due;');


                 return view("mensajes.msj_borrado");
            }
            else
            {
                return view("mensajes.mensaje_error")->with("msj","Hubo un error al agregar, intentarlo nuevamente.");
            }


    }

    public function validar_header_establecimientos($archivo){

      $opened_file = fopen($archivo, 'r');
      $header = fgetcsv($opened_file, 0, ';');
      $header_convert= $this->utf8_converter($header);
      $header_convert_trim = array_map('trim', $header);

      $secretaria  =  $this->getColumnNameByValue($header_convert_trim,'Secretaría');
      $codigo_departamento  =  $this->getColumnNameByValue($header_convert_trim,'Código Departamento');
      $departamento=$this->getColumnNameByValue($header_convert_trim,'Departamento');
      $codigo_municipio=$this->getColumnNameByValue($header_convert_trim,'Código Municipio');
      $municipio=$this->getColumnNameByValue($header_convert_trim,'Municipio');
      $codigo =$this->getColumnNameByValue($header_convert_trim,'Código');
      $nombre=$this->getColumnNameByValue($header_convert_trim,'Nombre');
      $direccion=$this->getColumnNameByValue($header_convert_trim,'Dirección');
      $telefono=$this->getColumnNameByValue($header_convert_trim,'Teléfono');
      $nombre_rector=$this->getColumnNameByValue($header_convert_trim,'Nombre Rector');
      $tipo_establecimiento  =$this->getColumnNameByValue($header_convert_trim,'Tipo Establecimiento');
      $etnias =$this->getColumnNameByValue($header_convert_trim,'Etnias');
      $sector =$this->getColumnNameByValue($header_convert_trim,'Sector');
      $genero =$this->getColumnNameByValue($header_convert_trim,'Genero');
      $zona =$this->getColumnNameByValue($header_convert_trim,'Zona');
      $niveles =$this->getColumnNameByValue($header_convert_trim,'Niveles');
      $jornadas =$this->getColumnNameByValue($header_convert_trim,'Jornadas');
      $caracter=$this->getColumnNameByValue($header_convert_trim,'Caracter');
      $especialidad=$this->getColumnNameByValue($header_convert_trim,'Especialidad');
      $licencia   =$this->getColumnNameByValue($header_convert_trim,'Licencia');
      $grados=$this->getColumnNameByValue($header_convert_trim,'Grados');
      $modelos_educativos=$this->getColumnNameByValue($header_convert_trim,'Modelos Educativos');
      $capacidades_excepcionales=$this->getColumnNameByValue($header_convert_trim,'Capacidades Excepcionales');
      $discapacidades=$this->getColumnNameByValue($header_convert_trim,'Discapacidades');
      $idiomas=$this->getColumnNameByValue($header_convert_trim,'Idiomas');
      $numero_de_sedes=$this->getColumnNameByValue($header_convert_trim,'Número de Sedes');
      $estado=$this->getColumnNameByValue($header_convert_trim,'Estado');
      $prestador_de_servicio=$this->getColumnNameByValue($header_convert_trim,'Prestador de servicio');
      $propiedad_de_la_planta_fisica  =$this->getColumnNameByValue($header_convert_trim,'Propiedad de la planta fisíca');
      $resguardo=$this->getColumnNameByValue($header_convert_trim,'Resguardo');
      $matricula_contratada =$this->getColumnNameByValue($header_convert_trim,'Matricula contratada');
      $calendario =$this->getColumnNameByValue($header_convert_trim,'Calendario');
      $internado =$this->getColumnNameByValue($header_convert_trim,'Internado');
      $estrato_socio_economico=$this->getColumnNameByValue($header_convert_trim,'Estrato socio-economico');
      $correo_electronico =$this->getColumnNameByValue($header_convert_trim,'Correo Electrónico');

      fclose($opened_file);

      $validation_array = [
        'Secretaría'=>$secretaria,
        'Código Departamento'=>$codigo_departamento,
        'Departamento'=>$departamento,
        'Código Municipio'=>$codigo_municipio,
        'Municipio'=>$municipio,
        'Código'=>$codigo,
        'Nombre'=>$nombre,
        'Dirección'=>$direccion,
        'Teléfono'=>$telefono,
        'Nombre Rector'=>$nombre_rector,
        'Tipo Establecimiento'=>$tipo_establecimiento,
        'Etnias'=>$etnias,
        'Sector'=>$sector,
        'Genero'=>$genero,
        'Zona'=>$zona,
        'Niveles'=>$niveles,
        'Jornadas'=>$jornadas,
        'Caracter'=>$caracter,
        'Especialidad'=>$especialidad,
        'Licencia'=>$licencia,
        'Grados'=>$grados,
        'Modelos Educativos'=>$modelos_educativos,
        'Capacidades Excepcionales'=>$capacidades_excepcionales,
        'Discapacidades'=>$discapacidades,
        'Idiomas'=>$idiomas,
        'Número de Sedes'=>$numero_de_sedes,
        'Estado'=>$estado,
        'Prestador de servicio'=>$prestador_de_servicio,
        'Propiedad de la planta fisíca'=>$propiedad_de_la_planta_fisica,
        'Resguardo'=>$resguardo,
        'Matricula contratada'=>$matricula_contratada,
        'Calendario'=>$calendario,
        'Internado'=>$internado,
        'Estrato socio-economico'=>$estrato_socio_economico,
        'Correo Electrónico'=>$correo_electronico,

        ];


      return $validation_array;

    }


    public function validar_header_sedes($archivo){

      $opened_file = fopen($archivo, 'r');
      $header = fgetcsv($opened_file, 0, ';');
      $header_convert= $this->utf8_converter($header);
      $header_convert_trim = array_map('trim', $header_convert);

      $secretaria  =  $this->getColumnNameByValue($header_convert_trim,'Secretaria');
      $codigo_departamento  =  $this->getColumnNameByValue($header_convert_trim,'Código departamento');
      $nombre_departamento=$this->getColumnNameByValue($header_convert_trim,'Nombre departamento');
      $codigo_municipio=$this->getColumnNameByValue($header_convert_trim,'Código municipio');
      $nombre_municipio=$this->getColumnNameByValue($header_convert_trim,'Nombre municipio');
      $codigo_establecimiento =$this->getColumnNameByValue($header_convert_trim,'Código Establecimiento');
      $nombre_establecimiento=$this->getColumnNameByValue($header_convert_trim,'Nombre Establecimiento');
      $codigo_sede =$this->getColumnNameByValue($header_convert_trim,'Código Sede');
      $nombre_sede=$this->getColumnNameByValue($header_convert_trim,'Nombre Sede');
      $zona =$this->getColumnNameByValue($header_convert_trim,'Zona');
      $direccion=$this->getColumnNameByValue($header_convert_trim,'Dirección');
      $telefono=$this->getColumnNameByValue($header_convert_trim,'Teléfono');
      $estado_sede=$this->getColumnNameByValue($header_convert_trim,'Estado Sede');
      $niveles =$this->getColumnNameByValue($header_convert_trim,'Niveles');
      $modelos=$this->getColumnNameByValue($header_convert_trim,'Modelos');
      $grados=$this->getColumnNameByValue($header_convert_trim,'Grados');


      fclose($opened_file);

      $validation_array = [
        'Secretaria'=>$secretaria,
        'Código departamento'=>$codigo_departamento,
        'Nombre departamento'=>$nombre_departamento,
        'Código municipio'=>$codigo_municipio,
        'Nombre municipio'=>$nombre_municipio,
        'Código Establecimiento'=>$codigo_establecimiento,
        'Nombre Establecimiento'=>$nombre_establecimiento,
        'Código Sede'=>$codigo_sede,
        'Nombre Sede'=>$nombre_sede,
        'Zona'=>$zona,
        'Dirección'=>$direccion,
        'Teléfono'=>$telefono,
        'Estado Sede'=>$estado_sede,
        'Niveles'=>$niveles,
        'Modelos'=>$modelos,
        'Grados'=>$grados,
        ];


      return $validation_array;

    }

  public  function utf8_converter($array)
    {
        array_walk_recursive($array, function(&$item, $key){
            if(!mb_detect_encoding($item, 'utf-8', true)){
                    $item = utf8_encode($item);
            }
        });

        return $array;
    }

    public function getColumnNameByValue($array, $value)
        {
            return in_array($value, $array)? $value : '';
        }



    public function carga_masiva_establecimientos($archivo,$mes,$anio){


            DB::delete('delete from temp_historico_establecimientos');
            DB::delete('delete from temp_archivo_establecimientos');
            DB::statement("COPY temp_archivo_establecimientos FROM '$archivo' DELIMITER ';' CSV HEADER;");
                      DB::statement("insert into temp_historico_establecimientos (secretaria,codigo_departamento,departamento,codigo_dane_municipio,municipio,
                      codigo_establecimiento,nombre_establecimiento,direccion,telefono,nombre_rector,tipo_establecimiento,etnias,sector,genero,zona,niveles,
                      jornadas,caracter,especialidad,licencia,grados,modelos_educativos,capacidades_excepcionales,discapacidades,idiomas,
                      numero_de_sedes,estado,prestador_de_servicio,propiedad_de_la_planta_fisica,resguardo,matricula_contratada,calendario,
                      internado,estrato_socio_economico,correo_electronico) select secretaria,codigo_departamento,departamento,codigo_dane_municipio,municipio,
                      codigo_establecimiento,nombre_establecimiento,direccion,telefono,nombre_rector,tipo_establecimiento,etnias,sector,genero,zona,niveles,
                      jornadas,caracter,especialidad,licencia,grados,modelos_educativos,capacidades_excepcionales,discapacidades,idiomas,
                      numero_de_sedes,estado,prestador_de_servicio,propiedad_de_la_planta_fisica,resguardo,matricula_contratada,calendario,
                      internado,estrato_socio_economico,correo_electronico  from temp_archivo_establecimientos where secretaria = 'ANTIOQUIA';");


                      DB::statement("update temp_historico_establecimientos set anio_corte ='$anio', mes_corte = '$mes'  , created_at=now(), updated_at=now();");

                      DB::statement('insert into historico_establecimientos (mes_corte,anio_corte,secretaria,codigo_departamento,departamento,codigo_dane_municipio,municipio,
                                    codigo_establecimiento,nombre_establecimiento,direccion,telefono,nombre_rector,tipo_establecimiento,etnias,sector,genero,zona,niveles,
                                    jornadas,caracter,especialidad,licencia,grados,modelos_educativos,capacidades_excepcionales,discapacidades,idiomas,
                                    numero_de_sedes,estado,prestador_de_servicio,propiedad_de_la_planta_fisica,resguardo,matricula_contratada,calendario,
                                    internado,estrato_socio_economico,correo_electronico,created_at,updated_at) select * from temp_historico_establecimientos;');

                    DB::statement('refresh materialized view registro_archivos_cargados_due;');


                return view("mensajes.mensaje_error")->with("msj","Hubo un error al agregar, intentarlo nuevamente.");



}


public function carga_masiva_sedes($archivo,$mes,$anio){


        DB::delete('delete from temp_archivo_sedes');
        DB::statement("COPY temp_archivo_sedes FROM '$archivo' DELIMITER ';' CSV HEADER;");


        DB::delete('delete from temp_historico_sedes');
                  DB::statement("insert into temp_historico_sedes (secretaria,codigo_departamento,nombre_departamento,codigo_dane_municipio,
                  nombre_municipio,codigo_establecimiento,nombre_establecimiento,codigo_sede,nombre_sede,zona,direccion,telefono,
                  estado_sede,niveles,modelos,grados) select secretaria,codigo_departamento,nombre_departamento,codigo_dane_municipio,
                  nombre_municipio,codigo_establecimiento,nombre_establecimiento,codigo_sede,nombre_sede,zona,direccion,telefono,
                  estado_sede,niveles,modelos,grados from temp_archivo_sedes where secretaria = 'ANTIOQUIA';");


                  DB::statement("update temp_historico_sedes set anio_corte ='$anio', mes_corte = '$mes'  , created_at=now(), updated_at=now();");


                  DB::statement('insert into historico_sedes (mes_corte,anio_corte,secretaria,codigo_departamento,nombre_departamento,codigo_dane_municipio,
                  nombre_municipio,codigo_establecimiento,nombre_establecimiento,codigo_sede,nombre_sede,zona,direccion,telefono,
                  estado_sede,niveles,modelos,grados,created_at,updated_at) select * from temp_historico_sedes;');

                  DB::statement('refresh materialized view registro_archivos_cargados_due;');




                   return view("mensajes.mensaje_error")->with("msj","Hubo un error al agregar, intentarlo nuevamente.");



}

    public function subir_archivos(Request $request)
    {


      $reglas=[  'archivo_establecimientos' => 'required|mimes:csv,txt',
                 'archivo_sedes' => 'required|mimes:csv,txt',


            ];

      $mensajes=[  'archivo_establecimientos.mimes' => 'El archivo seleccionado en establecimientos no corresponde al formato permitido (CSV,txt) ',
                   'archivo_sedes.mimes' => 'El archivo seleccionado en sedes no corresponde al formato permitido (CSV,txt) ',

               ];


      $validator0 = Validator::make( $request->all(),$reglas,$mensajes );
      if( $validator0->fails() ){
          return view("mensajes.mensaje_error")->with("msj","Existen errores.")
                                              ->withErrors($validator0->errors());
      }

      $mes =$request->input("mes");
      $anio =$request->input("anio");
      $establecimientos = $request->file('archivo_establecimientos');
      $sedes = $request->file('archivo_sedes');


      $nuevo_nombre_establecimientos ="establecimientos.csv";
      $nuevo_nombre_sedes ="sedes.csv";


     Storage::disk('temp')->put($nuevo_nombre_establecimientos,  \File::get($establecimientos) );
     $archivo_establecimientos = Storage::disk('temp')->getDriver()->getAdapter()->applyPathPrefix('establecimientos.csv');

     Storage::disk('temp')->put($nuevo_nombre_sedes,  \File::get($sedes) );
     $archivo_sedes = Storage::disk('temp')->getDriver()->getAdapter()->applyPathPrefix('sedes.csv');

     $reglas_establecimientos=[
       'Secretaría'=>'required',
       'Código Departamento'=>'required',
       'Departamento'=>'required',
       'Código Municipio'=>'required',
       'Municipio'=>'required',
       'Código'=>'required',
       'Nombre'=>'required',
       'Dirección'=>'required',
       'Teléfono'=>'required',
       'Nombre Rector'=>'required',
       'Tipo Establecimiento'=>'required',
       'Etnias'=>'required',
       'Sector'=>'required',
       'Genero'=>'required',
       'Zona'=>'required',
       'Niveles'=>'required',
       'Jornadas'=>'required',
       'Caracter'=>'required',
       'Especialidad'=>'required',
       'Licencia'=>'required',
       'Grados'=>'required',
       'Modelos Educativos'=>'required',
       'Capacidades Excepcionales'=>'required',
       'Discapacidades'=>'required',
       'Idiomas'=>'required',
       'Número de Sedes'=>'required',
       'Estado'=>'required',
       'Prestador de servicio'=>'required',
       'Propiedad de la planta fisíca'=>'required',
       'Resguardo'=>'required',
       'Matricula contratada'=>'required',
       'Calendario'=>'required',
       'Internado'=>'required',
       'Estrato socio-economico'=>'required',
       'Correo Electrónico'=>'required',
      ];

      $reglas_sedes=[
        'Secretaria'=>'required',
        'Código departamento'=>'required',
        'Nombre departamento'=>'required',
        'Código municipio'=>'required',
        'Nombre municipio'=>'required',
        'Código Establecimiento'=>'required',
        'Nombre Establecimiento'=>'required',
        'Código Sede'=>'required',
        'Nombre Sede'=>'required',
        'Zona'=>'required',
        'Dirección'=>'required',
        'Teléfono'=>'required',
        'Estado Sede'=>'required',
        'Niveles'=>'required',
        'Modelos'=>'required',
        'Grados'=>'required',

       ];


     $mensajes1=[  'required'=>'La columna :attribute no se encuentra en el encabezado del archivo cargado o no está escrito correctamente ',
      ];

     $validator1 = Validator::make( $this->validar_header_establecimientos($archivo_establecimientos),$reglas_establecimientos,$mensajes1 );
     $validator2 = Validator::make( $this->validar_header_sedes($archivo_sedes),$reglas_sedes,$mensajes1 );



 if( $validator1->fails() ){

     return view("mensajes.mensaje_error")->with("msj","Existen errores en el archivo establecimientos.")
                                         ->withErrors($validator1->errors());

}

if( $validator2->fails() ){

    return view("mensajes.mensaje_error")->with("msj","Existen errores en el archivo sedes.")
                                        ->withErrors($validator2->errors());

}


     $busqueda1= DB::table('registro_archivos_cargados_due')->where('anio_corte', $anio)->where('mes_corte', $mes)->count();


     if( $busqueda1>0  ){

       switch($mes) {
           case 1: $mes_letras= "Enero"; break;
           case 2: $mes_letras= "Febrero"; break;
           case 3: $mes_letras= "Marzo"; break;
           case 4: $mes_letras= "Abril"; break;
           case 5: $mes_letras= "Mayo"; break;
           case 6: $mes_letras= "Junio"; break;
           case 7: $mes_letras= "Julio"; break;
           case 8: $mes_letras= "Agosto"; break;
           case 9: $mes_letras= "Septiembre"; break;
           case 10: $mes_letras= "Octubre"; break;
           case 11: $mes_letras= "Noviembre"; break;
           case 12: $mes_letras= "Diciembre"; break;
           default:$mes_letras= "No match!"; break;
       }

         return view("mensajes.mensaje_error")->with("msj","Ya existen registros para el Mes: $mes_letras Año: $anio .");
     }


      $this->carga_masiva_establecimientos($archivo_establecimientos,$mes,$anio);
      $this->carga_masiva_sedes($archivo_sedes,$mes,$anio);


      Storage::disk('temp')->delete( $nuevo_nombre_establecimientos);
      Storage::disk('temp')->delete( $nuevo_nombre_sedes);




      return view("mensajes.msj_anexo_cargado");



    }

    public function DescargarPlantillaSedes()
    {
      $filename=public_path().'/plantilla_sedes/sedes.csv';
      $finfo=finfo_open(FILEINFO_MIME_TYPE);
      $mimeType=finfo_file($finfo, $filename);

      header("Content-type:".$mimeType);
      header("Content-Disposition: attachment; filename=sedes.csv");
      echo file_get_contents($filename);
    }

    public function DescargarPlantillaEstablecimientos()
    {
      $filename=public_path().'/plantilla_establecimientos/establecimientos.csv';
      $finfo=finfo_open(FILEINFO_MIME_TYPE);
      $mimeType=finfo_file($finfo, $filename);

      header("Content-type:".$mimeType);
      header("Content-Disposition: attachment; filename=establecimientos.csv");
      echo file_get_contents($filename);
    }

    /**
     * Show the application dashboard.
     *
     * @return Response
     */
    public function index()
    {

        $meses =Meses::all();
        $listado = DB::table('registro_archivos_cargados_due')
        ->select('anio_corte', 'mes_corte','fecha', 'cantidad_establecimientos')
        ->paginate(12);



        return view("due.cargar_datos_due")->with("meses",$meses)->with("listados",$listado);
    }
}
