<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\HistoricalClassifications;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Storage;

/**
* Class HomeController
* @package App\Http\Controllers
*/
class ClasificacionesController extends Controller
{
  /**
  * Create a new controller instance.
  *
  * @return void
  */
  public function __construct()
  {
    $this->middleware('auth');
  }

  public function form_borrado_archivos_clasificaciones($periodo){
    return view("confirmaciones.form_borrado_archivos_clasificaciones")->with("periodo",$periodo);
  }


  public function borrar_archivos_clasificaciones(Request $request){

    $periodo = $request->input("periodos");

    $clasificaciones = HistoricalClassifications::where('periodo',$periodo)->delete();

    if($clasificaciones){
      DB::statement('refresh materialized view registro_archivos_cargados_clasificaciones_saber;');
      return view("mensajes.msj_borrado");
    }
    else
    {
      return view("mensajes.mensaje_error")->with("msj","Hubo un error al agregar, intentarlo nuevamente.");
    }
  }

  public function validar_header_clasificaciones($archivo){

    $opened_file = fopen($archivo, 'r');
    $header = fgetcsv($opened_file, 0, ';');
    $header_convert_trim = array_map('trim', $header);

    $periodo =  $this->getColumnNameByValue($header,'PERIODO');
    $cole_cod_dane =  $this->getColumnNameByValue($header,'COLE_COD_DANE');
    $cole_inst_nombre =  $this->getColumnNameByValue($header,'COLE_INST_NOMBRE');
    $cole_codmpio_colegio =  $this->getColumnNameByValue($header,'COLE_CODMPIO_COLEGIO');
    $cole_mpio_municipio =  $this->getColumnNameByValue($header,'COLE_MPIO_MUNICIPIO');
    $cole_cod_depto =  $this->getColumnNameByValue($header,'COLE_COD_DEPTO');
    $cole_depto_colegio =  $this->getColumnNameByValue($header,'COLE_DEPTO_COLEGIO');
    $cole_naturaleza =  $this->getColumnNameByValue($header,'COLE_NATURALEZA');
    $cole_grado =  $this->getColumnNameByValue($header,'COLE_GRADO');
    $cole_calendario_colegio =  $this->getColumnNameByValue($header,'COLE_CALENDARIO_COLEGIO');
    $cole_generopoblacion =  $this->getColumnNameByValue($header,'COLE_GENEROPOBLACION');
    $matriculados_ultimos_3 =  $this->getColumnNameByValue($header,'MATRICULADOS_ULTIMOS_3');
    $evaluados_ultimos_3 =  $this->getColumnNameByValue($header,'EVALUADOS_ULTIMOS_3');
    $indice_matematicas =  $this->getColumnNameByValue($header,'INDICE_MATEMATICAS');
    $indice_c_naturales =  $this->getColumnNameByValue($header,'INDICE_C_NATURALES');
    $indice_sociales_ciudadanas =  $this->getColumnNameByValue($header,'INDICE_SOCIALES_CIUDADANAS');
    $indice_lectura_critica =  $this->getColumnNameByValue($header,'INDICE_LECTURA_CRITICA');
    $indice_ingles =  $this->getColumnNameByValue($header,'INDICE_INGLES');
    $indice_total =  $this->getColumnNameByValue($header,'INDICE_TOTAL');
    $cole_categoria =  $this->getColumnNameByValue($header,'COLE_CATEGORIA');


    fclose($opened_file);

    $validation_array = [
      'periodo'=>$periodo,
      'cole_cod_dane'=>$cole_cod_dane,
      'cole_inst_nombre'=>$cole_inst_nombre,
      'cole_codmpio_colegio'=>$cole_codmpio_colegio,
      'cole_mpio_municipio'=>$cole_mpio_municipio,
      'cole_cod_depto'=>$cole_cod_depto,
      'cole_depto_colegio'=>$cole_depto_colegio,
      'cole_naturaleza'=>$cole_naturaleza,
      'cole_grado'=>$cole_grado,
      'cole_calendario_colegio'=>$cole_calendario_colegio,
      'cole_generopoblacion'=>$cole_generopoblacion,
      'matriculados_ultimos_3'=>$matriculados_ultimos_3,
      'evaluados_ultimos_3'=>$evaluados_ultimos_3,
      'indice_matematicas'=>$indice_matematicas,
      'indice_c_naturales'=>$indice_c_naturales,
      'indice_sociales_ciudadanas'=>$indice_sociales_ciudadanas,
      'indice_lectura_critica'=>$indice_lectura_critica,
      'indice_ingles'=>$indice_ingles,
      'indice_total'=>$indice_total,
      'cole_categoria'=>$cole_categoria,
    ];


    return $validation_array;

  }


  public function get_periodo($archivo){

    $rows = count(file($archivo));
    if($rows<=1){
      return 0;
    }
    ini_set('auto_detect_line_endings', true);

    $opened_file = fopen($archivo, 'r');
    $header = fgetcsv($opened_file, 0, ';');
    $data_row = fgetcsv($opened_file, 0, ';');
    $first_row = array_combine($header, $data_row);
    $periodoo = array_key_exists('PERIODO', $first_row)? $first_row['PERIODO'] : '';
    fclose($opened_file);

    return $periodoo;

  }

  public function getColumnNameByValue($array, $value)
  {
    return in_array($value, $array)? $value : '';
  }

  public function carga_masiva_clasificaciones($archivo, $periodo){

    DB::delete('delete from temp_historico_clasificaciones');
    DB::delete('delete from temp_archivo_clasificaciones');

    DB::statement("COPY temp_archivo_clasificaciones FROM '$archivo' DELIMITER ';' CSV HEADER;");

    DB::statement("insert into temp_historico_clasificaciones select PERIODO,COLE_COD_DANE,COLE_INST_NOMBRE,COLE_CODMPIO_COLEGIO,COLE_MPIO_MUNICIPIO,
    COLE_COD_DEPTO,COLE_DEPTO_COLEGIO,COLE_NATURALEZA,COLE_GRADO,COLE_CALENDARIO_COLEGIO,COLE_GENEROPOBLACION,MATRICULADOS_ULTIMOS_3,
    EVALUADOS_ULTIMOS_3,INDICE_MATEMATICAS,INDICE_C_NATURALES,INDICE_SOCIALES_CIUDADANAS,INDICE_LECTURA_CRITICA,INDICE_INGLES,
    INDICE_TOTAL,COLE_CATEGORIA from temp_archivo_clasificaciones;");

    DB::statement("update temp_historico_clasificaciones set periodo='$periodo', created_at=now(), updated_at=now();");

    DB::statement('insert into historico_clasificaciones (PERIODO,COLE_COD_DANE,COLE_INST_NOMBRE,COLE_CODMPIO_COLEGIO,COLE_MPIO_MUNICIPIO,
    COLE_COD_DEPTO,COLE_DEPTO_COLEGIO,COLE_NATURALEZA,COLE_GRADO,COLE_CALENDARIO_COLEGIO,COLE_GENEROPOBLACION,MATRICULADOS_ULTIMOS_3,
    EVALUADOS_ULTIMOS_3,INDICE_MATEMATICAS,INDICE_C_NATURALES,INDICE_SOCIALES_CIUDADANAS,INDICE_LECTURA_CRITICA,INDICE_INGLES,
    INDICE_TOTAL,COLE_CATEGORIA,created_at,updated_at) select * from temp_historico_clasificaciones;');

    DB::statement('refresh materialized view registro_archivos_cargados_clasificaciones_saber;');

    return view("mensajes.mensaje_error")->with("msj","Hubo un error al agregar, intentarlo nuevamente.");

  }

  public function subir_archivos(Request $request)
  {


    $reglas=[  'archivo_clasificaciones' => 'required|mimes:csv,txt',


  ];

  $mensajes=[  'archivo_clasificaciones.mimes' => 'El archivo seleccionado en clasificaciones no corresponde al formato permitido (CSV,txt) ',

];


$validator0 = Validator::make( $request->all(),$reglas,$mensajes );
if( $validator0->fails() ){
  return view("mensajes.mensaje_error")->with("msj","Existen errores.")
  ->withErrors($validator0->errors());
}

$clasificaciones = $request->file('archivo_clasificaciones');
$nuevo_nombre_clasificaciones ="clasificaciones.csv";


Storage::disk('temp')->put($nuevo_nombre_clasificaciones,  \File::get($clasificaciones) );
$archivo_clasificaciones = Storage::disk('temp')->getDriver()->getAdapter()->applyPathPrefix('clasificaciones.csv');


$reglas_clasificaciones=[
  'periodo'=>'required',
  'cole_cod_dane'=>'required',
  'cole_inst_nombre'=>'required',
  'cole_codmpio_colegio'=>'required',
  'cole_mpio_municipio'=>'required',
  'cole_cod_depto'=>'required',
  'cole_depto_colegio'=>'required',
  'cole_naturaleza'=>'required',
  'cole_grado'=>'required',
  'cole_calendario_colegio'=>'required',
  'cole_generopoblacion'=>'required',
  'matriculados_ultimos_3'=>'required',
  'evaluados_ultimos_3'=>'required',
  'indice_matematicas'=>'required',
  'indice_c_naturales'=>'required',
  'indice_sociales_ciudadanas'=>'required',
  'indice_lectura_critica'=>'required',
  'indice_ingles'=>'required',
  'indice_total'=>'required',
  'cole_categoria'=>'required',
];

$reglas_clasificaciones=[
  'periodo'=>'required',
  'cole_cod_dane'=>'required',
  'cole_inst_nombre'=>'required',
  'cole_codmpio_colegio'=>'required',
  'cole_mpio_municipio'=>'required',
  'cole_cod_depto'=>'required',
  'cole_depto_colegio'=>'required',
  'cole_naturaleza'=>'required',
  'cole_grado'=>'required',
  'cole_calendario_colegio'=>'required',
  'cole_generopoblacion'=>'required',
  'matriculados_ultimos_3'=>'required',
  'evaluados_ultimos_3'=>'required',
  'indice_matematicas'=>'required',
  'indice_c_naturales'=>'required',
  'indice_sociales_ciudadanas'=>'required',
  'indice_lectura_critica'=>'required',
  'indice_ingles'=>'required',
  'indice_total'=>'required',
  'cole_categoria'=>'required',
];


$mensajes1=[  'required'=>'La columna :attribute no se encuentra en el encabezado del archivo cargado o no está escrito correctamente ',
];

$validator1 = Validator::make( $this->validar_header_clasificaciones($archivo_clasificaciones),$reglas_clasificaciones,$mensajes1 );


if( $validator1->fails() ){

  return view("mensajes.mensaje_error")->with("msj","Existen errores en el archivo clasificaciones.")
  ->withErrors($validator1->errors());

}

$periodo= $this->get_periodo($archivo_clasificaciones);

$busqueda1= DB::table('registro_archivos_cargados_clasificaciones_saber')->where('periodo', $periodo)->count();

if( $busqueda1>0 ){
  $this->limpiar_temp($archivo_clasificaciones);
  return view("mensajes.mensaje_error")->with("msj","Ya existen registros para el periodo: $periodo.");
}

$this->carga_masiva_clasificaciones($archivo_clasificaciones, $periodo);
$this->limpiar_temp($archivo_clasificaciones);
return view("mensajes.msj_anexo_cargado_clasificaciones");
}

public function limpiar_temp($nuevo_nombre_clasificaciones)
{
  Storage::disk('temp')->delete( $nuevo_nombre_clasificaciones);
}

public function clasificaciones()
{
  $listado = DB::table('registro_archivos_cargados_clasificaciones_saber')
  ->select('periodo','fecha', 'cantidad_clasificaciones')
  ->paginate(12);

  return view("PruebaSaber.clasificaciones_saber")->with("listados",$listado);
}

public function DescargarPlantillaClasificaciones()
{
  $filename=public_path().'/plantilla_clasificaciones/clasificaciones.csv';
  $finfo=finfo_open(FILEINFO_MIME_TYPE);
  $mimeType=finfo_file($finfo, $filename);

  header("Content-type:".$mimeType);
  header("Content-Disposition: attachment; filename=clasificaciones.csv");
  echo file_get_contents($filename);
}

}
