<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Municipio;
use App\Departamento;
use App\temp_historico_sede;
use App\SedeConectividad;
use App\Subregion;
use App\temp_historico_establecimiento;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Datatables;
use Storage;
use App\SiegaTrait\SedesTrait;
use Illuminate\Support\Facades\Auth;



class ConectividadIndividualController extends Controller
{



    public function form_nuevo_sedesconectividad(){

      $subregiones = Subregion::orderBy('id', 'asc')->get();

        return view("formularios.form_nuevo_sedesconectividad")->with("subregiones", $subregiones);

    }


    public function crear_sedesconectividad(Request $request){
    //Esta validación se comento por las razones del issue 56
    //$sede_id=$request->sede_id;
    //$existenciaa = SedeConectividad::select('Codigodanesede')
    //->where('Codigodanesede', '=', $sede_id)
    //->get();

      //if(count($existenciaa) == 0){
      //$sedesconectividad=new SedeConectividad;
      //$sedesconectividad->Codigodanesede=$request->input("sede_id");
      //$nombres = temp_historico_sede::select('nombre_sede')
      //->where('codigo_sede', '=', $sedesconectividad->Codigodanesede)
      //->get();
      //foreach ($nombres as $nombre) {
      //  $nombre = $nombre->nombre_sede;
      //}
      $sedesconectividad=new SedeConectividad;
      $sedesconectividad->Codigodanesede=$request->input("sede_id");
      $sedesconectividad->Programaorigendelosrecursos=$request->input("programaorigen");
      $sedesconectividad->Numerodecontrato=$request->input("numerocontrato");
      $sedesconectividad->Operador=$request->input("operador");
      $sedesconectividad->Anchodebanda=$request->input("anchobanda");
      $sedesconectividad->Fechainicioservicio=$request->input("fechainicio");
      $sedesconectividad->Fechafinservicio=$request->input("fechafin");
      $sedesconectividad->Estado=$request->input("estado");
      $sedesconectividad->Servicio=$request->input("servicio");
      $sedesconectividad->Uso=$request->input("uso");



        if($sedesconectividad->save())
        {


          return view("mensajes.msj_sedesconectividad_creado")->with("msj","agregado correctamente.") ;
        }

        else
        {

        //  $nombres = temp_historico_sede::select('nombre_sede')
        //  ->where('codigo_sede', '=', $sede_id)
        //  ->get();
        //   return view("mensajes.mensaje_error_actualizacion4")->with("nombres",$existenciaa);
         return view("mensajes.mensaje_error")->with("msj","...Hubo un error al agregar ;...") ;
        }

    }


    public function form_editar_sedesconectividad($id){

        $sedesconectividad=SedeConectividad::findOrFail($id);
        $nombres = DB::table('temp_historico_sedes')
          ->select('nombre_municipio', 'nombre_establecimiento', 'nombre_sede', 'direccion')
          ->where('codigo_sede', '=', $sedesconectividad->Codigodanesede)
          ->get();
        return view("formularios.form_editar_sedesconectividad")->with("sedesconectividad",$sedesconectividad)->with("nombres",$nombres);
    }




    public function editar_sedesconectividad(Request $request){

      $idsedesconectividad=$request->input("id_sedesconectividad");
      $sedesconectividad=SedeConectividad::find($idsedesconectividad);
      $sedesconectividad->Codigodanesede=$request->input("sede_id");
      $sedesconectividad->Programaorigendelosrecursos=$request->input("programaorigen");
      $sedesconectividad->Numerodecontrato=$request->input("numerocontrato");
      $sedesconectividad->Operador=$request->input("operador");
      $sedesconectividad->Anchodebanda=$request->input("anchobanda");
      $sedesconectividad->Fechainicioservicio=$request->input("fechainicio");
      $sedesconectividad->Fechafinservicio=$request->input("fechafin");
      $sedesconectividad->Estado=$request->input("estado");
      $sedesconectividad->Servicio=$request->input("servicio");
      $sedesconectividad->Uso=$request->input("uso");



        if( $sedesconectividad->save()){
        return view("mensajes.msj_usuario_actualizado")
                                                         ->with("idusuario",$idsedesconectividad) ;
        }
        else
        {
        return view("mensajes.mensaje_error")->with("msj","..Hubo un error al agregar ; intentarlo nuevamente..");
        }
    }


    public function form_borrado_sedesconectividad($id){
      $sedesconectividad=SedeConectividad::find($id);
      return view("confirmaciones.form_borrado_sedesconectividad")->with("sedesconectividad",$sedesconectividad);

    }

    public function borrar_sedesconectividad(Request $request){

            $idsedesconectividad=$request->input("id_sedesconectividad");
            $sedesconectividad=SedeConectividad::find($idsedesconectividad);

            if($sedesconectividad->delete()){
                 return view("mensajes.msj_borrado");
            }
            else
            {
                return view("mensajes.mensaje_error")->with("msj","Hubo un error al boorrar ; intentarlo nuevamente.");
            }


    }

    public function listado_conectividad(){

        if(Auth::user()->isRole('secretarios'))
        {

          $datos = DB::table('sede_conectividads')
          ->join('temp_historico_sedes', 'sede_conectividads.Codigodanesede', '=', 'temp_historico_sedes.codigo_sede')
          ->select('temp_historico_sedes.nombre_sede','nombre_establecimiento','nombre_municipio','direccion','sede_conectividads.id','Codigodanesede','Programaorigendelosrecursos','Numerodecontrato',
          'Operador','Anchodebanda','Fechainicioservicio','Fechafinservicio','Estado','Servicio','Uso' )
          ->where('temp_historico_sedes.codigo_dane_municipio', Auth::user()->name)
          ->get();
        }
        elseif (Auth::user()->isRole('rectores'))
        {
          $datos = DB::table('sede_conectividads')
          ->join('temp_historico_sedes', 'sede_conectividads.Codigodanesede', '=', 'temp_historico_sedes.codigo_sede')
          ->select(['temp_historico_sedes.nombre_sede','nombre_establecimiento','nombre_municipio','direccion','sede_conectividads.id','Codigodanesede','Programaorigendelosrecursos','Numerodecontrato',
          'Operador','Anchodebanda','Fechainicioservicio','Fechafinservicio','Estado','Servicio','Uso'])
          ->where('temp_historico_sedes.codigo_establecimiento', Auth::user()->name)
          ->get();
        }
        else
        {
          $datos = DB::table('sede_conectividads')
          ->join('temp_historico_sedes', 'sede_conectividads.Codigodanesede', '=', 'temp_historico_sedes.codigo_sede')
          ->select(['temp_historico_sedes.nombre_sede','nombre_establecimiento','nombre_municipio','direccion','sede_conectividads.id','Codigodanesede','Programaorigendelosrecursos','Numerodecontrato',
          'Operador','Anchodebanda','Fechainicioservicio','Fechafinservicio','Estado','Servicio','Uso'])
          //->where('temp_historico_sedes.codigo_establecimiento', Auth::user()->name)
          ->get();
        }

        return Datatables::of($datos)->make(true);

      }


    public function listado_informacion()
    {
        return view("conectividad.individual");


    }

    public  function objectToArray($d) {
      if (is_object($d)) {
        $d = get_object_vars($d);
      }

      if (is_array($d)) {
        return array_map('trim', $d);
      }
      else {
        return $d;
      }
    }





      /**
       * Show the application dashboard.
       *
       * @return Response
       */

       public function index()
       {

       }
}
