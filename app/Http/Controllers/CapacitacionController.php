<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Meses;
use App\NombreCapacitacion;
use Illuminate\Support\Facades\DB;
use App\Matricula;
use Illuminate\Support\Facades\Validator;
use Storage;


/**
 * Class HomeController
 * @package App\Http\Controllers
 */

class CapacitacionController extends Controller
{
  /**
   * Create a new controller instance.
   *
   * @return void
   */
  public function __construct()
  {
      $this->middleware('auth');
  }

  public function carga_masiva_capacitacion($archivo,$mes,$anio,$nombre){


          DB::statement("COPY temp_archivo_capacitacion FROM '$archivo' DELIMITER ';' CSV HEADER;");



                    DB::statement("insert into temp_historico_capacitacion (cedula,nombre,apellidos,correo,telefono,municipio_origen,descripcion_capacitacion,estado_curso,
                     fecha_inicio,fecha_finalizacion) select cedula,nombre,apellidos,correo,telefono,municipio_origen,descripcion_capacitacion,estado_curso,
                      fecha_inicio,fecha_finalizacion from temp_archivo_capacitacion");

                //    DB::delete('delete from temp_archivo_capacitacion');

                    DB::statement("update temp_historico_capacitacion set anio_corte ='$anio', mes_corte ='$mes', nombre_capacitacion ='$nombre' , created_at=now(), updated_at=now();");

                    DB::statement('insert into historico_capacitacion (mes_corte,anio_corte,nombre_capacitacion,cedula,nombre,apellidos,correo,telefono,municipio_origen,descripcion_capacitacion,estado_curso,
                     fecha_inicio,fecha_finalizacion,created_at,updated_at) select * from temp_historico_capacitacion;');

                  DB::delete('delete from temp_historico_capacitacion');




                     return view("mensajes.mensaje_error")->with("msj","..Hubo un error al agregar ; intentarlo nuevamente..");



}


  public function subir_archivos(Request $request)
  {

    $mes =$request->input("mes");
    $anio =$request->input("anio");
    $nombre =$request->input("nombre");
    $capacitacion = $request->file('archivo_capacitacion');

    $nuevo_nombre_capacitacion ="capacitacion.csv";

    Storage::disk('temp')->put($nuevo_nombre_capacitacion,  \File::get($capacitacion) );
    $archivo_capacitacion = Storage::disk('temp')->getDriver()->getAdapter()->applyPathPrefix('capacitacion.csv');




    $this->carga_masiva_capacitacion($archivo_capacitacion,$mes,$anio,$nombre);

    Storage::disk('temp')->delete( $nuevo_nombre_capacitacion);




    return view("mensajes.msj_capacitacion_cargado")->with("msj","Carga exitosa") ;



  }


  /**
   * Show the application dashboard.
   *
   * @return Response
   */
  public function index()
  {

      $meses =Meses::all();
      $nombres=NombreCapacitacion::all();

      return view("capacitacion.cargar_datos_capacitacion")->with("meses",$meses)->with("nombres",$nombres);
  }
}
