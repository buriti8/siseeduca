<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Meses;
use App\HistoricoMesa;
use App\HistoricoMesa2;
use Illuminate\Support\Facades\DB;
use App\Matricula;
use Illuminate\Support\Facades\Validator;
use Storage;


/**
 * Class HomeController
 * @package App\Http\Controllers
 */

class MesaController extends Controller
{
  /**
   * Create a new controller instance.
   *
   * @return void
   */
  public function __construct()
  {
      $this->middleware('auth');
  }
  public function form_borrado_archivos_mesa($ano_info,$mes_corte){

        return view("confirmaciones.form_borrado_archivos_mesa")->with("ano_info",$ano_info)->with("mes_corte",$mes_corte);

      }

      public function borrar_archivos_mesa(Request $request){

            $anio_corte=$request->input("ano_info");
            $mes_corte=$request->input("mes_corte");

            $res_mesa=HistoricoMesa::where('anio_corte',$anio_corte)->where('mes_corte',$mes_corte)->delete();
            $res_mesa2=HistoricoMesa2::where('anio_corte',$anio_corte)->where('mes_corte',$mes_corte)->delete();

            if($res_mesa && $res_mesa2){
              DB::statement('refresh materialized view registro_archivos_cargados_mesa;');


                 return view("mensajes.msj_borrado");
            }
            else
            {
                return view("mensajes.mensaje_error")->with("msj","..Hubo un error al agregar ; intentarlo nuevamente..");
            }


    }


  public function carga_masiva_mesa($archivo,$mes,$anio){


          DB::statement("COPY temp_archivo_mesa FROM '$archivo' DELIMITER ';' CSV HEADER;");



                    DB::statement("insert into temp_historico_mesa (codigo_dane,nombre,localidad,tecnologia,id_caso,motive_id,tipificacion,tipificacion1,
                    tipificacion2,solicitud,solucion,estado_caso,nombre_causal,tipo_solucion,tiempo_gestion,fecha_creacion,fecha_real_solucion,grupo_soluciona,
                    usuario_cierra,impacto,contrato_escuela,convenio,cumplimiento_garantia ) select codigo_dane,nombre,localidad,tecnologia,id_caso,motive_id,tipificacion,tipificacion1,
                    tipificacion2,solicitud,solucion,estado_caso,nombre_causal,tipo_solucion,tiempo_gestion,fecha_creacion,fecha_real_solucion,grupo_soluciona,
                    usuario_cierra,impacto,contrato_escuela,convenio,cumplimiento_garantia from temp_archivo_mesa");

                    DB::delete('delete from temp_archivo_mesa');

                    DB::statement("update temp_historico_mesa set anio_corte ='$anio', mes_corte = '$mes'  , created_at=now(), updated_at=now();");

                    DB::statement('insert into historico_mesa (mes_corte,anio_corte,codigo_dane,nombre,localidad,tecnologia,id_caso,motive_id,tipificacion,tipificacion1,
                    tipificacion2,solicitud,solucion,estado_caso,nombre_causal,tipo_solucion,tiempo_gestion,fecha_creacion,fecha_real_solucion,grupo_soluciona,
                    usuario_cierra,impacto,contrato_escuela,convenio,cumplimiento_garantia,created_at,updated_at) select * from temp_historico_mesa;');

                  DB::delete('delete from temp_historico_mesa');
                  DB::statement('refresh materialized view registro_archivos_cargados_mesa;');


                     return view("mensajes.mensaje_error")->with("msj","..Hubo un error al agregar ; intentarlo nuevamente..");



}
public function carga_masiva_mesa2($archivo,$mes,$anio){

      try {
        DB::statement("COPY temp_archivo_mesa2 FROM '$archivo' DELIMITER ';' CSV HEADER;");



                  DB::statement("insert into temp_historico_mesa2 (id_caso,codigo_dane,nombre_sede,localidad,tecnologia,telefono_contacto,correo_contacto,
                  zona_escuela,departamento,fecha_creacion,nombre_causal,tipo_solucion,motivo,fecha_solucion,cerrado_primer_contacto,
                   impacto,estado_caso,tipificacion,tipificacion1,tipificacion2,grupo_soluciona,susccodi,convenio,quien_lo_tiene,correo_contacto2,
                   solicitud,solucion,n1,n2,cumplimiento ) select id_caso,codigo_dane,nombre_sede,localidad,tecnologia,telefono_contacto,correo_contacto,
                   zona_escuela,departamento,fecha_creacion,nombre_causal,tipo_solucion,motivo,fecha_solucion,cerrado_primer_contacto,
                    impacto,estado_caso,tipificacion,tipificacion1,tipificacion2,grupo_soluciona,susccodi,convenio,quien_lo_tiene,correo_contacto2,
                    solicitud,solucion,n1,n2,cumplimiento from temp_archivo_mesa2");

                DB::delete('delete from temp_archivo_mesa2');

                 DB::statement("update temp_historico_mesa2 set anio_corte ='$anio', mes_corte = '$mes'  , created_at=now(), updated_at=now();");

            DB::statement('insert into historico_mesa2 (mes_corte,anio_corte,id_caso,codigo_dane,nombre_sede,localidad,tecnologia,telefono_contacto,correo_contacto,
                  zona_escuela,departamento,fecha_creacion,nombre_causal,tipo_solucion,motivo,fecha_solucion,cerrado_primer_contacto,
                   impacto,estado_caso,tipificacion,tipificacion1,tipificacion2,grupo_soluciona,susccodi,convenio,quien_lo_tiene,correo_contacto2,
                   solicitud,solucion,n1,n2,cumplimiento,created_at,updated_at) select * from temp_historico_mesa2;');

                DB::delete('delete from temp_historico_mesa2');
                DB::statement('refresh materialized view registro_archivos_cargados_mesa;');






                } catch (Exception $e) {
                    report($e);

                   return view("mensajes.mensaje_error")->with("msj","..Hubo un error al agregar ; intentarlo nuevamente..");
                }


}





  public function subir_archivos(Request $request)
  {

    $mes =$request->input("mes");
    $anio =$request->input("anio");
    $mesa = $request->file('archivo_mesa');
    $mesa2 = $request->file('archivo_mesa2');

    $nuevo_nombre_mesa ="mesa.csv";
    $nuevo_nombre_mesa2 ="mesa2.csv";

    Storage::disk('temp')->put($nuevo_nombre_mesa,  \File::get($mesa) );
   $archivo_mesa = Storage::disk('temp')->getDriver()->getAdapter()->applyPathPrefix('mesa.csv');

   Storage::disk('temp')->put($nuevo_nombre_mesa2,  \File::get($mesa2) );
   $archivo_mesa2 = Storage::disk('temp')->getDriver()->getAdapter()->applyPathPrefix('mesa2.csv');

   $this->carga_masiva_mesa($archivo_mesa,$mes,$anio);
   $this->carga_masiva_mesa2($archivo_mesa2,$mes,$anio);

   Storage::disk('temp')->delete( $nuevo_nombre_mesa);
   Storage::disk('temp')->delete( $nuevo_nombre_mesa2);


    return view("mensajes.msj_anexo_cargado")->with("msj","Carga exitosa") ;



  }


  /**
   * Show the application dashboard.
   *
   * @return Response
   */
  public function index()
  {

      $meses =Meses::all();
      $listado = DB::table('registro_archivos_cargados_mesa')
        ->select('anio_corte', 'mes_corte','fecha')
        ->paginate(12);

      return view("conectividad.cargar_datos_mesa")->with("meses",$meses)->with("listados",$listado);
  }
}
