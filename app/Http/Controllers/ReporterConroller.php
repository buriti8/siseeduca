<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Subregion;
use App\HistoricoSede;

class ReporterConroller extends Controller
{
  public function index()
  {
    $subregiones=Subregion::all();
    return view('reportes.reportesview', compact('subregiones'));
  }


  public function genReport(Request $request)
  {
    $cadena = $request->input('colums');
    $tprincipal = $request->input('tPrincipal');
    $rows = DB::table($tprincipal)->select($cadena);
    $keys['local'] = $request->input('local');
    $keys['foraneo'] = $request->input('foraneo');
    $keys['tforanea'] = $request->input('tforanea');

    for ($i=0; $i < count($keys['local']); $i++) {
      $rows->leftJoin($keys['tforanea'][$i], $keys['local'][$i], "=", $keys['foraneo'][$i]);
    }

    if($request->input('hdnsubregion') != ""){
        $rows->where("subregions.CodigoSubregion", "=", $request->input('hdnsubregion'));
        if($request->input('hdnmunicipio') != ""){
            $rows->where("municipios.CodigoDaneMunicipio", "=", $request->input('hdnmunicipio'));
          }
    }

    if($request->input('hdnaniocorte') != ""){
      if($tprincipal == 'matricula_actual'){
        $campanio = "matricula_actual.ano_inf";
      }else{
        $campanio = $tprincipal.".anio_corte";
      }

        $rows->where($campanio, "=", $request->input('hdnaniocorte'));
        if($request->input('hdnmescorte') != ""){
            $rows->where($tprincipal.".mes_corte", "=", $request->input('hdnmescorte'));
          }
    }

    $filas = $rows->get();
    if(count($filas) ==0){
      echo '[{"nfilas":"0"}]';
    }else{
      foreach ($filas as $row) {
        $stdClass =  $this->objectToArray($row);
        $arreglo[] = $stdClass;
      }
      echo json_encode($arreglo);
    }
  }

    public function filtrarMunicipios(Request $request)
    {
      $subregion = $request->input('subregion');
      if($subregion == ""){
        echo '[{"CodigoDaneMunicipio":"","NombreMunicipio":""}]';
      }else {
        $cadena = ['CodigoDaneMunicipio','NombreMunicipio'];
        $tprincipal = 'municipios';
        $rows = DB::table($tprincipal)->select($cadena)->where('CodigoSubregion', '=', $subregion)->get();
        foreach ($rows as $row) {
          $stdClass =  $this->objectToArray($row);
          $arreglo[] = $stdClass;
        }
        echo json_encode($arreglo);
      }
    }

    public function filtrarAnioCorte(Request $request)
    {
      $tabla = $request->input('tPrincipal');
      if($tabla == ""){
        echo '[{"anio_corte":""}]';
      }else {
        if($tabla == 'matricula_actual'){
            $cadena = ['ano_inf'];
            $anio = "ano_inf";
        }else{
            $cadena = ['anio_corte'];
            $anio = "anio_corte";
        }

        $rows = DB::table($tabla)->select($cadena)->groupBy($anio)->orderBy($anio, 'desc');
        $filas = $rows->get();
        if(count($filas) ==0){
          echo '[{"'.$anio.'":"0"}]';
        }else{
          foreach ($filas as $row) {
            $stdClass =  $this->objectToArray($row);
            $arreglo[] = $stdClass;
          }
          echo json_encode($arreglo);
        }
      }
    }

    public function filtrarMesCorte(Request $request)
    {
      $tabla = $request->input('tPrincipal');
      $anio = $request->input('hdnaniocorte');
      if($tabla == 'matricula_actual'){
        $campo ="ano_inf";
      }else{
        $campo = "anio_corte";
      }
      if($tabla == "" || $anio == ""){
        echo '[{"mes_corte":"","mes":""}]';
      }else {
        $cadena = ['mes_corte', 'meses.mes'];
          $rows = DB::table($tabla)->select($cadena)->join('meses', 'mes_corte', "=", 'meses.id')->where($campo, "=", $anio)->groupBy('mes_corte')->groupBy('mes')->orderBy('mes_corte', 'desc');

          $filas = $rows->get();
          if(count($filas) ==0){
            echo '[{"mes_corte":"0"}]';
          }else{
            foreach ($filas as $row) {
              $stdClass =  $this->objectToArray($row);
              $arreglo[] = $stdClass;
            }
            echo json_encode($arreglo);
          }

      }
    }

    public function reportEnsayo (){
      $tabla = "matricula_actual";
      // $tabla = "historico_conectividad";
      if($tabla == ""){
        echo '[{"anio_corte":""}]';
      }else {
        if($tabla == 'matricula_actual'){
            $cadena = ['ano_inf'];
            $anio = "ano_inf";
        }else{
            $cadena = ['anio_corte'];
            $anio = "anio_corte";
        }

        $rows = DB::table($tabla)->select($cadena)->groupBy($anio)->orderBy($anio, 'desc');
        $filas = $rows->get();
        if(count($filas) ==0){
          echo '[{"'.$anio.'":"0"}]';
        }else{
          foreach ($filas as $row) {
            $stdClass =  $this->objectToArray($row);
            $arreglo[] = $stdClass;
          }
          echo json_encode($arreglo);
        }


      }
    }

  public  function objectToArray($d) {
    if (is_object($d)) {
      $d = get_object_vars($d);
    }

    if (is_array($d)) {
      return array_map('trim', $d);
    }
    else {
      return $d;
    }
  }
}
