<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Departamento;
use App\ComitesJuntas;
use App\Documentos;
use App\TipoDocumento;
use App\ConAlunmAnt;
use App\SitAcaAnt;
use App\Zona;
use App\TipoDiscapacidad;
use App\Metodologia;
use App\Estrato;
use App\Resguardo;
use App\Jornada;
use App\VictimaConflicto;
use App\CapacidadExcepcional;
use App\Etnia;
use App\Nivel;
use App\NivelMediaTotal;
use App\NivelCine;
use App\Subregion;
use App\Grado;
use App\Municipio;
use Datatables;
use Maatwebsite\Excel\Facades\Excel;

//EMPIENZAN LAS FUNCIONES PARA CADA TIPO DE VISTA.......
use Illuminate\Support\Facades\Validator;

class EstructuraController extends Controller
{





  public function crear_departamento(Request $request){


    $departamento=new Departamento;
    $departamento->CodigoDepartamento=$request->input("codigo_departamento");
    $departamento->CodigoDaneDepartamento =$request->input("codigo_dane_departamento");
    $departamento->NombreDepartamento=strtoupper( $request->input("nombre_departamento") ) ;

    if($departamento->save())
    {


      return view("mensajes.msj_info_ingresada");
    }
    else
    {
      return view("mensajes.mensaje_error")->with("msj","Hubo un error al agregar, intentarlo nuevamente.") ;
    }

  }


  public function form_editar_departamento($id){

    $departamento=Departamento::find($id);

    return view("formularios.form_editar_departamento")->with("departamento",$departamento);
  }


  public function editar_departamento(Request $request){

    $iddepartamento=$request->input("id_departamento");
    $departamento=Departamento::find($iddepartamento);
    $departamento->CodigoDepartamento=$request->input("codigo_departamento");
    $departamento->NombreDepartamento=strtoupper( $request->input("nombre_departamento") ) ;


    if( $departamento->save()){
      return view("mensajes.msj_actualizado");
    }
    else
    {
      return view("mensajes.mensaje_error")->with("msj","Hubo un error al agregar, intentarlo nuevamente.");
    }
  }


  public function form_borrado_departamento($id){
    $departamento=Departamento::find($id);
    return view("confirmaciones.form_borrado_departamento")->with("departamento",$departamento);

  }

  public function borrar_departamento(Request $request){

    $iddepartamento=$request->input("id_departamento");
    $departamento=Departamento::find($iddepartamento);

    if($departamento->delete()){
      return view("mensajes.msj_borrado");
    }
    else
    {
      return view("mensajes.mensaje_error")->with("msj","Hubo un error al borrar, intentarlo nuevamente.");
    }


  }


  //NEW Subregion...................................................................................................
  //->middleware('shinobi:administrador_sistema/Retornar vista a formularios ')
  //carga el formulario para agregar un nuevo usuario

  public function form_nuevo_subregion(){
    $departamentos= Departamento::all();
    return view("formularios.form_nuevo_subregion")->with("departamentos",$departamentos);

  }


  public function form_nuevo_departamento(){

    return view("formularios.form_nuevo_departamento");

  }


  public function form_nuevo_comiteJunta(){

    return view("formularios.form_nuevo_comiteJunta");

  }

  public function crear_comiteJunta(Request $request){

    $existencia = ComitesJuntas::select('codigo')->where('codigo','=',$request->input("codigo"))->get();
    if(count($existencia)==0){
      $data=new ComitesJuntas;
      $data->codigo=$request->input("codigo");
      $data->comite_junta=$request->input("nombre");
      if($data->save()){
        return view("mensajes.msj_info_ingresada");
      }else{
        return view("mensajes.mensaje_error")->with("msj","Hubo un error al agregar, intentarlo nuevamente.") ;
      }

    }else{

      foreach ($existencia as $codigo) {
        $codigo = $codigo->codigo;
      }

      return view("mensajes.msj_existencia_comiteJunta")->with("codigo",$codigo);

    }


  }

  public function form_nuevo_documento(){

    return view("formularios.form_nuevo_documento_estructura");

  }

  public function crear_documento(Request $request){

    $existencia = Documentos::select('codigo')->where('codigo','=',$request->input("codigo"))->get();
    if(count($existencia)==0){
      $data=new Documentos;
      $data->codigo=$request->input("codigo");
      $data->documento=$request->input("nombre");
      if($data->save()){
        return view("mensajes.msj_info_ingresada");
      }else{
        return view("mensajes.mensaje_error")->with("msj","Hubo un error al agregar, intentarlo nuevamente.") ;
      }

    }else{

      foreach ($existencia as $codigo) {
        $codigo = $codigo->codigo;
      }

      return view("mensajes.msj_existencia_comiteJunta")->with("codigo",$codigo);

    }


  }

  public function form_editar_documento($value){

    $datos=Documentos::findOrFail($value);

    return view("formularios.form_editar_documento_estructura")->with("datos",$datos)->with("value",$value);

  }

  public function editar_documento(Request $request){

    $data=Documentos::findOrFail($request->input("id"));
    $data->codigo=$request->input("codigo");
    $data->documento=$request->input("nombre");

    if($data->save())
    {
      return view("mensajes.msj_actualizado");
    }
    else
    {
      return view("mensajes.mensaje_error")->with("msj","Hubo un error al agregar, intentarlo nuevamente.") ;
    }

  }

  public function form_borrado_documento($value){

    $datos=Documentos::findOrFail($value);

    return view("confirmaciones.form_borrado_documento_estructura")->with("datos",$datos);


  }

  public function borrar_documento(Request $request){

    $data=Documentos::findOrFail($request->input("id"));

    if($data->delete())
    {
      return view("mensajes.msj_borrado");
    }
    else
    {
      return view("mensajes.mensaje_error")->with("msj","Hubo un error al agregar, intentarlo nuevamente.") ;
    }


  }

  public function form_editar_comiteJunta($value){

    $datos=ComitesJuntas::findOrFail($value);

    return view("formularios.form_editar_comiteJunta")->with("datos",$datos)->with("value",$value);


  }

  public function editar_comiteJunta(Request $request){

    $data=ComitesJuntas::findOrFail($request->input("id"));
    $data->codigo=$request->input("codigo");
    $data->comite_junta=$request->input("nombre");

    if($data->save())
    {
      return view("mensajes.msj_actualizado");
    }
    else
    {
      return view("mensajes.mensaje_error")->with("msj","Hubo un error al agregar, intentarlo nuevamente.") ;
    }


  }


  public function form_borrado_comiteJunta($value){

    $datos=ComitesJuntas::findOrFail($value);

    return view("confirmaciones.form_borrado_comiteJunta")->with("datos",$datos);


  }

  public function borrar_comiteJunta(Request $request){

    $data=ComitesJuntas::findOrFail($request->input("id"));

    if($data->delete())
    {
      return view("mensajes.msj_borrado");
    }
    else
    {
      return view("mensajes.mensaje_error")->with("msj","Hubo un error al agregar, intentarlo nuevamente.") ;
    }


  }



  public function crear_subregion(Request $request){

    $subregion=new Subregion;
    $subregion->CodigoSubregion=$request->input("codigo_subregion");
    $subregion->NombreSubregion =$request->input("nombre_subregion");
    $subregion->CodigoDepartamento =strtoupper($request->input("codigo_departamento"));
    if($subregion->save())

    {


      return view("mensajes.msj_info_ingresada");
    }
    else
    {
      return view("mensajes.mensaje_error")->with("msj","Hubo un error al agregar, intentarlo nuevamente.") ;
    }

  }

  public function form_editar_subregion($id){

    $subregion=Subregion::find($id);
    return view("formularios.form_editar_subregion")->with("subregion",$subregion);
  }

  public function editar_subregion(Request $request){

    $idsubregion=$request->input("id_subregion");
    $subregion=Subregion::find($idsubregion);
    $subregion->CodigoSubregion=$request->input("codigo_subregion");
    $subregion->NombreSubregion =$request->input("nombre_subregion");
    $subregion->CodigoDepartamento =strtoupper($request->input("codigo_departamento"));


    if( $subregion->save()){
      return view("mensajes.msj_actualizado");
    }
    else
    {
      return view("mensajes.mensaje_error")->with("msj","Hubo un error al agregar, intentarlo nuevamente.");
    }
  }



  public function form_borrado_subregion($id){
    $subregion=Subregion::find($id);
    return view("confirmaciones.form_borrado_subregion")->with("subregion",$subregion);

  }

  public function borrar_subregion(Request $request){

    $idsubregion=$request->input("id_subregion");
    $subregion=Subregion::find($idsubregion);

    if($subregion->delete()){
      return view("mensajes.msj_borrado");
    }
    else
    {
      return view("mensajes.mensaje_error")->with("msj","Hubo un error al agregar, intentarlo nuevamente.");
    }

  }


  //NEW Municipio ..................................................................................................
  //->middleware('shinobi:administrador_sistema/Retornar vista a formularios ')
  //carga el formulario para agregar un nuevo usuario

  public function form_nuevo_municipio(){
    $subregion= Subregion::all();
    return view("formularios.form_nuevo_municipio")->with("subregion",$subregion);

  }

  public function crear_municipio(Request $request){

    $municipio=new Municipio;
    $municipio->CodigoMunicipio=$request->input("codigomunicipio");
    $municipio->CodigoDaneMunicipio=$request->input("codigo_dane_municipio");
    $municipio->NombreMunicipio =strtoupper($request->input("nombre_municipio"));
    $municipio->CodigoSubregion =$request->input("codigo_subregion");

    if($municipio->save())

    {


      return view("mensajes.msj_info_ingresada");
    }
    else
    {
      return view("mensajes.mensaje_error")->with("msj","Hubo un error al agregar, intentarlo nuevamente.") ;
    }

  }


  public function form_editar_municipio($id){

    $municipio=Municipio::find($id);
    $cod_sub=$municipio->CodigoSubregion;
    $subregion1=Subregion::find($cod_sub);
    $subregion=Subregion::all();
    return view("formularios.form_editar_municipio")->with("subregion",$subregion)
    ->with("municipio",$municipio)
    ->with("subregion1",$subregion1);
  }

  public function editar_municipio(Request $request){

    $idmunicipio=$request->input("id_municipio");
    $municipio=Municipio::find($idmunicipio);
    $municipio->CodigoMunicipio=$request->input("codigomunicipio");
    $municipio->CodigoDaneMunicipio=$request->input("codigo_dane_municipio");
    $municipio->NombreMunicipio =strtoupper($request->input("nombre_municipio"));
    $municipio->CodigoSubregion =$request->input("codigo_subregion");
    //  $subregion->CodigoDepartamento =$request->input("codigo_departamento");


    if( $municipio->save()){
      return view("mensajes.msj_actualizado");
    }
    else
    {
      return view("mensajes.mensaje_error")->with("msj","Hubo un error al agregar, intentarlo nuevamente.");
    }
  }



  public function form_borrado_municipio($id){
    $municipio=Municipio::find($id);
    return view("confirmaciones.form_borrado_municipio")->with("municipio",$municipio);

  }

  public function borrar_municipio(Request $request){

    $idmunicipio=$request->input("id_municipio");
    $municipio=Municipio::find($idmunicipio);

    if($municipio->delete()){
      return view("mensajes.msj_borrado");
    }
    else
    {
      return view("mensajes.mensaje_error")->with("msj","Hubo un error al agregar, intentarlo nuevamente.");
    }

  }

  //aqui funcion $grados

  public function form_nuevo_grado(){
    $nivels= Nivel::all();
    $nivelmediatotals= NivelMediaTotal::all();
    $nivelcines= NivelCine::all();
    return view("formularios.form_nuevo_grado")->with("nivels",$nivels)
    ->with("nivelmediatotals",$nivelmediatotals)
    ->with("nivelcines",$nivelcines);






  }


  public function crear_grado(Request $request){


    $grado=new Grado;
    $grado->IdGrado=$request->input("id_grado");
    $grado->Grado =strtoupper($request->input("grado"));
    $grado->LimiteEdad=$request->input("limiteedad");
    $grado->IdNivel =$request->input("id_nivel");
    $grado->IdNivelMediaTotal =$request->input("id_nivelmediatotal");
    $grado->IdNivelCine =$request->input("id_nivelcine");




    if($grado->save())

    {


      return view("mensajes.msj_info_ingresada");
    }
    else
    {
      return view("mensajes.mensaje_error")->with("msj","Hubo un error al agregar, intentarlo nuevamente.") ;
    }


  }


  public function form_editar_grado($id){

    $grado=Grado::find($id);
    $cod_n1=$grado->IdNivel;
    $cod_n2=$grado->IdNivelMediaTotal;
    $cod_n3=$grado->IdNivelCine;
    $nivel1=Nivel::find($cod_n1);
    $nivelmediatotal1=NivelMediaTotal::find($cod_n2);
    $nivelcine1=NivelCine::find($cod_n3);
    $nivels=Nivel::all();
    $nivelmediatotals=NivelMediaTotal::all();
    $nivelcines=NivelCine::all();
    return view("formularios.form_editar_grado")->with("grado",$grado)
    ->with("nivels",$nivels)
    ->with("nivelmediatotals",$nivelmediatotals)
    ->with("nivelcines",$nivelcines)
    ->with("nivel1",$nivel1)
    ->with("nivelmediatotal1",$nivelmediatotal1)
    ->with("nivelcine1",$nivelcine1);

  }


  public function editar_grado(Request $request){

    $idgrado=$request->input("id_grado_");
    $grado=Grado::find($idgrado);
    $grado->IdGrado=strtoupper($request->input("id_grado"));
    $grado->Grado =$request->input("grado");
    $grado->LimiteEdad=$request->input("limiteedad");
    $grado->IdNivel =$request->input("id_nivel");
    $grado->IdNivelMediaTotal =$request->input("id_nivelmediatotal");
    $grado->IdNivelCine =$request->input("id_nivelcine");




    if( $grado->save()){
      return view("mensajes.msj_actualizado");
    }
    else
    {
      return view("mensajes.mensaje_error")->with("msj","Hubo un error al agregar, intentarlo nuevamente.");
    }
  }


  public function form_borrado_grado($id){
    $grado=Grado::find($id);
    return view("confirmaciones.form_borrado_grado")->with("grado",$grado);




  }

  public function borrar_grado(Request $request){

    $idgrado=$request->input("id_grado");
    $grado=Grado::find($idgrado);




    if($grado->delete()){
      return view("mensajes.msj_borrado");
    }
    else
    {
      return view("mensajes.mensaje_error")->with("msj","Hubo un error al agregar, intentarlo nuevamente.");
    }
    //FIN
  }



  // funciones de tipo documento falta modifico





  // funciones de tipo documento falta

  public function form_nuevo_tipodocumento(){

    return view("formularios.form_nuevo_tipodocumento");

  }


  public function crear_tipodocumento(Request $request){

    //AQUI VOY 5pm
    $tipodocumento=new TipoDocumento;
    $tipodocumento->IdTipoDocumento=$request->input("id_tipodocumento");
    $tipodocumento->Descripcion =strtoupper($request->input("descripcion"));
    $tipodocumento->Abreviado =strtoupper($request->input("abreviado"));


    if($tipodocumento->save())
    {


      return view("mensajes.msj_info_ingresada");
    }
    else
    {
      return view("mensajes.mensaje_error")->with("msj","Hubo un error al agregar, intentarlo nuevamente.") ;
    }

  }


  public function form_editar_tipodocumento($id){

    $tipodocumento=TipoDocumento::find($id);

    return view("formularios.form_editar_tipodocumento")->with("tipodocumento",$tipodocumento);
  }


  public function editar_tipodocumento(Request $request){

    $idtipodocumento=$request->input("id_tipo_documento");
    $tipodocumento=TipoDocumento::find($idtipodocumento);
    $tipodocumento->IdTipoDocumento=$request->input("id_tipodocumento");
    $tipodocumento->Descripcion =strtoupper($request->input("descripcion"));
    $tipodocumento->Abreviado =strtoupper($request->input("abreviado"));



    if( $tipodocumento->save()){
      return view("mensajes.msj_actualizado");
    }
    else
    {
      return view("mensajes.mensaje_error")->with("msj","Hubo un error al agregar, intentarlo nuevamente.");
    }
  }


  public function form_borrado_tipodocumento($id){
    $tipodocumento=TipoDocumento::find($id);
    return view("confirmaciones.form_borrado_tipodocumento")->with("tipodocumento",$tipodocumento);

  }

  public function borrar_tipodocumento(Request $request){

    $idtipodocumento=$request->input("id_tipo_documento");
    $tipodocumento=TipoDocumento::find($idtipodocumento);

    if($tipodocumento->delete()){
      return view("mensajes.msj_borrado");
    }
    else
    {
      return view("mensajes.mensaje_error")->with("msj","Hubo un error al agregar, intentarlo nuevamente.");
    }


  }
  //fin funcion

  // funciones de tipo documento falta modificar los formularios aqui

  public function form_nuevo_conalunmant(){

    return view("formularios.form_nuevo_conalunmant");

  }


  public function crear_conalunmant(Request $request){

    //AQUI VOY 5pm
    $conalunmant=new ConAlunmAnt;
    $conalunmant->IdConAlunmAnt=$request->input("id_conalunmant");
    $conalunmant->Descripcion =strtoupper($request->input("descripcion"));



    if($conalunmant->save())
    {


      return view("mensajes.msj_info_ingresada");
    }
    else
    {
      return view("mensajes.mensaje_error")->with("msj","Hubo un error al agregar, intentarlo nuevamente.") ;
    }

  }


  public function form_editar_conalunmant($id){

    $conalunmant=ConAlunmAnt::find($id);

    return view("formularios.form_editar_conalunmant")->with("conalunmant",$conalunmant);
  }


  public function editar_conalunmant(Request $request){

    $idconalunmant=$request->input("id_con_alunmant");
    $conalunmant=ConAlunmAnt::find($idconalunmant);
    $conalunmant->IdConAlunmAnt=$request->input("id_conalunmant");
    $conalunmant->Descripcion =strtoupper($request->input("descripcion"));




    if( $conalunmant->save()){
      return view("mensajes.msj_actualizado");
    }
    else
    {
      return view("mensajes.mensaje_error")->with("msj","Hubo un error al agregar, intentarlo nuevamente.");
    }
  }


  public function form_borrado_conalunmant($id){
    $conalunmant=ConAlunmAnt::find($id);
    return view("confirmaciones.form_borrado_conalunmant")->with("conalunmant",$conalunmant);

  }

  public function borrar_conalunmant(Request $request){

    $idconalunmant=$request->input("id_con_alunmant");
    $conalunmant=ConAlunmAnt::find($idconalunmant);

    if($conalunmant->delete()){
      return view("mensajes.msj_borrado");
    }
    else
    {
      return view("mensajes.mensaje_error")->with("msj","Hubo un error al agregar, intentarlo nuevamente.");
    }


  }
  //fin funcion
  // funciones de tipo documento falta modificar los formularios aqui

  public function form_nuevo_sitacaant(){

    return view("formularios.form_nuevo_sitacaant");

  }


  public function crear_sitacaant(Request $request){

    //AQUI VOY 5pm
    $sitacaant=new SitAcaAnt;
    $sitacaant->IdSitAcaAnt=$request->input("id_sitacaant");
    $sitacaant->Descripcion =strtoupper($request->input("descripcion"));



    if($sitacaant->save())
    {


      return view("mensajes.msj_info_ingresada");
    }
    else
    {
      return view("mensajes.mensaje_error")->with("msj","Hubo un error al agregar, intentarlo nuevamente.") ;
    }

  }


  public function form_editar_sitacaant($id){

    $sitacaant=SitAcaAnt::find($id);

    return view("formularios.form_editar_sitacaant")->with("sitacaant",$sitacaant);
  }


  public function editar_sitacaant(Request $request){

    $idsitacaant=$request->input("id_sit_acaant");
    $sitacaant=SitAcaAnt::find($idsitacaant);
    $sitacaant->IdSitAcaAnt=$request->input("id_sitacaant");
    $sitacaant->Descripcion =strtoupper($request->input("descripcion"));




    if( $sitacaant->save()){
      return view("mensajes.msj_actualizado");
    }
    else
    {
      return view("mensajes.mensaje_error")->with("msj","Hubo un error al agregar, intentarlo nuevamente.");
    }
  }


  public function form_borrado_sitacaant($id){
    $sitacaant=SitAcaAnt::find($id);
    return view("confirmaciones.form_borrado_sitacaant")->with("sitacaant",$sitacaant);

  }

  public function borrar_sitacaant(Request $request){

    $idsitacaant=$request->input("id_sit_acaant");
    $sitacaant=SitAcaAnt::find($idsitacaant);

    if($sitacaant->delete()){
      return view("mensajes.msj_borrado");
    }
    else
    {
      return view("mensajes.mensaje_error")->with("msj","Hubo un error al agregar, intentarlo nuevamente.");
    }


  }
  //fin funcion
  // funciones de tipo documento falta modificar los formularios aqui

  public function form_nuevo_zona(){

    return view("formularios.form_nuevo_zona");

  }


  public function crear_zona(Request $request){

    //AQUI VOY 5pm
    $zona=new Zona;
    $zona->IdZona=$request->input("id_zona");
    $zona->Descripcion =strtoupper($request->input("descripcion"));



    if($zona->save())
    {


      return view("mensajes.msj_info_ingresada");
    }
    else
    {
      return view("mensajes.mensaje_error")->with("msj","Hubo un error al agregar, intentarlo nuevamente.") ;
    }

  }


  public function form_editar_zona($id){

    $zona=Zona::find($id);

    return view("formularios.form_editar_zona")->with("zona",$zona);
  }


  public function editar_zona(Request $request){

    $idzona=$request->input("id_zona_");
    $zona=Zona::find($idzona);
    $zona->IdZona=$request->input("id_zona");
    $zona->Descripcion =strtoupper($request->input("descripcion"));




    if( $zona->save()){
      return view("mensajes.msj_actualizado");
    }
    else
    {
      return view("mensajes.mensaje_error")->with("msj","Hubo un error al agregar, intentarlo nuevamente.");
    }
  }


  public function form_borrado_zona($id){
    $zona=Zona::find($id);
    return view("confirmaciones.form_borrado_zona")->with("zona",$zona);

  }

  public function borrar_zona(Request $request){

    $idzona=$request->input("id_zona_");
    $zona=Zona::find($idzona);

    if($zona->delete()){
      return view("mensajes.msj_borrado");
    }
    else
    {
      return view("mensajes.mensaje_error")->with("msj","Hubo un error al agregar, intentarlo nuevamente.");
    }


  }
  //fin funcion

  // funciones de tipo documento falta modificar los formularios aqui

  public function form_nuevo_tipodiscapacidad(){

    return view("formularios.form_nuevo_tipodiscapacidad");

  }


  public function crear_tipodiscapacidad(Request $request){

    //AQUI VOY 5pm
    $tipodiscapacidad=new TipoDiscapacidad;
    $tipodiscapacidad->IdTipoDiscapacidad=$request->input("id_tipodiscapacidad");
    $tipodiscapacidad->Descripcion =strtoupper($request->input("descripcion"));



    if($tipodiscapacidad->save())
    {


      return view("mensajes.msj_info_ingresada");
    }
    else
    {
      return view("mensajes.mensaje_error")->with("msj","Hubo un error al agregar, intentarlo nuevamente.") ;
    }

  }


  public function form_editar_tipodiscapacidad($id){

    $tipodiscapacidad=TipoDiscapacidad::find($id);

    return view("formularios.form_editar_tipodiscapacidad")->with("tipodiscapacidad",$tipodiscapacidad);
  }


  public function editar_tipodiscapacidad(Request $request){

    $idtipodiscapacidad=$request->input("id_tipodiscapacidad_");
    $tipodiscapacidad=TipoDiscapacidad::find($idtipodiscapacidad);
    $tipodiscapacidad->IdTipoDiscapacidad=$request->input("id_tipodiscapacidad");
    $tipodiscapacidad->Descripcion =strtoupper($request->input("descripcion"));




    if( $tipodiscapacidad->save()){
      return view("mensajes.msj_actualizado");
    }
    else
    {
      return view("mensajes.mensaje_error")->with("msj","Hubo un error al agregar, intentarlo nuevamente.");
    }
  }


  public function form_borrado_tipodiscapacidad($id){
    $tipodiscapacidad=TipoDiscapacidad::find($id);
    return view("confirmaciones.form_borrado_tipodiscapacidad")->with("tipodiscapacidad",$tipodiscapacidad);

  }

  public function borrar_tipodiscapacidad(Request $request){

    $idtipodiscapacidad=$request->input("id_tipodiscapacidad_");
    $tipodiscapacidad=TipoDiscapacidad::find($idtipodiscapacidad);

    if($tipodiscapacidad->delete()){
      return view("mensajes.msj_borrado");
    }
    else
    {
      return view("mensajes.mensaje_error")->with("msj","Hubo un error al agregar, intentarlo nuevamente.");
    }


  }
  //fin funcion

  // funciones de tipo documento falta modificar los formularios aqui

  public function form_nuevo_metodologia(){

    return view("formularios.form_nuevo_metodologia");

  }


  public function crear_metodologia(Request $request){

    //AQUI VOY 5pm
    $metodologia=new Metodologia;
    $metodologia->IdMetodologia=$request->input("id_metodologia");
    $metodologia->Descripcion =strtoupper($request->input("descripcion"));



    if($metodologia->save())
    {


      return view("mensajes.msj_info_ingresada");
    }
    else
    {
      return view("mensajes.mensaje_error")->with("msj","Hubo un error al agregar, intentarlo nuevamente.") ;
    }

  }


  public function form_editar_metodologia($id){

    $metodologia=Metodologia::find($id);

    return view("formularios.form_editar_metodologia")->with("metodologia",$metodologia);
  }


  public function editar_metodologia(Request $request){

    $idmetodologia=$request->input("id_metodologia_");
    $metodologia=Metodologia::find($idmetodologia);
    $metodologia->IdMetodologia=$request->input("id_metodologia");
    $metodologia->Descripcion =strtoupper($request->input("descripcion"));




    if( $metodologia->save()){
      return view("mensajes.msj_actualizado");
    }
    else
    {
      return view("mensajes.mensaje_error")->with("msj","Hubo un error al agregar, intentarlo nuevamente.");
    }
  }


  public function form_borrado_metodologia($id){
    $metodologia=Metodologia::find($id);
    return view("confirmaciones.form_borrado_metodologia")->with("metodologia",$metodologia);

  }

  public function borrar_metodologia(Request $request){

    $idmetodologia=$request->input("id_metodologia_");
    $metodologia=Metodologia::find($idmetodologia);

    if($metodologia->delete()){
      return view("mensajes.msj_borrado");
    }
    else
    {
      return view("mensajes.mensaje_error")->with("msj","Hubo un error al agregar, intentarlo nuevamente.");
    }


  }
  //fin funcion


  // funciones de tipo documento falta modificar los formularios aqui

  public function form_nuevo_estrato(){

    return view("formularios.form_nuevo_estrato");

  }


  public function crear_estrato(Request $request){

    //AQUI VOY 5pm
    $estrato=new Estrato;
    $estrato->IdEstratos=$request->input("id_estrato");
    $estrato->Descripcion =strtoupper($request->input("descripcion"));



    if($estrato->save())
    {


      return view("mensajes.msj_info_ingresada");
    }
    else
    {
      return view("mensajes.mensaje_error")->with("msj","Hubo un error al agregar, intentarlo nuevamente.") ;
    }

  }


  public function form_editar_estrato($id){

    $estrato=Estrato::find($id);

    return view("formularios.form_editar_estrato")->with("estrato",$estrato);
  }


  public function editar_estrato(Request $request){

    $idestrato=$request->input("id_estrato_");
    $estrato=Estrato::find($idestrato);
    $estrato->IdEstratos=$request->input("id_estrato");
    $estrato->Descripcion =strtoupper($request->input("descripcion"));




    if( $estrato->save()){
      return view("mensajes.msj_actualizado");
    }
    else
    {
      return view("mensajes.mensaje_error")->with("msj","Hubo un error al agregar, intentarlo nuevamente.");
    }
  }


  public function form_borrado_estrato($id){
    $estrato=Estrato::find($id);
    return view("confirmaciones.form_borrado_estrato")->with("estrato",$estrato);

  }

  public function borrar_estrato(Request $request){

    $idestrato=$request->input("id_estrato_");
    $estrato=Estrato::find($idestrato);

    if($estrato->delete()){
      return view("mensajes.msj_borrado");
    }
    else
    {
      return view("mensajes.mensaje_error")->with("msj","Hubo un error al agregar, intentarlo nuevamente.");
    }


  }
  //fin funcion

  public function form_nuevo_resguardo(){

    return view("formularios.form_nuevo_resguardo");

  }


  public function crear_resguardo(Request $request){

    //AQUI VOY 5pm
    $resguardo=new Resguardo;
    $resguardo->IdResguardos=$request->input("id_resguardo");
    $resguardo->Nombre =strtoupper($request->input("nombre"));



    if($resguardo->save())
    {


      return view("mensajes.msj_info_ingresada");
    }
    else
    {
      return view("mensajes.mensaje_error")->with("msj","Hubo un error al agregar, intentarlo nuevamente.") ;
    }

  }


  public function form_editar_resguardo($id){

    $resguardo=Resguardo::find($id);

    return view("formularios.form_editar_resguardo")->with("resguardo",$resguardo);
  }


  public function editar_resguardo(Request $request){

    $idresguardo=$request->input("id_resguardo_");
    $resguardo=Resguardo::find($idresguardo);
    $resguardo->IdResguardos=$request->input("id_resguardo");
    $resguardo->Nombre =strtoupper($request->input("nombre"));




    if( $resguardo->save()){
      return view("mensajes.msj_actualizado");
    }
    else
    {
      return view("mensajes.mensaje_error")->with("msj","Hubo un error al agregar, intentarlo nuevamente.");
    }
  }


  public function form_borrado_resguardo($id){
    $resguardo=Resguardo::find($id);
    return view("confirmaciones.form_borrado_resguardo")->with("resguardo",$resguardo);

  }

  public function borrar_resguardo(Request $request){

    $idresguardo=$request->input("id_resguardo_");
    $resguardo=Resguardo::find($idresguardo);

    if($resguardo->delete()){
      return view("mensajes.msj_borrado");
    }
    else
    {
      return view("mensajes.mensaje_error")->with("msj","Hubo un error al agregar, intentarlo nuevamente.");
    }


  }
  //fin funcion


  // funciones de tipo documento falta modificar los formularios aqui

  public function form_nuevo_jornada(){

    return view("formularios.form_nuevo_jornada");

  }


  public function crear_jornada(Request $request){

    //AQUI VOY 5pm
    $jornada=new Jornada;
    $jornada->IdJornada=$request->input("id_jornada");
    $jornada->Descripcion =strtoupper($request->input("descripcion"));



    if($jornada->save())
    {


      return view("mensajes.msj_info_ingresada");
    }
    else
    {
      return view("mensajes.mensaje_error")->with("msj","Hubo un error al agregar, intentarlo nuevamente.") ;
    }

  }


  public function form_editar_jornada($id){

    $jornada=Jornada::find($id);

    return view("formularios.form_editar_jornada")->with("jornada",$jornada);
  }


  public function editar_jornada(Request $request){

    $idjornada=$request->input("id_jornada_");
    $jornada=Jornada::find($idjornada);
    $jornada->IdJornada=$request->input("id_jornada");
    $jornada->Descripcion =strtoupper($request->input("descripcion"));




    if( $jornada->save()){
      return view("mensajes.msj_actualizado");
    }
    else
    {
      return view("mensajes.mensaje_error")->with("msj","Hubo un error al agregar, intentarlo nuevamente.");
    }
  }


  public function form_borrado_jornada($id){
    $jornada=Jornada::find($id);
    return view("confirmaciones.form_borrado_jornada")->with("jornada",$jornada);

  }

  public function borrar_jornada(Request $request){

    $idjornada=$request->input("id_jornada_");
    $jornada=Jornada::find($idjornada);

    if($jornada->delete()){
      return view("mensajes.msj_borrado");
    }
    else
    {
      return view("mensajes.mensaje_error")->with("msj","Hubo un error al agregar, intentarlo nuevamente.");
    }


  }
  //fin funcion

  public function form_nuevo_victimaconflicto(){

    return view("formularios.form_nuevo_victimaconflicto");

  }

  public function crear_victimaconflicto(Request $request){

    $victimaconflicto=new VictimaConflicto;
    $victimaconflicto->IdVictimaConflicto=$request->input("id_victimaconflicto");
    $victimaconflicto->Descripcion =strtoupper($request->input("descripcion"));

    if($victimaconflicto->save())

    {


      return view("mensajes.msj_info_ingresada");
    }
    else
    {
      return view("mensajes.mensaje_error")->with("msj","Hubo un error al agregar, intentarlo nuevamente.") ;
    }

  }


  public function form_editar_victimaconflicto($id){

    $victimaconflicto=VictimaConflicto::find($id);

    return view("formularios.form_editar_victimaconflicto")->with("victimaconflicto",$victimaconflicto);
  }



  public function editar_victimaconflicto(Request $request){

    $idvictimaconflicto=$request->input("id_victima_conflicto");
    $victimaconflicto=VictimaConflicto::find($idvictimaconflicto);
    $victimaconflicto->IdVictimaConflicto=$request->input("id_victimaconflicto");
    $victimaconflicto->Descripcion =strtoupper($request->input("descripcion"));

    if( $victimaconflicto->save()){
      return view("mensajes.msj_actualizado");
    }
    else
    {
      return view("mensajes.mensaje_error")->with("msj","Hubo un error al agregar, intentarlo nuevamente.");
    }
  }


  public function form_borrado_victimaconflicto($id){
    $victimaconflicto=VictimaConflicto::find($id);
    return view("confirmaciones.form_borrado_victimaconflicto")->with("victimaconflicto",$victimaconflicto);

  }

  public function borrar_victimaconflicto(Request $request){

    $idvictimaconflicto=$request->input("id_victima_conflicto");
    $victimaconflicto=VictimaConflicto::find($idvictimaconflicto);

    if($victimaconflicto->delete()){
      return view("mensajes.msj_borrado");
    }
    else
    {
      return view("mensajes.mensaje_error")->with("msj","Hubo un error al agregar, intentarlo nuevamente.");
    }

  }
  //FIN

  //NEW CAPACIDAD EXCEPCIONAL...................................................................................................
  //->middleware('shinobi:administrador_sistema/Retornar vista a formularios ')

  public function form_nuevo_capacidadexcepcional(){

    return view("formularios.form_nuevo_capacidadexcepcional");

  }

  public function crear_capacidadexcepcional(Request $request){

    $capacidadexcepcional=new CapacidadExcepcional;
    $capacidadexcepcional->IdCapacidadExcepcional=$request->input("id_capacidadexcepcional");
    $capacidadexcepcional->Descripcion =strtoupper($request->input("descripcion"));

    if($capacidadexcepcional->save())

    {


      return view("mensajes.msj_info_ingresada");
    }
    else
    {
      return view("mensajes.mensaje_error")->with("msj","Hubo un error al agregar, intentarlo nuevamente.") ;
    }

  }


  public function form_editar_capacidadexcepcional($id){

    $capacidadexcepcional=CapacidadExcepcional::find($id);

    return view("formularios.form_editar_capacidadexcepcional")->with("capacidadexcepcional",$capacidadexcepcional);
  }



  public function editar_capacidadexcepcional(Request $request){

    $idcapacidadexcepcional=$request->input("id_capacidad_excepcional");
    $capacidadexcepcional=CapacidadExcepcional::find($idcapacidadexcepcional);
    $capacidadexcepcional->IdCapacidadExcepcional=$request->input("id_capacidadexcepcional");
    $capacidadexcepcional->Descripcion =strtoupper($request->input("descripcion"));

    if( $capacidadexcepcional->save()){
      return view("mensajes.msj_actualizado")->with("msj","Capacidad excepcional actualizada correctamente.")
      ->with("idusuario",$idcapacidadexcepcional) ;
    }
    else
    {
      return view("mensajes.mensaje_error")->with("msj","Hubo un error al agregar, intentarlo nuevamente.");
    }
  }


  public function form_borrado_capacidadexcepcional($id){
    $capacidadexcepcional=CapacidadExcepcional::find($id);
    return view("confirmaciones.form_borrado_capacidadexcepcional")->with("capacidadexcepcional",$capacidadexcepcional);

  }

  public function borrar_capacidadexcepcional(Request $request){

    $idcapacidadexcepcional=$request->input("id_capacidad_excepcional");
    $capacidadexcepcional=CapacidadExcepcional::find($idcapacidadexcepcional);

    if($capacidadexcepcional->delete()){
      return view("mensajes.msj_borrado");
    }
    else
    {
      return view("mensajes.mensaje_error")->with("msj","Hubo un error al agregar, intentarlo nuevamente.");
    }
    //FIN
  }

  //NEW CAPACIDAD ETNIA...................................................................................................
  //->middleware('shinobi:administrador_sistema/Retornar vista a formularios ')

  public function form_nuevo_etnia(){

    return view("formularios.form_nuevo_etnia");

  }

  public function crear_etnia(Request $request){

    $etnia=new Etnia;
    $etnia->IdEtnias=$request->input("id_etnias");
    $etnia->Descripcion =strtoupper($request->input("descripcion"));

    if($etnia->save())

    {


      return view("mensajes.msj_info_ingresada");
    }
    else
    {
      return view("mensajes.mensaje_error")->with("msj","Hubo un error al agregar, intentarlo nuevamente.") ;
    }

  }


  public function form_editar_etnia($id){

    $etnia=Etnia::find($id);

    return view("formularios.form_editar_etnia")->with("etnia",$etnia);
  }



  public function editar_etnia(Request $request){

    $IdEtnias=$request->input("id_etnias");
    $etnia=Etnia::find($IdEtnias);
    $etnia->IdEtnias=$request->input("id_etnias");
    $etnia->Descripcion =strtoupper($request->input("descripcion"));

    if( $etnia->save()){
      return view("mensajes.msj_actualizado");
    }
    else
    {
      return view("mensajes.mensaje_error")->with("msj","Hubo un error al agregar, intentarlo nuevamente.");
    }
  }


  public function form_borrado_etnia($id){
    $etnia=Etnia::find($id);
    return view("confirmaciones.form_borrado_etnia")->with("etnia",$etnia);

  }

  public function borrar_etnia(Request $request){

    $IdEtnias=$request->input("id_etnias");
    $etnia=Etnia::find($IdEtnias);

    if($etnia->delete()){
      return view("mensajes.msj_borrado");
    }
    else
    {
      return view("mensajes.mensaje_error")->with("msj","Hubo un error al agregar, intentarlo nuevamente.");
    }
    //FIN
  }

  //NEW NIVEL...................................................................................................
  //->middleware('shinobi:administrador_sistema/Retornar vista a formularios ')

  public function form_nuevo_nivel(){

    return view("formularios.form_nuevo_nivel");

  }

  public function crear_nivel(Request $request){

    $nivel=new Nivel;
    $nivel->IdNivel=$request->input("id_nivel");
    $nivel->Descripcion =strtoupper($request->input("descripcion"));

    if($nivel->save())

    {


      return view("mensajes.msj_info_ingresada");
    }
    else
    {
      return view("mensajes.mensaje_error")->with("msj","Hubo un error al agregar, intentarlo nuevamente.") ;
    }

  }


  public function form_editar_nivel($id){

    $nivel=Nivel::find($id);

    return view("formularios.form_editar_nivel")->with("nivel",$nivel);
  }


  public function editar_nivel(Request $request){

    $idnivel=$request->input("id_nivel_1");
    $nivel=Nivel::find($idnivel);
    $nivel->IdNivel=$request->input("id_nivel");
    $nivel->Descripcion =strtoupper($request->input("descripcion"));


    if( $nivel->save()){
      return view("mensajes.msj_actualizado");
    }
    else
    {
      return view("mensajes.mensaje_error")->with("msj","Hubo un error al agregar, intentarlo nuevamente.");
    }
  }


  public function form_borrado_nivel($id){
    $nivel=Nivel::find($id);
    return view("confirmaciones.form_borrado_nivel")->with("nivel",$nivel);

  }

  public function borrar_nivel(Request $request){

    $idnivel=$request->input("id_nivel");
    $nivel=Nivel::find($idnivel);

    if($nivel->delete()){
      return view("mensajes.msj_borrado");
    }
    else
    {
      return view("mensajes.mensaje_error")->with("msj","Hubo un error al agregar, intentarlo nuevamente.");
    }
    //FIN
  }

  //NEW NIVEL MEDIA TOTAL...................................................................................................
  //->middleware('shinobi:administrador_sistema/Retornar vista a formularios ')

  public function form_nuevo_nivelmediatotal(){

    return view("formularios.form_nuevo_nivelmediatotal");

  }

  public function crear_nivelmediatotal(Request $request){

    $nivelmediatotal=new NivelMediaTotal;
    $nivelmediatotal->IdNivelMediaTotal=$request->input("id_nivelmediatotal");
    $nivelmediatotal->Descripcion =strtoupper($request->input("descripcion"));
    $nivelmediatotal->LimiteEdad =$request->input("limiteedad");

    if($nivelmediatotal->save())

    {


      return view("mensajes.msj_info_ingresada");
    }
    else
    {
      return view("mensajes.mensaje_error")->with("msj","Hubo un error al agregar, intentarlo nuevamente.") ;
    }

  }


  public function form_editar_nivelmediatotal($id){

    $nivelmediatotal=NivelMediaTotal::find($id);

    return view("formularios.form_editar_nivelmediatotal")->with("nivelmediatotal",$nivelmediatotal);
  }


  public function editar_nivelmediatotal(Request $request){

    $idnivelmediatotal=$request->input("id_nivel_media_total");
    $nivelmediatotal=NivelMediaTotal::find($idnivelmediatotal);
    $nivelmediatotal->IdNivelMediaTotal=$request->input("id_nivelmediatotal");
    $nivelmediatotal->Descripcion =strtoupper($request->input("descripcion"));
    $nivelmediatotal->LimiteEdad =$request->input("limiteedad");


    if( $nivelmediatotal->save()){
      return view("mensajes.msj_actualizado");
    }
    else
    {
      return view("mensajes.mensaje_error")->with("msj","Hubo un error al agregar, intentarlo nuevamente.");
    }
  }


  public function form_borrado_nivelmediatotal($id){
    $nivelmediatotal=NivelMediaTotal::find($id);
    return view("confirmaciones.form_borrado_nivelmediatotal")->with("nivelmediatotal",$nivelmediatotal);

  }

  public function borrar_nivelmediatotal(Request $request){

    $idnivelmediatotal=$request->input("id_nivelmediatotal");
    $nivelmediatotal=NivelMediaTotal::find($idnivelmediatotal);

    if($nivelmediatotal->delete()){
      return view("mensajes.msj_borrado");
    }
    else
    {
      return view("mensajes.mensaje_error")->with("msj","Hubo un error al agregar, intentarlo nuevamente.");
    }
    //FIN
  }

  //NEW NIVEL CINE...................................................................................................
  //->middleware('shinobi:administrador_sistema/Retornar vista a formularios ')

  public function form_nuevo_nivelcine(){

    return view("formularios.form_nuevo_nivelcine");

  }

  public function crear_nivelcine(Request $request){

    $nivelcine=new NivelCine;
    $nivelcine->IdNivelCine=$request->input("id_nivelcine");
    $nivelcine->Descripcion =strtoupper($request->input("descripcion"));

    if($nivelcine->save())

    {


      return view("mensajes.msj_info_ingresada");
    }
    else
    {
      return view("mensajes.mensaje_error")->with("msj","Hubo un error al agregar, intentarlo nuevamente.") ;
    }

  }


  public function form_editar_nivelcine($id){

    $nivelcine=NivelCine::find($id);

    return view("formularios.form_editar_nivelcine")->with("nivelcine",$nivelcine);
  }


  public function editar_nivelcine(Request $request){

    $idnivelcine=$request->input("id_nivel_cine");
    $nivelcine=NivelCine::find($idnivelcine);
    $nivelcine->IdNivelCine=$request->input("id_nivelcine");
    $nivelcine->Descripcion =strtoupper($request->input("descripcion"));


    if( $nivelcine->save()){
      return view("mensajes.msj_actualizado");
    }
    else
    {
      return view("mensajes.mensaje_error")->with("msj","Hubo un error al agregar, intentarlo nuevamente.");
    }
  }


  public function form_borrado_nivelcine($id){
    $nivelcine=NivelCine::find($id);
    return view("confirmaciones.form_borrado_nivelcine")->with("nivelcine",$nivelcine);

  }

  public function borrar_nivelcine(Request $request){

    $idnivelcine=$request->input("id_nivelcine");
    $nivelcine=NivelCine::find($idnivelcine);

    if($nivelcine->delete()){
      return view("mensajes.msj_borrado");
    }
    else
    {
      return view("mensajes.mensaje_error")->with("msj","Hubo un error al agregar, intentarlo nuevamente.");
    }
    //FIN
  }


  //Funciones DataTable submódulo editar estructura matrícula
  public function listado_municipios(){
    $datos = Municipio::select(['id','CodigoMunicipio','CodigoDaneMunicipio','NombreMunicipio','CodigoSubregion']);
    return Datatables::of($datos)->make(true);
  }

  public function listado_comiteJunta(){
    $datos = ComitesJuntas::select(['id','codigo','comite_junta']);
    return Datatables::of($datos)->make(true);
  }

  public function listado_documento(){
    $datos = Documentos::select(['id','codigo','documento']);
    return Datatables::of($datos)->make(true);
  }





  public function listado_departamentos(){
    $datos = Departamento::select(['id','CodigoDepartamento','CodigoDaneDepartamento','NombreDepartamento']);
    return Datatables::of($datos)->make(true);
  }

  public function listado_subregiones(){
    $datos = Subregion::select(['id','CodigoSubregion','NombreSubregion','CodigoDepartamento']);
    return Datatables::of($datos)->make(true);
  }

  public function listado_tipodoc(){
    $datos = TipoDocumento::select(['id','IdTipoDocumento','Descripcion','Abreviado']);
    return Datatables::of($datos)->make(true);
  }
  public function listado_conaluant(){
    $datos = ConAlunmAnt::select(['id','IdConAlunmAnt','Descripcion']);
    return Datatables::of($datos)->make(true);
  }

  public function listado_sitacant(){
    $datos = SitAcaAnt::select(['id','IdSitAcaAnt','Descripcion']);
    return Datatables::of($datos)->make(true);
  }

  public function listado_zonas(){
    $datos = Zona::select(['id','IdZona','Descripcion']);
    return Datatables::of($datos)->make(true);
  }

  public function listado_tipodis(){
    $datos = TipoDiscapacidad::select(['id','IdTipoDiscapacidad','Descripcion']);
    return Datatables::of($datos)->make(true);
  }

  public function listado_metodologia(){
    $datos = Metodologia::select(['id','IdMetodologia','Descripcion']);
    return Datatables::of($datos)->make(true);
  }

  public function listado_estrato(){
    $datos = Estrato::select(['id','IdEstratos','Descripcion']);
    return Datatables::of($datos)->make(true);
  }

  public function listado_resguardos(){
    $datos = Resguardo::select(['id','IdResguardos','Nombre']);
    return Datatables::of($datos)->make(true);
  }

  public function listado_jornada(){
    $datos = Jornada::select(['id','IdJornada','Descripcion']);
    return Datatables::of($datos)->make(true);
  }

  public function listado_etnias(){
    $datos = Etnia::select(['id','IdEtnias','Descripcion']);
    return Datatables::of($datos)->make(true);
  }

  public function listado_victcon(){
    $datos = VictimaConflicto::select(['id','IdVictimaConflicto','Descripcion']);
    return Datatables::of($datos)->make(true);
  }

  public function listado_capacidadexcepcionals(){
    $datos = CapacidadExcepcional::select(['id','IdCapacidadExcepcional','Descripcion']);
    return Datatables::of($datos)->make(true);
  }

  public function listado_nivel(){
    $datos = Nivel::select(['id','IdNivel','Descripcion']);
    return Datatables::of($datos)->make(true);
  }

  public function listado_nivelmedia(){
    $datos = NivelMediaTotal::select(['id','IdNivelMediaTotal','Descripcion', 'LimiteEdad']);
    return Datatables::of($datos)->make(true);
  }

  public function listado_nivelcine(){
    $datos = NivelCine::select(['id','IdNivelCine','Descripcion']);
    return Datatables::of($datos)->make(true);
  }

  public function listado_grado(){
    $datos = Grado::select(['id','IdGrado','Grado', 'LimiteEdad', 'IdNivel', 'IdNivelMediaTotal', 'IdNivelCine']);
    return Datatables::of($datos)->make(true);
  }
  //Fin funciones DataTable submódulo editar estructura matrícula


  public function listado_estructura(){
    //carga el formulario para agregar un nuevo usuario

    $departamentos=Departamento::paginate(10);
    $tipodocumentos=TipoDocumento::paginate(10);
    $conalunmants=ConAlunmAnt::paginate(10);
    $sitacaants=SitAcaAnt::paginate(10);
    $zonas=Zona::Paginate(10);
    $tipodiscapacidads=TipoDiscapacidad::paginate(10);
    $metodologias=Metodologia::paginate(10);
    $estratos=Estrato::paginate(10);
    $resguardos=Resguardo::paginate(10);
    $jornadas=Jornada::paginate(10);
    $victimaconflictos=VictimaConflicto::paginate(10);
    $capacidadexcepcionals=CapacidadExcepcional::paginate(10);
    $etnias=Etnia::paginate(10);
    $nivels=Nivel::paginate(10);
    $nivelmediatotals=NivelMediaTotal::paginate(10);
    $nivelcines=NivelCine::paginate(10);
    $subregions=Subregion::paginate(10);
    $grados=Grado::paginate(10);





    return view("matricula.editar_estructura")
    ->with("departamentos",$departamentos)
    ->with("tipodocumentos",$tipodocumentos)
    ->with("conalunmants",$conalunmants)
    ->with("sitacaants",$sitacaants)
    ->with("zonas",$zonas)
    ->with("tipodiscapacidads",$tipodiscapacidads)
    ->with("metodologias",$metodologias)
    ->with("estratos",$estratos)
    ->with("resguardos",$resguardos)
    ->with("jornadas",$jornadas)
    ->with("victimaconflictos",$victimaconflictos)
    ->with("capacidadexcepcionals",$capacidadexcepcionals)
    ->with("etnias",$etnias)
    ->with("nivels",$nivels)
    ->with("nivelmediatotals",$nivelmediatotals)
    ->with("nivelcines",$nivelcines)
    ->with("subregion",$subregions)
    ->with("grados",$grados);

  }
  public function listado_avanzada(){
    //carga el formulario para agregar un nuevo usuario







    return view("matricula.consulta_avanzada");

  }
  /**
  * Show the application dashboard.
  *
  * @return Response
  */
  public function index()
  {

  }

}
