<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Solicitud;
use App\TipoDispositivo;
use App\Subregion;
use App\HistoricoEstablecimiento;
use App\temp_historico_sede;
use App\temp_historico_establecimiento;
use App\SedesServicio;
use App\SedeConectividad;
use App\RespuestaSolicitudIT;


use App\modal;

use App\SiegaTrait\SedesTrait;

use Illuminate\Support\Facades\DB;
use Datatables;
use Carbon\Carbon;
use Illuminate\Support\Facades\Validator;
use Storage;

use Illuminate\Support\Facades\Auth;

class SolicitudesController extends Controller
{
    //Solicitudes de infraestructura tecnológica.

    use SedesTrait;

    /**
     * Create a new controller instance.
     *
     * @return void
     */

    public function __construct()
    {
        $this->middleware('auth');
    }



    public function filtrarMunicipiosEstablecimientos(Request $request){
      $daneMunicipio = $request->input('municipio');
      $establecimientos = temp_historico_establecimiento::all()
      ->sortBy('nombre_establecimiento')
      ->where("prestador_de_servicio", "=", "OFICIAL")
      ->where("estado", "<>", 'CIERRE DEFINITIVO')
      ->where("estado", "<>", 'CIERRE TEMPORAL')
      ->where("codigo_dane_municipio", "=", $daneMunicipio);

      $arreglo = $this->conversionArray($establecimientos);
      echo json_encode($arreglo);

    }

    public function FiltroSede(Request $request){
      $establecimiento = $request->input('selEstablecimiento');
      $sedes = temp_historico_sede::all()
      ->where("estado_sede", "<>", "CIERRE DEFINITIVO")
      ->where("estado_sede", "<>", "CIERRE TEMPORAL")
      ->where("codigo_establecimiento", "=", $establecimiento);
      $arreglo = $this->conversionArray($sedes);
      echo json_encode($arreglo);

    }

    public  function conversionArray($rows) {
      foreach ($rows as $row) {
        $stdClass =  $this->objectToArray($row->toArray());
        $arreglo[] = $stdClass;
      }
      return $arreglo;
    }

    public function RegMunicipio(Request $request, $daneMunicipio){

      $SolicitudIF = Solicitud::whereHas('Establecimiento', function($q) {
        $q->where("codigo_dane_municipio", "=", Auth::user()->name);
      })->where("respuesta", "=", "false")->paginate(25);

      $SolicitudIFAT = Solicitud::whereHas('Establecimiento', function($q) {
        $q->where("codigo_dane_municipio", "=", Auth::user()->name);
      })->where("respuesta", "=", "true")->paginate(25);

      return view('solicitudes.reqconsultamunn')->with("SolicitudIF", $SolicitudIF)
                                                ->with("SolicitudIFAT", $SolicitudIFAT);

    }

    public function RegEstablecimiento(Request $request, $codigo_establecimiento){
      $establecimiento = temp_historico_establecimiento::all()->where("codigo_establecimiento", "=", $codigo_establecimiento);

      $SolicitudIF = Solicitud::where("DaneEstablecimiento", "=", $codigo_establecimiento)->where("respuesta", "=", "false")->paginate(25);
      $SolicitudIFAT = Solicitud::where("DaneEstablecimiento", "=", $codigo_establecimiento)->where("respuesta", "=", "true")->paginate(25);

      return view('solicitudes.reqconsultaestt')->with("SolicitudIF", $SolicitudIF)
                                                ->with("SolicitudIFAT", $SolicitudIFAT)
                                                 ->with("establecimiento", $establecimiento);
    }

    public function RegSede(Request $request, $codigo_sede){
      $sede = temp_historico_sede::all()->where("codigo_sede", "=", $codigo_sede);

      $SolicitudIF = Solicitud::where("sede_id", "=", $codigo_sede)->where("respuesta", "=", "false")->paginate(25);
      $SolicitudIFAT = Solicitud::where("sede_id", "=", $codigo_sede)->where("respuesta", "=", "true")->paginate(25);

      return view('solicitudes.reqconsultasedee')->with("SolicitudIF", $SolicitudIF)
                                                ->with("SolicitudIFAT", $SolicitudIFAT)
                                                 ->with("sede", $sede);

    }

    public function RegGen(Request $request){

      $SolicitudIF = Solicitud::where("respuesta", "=", "false");
      $SolicitudIFAT = Solicitud::where("respuesta", "=", "true");

      $SolicitudIFAT = $SolicitudIFAT->paginate(25);
      $SolicitudIF = $SolicitudIF->paginate(25);



      return view('solicitudes.reqconsultagenn')->with("SolicitudIF", $SolicitudIF)
                                                ->with("SolicitudIFAT", $SolicitudIFAT);

    }






    //función encargada de llamar el "form_nuevo_modal" Para realizar la validación.
    public function form_nuevo_modal()
    {
      $temp_historico_establecimientos = temp_historico_establecimiento::all()
      ->sortBy('nombre_establecimiento')
      ->where("prestador_de_servicio", "=", "OFICIAL")
      ->where("estado", "<>", 'CIERRE DEFINITIVO')
      ->where("estado", "<>", 'CIERRE TEMPORAL');

      $usuario = Auth::user()->user;
      if(Auth::user()->isRole('secretarios')){
        $temp_historico_establecimientos = $temp_historico_establecimientos ->where("codigo_dane_municipio", "=", Auth::user()->name);
      }elseif (Auth::user()->isRole('rectores')) {
        $temp_historico_establecimientos = $temp_historico_establecimientos ->where("codigo_establecimiento", "=", Auth::user()->name);
      }


        return view('formularios.form_nuevo_modal', array('temp_historico_establecimientos' => $temp_historico_establecimientos));
    }

    //Función encargada de guardar la información y realizar las validación.

    public function crear_modal(Request $request){

      $establecimiento = $request->input('selEstablecimiento');
      $sede = $request->input('selSede');
      $user = $request->input('userSolicitud');
      //$sede_id=$request->sede_id;
      $existencia = SedesServicio::select('DaneSede','NombreSede')->where('DaneSede', '=', $sede)->get();
      $existenciaaa = SedesServicio::select('DaneSede','NombreSede','TipoEnergia')->where('DaneSede', '=', $sede)->where('TipoEnergia', '=',"No tiene")->get();
      $existenciaa = SedeConectividad::select('Codigodanesede','Uso','Programaorigendelosrecursos')->where('Codigodanesede', '=', $sede)->where('Uso','=', "Educativo" )->get();


      if (count($existencia)==0){

              $modal = new modal;

              $modal->DaneSede=$request->input("selSede");
              $modal->DaneEstablecimiento=$request->input("selEstablecimiento");
              $modal->Usuario=$request->input("userSolicitud");


              $modal->save();

              $nombres = temp_historico_sede::where("codigo_sede","=",$sede)->get(["nombre_sede","codigo_sede"]);

              return view("mensajes.mensaje_validacionn")->with("nombres",$nombres);
            }

            elseif (count($existenciaaa)==1) {


            $nombres = temp_historico_sede::where("codigo_sede","=",$sede)->get(["nombre_sede","codigo_sede"]);
            return view("mensajes.mensaje_advertenciaaa")->with("nombres",$nombres);

           }

            elseif (count($existenciaa)==0) {
              $modal = new modal;

              $modal->DaneSede=$request->input("selSede");
              $modal->DaneEstablecimiento=$request->input("selEstablecimiento");
              $modal->Usuario=$request->input("userSolicitud");


              $modal->save();


            return view("mensajes.msj_advertencia");


            }
            else{
              $modal = new modal;

              $modal->DaneSede=$request->input("selSede");
              $modal->DaneEstablecimiento=$request->input("selEstablecimiento");
              $modal->Usuario=$request->input("userSolicitud");


              $modal->save();
              $nombres = temp_historico_sede::where("codigo_sede","=",$sede)->get(["nombre_sede","codigo_sede"]);

              return view("mensajes.mensaje_advertenciaa")->with("nombres",$nombres);

            }



    }

    public function form_nueva_solicitud(Request $request)
    {

      $tipos = TipoDispositivo::orderBy('id', 'asc')->get();
      //$subregiones = Subregion::orderBy('id', 'asc')->get();
       $sede_id = modal::all()->last();

       $temp_historico_establecimientos = temp_historico_establecimiento::all()
       ->sortBy('nombre_establecimiento')
       ->where("prestador_de_servicio", "=", "OFICIAL")
       ->where("estado", "<>", 'CIERRE DEFINITIVO')
       ->where("estado", "<>", 'CIERRE TEMPORAL');



       $nombres = temp_historico_sede::select('nombre_sede', 'codigo_sede')
         ->where("codigo_sede","=",$sede_id->DaneSede)
         ->get();





       return view('formularios.form_nueva_solicitud')->with("tipos", $tipos)


                                                      ->with("nombres", $nombres);


    }



    public function crear_solicitud(Request $request)
    {

      $DaneEstablecimiento =modal::all()->last();
      $establecimientos= temp_historico_establecimiento::select('codigo_dane_municipio','nombre_establecimiento','codigo_establecimiento')
        ->where("codigo_establecimiento","=",$DaneEstablecimiento->DaneEstablecimiento)
        ->get();

        $u =modal::all()->last();
        $usuarios= modal::select('Usuario')
         ->where("Usuario","=",$u->Usuario)
         ->get();



        $archivo = '';

        if($request->hasFile('solicitud_fisica'))
        {                $archivo = 'asdasd';
            $reglas=[  'solicitud_fisica' => 'required|mimes:pdf',];

            $mensajes=[  'solicitud_fisica.mimes' => 'El archivo seleccionado en Solicitudes no corresponde al formato permitido (pdf)'];

            $validator0 = Validator::make( $request->all(),$reglas,$mensajes );
            if( $validator0->fails() )
            {
                return view("mensajes.mensaje_error_adjunto_solicitud")->with("msj","...Existen errores...")
                                            ->withErrors($validator0->errors());
            }

            $file = $request->file('solicitud_fisica');
            $name = $file->getClientOriginalName();
            $file->move(public_path().'/solicitudes', $name);

            $archivo = '/solicitudes/' . $name;
        }

        if($request->input("tipo_solicitud") == "1")
        {
            $tipodisp = $request->input("tipo_id");
        }
        else
        {
            $tipodisp = null;
        }
        $TipoSolicitud=  $request->input("tipo_solicitud");
        $descripcion=  strtoupper($request->input("descripcion"));
        $cantidad= $request->input("cantidad_d");
        $tipouso= $request->input("tipo_uso");
        $sede= $request->input("sede_id");
        $dane= $request->input("DaneEstablecimiento");
        $usu=$request->input("Usuario");




        $solicitud=new Solicitud;
        $solicitud->sede_id =  $sede;
        $solicitud->tipo_solicitud =  $TipoSolicitud;
        $solicitud->tipodispositivo_id =  $tipodisp;
        $solicitud->descripcion =  $descripcion;
        $solicitud->estado_atencion =  0;
        $solicitud->solicitud_fisica = $archivo;
        $solicitud->cantidad_d = $cantidad;
        $solicitud->tipo_uso = $tipouso;
        $solicitud->respuesta =false;
        foreach ($establecimientos as $key => $establecimiento) {
          $solicitud->DaneEstablecimiento=$establecimiento->codigo_establecimiento;
        }
        foreach ($usuarios as $key => $usuario) {
          $solicitud->Usuario=$usuario->Usuario;
        }
        $now = new \DateTime();
        $now->format('d-m-Y');
        $solicitud->Fecha = $now;


        if($solicitud->save())
        {
            return view("mensajes.msj_solicitud_creada")->with("msj","Solicitud agregada correctamente");
        }
        else
        {
            return view("mensajes.mensaje_error_solicitud_dispositivos")->with("msj","...Hubo un error al agregar ;...") ;
        }


        }

        public function verDetalleSolicitud($id){
          $SolicitudIF = Solicitud::all()->where("id", "=", $id);
          $tipos = TipoDispositivo::orderBy('id', 'asc')->get();

          return view("solicitudes.reqconsultadetallee")
          ->with("tipos", $tipos)
          ->with("SolicitudIF", $SolicitudIF);



        }

        public function updateSolicitud(Request $request){
          $id = $request->input('valueupdatee');
          $solicitud = Solicitud::find($id);
          $tipos = TipoDispositivo::orderBy('id', 'asc')->get();
          $solicitud->respuesta = true;
          $solicitud->save();
          $SolicitudIF = Solicitud::all()->where("id", "=", $id);
          return view("solicitudes.reqconsultadetallee")
          ->with("tipos", $tipos)
          ->with("SolicitudIF", $SolicitudIF);
        }




    public function form_borrado_solicitud($id)
    {
        $solicitud = Solicitud::find($id);
        return view("confirmaciones.form_borrado_solicitud")->with("solicitud",$solicitud);
    }
    //Función encargada de eliminar el resgitro de la petición, sino se procede a ingresar la información de los riesgos de la sede educativa.
      public function eliminaSolicitud(Request $request){
        $id = $request->input('selSede');

        $solicitud = solicitud::where("sede_id", "=", $id)->delete();
        if($solicitud){
          return view("home");
        }
        else
        {
          return view("mensajes.mensaje_error")->with("msj","Ocurrió un error, recarga la página e intente nuevamente");
        }

      }

    public function borrar_solicitud(Request $request)
    {
        $solicitud = Solicitud::find($request->input('id_solicitud'));

        if($solicitud->delete()){
            return view("mensajes.msj_borrado");
        }
        else
        {
            return view("mensajes.mensaje_error_2")->with("msj","..Hubo un error al borrar ; intentarlo nuevamente..");
        }
    }

    //función encargada de llamar el "form_ingresar_SedesServicio" luego de la validación, para ingresar la información de los sedesservicio de la sedes educativas.
    public function form_ingresar_sedesservicio(){

      $sede_id = modal::all()->last();

      $nombres = temp_historico_sede::select('nombre_sede', 'codigo_sede')
        ->where("codigo_sede","=",$sede_id->DaneSede)
        ->get();

      return view("formularios.form_ingresar_sedesservicio")->with("nombres", $nombres);

    }

    //función encargada de guardar en las tabla sedes_servicios  la información ingresada en el formulario "form_ingresar_SedesServicio", en la base de datos.

    public function ingresar_sedesservicios(Request $request){

      $sede_id=$request->sede_id;
      $existencia = SedesServicio::select('DaneSede')
      ->where('DaneSede', '=', $sede_id)
      ->get();
      if(count($existencia) == 0) {
        $data=new SedesServicio;
        $Tipoenergia=$request["tipoenergia"];
        $array = implode(', ', $Tipoenergia);
        $data->DaneSede= $request->input("sede_id");
        $nombres = temp_historico_sede::select('nombre_sede')
        ->where('codigo_sede', '=', $sede_id)
        ->get();
        foreach ($nombres as $nombre) {
          $nombre = $nombre->NombreSede;
        }
        $data->NombreSede = $nombre;

        $data->TipoEnergia=$array;
        $data->Acueducto=$request->input("acueducto");
        $data->AguaPotable=$request->input("aguapotable");
        $data->PlantaTratamiento=$request->input("plantatratamiento");
        $data->Alcantarillado=$request->input("alcantarillado");
        $data->PozoSeptico=$request->input("pozoseptico");


        if($data->save())
        {

          return view("mensajes.msj_solicitudRegistrada4");
        }


      else{

        return view("mensajes.mensaje_error")->with("msj","Hubo un error al agregar. Por favor, intentalo de nuevo;");

      }
      }else {
        return view("mensajes.mensaje_error_actualizacion3")->with("nombres",$existencia);
      }

    }

    public function RegistrarRespuesta(Request $request){
      $form = $request->input('tipoFormn');
      $SolicitudId = $request->input('idSolicitud');
      $mensaje = $request->input('RespuestaSolicitudd');
      $url = $request->input('urlActuall');
      if($mensaje == ""){

      }elseif($form == 1){
        $respuesta = new RespuestaSolicitudIT();
        $respuesta->SolicitudId = $SolicitudId;
        $respuesta->Mensaje = $mensaje;
        $respuesta->VistoRector = false;
        $respuesta->VistoSecretario = false;
        $now = new \DateTime();
        $now->format('d-m-Y');
        $respuesta->Fecha = $now;
        $respuesta->save();
      }else{
        $idRespuesta = $request->input('RespuestaId');
        $respuesta = RespuestaSolicitudIT::find($idRespuesta);
        if($respuesta != null){
          $respuesta->Mensaje = $mensaje;
          $respuesta->VistoRector = false;
          $respuesta->VistoSecretario = false;
          $now = new \DateTime();
          $now->format('d-m-Y');
          $respuesta->Fecha = $now;
          $respuesta->save();
        }
      }
      header("Location:$url");
      die();
      //return $this->verDetalleSolicitud($SolicitudId);

    }

    public function vistoRequerimientoo(Request $request){
      $idRespuesta = $request->input('RespuestaId');
      $respuesta = RespuestaSolicitudIT::find($idRespuesta);

      if(Auth::user()->isRole('secretarios')){
        $respuesta->VistoSecretario = true;
      }elseif (Auth::user()->isRole('rectores')) {
        $respuesta->VistoRector = true;
      }
      $respuesta->save();
    }


    public function descargarSolicitud(Request $request, $archivo)
    {
        $filename=public_path().'/solicitudes/'.$archivo;
        $finfo=finfo_open(FILEINFO_MIME_TYPE);
        $mimeType=finfo_file($finfo, $filename);

        header("Content-type:".$mimeType);
        header("Content-Disposition: attachment; filename=solicitud.pdf");
        echo file_get_contents($filename);
    }

}
