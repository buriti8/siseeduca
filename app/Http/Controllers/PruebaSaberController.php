<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\HistoricoPrueba;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Storage;

/**
* Class HomeController
* @package App\Http\Controllers
*/
class PruebaSaberController extends Controller
{
  /**
  * Create a new controller instance.
  *
  * @return void
  */
  public function __construct()
  {
    $this->middleware('auth');
  }

  public function antioquia()
  {
    return view('PruebaSaber.consulta_basica_saber_antioquia');
  }


  public function form_borrado_archivos_saber($periodo){
    return view("confirmaciones.form_borrado_archivos_saber")->with("periodo",$periodo);
  }


  public function borrar_archivos_saber(Request $request){

    $periodo = $request->input("periodos");

    $pruebas = HistoricoPrueba::where('periodo',$periodo)->delete();

    if($pruebas){
      DB::statement('refresh materialized view registro_archivos_cargados_pruebas;');
      return view("mensajes.msj_borrado");
    }
    else
    {
      return view("mensajes.mensaje_error")->with("msj","Hubo un error al agregar, intentarlo nuevamente.");
    }
  }

  public function validar_header_pruebas($archivo){

    $opened_file = fopen($archivo, 'r');
    $header = fgetcsv($opened_file, 0, ';');
    $header_convert_trim = array_map('trim', $header);

    $periodo =  $this->getColumnNameByValue($header,'PERIODO');
    $estu_tipodocumento =  $this->getColumnNameByValue($header,'ESTU_TIPODOCUMENTO');
    $estu_nacionalidad =  $this->getColumnNameByValue($header,'ESTU_NACIONALIDAD');
    $estu_genero =  $this->getColumnNameByValue($header,'ESTU_GENERO');
    $estu_fechanacimiento =  $this->getColumnNameByValue($header,'ESTU_FECHANACIMIENTO');
    $estu_consecutivo =  $this->getColumnNameByValue($header,'ESTU_CONSECUTIVO');
    $estu_estudiante =  $this->getColumnNameByValue($header,'ESTU_ESTUDIANTE');
    $estu_pais_reside =  $this->getColumnNameByValue($header,'ESTU_PAIS_RESIDE');
    $estu_tieneetnia =  $this->getColumnNameByValue($header,'ESTU_TIENEETNIA');
    $estu_etnia =  $this->getColumnNameByValue($header,'ESTU_ETNIA');
    $estu_limita_motriz =  $this->getColumnNameByValue($header,'ESTU_LIMITA_MOTRIZ');
    $estu_depto_reside =  $this->getColumnNameByValue($header,'ESTU_DEPTO_RESIDE');
    $estu_cod_reside_depto =  $this->getColumnNameByValue($header,'ESTU_COD_RESIDE_DEPTO');
    $estu_mcpio_reside =  $this->getColumnNameByValue($header,'ESTU_MCPIO_RESIDE');
    $estu_cod_reside_mcpio =  $this->getColumnNameByValue($header,'ESTU_COD_RESIDE_MCPIO');
    $fami_estratovivienda =  $this->getColumnNameByValue($header,'FAMI_ESTRATOVIVIENDA');
    $fami_personashogar =  $this->getColumnNameByValue($header,'FAMI_PERSONASHOGAR');
    $fami_cuartoshogar =  $this->getColumnNameByValue($header,'FAMI_CUARTOSHOGAR');
    $fami_educacionpadre =  $this->getColumnNameByValue($header,'FAMI_EDUCACIONPADRE');
    $fami_educacionmadre =  $this->getColumnNameByValue($header,'FAMI_EDUCACIONMADRE');
    $fami_trabajolaborpadre =  $this->getColumnNameByValue($header,'FAMI_TRABAJOLABORPADRE');
    $fami_trabajolabormadre =  $this->getColumnNameByValue($header,'FAMI_TRABAJOLABORMADRE');
    $fami_tieneinternet =  $this->getColumnNameByValue($header,'FAMI_TIENEINTERNET');
    $fami_tieneserviciotv =  $this->getColumnNameByValue($header,'FAMI_TIENESERVICIOTV');
    $fami_tienecomputador =  $this->getColumnNameByValue($header,'FAMI_TIENECOMPUTADOR');
    $fami_tienelavadora =  $this->getColumnNameByValue($header,'FAMI_TIENELAVADORA');
    $fami_tienehornomicroogas =  $this->getColumnNameByValue($header,'FAMI_TIENEHORNOMICROOGAS');
    $fami_tieneautomovil =  $this->getColumnNameByValue($header,'FAMI_TIENEAUTOMOVIL');
    $fami_tienemotocicleta =  $this->getColumnNameByValue($header,'FAMI_TIENEMOTOCICLETA');
    $fami_tieneconsolavideojuegos =  $this->getColumnNameByValue($header,'FAMI_TIENECONSOLAVIDEOJUEGOS');
    $fami_numlibros =  $this->getColumnNameByValue($header,'FAMI_NUMLIBROS');
    $fami_comelechederivados =  $this->getColumnNameByValue($header,'FAMI_COMELECHEDERIVADOS');
    $fami_comecarnepescadohuevo =  $this->getColumnNameByValue($header,'FAMI_COMECARNEPESCADOHUEVO');
    $fami_comecerealfrutoslegumbre =  $this->getColumnNameByValue($header,'FAMI_COMECEREALFRUTOSLEGUMBRE');
    $fami_situacioneconomica =  $this->getColumnNameByValue($header,'FAMI_SITUACIONECONOMICA');
    $estu_dedicacionlecturadiaria =  $this->getColumnNameByValue($header,'ESTU_DEDICACIONLECTURADIARIA');
    $estu_dedicacioninternet =  $this->getColumnNameByValue($header,'ESTU_DEDICACIONINTERNET');
    $estu_horassemanatrabaja =  $this->getColumnNameByValue($header,'ESTU_HORASSEMANATRABAJA');
    $estu_tiporemuneracion =  $this->getColumnNameByValue($header,'ESTU_TIPOREMUNERACION');
    $cole_codigo_icfes =  $this->getColumnNameByValue($header,'COLE_CODIGO_ICFES');
    $cole_cod_dane_establecimiento =  $this->getColumnNameByValue($header,'COLE_COD_DANE_ESTABLECIMIENTO');
    $cole_nombre_establecimiento =  $this->getColumnNameByValue($header,'COLE_NOMBRE_ESTABLECIMIENTO');
    $cole_genero =  $this->getColumnNameByValue($header,'COLE_GENERO');
    $cole_naturaleza =  $this->getColumnNameByValue($header,'COLE_NATURALEZA');
    $cole_calendario =  $this->getColumnNameByValue($header,'COLE_CALENDARIO');
    $cole_bilingue =  $this->getColumnNameByValue($header,'COLE_BILINGUE');
    $cole_caracter =  $this->getColumnNameByValue($header,'COLE_CARACTER');
    $cole_cod_dane_sede =  $this->getColumnNameByValue($header,'COLE_COD_DANE_SEDE');
    $cole_nombre_sede =  $this->getColumnNameByValue($header,'COLE_NOMBRE_SEDE');
    $cole_sede_principal =  $this->getColumnNameByValue($header,'COLE_SEDE_PRINCIPAL');
    $cole_area_ubicacion =  $this->getColumnNameByValue($header,'COLE_AREA_UBICACION');
    $cole_jornada =  $this->getColumnNameByValue($header,'COLE_JORNADA');
    $cole_cod_mcpio_ubicacion =  $this->getColumnNameByValue($header,'COLE_COD_MCPIO_UBICACION');
    $cole_mcpio_ubicacion =  $this->getColumnNameByValue($header,'COLE_MCPIO_UBICACION');
    $cole_cod_depto_ubicacion =  $this->getColumnNameByValue($header,'COLE_COD_DEPTO_UBICACION');
    $cole_depto_ubicacion =  $this->getColumnNameByValue($header,'COLE_DEPTO_UBICACION');
    $estu_privado_libertad =  $this->getColumnNameByValue($header,'ESTU_PRIVADO_LIBERTAD');
    $estu_cod_mcpio_presentacion =  $this->getColumnNameByValue($header,'ESTU_COD_MCPIO_PRESENTACION');
    $estu_mcpio_presentacion =  $this->getColumnNameByValue($header,'ESTU_MCPIO_PRESENTACION');
    $estu_depto_presentacion =  $this->getColumnNameByValue($header,'ESTU_DEPTO_PRESENTACION');
    $estu_cod_depto_presentacion =  $this->getColumnNameByValue($header,'ESTU_COD_DEPTO_PRESENTACION');
    $punt_lectura_critica =  $this->getColumnNameByValue($header,'PUNT_LECTURA_CRITICA');
    $percentil_lectura_critica =  $this->getColumnNameByValue($header,'PERCENTIL_LECTURA_CRITICA');
    $desemp_lectura_critica =  $this->getColumnNameByValue($header,'DESEMP_LECTURA_CRITICA');
    $punt_matematicas =  $this->getColumnNameByValue($header,'PUNT_MATEMATICAS');
    $percentil_matematicas =  $this->getColumnNameByValue($header,'PERCENTIL_MATEMATICAS');
    $desemp_matematicas =  $this->getColumnNameByValue($header,'DESEMP_MATEMATICAS');
    $punt_c_naturales =  $this->getColumnNameByValue($header,'PUNT_C_NATURALES');
    $percentil_c_naturales =  $this->getColumnNameByValue($header,'PERCENTIL_C_NATURALES');
    $desemp_c_naturales =  $this->getColumnNameByValue($header,'DESEMP_C_NATURALES');
    $punt_sociales_ciudadanas =  $this->getColumnNameByValue($header,'PUNT_SOCIALES_CIUDADANAS');
    $percentil_sociales_ciudadanas =  $this->getColumnNameByValue($header,'PERCENTIL_SOCIALES_CIUDADANAS');
    $desemp_sociales_ciudadanas =  $this->getColumnNameByValue($header,'DESEMP_SOCIALES_CIUDADANAS');
    $punt_ingles =  $this->getColumnNameByValue($header,'PUNT_INGLES');
    $percentil_ingles =  $this->getColumnNameByValue($header,'PERCENTIL_INGLES');
    $desemp_ingles =  $this->getColumnNameByValue($header,'DESEMP_INGLES');
    $punt_global =  $this->getColumnNameByValue($header,'PUNT_GLOBAL');
    $percentil_global =  $this->getColumnNameByValue($header,'PERCENTIL_GLOBAL');
    $estu_nse_establecimiento =  $this->getColumnNameByValue($header,'ESTU_NSE_ESTABLECIMIENTO');
    $estu_inse_individual =  $this->getColumnNameByValue($header,'ESTU_INSE_INDIVIDUAL');
    $estu_nse_individual =  $this->getColumnNameByValue($header,'ESTU_NSE_INDIVIDUAL');
    $estu_estadoinvestigacion =  $this->getColumnNameByValue($header,'ESTU_ESTADOINVESTIGACION');


    fclose($opened_file);

    $validation_array = [
      'periodo'=>$periodo,
      'estu_tipodocumento'=>$estu_tipodocumento,
      'estu_nacionalidad'=>$estu_nacionalidad,
      'estu_genero'=>$estu_genero,
      'estu_fechanacimiento'=>$estu_fechanacimiento,
      'estu_consecutivo'=>$estu_consecutivo,
      'estu_estudiante'=>$estu_estudiante,
      'estu_pais_reside'=>$estu_pais_reside,
      'estu_tieneetnia'=>$estu_tieneetnia,
      'estu_etnia'=>$estu_etnia,
      'estu_limita_motriz'=>$estu_limita_motriz,
      'estu_depto_reside'=>$estu_depto_reside,
      'estu_cod_reside_depto'=>$estu_cod_reside_depto,
      'estu_mcpio_reside'=>$estu_mcpio_reside,
      'estu_cod_reside_mcpio'=>$estu_cod_reside_mcpio,
      'fami_estratovivienda'=>$fami_estratovivienda,
      'fami_personashogar'=>$fami_personashogar,
      'fami_cuartoshogar'=>$fami_cuartoshogar,
      'fami_educacionpadre'=>$fami_educacionpadre,
      'fami_educacionmadre'=>$fami_educacionmadre,
      'fami_trabajolaborpadre'=>$fami_trabajolaborpadre,
      'fami_trabajolabormadre'=>$fami_trabajolabormadre,
      'fami_tieneinternet'=>$fami_tieneinternet,
      'fami_tieneserviciotv'=>$fami_tieneserviciotv,
      'fami_tienecomputador'=>$fami_tienecomputador,
      'fami_tienelavadora'=>$fami_tienelavadora,
      'fami_tienehornomicroogas'=>$fami_tienehornomicroogas,
      'fami_tieneautomovil'=>$fami_tieneautomovil,
      'fami_tienemotocicleta'=>$fami_tienemotocicleta,
      'fami_tieneconsolavideojuegos'=>$fami_tieneconsolavideojuegos,
      'fami_numlibros'=>$fami_numlibros,
      'fami_comelechederivados'=>$fami_comelechederivados,
      'fami_comecarnepescadohuevo'=>$fami_comecarnepescadohuevo,
      'fami_comecerealfrutoslegumbre'=>$fami_comecerealfrutoslegumbre,
      'fami_situacioneconomica'=>$fami_situacioneconomica,
      'estu_dedicacionlecturadiaria'=>$estu_dedicacionlecturadiaria,
      'estu_dedicacioninternet'=>$estu_dedicacioninternet,
      'estu_horassemanatrabaja'=>$estu_horassemanatrabaja,
      'estu_tiporemuneracion'=>$estu_tiporemuneracion,
      'cole_codigo_icfes'=>$cole_codigo_icfes,
      'cole_cod_dane_establecimiento'=>$cole_cod_dane_establecimiento,
      'cole_nombre_establecimiento'=>$cole_nombre_establecimiento,
      'cole_genero'=>$cole_genero,
      'cole_naturaleza'=>$cole_naturaleza,
      'cole_calendario'=>$cole_calendario,
      'cole_bilingue'=>$cole_bilingue,
      'cole_caracter'=>$cole_caracter,
      'cole_cod_dane_sede'=>$cole_cod_dane_sede,
      'cole_nombre_sede'=>$cole_nombre_sede,
      'cole_sede_principal'=>$cole_sede_principal,
      'cole_area_ubicacion'=>$cole_area_ubicacion,
      'cole_jornada'=>$cole_jornada,
      'cole_cod_mcpio_ubicacion'=>$cole_cod_mcpio_ubicacion,
      'cole_mcpio_ubicacion'=>$cole_mcpio_ubicacion,
      'cole_cod_depto_ubicacion'=>$cole_cod_depto_ubicacion,
      'cole_depto_ubicacion'=>$cole_depto_ubicacion,
      'estu_privado_libertad'=>$estu_privado_libertad,
      'estu_cod_mcpio_presentacion'=>$estu_cod_mcpio_presentacion,
      'estu_mcpio_presentacion'=>$estu_mcpio_presentacion,
      'estu_depto_presentacion'=>$estu_depto_presentacion,
      'estu_cod_depto_presentacion'=>$estu_cod_depto_presentacion,
      'punt_lectura_critica'=>$punt_lectura_critica,
      'percentil_lectura_critica'=>$percentil_lectura_critica,
      'desemp_lectura_critica'=>$desemp_lectura_critica,
      'punt_matematicas'=>$punt_matematicas,
      'percentil_matematicas'=>$percentil_matematicas,
      'desemp_matematicas'=>$desemp_matematicas,
      'punt_c_naturales'=>$punt_c_naturales,
      'percentil_c_naturales'=>$percentil_c_naturales,
      'desemp_c_naturales'=>$desemp_c_naturales,
      'punt_sociales_ciudadanas'=>$punt_sociales_ciudadanas,
      'percentil_sociales_ciudadanas'=>$percentil_sociales_ciudadanas,
      'desemp_sociales_ciudadanas'=>$desemp_sociales_ciudadanas,
      'punt_ingles'=>$punt_ingles,
      'percentil_ingles'=>$percentil_ingles,
      'desemp_ingles'=>$desemp_ingles,
      'punt_global'=>$punt_global,
      'percentil_global'=>$percentil_global,
      'estu_nse_establecimiento'=>$estu_nse_establecimiento,
      'estu_inse_individual'=>$estu_inse_individual,
      'estu_nse_individual'=>$estu_nse_individual,
      'estu_estadoinvestigacion'=>$estu_estadoinvestigacion,
    ];


    return $validation_array;

  }

  public function get_periodo($archivo){

    $rows = count(file($archivo));
    if($rows<=1){
      return 0;
    }
    ini_set('auto_detect_line_endings', true);

    $opened_file = fopen($archivo, 'r');
    $header = fgetcsv($opened_file, 0, ';');
    $data_row = fgetcsv($opened_file, 0, ';');
    $first_row = array_combine($header, $data_row);
    $periodoo = array_key_exists('PERIODO', $first_row)? $first_row['PERIODO'] : '';
    fclose($opened_file);

    return $periodoo;

  }

  public function getColumnNameByValue($array, $value)
  {
    return in_array($value, $array)? $value : '';
  }



  public function carga_masiva_pruebas($archivo, $periodo){


    DB::delete('delete from temp_historico_pruebas');
    DB::delete('delete from temp_archivo_pruebas');

    DB::statement("COPY temp_archivo_pruebas FROM '$archivo' DELIMITER ';' CSV HEADER;");

    DB::statement("insert into temp_historico_pruebas select PERIODO,ESTU_TIPODOCUMENTO,ESTU_NACIONALIDAD,ESTU_GENERO,ESTU_FECHANACIMIENTO,ESTU_CONSECUTIVO,ESTU_ESTUDIANTE,ESTU_PAIS_RESIDE,ESTU_TIENEETNIA,ESTU_ETNIA,ESTU_LIMITA_MOTRIZ,ESTU_DEPTO_RESIDE,
    ESTU_COD_RESIDE_DEPTO,ESTU_MCPIO_RESIDE,ESTU_COD_RESIDE_MCPIO,FAMI_ESTRATOVIVIENDA,FAMI_PERSONASHOGAR,FAMI_CUARTOSHOGAR,FAMI_EDUCACIONPADRE,
    FAMI_EDUCACIONMADRE,FAMI_TRABAJOLABORPADRE,FAMI_TRABAJOLABORMADRE,FAMI_TIENEINTERNET,FAMI_TIENESERVICIOTV,FAMI_TIENECOMPUTADOR,FAMI_TIENELAVADORA,
    FAMI_TIENEHORNOMICROOGAS,FAMI_TIENEAUTOMOVIL,FAMI_TIENEMOTOCICLETA,FAMI_TIENECONSOLAVIDEOJUEGOS,FAMI_NUMLIBROS,FAMI_COMELECHEDERIVADOS,
    FAMI_COMECARNEPESCADOHUEVO,FAMI_COMECEREALFRUTOSLEGUMBRE,FAMI_SITUACIONECONOMICA,ESTU_DEDICACIONLECTURADIARIA,ESTU_DEDICACIONINTERNET,
    ESTU_HORASSEMANATRABAJA,ESTU_TIPOREMUNERACION,COLE_CODIGO_ICFES,COLE_COD_DANE_ESTABLECIMIENTO,COLE_NOMBRE_ESTABLECIMIENTO,COLE_GENERO,
    COLE_NATURALEZA,COLE_CALENDARIO,COLE_BILINGUE,COLE_CARACTER,COLE_COD_DANE_SEDE,COLE_NOMBRE_SEDE,COLE_SEDE_PRINCIPAL,COLE_AREA_UBICACION,
    COLE_JORNADA,COLE_COD_MCPIO_UBICACION,COLE_MCPIO_UBICACION,COLE_COD_DEPTO_UBICACION,COLE_DEPTO_UBICACION,ESTU_PRIVADO_LIBERTAD,
    ESTU_COD_MCPIO_PRESENTACION,ESTU_MCPIO_PRESENTACION,ESTU_DEPTO_PRESENTACION,ESTU_COD_DEPTO_PRESENTACION,PUNT_LECTURA_CRITICA,
    PERCENTIL_LECTURA_CRITICA,DESEMP_LECTURA_CRITICA,PUNT_MATEMATICAS,PERCENTIL_MATEMATICAS,DESEMP_MATEMATICAS,PUNT_C_NATURALES,
    PERCENTIL_C_NATURALES,DESEMP_C_NATURALES,PUNT_SOCIALES_CIUDADANAS,PERCENTIL_SOCIALES_CIUDADANAS,DESEMP_SOCIALES_CIUDADANAS,PUNT_INGLES,
    PERCENTIL_INGLES,DESEMP_INGLES,PUNT_GLOBAL,PERCENTIL_GLOBAL,ESTU_NSE_ESTABLECIMIENTO,ESTU_INSE_INDIVIDUAL,ESTU_NSE_INDIVIDUAL,
    ESTU_ESTADOINVESTIGACION from temp_archivo_pruebas;");

    DB::statement("update temp_historico_pruebas set periodo='$periodo', created_at=now(), updated_at=now();");

    DB::statement('insert into historico_pruebas (PERIODO,ESTU_TIPODOCUMENTO,ESTU_NACIONALIDAD,ESTU_GENERO,ESTU_FECHANACIMIENTO,ESTU_CONSECUTIVO,ESTU_ESTUDIANTE,ESTU_PAIS_RESIDE,ESTU_TIENEETNIA,ESTU_ETNIA,ESTU_LIMITA_MOTRIZ,ESTU_DEPTO_RESIDE,
    ESTU_COD_RESIDE_DEPTO,ESTU_MCPIO_RESIDE,ESTU_COD_RESIDE_MCPIO,FAMI_ESTRATOVIVIENDA,FAMI_PERSONASHOGAR,FAMI_CUARTOSHOGAR,FAMI_EDUCACIONPADRE,
    FAMI_EDUCACIONMADRE,FAMI_TRABAJOLABORPADRE,FAMI_TRABAJOLABORMADRE,FAMI_TIENEINTERNET,FAMI_TIENESERVICIOTV,FAMI_TIENECOMPUTADOR,FAMI_TIENELAVADORA,
    FAMI_TIENEHORNOMICROOGAS,FAMI_TIENEAUTOMOVIL,FAMI_TIENEMOTOCICLETA,FAMI_TIENECONSOLAVIDEOJUEGOS,FAMI_NUMLIBROS,FAMI_COMELECHEDERIVADOS,
    FAMI_COMECARNEPESCADOHUEVO,FAMI_COMECEREALFRUTOSLEGUMBRE,FAMI_SITUACIONECONOMICA,ESTU_DEDICACIONLECTURADIARIA,ESTU_DEDICACIONINTERNET,
    ESTU_HORASSEMANATRABAJA,ESTU_TIPOREMUNERACION,COLE_CODIGO_ICFES,COLE_COD_DANE_ESTABLECIMIENTO,COLE_NOMBRE_ESTABLECIMIENTO,COLE_GENERO,
    COLE_NATURALEZA,COLE_CALENDARIO,COLE_BILINGUE,COLE_CARACTER,COLE_COD_DANE_SEDE,COLE_NOMBRE_SEDE,COLE_SEDE_PRINCIPAL,COLE_AREA_UBICACION,
    COLE_JORNADA,COLE_COD_MCPIO_UBICACION,COLE_MCPIO_UBICACION,COLE_COD_DEPTO_UBICACION,COLE_DEPTO_UBICACION,ESTU_PRIVADO_LIBERTAD,
    ESTU_COD_MCPIO_PRESENTACION,ESTU_MCPIO_PRESENTACION,ESTU_DEPTO_PRESENTACION,ESTU_COD_DEPTO_PRESENTACION,PUNT_LECTURA_CRITICA,
    PERCENTIL_LECTURA_CRITICA,DESEMP_LECTURA_CRITICA,PUNT_MATEMATICAS,PERCENTIL_MATEMATICAS,DESEMP_MATEMATICAS,PUNT_C_NATURALES,
    PERCENTIL_C_NATURALES,DESEMP_C_NATURALES,PUNT_SOCIALES_CIUDADANAS,PERCENTIL_SOCIALES_CIUDADANAS,DESEMP_SOCIALES_CIUDADANAS,PUNT_INGLES,
    PERCENTIL_INGLES,DESEMP_INGLES,PUNT_GLOBAL,PERCENTIL_GLOBAL,ESTU_NSE_ESTABLECIMIENTO,ESTU_INSE_INDIVIDUAL,ESTU_NSE_INDIVIDUAL,
    ESTU_ESTADOINVESTIGACION,created_at,updated_at) select * from temp_historico_pruebas;');

    DB::statement('refresh materialized view registro_archivos_cargados_pruebas;');

    return view("mensajes.mensaje_error")->with("msj","Hubo un error al agregar, intentarlo nuevamente.");

  }

  public function subir_archivos(Request $request)
  {


    $reglas=[  'archivo_pruebas' => 'required|mimes:csv,txt',


  ];

  $mensajes=[  'archivo_pruebas.mimes' => 'El archivo seleccionado en pruebas no corresponde al formato permitido (CSV,txt) ',

];


$validator0 = Validator::make( $request->all(),$reglas,$mensajes );
if( $validator0->fails() ){
  return view("mensajes.mensaje_error")->with("msj","Existen errores.")
  ->withErrors($validator0->errors());
}

$pruebas = $request->file('archivo_pruebas');
$nuevo_nombre_pruebas ="pruebas.csv";


Storage::disk('temp')->put($nuevo_nombre_pruebas,  \File::get($pruebas) );
$archivo_pruebas = Storage::disk('temp')->getDriver()->getAdapter()->applyPathPrefix('pruebas.csv');

$reglas_pruebas=[
  'periodo'=>'required',
  'estu_tipodocumento'=>'required',
  'estu_nacionalidad'=>'required',
  'estu_genero'=>'required',
  'estu_fechanacimiento'=>'required',
  'estu_consecutivo'=>'required',
  'estu_estudiante'=>'required',
  'estu_pais_reside'=>'required',
  'estu_tieneetnia'=>'required',
  'estu_etnia'=>'required',
  'estu_limita_motriz'=>'required',
  'estu_depto_reside'=>'required',
  'estu_cod_reside_depto'=>'required',
  'estu_mcpio_reside'=>'required',
  'estu_cod_reside_mcpio'=>'required',
  'fami_estratovivienda'=>'required',
  'fami_personashogar'=>'required',
  'fami_cuartoshogar'=>'required',
  'fami_educacionpadre'=>'required',
  'fami_educacionmadre'=>'required',
  'fami_trabajolaborpadre'=>'required',
  'fami_trabajolabormadre'=>'required',
  'fami_tieneinternet'=>'required',
  'fami_tieneserviciotv'=>'required',
  'fami_tienecomputador'=>'required',
  'fami_tienelavadora'=>'required',
  'fami_tienehornomicroogas'=>'required',
  'fami_tieneautomovil'=>'required',
  'fami_tienemotocicleta'=>'required',
  'fami_tieneconsolavideojuegos'=>'required',
  'fami_numlibros'=>'required',
  'fami_comelechederivados'=>'required',
  'fami_comecarnepescadohuevo'=>'required',
  'fami_comecerealfrutoslegumbre'=>'required',
  'fami_situacioneconomica'=>'required',
  'estu_dedicacionlecturadiaria'=>'required',
  'estu_dedicacioninternet'=>'required',
  'estu_horassemanatrabaja'=>'required',
  'estu_tiporemuneracion'=>'required',
  'cole_codigo_icfes'=>'required',
  'cole_cod_dane_establecimiento'=>'required',
  'cole_nombre_establecimiento'=>'required',
  'cole_genero'=>'required',
  'cole_naturaleza'=>'required',
  'cole_calendario'=>'required',
  'cole_bilingue'=>'required',
  'cole_caracter'=>'required',
  'cole_cod_dane_sede'=>'required',
  'cole_nombre_sede'=>'required',
  'cole_sede_principal'=>'required',
  'cole_area_ubicacion'=>'required',
  'cole_jornada'=>'required',
  'cole_cod_mcpio_ubicacion'=>'required',
  'cole_mcpio_ubicacion'=>'required',
  'cole_cod_depto_ubicacion'=>'required',
  'cole_depto_ubicacion'=>'required',
  'estu_privado_libertad'=>'required',
  'estu_cod_mcpio_presentacion'=>'required',
  'estu_mcpio_presentacion'=>'required',
  'estu_depto_presentacion'=>'required',
  'estu_cod_depto_presentacion'=>'required',
  'punt_lectura_critica'=>'required',
  'percentil_lectura_critica'=>'required',
  'desemp_lectura_critica'=>'required',
  'punt_matematicas'=>'required',
  'percentil_matematicas'=>'required',
  'desemp_matematicas'=>'required',
  'punt_c_naturales'=>'required',
  'percentil_c_naturales'=>'required',
  'desemp_c_naturales'=>'required',
  'punt_sociales_ciudadanas'=>'required',
  'percentil_sociales_ciudadanas'=>'required',
  'desemp_sociales_ciudadanas'=>'required',
  'punt_ingles'=>'required',
  'percentil_ingles'=>'required',
  'desemp_ingles'=>'required',
  'punt_global'=>'required',
  'percentil_global'=>'required',
  'estu_nse_establecimiento'=>'required',
  'estu_inse_individual'=>'required',
  'estu_nse_individual'=>'required',
  'estu_estadoinvestigacion'=>'required',
];

$mensajes1=[  'required'=>'La columna :attribute no se encuentra en el encabezado del archivo cargado o no está escrito correctamente ',
];

$validator1 = Validator::make( $this->validar_header_pruebas($archivo_pruebas),$reglas_pruebas,$mensajes1 );



if( $validator1->fails() ){

  return view("mensajes.mensaje_error")->with("msj","Existen errores en el archivo pruebas.")
  ->withErrors($validator1->errors());

}

$periodo= $this->get_periodo($archivo_pruebas);

$busqueda1= DB::table('registro_archivos_cargados_pruebas')->where('periodo', $periodo)->count();

if( $busqueda1>0 ){
  $this->limpiar_temp($archivo_pruebas);
  return view("mensajes.mensaje_error")->with("msj","Ya existen registros para el periodo: $periodo.");
}

$this->carga_masiva_pruebas($archivo_pruebas, $periodo);
$this->limpiar_temp($archivo_pruebas);
return view("mensajes.msj_anexo_cargado_saber");
}

public function limpiar_temp($nuevo_nombre_pruebas)
{
  Storage::disk('temp')->delete( $nuevo_nombre_pruebas);
}

public function index()
{

  $listado = DB::table('registro_archivos_cargados_pruebas')
  ->select('periodo','fecha', 'cantidad_pruebas')
  ->paginate(12);

  return view("PruebaSaber.cargar_datos_saber")->with("listados",$listado);
}

public function DescargarPlantillaSaber()
{
  $filename=public_path().'/plantilla_saber/saber.csv';
  $finfo=finfo_open(FILEINFO_MIME_TYPE);
  $mimeType=finfo_file($finfo, $filename);

  header("Content-type:".$mimeType);
  header("Content-Disposition: attachment; filename=saber.csv");
  echo file_get_contents($filename);
}

}
