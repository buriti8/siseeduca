<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Meses;
use App\HistoricoEstablecimientosNacional;
use App\HistoricoSedesNacional;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Storage;

class DueNacionalController extends Controller
{
  /**
  * Create a new controller instance.
  *
  * @return void
  */
  public function __construct()
  {
    $this->middleware('auth');
  }

  public function form_borrado_archivos_due_nacional($ano_info,$mes_corte){

    return view("confirmaciones.form_borrado_archivos_due_nacional")->with("ano_info",$ano_info)->with("mes_corte",$mes_corte);
  }

  public function borrar_archivos_due_nacional(Request $request){

    $anio_corte=$request->input("ano_info");
    $mes_corte=$request->input("mes_corte");

    $res_establecimiento_nacional=HistoricoEstablecimientosNacional::where('anio_corte',$anio_corte)->where('mes_corte',$mes_corte)->delete();
    $res_sede_nacional=HistoricoSedesNacional::where('anio_corte',$anio_corte)->where('mes_corte',$mes_corte)->delete();

    if($res_establecimiento_nacional && $res_sede_nacional){
      DB::statement('refresh materialized view registro_archivos_cargados_due_nacional;');
      return view("mensajes.msj_borrado");
    }
    else
    {
      return view("mensajes.mensaje_error")->with("msj","Hubo un error al agregar, intentarlo nuevamente.");
    }
  }

  public function validar_header_establecimientos_nacional($archivo){

    $opened_file = fopen($archivo, 'r');
    $header = fgetcsv($opened_file, 0, ';');
    $header_convert_trim = array_map('trim', $header);

    $secretaria  =  $this->getColumnNameByValue($header_convert_trim,'Secretaría');
    $codigo_departamento  =  $this->getColumnNameByValue($header_convert_trim,'Código Departamento');
    $departamento=$this->getColumnNameByValue($header_convert_trim,'Departamento');
    $codigo_municipio=$this->getColumnNameByValue($header_convert_trim,'Código Municipio');
    $municipio=$this->getColumnNameByValue($header_convert_trim,'Municipio');
    $codigo =$this->getColumnNameByValue($header_convert_trim,'Código');
    $nombre=$this->getColumnNameByValue($header_convert_trim,'Nombre');
    $direccion=$this->getColumnNameByValue($header_convert_trim,'Dirección');
    $telefono=$this->getColumnNameByValue($header_convert_trim,'Teléfono');
    $nombre_rector=$this->getColumnNameByValue($header_convert_trim,'Nombre Rector');
    $tipo_establecimiento  =$this->getColumnNameByValue($header_convert_trim,'Tipo Establecimiento');
    $etnias =$this->getColumnNameByValue($header_convert_trim,'Etnias');
    $sector =$this->getColumnNameByValue($header_convert_trim,'Sector');
    $genero =$this->getColumnNameByValue($header_convert_trim,'Genero');
    $zona =$this->getColumnNameByValue($header_convert_trim,'Zona');
    $niveles =$this->getColumnNameByValue($header_convert_trim,'Niveles');
    $jornadas =$this->getColumnNameByValue($header_convert_trim,'Jornadas');
    $caracter=$this->getColumnNameByValue($header_convert_trim,'Caracter');
    $especialidad=$this->getColumnNameByValue($header_convert_trim,'Especialidad');
    $licencia   =$this->getColumnNameByValue($header_convert_trim,'Licencia');
    $grados=$this->getColumnNameByValue($header_convert_trim,'Grados');
    $modelos_educativos=$this->getColumnNameByValue($header_convert_trim,'Modelos Educativos');
    $capacidades_excepcionales=$this->getColumnNameByValue($header_convert_trim,'Capacidades Excepcionales');
    $discapacidades=$this->getColumnNameByValue($header_convert_trim,'Discapacidades');
    $idiomas=$this->getColumnNameByValue($header_convert_trim,'Idiomas');
    $numero_de_sedes=$this->getColumnNameByValue($header_convert_trim,'Número de Sedes');
    $estado=$this->getColumnNameByValue($header_convert_trim,'Estado');
    $prestador_de_servicio=$this->getColumnNameByValue($header_convert_trim,'Prestador de servicio');
    $propiedad_de_la_planta_fisica  =$this->getColumnNameByValue($header_convert_trim,'Propiedad de la planta fisíca');
    $resguardo=$this->getColumnNameByValue($header_convert_trim,'Resguardo');
    $matricula_contratada =$this->getColumnNameByValue($header_convert_trim,'Matricula contratada');
    $calendario =$this->getColumnNameByValue($header_convert_trim,'Calendario');
    $internado =$this->getColumnNameByValue($header_convert_trim,'Internado');
    $estrato_socio_economico=$this->getColumnNameByValue($header_convert_trim,'Estrato socio-economico');
    $correo_electronico =$this->getColumnNameByValue($header_convert_trim,'Correo Electrónico');

    fclose($opened_file);

    $validation_array = [
      'Secretaría'=>$secretaria,
      'Código Departamento'=>$codigo_departamento,
      'Departamento'=>$departamento,
      'Código Municipio'=>$codigo_municipio,
      'Municipio'=>$municipio,
      'Código'=>$codigo,
      'Nombre'=>$nombre,
      'Dirección'=>$direccion,
      'Teléfono'=>$telefono,
      'Nombre Rector'=>$nombre_rector,
      'Tipo Establecimiento'=>$tipo_establecimiento,
      'Etnias'=>$etnias,
      'Sector'=>$sector,
      'Genero'=>$genero,
      'Zona'=>$zona,
      'Niveles'=>$niveles,
      'Jornadas'=>$jornadas,
      'Caracter'=>$caracter,
      'Especialidad'=>$especialidad,
      'Licencia'=>$licencia,
      'Grados'=>$grados,
      'Modelos Educativos'=>$modelos_educativos,
      'Capacidades Excepcionales'=>$capacidades_excepcionales,
      'Discapacidades'=>$discapacidades,
      'Idiomas'=>$idiomas,
      'Número de Sedes'=>$numero_de_sedes,
      'Estado'=>$estado,
      'Prestador de servicio'=>$prestador_de_servicio,
      'Propiedad de la planta fisíca'=>$propiedad_de_la_planta_fisica,
      'Resguardo'=>$resguardo,
      'Matricula contratada'=>$matricula_contratada,
      'Calendario'=>$calendario,
      'Internado'=>$internado,
      'Estrato socio-economico'=>$estrato_socio_economico,
      'Correo Electrónico'=>$correo_electronico,

    ];


    return $validation_array;

  }


  public function validar_header_sedes_nacional($archivo){

    $opened_file = fopen($archivo, 'r');
    $header = fgetcsv($opened_file, 0, ';');
    $header_convert_trim = array_map('trim', $header);

    $secretaria  =  $this->getColumnNameByValue($header_convert_trim,'Secretaria');
    $codigo_departamento  =  $this->getColumnNameByValue($header_convert_trim,'Código departamento');
    $nombre_departamento=$this->getColumnNameByValue($header_convert_trim,'Nombre departamento');
    $codigo_municipio=$this->getColumnNameByValue($header_convert_trim,'Código municipio');
    $nombre_municipio=$this->getColumnNameByValue($header_convert_trim,'Nombre municipio');
    $codigo_establecimiento =$this->getColumnNameByValue($header_convert_trim,'Código Establecimiento');
    $nombre_establecimiento=$this->getColumnNameByValue($header_convert_trim,'Nombre Establecimiento');
    $codigo_sede =$this->getColumnNameByValue($header_convert_trim,'Código Sede');
    $nombre_sede=$this->getColumnNameByValue($header_convert_trim,'Nombre Sede');
    $zona =$this->getColumnNameByValue($header_convert_trim,'Zona');
    $direccion=$this->getColumnNameByValue($header_convert_trim,'Dirección');
    $telefono=$this->getColumnNameByValue($header_convert_trim,'Teléfono');
    $estado_sede=$this->getColumnNameByValue($header_convert_trim,'Estado Sede');
    $niveles =$this->getColumnNameByValue($header_convert_trim,'Niveles');
    $modelos=$this->getColumnNameByValue($header_convert_trim,'Modelos');
    $grados=$this->getColumnNameByValue($header_convert_trim,'Grados');


    fclose($opened_file);

    $validation_array = [
      'Secretaria'=>$secretaria,
      'Código departamento'=>$codigo_departamento,
      'Nombre departamento'=>$nombre_departamento,
      'Código municipio'=>$codigo_municipio,
      'Nombre municipio'=>$nombre_municipio,
      'Código Establecimiento'=>$codigo_establecimiento,
      'Nombre Establecimiento'=>$nombre_establecimiento,
      'Código Sede'=>$codigo_sede,
      'Nombre Sede'=>$nombre_sede,
      'Zona'=>$zona,
      'Dirección'=>$direccion,
      'Teléfono'=>$telefono,
      'Estado Sede'=>$estado_sede,
      'Niveles'=>$niveles,
      'Modelos'=>$modelos,
      'Grados'=>$grados,
    ];


    return $validation_array;

  }

  public function getColumnNameByValue($array, $value)
  {
    return in_array($value, $array)? $value : '';
  }



  public function carga_masiva_establecimientos_nacional($archivo,$mes,$anio){


    DB::delete('delete from temp_historico_establecimientos_nacional');
    DB::delete('delete from temp_archivo_establecimientos_nacional');
    DB::statement("COPY temp_archivo_establecimientos_nacional FROM '$archivo' DELIMITER ';' CSV HEADER;");
    DB::statement("insert into temp_historico_establecimientos_nacional (secretaria,codigo_departamento,departamento,codigo_dane_municipio,municipio,
    codigo_establecimiento,nombre_establecimiento,direccion,telefono,nombre_rector,tipo_establecimiento,etnias,sector,genero,zona,niveles,
    jornadas,caracter,especialidad,licencia,grados,modelos_educativos,capacidades_excepcionales,discapacidades,idiomas,
    numero_de_sedes,estado,prestador_de_servicio,propiedad_de_la_planta_fisica,resguardo,matricula_contratada,calendario,
    internado,estrato_socio_economico,correo_electronico) select secretaria,codigo_departamento,departamento,codigo_dane_municipio,municipio,
    codigo_establecimiento,nombre_establecimiento,direccion,telefono,nombre_rector,tipo_establecimiento,etnias,sector,genero,zona,niveles,
    jornadas,caracter,especialidad,licencia,grados,modelos_educativos,capacidades_excepcionales,discapacidades,idiomas,
    numero_de_sedes,estado,prestador_de_servicio,propiedad_de_la_planta_fisica,resguardo,matricula_contratada,calendario,
    internado,estrato_socio_economico,correo_electronico  from temp_archivo_establecimientos_nacional;");


    DB::statement("update temp_historico_establecimientos_nacional set anio_corte ='$anio', mes_corte = '$mes'  , created_at=now(), updated_at=now();");

    DB::statement('insert into historico_establecimientos_nacional (mes_corte,anio_corte,secretaria,codigo_departamento,departamento,codigo_dane_municipio,municipio,
    codigo_establecimiento,nombre_establecimiento,direccion,telefono,nombre_rector,tipo_establecimiento,etnias,sector,genero,zona,niveles,
    jornadas,caracter,especialidad,licencia,grados,modelos_educativos,capacidades_excepcionales,discapacidades,idiomas,
    numero_de_sedes,estado,prestador_de_servicio,propiedad_de_la_planta_fisica,resguardo,matricula_contratada,calendario,
    internado,estrato_socio_economico,correo_electronico,created_at,updated_at) select * from temp_historico_establecimientos_nacional;');

    DB::statement('refresh materialized view registro_archivos_cargados_due_nacional;');


    return view("mensajes.mensaje_error")->with("msj","Hubo un error al agregar, intentarlo nuevamente.");



  }


  public function carga_masiva_sedes_nacional($archivo,$mes,$anio){


    DB::delete('delete from temp_archivo_sedes_nacional');
    DB::statement("COPY temp_archivo_sedes_nacional FROM '$archivo' DELIMITER ';' CSV HEADER;");
    DB::delete('delete from temp_historico_sedes_nacional');

    DB::statement("insert into temp_historico_sedes_nacional (secretaria,codigo_departamento,nombre_departamento,codigo_dane_municipio,
    nombre_municipio,codigo_establecimiento,nombre_establecimiento,codigo_sede,nombre_sede,zona,direccion,telefono,
    estado_sede,niveles,modelos,grados) select secretaria,codigo_departamento,nombre_departamento,codigo_dane_municipio,
    nombre_municipio,codigo_establecimiento,nombre_establecimiento,codigo_sede,nombre_sede,zona,direccion,telefono,
    estado_sede,niveles,modelos,grados from temp_archivo_sedes;");

    DB::statement("update temp_historico_sedes_nacional set anio_corte ='$anio', mes_corte = '$mes'  , created_at=now(), updated_at=now();");

    DB::statement('insert into historico_sedes_nacional (mes_corte,anio_corte,secretaria,codigo_departamento,nombre_departamento,codigo_dane_municipio,
    nombre_municipio,codigo_establecimiento,nombre_establecimiento,codigo_sede,nombre_sede,zona,direccion,telefono,
    estado_sede,niveles,modelos,grados,created_at,updated_at) select * from temp_historico_sedes_nacional;');

    DB::statement('refresh materialized view registro_archivos_cargados_due_nacional;');

    return view("mensajes.mensaje_error")->with("msj","Hubo un error al agregar, intentarlo nuevamente.");
  }

  public function subir_archivos(Request $request)
  {
    $reglas=[  'archivo_establecimientos_nacional' => 'required|mimes:csv,txt',
    'archivo_sedes_nacional' => 'required|mimes:csv,txt',
  ];

  $mensajes=[  'archivo_establecimientos_nacional.mimes' => 'El archivo seleccionado en establecimientos no corresponde al formato permitido (CSV,txt) ',
  'archivo_sedes_nacional.mimes' => 'El archivo seleccionado en sedes no corresponde al formato permitido (CSV,txt) ',
];


$validator0 = Validator::make( $request->all(),$reglas,$mensajes );
if( $validator0->fails() ){
  return view("mensajes.mensaje_error")->with("msj","Existen errores.")
  ->withErrors($validator0->errors());
}

$mes =$request->input("mes");
$anio =$request->input("anio");
$establecimientos = $request->file('archivo_establecimientos_nacional');
$sedes = $request->file('archivo_sedes_nacional');

$nuevo_nombre_establecimientos_nacional ="establecimientos_nacional.csv";
$nuevo_nombre_sedes_nacional ="sedes_nacional.csv";

Storage::disk('temp')->put($nuevo_nombre_establecimientos_nacional,  \File::get($establecimientos) );
$archivo_establecimientos_nacional = Storage::disk('temp')->getDriver()->getAdapter()->applyPathPrefix('establecimientos_nacional.csv');

Storage::disk('temp')->put($nuevo_nombre_sedes_nacional,  \File::get($sedes) );
$archivo_sedes_nacional = Storage::disk('temp')->getDriver()->getAdapter()->applyPathPrefix('sedes_nacional.csv');

$reglas_establecimientos_nacional=[
  'Secretaría'=>'required',
  'Código Departamento'=>'required',
  'Departamento'=>'required',
  'Código Municipio'=>'required',
  'Municipio'=>'required',
  'Código'=>'required',
  'Nombre'=>'required',
  'Dirección'=>'required',
  'Teléfono'=>'required',
  'Nombre Rector'=>'required',
  'Tipo Establecimiento'=>'required',
  'Etnias'=>'required',
  'Sector'=>'required',
  'Genero'=>'required',
  'Zona'=>'required',
  'Niveles'=>'required',
  'Jornadas'=>'required',
  'Caracter'=>'required',
  'Especialidad'=>'required',
  'Licencia'=>'required',
  'Grados'=>'required',
  'Modelos Educativos'=>'required',
  'Capacidades Excepcionales'=>'required',
  'Discapacidades'=>'required',
  'Idiomas'=>'required',
  'Número de Sedes'=>'required',
  'Estado'=>'required',
  'Prestador de servicio'=>'required',
  'Propiedad de la planta fisíca'=>'required',
  'Resguardo'=>'required',
  'Matricula contratada'=>'required',
  'Calendario'=>'required',
  'Internado'=>'required',
  'Estrato socio-economico'=>'required',
  'Correo Electrónico'=>'required',
];

$reglas_sedes_nacional=[
  'Secretaria'=>'required',
  'Código departamento'=>'required',
  'Nombre departamento'=>'required',
  'Código municipio'=>'required',
  'Nombre municipio'=>'required',
  'Código Establecimiento'=>'required',
  'Nombre Establecimiento'=>'required',
  'Código Sede'=>'required',
  'Nombre Sede'=>'required',
  'Zona'=>'required',
  'Dirección'=>'required',
  'Teléfono'=>'required',
  'Estado Sede'=>'required',
  'Niveles'=>'required',
  'Modelos'=>'required',
  'Grados'=>'required',
];

$mensajes1=[  'required'=>'La columna :attribute no se encuentra en el encabezado del archivo cargado o no está escrito correctamente ',
];

$validator1 = Validator::make( $this->validar_header_establecimientos_nacional($archivo_establecimientos_nacional),$reglas_establecimientos_nacional,$mensajes1 );
$validator2 = Validator::make( $this->validar_header_sedes_nacional($archivo_sedes_nacional),$reglas_sedes_nacional,$mensajes1 );

if( $validator1->fails() ){

  return view("mensajes.mensaje_error")->with("msj","Existen errores en el archivo establecimientos.")
  ->withErrors($validator1->errors());

}

if( $validator2->fails() ){

  return view("mensajes.mensaje_error")->with("msj","Existen errores en el archivo sedes.")
  ->withErrors($validator2->errors());

}


$busqueda1= DB::table('registro_archivos_cargados_due_nacional')->where('anio_corte', $anio)->where('mes_corte', $mes)->count();

if( $busqueda1>0  ){

  switch($mes) {
    case 1: $mes_letras= "Enero"; break;
    case 2: $mes_letras= "Febrero"; break;
    case 3: $mes_letras= "Marzo"; break;
    case 4: $mes_letras= "Abril"; break;
    case 5: $mes_letras= "Mayo"; break;
    case 6: $mes_letras= "Junio"; break;
    case 7: $mes_letras= "Julio"; break;
    case 8: $mes_letras= "Agosto"; break;
    case 9: $mes_letras= "Septiembre"; break;
    case 10: $mes_letras= "Octubre"; break;
    case 11: $mes_letras= "Noviembre"; break;
    case 12: $mes_letras= "Diciembre"; break;
    default:$mes_letras= "No match!"; break;
  }
  return view("mensajes.mensaje_error")->with("msj","Ya existen registros para el Mes: $mes_letras Año: $anio .");
}

$this->carga_masiva_establecimientos_nacional($archivo_establecimientos_nacional,$mes,$anio);
$this->carga_masiva_sedes_nacional($archivo_sedes_nacional,$mes,$anio);

Storage::disk('temp')->delete( $nuevo_nombre_establecimientos_nacional);
Storage::disk('temp')->delete( $nuevo_nombre_sedes_nacional);

return view("mensajes.msj_anexo_cargado");

}

public function DescargarPlantillaSedes()
{
  $filename=public_path().'/plantilla_sedes/sedes.csv';
  $finfo=finfo_open(FILEINFO_MIME_TYPE);
  $mimeType=finfo_file($finfo, $filename);

  header("Content-type:".$mimeType);
  header("Content-Disposition: attachment; filename=sedes.csv");
  echo file_get_contents($filename);
}

public function DescargarPlantillaEstablecimientos()
{
  $filename=public_path().'/plantilla_establecimientos/establecimientos.csv';
  $finfo=finfo_open(FILEINFO_MIME_TYPE);
  $mimeType=finfo_file($finfo, $filename);

  header("Content-type:".$mimeType);
  header("Content-Disposition: attachment; filename=establecimientos.csv");
  echo file_get_contents($filename);
}

/**
* Show the application dashboard.
*
* @return Response
*/
public function index()
{
  $meses =Meses::all();
  $listado = DB::table('registro_archivos_cargados_due_nacional')
  ->select('anio_corte', 'mes_corte','fecha', 'cantidad_establecimientos')
  ->paginate(12);

  return view("due.cargar_datos_due_nacional")->with("meses",$meses)->with("listados",$listado);
}
}
