<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use DB;
use App\Http\Controllers\Controller;
use App\User;
use PDO;

class GraficasController extends Controller
{
  function fetch(Request $request)
      {
        $select = $request->get('select');
        $value = $request->get('value');
        $dependent = $request->get('dependent');
        $valor = $request->get('valor');
        if ($valor=="todos") {
          $data = DB::table('municipios')
          ->orderBy("NombreMunicipio","ASC")
            ->get();
            $out = "TODOS";
        }else{
          $data = DB::table('municipios')
            ->where($select, $value)
            ->orderBy($dependent)
            ->get();
            $out = "TODOS";
        }

       $output = '<option value="0">'.$out.'</option>';
       foreach($data as $row)
       {
        $output .= '<option value="'.$row->CodigoDaneMunicipio.'">'.$row->$dependent.'</option>';
       }
       echo $output;
      }

      function _fetchesta(Request $request)
          {
            $select = $request->get('select');
            $location = $request->get('location');
            $value = $request->get('value');
            $dependent = $request->get('dependent');
            $valor = $request->get('valor');
            $anio = $request->get('anio_corte');
            $mes = $request->get('mes_corte');
            if ($valor=="todos") {
              $data = DB::table('historico_establecimientos')
              ->where(function($data) use ($location,$anio,$mes)  {
                 if($location=="due") {
                     $data->where("anio_corte",$anio);
                     $data->where("mes_corte",$mes);
                 }else{
                     $data->where("anio_corte",DB::raw("(select MAX(anio_corte) from historico_establecimientos)"));
                     $data->where("mes_corte",DB::raw("(select MAX(mes_corte) from historico_establecimientos)"));
                 }
              })
              ->orderBy("nombre_establecimiento","ASC")
              ->get();
                $out = "TODOS";
            }else{
              $data = DB::table('historico_establecimientos')
                ->where($select, $value)
                ->where(function($data) use ($location,$anio,$mes)  {
                   if($location=="due") {
                       $data->where("anio_corte",$anio);
                       $data->where("mes_corte",$mes);
                   }else{
                       $data->where("anio_corte",DB::raw("(select MAX(anio_corte) from historico_establecimientos)"));
                       $data->where("mes_corte",DB::raw("(select MAX(mes_corte) from historico_establecimientos)"));
                   }
                })
                ->orderBy($dependent,'ASC')
                ->get();
                $out = "TODOS";
            }
            if ($location=="due") {
              $output = '<div class="form-check"><a ><label class="">
                <input type="radio" value="0"  class="changesedes" onclick="changesedes(this);establedue(this);_cambiargraficadue();" data-dependent="nombre_sede"><span class="label-text">'.$out.'</span>
              </label></a></div>';
              foreach($data as $row)
              {
               $output .= '<div class="form-check"><a ><label class="">
                 <input type="radio" name="cbestable" class="cbestable"  value="'. $row->codigo_establecimiento .'" onclick="changesedes(this);establedue(this);_cambiargraficadue();activartablalistasedes(this);" data-dependent="nombre_sede"><span class="label-text">'.$row->$dependent.'</span>
               </label></a></div>';
              }

            }else{
              $output = '<div class="form-check"><a ><label class="">
                <input type="radio" value="0"  class="changesedes" onclick="changesedes(this);establematricula(this);_cambiargrafica();_cambiarzonasgrafica();" data-dependent="nombre_sede"><span class="label-text">'.$out.'</span>
              </label></a></div>';
              foreach($data as $row)
              {
               $output .= '<div class="form-check"><a ><label class="">
                 <input type="radio" name="cbestable" class="cbestable"  value="'. $row->codigo_establecimiento .'" onclick="changesedes(this);establematricula(this);_cambiargrafica();_cambiarzonasgrafica();" data-dependent="nombre_sede"><span class="label-text">'.$row->$dependent.'</span>
               </label></a></div>';
              }

            }
            echo $output;

          }
          function _fetchsedes(Request $request)
              {
                $filtro = $request->get('select');
                $location = $request->get('location');
                $value = $request->get('value');
                $dependent = $request->get('dependent');
                $valor = $request->get('valor');
                $anio = $request->get('anio_corte');
                $mes = $request->get('mes_corte');
                if ($valor=="todos") {
                  $data = DB::table('historico_sedes')
                  ->where(function($data) use ($location,$anio,$mes)  {
                     if($location=="due") {
                         $data->where("anio_corte",$anio);
                         $data->where("mes_corte",$mes);
                     }else{
                         $data->where("anio_corte",DB::raw("(select MAX(anio_corte) from historico_sedes)"));
                         $data->where("mes_corte",DB::raw("(select MAX(mes_corte) from historico_sedes)"));
                     }
                  })
                  ->orderBy("nombre_sede","ASC")
                  ->get();
                    $out = "TODOS";
                }else{
                  $data = DB::table('historico_sedes')
                    ->where($filtro, $value)
                    ->where(function($data) use ($location,$anio,$mes)  {
                       if($location=="due") {
                           $data->where("anio_corte",$anio);
                           $data->where("mes_corte",$mes);
                       }else{
                           $data->where("anio_corte",DB::raw("(select MAX(anio_corte) from historico_sedes)"));
                           $data->where("mes_corte",DB::raw("(select MAX(mes_corte) from historico_sedes)"));
                       }
                    })
                    ->orderBy($dependent,'ASC')
                    ->get();
                    $out = "TODOS";
                }
                if ($location=="due") {
                  $output = '<div><a ><label class="">
                    <input type="radio" value="0" disable name="radio" onclick="sedesdue(this);_cambiargraficadue()"><span class="label-text">'.$out.'</span>
                  </label></a></div>';
                  foreach($data as $row)
                  {
                   $output .= '<div><a ><label class="">
                     <input type="radio" name="radio" value="'.$row->codigo_sede.'" onclick="sedesdue(this);_cambiargraficadue()"><span class="label-text">'.$row->$dependent.'</span>
                   </label></a></div>';
                  }
                }
                else{
                  $output = '<div><a ><label class="">
                    <input type="radio" value="0" disable name="radio" onclick="sedesmatricula(this);_cambiargrafica();_cambiarzonasgrafica();"><span class="label-text">'.$out.'</span>
                  </label></a></div>';
                  foreach($data as $row)
                  {
                   $output .= '<div><a ><label class="">
                     <input type="radio" name="radio" value="'.$row->codigo_sede.'" onclick="sedesmatricula(this);_cambiargrafica();_cambiarzonasgrafica();"><span class="label-text">'.$row->$dependent.'</span>
                   </label></a></div>';
                  }
                }
               echo $output;
              }
              function _fetchmeses(Request $request)
                  {
                    $select = $request->get('select');
                    $value = $request->get('value');
                    $dependent = $request->get('dependent');
                    $data = DB::table('historico_establecimientos')
                        ->select(DB::raw('mes_corte as id,meses.mes as mes'))
                        ->join('meses', 'meses.id', '=', 'historico_establecimientos.mes_corte')
                        ->where($select, $value)
                        ->groupBy('mes_corte')
                        ->groupBy('meses.mes')
                        ->orderBy($dependent)
                        ->get();
                    $out = "Seleccionar";

                   $output = '<option value="0">'.$out.'</option>';
                   foreach($data as $row)
                   {
                    $output .= '<option value="'.$row->id.'">'.$row->mes.'</option>';
                   }
                   echo $output;
                  }

}
