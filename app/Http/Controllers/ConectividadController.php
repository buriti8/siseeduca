<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Meses;
use App\HistoricoConectividad;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Storage;


/**
 * Class HomeController
 * @package App\Http\Controllers
 */

class ConectividadController extends Controller
{
  /**
   * Create a new controller instance.
   *
   * @return void
   */
  public function __construct()
  {
      $this->middleware('auth');
  }
  public function form_borrado_archivos_conectividad($ano_info,$mes_corte){

        return view("confirmaciones.form_borrado_archivos_conectividad")->with("ano_info",$ano_info)->with("mes_corte",$mes_corte);

      }

      public function borrar_archivos_conectividad(Request $request){

            $anio_corte=$request->input("ano_info");
            $mes_corte=$request->input("mes_corte");

            $res_conectividad=HistoricoConectividad::where('anio_corte',$anio_corte)->where('mes_corte',$mes_corte)->delete();


            if($res_conectividad){
              DB::statement('refresh materialized view registro_archivos_cargados_conectividad;');


                 return view("mensajes.msj_borrado");
            }
            else
            {
                return view("mensajes.mensaje_error")->with("msj","..Hubo un error al borrar ; intentarlo nuevamente..");
            }


    }

    public function validar_header_conectividad($archivo){

      $opened_file = fopen($archivo, 'r');
      $header = fgetcsv($opened_file, 0, ';');
      $header_convert= $this->utf8_converter($header);
      $header_convert_trim = array_map('trim', $header_convert);

      $codigo_dane_sede  =  $this->getColumnNameByValue($header_convert_trim,'CODIGO DANE SEDE');
      $nombre_sede_educativa  =  $this->getColumnNameByValue($header_convert_trim,'NOMBRE SEDE EDUCATIVA');
      $direccion=$this->getColumnNameByValue($header_convert_trim,'DIRECCION');
      $telefono=$this->getColumnNameByValue($header_convert_trim,'TELEFONO');
      $zona=$this->getColumnNameByValue($header_convert_trim,'ZONA (URBANA/RURAL)');
      $departamento =$this->getColumnNameByValue($header_convert_trim,'DEPARTAMENTO');
      $municipio=$this->getColumnNameByValue($header_convert_trim,'MUNICIPIO');
      $programa_origen_de_los_recursos =$this->getColumnNameByValue($header_convert_trim,'PROGRAMA ORIGEN DE LOS RECURSOS');
      $numero_de_contrato=$this->getColumnNameByValue($header_convert_trim,'NUMERO DE CONTRATO');
      $operador =$this->getColumnNameByValue($header_convert_trim,'OPERADOR');
      $tecnologia_ultima_milla=$this->getColumnNameByValue($header_convert_trim,'Tecnología Última Milla');
      $ancho_de_banda=$this->getColumnNameByValue($header_convert_trim,'ANCHO DE BANDA');
      $meses_de_servicio=$this->getColumnNameByValue($header_convert_trim,'MESES DE SERVICIO');
      $fecha_inicio_servicio =$this->getColumnNameByValue($header_convert_trim,'FECHA INICIO SERVICIO');
      $fecha_fin_servicio=$this->getColumnNameByValue($header_convert_trim,'FECHA FIN SERVICIO');



      fclose($opened_file);

      $validation_array = [
        'CODIGO DANE SEDE'=>$codigo_dane_sede,
        'NOMBRE SEDE EDUCATIVA'=>  $nombre_sede_educativa,
        'DIRECCION'=>$direccion,
        'TELEFONO'=>$telefono,
        'ZONA (URBANA/RURAL)'=>$zona,
        'DEPARTAMENTO'=>$departamento,
        'MUNICIPIO'=>$municipio,
        'PROGRAMA ORIGEN DE LOS RECURSOS'=>$programa_origen_de_los_recursos,
        'NUMERO DE CONTRATO'=>$numero_de_contrato,
        'OPERADOR'=>$operador,
        'Tecnologia Ultima Milla'=>$tecnologia_ultima_milla,
        'ANCHO DE BANDA'=>$ancho_de_banda,
        'MESES DE SERVICIO'=>$meses_de_servicio,
        'FECHA INICIO DE SERVICIO'=>$fecha_inicio_servicio,
        'FECHA FIN SERVICIO'=>$fecha_fin_servicio,

        ];


      return $validation_array;

    }

    public  function utf8_converter($array)
      {
          array_walk_recursive($array, function(&$item, $key){
              if(!mb_detect_encoding($item, 'utf-8', true)){
                      $item = utf8_encode($item);
              }
          });

          return $array;
      }

      public function getColumnNameByValue($array, $value)
          {
              return in_array($value, $array)? $value : '';
          }

    public function carga_masiva_conectividad($archivo,$mes,$anio){


            DB::statement("COPY temp_archivo_conectividad FROM '$archivo' DELIMITER ';' CSV HEADER;");



                      DB::statement("insert into temp_historico_conectividad (codigo_dane_sede,nombre_sede_educativa,direccion,telefono,zona,departamento,municipio,programa_origen_de_los_recursos,
                      numero_de_contrato,operador,tecnologia_ultima_milla,ancho_de_banda,meses_de_servicio,fecha_inicio_servicio,fecha_fin_servicio) select codigo_dane_sede,nombre_sede_educativa,direccion,telefono,zona,departamento,municipio,programa_origen_de_los_recursos,
                      numero_de_contrato,operador,tecnologia_ultima_milla,ancho_de_banda,meses_de_servicio,fecha_inicio_servicio,fecha_fin_servicio from temp_archivo_conectividad");

                      DB::delete('delete from temp_archivo_conectividad');

                      DB::statement("update temp_historico_conectividad set anio_corte ='$anio', mes_corte = '$mes'  , created_at=now(), updated_at=now();");

                      DB::statement('insert into historico_conectividad (mes_corte,anio_corte,codigo_dane_sede,nombre_sede_educativa,direccion,telefono,zona,departamento,municipio,programa_origen_de_los_recursos,
                      numero_de_contrato,operador,tecnologia_ultima_milla,ancho_de_banda,meses_de_servicio,fecha_inicio_servicio,fecha_fin_servicio,created_at,updated_at) select * from temp_historico_conectividad;');

                    DB::delete('delete from temp_historico_conectividad');
                    DB::statement('refresh materialized view registro_archivos_cargados_conectividad;');


                       return view("mensajes.mensaje_error")->with("msj","..Hubo un error al agregar ; intentarlo nuevamente..");

  }

      public function subir_archivos(Request $request)
      {

        $reglas=[  'archivo_conectividad' => 'required|mimes:csv,txt',



              ];

        $mensajes=[  'archivo_conectividad.mimes' => 'El archivo seleccionado conectividad no corresponde al formato permitido (CSV,txt) ',


                 ];


        $validator0 = Validator::make( $request->all(),$reglas,$mensajes );
        if( $validator0->fails() ){
            return view("mensajes.mensaje_error")->with("msj","...Existen errores...")
                                                ->withErrors($validator0->errors());
        }

        $mes =$request->input("mes");
        $anio =$request->input("anio");
        $conectividad = $request->file('archivo_conectividad');


        $nuevo_nombre_conectividad ="conectividad.csv";


        Storage::disk('temp')->put($nuevo_nombre_conectividad,  \File::get($conectividad) );
        $archivo_conectividad = Storage::disk('temp')->getDriver()->getAdapter()->applyPathPrefix('conectividad.csv');

        $reglas_conectividad=[

          'CODIGO DANE SEDE'=>'required',
          'NOMBRE SEDE EDUCATIVA'=>  'required',
          'DIRECCION'=>'required',
          'TELEFONO'=>'required',
          'ZONA (URBANA/RURAL)'=>'required',
          'DEPARTAMENTO'=>'required',
          'MUNICIPIO'=>'required',
          'PROGRAMA ORIGEN DE LOS RECURSOS'=>'required',
          'NUMERO DE CONTRATO'=>'required',
          'OPERADOR'=>'required',
          'ANCHO DE BANDA'=>'required',
          'MESES DE SERVICIO'=>'required',
          'FECHA INICIO DE SERVICIO'=>'required',
          'FECHA FIN SERVICIO'=>'required',

         ];


       $mensajes1=[  'required'=>'La columna :attribute no se encuentra en el encabezado del archivo cargado o no está escrito correctamente ',
        ];

       $validator1 = Validator::make( $this->validar_header_conectividad($archivo_conectividad),$reglas_conectividad,$mensajes1 );


   if( $validator1->fails() ){

       return view("mensajes.mensaje_error")->with("msj","...Existen errores en el archivo...")
                                           ->withErrors($validator1->errors());

  }

  $busqueda1= DB::table('registro_archivos_cargados_conectividad')->where('anio_corte', $anio)->where('mes_corte', $mes)->count();


  if( $busqueda1>0  ){

    switch($mes) {
        case 1: $mes_letras= "Enero"; break;
        case 2: $mes_letras= "Febrero"; break;
        case 3: $mes_letras= "Marzo"; break;
        case 4: $mes_letras= "Abril"; break;
        case 5: $mes_letras= "Mayo"; break;
        case 6: $mes_letras= "Junio"; break;
        case 7: $mes_letras= "Julio"; break;
        case 8: $mes_letras= "Agosto"; break;
        case 9: $mes_letras= "Septiembre"; break;
        case 10: $mes_letras= "Octubre"; break;
        case 11: $mes_letras= "Noviembre"; break;
        case 12: $mes_letras= "Diciembre"; break;
        default:$mes_letras= "No match!"; break;
    }

      return view("mensajes.mensaje_error")->with("msj","Ya existen registros para el Mes: $mes_letras Año: $anio .");
  }



       $this->carga_masiva_conectividad($archivo_conectividad,$mes,$anio);
       Storage::disk('temp')->delete( $nuevo_nombre_conectividad);



        return view("mensajes.msj_anexo_cargado");

      }

      public function DescargarPlantillaConectividad()
      {
        $filename=public_path().'/plantilla_conectividad/conectividad.csv';
        $finfo=finfo_open(FILEINFO_MIME_TYPE);
        $mimeType=finfo_file($finfo, $filename);

        header("Content-type:".$mimeType);
        header("Content-Disposition: attachment; filename=conectividad.csv");
        echo file_get_contents($filename);
      }

      /**
       * Show the application dashboard.
       *
       * @return Response
       */
      public function index()
      {

          $meses =Meses::all();
          $listado = DB::table('registro_archivos_cargados_conectividad')
            ->select('anio_corte', 'mes_corte','fecha','cantidad_conectividad')
            ->paginate(12);

          return view("conectividad.cargar_datos_conectividad")->with("meses",$meses)->with("listados",$listado);
      }
}
