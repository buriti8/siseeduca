<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Meses;
use Illuminate\Support\Facades\DB;
use App\Matricula;
use Illuminate\Support\Facades\Validator;

/**
* Class HomeController
* @package App\Http\Controllers
*/


class CalidadConectividadController extends Controller
{
 /**
  * Create a new controller instance.
  *
  * @return void
  */
 public function __construct()
 {
     $this->middleware('auth');
 }

 public function carga_masiva_calidadconectividad($archivo,$mes,$anio){

       try {
         DB::statement("COPY temp_archivo_calidad_conectividad FROM '$archivo' DELIMITER ';' CSV HEADER;");



                   DB::statement("insert into temp_historico_calidad_conectividad (mes_operacion,elaborado_por,cargo,correo_institucional,
                   operador_prestador_servicio,numero_contrato,plazo_contrato,numero_sedes,inicio_contrato,fin_contrato,numero,codigo_dane_sede,sede_educativa,
                   tipo_canal,disponibilidad_servicios,descuento_servicios,latencia,descuento_latencia,efectividad_instalacion,descuento_efectividad,velocidad_transferencia,
                   descuento_velocidad ) select mes_operacion,elaborado_por,cargo,correo_institucional,operador_prestador_servicio,numero_contrato,plazo_contrato,numero_sedes,inicio_contrato,fin_contrato,numero,codigo_dane_sede,sede_educativa,
                   tipo_canal,disponibilidad_servicios,descuento_servicios,latencia,descuento_latencia,efectividad_instalacion,descuento_efectividad,velocidad_transferencia,
                   descuento_velocidad from temp_archivo_calidad_conectividad");

                   DB::delete('delete from temp_archivo_calidad_conectividad');

                   DB::statement("update temp_historico_calidad_conectividad set anio_corte ='$anio', mes_corte = '$mes'  , created_at=now(), updated_at=now();");

                   DB::statement('insert into historico_calidad_conectividad (mes_corte,anio_corte,mes_operacion,elaborado_por,cargo,correo_institucional,
                   operador_prestador_servicio,numero_contrato,plazo_contrato,numero_sedes,inicio_contrato,fin_contrato,numero,codigo_dane_sede,sede_educativa,
                   tipo_canal,disponibilidad_servicios,descuento_servicios,latencia,descuento_latencia,efectividad_instalacion,descuento_efectividad,velocidad_transferencia,
                   descuento_velocidad,created_at,updated_at) select * from temp_historico_calidad_conectividad;');

                 DB::delete('delete from temp_historico_calidad_conectividad');



                 } catch (Exception $e) {
                     report($e);

                    return view("mensajes.mensaje_error")->with("msj","..Hubo un error al agregar ; intentarlo nuevamente..");
                 }


}






 public function subir_archivos(Request $request)
 {

   $mes =$request->input("mes");
   $anio =$request->input("anio");
   $calidad = $request->file('archivo_calidad');



   $this->carga_masiva_calidadconectividad($calidad,$mes,$anio);





   return view("mensajes.msj_anexo_cargado");



 }




 /**
  * Show the application dashboard.
  *
  * @return Response
  */
 public function index()
 {

     $meses =Meses::all();

     return view("conectividad.cargar_datos_calidad")->with("meses",$meses);
 }
}
