<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Meses;
use Illuminate\Support\Facades\DB;
use App\Matricula;
use Illuminate\Support\Facades\Validator;
use Storage;


/**
 * Class HomeController
 * @package App\Http\Controllers
 */

class DotacionController extends Controller
{
  /**
   * Create a new controller instance.
   *
   * @return void
   */
  public function __construct()
  {
      $this->middleware('auth');
  }

  public function carga_masiva_dotacion($archivo,$mes,$anio){


          DB::statement("COPY temp_archivo_dotacion FROM '$archivo' DELIMITER ';' CSV HEADER;");



                    DB::statement("insert into temp_historico_dotacion (numero,elaborado_por,cargo,correo_institucional,numero_total_computo_sedes,fecha_reporte,codigo_dane_sede,
                    nombre_sede,direccion,departamento,municipio,equipo_escritorio_bueno,equipo_escritorio_malo,portatiles_buenos,portatiles_malos,tabletas_buenas,tabletas_malas,
                    total_equipos_buenos,total_equipos_malos,total_equipos ) select numero,elaborado_por,cargo,correo_institucional,numero_total_computo_sedes,fecha_reporte,codigo_dane_sede,
                    nombre_sede,direccion,departamento,municipio,equipo_escritorio_bueno,equipo_escritorio_malo,portatiles_buenos,portatiles_malos,tabletas_buenas,tabletas_malas,
                    total_equipos_buenos,total_equipos_malos,total_equipos from temp_archivo_dotacion");

                    DB::delete('delete from temp_archivo_dotacion');

                    DB::statement("update temp_historico_dotacion set anio_corte ='$anio', mes_corte = '$mes'  , created_at=now(), updated_at=now();");

                    DB::statement('insert into historico_dotacion (mes_corte,anio_corte,numero,elaborado_por,cargo,correo_institucional,numero_total_computo_sedes,fecha_reporte,codigo_dane_sede,
                    nombre_sede,direccion,departamento,municipio,equipo_escritorio_bueno,equipo_escritorio_malo,portatiles_buenos,portatiles_malos,tabletas_buenas,tabletas_malas,
                    total_equipos_buenos,total_equipos_malos,total_equipos,created_at,updated_at) select * from temp_historico_dotacion;');

                     DB::delete('delete from temp_historico_dotacion');




                     return view("mensajes.mensaje_error")->with("msj","..Hubo un error al agregar ; intentarlo nuevamente..");


}






  public function subir_archivos(Request $request)
  {

    $mes =$request->input("mes");
    $anio =$request->input("anio");
    $dotacion = $request->file('archivo_dotacion');

    $nuevo_nombre_dotacion ="dotacion.csv";

    Storage::disk('temp')->put($nuevo_nombre_dotacion,  \File::get($dotacion) );
   $archivo_dotacion = Storage::disk('temp')->getDriver()->getAdapter()->applyPathPrefix('dotacion.csv');


    $this->carga_masiva_dotacion($archivo_dotacion,$mes,$anio);

    Storage::disk('temp')->delete( $nuevo_nombre_dotacion);



    return view("mensajes.msj_dotacion_cargado")->with("msj","Carga exitosa") ;



  }


  /**
   * Show the application dashboard.
   *
   * @return Response
   */
  public function index()
  {

      $meses =Meses::all();

      return view("dotacion.cargar_datos_dotacion")->with("meses",$meses);
  }
}
