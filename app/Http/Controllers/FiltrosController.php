<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use DB;
use App\Http\Controllers\Controller;
use App\User;
use PDO;

use App\SiegaTrait\SedesTrait;

class FiltrosController extends Controller
{
    use SedesTrait;

    function municipios_subregion(Request $request)
    {
        $value = $request->get('value');
        $valor = $request->get('valor');

        if ($valor == "todos")
        {
             $data = DB::table('municipios')
                    ->orderBy('NombreMunicipio','ASC')
                    ->get();
                    $out = "SELECCIONE UN MUNICIPIO...";
        }
        else
        {
            $data = DB::table('municipios')
                ->where('CodigoSubregion', $value)
                ->orderBy('NombreMunicipio', 'ASC')
                ->get();
                 $out = "SELECCIONE UN MUNICIPIO...";
        }

        $output = '<option value="">'. $out .'</option>';
        foreach($data as $row)
        {
            $output .= '<option value="'.$row->CodigoDaneMunicipio.'">'.$row->NombreMunicipio.'</option>';
        }

        echo $output;
    }

    function establecimientos_municipio(Request $request)
    {
        //Por aquí pasa cuando es administrador.
        $value = $request->get('value');
        $valor = $request->get('valor');

        $data = $this->ambito_establecimientos($value, $valor);

        $out = 'SELECCIONE UN ESTABLECIMIENTO...';
        $output = '<option value="">'. $out .'</option>';
        foreach($data as $row)
        {
            $output .= '<option value="' . $row->codigo_establecimiento . '">' . $row->nombre_establecimiento . '</option>';
        }

        echo $output;
    }

    function sedes_establecimientos(Request $request)
    {
        //Por aquí pasa cuando es administrador.
        $value = $request->get('value');
        $dependent = $request->get('dependent');
        $valor = $request->get('valor'); //uno-todos

        $data = $this->ambito_sedes($value, $valor);

        $out = "SELECCIONE UNA SEDE...";

        $output = '<option value="">'. $out .'</option>';
        foreach($data as $row)
        {
            $output .= '<option value="' . $row->codigo_sede . '">' . $row->nombre_sede . '</option>';
        }

        echo $output;
    }
}
