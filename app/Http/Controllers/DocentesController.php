<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Meses;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

/**
 * Class HomeController
 * @package App\Http\Controllers
 */
class DocentesController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function carga_masiva_pp_docente($archivo,$mes,$anio)
    {
      try {
          DB::statement("COPY temp_archivo_pp_docente FROM '$archivo' DELIMITER ';' CSV HEADER;");
          }
          catch (Exception $e)
          {
            report($e);
            return view("mensajes.mensaje_error")->with("msj","..Hubo un error al agregar ; intentarlo nuevamente..");
          }
    }

    public function carga_masiva_eo_pcargos($archivo,$mes,$anio)
    {
      try {
          DB::statement("COPY temp_archivo_eo_pcargos FROM '$archivo' DELIMITER ';' CSV HEADER;");
          }
          catch (Exception $e)
          {
            report($e);
            return view("mensajes.mensaje_error")->with("msj","..Hubo un error al agregar ; intentarlo nuevamente..");
          }
    }


    public function carga_masiva_eo_dirdoc($archivo,$mes,$anio)
    {
      try {
          DB::statement("COPY temp_archivo_eo_dirdoc FROM '$archivo' DELIMITER ';' CSV HEADER;");
          }
          catch (Exception $e)
          {
            report($e);
            return view("mensajes.mensaje_error")->with("msj","..Hubo un error al agregar ; intentarlo nuevamente..");
          }
    }


    public function subir_archivos(Request $request)
    {

      $mes =$request->input("mes");
      $anio =$request->input("anio");
      $archivo_pp_docente = $request->file('archivo_pp_docente');
      $archivo_eo_pcargos = $request->file('archivo_eo_pcargos');
      $archivo_eo_dirdoc = $request->file('archivo_eo_dirdoc');

      $this->carga_masiva_pp_docente($archivo_pp_docente,$mes,$anio);
      $this->carga_masiva_eo_pcargos($archivo_eo_pcargos,$mes,$anio);
      $this->carga_masiva_eo_dirdoc($archivo_eo_dirdoc,$mes,$anio);

      return view("mensajes.msj_anexo_cargado")->with("msj","Carga exitosa") ;

    }



    /**
     * Show the application dashboard.
     *
     * @return Response
     */
    public function index()
    {

        $meses =Meses::all();

        return view("docentes.cargar_datos_docentes")->with("meses",$meses);
    }
}
