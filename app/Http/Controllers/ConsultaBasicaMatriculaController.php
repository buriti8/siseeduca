<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use DB;
use App\Http\Controllers\Controller;
use App\User;
use PDO;

class ConsultaBasicaMatriculaController extends Controller
{

   public function registro_matriculas($anio,$sector,$calendario,$subregion,$municipio,$estableselect,$sedesselect){
      if ($subregion == 0 && $municipio == 0) {
        $dataa = DB::table('vista_matriculas_todas')
                     ->select(DB::raw('vista_matriculas_todas.ano_inf as anio,meses.id AS MES,SUM(cantidad) as MAT'))
                     ->join('meses', 'meses.id', '=', 'vista_matriculas_todas.mes_corte')
                     ->whereIn('vista_matriculas_todas.ano_inf',explode(",", $anio))
                     ->groupBy('meses.id')
                     ->groupBy('vista_matriculas_todas.ano_inf')
                     ->orderBy('vista_matriculas_todas.ano_inf','DESC')
                     ->orderBy('meses.id','ASC')
                     ->get();
      }
        elseif ($subregion != 0 && $municipio != 0) {
          $dataa = DB::table('vista_matriculas_todas')
                         ->select(DB::raw('vista_matriculas_todas.ano_inf as anio,meses.id AS MES,SUM(cantidad) as MAT'))
                         ->join('meses', 'meses.id', '=', 'vista_matriculas_todas.mes_corte')
                         ->whereIn('vista_matriculas_todas.ano_inf',explode(",", $anio))
                         ->whereIn('vista_matriculas_todas.sector',explode(",", $sector))
                         ->where('vista_matriculas_todas.subregion',$subregion)
                         ->where('vista_matriculas_todas.CodigoDaneMunicipio', $municipio)
                         ->where(function($dataa) use ($estableselect)  {
                            if($estableselect!=0) {
                                $dataa->where('vista_matriculas_todas.codigo_establecimiento', $estableselect);
                            }
                         })
                         ->where(function($dataa) use ($sedesselect)  {
                            if($sedesselect!=0) {
                                $dataa->where('vista_matriculas_todas.codigo_sede', $sedesselect);
                            }
                         })
                         ->whereIn('vista_matriculas_todas.calendario',explode(",", $calendario))
                         ->groupBy('vista_matriculas_todas.ano_inf')
                         ->groupBy('meses.id')
                         ->orderBy('vista_matriculas_todas.ano_inf','DESC')
                         ->orderBy('meses.id','ASC')
                         ->get();

        }

        elseif ($subregion != 0) {
              $dataa = DB::table('vista_matriculas_todas')
                             ->select(DB::raw('vista_matriculas_todas.ano_inf as anio,meses.id AS MES,SUM(cantidad) as MAT'))
                             ->join('meses', 'meses.id', '=', 'vista_matriculas_todas.mes_corte')
                             ->whereIn('vista_matriculas_todas.ano_inf',explode(",", $anio))
                             ->where('vista_matriculas_todas.subregion',$subregion)
                             ->whereIn('vista_matriculas_todas.sector',explode(",", $sector))
                             ->whereIn('vista_matriculas_todas.calendario',explode(",", $calendario))
                             ->groupBy('vista_matriculas_todas.ano_inf')
                             ->groupBy('meses.id')
                             ->orderBy('vista_matriculas_todas.ano_inf','DESC')
                             ->orderBy('meses.id','ASC')
                             ->get();
        }

        else{
          $dataa = DB::table('meses as m')
             ->leftJoin('vista_matriculas_todas as vmt', function ($join) use ($anio) {
               $join->on('m.id', '=', 'vmt.mes_corte')
               ->whereIn('vmt.ano_inf','=',explode(",", $anio));
             })
             ->select(DB::raw('coalesce(vmt.ano_inf,vmt.ano_inf) as anio,m.id as MES,SUM(coalesce(vmt.cantidad,0)) as MAT'))
             ->groupBy('vmt.ano_inf')
             ->groupBy('m.id')
             ->orderBy('vmt.ano_inf','ASC')
             ->get();
        }



        for ($i=0; $i <=11; $i++) {
          $matriculas[$i]=0;
        }
      $ct=0;
        $ct=count($dataa);
        if ($ct==0) {
          $anios[]=0;
          $matriculas[]=0;
          $datos[]=0;
          $mes[]=0;
          $ct=1;
        }else{
          foreach ($dataa as $data)
          {
            $diasel = intval($data->mes); //Obtiene valor del mes
            $matriculas[$data->mes-1]=$data->mat; //Asigna el valor de la matricula a la posición del array que corresponde al mes
            $mes = array('anioTtile'=>$data->anio,'anioData'=>$matriculas);
            $datos[] = $mes;
          }

        }

        return   json_encode($datos);

   }


       function vistas_matriculas_zona($zona,$contra,$grado,$victi,$anio,$subregion,$municipio,$estableselect,$sedesselect)
       {
         if ($subregion== 0 && $municipio == 0) {
           $dataa = DB::table('vista_matriculas_zonas')
                                             ->select(DB::raw('vista_matriculas_zonas.ano_inf as anio,meses.id AS MES,SUM(cantidad) as MAT'))
                                             ->join('meses', 'meses.id', '=', 'vista_matriculas_zonas.mes_corte')
                                             ->whereIn('vista_matriculas_zonas.zon_alu',explode(",", $zona))
                                             ->whereIn('vista_matriculas_zonas.matricula_contratada',explode(",", $contra))
                                             ->whereIn('vista_matriculas_zonas.grado',explode(",", $grado))
                                             ->whereIn('vista_matriculas_zonas.pob_vict_conf',explode(",", $victi))
                                             ->whereIn('vista_matriculas_zonas.ano_inf',explode(",", $anio))
                                             ->groupBy('vista_matriculas_zonas.ano_inf')
                                             ->groupBy('meses.id')
                                             ->orderBy('vista_matriculas_zonas.ano_inf','DESC')
                                             ->orderBy('meses.id','ASC')
                                             ->get();
         }
         elseif($municipio == 0){
           $dataa = DB::table('vista_matriculas_zonas')
                                             ->select(DB::raw('vista_matriculas_zonas.ano_inf as anio,meses.id AS MES,SUM(cantidad) as MAT'))
                                             ->join('meses', 'meses.id', '=', 'vista_matriculas_zonas.mes_corte')
                                             ->whereIn('vista_matriculas_zonas.zon_alu',explode(",", $zona))
                                             ->whereIn('vista_matriculas_zonas.matricula_contratada',explode(",", $contra))
                                             ->whereIn('vista_matriculas_zonas.grado',explode(",", $grado))
                                             ->whereIn('vista_matriculas_zonas.pob_vict_conf',explode(",", $victi))
                                             ->whereIn('vista_matriculas_zonas.ano_inf',explode(",", $anio))
                                             ->where('vista_matriculas_zonas.subregion',$subregion)
                                             ->groupBy('vista_matriculas_zonas.ano_inf')
                                             ->groupBy('meses.id')
                                             ->orderBy('vista_matriculas_zonas.ano_inf','DESC')
                                             ->orderBy('meses.id','ASC')
                                             ->get();

         }
         else{
           $dataa = DB::table('vista_matriculas_zonas')
                                             ->select(DB::raw('vista_matriculas_zonas.ano_inf as anio,meses.id AS MES,SUM(cantidad) as MAT'))
                                             ->join('meses', 'meses.id', '=', 'vista_matriculas_zonas.mes_corte')
                                             ->whereIn('vista_matriculas_zonas.zon_alu',explode(",", $zona))
                                             ->whereIn('vista_matriculas_zonas.matricula_contratada',explode(",", $contra))
                                             ->whereIn('vista_matriculas_zonas.grado',explode(",", $grado))
                                             ->whereIn('vista_matriculas_zonas.pob_vict_conf',explode(",", $victi))
                                             ->whereIn('vista_matriculas_zonas.ano_inf',explode(",", $anio))
                                             ->where('vista_matriculas_zonas.subregion',$subregion)
                                             ->where('vista_matriculas_zonas.CodigoDaneMunicipio',$municipio)
                                             ->where(function($dataa) use ($estableselect)  {
                                                if($estableselect!=0) {
                                                    $dataa->where('vista_matriculas_zonas.codigo_establecimiento', $estableselect);
                                                }
                                             })
                                             ->where(function($dataa) use ($sedesselect)  {
                                                if($sedesselect!=0) {
                                                    $dataa->where('vista_matriculas_zonas.codigo_sede', $sedesselect);
                                                }
                                             })
                                             ->groupBy('vista_matriculas_zonas.ano_inf')
                                             ->groupBy('meses.id')
                                             ->orderBy('vista_matriculas_zonas.ano_inf','DESC')
                                             ->orderBy('meses.id','ASC')
                                             ->get();

         }
         for ($i=0; $i <=11; $i++) {
           $matriculas[$i]=0;
         }
       $ct=0;
         $ct=count($dataa);
         if ($ct==0) {
           $anios[]=0;
           $matriculas[]=0;
           $datos[]=0;
           $mes[]=0;
           $ct=1;
         }else{
           foreach ($dataa as $data)
           {
             $diasel = intval($data->mes); //Obtiene valor del mes
             $matriculas[$data->mes-1]=$data->mat; //Asigna el valor de la matricula a la posición del array que corresponde al mes
             $mes = array('anioTtile'=>$data->anio,'anioData'=>$matriculas);
             $datos[] = $mes;
           }

         }

         return   json_encode($datos);
       }


   public function index()
   {
       $anios = DB::table('vista_matriculas_todas')
                      ->select('ano_inf as anio')
                      ->groupBy('ano_inf')
                      ->orderBy('ano_inf','DESC')
                      ->get();
      $subregions = DB::table('subregions')
                    ->orderBy('id', 'ASC')
                    ->get();
      $grados = \DB::table('vista_matriculas_zonas')
                       ->select('grados.Grado','grados.IdGrado as grado')
                       ->join('grados', 'grados.id', '=', 'vista_matriculas_zonas.grado')
                       ->groupBy('grados.IdGrado')
                       ->groupBy('grados.Grado')
                       ->groupBy('vista_matriculas_zonas.grado')
                       ->orderBy('vista_matriculas_zonas.grado','ASC')
                       ->get();
       $anio=date("Y");
       $mes=date("m");
       $region="";
       return view("matricula.consulta_basica")
              ->with("anio",$anio)
              ->with("mes",$mes)
              ->with("region",$region)->with("subregions",$subregions)->with("anios",$anios)->with("grados",$grados);
   }



}
