<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\PermissionRole;
use App\RoleUser;
use App\ControlUsuarios;
use Illuminate\Support\Facades\Validator;
use Caffeinated\Shinobi\Models\Role;
use Caffeinated\Shinobi\Models\Permission;

class UsuariosController extends Controller
{


public function form_nuevo_usuario(){
    //carga el formulario para agregar un nuevo usuario
    $roles=Role::all();
    return view("formularios.form_nuevo_usuario")->with("roles",$roles);

}


public function form_nuevo_rol(){
    //carga el formulario para agregar un nuevo rol
    $roles=Role::all();
    return view("formularios.form_nuevo_rol")->with("roles",$roles);
}


public function form_nuevo_permiso(){
    //carga el formulario para agregar un nuevo permiso
     $roles=Role::all();
     $permisos=Permission::all();
    return view("formularios.form_nuevo_permiso")->with("roles",$roles)->with("permisos", $permisos);
}



public function listado_usuarios(){


    //presenta un listado de usuarios paginados de 100 en 100
	$usuarios=User::paginate(100);
	return view("listados.listado_usuarios")->with("usuarios",$usuarios);
}





public function crear_usuario(Request $request){
  //valida que el usuario es administrador

    //crea un nuevo usuario en el sistema

    $reglas=[  'password' => 'required|min:8',
    'email' => 'required|unique:users', ];

    $mensajes=[  'password.min' => 'El password debe tener al menos 8 caracteres',
        'email.unique' => 'El usuario ya se encuentra registrado en la base de datos',
    ];

	$validator = Validator::make( $request->all(),$reglas,$mensajes );
	if( $validator->fails() ){
	  	return view("mensajes.mensaje_error")->with("msj","...Existen errores...")
	  	                                    ->withErrors($validator->errors());
	}

	$usuario=new User;
  $control = ControlUsuarios::find(1);
  $nextId = $control->User+1;
  $usuario->id = $nextId;
	// $usuario->name=strtoupper( $request->input("nombres")." ".$request->input("apellidos") ) ;
	$usuario->name=ucwords(strtolower( $request->input("nombres") ) ) ;
    $usuario->lastname=ucwords(strtolower( $request->input("apellidos") ) ) ;
	$usuario->phone=$request->input("telefono");
	$usuario->email=$request->input("email");
	$usuario->password= bcrypt( $request->input("password") );


    if($usuario->save())
    {
      $control->User = $nextId;
      $control->save();

      return view("mensajes.msj_usuario_creado")->with("msj","Usuario agregado correctamente") ;
    }
    else
    {
        return view("mensajes.mensaje_error")->with("msj","...Hubo un error al agregar ;...") ;
    }

}



public function crear_rol(Request $request){



   $rol=new Role;
   $control = ControlUsuarios::find(1);
   $nextId = $control->Role+1;
   $rol->id = $nextId;
   $rol->name=$request->input("rol_nombre") ;
   $rol->slug=$request->input("rol_slug") ;
   $rol->description=$request->input("rol_descripcion") ;
    if($rol->save())
    {
      $control->Role = $nextId;
      $control->save();
        return view("mensajes.msj_rol_creado")->with("msj","Rol agregado correctamente") ;
    }
    else
    {
        return view("mensajes.mensaje_error")->with("msj","...Hubo un error al agregar ;...") ;
    }
}




public function crear_permiso(Request $request){


   $permiso=new Permission;
   $control = ControlUsuarios::find(1);
   $nextId = $control->Permission+1;
   $permiso->id = $nextId;
   $permiso->name=$request->input("permiso_nombre") ;
   $permiso->slug=$request->input("permiso_slug") ;
   $permiso->description=$request->input("permiso_descripcion") ;
    if($permiso->save())
    {
      $control->Permission = $nextId;
      $control->save();
        return view("mensajes.msj_permiso_creado")->with("msj","Permiso creado correctamente") ;
    }
    else
    {
        return view("mensajes.mensaje_error")->with("msj","...Hubo un error al agregar ;...") ;
    }


}

public function asignar_permiso(Request $request){



     $roleid=$request->input("rol_sel");
     $idper=$request->input("permiso_rol");
     // return view("mensajes.mensaje_error")->with("msj","$roleid permiso $idper");
     $control = ControlUsuarios::find(1);
     $nextId = $control->permission_role+1;

     $permission_role = new PermissionRole();

     $permission_role->id = $nextId;
     $permission_role->permission_id = $idper;
     $permission_role->role_id = $roleid;


    //  $rol=Role::find($roleid);
    //  $rol->assignPermission($idper);
    //
    if($permission_role->save())
    {
      $control->permission_role = $nextId;
      $control->save();
        return view("mensajes.msj_permiso_creado")->with("msj","Permiso asignado correctamente") ;
    }
    else
    {
        return view("mensajes.mensaje_error")->with("msj","...Hubo un error al agregar ;...") ;
    }



}



public function form_editar_usuario($id){

    $usuario=User::find($id);
    $roles=Role::all();
    return view("formularios.form_editar_usuario")->with("usuario",$usuario)
	                                              ->with("roles",$roles);
}

public function editar_usuario(Request $request){


    $idusuario=$request->input("id_usuario");
    $usuario=User::find($idusuario);
    $usuario->name=ucwords(strtolower( $request->input("nombres") ) );
    $usuario->lastname=ucwords(strtolower( $request->input("apellidos") ) );
    $usuario->phone=$request->input("telefono");

     if($request->has("rol")){
	    $rol=$request->input("rol");
	    $usuario->revokeAllRoles();
	    $usuario->assignRole($rol);
     }

    if( $usuario->save()){
		return view("mensajes.msj_usuario_actualizado")->with("msj","Usuario actualizado correctamente")
	                                                   ->with("idusuario",$idusuario) ;
    }
    else
    {
		return view("mensajes.mensaje_error")->with("msj","..Hubo un error al agregar ; intentarlo nuevamente..");
    }
}


public function form_editar_usuario2($id){

    $usuario=User::find($id);
    $roles=Role::all();
    return view("formularios.form_editar_usuario2")->with("usuario",$usuario)
	                                              ->with("roles",$roles);
}

public function editar_usuario2(Request $request){


    $idusuario=$request->input("id_usuario");
    $usuario=User::find($idusuario);
    $usuario->name=ucwords(strtolower( $request->input("nombres") ) );
    $usuario->lastname=ucwords(strtolower( $request->input("apellidos") ) );
    $usuario->phone=$request->input("telefono");

     if($request->has("rol")){
	    $rol=$request->input("rol");
	    $usuario->revokeAllRoles();
	    $usuario->assignRole($rol);
     }

    if( $usuario->save()){
		return view("mensajes.msj_usuario_actualizado")->with("msj","Usuario actualizado correctamente")
	                                                   ->with("idusuario",$idusuario) ;
    }
    else
    {
		return view("mensajes.mensaje_error")->with("msj","..Hubo un error al agregar ; intentarlo nuevamente..");
    }
}



public function buscar_usuario(Request $request){

	$dato=$request->input("dato_buscado");
	$usuarios=User::where("name","like","%".$dato."%")->orwhere("lastname","like","%".$dato."%")                                              ->paginate(100);
	return view('listados.listado_usuarios')->with("usuarios",$usuarios);
      }




public function borrar_usuario(Request $request){



        $idusuario=$request->input("id_usuario");
        $usuario=User::find($idusuario);

        if($usuario->delete()){
             return view("mensajes.msj_borrado") ;
        }
        else
        {
            return view("mensajes.mensaje_error")->with("msj","..Hubo un error al agregar ; intentarlo nuevamente..");
        }


}


public function editar_acceso(Request $request){

         $idusuario=$request->input("id_usuario");
         $usuario=User::find($idusuario);
         $usuario->email=$request->input("email");
         $usuario->password= bcrypt( $request->input("password") );
          if( $usuario->save()){
          return view("mensajes.msj_password_actualizada")->with("idusuario",$idusuario) ;
         }
          else
          {
        return view("mensajes.mensaje_error")->with("msj","...Hubo un error al agregar ; intentarlo nuevamente ...") ;
          }
}



public function asignar_rol($idusu,$idrol){


        //$usuario=User::find($idusu);
        //$usuario->assignRole($idrol);
        $control = ControlUsuarios::find(1);
        $nextId = $control->role_user+1;

        $role_user = new RoleUser();
        $role_user->id = $nextId;
        $role_user->role_id = $idrol;
        $role_user->user_id = $idusu;

        if($role_user->save()){
          $control->role_user = $nextId;
          $control->save();
        }


        $usuario=User::find($idusu);
        $rolesasignados=$usuario->getRoles();

        return json_encode ($rolesasignados);


}


public function quitar_rol($idusu,$idrol){


    $usuario=User::find($idusu);
    $usuario->revokeRole($idrol);
    $rolesasignados=$usuario->getRoles();
    return json_encode ($rolesasignados);


}


public function form_borrado_usuario($id){
  $usuario=User::find($id);
  return view("confirmaciones.form_borrado_usuario")->with("usuario",$usuario);

}




public function quitar_permiso($idrole,$idper){


    $role = Role::find($idrole);
    $role->revokePermission($idper);
    $role->save();

    return "ok";
}


public function borrar_rol($idrole){


    $role = Role::find($idrole);
    $role->delete();
    return "ok";
}








}
