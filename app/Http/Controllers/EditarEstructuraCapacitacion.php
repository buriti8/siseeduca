<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\NombreCapacitacion;

use Datatables;



class EditarEstructuraCapacitacion extends Controller
{
  public function form_nuevo_capacitacion(){

      return view("formularios.form_nuevo_capacitacion");

  }


  public function crear_capacitacion(Request $request){


    $capacitacion=new NombreCapacitacion;
    $capacitacion->nombre=$request->input("nombre");


      if($capacitacion->save())
      {


        return view("mensajes.msj_capacitacion_creado")->with("msj","Capacitación agregada correctamente.") ;
      }
      else
      {
          return view("mensajes.mensaje_error")->with("msj","...Hubo un error al agregar ;...") ;
      }

  }


  public function form_editar_capacitacion($id){

      $capacitacion=NombreCapacitacion::find($id);

      return view("formularios.form_editar_capacitacion")->with("capacitacion",$capacitacion);
  }


  public function editar_capacitacion(Request $request){

      $idcapacitacion=$request->input("id_capacidad");
      $capacitacion=NombreCapacitacion::find($idcapacitacion);
      $capacitacion->nombre=$request->input("nombre");



      if( $capacitacion->save()){
      return view("mensajes.msj_usuario_actualizado")->with("msj","Capacitación actualizada correctamente")
                                                       ->with("idusuario",$idcapacitacion) ;
      }
      else
      {
      return view("mensajes.mensaje_error")->with("msj","..Hubo un error al agregar ; intentarlo nuevamente..");
      }
  }


  public function form_borrado_capacidad($id){
    $capacidad=NombreCapacitacion::find($id);
    return view("confirmaciones.form_borrado_capacidad")->with("capacidad",$capacidad);

  }

  public function borrar_capacidad(Request $request){

          $idcapacidad=$request->input("id_capacidad");
          $capacidad=NombreCapacitacion::find($idcapacidad);

          if($capacidad->delete()){
               return view("mensajes.msj_capacidad_borrado")->with("msj","Capacidad borrado correctamente.") ;
          }
          else
          {
              return view("mensajes.mensaje_error")->with("msj","..Hubo un error al agregar ; intentarlo nuevamente..");
          }


  }

  public function listado_estructura(){
      //carga el formulario para agregar un nuevo usuario

      $capacitacions=NombreCapacitacion::paginate(10);



      return view("capacitacion.editar_estructura")
      ->with("capacitacions",$capacitacions);

  }

  /**
   * Show the application dashboard.
   *
   * @return Response
   */
  public function index()
  {

  }

}
