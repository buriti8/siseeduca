<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Traslado;
use App\Dispositivo;
use App\Subregion;
use App\HistoricoSede;
use Storage;
use App\SiegaTrait\SedesTrait;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Datatables;

use Illuminate\Support\Facades\Auth;

class TrasladoController extends Controller
{
    //Traslado de dispositivos.

    use SedesTrait;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {

        return view('inventario.listado_traslados_dispositivos');
    }


public function listado_traslados(){

          if(Auth::user()->isRole('secretarios'))
          {
              $datos = DB::table('traslados')
                  ->leftJoin('temp_historico_sedes', 'traslados.sede_origen_id', '=', 'temp_historico_sedes.codigo_sede')
                  ->join('dispositivos', 'traslados.dispositivo_id', '=', 'dispositivos.id')
                  ->join('tipo_dispositivos', 'dispositivos.tipo_id', '=', 'tipo_dispositivos.id')
                  ->select('traslados.id', 'traslados.sede_origen_id', 'temp_historico_sedes.nombre_sede', 'traslados.sede_destino_id', DB::raw('(select nombre_sede from temp_historico_sedes where codigo_sede = traslados.sede_destino_id) as destino') , 'tipo_dispositivos.nombre', 'dispositivos.serial', 'traslados.motivo', 'traslados.created_at')
                  ->where('temp_historico_sedes.codigo_dane_municipio', Auth::user()->name)
                  ->get();
          }
          elseif (Auth::user()->isRole('rectores'))
          {
              $datos = DB::table('traslados')
                  ->leftJoin('temp_historico_sedes', 'traslados.sede_origen_id', '=', 'temp_historico_sedes.codigo_sede')
                  ->join('dispositivos', 'traslados.dispositivo_id', '=', 'dispositivos.id')
                  ->join('tipo_dispositivos', 'dispositivos.tipo_id', '=', 'tipo_dispositivos.id')
                  ->select('traslados.id', 'traslados.sede_origen_id', 'temp_historico_sedes.nombre_sede', 'traslados.sede_destino_id', DB::raw('(select nombre_sede from temp_historico_sedes where codigo_sede = traslados.sede_destino_id) as destino'), 'tipo_dispositivos.nombre', 'dispositivos.serial', 'traslados.motivo', 'traslados.created_at')
                  ->where('temp_historico_sedes.codigo_establecimiento', Auth::user()->name)
                  ->get();
          }
          else
          {
               $datos = DB::table('traslados')
                  ->leftJoin('temp_historico_sedes', 'traslados.sede_origen_id', '=', 'temp_historico_sedes.codigo_sede')
                  // ->leftJoin('temp_historico_sedes', 'traslados.sede_destino_id', '=', 'temp_historico_sedes.codigo_sede')
                  ->join('dispositivos', 'traslados.dispositivo_id', '=', 'dispositivos.id')
                  ->join('tipo_dispositivos', 'dispositivos.tipo_id', '=', 'tipo_dispositivos.id')
                  ->select('traslados.id', 'traslados.sede_origen_id', 'temp_historico_sedes.nombre_sede','traslados.sede_destino_id',  DB::raw('(select nombre_sede from temp_historico_sedes where codigo_sede = traslados.sede_destino_id) as destino') ,'tipo_dispositivos.nombre', 'dispositivos.serial', 'traslados.motivo', 'traslados.created_at')
                  ->get();
          }

   return Datatables::of($datos)->make(true);

}



    public function form_nuevo_traslado($id)
    {
        $dispositivo = Dispositivo::find($id);
        $sede_origen = $dispositivo->sede_id;
        $nombre_sede_origen = $dispositivo->temphistoricosedes->nombre_sede;
        $direccion = $dispositivo->temphistoricosedes->direccion;
        $serial = $dispositivo->serial;
        $marca = $dispositivo->marca;
        $subregiones = Subregion::orderBy('id', 'asc')->get();

        return view("formularios.form_nuevo_traslado")->with('id_dispositivo', $id)
                                                      ->with("sede_origen", $sede_origen)
                                                      ->with("subregiones", $subregiones)
                                                      ->with("nombre_sede_origen", $nombre_sede_origen)
                                                      ->with("serial", $serial)
                                                      ->with("marca", $marca)
                                                      ->with("direccion", $direccion);
    }

    public function crear_traslado(Request $request)
    {
        $dispositivo = Dispositivo::where("id", $request->input("dispositivo_id"))->first();
        if($dispositivo)
        {
            //Inicia la transacción
            DB::beginTransaction();
            try
            {
                //Actualiza la sede del dispositivo
                $dispositivo->sede_id = $request->input('sede_id'); //Sede destino.
                if(!$dispositivo->save())
                {
                    DB::rollBack();
                    return view("mensajes.mensaje_error_3")->with("msj","...Hubo un error durante la actualización del dispositivo en el traslado ;...") ;
                }

                //Crea el traslado
                $traslado = new Traslado();
                $traslado->sede_origen_id = $request->input('origen');
                $traslado->sede_destino_id = $request->input('sede_id');
                $traslado->motivo = $request->input('motivo');
                $traslado->descripcion = strtoupper($request->input('descripcion'));
                $traslado->dispositivo_id =  $request->input('dispositivo_id');

                if($traslado->save())
                {
                    DB::commit(); //Confirma la transacción
                    return view("mensajes.msj_traslado_creado")->with("msj","Traslado realizado correctamente");
                }
                else
                {
                    DB::rollBack();
                    return view("mensajes.mensaje_error_3")->with("msj","...Hubo un error durante el registro del traslado ;...") ;
                }
                //
            }
            catch(Exception $e)
            {
                DB::rollBack();
                return view("mensajes.mensaje_error_3")->with("msj","...Hubo un error durante el traslado ;...");
            }
        }
        else
        {
            return view("mensajes.mensaje_error_3")->with("msj", "El dispositivo " . $request->input("dispositivo_id") . " no existe.");
        }
    }

    public function form_editar_traslado($id)
    {
        $traslado = Traslado::find($id);
        $sede_origen = $traslado->sede_origen_id;
        $id_dispositivo = $traslado->dispositivo_id;
        $serial = $traslado->dispositivo->serial;
        $marca = $traslado->dispositivo->marca;

        $subregiones = Subregion::orderBy('id', 'asc')->get();

        $nombre_sede = $traslado->temphistoricosedes->nombre_sede;
        $nombre_sede_origen = $traslado->temphistoricosedes_sede_origen->nombre_sede;
        $nombre_sede_destino = $traslado->temphistoricosedes_sede_destino->nombre_sede;
        $direccion = $traslado->temphistoricosedes_sede_destino->direccion;

        return view('formularios.form_editar_traslado')->with('traslado', $traslado)
                                                       ->with("sede_origen", $sede_origen)
                                                       ->with("subregiones", $subregiones)
                                                       ->with("nombre_sede", $nombre_sede)
                                                       ->with("nombre_sede_origen", $nombre_sede_origen)
                                                       ->with("nombre_sede_destino", $nombre_sede_destino)
                                                       ->with("serial", $serial)
                                                       ->with("marca", $marca)
                                                       ->with("direccion", $direccion);

    }

    public function editar_traslado(Request $request, $id)
    {
        $buscar = Dispositivo::where("id", $request->input("dispositivo_id"))->first();
        if($buscar)
        {
            $traslado = Traslado::find($id);
            //$traslado->sede_origen_id = $request->input('origen');
            $traslado->sede_destino_id = $request->input('sede_id');
            $traslado->motivo = $request->input('motivo');
            $traslado->descripcion = strtoupper($request->input('descripcion'));
            $traslado->dispositivo_id =  $request->input('dispositivo_id');

            if($traslado->save())
            {
                return view("mensajes.msj_actualizado_traslado");
            }
            else
            {
                return view("mensajes.mensaje_error_3")->with("msj","...Hubo un error durante el traslado ;...") ;
            }
        }
        else
        {
            return view("mensajes.mensaje_error_3")->with("msj", "El dispositivo " . $request->input("dispositivo_id") . " no existe.");
        }
    }

    public function form_borrado_traslado($id)
    {
        $traslado = Traslado::find($id);
        return view("confirmaciones.form_borrado_traslado")->with("traslado",$traslado);
    }

    public function borrar_traslado(Request $request)
    {
        $traslado = Traslado::find($request->input('id_traslado'));

        if($traslado->delete())
        {
            return view("mensajes.msj_borrado");
        }
        else
        {
            return view("mensajes.mensaje_error_2")->with("msj","..Hubo un error al borrar ; intentarlo nuevamente..");
        }
    }

}
