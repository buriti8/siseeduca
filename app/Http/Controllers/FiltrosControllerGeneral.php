<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use DB;
use App\Http\Controllers\Controller;
use App\User;
use PDO;

class FiltrosControllerGeneral extends Controller
{
    function municipios_subregion(Request $request)
    {
        $select = $request->get('select');
        $value = $request->get('value');
        $dependent = $request->get('dependent');
        $valor = $request->get('valor');

        if ($valor == "todos")
        {
             $data = DB::table('municipios')
                    ->orderBy("NombreMunicipio","ASC")
                    ->get();
                    $out = "SELECCIONE UN MUNICIPIO...";
        }
        else
        {
            $data = DB::table('municipios')
                ->where('CodigoSubregion', $value)
                ->orderBy($dependent)
                ->get();
                $out = "SELECCIONE UN MUNICIPIO...";
        }

        $output = '<option value="0">'. $out .'</option>';
        foreach($data as $row)
        {
            // $output .= '<option value="'.$row->CodigoDaneMunicipio.'">'.$row->$dependent.'</option>';
            $output .= '<option value="'.$row->CodigoDaneMunicipio.'">'.$row->NombreMunicipio.'</option>';
        }

        echo $output;
    }
}
