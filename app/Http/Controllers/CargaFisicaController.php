<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\CargaFisica;
use Illuminate\Support\Facades\Validator;


class CargaFisicaController extends Controller
{
  /**
   * Create a new controller instance.
   *
   * @return void
   */
  public function __construct()
  {
      $this->middleware('auth');
  }

  public function carga_masiva_fisica_sedes($archivo){


          DB::statement("COPY sedes_infras FROM '$archivo' DELIMITER ';' CSV HEADER;");

          DB::statement('insert into sedes_infras (municipio,daneestablecimineto,nombreestablecimiento,danesede,nombresede,zona,
          ubicacion,telefono,numeroalumnos,distanciamunicipio,tipovia,created_at,updated_at) select * from sedes_infras;');






 }


  public function subir_archivos_fisica_sedes(Request $request)
  {

    $fisica_sedes = $request->file('archivo_fisica_sedes');

    $this->carga_masiva_fisica_sedes($fisica_sedes);


    return view("mensajes.msj_calidad_cargado")->with("msj","Carga exitosa") ;



  }


  /**
   * Show the application dashboard.
   *
   * @return Response
   */
  public function index()
  {

      return view("actualizacion.cargar_datos_fisica_sedes");
  }

}
