<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Support\Facades\DB;
use App\Matricula;
use Illuminate\Support\Facades\Validator;
use Storage;



/**
* Class HomeController
* @package App\Http\Controllers
*/
class MatriculaController extends Controller
{
  /**
  * Create a new controller instance.
  *
  * @return void
  */
  public function __construct()
  {
    $this->middleware('auth');
  }


  public function form_borrado_anexo($ano_info,$mes_corte){

    return view("confirmaciones.form_borrado_anexo")->with("ano_info",$ano_info)->with("mes_corte",$mes_corte);

  }

  public function borrar_anexo(Request $request){

    $ano_inf=$request->input("ano_info");
    $mes_corte=$request->input("mes_corte");

    $temporal = DB::delete("delete from matricula_actual where mes_corte=$mes_corte and ano_inf=$ano_inf");
    $res=Matricula::where('ano_inf',$ano_inf)->where('mes_corte',$mes_corte)->delete();


    if($res || ($res && $temporal)){
      DB::statement('refresh materialized view registro_anexos_cargados;');
      DB::statement('refresh materialized view vista_matriculas_todas;');
      DB::statement('refresh materialized view vista_matriculas_zonas;');

      return view("mensajes.msj_borrado");
    }
    else
    {
      return view("mensajes.mensaje_error")->with("msj","..Hubo un error al agregar ; intentarlo nuevamente..");
    }


  }

  public function carga_masiva($archivo,$mes,$sector,$calendario){
    try {

      DB::delete('delete from temp_historico_matricula');
      DB::statement("COPY temp_anexo_matricula FROM '$archivo' DELIMITER ';' CSV HEADER;");
      DB::statement('insert into temp_historico_matricula ( ANO_INF,MUN_CODIGO,CODIGO_DANE,DANE_ANTERIOR,CONS_SEDE,TIPO_DOCUMENTO,
      NRO_DOCUMENTO,EXP_DEPTO,EXP_MUN,APELLIDO1,APELLIDO2,NOMBRE1,NOMBRE2,DIRECCION_RESIDENCIA,TEL,RES_DEPTO,
      RES_MUN,ESTRATO,SISBEN,FECHA_NACIMIENTO,NAC_DEPTO,NAC_MUN,GENERO,POB_VICT_CONF,DPTO_EXP,MUN_EXP,
      PROVIENE_SECTOR_PRIV,PROVIENE_OTR_MUN,TIPO_DISCAPACIDAD,CAP_EXC,ETNIA,RES,INS_FAMILIAR,TIPO_JORNADA,
      CARACTER,ESPECIALIDAD,GRADO,GRUPO,METODOLOGIA,MATRICULA_CONTRATADA,REPITENTE,NUEVO,SIT_ACAD_ANO_ANT,
      CON_ALUM_ANO_ANT,FUE_RECU,ZON_ALU,CAB_FAMILIA,BEN_MAD_FLIA,BEN_VET_FP,BEN_HER_NAC,CODIGO_INTERNADO,CODIGO_VALORACION_1,
      CODIGO_VALORACION_2,NUM_CONVENIO,PER_ID,APOYO_ACADEMICO_ESPECIAL,SRPA, PAIS_ORIGEN) select ANO_INF,MUN_CODIGO,CODIGO_DANE,DANE_ANTERIOR,CONS_SEDE,TIPO_DOCUMENTO,
      NRO_DOCUMENTO,EXP_DEPTO,EXP_MUN,APELLIDO1,APELLIDO2,NOMBRE1,NOMBRE2,DIRECCION_RESIDENCIA,TEL,RES_DEPTO,
      RES_MUN,ESTRATO,SISBEN,FECHA_NACIMIENTO,NAC_DEPTO,NAC_MUN,GENERO,POB_VICT_CONF,DPTO_EXP,MUN_EXP,
      PROVIENE_SECTOR_PRIV,PROVIENE_OTR_MUN,TIPO_DISCAPACIDAD,CAP_EXC,ETNIA,RES,INS_FAMILIAR,TIPO_JORNADA,
      CARACTER,ESPECIALIDAD,GRADO,GRUPO,METODOLOGIA,MATRICULA_CONTRATADA,REPITENTE,NUEVO,SIT_ACAD_ANO_ANT,
      CON_ALUM_ANO_ANT,FUE_RECU,ZON_ALU,CAB_FAMILIA,BEN_MAD_FLIA,BEN_VET_FP,BEN_HER_NAC,CODIGO_INTERNADO,CODIGO_VALORACION_1,
      CODIGO_VALORACION_2,NUM_CONVENIO,PER_ID,APOYO_ACADEMICO_ESPECIAL,SRPA, PAIS_ORIGEN from temp_anexo_matricula;');

      DB::delete('delete from temp_anexo_matricula');

      DB::statement("update temp_historico_matricula  set mes_corte = '$mes' ,sector ='$sector', calendario='$calendario' , created_at=now(), updated_at=now();");

      DB::statement('insert into historico_matricula ( MES_CORTE,SECTOR,CALENDARIO,ANO_INF,MUN_CODIGO,CODIGO_DANE,DANE_ANTERIOR,CONS_SEDE,TIPO_DOCUMENTO,
      NRO_DOCUMENTO,EXP_DEPTO,EXP_MUN,APELLIDO1,APELLIDO2,NOMBRE1,NOMBRE2,DIRECCION_RESIDENCIA,TEL,RES_DEPTO,
      RES_MUN,ESTRATO,SISBEN,FECHA_NACIMIENTO,NAC_DEPTO,NAC_MUN,GENERO,POB_VICT_CONF,DPTO_EXP,MUN_EXP,
      PROVIENE_SECTOR_PRIV,PROVIENE_OTR_MUN,TIPO_DISCAPACIDAD,CAP_EXC,ETNIA,RES,INS_FAMILIAR,TIPO_JORNADA,
      CARACTER,ESPECIALIDAD,GRADO,GRUPO,METODOLOGIA,MATRICULA_CONTRATADA,REPITENTE,NUEVO,SIT_ACAD_ANO_ANT,
      CON_ALUM_ANO_ANT,FUE_RECU,ZON_ALU,CAB_FAMILIA,BEN_MAD_FLIA,BEN_VET_FP,BEN_HER_NAC,CODIGO_INTERNADO,CODIGO_VALORACION_1,
      CODIGO_VALORACION_2,NUM_CONVENIO,PER_ID,APOYO_ACADEMICO_ESPECIAL,SRPA,created_at,updated_at, PAIS_ORIGEN) select * from temp_historico_matricula;');

      DB::statement('insert into matricula_actual ( MES_CORTE,SECTOR,CALENDARIO,ANO_INF,MUN_CODIGO,CODIGO_DANE,DANE_ANTERIOR,CONS_SEDE,TIPO_DOCUMENTO,
      NRO_DOCUMENTO,EXP_DEPTO,EXP_MUN,APELLIDO1,APELLIDO2,NOMBRE1,NOMBRE2,DIRECCION_RESIDENCIA,TEL,RES_DEPTO,
      RES_MUN,ESTRATO,SISBEN,FECHA_NACIMIENTO,NAC_DEPTO,NAC_MUN,GENERO,POB_VICT_CONF,DPTO_EXP,MUN_EXP,
      PROVIENE_SECTOR_PRIV,PROVIENE_OTR_MUN,TIPO_DISCAPACIDAD,CAP_EXC,ETNIA,RES,INS_FAMILIAR,TIPO_JORNADA,
      CARACTER,ESPECIALIDAD,GRADO,GRUPO,METODOLOGIA,MATRICULA_CONTRATADA,REPITENTE,NUEVO,SIT_ACAD_ANO_ANT,
      CON_ALUM_ANO_ANT,FUE_RECU,ZON_ALU,CAB_FAMILIA,BEN_MAD_FLIA,BEN_VET_FP,BEN_HER_NAC,CODIGO_INTERNADO,CODIGO_VALORACION_1,
      CODIGO_VALORACION_2,NUM_CONVENIO,PER_ID,APOYO_ACADEMICO_ESPECIAL,SRPA,created_at,updated_at, PAIS_ORIGEN) select * from temp_historico_matricula;');


      DB::statement('refresh materialized view registro_anexos_cargados;');
      DB::statement('refresh materialized view vista_matriculas_todas;');
      DB::statement('refresh materialized view vista_matriculas_zonas;');
    } catch (Exception $e) {
      report($e);

      return view("mensajes.mensaje_error")->with("msj","..Hubo un error al agregar ; intentarlo nuevamente..");
    }
  }



  public function getColumnNameByValue($array, $value)
  {
    return in_array($value, $array)? $value : '';
  }


  public function validar_header($archivo){

    $opened_file = fopen($archivo, 'r');
    $header_init = fgetcsv($opened_file, 0, ';');
    $header = array_map('trim', $header_init);

    $ano_inf =  $this->getColumnNameByValue($header,'ANO_INF');
    $mun_codigo =  $this->getColumnNameByValue($header,'MUN_CODIGO');
    $codigo_dane =  $this->getColumnNameByValue($header,'CODIGO_DANE');
    $dane_anterior =  $this->getColumnNameByValue($header,'DANE_ANTERIOR');
    $cons_sede =  $this->getColumnNameByValue($header,'CONS_SEDE');
    $tipo_documento =  $this->getColumnNameByValue($header,'TIPO_DOCUMENTO');
    $nro_documento =  $this->getColumnNameByValue($header,'NRO_DOCUMENTO');
    $exp_depto =  $this->getColumnNameByValue($header,'EXP_DEPTO');
    $exp_mun =  $this->getColumnNameByValue($header,'EXP_MUN');
    $apellido1 =  $this->getColumnNameByValue($header,'APELLIDO1');
    $apellido2 =  $this->getColumnNameByValue($header,'APELLIDO2');
    $nombre1 =  $this->getColumnNameByValue($header,'NOMBRE1');
    $nombre2 =  $this->getColumnNameByValue($header,'NOMBRE2');
    $direccion_residencia =  $this->getColumnNameByValue($header,'DIRECCION_RESIDENCIA');
    $tel =  $this->getColumnNameByValue($header,'TEL');
    $res_depto =  $this->getColumnNameByValue($header,'RES_DEPTO');
    $res_mun =  $this->getColumnNameByValue($header,'RES_MUN');
    $estrato =  $this->getColumnNameByValue($header,'ESTRATO');
    $sisben =  $this->getColumnNameByValue($header,'SISBEN');
    $fecha_nacimiento =  $this->getColumnNameByValue($header,'FECHA_NACIMIENTO');
    $nac_depto =  $this->getColumnNameByValue($header,'NAC_DEPTO');
    $nac_mun =  $this->getColumnNameByValue($header,'NAC_MUN');
    $genero =  $this->getColumnNameByValue($header,'GENERO');
    $pob_vict_conf =  $this->getColumnNameByValue($header,'POB_VICT_CONF');
    $dpto_exp =  $this->getColumnNameByValue($header,'DPTO_EXP');
    $mun_exp =  $this->getColumnNameByValue($header,'MUN_EXP');
    $proviene_sector_priv =  $this->getColumnNameByValue($header,'PROVIENE_SECTOR_PRIV');
    $proviene_otr_mun =  $this->getColumnNameByValue($header,'PROVIENE_OTR_MUN');
    $tipo_discapacidad =  $this->getColumnNameByValue($header,'TIPO_DISCAPACIDAD');
    $cap_exc =  $this->getColumnNameByValue($header,'CAP_EXC');
    $etnia =  $this->getColumnNameByValue($header,'ETNIA');
    $res =  $this->getColumnNameByValue($header,'RES');
    $ins_familiar =  $this->getColumnNameByValue($header,'INS_FAMILIAR');
    $tipo_jornada =  $this->getColumnNameByValue($header,'TIPO_JORNADA');
    $caracter =  $this->getColumnNameByValue($header,'CARACTER');
    $especialidad =  $this->getColumnNameByValue($header,'ESPECIALIDAD');
    $grado =  $this->getColumnNameByValue($header,'GRADO');
    $grupo =  $this->getColumnNameByValue($header,'GRUPO');
    $metodologia =  $this->getColumnNameByValue($header,'METODOLOGIA');
    $matricula_contratada =  $this->getColumnNameByValue($header,'MATRICULA_CONTRATADA');
    $repitente =  $this->getColumnNameByValue($header,'REPITENTE');
    $nuevo =  $this->getColumnNameByValue($header,'NUEVO');
    $sit_acad_ano_ant =  $this->getColumnNameByValue($header,'SIT_ACAD_ANO_ANT');
    $con_alum_ano_ant =  $this->getColumnNameByValue($header,'CON_ALUM_ANO_ANT');
    $fue_recu =  $this->getColumnNameByValue($header,'FUE_RECU');
    $zon_alu =  $this->getColumnNameByValue($header,'ZON_ALU');
    $cab_familia =  $this->getColumnNameByValue($header,'CAB_FAMILIA');
    $ben_mad_flia =  $this->getColumnNameByValue($header,'BEN_MAD_FLIA');
    $ben_vet_fp =  $this->getColumnNameByValue($header,'BEN_VET_FP');
    $ben_her_nac =  $this->getColumnNameByValue($header,'BEN_HER_NAC');
    $codigo_internado =  $this->getColumnNameByValue($header,'CODIGO_INTERNADO');
    $codigo_valoracion_1 =  $this->getColumnNameByValue($header,'CODIGO_VALORACION_1');
    $codigo_valoracion_2 =  $this->getColumnNameByValue($header,'CODIGO_VALORACION_2');
    $num_convenio =  $this->getColumnNameByValue($header,'NUM_CONVENIO');
    $per_id =  $this->getColumnNameByValue($header,'PER_ID');
    $apoyo_academico_especial =  $this->getColumnNameByValue($header,'APOYO_ACADEMICO_ESPECIAL');
    $srpa =  $this->getColumnNameByValue($header,'SRPA');
    $pais_origen = $this->getColumnNameByValue($header, 'PAIS_ORIGEN');


    fclose($opened_file);

    $validation_array = [
      'ano_inf'=>$ano_inf,
      'mun_codigo'=>$mun_codigo,
      'codigo_dane'=>$codigo_dane,
      'dane_anterior'=>$dane_anterior,
      'cons_sede'=>$cons_sede,
      'tipo_documento'=>$tipo_documento,
      'nro_documento'=>$nro_documento,
      'exp_depto'=>$exp_depto,
      'exp_mun'=>$exp_mun,
      'apellido1'=>$apellido1,
      'apellido2'=>$apellido2,
      'nombre1'=>$nombre1,
      'nombre2'=>$nombre2,
      'direccion_residencia'=>$direccion_residencia,
      'tel'=>$tel,
      'res_depto'=>$res_depto,
      'res_mun'=>$res_mun,
      'estrato'=>$estrato,
      'sisben'=>$sisben,
      'fecha_nacimiento'=>$fecha_nacimiento,
      'nac_depto'=>$nac_depto,
      'nac_mun'=>$nac_mun,
      'genero'=>$genero,
      'pob_vict_conf'=>$pob_vict_conf,
      'dpto_exp'=>$dpto_exp,
      'mun_exp'=>$mun_exp,
      'proviene_sector_priv'=>$proviene_sector_priv,
      'proviene_otr_mun'=>$proviene_otr_mun,
      'tipo_discapacidad'=>$tipo_discapacidad,
      'cap_exc'=>$cap_exc,
      'etnia'=>$etnia,
      'res'=>$res,
      'ins_familiar'=>$ins_familiar,
      'tipo_jornada'=>$tipo_jornada,
      'caracter'=>$caracter,
      'especialidad'=>$especialidad,
      'grado'=>$grado,
      'grupo'=>$grupo,
      'metodologia'=>$metodologia,
      'matricula_contratada'=>$matricula_contratada,
      'repitente'=>$repitente,
      'nuevo'=>$nuevo,
      'sit_acad_ano_ant'=>$sit_acad_ano_ant,
      'con_alum_ano_ant'=>$con_alum_ano_ant,
      'fue_recu'=>$fue_recu,
      'zon_alu'=>$zon_alu,
      'cab_familia'=>$cab_familia,
      'ben_mad_flia'=>$ben_mad_flia,
      'ben_vet_fp'=>$ben_vet_fp,
      'ben_her_nac'=>$ben_her_nac,
      'codigo_internado'=>$codigo_internado,
      'codigo_valoracion_1'=>$codigo_valoracion_1,
      'codigo_valoracion_2'=>$codigo_valoracion_2,
      'num_convenio'=>$num_convenio,
      'per_id'=>$per_id,
      'apoyo_academico_especial'=>$apoyo_academico_especial,
      'srpa'=>$srpa,
      'pais_origen'=>$pais_origen
    ];


    return $validation_array;

  }


  public function get_anio($archivo){

    $rows = count(file($archivo));
    if($rows<=1){
      return 0;
    }
    ini_set('auto_detect_line_endings', true);

    $opened_file = fopen($archivo, 'r');
    $header = fgetcsv($opened_file, 0, ';');
    $data_row = fgetcsv($opened_file, 0, ';');
    $first_row = array_combine($header, $data_row);
    $anio = array_key_exists('ANO_INF', $first_row)? $first_row['ANO_INF'] : '';
    fclose($opened_file);

    return $anio;

  }


  public function subir_anexo(Request $request)
  {

    $reglas=[  'anexo1' => 'required|mimes:csv,txt',
    'anexo2' => 'required|mimes:csv,txt',
    'anexo3' => 'required|mimes:csv,txt',
    'anexo4' => 'required|mimes:csv,txt',
    'anexo5' => 'required|mimes:csv,txt',
    'anexo6' => 'required|mimes:csv,txt',

  ];

  $mensajes=[  'anexo1.mimes' => 'El archivo seleccionado en Anexo 5 AA no corresponde al formato permitido (CSV,txt) ',
  'anexo2.mimes' => 'El archivo seleccionado en Anexo 5 AB no corresponde al formato permitido (CSV,txt) ',
  'anexo3.mimes' => 'El archivo seleccionado en Anexo 5 OTRO no corresponde al formato permitido (CSV,txt) ',
  'anexo4.mimes' => 'El archivo seleccionado en Anexo 6 AA no corresponde al formato permitido (CSV,txt) ',
  'anexo5.mimes' => 'El archivo seleccionado en Anexo 6 AB no corresponde al formato permitido (CSV,txt) ',
  'anexo6.mimes' => 'El archivo seleccionado en Anexo 6 OTRO no corresponde al formato permitido (CSV,txt) ',
];


$validator0 = Validator::make( $request->all(),$reglas,$mensajes );
if( $validator0->fails() ){

  return view("mensajes.mensaje_error")->with("msj","Existen errores.")
  ->withErrors($validator0->errors());
}




$mes =$request->input("mes");
$sector_oficial="OFICIAL";
$sector_no_oficial="NO OFICIAL";
$calendario_A="A";
$calendario_B="B";
$calendario_Otro="OTRO";

$nuevo_nombre_archivo1 ="ANEXO5AA.csv";
$nuevo_nombre_archivo2 ="ANEXO5AB.csv";
$nuevo_nombre_archivo3 ="ANEXO5BB.csv";
$nuevo_nombre_archivo4 ="ANEXO6AA.csv";
$nuevo_nombre_archivo5 ="ANEXO6AB.csv";
$nuevo_nombre_archivo6 ="ANEXO6BB.csv";


Storage::disk('temp')->put($nuevo_nombre_archivo1,  \File::get($request->file('anexo1')) );
Storage::disk('temp')->put($nuevo_nombre_archivo2,  \File::get($request->file('anexo2')) );
Storage::disk('temp')->put($nuevo_nombre_archivo3,  \File::get($request->file('anexo3')) );
Storage::disk('temp')->put($nuevo_nombre_archivo4,  \File::get($request->file('anexo4')) );
Storage::disk('temp')->put($nuevo_nombre_archivo5,  \File::get($request->file('anexo5')) );
Storage::disk('temp')->put($nuevo_nombre_archivo6,  \File::get($request->file('anexo6')) );

$archivo1=Storage::disk('temp')->getDriver()->getAdapter()->applyPathPrefix('ANEXO5AA.csv');
$archivo2=Storage::disk('temp')->getDriver()->getAdapter()->applyPathPrefix('ANEXO5AB.csv');
$archivo3=Storage::disk('temp')->getDriver()->getAdapter()->applyPathPrefix('ANEXO5BB.csv');
$archivo4=Storage::disk('temp')->getDriver()->getAdapter()->applyPathPrefix('ANEXO6AA.csv');
$archivo5=Storage::disk('temp')->getDriver()->getAdapter()->applyPathPrefix('ANEXO6AB.csv');
$archivo6=Storage::disk('temp')->getDriver()->getAdapter()->applyPathPrefix('ANEXO6BB.csv');

$reglas1=[  'ano_inf'=>'required',
'mun_codigo'=>'required',
'codigo_dane'=>'required',
'dane_anterior'=>'required',
'cons_sede'=>'required',
'tipo_documento'=>'required',
'nro_documento'=>'required',
'exp_depto'=>'required',
'exp_mun'=>'required',
'apellido1'=>'required',
'apellido2'=>'required',
'nombre1'=>'required',
'nombre2'=>'required',
'direccion_residencia'=>'required',
'tel'=>'required',
'res_depto'=>'required',
'res_mun'=>'required',
'estrato'=>'required',
'sisben'=>'required',
'fecha_nacimiento'=>'required',
'nac_depto'=>'required',
'nac_mun'=>'required',
'genero'=>'required',
'pob_vict_conf'=>'required',
'dpto_exp'=>'required',
'mun_exp'=>'required',
'proviene_sector_priv'=>'required',
'proviene_otr_mun'=>'required',
'tipo_discapacidad'=>'required',
'cap_exc'=>'required',
'etnia'=>'required',
'res'=>'required',
'ins_familiar'=>'required',
'tipo_jornada'=>'required',
'caracter'=>'required',
'especialidad'=>'required',
'grado'=>'required',
'grupo'=>'required',
'metodologia'=>'required',
'matricula_contratada'=>'required',
'repitente'=>'required',
'nuevo'=>'required',
'sit_acad_ano_ant'=>'required',
'con_alum_ano_ant'=>'required',
'fue_recu'=>'required',
'zon_alu'=>'required',
'cab_familia'=>'required',
'ben_mad_flia'=>'required',
'ben_vet_fp'=>'required',
'ben_her_nac'=>'required',
'codigo_internado'=>'required',
'codigo_valoracion_1'=>'required',
'codigo_valoracion_2'=>'required',
'num_convenio'=>'required',
'per_id'=>'required',
'apoyo_academico_especial'=>'required',
'srpa'=>'required',
'pais_origen'=>'required',
];

$mensajes1=[  'required'=>'La columna :attribute no se encuentra en el encabezado del anexo cargado o tiene espacios en blanco. Recuerde que el encabezado de los anexos debe ser igual al formato  2018.  ',
];

$validator1 = Validator::make( $this->validar_header($archivo1),$reglas1,$mensajes1 );
$validator2 = Validator::make( $this->validar_header($archivo2),$reglas1,$mensajes1 );
$validator3 = Validator::make( $this->validar_header($archivo3),$reglas1,$mensajes1 );
$validator4 = Validator::make( $this->validar_header($archivo4),$reglas1,$mensajes1 );
$validator5 = Validator::make( $this->validar_header($archivo5),$reglas1,$mensajes1 );
$validator6 = Validator::make( $this->validar_header($archivo6),$reglas1,$mensajes1 );


if( $validator1->fails() ){

  $this->limpiar_temp($nuevo_nombre_archivo1,$nuevo_nombre_archivo2,$nuevo_nombre_archivo3,$nuevo_nombre_archivo4,$nuevo_nombre_archivo5,$nuevo_nombre_archivo6);

  return view("mensajes.mensaje_error")->with("msj","Existen errores en ANEXO 5 AA.")
  ->withErrors($validator1->errors());
}
if( $validator2->fails() ){
  $this->limpiar_temp($nuevo_nombre_archivo1,$nuevo_nombre_archivo2,$nuevo_nombre_archivo3,$nuevo_nombre_archivo4,$nuevo_nombre_archivo5,$nuevo_nombre_archivo6);

  return view("mensajes.mensaje_error")->with("msj","Existen errores en ANEXO 5 AB.")
  ->withErrors($validator2->errors());
}
if( $validator3->fails() ){
  $this->limpiar_temp($nuevo_nombre_archivo1,$nuevo_nombre_archivo2,$nuevo_nombre_archivo3,$nuevo_nombre_archivo4,$nuevo_nombre_archivo5,$nuevo_nombre_archivo6);

  return view("mensajes.mensaje_error")->with("msj","Existen errores en ANEXO 5 OTRO.")
  ->withErrors($validator3->errors());
}
if( $validator4->fails() ){
  $this->limpiar_temp($nuevo_nombre_archivo1,$nuevo_nombre_archivo2,$nuevo_nombre_archivo3,$nuevo_nombre_archivo4,$nuevo_nombre_archivo5,$nuevo_nombre_archivo6);

  return view("mensajes.mensaje_error")->with("msj","Existen errores en ANEXO 6 AA.")
  ->withErrors($validator4->errors());
}

if( $validator5->fails() ){
  $this->limpiar_temp($nuevo_nombre_archivo1,$nuevo_nombre_archivo2,$nuevo_nombre_archivo3,$nuevo_nombre_archivo4,$nuevo_nombre_archivo5,$nuevo_nombre_archivo6);

  return view("mensajes.mensaje_error")->with("msj","Existen errores en ANEXO 6 AB.")
  ->withErrors($validator5->errors());
}
if( $validator6->fails() ){
  $this->limpiar_temp($nuevo_nombre_archivo1,$nuevo_nombre_archivo2,$nuevo_nombre_archivo3,$nuevo_nombre_archivo4,$nuevo_nombre_archivo5,$nuevo_nombre_archivo6);

  return view("mensajes.mensaje_error")->with("msj","Existen errores en ANEXO 6 OTRO.")
  ->withErrors($validator6->errors());
}

$anio1= $this->get_anio($archivo1);
$anio2= $this->get_anio($archivo2);
$anio3= $this->get_anio($archivo3);
$anio4= $this->get_anio($archivo4);
$anio5= $this->get_anio($archivo5);
$anio6= $this->get_anio($archivo6);

$anio_max= max($anio1,$anio2,$anio3,$anio4,$anio5,$anio6);

$array_anio = array($anio_max,0);

if (!in_array($anio1, $array_anio) || !in_array($anio2, $array_anio) || !in_array($anio3, $array_anio) || !in_array($anio4, $array_anio)  || !in_array($anio5, $array_anio) || !in_array($anio6, $array_anio)) {

  $this->limpiar_temp($nuevo_nombre_archivo1,$nuevo_nombre_archivo2,$nuevo_nombre_archivo3,$nuevo_nombre_archivo4,$nuevo_nombre_archivo5,$nuevo_nombre_archivo6);

  return view("mensajes.mensaje_error")->with("msj","No coinciden el año de los anexos cargados.");

}



$busqueda1= DB::table('registro_anexos_cargados')->where('ano_inf', $anio1)->where('mes_corte', $mes)->count();
$busqueda2= DB::table('registro_anexos_cargados')->where('ano_inf', $anio2)->where('mes_corte', $mes)->count();
$busqueda3= DB::table('registro_anexos_cargados')->where('ano_inf', $anio3)->where('mes_corte', $mes)->count();
$busqueda4= DB::table('registro_anexos_cargados')->where('ano_inf', $anio4)->where('mes_corte', $mes)->count();
$busqueda5= DB::table('registro_anexos_cargados')->where('ano_inf', $anio5)->where('mes_corte', $mes)->count();
$busqueda6= DB::table('registro_anexos_cargados')->where('ano_inf', $anio6)->where('mes_corte', $mes)->count();


if( $busqueda1>0 || $busqueda2>0 || $busqueda3>0 || $busqueda4>0 || $busqueda5>0 || $busqueda6>0 ){

  switch($mes) {
    case 1: $mes_letras= "Enero"; break;
    case 2: $mes_letras= "Febrero"; break;
    case 3: $mes_letras= "Marzo"; break;
    case 4: $mes_letras= "Abril"; break;
    case 5: $mes_letras= "Mayo"; break;
    case 6: $mes_letras= "Junio"; break;
    case 7: $mes_letras= "Julio"; break;
    case 8: $mes_letras= "Agosto"; break;
    case 9: $mes_letras= "Septiembre"; break;
    case 10: $mes_letras= "Octubre"; break;
    case 11: $mes_letras= "Noviembre"; break;
    case 12: $mes_letras= "Diciembre"; break;
    default:$mes_letras= "No match!"; break;
  }
  $this->limpiar_temp($nuevo_nombre_archivo1,$nuevo_nombre_archivo2,$nuevo_nombre_archivo3,$nuevo_nombre_archivo4,$nuevo_nombre_archivo5,$nuevo_nombre_archivo6);

  return view("mensajes.mensaje_error")->with("msj","Ya existen registros para el Mes: $mes_letras Año: $anio_max .");
}
DB::delete('delete from matricula_actual');

$this->carga_masiva($archivo1,$mes,$sector_no_oficial,$calendario_A);
$this->carga_masiva($archivo2,$mes,$sector_no_oficial,$calendario_B);
$this->carga_masiva($archivo3,$mes,$sector_no_oficial,$calendario_Otro);
$this->carga_masiva($archivo4,$mes,$sector_oficial,$calendario_A);
$this->carga_masiva($archivo5,$mes,$sector_oficial,$calendario_B);
$this->carga_masiva($archivo6,$mes,$sector_oficial,$calendario_Otro);

$this->limpiar_temp($nuevo_nombre_archivo1,$nuevo_nombre_archivo2,$nuevo_nombre_archivo3,$nuevo_nombre_archivo4,$nuevo_nombre_archivo5,$nuevo_nombre_archivo6);

return view("mensajes.msj_anexo_cargado")->with("msj","Carga exitosa") ;

}


public function limpiar_temp($nombre1,$nombre2,$nombre3,$nombre4,$nombre5,$nombre6)
{
  Storage::disk('temp')->delete( $nombre1);
  Storage::disk('temp')->delete( $nombre2);
  Storage::disk('temp')->delete( $nombre3);
  Storage::disk('temp')->delete( $nombre4);
  Storage::disk('temp')->delete( $nombre5);
  Storage::disk('temp')->delete( $nombre6);

}

public function DescargarPlantillaMatricula()
{
  $filename=public_path().'/plantilla_matricula/matricula.csv';
  $finfo=finfo_open(FILEINFO_MIME_TYPE);
  $mimeType=finfo_file($finfo, $filename);

  header("Content-type:".$mimeType);
  header("Content-Disposition: attachment; filename=matricula.csv");
  echo file_get_contents($filename);
}




/**
* Show the application dashboard.
*
* @return Response
*/
public function index()
{
  $listado = DB::table('registro_anexos_cargados')
  ->select('ano_inf', 'mes_corte','fecha','cantidad5aa','cantidad5ab','cantidad5otro','cantidad6aa','cantidad6ab','cantidad6otro','cantidadtotal')
  ->paginate(12);

  return view("matricula.cargar_datos")->with("listados",$listado);
}
}
