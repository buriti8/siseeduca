<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\temp_historico_establecimiento;
use App\HistoricoPrueba;
use Illuminate\Support\Facades\Auth;

class ConsultaBasicaPruebasController extends Controller
{
  /**
  * Display a listing of the resource.
  *
  * @return \Illuminate\Http\Response
  */
  public function index()
  {
    $temp_historico_establecimientos = temp_historico_establecimiento::all()
    ->sortBy('nombre_establecimiento')
    ->where("prestador_de_servicio", "=", "OFICIAL")
    ->where("estado", "<>", 'CIERRE DEFINITIVO')
    ->where("estado", "<>", 'CIERRE TEMPORAL');

    $usuario = Auth::user()->user;
    if(Auth::user()->isRole('secretarios')){
      $temp_historico_establecimientos = $temp_historico_establecimientos ->where("codigo_dane_municipio", "=", Auth::user()->name);
    }elseif (Auth::user()->isRole('rectores')) {
      $temp_historico_establecimientos = $temp_historico_establecimientos ->where("codigo_establecimiento", "=", Auth::user()->name);
    }

    return view('PruebaSaber.consulta_basica_saber', array('temp_historico_establecimientos' => $temp_historico_establecimientos));
  }

  public function antioquia()
  {
    return view('PruebaSaber.consulta_basica_saber_antioquia');
  }

  //Inicio de funciones para extraer los arrays para gráficas por establecimiento educativo
  public function reporte_saberMaterias($sede){
    $resultado = [];
    foreach (HistoricoPrueba::reportGetBySedeMaterias($sede) as $row) {
      $resultado[] = [
        'periodo' => (float) $row->periodo,
        'total' => (float) $row->total,
        'naturales' => (float) number_format(round($row->naturales/$row->total, 2), 2),
        'ingles' => (float) number_format(round($row->ingles/$row->total, 2), 2),
        'matematicas' => (float) number_format(round($row->matematicas/$row->total, 2), 2),
        'lectura' => (float) number_format(round($row->lectura/$row->total, 2), 2),
        'sociales' => (float) number_format(round($row->sociales/$row->total, 2), 2)
      ];
    }
    return $resultado;
  }

  public function reporte_saberMatematicas($sede){
    $resultado = [];
    foreach (HistoricoPrueba::reportGetBySedeMatematicas($sede) as $row) {
      $resultado[] = [
        'periodo' => (float) $row->periodo,
        'total' => (float) $row->total,
        'uno_m' => (float) $row->uno_m,
        'dos_m' => (float) $row->dos_m,
        'tres_m' => (float) $row->tres_m,
        'cuatro_m' => (float) $row->cuatro_m
      ];
    }
    return $resultado;
  }

  public function reporte_saberNaturales($sede){
    $resultado = [];
    foreach (HistoricoPrueba::reportGetBySedeNaturales($sede) as $row) {
      $resultado[] = [
        'periodo' => (float) $row->periodo,
        'total' => (float) $row->total,
        'uno_n' => (float) $row->uno_n,
        'dos_n' => (float) $row->dos_n,
        'tres_n' => (float) $row->tres_n,
        'cuatro_n' => (float) $row->cuatro_n
      ];
    }
    return $resultado;
  }

  public function reporte_saberLectura($sede){
    $resultado = [];
    foreach (HistoricoPrueba::reportGetBySedeLectura($sede) as $row) {
      $resultado[] = [
        'periodo' => (float) $row->periodo,
        'total' => (float) $row->total,
        'uno_l' => (float) $row->uno_l,
        'dos_l' => (float) $row->dos_l,
        'tres_l' => (float) $row->tres_l,
        'cuatro_l' => (float) $row->cuatro_l
      ];
    }
    return $resultado;
  }

  public function reporte_saberIngles($sede){
    $resultado = [];
    foreach (HistoricoPrueba::reportGetBySedeIngles($sede) as $row) {
      $resultado[] = [
        'periodo' => (float) $row->periodo,
        'total' => (float) $row->total,
        'uno_i' => (float) $row->uno_i,
        'dos_i' => (float) $row->dos_i,
        'tres_i' => (float) $row->tres_i,
        'cuatro_i' => (float) $row->cuatro_i,
        'cinco_i' => (float) $row->cinco_i
      ];
    }
    return $resultado;
  }

  public function reporte_saberSociales($sede){
    $resultado = [];
    foreach (HistoricoPrueba::reportGetBySedeSociales($sede) as $row) {
      $resultado[] = [
        'periodo' => (float) $row->periodo,
        'total' => (float) $row->total,
        'uno_s' => (float) $row->uno_s,
        'dos_s' => (float) $row->dos_s,
        'tres_s' => (float) $row->tres_s,
        'cuatro_s' => (float) $row->cuatro_s
      ];
    }
    return $resultado;
  }
  //Fin de funciones para extraer los arrays para gráficas por establecimiento educativo


  /**
  * Show the form for creating a new resource.
  *
  * @return \Illuminate\Http\Response
  */
  public function create()
  {

  }

  /**
  * Store a newly created resource in storage.
  *
  * @param  \Illuminate\Http\Request  $request
  * @return \Illuminate\Http\Response
  */
  public function store(Request $request)
  {
    //
  }

  /**
  * Display the specified resource.
  *
  * @param  int  $id
  * @return \Illuminate\Http\Response
  */
  public function show($id)
  {
    //
  }

  /**
  * Show the form for editing the specified resource.
  *
  * @param  int  $id
  * @return \Illuminate\Http\Response
  */
  public function edit($id)
  {
    //
  }

  /**
  * Update the specified resource in storage.
  *
  * @param  \Illuminate\Http\Request  $request
  * @param  int  $id
  * @return \Illuminate\Http\Response
  */
  public function update(Request $request, $id)
  {
    //
  }

  /**
  * Remove the specified resource from storage.
  *
  * @param  int  $id
  * @return \Illuminate\Http\Response
  */
  public function destroy($id)
  {
    //
  }
}
