<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SolicitudEspacio extends Model
{
    public function Solicitud(){
       return $this->belongsTo('App\SolicitudIF', 'SolicitudId', 'id');
   }

   public function TipoEspacios(){
	     return $this->belongsTo('App\TipoEspacio', 'TipoEspacio', 'id');
   }

   public function Estado(){
	     return $this->belongsTo('App\IFisicaEstado', 'EstadoEspacio', 'id');
   }

   public function TipoS(){
	     // return $this->hasOne('App\TipoSolicitud', 'id', 'TipoSolicitud');
	     return $this->belongsTo('App\TipoSolicitud', 'TipoSolicitud', 'id');
	     // return $this->belongsTo('App\TipoSolicitud');
   }
}
