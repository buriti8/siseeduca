<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Traslado extends Model
{
    protected $table = 'traslados';
    protected $fillable = ['sede_origen_id', 'sede_destino_id', 'serial', 'motivo', 'descripcion'];
    
    // Ejemplo de casting de eloquent: protected $casts = [ 'is_admin' => 'boolean',];

    public function dispositivo()
    {
        return $this->belongsTo('App\Dispositivo');
    }

    public function temphistoricosedes()
    {        
        return $this->belongsTo('App\TempHistoricoSede', 'sede_destino_id', 'codigo_sede');
    }

    //Esta relación hace la misma función que la anterior, pero aquí relaciona con otro campo.
    public function temphistoricosedes_sede_origen()
    {        
        return $this->belongsTo('App\TempHistoricoSede', 'sede_origen_id', 'codigo_sede');
    }

    //Esta relación hace la misma función que la anterior, pero aquí relaciona con otro campo.
    public function temphistoricosedes_sede_destino()
    {        
        return $this->belongsTo('App\TempHistoricoSede', 'sede_destino_id', 'codigo_sede');
    }
}
