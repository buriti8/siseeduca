<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HistoricoRiesgos extends Model
{
  protected $fillable = [
  'codigo_dane','nombre_sede',' riesgos'];

  public function HistoricoSedes()
  {
      return $this->belongsTo('App\temp_historico_sede','codigo_sede','codigo_dane');
  }

}
