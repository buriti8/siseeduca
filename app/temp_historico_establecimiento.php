<?php

namespace App;
use App\temp_historico_establecimiento;
use Illuminate\Database\Eloquent\Model;

class temp_historico_establecimiento extends Model
{
    protected $table = 'temp_historico_establecimientos';
    public function Sedes()
     {
         return $this->hasMany('App\temp_historico_sede', 'codigo_establecimiento', 'codigo_establecimiento');
     }

     public function SolicitudesIFisica(){
       return $this->hasMany('App\SolicitudIF', 'DaneEstablecimiento', 'codigo_establecimiento');
     }

     public function modal(){
       return $this->hasMany('App\modal', 'DaneEstablecimiento', 'codigo_establecimiento');
     }

     









}
