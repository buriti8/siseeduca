<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PeriodoPrueba extends Model
{
  protected $fillable = [
    'IdPeriodoSaber','periodo', 'Anio','orden_anio',
  ];
}
