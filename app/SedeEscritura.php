<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SedeEscritura extends Model
{
  protected $fillable = [
'DaneSede','NombreSede','Propietario','MatriculaInmob','NumEscritura',
'FechaEscritura','DaneMunicipio','DaneEstablecimiento'];

public function HistoricoSedes()
{
    return $this->belongsTo('App\temp_historico_sede', 'DaneSede', 'codigo_sede');
}
}
