<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HistoricoSede extends Model
{
    protected $table = 'historico_sedes';

    public function HistoricoEstablecimiento()
    {
        return $this->belongsTo('App\HistoricoEstablecimiento', 'codigo_establecimiento', 'codigo_establecimiento');
    }

    public function dispositivos()
    {
        return $this->hasMany('App\Dispositivo', 'sede_id', 'id');
    }
}
