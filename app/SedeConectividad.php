<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SedeConectividad extends Model
{
  protected $fillable = [
    'Codigodanesede','Nombresedeeducativa','Direccion','Telefono','Zona','Departamento','Municipio','Programaorigendelosrecursos'.'Numerodecontrato',
    'Operador','Anchodebanda','Fechainicioservicio','Fechafinservicio', 'Estado','DaneMunicipio','DaneEstablecimiento', 'Servicio'
   ];

   public function HistoricoSedes()
   {
       return $this->belongsTo('App\temp_historico_sede', 'Codigodanesede', 'codigo_sede');
   }
}
