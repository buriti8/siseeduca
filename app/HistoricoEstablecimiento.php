<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HistoricoEstablecimiento extends Model
{
    protected $table = 'historico_establecimientos';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'codigo_dane_municipio','nombre_establecimiento','codigo_establecimiento',
    ];

public function Sedes()
 {
     return $this->hasMany('App\HistoricoSede', 'codigo_establecimiento', 'codigo_establecimiento');
 }
}
