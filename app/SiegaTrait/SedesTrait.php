<?php

namespace App\SiegaTrait;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

/**
 * Trait para las sedes
 */
trait SedesTrait
{
    public function ambito_sedes($value, $valor)
    {
         //Sedes según el rol del usuario
         $codigo = Auth::user()->name;
         if(Auth::user()->isRole('secretarios'))
         {
            //Busca por municipio
            $listado = DB::table('temp_historico_sedes')->select('codigo_sede', 'nombre_sede')
                                                                ->where('codigo_establecimiento', $value)
                                                                //->whereIn('estado_sede', ['NUEVO-ACTIVO', 'ANTIGUO-ACTIVO'])
                                                                ->orderBy('nombre_sede')
                                                                ->get();
            //Nota: se sabe que es del municipio porque hay filtro en los establecimientos que se muestran. Deshabilito por ahora estado_sede porque traería inconsistencias con traslado dispositivos.
         }
         else if(Auth::user()->isRole('rectores'))
         {
            //Busca por establecimiento
            $listado = DB::table('temp_historico_sedes')->where('codigo_establecimiento', $codigo)
                                                     ->select('codigo_sede', 'nombre_sede')
                                                     ->orderBy('nombre_sede')
                                                     ->get();
         }
         else
         {
            //Muestra todas las sedes si el rol es Administrador, Consulta, etc.
            if($valor == 'todos')
            {
                $listado = DB::table('temp_historico_sedes')
                ->orderBy('nombre_sede','ASC')
                ->get();
            }
            else
            {
                $listado = DB::table('temp_historico_sedes')
                ->where('codigo_establecimiento', $value)
                ->orderBy('nombre_sede','ASC')
                ->get();
            }
         }
         return $listado;
    }
    //Fin ámbito sedes

    public function ambito_establecimientos($value, $valor)
    {
         //Sedes según el rol del usuario
         $codigo = Auth::user()->name;
         if(Auth::user()->isRole('secretarios'))
         {
            //Busca por municipio
            $data = DB::table('temp_historico_establecimientos')
            ->where('codigo_dane_municipio', $codigo)
            ->where('sector', 'OFICIAL')
            ->whereIn("estado", ['NUEVO-ACTIVO', 'ANTIGUO-ACTIVO'])
            ->orderBy('nombre_establecimiento','ASC')
            ->get();
            //Nota: se sabe que es del municipio porque hay filtro en los establecimientos que se muestran. Deshabilito por ahora estado_sede porque traería inconsistencias con traslado dispositivos.
         }
         else if(Auth::user()->isRole('rectores'))
         {
            //Busca por establecimiento
            //No es necesario.
         }
         else
         {
            //Muestra todas las sedes si el rol es Administrador, Consulta, etc.
            if($valor == 'todos')
            {
                $data = DB::table('temp_historico_establecimientos')
                ->where('sector', 'OFICIAL')
                ->whereIn("estado", ['NUEVO-ACTIVO', 'ANTIGUO-ACTIVO'])
                ->orderBy('nombre_establecimiento','ASC')
                ->get();
            }
            else
            {
                $data = DB::table('temp_historico_establecimientos')
                ->where('codigo_dane_municipio', $value)
                ->where('sector', 'OFICIAL')
                ->whereIn("estado", ['NUEVO-ACTIVO', 'ANTIGUO-ACTIVO'])
                ->orderBy('nombre_establecimiento','ASC')
                ->get();
            }
         }
         return $data;
    }
}
