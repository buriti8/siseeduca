<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class IFisicaEstado extends Model
{
     public function Solicitud(){
       return $this->hasMany('App\SolicitudEspacio', 'EstadoEspacio', 'id');
   }
}
