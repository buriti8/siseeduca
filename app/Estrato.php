<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Estrato extends Model
{
  use Notifiable;
  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */

protected $fillable = [
  'IdEstratos','Descripcion',
    ];
}
