<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TipoEspacio extends Model
{
    public function Solicitud(){
       return $this->hasMany('App\SolicitudEspacio', 'TipoEspacio', 'id');
   }
}
