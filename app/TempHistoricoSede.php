<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TempHistoricoSede extends Model
{
    protected $table = 'temp_historico_sedes';

    public function dispositivos()
    {
        return $this->hasMany('App\Dispositivo');
    }
}
