<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class modal extends Model
{
  protected $table = 'modal';
  protected $fillable = ['DaneSede','NombreSede','DaneEstablecimiento'];

  public function temphistoricosedes()
  {
      return $this->belongsTo('App\TempHistoricoSede', 'sede_id', 'codigo_sede');
  }

  public function Establecimiento(){
      return $this->belongsTo('App\temp_historico_establecimiento', 'DaneEstablecimiento', 'codigo_establecimiento');
  }



  }
