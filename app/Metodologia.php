<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Metodologia extends Model
{
  use Notifiable;
/**
 * The attributes that are mass assignable.
 *
 * @var array
 */
protected $fillable = [
    'IdMetodologia','Descripcion',
];
}
