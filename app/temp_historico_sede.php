<?php

namespace App;
use App\Dispositivo;

use Illuminate\Database\Eloquent\Model;

class temp_historico_sede extends Model
{
    protected $table = 'temp_historico_sedes';
    public function HistoricoEstablecimiento()
    {
        return $this->belongsTo('App\temp_historico_establecimiento', 'codigo_establecimiento', 'codigo_establecimiento');
    }

    public function dispositivos()
    {
        return $this->hasMany('App\Dispositivo', 'sede_id', 'codigo_establecimiento');
    }

    public function SolicitudesIFisica(){
      return $this->hasMany('App\SolicitudIF', 'DaneSede', 'codigo_sede');
    }

    public function Sedeslegal()
     {
         return $this->hasone('App\SedesLegal', 'DaneSede', 'codigo_sede');

     }

     public function SedeEscritura()
      {
          return $this->hasone('App\SedeEscritura', 'DaneSede', 'codigo_sede');

      }

     public function SedesEspacio()
      {
          return $this->hasone('App\SedesEspacio', 'DaneSede', 'codigo_sede');
      }

      public function SedesServicio()
       {
           return $this->hasone('App\SedesServicio', 'DaneSede', 'codigo_sede');
       }

       public function SedesServicio2()
        {
            return $this->hasMany('App\SedeConectividad', 'Codigodanesede', 'codigo_sede');
        }
        public function Solicitud()
        {
            return $this->hasMany('App\Solicitud', 'sede_id', 'DaneEstablecimiento');
        }

}
