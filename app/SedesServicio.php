<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SedesServicio extends Model
{
  protected $fillable = [
'DaneSede','NombreSede','TipoEnergia', 'Acueducto','Alcantarillado','PozoSeptico','DaneMunicipio','DaneEstablecimiento'
  ];

  public function HistoricoSedes()
  {
      return $this->belongsTo('App\temp_historico_sede', 'DaneSede', 'codigo_sede');
  }
}
