<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SedesLegal extends Model
{
  protected $fillable = [
'DaneSede','NombreSede','Propietario','TipoPropietario', 'Modalidad','MatriculaInmob','NumEscritura',
'FechaEscritura','AreaLote','AreaConstruida','Plano','Foto','Coordenadas','Distancia','TipoVia','Latitud','DaneMunicipio','DaneEstablecimiento'];

public function HistoricoSedes()
{
    return $this->belongsTo('App\temp_historico_sede', 'DaneSede', 'codigo_sede');
}


}
