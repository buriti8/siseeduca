<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SedesInfra extends Model
{
  protected $fillable = [
   'codigo_departamento','nombre_departamento', 'codigo_dane_municipio','nombre_municipio','codigo_establecimiento','nombre_establecimiento','codigo_sede','nombre_sede','zona','direccion','telefono',
  ];



}
