<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HistoricoSedesNacional extends Model
{
  protected $table = 'temp_historico_sedes_nacional';

  public function HistoricoEstablecimiento()
  {
      return $this->belongsTo('App\HistoricoEstablecimientoNacional', 'codigo_establecimiento', 'codigo_establecimiento');
  }
}
