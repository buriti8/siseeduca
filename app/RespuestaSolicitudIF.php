<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RespuestaSolicitudIF extends Model
{
  protected $table = 'respuesta_solicitudIF';

  public function Solicitud(){
	     return $this->belongsTo('App\SolicitudIF', 'SolicitudId', 'id');
	}
}
