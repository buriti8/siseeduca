<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Solicitud extends Model
{
    protected $table = 'solicitudes';
    protected $fillable = [
        'id','respuesta'
    ];

    public function temphistoricosedes()
    {
        return $this->belongsTo('App\TempHistoricoSede', 'sede_id', 'codigo_sede');
   }


    public function Respuesta(){
         return $this->hasOne('App\RespuestaSolicitudIT', 'SolicitudId', 'id');
    }
    public function Establecimiento(){
        return $this->belongsTo('App\temp_historico_establecimiento', 'DaneEstablecimiento', 'codigo_establecimiento');
    }

}
