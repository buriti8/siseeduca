<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrigenDispositivo extends Model
{
    protected $fillable = ['nombre', 'descripcion'];    

    public function dispositivos()    
    {
        return $this->hasMany('App\Dispositivo');
    }
}
