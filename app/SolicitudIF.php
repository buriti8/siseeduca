<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SolicitudIF extends Model
{

	protected $fillable = [
      'id','Atendida'
  ];

  public function Establecimiento(){
      return $this->belongsTo('App\temp_historico_establecimiento', 'DaneEstablecimiento', 'codigo_establecimiento');
  }

  public function Sede(){
      // caqmpos temporal historico sede con codigo sede ---- dane sede es de solicitud if
       return $this->belongsTo('App\temp_historico_sede', 'DaneSede', 'codigo_sede');
   }

	public function Espacio(){
	     return $this->hasMany('App\SolicitudEspacio', 'SolicitudId', 'id');
	}

	public function Respuesta(){
	     return $this->hasOne('App\RespuestaSolicitudIF', 'SolicitudId', 'id');
	}

}
