<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HistoricalClassifications extends Model
{
      protected $table = 'historico_clasificaciones';

      /**
       * The attributes that are mass assignable.
       *
       * @var array
       */
      protected $fillable = [
        'periodo','cole_cod_dane','cole_inst_nombre','cole_codmpio_colegio','cole_mpio_municipio','cole_cod_depto','cole_depto_colegio',
        'cole_naturaleza','cole_grado','cole_calendario_colegio','cole_generopoblacion','matriculados_ultimos_3','evaluados_ultimos_3',
        'indice_matematicas','indice_c_naturales','indice_sociales_ciudadanas','indice_lectura_critica','indice_ingles','indice_total','cole_categoria'
      ];
}
