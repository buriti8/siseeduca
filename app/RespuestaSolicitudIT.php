<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RespuestaSolicitudIT extends Model
{
  protected $table = 'respuesta_solicitud_i_t';

  public function Solicitud(){
       return $this->belongsTo('App\Solicitud', 'SolicitudId', 'id');
  }
}
