<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TempArchivoDispositivos extends Model
{
    protected $table = 'temp_archivos_dispositivos';
}
