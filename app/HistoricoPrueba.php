<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\DB;

class HistoricoPrueba extends Model
{
    protected $table = 'historico_pruebas';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
      'cole_area_ubicacion','cole_bilingue','cole_calendario','cole_caracter','cole_cod_dane_establecimiento',
      'cole_cod_dane_sede','cole_cod_depto_ubicacion','cole_cod_mcpio_ubicacion','cole_codigo_icfes','cole_depto_ubicacion','cole_genero','cole_jornada',
      'cole_mcpio_ubicacion','cole_naturaleza','cole_nombre_establecimiento','cole_nombre_sede','cole_sede_principal','desemp_ingles','estu_cod_depto_presentacion',
      'estu_cod_mcpio_presentacion','estu_cod_reside_depto','estu_cod_reside_mcpio','estu_consecutivo','estu_depto_presentacion','estu_depto_reside','estu_estudiante',
      'estu_fechanacimiento','estu_genero','estu_inse_individual','estu_mcpio_presentacion','estu_mcpio_reside','estu_nacionalidad','estu_nse_individual',
      'estu_pais_reside','estu_privado_libertad','estu_tipodocumento','fami_cuartoshogar','fami_educacionmadre','fami_educacionpadre','fami_estratovivienda',
      'fami_personashogar','fami_tieneautomovil','fami_tienecomputador','fami_tienelavadora','fami_tieneserviciotv','periodo','punt_c_naturales','punt_global',
      'punt_ingles','punt_lectura_critica','punt_matematicas','punt_sociales_ciudadanas'
    ];

    public function Sedes()
    {
        return $this->hasMany(temp_historico_sede::class, 'cole_cod_dane_sede', 'codigo_sede');
    }

    //Inicio de funciones para gráficas por establecimiento educativo
    public static function reportGetBySedeMaterias($sede){
      return HistoricoPrueba::where('cole_cod_dane_sede', '=', $sede)
      ->groupBy('periodo')
      ->select('periodo', DB::raw('count(1) as total,
      sum(punt_c_naturales) as naturales, sum(punt_ingles) as ingles, sum(punt_matematicas) as matematicas,
      sum(punt_lectura_critica) as lectura, sum(punt_sociales_ciudadanas) as sociales'))
      ->get();
    }

    public static function reportGetBySedeMatematicas($sede){
      return HistoricoPrueba::where('cole_cod_dane_sede', '=', $sede)
      ->groupBy('periodo')
      ->select('periodo', DB::raw('count(1) as total,
      	count(CASE WHEN punt_matematicas BETWEEN 0 AND 35 THEN 1 END ) as uno_m,
      	count(CASE WHEN punt_matematicas BETWEEN 36 AND 50 THEN 1 END ) as dos_m,
      	count(CASE WHEN punt_matematicas BETWEEN 51 AND 70 THEN 1 END ) as tres_m,
      	count(CASE WHEN punt_matematicas BETWEEN 71 AND 100 THEN 1 END ) as cuatro_m'))
      ->get();
    }

    public static function reportGetBySedeNaturales($sede){
      return HistoricoPrueba::where('cole_cod_dane_sede', '=', $sede)
      ->groupBy('periodo')
      ->select('periodo', DB::raw('count(1) as total,
      	count(CASE WHEN punt_c_naturales BETWEEN 0 AND 40 THEN 1 END ) as uno_n,
      	count(CASE WHEN punt_c_naturales BETWEEN 41 AND 55 THEN 1 END ) as dos_n,
      	count(CASE WHEN punt_c_naturales BETWEEN 56 AND 70 THEN 1 END ) as tres_n,
      	count(CASE WHEN punt_c_naturales BETWEEN 71 AND 100 THEN 1 END ) as cuatro_n'))
      ->get();
    }

    public static function reportGetBySedeLectura($sede){
      return HistoricoPrueba::where('cole_cod_dane_sede', '=', $sede)
      ->groupBy('periodo')
      ->select('periodo', DB::raw('count(1) as total,
      	count(CASE WHEN punt_lectura_critica BETWEEN 0 AND 35 THEN 1 END ) as uno_l,
      	count(CASE WHEN punt_lectura_critica BETWEEN 36 AND 50 THEN 1 END ) as dos_l,
      	count(CASE WHEN punt_lectura_critica BETWEEN 51 AND 70 THEN 1 END ) as tres_l,
      	count(CASE WHEN punt_lectura_critica BETWEEN 71 AND 100 THEN 1 END ) as cuatro_l'))
      ->get();
    }

    public static function reportGetBySedeIngles($sede){
      return HistoricoPrueba::where('cole_cod_dane_sede', '=', $sede)
      ->groupBy('periodo')
      ->select('periodo', DB::raw('count(1) as total,
        count(CASE WHEN punt_ingles BETWEEN 0 AND 47 THEN 1 END ) as uno_i,
        count(CASE WHEN punt_ingles BETWEEN 48 AND 57 THEN 1 END ) as dos_i,
        count(CASE WHEN punt_ingles BETWEEN 58 AND 67 THEN 1 END ) as tres_i,
        count(CASE WHEN punt_ingles BETWEEN 68 AND 78 THEN 1 END ) as cuatro_i,
        count(CASE WHEN punt_ingles BETWEEN 79 AND 100 THEN 1 END ) as cinco_i'))
      ->get();
    }

    public static function reportGetBySedeSociales($sede){
      return HistoricoPrueba::where('cole_cod_dane_sede', '=', $sede)
      ->groupBy('periodo')
      ->select('periodo', DB::raw('count(1) as total,
        count(CASE WHEN punt_sociales_ciudadanas BETWEEN 0 AND 35 THEN 1 END ) as uno_s,
        count(CASE WHEN punt_sociales_ciudadanas BETWEEN 36 AND 50 THEN 1 END ) as dos_s,
        count(CASE WHEN punt_sociales_ciudadanas BETWEEN 51 AND 70 THEN 1 END ) as tres_s,
        count(CASE WHEN punt_sociales_ciudadanas BETWEEN 71 AND 100 THEN 1 END ) as cuatro_s'))
      ->get();
    }
    //Fin de funciones para gráficas por establecimiento educativo



}
