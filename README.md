# SI-SeEduca

Sistema de información educativo de la Secretaría de Educación de Antioquia.

## Empezando

Estas instrucciones le brindarán una copia del proyecto para el funcionamiento en su máquina local con fines de desarrollo y prueba. Para instalación y/o actualización en el servidor de producción o pruebas redirigirse [aquí](url)

### Requisitos

Para la correcta ejecución del proyecto se debe tener instalado lo siguiente:

```
Git
Servidor web
Sistema gestor de bases de datos PostgreSQL
PHP
Composer
```

### Configuración PHP

Para la correcta ejecución del proyecto se debe modificar la siguiente configuración en el archivo **php.ini** de PHP:

```
max_execution_time = 300
upload_max_filesize = 500M
post_max_size= 500M
memory_limit = 2048M
max_input_time = 500
```

Activar las siguientes extensiones:
```
extension=pdo_pgsql
extension=pgsql
```

Es posible que al finalizar la parametrización sea necesario ejecutar mediante consola el comando **php -i find /i "Configuration File"** para que sean reconocidos los cambios implementados.


### Instalación

Crear la base de datos en PostgreSQL la cual puede crearla desde línea de comandos o directamente desde pgAdmin.

```
psql -U postgres
create database siseeduca_qa;
```

Clonar el repositorio. Tener en cuenta que es un enlace a un servidor local y por lo cual es sólo accesible desde la Gobernación de Antioquia, en caso de no tener acceso al repositorio, realizar la descompresión de la carpeta que contiene el proyecto.

```
git clone http://10.0.4.112/gobant-php/siseeduca.git
```

Abrir la carpeta del proyecto renombrar el archivo “.env.example” por “.env”, luego en el editor de texto preferido, editar las credenciales de la base de datos, y guardar los cambios.
```
DB_CONNECTION=pgsql
DB_HOST=127.0.0.1
DB_PORT=5432
DB_DATABASE=siseeduca_qa
DB_USERNAME=postgres
DB_PASSWORD=""
```

Ejecutar desde la línea de comandos en la ruta del proyecto el comando para instalar el manejador de dependencias y paquetes de PHP: Composer.

```
composer install
```

Ejecutar desde la línea de comandos en la ruta del proyecto:

```
php artisan key:generate
```

Ejecutar desde la línea de comandos en la ruta del proyecto el comando que se encarga de la creación de las diferentes tablas de la base de datos, además de proporcionar algunos registros que permiten utilizar el proyecto con su funcionalidad básica.

Tener en cuenta que ejecutar este paso en alguna de las siguientes situaciones arrojará algún error:
* Servicios de postgresql detenidos o con problemas para que inicien. 
* No existencia de la base de datos previamente creada. 
* Credenciales incorrectas en el archivo .env
* Existencia de una anterior base de datos a la cual se le haya hecho ya el proceso de migración. Para solucionar este último ejecutar el comando "**php artisan migrate:fresh --seed**"

```
php artisan migrate --seed
```

Ejecutar en la base de datos **siseeduca_qa** las "[Vistas Materializadas](https://gitlab.com/buriti8/siseeduca/blob/master/database/VistasMaterializadas.sql)" en Postgresql ya sea desde la línea de comandos o desde PgAdmin.
Este archivo se encuentra en el directorio database/VistasMaterializadas.sql
```
psql -U postgres
\c siseeduca_qa

Pegar las vistas materializadas
```

Ejecutar en la base de datos **siseeduca_qa** las "[Listas de acceso](https://gitlab.com/buriti8/siseeduca/blob/master/database/ACL.sql)" en Postgresql ya sea desde la línea de comandos o desde PgAdmin.
Este archivo se encuentra en el directorio database/ACL.sql
```
psql -U postgres
\c siseeduca_qa

Pegar las listas de acceso
```

## Ejecución
Para iniciar el proyecto ejecute el siguiente comando:

```
php artisan serve
```

## Despliegue

Cuando finaliza la instalación local de SI-SeEduca, se puede ingresar con el usuario por defecto: admin.siseeduca@antioquia.gov.co, Contraseña: 123456789 

## Construido con

* [Laravel 5.7](https://laravel.com/docs/5.7) - Framework de desarrollo de PHP.

## Autores

El equipo de desarrollo ha estado conformado por profesionales en formación:

2018-1
* **Juan David Ortiz Valiente** - Ingeniero de sistemas e informático 
* **Andrés Cantillo Nava** - Ingeniero de sistemas
* **Jerson David Polo Muñoz** - Ingeniero de sistemas
* **Jhon Javer Uribe Diaz** - Ingeniero de sistemas
* **Jorge Mario Céspedes Piña** - Ingeniero electrónico

2018-2
* **Mauricio Velez Castrillón** - Ingeniero de sistemas
* **Jader Andrés Gaviria Betancur** - Tecnológo en informática
* **Edwin Bedoya Taborda** - Tecnológo de

2019-1
* **Yuly Johana Aristizábal Tobón** - Ingeniera de sistemas
* **Omar Levit Cantillo Romaña** - Ingeniero informático
* **Manuel Andrés Buriticá Yepes** - Ingeniero de sistemas