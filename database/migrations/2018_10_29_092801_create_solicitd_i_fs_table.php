<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSolicitdIFsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('solicitd_i_fs', function (Blueprint $table) {
            $table->increments('id');
            $table->biginteger('DaneEstablecimiento')->nullable();
            $table->biginteger('DaneSede')->nullable();
            $table->date('Fecha')->nullable();
            $table->text('Observacion')->nullable();
            $table->string('Usuario')->nullable();
            $table->string('Anexo')->nullable();
            $table->boolean('Atendida')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('solicitd_i_fs');
    }
}
