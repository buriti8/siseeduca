<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHistoricoEstablecimientosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('historico_establecimientos', function (Blueprint $table) {

          $table->increments('id');
          $table->tinyInteger   ('mes_corte')->nullable()->index();
          $table->year          ('anio_corte')->nullable()->index();
          $table->string        ('secretaria')->nullable();
          $table->biginteger    ('codigo_departamento')->nullable();
          $table->string        ('departamento')->nullable();
          $table->biginteger    ('codigo_dane_municipio')->nullable();
          $table->string        ('municipio')->nullable();
          $table->biginteger    ('codigo_establecimiento')->nullable()->index();
          $table->string        ('nombre_establecimiento')->nullable();
          $table->string        ('direccion')->nullable();
          $table->string        ('telefono')->nullable();
          $table->string        ('nombre_rector')->nullable();
          $table->string        ('tipo_establecimiento')->nullable();
          $table->string    ('etnias',2000)->nullable();
          $table->char      ('sector')->nullable();
          $table->string    ('genero')->nullable();
          $table->string    ('zona')->nullable();
          $table->string    ('niveles')->nullable();
          $table->string    ('jornadas')->nullable();
          $table->string    ('caracter')->nullable();
          $table->string    ('especialidad')->nullable();
          $table->string    ('licencia')->nullable();
          $table->string    ('grados')->nullable();
          $table->string    ('modelos_educativos',500)->nullable();
          $table->string    ('capacidades_excepcionales')->nullable();
          $table->string    ('discapacidades',500)->nullable();
          $table->string    ('idiomas')->nullable();
          $table->integer   ('numero_de_sedes')->nullable();
          $table->string    ('estado')->nullable();
          $table->string    ('prestador_de_servicio')->nullable();
          $table->string    ('propiedad_de_la_planta_fisica')->nullable();
          $table->string    ('resguardo')->nullable();
          $table->char      ('matricula_contratada')->nullable();
          $table->char      ('calendario')->nullable();
          $table->string    ('internado')->nullable();
          $table->string    ('estrato_socio_economico')->nullable();
          $table->string    ('correo_electronico')->nullable();
          $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('historico_establecimientos');
    }
}
