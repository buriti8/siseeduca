<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSubregionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('subregions', function (Blueprint $table) {
            $table->increments('id');
            $table->bigInteger('CodigoSubregion')->unique();
            $table->string    ('NombreSubregion',50);
            $table->integer ('CodigoDepartamento')->unsigned();
            $table->foreign   ('CodigoDepartamento')->references('CodigoDepartamento')->on('departamentos')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('subregions');
    }
}
