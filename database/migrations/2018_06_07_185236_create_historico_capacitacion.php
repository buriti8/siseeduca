<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHistoricoCapacitacion extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('historico_capacitacion', function (Blueprint $table) {

        $table->increments('id');
        $table->tinyInteger       ('mes_corte')->nullable()->index();
        $table->year              ('anio_corte')->nullable()->index();
        $table->string            ('nombre_capacitacion')->nullable()->index();
        $table->biginteger        ('cedula')->nullable();
        $table->string            ('nombre')->nullable();
        $table->string            ('apellidos')->nullable();
        $table->string            ('correo')->nullable();
        $table->string            ('telefono')->nullable();
        $table->string            ('municipio_origen')->nullable();
        $table->string            ('descripcion_capacitacion',5000)->nullable();
        $table->string            ('estado_curso')->nullable();
        $table->string            ('fecha_inicio')->nullable();
        $table->string            ('fecha_finalizacion')->nullable();
        $table->timestamps();
  });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('historico_capacitacion');
    }
}
