<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCamposServiciosEspecificos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('solicitud_espacios', function (Blueprint $table) {
            $table->boolean('RedElectrica')->nullable();
            $table->boolean('Acueducto')->nullable();
            $table->boolean('Alcantarillado')->nullable();
            $table->boolean('Piso')->nullable();
            $table->boolean('Pared')->nullable();
            $table->boolean('Puerta')->nullable();
            $table->boolean('Cerramiento')->nullable();
            $table->boolean('Cubierta')->nullable();
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('solicitud_espacios', function (Blueprint $table) {
            $table->dropColumn('RedElectrica');
            $table->dropColumn('Acueducto');
            $table->dropColumn('Alcantarillado');
            $table->dropColumn('Piso');
            $table->dropColumn('Pared');
            $table->dropColumn('Puerta');
            $table->dropColumn('Cerramiento');
            $table->dropColumn('Cubierta');
            
        });
    }
}
