<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDispositivosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dispositivos', function (Blueprint $table) {
            $table->increments('id');
            $table->biginteger("sede_id");
            $table->integer("tipo_id");
            $table->string("serial")->unique();
            $table->integer("origen_id");
            $table->string("acta")->nullable();
            $table->string("radicado")->nullable();
            $table->integer("inversion")->nullable();
            $table->string("proveedor")->nullable();
            $table->string("marca")->nullable();
            $table->string("referencia")->nullable();
            $table->date("fecha_entrega")->nullable();
            $table->date("fecha_fin_garantia")->nullable();
            $table->biginteger("numero_factura")->nullable();
            $table->integer("contrato_id")->nullable();
            $table->string("procesador")->nullable();
            $table->string("sistema_operativo")->nullable();
            $table->string("memoria_ram")->nullable();
            $table->string("disco_duro")->nullable();
            $table->boolean("estado")->nullable();
            $table->string("observaciones")->nullable();        
            $table->string("motivo")->nullable();
            $table->string("evidencia")->nullable();
            $table->date('fecha_baja')->nullable();
            $table->integer('numero_carga')->nullable();

            $table->foreign('tipo_id')->references('id')->on('tipo_dispositivos');
            $table->foreign('origen_id')->references('id')->on('origen_dispositivos');            
                                    
            $table->timestamps();            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dispositivos');
    }
}
