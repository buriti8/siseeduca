<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TempHistoricoSedesNacional extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
     public function up()
     {
         Schema::create('temp_historico_sedes_nacional', function (Blueprint $table) {

           $table->tinyInteger   ('mes_corte')->nullable()->index();
           $table->year          ('anio_corte')->nullable()->index();
           $table->string        ('secretaria')->nullable();
           $table->integer    ('codigo_departamento')->nullable();
           $table->string        ('nombre_departamento')->nullable();
           $table->biginteger    ('codigo_dane_municipio')->nullable();
           $table->string        ('nombre_municipio')->nullable();
           $table->biginteger    ('codigo_establecimiento')->nullable()->index();
           $table->string        ('nombre_establecimiento')->nullable();
           $table->biginteger    ('codigo_sede')->unique()->nullable()->index();
           $table->string        ('nombre_sede')->nullable();
           $table->string        ('zona')->nullable();
           $table->string        ('direccion')->nullable();
           $table->string        ('telefono')->nullable();
           $table->string    ('estado_sede')->nullable();
           $table->string    ('niveles')->nullable();
           $table->string    ('modelos',500)->nullable();
           $table->string    ('grados')->nullable();
           $table->timestamps();
         });
     }

     /**
      * Reverse the migrations.
      *
      * @return void
      */
     public function down()
     {
         Schema::dropIfExists('temp_historico_sedes_nacional');
     }
}
