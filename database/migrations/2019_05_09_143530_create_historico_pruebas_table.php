<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHistoricoPruebasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('historico_pruebas', function (Blueprint $table) {
          $table->increments('id');
          $table->integer('periodo')->nullable();
          $table->string('estu_tipodocumento')->nullable();
          $table->string('estu_nacionalidad')->nullable();
          $table->string('estu_genero')->nullable();
          $table->string('estu_fechanacimiento')->nullable();
          $table->string('estu_consecutivo')->nullable();
          $table->string('estu_estudiante')->nullable();
          $table->string('estu_pais_reside')->nullable();
          $table->string('estu_tieneetnia')->nullable();
          $table->string('estu_etnia')->nullable();
          $table->string('estu_limita_motriz')->nullable();
          $table->string('estu_depto_reside')->nullable();
          $table->string('estu_cod_reside_depto')->nullable();
          $table->string('estu_mcpio_reside')->nullable();
          $table->string('estu_cod_reside_mcpio')->nullable();
          $table->string('fami_estratovivienda')->nullable();
          $table->string('fami_personashogar')->nullable();
          $table->string('fami_cuartoshogar')->nullable();
          $table->string('fami_educacionpadre')->nullable();
          $table->string('fami_educacionmadre')->nullable();
          $table->string('fami_trabajolaborpadre')->nullable();
          $table->string('fami_trabajolabormadre')->nullable();
          $table->string('fami_tieneinternet')->nullable();
          $table->string('fami_tieneserviciotv')->nullable();
          $table->string('fami_tienecomputador')->nullable();
          $table->string('fami_tienelavadora')->nullable();
          $table->string('fami_tienehornomicroogas')->nullable();
          $table->string('fami_tieneautomovil')->nullable();
          $table->string('fami_tienemotocicleta')->nullable();
          $table->string('fami_tieneconsolavideojuegos')->nullable();
          $table->string('fami_numlibros')->nullable();
          $table->string('fami_comelechederivados')->nullable();
          $table->string('fami_comecarnepescadohuevo')->nullable();
          $table->string('fami_comecerealfrutoslegumbre')->nullable();
          $table->string('fami_situacioneconomica')->nullable();
          $table->string('estu_dedicacionlecturadiaria')->nullable();
          $table->string('estu_dedicacioninternet')->nullable();
          $table->string('estu_horassemanatrabaja')->nullable();
          $table->string('estu_tiporemuneracion')->nullable();
          $table->integer('cole_codigo_icfes')->nullable();
          $table->biginteger('cole_cod_dane_establecimiento')->nullable();
          $table->string('cole_nombre_establecimiento')->nullable();
          $table->string('cole_genero')->nullable();
          $table->string('cole_naturaleza')->nullable();
          $table->string('cole_calendario')->nullable();
          $table->string('cole_bilingue')->nullable();
          $table->string('cole_caracter')->nullable();
          $table->biginteger('cole_cod_dane_sede')->nullable();
          $table->string('cole_nombre_sede')->nullable();
          $table->string('cole_sede_principal')->nullable();
          $table->string('cole_area_ubicacion')->nullable();
          $table->string('cole_jornada')->nullable();
          $table->integer('cole_cod_mcpio_ubicacion')->nullable();
          $table->string('cole_mcpio_ubicacion')->nullable();
          $table->integer('cole_cod_depto_ubicacion')->nullable();
          $table->string('cole_depto_ubicacion')->nullable();
          $table->string('estu_privado_libertad')->nullable();
          $table->string('estu_cod_mcpio_presentacion')->nullable();
          $table->string('estu_mcpio_presentacion')->nullable();
          $table->string('estu_depto_presentacion')->nullable();
          $table->integer('estu_cod_depto_presentacion')->nullable();
          $table->integer('punt_lectura_critica')->nullable();
          $table->string('percentil_lectura_critica')->nullable();
          $table->string('desemp_lectura_critica')->nullable();
          $table->integer('punt_matematicas')->nullable();
          $table->string('percentil_matematicas')->nullable();
          $table->string('desemp_matematicas')->nullable();
          $table->integer('punt_c_naturales')->nullable();
          $table->string('percentil_c_naturales')->nullable();
          $table->string('desemp_c_naturales')->nullable();
          $table->integer('punt_sociales_ciudadanas')->nullable();
          $table->string('percentil_sociales_ciudadanas')->nullable();
          $table->string('desemp_sociales_ciudadanas')->nullable();
          $table->integer('punt_ingles')->nullable();
          $table->string('percentil_ingles')->nullable();
          $table->string('desemp_ingles')->nullable();
          $table->integer('punt_global')->nullable();
          $table->string('percentil_global')->nullable();
          $table->string('estu_nse_establecimiento')->nullable();
          $table->float('estu_inse_individual')->nullable();
          $table->string('estu_nse_individual')->nullable();
          $table->string('estu_estadoinvestigacion')->nullable();
          $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('historico_pruebas');
    }
}
