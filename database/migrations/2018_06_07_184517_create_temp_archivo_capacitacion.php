<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTempArchivoCapacitacion extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('temp_archivo_capacitacion', function (Blueprint $table) {


        $table->biginteger        ('cedula')->nullable();
        $table->string            ('nombre')->nullable();
        $table->string            ('apellidos')->nullable();
        $table->string            ('correo')->nullable();
        $table->string            ('telefono')->nullable();
        $table->string            ('municipio_origen')->nullable();
        $table->string            ('descripcion_capacitacion')->nullable();
        $table->string            ('estado_curso')->nullable();
        $table->string            ('fecha_inicio')->nullable();
        $table->string            ('fecha_finalizacion')->nullable();

  });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('temp_archivo_capacitacion');
    }
}
