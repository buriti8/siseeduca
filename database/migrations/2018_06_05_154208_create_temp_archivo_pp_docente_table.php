<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTempArchivoPpDocenteTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('temp_archivo_pp_docente', function (Blueprint $table) {
                  $table->biginteger        ('codigo_empleado')->nullable();
                   $table->integer        ('codigo_ciudad_nacimiento')->nullable();
                   $table->biginteger    ('numero_vinculacion')->nullable();
                   $table->string    ('nombre1')->nullable();
                   $table->string    ('nombre2')->nullable();
                   $table->string    ('apellido1')->nullable();
                   $table->string    ('apellido2')->nullable();
                   $table->string    ('cargo_empresa')->nullable();
                   $table->string    ('grado')->nullable();
                   $table->biginteger    ('basico')->nullable();
                   $table->integer    ('codigo_esquema')->nullable();
                   $table->integer    ('codigo_area')->nullable();
                   $table->string    ('codigo_dane')->nullable();
                   $table->biginteger    ('codigo_centro_costo')->nullable();
                   $table->string    ('centro_costo')->nullable();
                  $table->string    ('codigo_dependencia')->nullable();
                  $table->string    ('ciudad')->nullable();
                  $table->string    ('codigo_en_cargo')->nullable();
                  $table->string    ('codigo_dependencia_en_cargo')->nullable();
                  $table->string    ('grado_en_cargo')->nullable();
                  $table->string    ('codigo_centro_costo_en_cargo')->nullable();
                  $table->string    ('fecha_nacimiento')->nullable();
                  $table->string    ('fecha_ingreso_empresa')->nullable();
                  $table->char    ('continuidad')->nullable();
                  $table->string    ('continuidad_fecha')->nullable();
                  $table->integer    ('codigo_nivel_contratacion')->nullable();
                  $table->string    ('nivel_contratacion')->nullable();
                  $table->string    ('cod_area_educativa')->nullable();
                  $table->string    ('area_educativa')->nullable();
                  $table->integer    ('codigo_vinculacion_tipo')->nullable();
                  $table->string    ('vinculacion_tipo')->nullable();
                  $table->integer    ('codigo_sucursal')->nullable();
                  $table->integer    ('codigo_situacion_laboral')->nullable();
                  $table->string    ('situacion_laboral')->nullable();
                  $table->string    ('ref_num_vinculacion')->nullable();
                  $table->integer    ('codigo_nivel_educacion')->nullable();
                  $table->string    ('nivel_educacion')->nullable();
                  $table->char      ('sexo')->nullable();
                  $table->integer   ('codigo_cargo_empresa')->nullable();
                  $table->string    ('otra_area_educativa_tecnica')->nullable();
                  $table->string      ('fecha_creacion')->nullable();
                  $table->integer   ('codigo_vinculacion_novedad')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('temp_archivo_pp_docente');
    }
}
