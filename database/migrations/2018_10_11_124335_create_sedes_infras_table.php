<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSedesInfrasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sedes_infras', function (Blueprint $table) {
            $table->increments('id');
            $table->String    ('Municipio')->nullable();
            $table->bigInteger('DaneEstablecimiento')->nullable();
            $table->String    ('NombreEstablecimiento')->nullable();
            $table->bigInteger ('DaneSede')->unique();
            $table->String   ('NombreSede')->nullable();
            $table->String    ('Zona')->nullable();
            $table->String    ('Ubicacion')->nullable();
            $table->String   ('Telefono')->nullable();
            $table->Integer    ('NumeroAlumnos')->nullable();
            $table->String    ('DistanciaMunicipio')->nullable();
            $table->String    ('TipoVia')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sedes_infras');
    }
}
