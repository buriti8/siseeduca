<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTempArchivoEoPcargosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('temp_archivo_eo_pcargos', function (Blueprint $table) {
          $table->string        ('subregion')->nullable();
          $table->string        ('municipio')->nullable();
          $table->string        ('codigo_establecimiento')->nullable();
          $table->string        ('nombre_establecimiento')->nullable();
          $table->biginteger    ('codigo_dane_establecimiento')->nullable();
          $table->string        ('codigo_sede')->nullable();
          $table->string        ('nombre_sede')->nullable();
          $table->string    ('codigo_dane_sede')->nullable();
          $table->string        ('codigo_area')->nullable();
          $table->string        ('area')->nullable();
          $table->integer    ('ocurrencia_cargo')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('temp_archivo_eo_pcargos');
    }
}
