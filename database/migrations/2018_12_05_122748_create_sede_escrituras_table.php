<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSedeEscriturasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sede_escrituras', function (Blueprint $table) {
            $table->increments('id');
            //$table->bigInteger   ('DaneMunicipio')->nullable();
            //$table->bigInteger   ('DaneEstablecimiento')->nullable();
            $table->bigInteger   ('DaneSede')->nullable();
            $table->String       ('NombreSede')->nullable();
            $table->String       ('Propietario')->nullable();
            $table->String       ('MatriculaInmob')->nullable();
            $table->String       ('NumEscritura')->nullable();
            $table->String       ('FechaEscritura')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sede_escrituras');
    }
}
