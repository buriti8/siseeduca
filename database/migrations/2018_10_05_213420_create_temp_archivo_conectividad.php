<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTempArchivoConectividad extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

      Schema::create('temp_archivo_conectividad', function (Blueprint $table) {

      $table->biginteger    ('codigo_dane_sede')->nullable();
$table->string        ('nombre_sede_educativa')->nullable();
$table->string        ('direccion')->nullable();
$table->string    ('telefono')->nullable();
$table->string        ('zona')->nullable();
$table->string        ('departamento')->nullable();
$table->string        ('municipio')->nullable();
$table->string        ('programa_origen_de_los_recursos')->nullable();
$table->string        ('numero_de_contrato')->nullable();
$table->string        ('operador')->nullable();
$table->string        ('tecnologia_ultima_milla')->nullable();
$table->string        ('ancho_de_banda')->nullable();
$table->string        ('meses_de_servicio')->nullable();
$table->string        ('fecha_inicio_servicio')->nullable();
$table->string        ('fecha_fin_servicio')->nullable();

});
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */

    public function down()
    {
      Schema::dropIfExists('temp_archivo_conectividad');

    }
}
