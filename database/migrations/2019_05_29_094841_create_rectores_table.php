<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRectoresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rectores', function (Blueprint $table) {
            $table->increments('id');
            $table->biginteger('codigo_municipio')->nullable();
            $table->biginteger('codigo_establecimiento')->nullable();
            $table->String('nombre')->nullable();
            $table->String('apellido')->nullable();
            $table->String('celular')->nullable();
            $table->String('telefono')->nullable();
            $table->String('correo')->nullable();
            $table->String('correo_establecimiento')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rectores');
    }
}
