<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSedesEspaciosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sedes_espacios', function (Blueprint $table) {
            $table->increments('id');
            $table->biginteger    ('DaneSede')->unique();
            //$table->string        ('NombreSede')->nullable();
            $table->integer        ('AulasPreescolar')->nullable();
            $table->integer        ('AulasPrimaria')->nullable();
            $table->integer        ('AulasSecundaria')->nullable();
            $table->integer        ('AulasEspaciales')->nullable();
            $table->integer        ('Biblioteca')->nullable();
            $table->integer        ('AulasSistemas')->nullable();
            $table->integer        ('AulasBilinguismo')->nullable();
            $table->integer        ('Laboratorio')->nullable();
            $table->integer        ('AulasTalleres')->nullable();
            $table->integer        ('AulasMultiples')->nullable();
            $table->integer        ('Cocinas')->nullable();
            $table->integer        ('Comedores')->nullable();
            $table->integer        ('SantiriosHombres')->nullable();
            $table->integer        ('SanitariosMujeres')->nullable();
            $table->integer        ('LavamanosHombres')->nullable();
            $table->integer        ('LavamanosMujeres')->nullable();
            $table->integer        ('Orinales')->nullable();
            $table->integer        ('Vivienda')->nullable();
            $table->integer        ('Canchas')->nullable();
            $table->integer        ('PlacasMulti')->nullable();
            $table->integer        ('JuegosInfantiles')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sedes_espacios');
    }
}
