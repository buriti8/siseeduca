<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTempArchivoEoDirdocTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('temp_archivo_eo_dirdoc', function (Blueprint $table) {
          $table->string        ('subregion')->nullable();
          $table->string        ('municipio')->nullable();
          $table->string        ('codigo_establecimiento')->nullable();
          $table->string        ('nombre_establecimiento')->nullable();
          $table->biginteger    ('codigo_dane_establecimiento')->nullable();
          $table->integer    ('codigo_cargo_empresa')->nullable();
          $table->string    ('cargo_empresa')->nullable();
          $table->integer    ('ocurrencia_cargo')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('temp_archivo_eo_dirdoc');
    }
}
