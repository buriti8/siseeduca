<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJefesNucleosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('jefes_nucleos', function (Blueprint $table) {
            $table->increments('id');
            $table->biginteger('codigo_municipio')->nullable();
           $table->String('nombre')->nullable();
           $table->String('apellido')->nullable();
           $table->String('celular')->nullable();
           $table->String('telefono')->nullable();
           $table->String('correo')->nullable();
           $table->String('cargo')->nullable();
           $table->String('direccion')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('jefes_nucleos');
    }
}
