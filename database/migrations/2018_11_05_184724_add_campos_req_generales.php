<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCamposReqGenerales extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('solicitud_i_fs', function (Blueprint $table) {
            $table->boolean('Lote')->nullable();
            $table->boolean('DisenioTecnico')->nullable();
            $table->boolean('ReposicionTotal')->nullable();
            $table->boolean('TerminacionInconclusa')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('solicitud_i_fs', function (Blueprint $table) {
            $table->dropColumn('Lote');
            $table->dropColumn('DisenioTecnico');
            $table->dropColumn('ReposicionTotal');
            $table->dropColumn('TerminacionInconclusa');
        });
    }
}
