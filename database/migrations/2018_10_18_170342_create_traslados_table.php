<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTrasladosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('traslados', function (Blueprint $table) {
            $table->increments('id');
            $table->biginteger('sede_origen_id');
            $table->biginteger('sede_destino_id');            
            $table->integer('motivo');
            $table->text('descripcion')->nullable(); 

            $table->integer('dispositivo_id');
            $table->foreign('dispositivo_id')->references('id')->on('dispositivos');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('traslados');
    }
}
