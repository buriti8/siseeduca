<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTempHistoricoMesa extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('temp_historico_mesa', function (Blueprint $table) {

      $table->tinyInteger   ('mes_corte')->nullable()->index();
      $table->year          ('anio_corte')->nullable()->index();
      $table->biginteger    ('codigo_dane')->nullable();
      $table->string        ('nombre')->nullable();
      $table->string        ('localidad')->nullable();
      $table->string        ('tecnologia')->nullable();
      $table->biginteger    ('id_caso')->nullable();
      $table->biginteger    ('motive_id')->nullable();
      $table->string        ('tipificacion')->nullable();
      $table->string        ('tipificacion1')->nullable();
      $table->string        ('tipificacion2')->nullable();
      $table->string        ('solicitud',5000)->nullable();
      $table->string        ('solucion',5000)->nullable();
      $table->string        ('estado_caso')->nullable();
      $table->string        ('nombre_causal')->nullable();
      $table->string         ('tipo_solucion')->nullable();
      $table->string          ('tiempo_gestion')->nullable();
      $table->string          ('fecha_creacion')->nullable();
      $table->string         ('fecha_real_solucion')->nullable();
      $table->string        ('grupo_soluciona')->nullable();
      $table->string        ('usuario_cierra')->nullable();
      $table->string        ('impacto')->nullable();
      $table->biginteger    ('contrato_escuela')->nullable();
      $table->string        ('convenio')->nullable();
      $table->string        ('cumplimiento_garantia')->nullable();
      $table->timestamps();
    });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('temp_historico_mesa');
    }
}
