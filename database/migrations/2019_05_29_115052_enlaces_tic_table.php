<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class EnlacesTicTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('enlaces_tic', function (Blueprint $table) {
            $table->increments('id');
            $table->biginteger('codigo_municipio')->nullable();
            $table->String('nombre_enlacetic')->nullable();
            $table->String('apellido_enlacetic')->nullable();
            $table->String('celular_enlacetic')->nullable();
            $table->String('telefono_enlacetic')->nullable();
            $table->String('correo_enlacetic')->nullable();
            $table->String('cargo_enlacetic')->nullable();
            $table->String('direccion_enlacetic')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('enlaces_tic');
    }
}
