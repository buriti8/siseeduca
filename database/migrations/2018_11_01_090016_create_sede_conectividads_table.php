<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSedeConectividadsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sede_conectividads', function (Blueprint $table) {
            $table->increments('id');
            $table->biginteger    ('Codigodanesede')->nullable();
            //$table->string        ('Nombresedeeducativa')->nullable();
            //$table->string        ('Direccion')->nullable();
            //$table->string        ('Telefono')->nullable();
            //$table->string        ('Zona')->nullable();
            //$table->string        ('Departamento')->nullable();
            //$table->string        ('Municipio')->nullable();
            $table->string        ('Programaorigendelosrecursos')->nullable();
            $table->string        ('Numerodecontrato')->nullable();
            $table->string        ('Operador')->nullable();
            $table->string        ('Anchodebanda')->nullable();
            $table->string        ('Fechainicioservicio')->nullable();
            $table->string        ('Fechafinservicio')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sede_conectividads');
    }
}
