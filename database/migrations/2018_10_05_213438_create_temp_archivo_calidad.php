<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTempArchivoCalidad extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('temp_archivo_calidad', function (Blueprint $table) {

      $table->biginteger    ('codigo_dane_sede')->nullable();
$table->string        ('sede_educativa')->nullable();
$table->string        ('tipo_de_canal')->nullable();
$table->string        ('disponibilidad_del_servicio')->nullable();
$table->string        ('descuento_disponibilidad_del_servicio')->nullable();
$table->string        ('latencia')->nullable();
$table->string        ('descuento_latencia')->nullable();
$table->string        ('efectividad_instalacion')->nullable();
$table->string        ('descuento_efectividad_instalacion')->nullable();
$table->string        ('velocidad_de_transferencia')->nullable();
$table->string        ('descuento_velocidad_de_transferencia')->nullable();

});
}

/**
 * Reverse the migrations.
 *
 * @return void
 */

  public function down()
  {
    Schema::dropIfExists('temp_archivo_calidad');

  }
}
