<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGradosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('grados', function (Blueprint $table) {
            $table->increments('id');
            $table->bigInteger('IdGrado')->unique();
            $table->string    ('Grado',50);
            $table->integer   ('LimiteEdad')->nullable()->unsigned();
            $table->integer   ('IdNivel')->nullable()->unsigned();
            $table->integer   ('IdNivelMediaTotal')->nullable()->unsigned();
            $table->integer   ('IdNivelCine')->nullable()->unsigned();
            $table->foreign   ('IdNivel')->references('IdNivel')->on('nivels')->onDelete('cascade');
            $table->foreign   ('IdNivelMediaTotal')->references('IdNivelMediaTotal')->on('nivel_media_totals')->onDelete('cascade');
            $table->foreign   ('IdNivelCine')->references('IdNivelCine')->on('nivel_cines')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('grados');
    }
}
