<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDistanciaToSedesLegals extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::table('sedes_legals', function (Blueprint $table) {
          $table->String   ('Coordenadas')->nullable();
          $table->String   ('Distancia')->nullable();
          $table->String   ('TipoVia')->nullable();

      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

    }
}
