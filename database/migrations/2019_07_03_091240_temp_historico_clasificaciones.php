<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TempHistoricoClasificaciones extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('temp_historico_clasificaciones', function (Blueprint $table) {
            $table->string('periodo')->nullable();
            $table->string('cole_cod_dane')->nullable();
            $table->string('cole_inst_nombre')->nullable();
            $table->string('cole_codmpio_colegio')->nullable();
            $table->string('cole_mpio_municipio')->nullable();
            $table->string('cole_cod_depto')->nullable();
            $table->string('cole_depto_colegio')->nullable();
            $table->string('cole_naturaleza')->nullable();
            $table->string('cole_grado')->nullable();
            $table->string('cole_calendario_colegio')->nullable();
            $table->string('cole_generopoblacion')->nullable();
            $table->string('matriculados_ultimos_3')->nullable();
            $table->string('evaluados_ultimos_3')->nullable();
            $table->string('indice_matematicas')->nullable();
            $table->string('indice_c_naturales')->nullable();
            $table->string('indice_sociales_ciudadanas')->nullable();
            $table->string('indice_lectura_critica')->nullable();
            $table->string('indice_ingles')->nullable();
            $table->string('indice_total')->nullable();
            $table->string('cole_categoria')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
