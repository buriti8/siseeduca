<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTempArchivoSedesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('temp_archivo_sedes', function (Blueprint $table) {

          $table->string    ('secretaria')->nullable();
          $table->biginteger    ('codigo_departamento')->nullable();
          $table->string        ('nombre_departamento')->nullable();
          $table->biginteger    ('codigo_dane_municipio')->nullable();
          $table->string        ('nombre_municipio')->nullable();
          $table->biginteger    ('codigo_establecimiento')->nullable();
          $table->string        ('nombre_establecimiento')->nullable();
          $table->biginteger    ('codigo_sede')->nullable();
          $table->string        ('nombre_sede')->nullable();
          $table->string        ('zona')->nullable();
          $table->string        ('direccion')->nullable();
          $table->string        ('telefono')->nullable();
          $table->string    ('estado_sede')->nullable();
          $table->string    ('niveles')->nullable();
          $table->string    ('modelos',500)->nullable();
          $table->string    ('grados')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('temp_archivo_sedes');
    }
}
