<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRespuestaSolicitudIFsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('respuesta_solicitudIF', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('SolicitudId')->nullable();
            $table->text('Mensaje')->nullable();
            $table->date('Fecha')->nullable();
            $table->boolean('VistoRector')->nullable();
            $table->boolean('VistoSecretario')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('respuesta_solicitudIF');
    }
}
