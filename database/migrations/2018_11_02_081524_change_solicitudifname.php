<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeSolicitudifname extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('solicitd_i_fs', function (Blueprint $table) {
            Schema::rename('solicitd_i_fs', 'solicitud_i_fs');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('solicitd_i_fs', function (Blueprint $table) {
            Schema::rename('solicitud_i_fs', 'solicitd_i_fs');
        });
    }
}
