<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNivelMediaTotalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('nivel_media_totals', function (Blueprint $table) {
            $table->increments('id');
            $table->bigInteger('IdNivelMediaTotal')->unique();
            $table->string    ('Descripcion',50);
            $table->integer   ('LimiteEdad');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('nivel_media_totals');
    }
}
