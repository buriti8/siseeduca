<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTempArchivoMesa2 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('temp_archivo_mesa2', function (Blueprint $table) {


        $table->biginteger    ('id_caso')->nullable();
        $table->string    ('codigo_dane')->nullable();
        $table->string        ('nombre_sede')->nullable();
        $table->string        ('localidad')->nullable();
        $table->string        ('tecnologia')->nullable();
        $table->string       ('telefono_contacto')->nullable();
        $table->string        ('correo_contacto')->nullable();
        $table->string        ('zona_escuela')->nullable();
        $table->string        ('departamento')->nullable();
        $table->string        ('fecha_creacion')->nullable();
        $table->string        ('nombre_causal')->nullable();
        $table->string        ('tipo_solucion')->nullable();
        $table->string        ('motivo')->nullable();
        $table->string        ('fecha_solucion')->nullable();
        $table->string        ('cerrado_primer_contacto')->nullable();
        $table->string        ('impacto')->nullable();
        $table->string        ('estado_caso')->nullable();
        $table->string        ('tipificacion')->nullable();
        $table->string        ('tipificacion1')->nullable();
        $table->string        ('tipificacion2')->nullable();
        $table->string        ('grupo_soluciona')->nullable();
        $table->string          ('susccodi')->nullable();
        $table->string        ('convenio')->nullable();
        $table->string        ('quien_lo_tiene')->nullable();
        $table->string        ('correo_contacto2')->nullable();
        $table->string        ('solicitud',5000)->nullable();
        $table->string        ('solucion',5000)->nullable();
        $table->string         ('n1')->nullable();
        $table->string         ('n2')->nullable();
        $table->string        ('cumplimiento')->nullable();


      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('temp_archivo_mesa2');
    }
}
