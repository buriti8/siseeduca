<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContratosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contratos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('numero_contrato');
            $table->text('objeto_contrato');
            $table->integer('tipo_contrato');                                                                                         
            $table->date('fecha_acta_inicio');                                                                                         
            $table->date('fecha_terminacion_contrato');                                                                                         
            $table->string('nit_contratista');                                                                                         
            $table->string('nombre_contratista');                                                                                         
            $table->string('telefono_contratista');                                                                                         
            $table->bigInteger('valor_total_contrato');                                                                                         
            $table->string('dependencia_contratante');                                                                                         
            $table->string('telefono_mesa_ayuda');                                                                                         
            $table->text('observaciones');                                                                                         
            
            $table->integer('proyecto_id');
            $table->foreign('proyecto_id')->references('id')->on('proyectos');                            
            
            $table->timestamps();                    
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contratos');
    }
}
