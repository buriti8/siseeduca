<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DirectorioMunicipalTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('directorio_municipal', function (Blueprint $table) {
            $table->increments('id');
            $table->biginteger('codigo_municipio')->nullable();
            $table->String('nombre_alcalde')->nullable();
            $table->String('apellido_alcalde')->nullable();
            $table->String('celular_alcalde')->nullable();
            $table->String('telefono_alcalde')->nullable();
            $table->String('correo_alcalde')->nullable();
            $table->String('correo_alcaldia')->nullable();
            $table->String('direccion_alcaldia')->nullable();
            $table->String('nombre_secretario')->nullable();
            $table->String('apellido_secretario')->nullable();
            $table->String('celular_secretario')->nullable();
            $table->String('telefono_secretario')->nullable();
            $table->String('correo_secretario')->nullable();
            $table->String('correo_secretariaeducacion')->nullable();
            $table->String('cargo_secretario')->nullable();
            $table->String('direccion_secretario')->nullable();
            $table->String('nombre_adminsimat')->nullable();
            $table->String('apellido_adminsimat')->nullable();
            $table->String('celular_adminsimat')->nullable();
            $table->String('telefono_adminsimat')->nullable();
            $table->String('correo_adminsimat')->nullable();
            $table->String('cargo_adminsimat')->nullable();
            $table->String('direccion_adminsimat')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('directorio_Municipal');
    }
}
