<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSedesServiciosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sedes_servicios', function (Blueprint $table) {
            $table->increments('id');
            $table->bigInteger('DaneSede')->unique();;
            $table->String   ('TipoEnergia')->nullable();
            $table->String    ('Acueducto')->nullable();
            $table->String    ('AguaPotable')->nullable();
            $table->String    ('PlantaTratamiento')->nullable();
            $table->String    ('Alcantarillado')->nullable();
            $table->String    ('PozoSeptico')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sedes_servicios');
    }
}
