<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRectorDirectorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rector_directors', function (Blueprint $table) {
            $table->increments  ('id');
              $table->bigInteger('CC')->unique();
              $table->String    ('Nombre',100)->nullable();
              $table->bigInteger('Nit')->nullable();
              $table->Integer   ('TelefonoFijo')->nullable();
              $table->String    ('PaginaWeb',100)->nullable();
              $table->String    ('Email',100)->nullable();
              $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rector_directors');
    }
}
