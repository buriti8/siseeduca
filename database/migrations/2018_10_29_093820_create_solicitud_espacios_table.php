<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSolicitudEspaciosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('solicitud_espacios', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('SolicitudId')->nullable();
            $table->integer('TipoEspacio')->nullable();
            $table->integer('Cantidad')->nullable();
            $table->integer('TipoSolicitud')->nullable();
            $table->integer('EstadoEspacio')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('solicitud_espacios');
    }
}
