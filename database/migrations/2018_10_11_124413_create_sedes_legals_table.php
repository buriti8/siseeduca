<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSedesLegalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sedes_legals', function (Blueprint $table) {
            $table->increments('id');
            $table->bigInteger('DaneSede')->unique();
            //$table->String   ('Propietario')->nullable();
            $table->String    ('Modalidad')->nullable();
            //$table->String    ('MatriculaInmob')->nullable();
            //$table->String   ('NumEscritura')->nullable();
            //$table->String    ('FechaEscritura')->nullable();
            $table->String    ('AreaLote')->nullable();
            $table->String    ('AreaConstruida')->nullable();
            $table->String    ('Plano')->nullable();
            $table->String    ('Foto')->nullable();


            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sedes_legals');
    }
}
