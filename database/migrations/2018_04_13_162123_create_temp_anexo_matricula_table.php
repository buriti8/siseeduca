<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTempAnexoMatriculaTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('temp_anexo_matricula', function (Blueprint $table) {
          $table->year    ('ano_inf')->nullable();
          $table->integer    ('mun_codigo')->nullable();
          $table->biginteger    ('codigo_dane')->nullable();
          $table->biginteger    ('dane_anterior')->nullable();
          $table->biginteger    ('cons_sede')->nullable();
          $table->integer    ('tipo_documento')->nullable();
          $table->string    ('nro_documento',50)->nullable();
          $table->integer    ('exp_depto')->nullable();
          $table->integer    ('exp_mun')->nullable();
          $table->string    ('apellido1',50)->nullable();
          $table->string    ('apellido2',50)->nullable();
          $table->string    ('nombre1',50)->nullable();
          $table->string    ('nombre2',50)->nullable();
          $table->string    ('direccion_residencia',100)->nullable();
          $table->string    ('tel',50)->nullable();
          $table->integer    ('res_depto')->nullable();
          $table->integer    ('res_mun')->nullable();
          $table->integer    ('estrato')->nullable();
          $table->float      ('sisben',5,2)->nullable();
          $table->string     ('fecha_nacimiento')->nullable();;
          $table->integer    ('nac_depto')->nullable();
          $table->integer    ('nac_mun')->nullable();
          $table->char    ('genero',2)->nullable();
          $table->integer    ('pob_vict_conf')->nullable();
          $table->integer    ('dpto_exp')->nullable();
          $table->integer    ('mun_exp')->nullable();
          $table->char    ('proviene_sector_priv',2);
          $table->char    ('proviene_otr_mun',2)->nullable();
          $table->integer    ('tipo_discapacidad')->nullable();
          $table->integer    ('cap_exc')->nullable();
          $table->integer    ('etnia')->nullable();
          $table->integer    ('res')->nullable();
          $table->string    ('ins_familiar',100)->nullable();
          $table->integer    ('tipo_jornada')->nullable();
          $table->integer    ('caracter')->nullable();
          $table->integer    ('especialidad')->nullable();
          $table->integer    ('grado')->nullable();
          $table->string    ('grupo',50)->nullable();
          $table->string    ('metodologia',50)->nullable();
          $table->char    ('matricula_contratada',2)->nullable();
          $table->char    ('repitente',2)->nullable();
          $table->char    ('nuevo',2)->nullable();
          $table->integer    ('sit_acad_ano_ant')->nullable();
          $table->integer    ('con_alum_ano_ant')->nullable();
          $table->integer    ('fue_recu')->nullable();
          $table->integer    ('zon_alu')->nullable();
          $table->char    ('cab_familia',2)->nullable();
          $table->char    ('ben_mad_flia',2)->nullable();
          $table->char    ('ben_vet_fp',2)->nullable();
          $table->char    ('ben_her_nac',2)->nullable();
          $table->integer    ('codigo_internado')->nullable();
          $table->integer    ('codigo_valoracion_1')->nullable();
          $table->integer    ('codigo_valoracion_2')->nullable();
          $table->string    ('num_convenio',50)->nullable();
          $table->biginteger    ('per_id')->nullable();
          $table->string    ('apoyo_academico_especial',10)->nullable();
          $table->string    ('srpa',10)->nullable();

        });

/*
*Creación de indice sobre la columna fecha Corte
    DB::statement('ALTER TABLE matricula_historico ADD FULLTEXT full(FECHA_CORTE)');
*/


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('temp_anexo_matricula');
    }
}
