<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCamposToSedesEspaciosdos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::table('sedes_espacios', function (Blueprint $table) {
          $table->String   ('SanitariosReducida')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::table('sedes_espacios', function($table) {
          $table->dropColumn('SanitariosReducida');
      });
    }
}
