<?php

use Illuminate\Database\Seeder;

class ZonaSeeder extends Seeder
{
  /**
  * Run the database seeds.
  *
  * @return void
  */
  public function run()
  {
    $Zona = App\Zona::create(['IdZona' => 1, 'Descripcion' => 'URBANA']);
    $Zona = App\Zona::create(['IdZona' => 2, 'Descripcion' => 'RURAL']);
  }
}
