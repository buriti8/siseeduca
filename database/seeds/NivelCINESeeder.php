<?php

use Illuminate\Database\Seeder;

class NivelCineSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      $items = [

        ['IdNivelCine' =>0,'Descripcion' =>'CINE 0'],
        ['IdNivelCine' =>1,'Descripcion' =>'CINE 1'],
        ['IdNivelCine' =>2,'Descripcion' =>'CINE 2'],
        ['IdNivelCine' =>3,'Descripcion' =>'CINE 3'],
     ];

     foreach ($items as $item) {
            \App\NivelCine::create($item);
        }
    }
}
