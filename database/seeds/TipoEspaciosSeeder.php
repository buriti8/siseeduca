<?php

use Illuminate\Database\Seeder;

class TipoEspaciosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

      $items = [

        ['id' =>1, 'NombreEspacio' =>'AULAS TRANSICIÓN/PRESCOLAR','DescripcionEspacio' =>'AULAS'],
        ['id' =>2, 'NombreEspacio' =>'AULAS PRIMARIA','DescripcionEspacio' =>'AULAS'],
        ['id' =>3, 'NombreEspacio' =>'AULAS SECUNDARIA','DescripcionEspacio' =>'AULAS'],
        ['id' =>4, 'NombreEspacio' =>'AULAS ESPECIALES','DescripcionEspacio' =>'AULAS'],
        ['id' =>5, 'NombreEspacio' =>'BIBLIOTECA','DescripcionEspacio' =>'AULAS'],
        ['id' =>6, 'NombreEspacio' =>'AULAS DE SISTEMAS','DescripcionEspacio' =>'AULAS'],
        ['id' =>7, 'NombreEspacio' =>'AULAS DE BILINGÜISMO','DescripcionEspacio' =>'AULAS'],
        ['id' =>8, 'NombreEspacio' =>'LABORATORIOS','DescripcionEspacio' =>'AULAS'],
        ['id' =>9, 'NombreEspacio' =>'AULAS TALLERES','DescripcionEspacio' =>'AULAS'],
        ['id' =>10, 'NombreEspacio' =>'AULAS MÚLTIPLES','DescripcionEspacio' =>'AULAS'],
        ['id' =>11, 'NombreEspacio' =>'COCINAS','DescripcionEspacio' =>'AULAS'],
        ['id' =>12, 'NombreEspacio' =>'COMEDORES','DescripcionEspacio' =>'AULAS'],
        ['id' =>13, 'NombreEspacio' =>'SANITARIOS MUJERES','DescripcionEspacio' =>'BATERIAS SANITARIAS'],
        ['id' =>14, 'NombreEspacio' =>'SANITARIOS HOMBRES','DescripcionEspacio' =>'BATERIAS SANITARIAS'],
        ['id' =>15, 'NombreEspacio' =>'SANITARIOS MOVILIDAD REDUCIDA','DescripcionEspacio' =>'BATERIAS SANITARIAS'],
        ['id' =>16, 'NombreEspacio' =>'LAVAMANOS MUJERES','DescripcionEspacio' =>'BATERIAS SANITARIAS'],
        ['id' =>17, 'NombreEspacio' =>'LAVAMANOS HOMBRES','DescripcionEspacio' =>'BATERIAS SANITARIAS'],
        ['id' =>18, 'NombreEspacio' =>'ORINALES','DescripcionEspacio' =>'BATERIAS SANITARIAS'],
        ['id' =>19, 'NombreEspacio' =>'VIVIENDA O ALCOBA - DOCENTE','DescripcionEspacio' =>'ESPACIOS COMPLEMENTARIOS'],
        ['id' =>20, 'NombreEspacio' =>'CANCHAS','DescripcionEspacio' =>'ESPACIOS COMPLEMENTARIOS'],
        ['id' =>21, 'NombreEspacio' =>'PLACAS MULTIFUNCIONALES','DescripcionEspacio' =>'ESPACIOS COMPLEMENTARIOS'],
        ['id' =>22, 'NombreEspacio' =>'JUEGOS INFANTILES','DescripcionEspacio' =>'ESPACIOS COMPLEMENTARIOS'],
        ['id' =>23, 'NombreEspacio' =>'INFRAESTRUCTURA GENERAL','DescripcionEspacio' =>'INFRAETRUCTURA GENERAL']


     ];


     foreach ($items as $item) {
            \App\TipoEspacio::create($item);
        }
    }
}
