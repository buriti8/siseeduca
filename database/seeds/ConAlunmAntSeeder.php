<?php

use Illuminate\Database\Seeder;

class ConAlunmAntSeeder extends Seeder
{
  /**
  * Run the database seeds.
  *
  * @return void
  */
  public function run()
  {
    $ConAlunmAnt = App\ConAlunmAnt::create(['IdConAlunmAnt' => 3, 'Descripcion' => 'DESERTÓ']);
    $ConAlunmAnt = App\ConAlunmAnt::create(['IdConAlunmAnt' => 5, 'Descripcion' => 'TRASLADO A OTRA INSTITUCIÓN EDUCATIVA']);
    $ConAlunmAnt = App\ConAlunmAnt::create(['IdConAlunmAnt' => 8, 'Descripcion' => 'OTRO MOTIVO DE RETIRO']);
    $ConAlunmAnt = App\ConAlunmAnt::create(['IdConAlunmAnt' => 9, 'Descripcion' => 'NO APLICA']);
  }
}
