<?php

use Illuminate\Database\Seeder;

class GradosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      $items = [

        ['IdGrado' =>-2,'Grado' =>'PRE-JARDIN', 'LimiteEdad' => 3, 'IdNivel' =>6, 'IdNivelMediaTotal' =>7],
        ['IdGrado' =>-1,'Grado' =>'JRD I/KIND', 'LimiteEdad' => 4, 'IdNivel' =>6, 'IdNivelMediaTotal' =>7],
        ['IdGrado' =>0,'Grado' =>'GRADO 0', 'LimiteEdad' => 5, 'IdNivel' =>0, 'IdNivelMediaTotal' =>0, 'IdNivelCine' =>0],
        ['IdGrado' =>1,'Grado' =>'PRIMERO', 'LimiteEdad' => 6, 'IdNivel' =>1, 'IdNivelMediaTotal' =>1, 'IdNivelCine' =>1],
        ['IdGrado' =>2,'Grado' =>'SEGUNDO', 'LimiteEdad' => 7, 'IdNivel' =>1, 'IdNivelMediaTotal' =>1, 'IdNivelCine' =>1],
        ['IdGrado' =>3,'Grado' =>'TERCERO', 'LimiteEdad' => 8, 'IdNivel' =>1, 'IdNivelMediaTotal' =>1, 'IdNivelCine' =>1],
        ['IdGrado' =>4,'Grado' =>'CUARTO', 'LimiteEdad' => 9, 'IdNivel' =>1, 'IdNivelMediaTotal' =>1, 'IdNivelCine' =>1],
        ['IdGrado' =>5,'Grado' =>'QUINTO', 'LimiteEdad' => 10, 'IdNivel' =>1, 'IdNivelMediaTotal' =>1, 'IdNivelCine' =>1],
        ['IdGrado' =>6,'Grado' =>'SEXTO', 'LimiteEdad' => 11, 'IdNivel' =>2, 'IdNivelMediaTotal' =>2, 'IdNivelCine' =>2],
        ['IdGrado' =>7,'Grado' =>'SEPTIMO', 'LimiteEdad' => 12, 'IdNivel' =>2, 'IdNivelMediaTotal' =>2, 'IdNivelCine' =>2],
        ['IdGrado' =>8,'Grado' =>'OCTAVO', 'LimiteEdad' => 13, 'IdNivel' =>2, 'IdNivelMediaTotal' =>2, 'IdNivelCine' =>2],
        ['IdGrado' =>9,'Grado' =>'NOVENO', 'LimiteEdad' => 14, 'IdNivel' =>2, 'IdNivelMediaTotal' =>2, 'IdNivelCine' =>2],
        ['IdGrado' =>10,'Grado' =>'DÉCIMO', 'LimiteEdad' => 15, 'IdNivel' =>3, 'IdNivelMediaTotal' =>3, 'IdNivelCine' =>3],
        ['IdGrado' =>11,'Grado' =>'ONCE', 'LimiteEdad' => 16, 'IdNivel' =>3, 'IdNivelMediaTotal' =>3, 'IdNivelCine' =>3],
        ['IdGrado' =>12,'Grado' =>'DOCE NRML SUP', 'LimiteEdad' => 99, 'IdNivel' =>4, 'IdNivelMediaTotal' =>3, 'IdNivelCine' =>3],
        ['IdGrado' =>13,'Grado' =>'TRECE NRML SUP', 'LimiteEdad' => 99, 'IdNivel' =>4, 'IdNivelMediaTotal' =>3, 'IdNivelCine' =>3],
        ['IdGrado' =>14,'Grado' =>'ED DISC COG NO INT', 'IdNivel' =>111],
        ['IdGrado' =>15,'Grado' =>'ED DISC AUD NO INT',  'IdNivel' =>111],
        ['IdGrado' =>16,'Grado' =>'ED DISC VIS NO INT',  'IdNivel' =>111],
        ['IdGrado' =>17,'Grado' =>'ED DISC MOT NO INT',  'IdNivel' =>111],
        ['IdGrado' =>18,'Grado' =>'ED DISC MÚLT NO INT',  'IdNivel' =>111],
        ['IdGrado' =>21,'Grado' =>'CICLO 1 ADULTOS',  'IdNivel' =>5, 'IdNivelMediaTotal' =>4, 'IdNivelCine' =>1],
        ['IdGrado' =>22,'Grado' =>'CICLO 2 ADULTOS', 'IdNivel' =>5, 'IdNivelMediaTotal' =>4, 'IdNivelCine' =>1],
        ['IdGrado' =>23,'Grado' =>'CICLO 3 ADULTOS', 'IdNivel' =>5, 'IdNivelMediaTotal' =>5, 'IdNivelCine' =>2],
        ['IdGrado' =>24,'Grado' =>'CICLO 4 ADULTOS',  'IdNivel' =>5, 'IdNivelMediaTotal' =>5, 'IdNivelCine' =>2],
        ['IdGrado' =>25,'Grado' =>'CICLO 5 ADULTOS',  'IdNivel' =>5, 'IdNivelMediaTotal' =>6, 'IdNivelCine' =>3],
        ['IdGrado' =>26,'Grado' =>'CICLO 6 ADULTOS',  'IdNivel' =>5, 'IdNivelMediaTotal' =>6, 'IdNivelCine' =>3],
        ['IdGrado' =>99,'Grado' =>'ACELERACIÓN DEL APRENDIZAJE',  'IdNivel' =>1, 'IdNivelMediaTotal' =>1, 'IdNivelCine' =>1],
        ['IdGrado' =>999,'Grado' =>'NO APLICA',  'IdNivel' =>111],
     ];


     foreach ($items as $item) {
            \App\Grado::create($item);
        }
    }
}
