<?php

use Illuminate\Database\Seeder;

class TipoDocumentoSeeder extends Seeder
{
  /**
  * Run the database seeds.
  *
  * @return void
  */
  public function run()
  {
    $TipoDocumento = App\TipoDocumento::create(['IdTipoDocumento' => 1, 'Descripcion' => 'CÉDULA DE CIUDADANÍA', 'Abreviado' => ' ', 'Abreviado' => 'CC']);
    $TipoDocumento = App\TipoDocumento::create(['IdTipoDocumento' => 2, 'Descripcion' => 'TARJETA DE IDENTIDAD', 'Abreviado' => ' ', 'Abreviado' => 'TI']);
    $TipoDocumento = App\TipoDocumento::create(['IdTipoDocumento' => 3, 'Descripcion' => 'CÉDULA DE EXTRANJERÍA', 'Abreviado' => ' ', 'Abreviado' => 'CE']);
    $TipoDocumento = App\TipoDocumento::create(['IdTipoDocumento' => 5, 'Descripcion' => 'REGISTRO CIVIL DE NACIMIENTO', 'Abreviado' => ' ', 'Abreviado' => 'RC']);
    $TipoDocumento = App\TipoDocumento::create(['IdTipoDocumento' => 6, 'Descripcion' => 'NÚMERO DE IDENTIFICACIÓN PERSONAL', 'Abreviado' => ' ', 'Abreviado' => 'NIP']);
    $TipoDocumento = App\TipoDocumento::create(['IdTipoDocumento' => 7, 'Descripcion' => 'NÚMERO UNICO DE IDENTIFICACIÓN PERSONAL', 'Abreviado' => ' ', 'Abreviado' => 'NUIP']);
    $TipoDocumento = App\TipoDocumento::create(['IdTipoDocumento' => 8, 'Descripcion' => 'NÚMERO DE IDENTIFICACIÓN ESTABLECIDO POR LA SECRETARÍA DE EDUCACION', 'Abreviado' => ' ', 'Abreviado' => 'NES']);
    $TipoDocumento = App\TipoDocumento::create(['IdTipoDocumento' => 9, 'Descripcion' => 'CERTIFICADO CABILDO', 'Abreviado' => ' ', 'Abreviado' => 'CCB']);
    $TipoDocumento = App\TipoDocumento::create(['IdTipoDocumento' => 10, 'Descripcion' => 'PERMISO ESPECIAL DE PERMANENCIA', 'Abreviado' => ' ', 'Abreviado' => 'PEP']);
    $TipoDocumento = App\TipoDocumento::create(['IdTipoDocumento' => 11, 'Descripcion' => 'VISA', 'Abreviado' => ' ', 'Abreviado' => 'VIS']);
    $TipoDocumento = App\TipoDocumento::create(['IdTipoDocumento' => 12, 'Descripcion' => 'TARJETA DE MOVILIDAD FRONTERIZA', 'Abreviado' => ' ', 'Abreviado' => 'TMF']);    
  }
}
