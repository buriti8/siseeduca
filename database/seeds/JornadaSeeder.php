<?php

use Illuminate\Database\Seeder;

class JornadaSeeder extends Seeder
{
  /**
  * Run the database seeds.
  *
  * @return void
  */
  public function run()
  {
    $Jornada = App\Jornada::create(['IdJornada' => 1, 'Descripcion' => 'COMPLETA']);
    $Jornada = App\Jornada::create(['IdJornada' => 2, 'Descripcion' => 'MAÑANA']);
    $Jornada = App\Jornada::create(['IdJornada' => 3, 'Descripcion' => 'TARDE']);
    $Jornada = App\Jornada::create(['IdJornada' => 4, 'Descripcion' => 'NOCTURNA']);
    $Jornada = App\Jornada::create(['IdJornada' => 5, 'Descripcion' => 'FIN DE SEMANA']);
    $Jornada = App\Jornada::create(['IdJornada' => 6, 'Descripcion' => 'ÚNICA']);
  }
}
