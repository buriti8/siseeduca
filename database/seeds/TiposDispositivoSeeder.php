<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TiposDispositivoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tipo_dispositivos')->insert(['nombre' => 'COMPUTADOR PORTATIL', 'descripcion' => '']);                
        DB::table('tipo_dispositivos')->insert(['nombre' => 'TABLETA', 'descripcion' => '']);
        DB::table('tipo_dispositivos')->insert(['nombre' => 'TELEVISOR', 'descripcion' => '']);
        DB::table('tipo_dispositivos')->insert(['nombre' => 'VIDEO BEAM', 'descripcion' => '']);
        DB::table('tipo_dispositivos')->insert(['nombre' => 'PANTALLA', 'descripcion' => '']);
        DB::table('tipo_dispositivos')->insert(['nombre' => 'OTRO', 'descripcion' => '']);
        
    }
}
