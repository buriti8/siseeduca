<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class ContratoGenericoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //No se puede activar debido a que no se sabe con certeza cual es proyecto_id(es autonumérico):
        // DB::table('contratos')->insert([
        //     "numero_contrato" => '999999999',
        //     "objeto_contrato" => 'CONTRATO GENÉRICO',
        //     "tipo_contrato" => '1',            
        //     "fecha_acta_inicio" => Carbon::now(),
        //     "fecha_terminacion_contrato" => Carbon::now(),
        //     "nit_contratista" => '9',
        //     "nombre_contratista" => 'NO APLICA',
        //     "telefono_contratista" => '9',          
        //     "valor_total_contrato" => '1',
        //     "dependencia_contratante" => 'NO APLICA',            
        //     "telefono_mesa_ayuda" => '9',    
        //     "observaciones" => '',
        //     "proyecto_id" => '1']);
    }
}
