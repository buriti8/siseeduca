<?php

use Illuminate\Database\Seeder;

class SubregionesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      $items = [

        ['CodigoSubregion' =>1,'NombreSubregion' =>'BAJO CAUCA', 'CodigoDepartamento' => 5],
        ['CodigoSubregion' =>2,'NombreSubregion' =>'MAGDALENA MEDIO', 'CodigoDepartamento' => 5],
        ['CodigoSubregion' =>3,'NombreSubregion' =>'NORDESTE', 'CodigoDepartamento' => 5],
        ['CodigoSubregion' =>4,'NombreSubregion' =>'NORTE', 'CodigoDepartamento' => 5],
        ['CodigoSubregion' =>5,'NombreSubregion' =>'OCCIDENTE', 'CodigoDepartamento' => 5],
        ['CodigoSubregion' =>6,'NombreSubregion' =>'ORIENTE', 'CodigoDepartamento' => 5],
        ['CodigoSubregion' =>7,'NombreSubregion' =>'SUROESTE', 'CodigoDepartamento' => 5],
        ['CodigoSubregion' =>8,'NombreSubregion' =>'URABÁ', 'CodigoDepartamento' => 5],
        ['CodigoSubregion' =>9,'NombreSubregion' =>'VALLE DE ABURRÁ', 'CodigoDepartamento' => 5],

     ];


     foreach ($items as $item) {
            \App\Subregion::create($item);
        }
    }
}
