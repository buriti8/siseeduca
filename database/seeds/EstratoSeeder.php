<?php

use Illuminate\Database\Seeder;

class EstratoSeeder extends Seeder
{
  /**
  * Run the database seeds.
  *
  * @return void
  */
  public function run()
  {
    $Estrato = App\Estrato::create(['IdEstratos' => 0, 'Descripcion' => 'ESTRATO 0']);
    $Estrato = App\Estrato::create(['IdEstratos' => 1, 'Descripcion' => 'ESTRATO 1']);
    $Estrato = App\Estrato::create(['IdEstratos' => 2, 'Descripcion' => 'ESTRATO 2']);
    $Estrato = App\Estrato::create(['IdEstratos' => 3, 'Descripcion' => 'ESTRATO 3']);
    $Estrato = App\Estrato::create(['IdEstratos' => 4, 'Descripcion' => 'ESTRATO 4']);
    $Estrato = App\Estrato::create(['IdEstratos' => 5, 'Descripcion' => 'ESTRATO 5']);
    $Estrato = App\Estrato::create(['IdEstratos' => 6, 'Descripcion' => 'ESTRATO 6']);
    $Estrato = App\Estrato::create(['IdEstratos' => 9, 'Descripcion' => 'NO APLICA']);
  }
}
