<?php

use Illuminate\Database\Seeder;

class NombreCapacitacionSeeder extends Seeder
{
  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run()
  {
    $items = [

      ['nombre' =>'Especialización'],
      ['nombre' =>'Maestría'],
      ['nombre' =>'Doctorado'],
      ['nombre' =>'Diplomado'],



   ];


   foreach ($items as $item) {
          \App\NombreCapacitacion::create($item);
      }
  }
}
