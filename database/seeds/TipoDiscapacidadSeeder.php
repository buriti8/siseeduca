<?php

use Illuminate\Database\Seeder;

class TipoDiscapacidadSeeder extends Seeder
{
  /**
  * Run the database seeds.
  *
  * @return void
  */
  public function run()
  {
    $TipoDiscapacidad = App\TipoDiscapacidad::create(['IdTipoDiscapacidad' => 1, 'Descripcion' => 'SORDERA PROFUNDA']);
    $TipoDiscapacidad = App\TipoDiscapacidad::create(['IdTipoDiscapacidad' => 2, 'Descripcion' => 'HIPOACUSIA O BAJA AUDICIÓN']);
    $TipoDiscapacidad = App\TipoDiscapacidad::create(['IdTipoDiscapacidad' => 3, 'Descripcion' => 'BAJA VISION IRREVERSIBLE']);
    $TipoDiscapacidad = App\TipoDiscapacidad::create(['IdTipoDiscapacidad' => 4, 'Descripcion' => 'CEGUERA']);
    $TipoDiscapacidad = App\TipoDiscapacidad::create(['IdTipoDiscapacidad' => 5, 'Descripcion' => 'PARÁLISIS CEREBRAL']);
    $TipoDiscapacidad = App\TipoDiscapacidad::create(['IdTipoDiscapacidad' => 6, 'Descripcion' => 'LESIÓN NEUROMUSCULAR']);
    $TipoDiscapacidad = App\TipoDiscapacidad::create(['IdTipoDiscapacidad' => 7, 'Descripcion' => 'AUTISMO']);
    $TipoDiscapacidad = App\TipoDiscapacidad::create(['IdTipoDiscapacidad' => 8, 'Descripcion' => 'DEFICIENCIA INTELECTUAL']);
    $TipoDiscapacidad = App\TipoDiscapacidad::create(['IdTipoDiscapacidad' => 9, 'Descripcion' => 'SÍNDROME DE DOWN']);
    $TipoDiscapacidad = App\TipoDiscapacidad::create(['IdTipoDiscapacidad' => 10, 'Descripcion' => 'MÚLTIPLE']);
    $TipoDiscapacidad = App\TipoDiscapacidad::create(['IdTipoDiscapacidad' => 12, 'Descripcion' => 'LENGUA DE SEÑAS COLOMBIANA']);
    $TipoDiscapacidad = App\TipoDiscapacidad::create(['IdTipoDiscapacidad' => 13, 'Descripcion' => 'USUARIO DEL CASTELLANO']);
    $TipoDiscapacidad = App\TipoDiscapacidad::create(['IdTipoDiscapacidad' => 14, 'Descripcion' => 'SORDOCEGUERA']);
    $TipoDiscapacidad = App\TipoDiscapacidad::create(['IdTipoDiscapacidad' => 15, 'Descripcion' => 'MOVILIDAD']);
    $TipoDiscapacidad = App\TipoDiscapacidad::create(['IdTipoDiscapacidad' => 17, 'Descripcion' => 'SISTEMICA']);
    $TipoDiscapacidad = App\TipoDiscapacidad::create(['IdTipoDiscapacidad' => 18, 'Descripcion' => 'MENTAL- PSICOSOCIAL']);
    $TipoDiscapacidad = App\TipoDiscapacidad::create(['IdTipoDiscapacidad' => 19, 'Descripcion' => 'TRANSTORNO PERMANENTE DE VOZ Y HABLAL']);
    $TipoDiscapacidad = App\TipoDiscapacidad::create(['IdTipoDiscapacidad' => 99, 'Descripcion' => 'NO APLICA']);
  }
}
