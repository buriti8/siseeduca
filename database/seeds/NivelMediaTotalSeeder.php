<?php

use Illuminate\Database\Seeder;

class NivelMediaTotalSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      $items = [

        ['IdNivelMediaTotal' =>0,'Descripcion' =>'PRIMERA INFANCIA','LimiteEdad' =>99],
        ['IdNivelMediaTotal' =>1,'Descripcion' =>'TRANSICIÓN','LimiteEdad' =>5],
        ['IdNivelMediaTotal' =>2,'Descripcion' =>'PRIMARIA','LimiteEdad' =>10],
        ['IdNivelMediaTotal' =>3,'Descripcion' =>'SECUNDARIA','LimiteEdad' =>14],
        ['IdNivelMediaTotal' =>4,'Descripcion' =>'MEDIA TOTAL','LimiteEdad' =>16],
        ['IdNivelMediaTotal' =>5,'Descripcion' =>'CLEIS 1 & 2','LimiteEdad' =>99],
        ['IdNivelMediaTotal' =>6,'Descripcion' =>'CLEIS 3 & 4','LimiteEdad' =>99],
        ['IdNivelMediaTotal' =>7,'Descripcion' =>'CLEIS 5 & 6','LimiteEdad' =>99],
     ];


     foreach ($items as $item) {
            \App\NivelMediaTotal::create($item);
        }
    }
}
