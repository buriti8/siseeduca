<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProyectoGenericoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('proyectos')->insert(['nombre' => 'PROYECTO GENÉRICO', 'plan_desarrollo' => 'PIENSA EN GRANDE', 'periodo' => '2016-2019']);
    }
}
