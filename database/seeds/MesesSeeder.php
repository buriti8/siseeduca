<?php

use Illuminate\Database\Seeder;

class MesesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      $items = [

        ['mes' =>'Enero'],
        ['mes' =>'Febrero'],
        ['mes' =>'Marzo'],
        ['mes' =>'Abril'],
        ['mes' =>'Mayo'],
        ['mes' =>'Junio'],
        ['mes' =>'Julio'],
        ['mes' =>'Agosto'],
        ['mes' =>'Septiembre'],
        ['mes' =>'Octubre'],
        ['mes' =>'Noviembre'],
        ['mes' =>'Diciembre'],


     ];


     foreach ($items as $item) {
            \App\Meses::create($item);
        }
    }
}
