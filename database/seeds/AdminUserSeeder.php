<?php

use Illuminate\Database\Seeder;

class AdminUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      $items = [

        ['name' =>'Admin','lastname' =>'SI-SeEduca', 'email' => 'admin.siseeduca@antioquia.gov.co', 'phone' =>'01-8000-415-221', 'password' =>bcrypt('123456789')],
        ];


     foreach ($items as $item) {
            \App\User::create($item);
        }
    }
}
