<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
  $this->call(DepartamentosSeeder::class);
  $this->call(SubregionesSeeder::class);
  $this->call(MunicipiosSeeder::class);
  $this->call(MesesSeeder::class);
  $this->call(NivelesSeeder::class);
  $this->call(NivelCineSeeder::class);
  $this->call(NivelMediaTotalSeeder::class);
  $this->call(GradosSeeder::class);
  $this->call(VictimaConflictoSeeder::class);
  $this->call(NombreCapacitacionSeeder::class);
  $this->call(AdminUserSeeder::class);
  $this->call(TipoEspaciosSeeder::class);
  $this->call(TipoSolicitudSeeder::class);
  $this->call(IFEspacioSeeder::class);
  $this->call(ProyectoGenericoSeeder::class);
  $this->call(ContratoGenericoSeeder::class);
  $this->call(FuentesDispositivoSeeder::class);
  $this->call(TiposDispositivoSeeder::class);
  $this->call(ConAlunmAntSeeder::class);
  $this->call(EstratoSeeder::class);
  $this->call(JornadaSeeder::class);
  $this->call(MetodologiaSeeder::class);
  $this->call(SitAcaAntSeeder::class);
  $this->call(TipoDiscapacidadSeeder::class);
  $this->call(TipoDocumentoSeeder::class);
  $this->call(ZonaSeeder::class);
  $this->call(CapacidadExcepcionalSeeder::class);
  $this->call(EtniaSeeder::class);
  $this->call(ComitesSeeder::class);
  $this->call(ResguardoSeeder::class);
  $this->call(PeriodoSeeder::class);
  $this->call(DocumentoSeeder::class);
    }
}
