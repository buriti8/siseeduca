<?php

use Illuminate\Database\Seeder;

class IFEspacioSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
     public function run()
     {

       $items = [

         ['id'=>1, 'Nombre' =>'REGULAR','Descripcion' =>''],
         ['id'=>2, 'Nombre' =>'MAL ESTADO','Descripcion' =>''],
         ['id'=>3, 'Nombre' =>'NO APLICA','Descripcion' =>'']
      ];


      foreach ($items as $item) {
             \App\IFisicaEstado::create($item);
         }
     }
}
