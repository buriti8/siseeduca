<?php

use Illuminate\Database\Seeder;

class ResguardoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      Eloquent::unguard();
      $path = 'database/master/resguardos.sql';
      DB::unprepared(file_get_contents($path));
    }
}
