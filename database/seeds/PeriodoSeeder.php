<?php

use Illuminate\Database\Seeder;

class PeriodoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $Periodo = App\Periodo::create(['id' => 1, 'periodo' => 20142, 'Anio' => 2014, 'orden_anio' => 1]);
        $Periodo = App\Periodo::create(['id' => 2, 'periodo' => 20152, 'Anio' => 2015, 'orden_anio' => 2]);
        $Periodo = App\Periodo::create(['id' => 3, 'periodo' => 20162, 'Anio' => 2016, 'orden_anio' => 3]);
        $Periodo = App\Periodo::create(['id' => 4, 'periodo' => 20172, 'Anio' => 2017, 'orden_anio' => 4]);
        $Periodo = App\Periodo::create(['id' => 5, 'periodo' => 20182, 'Anio' => 2018, 'orden_anio' => 5]);
    }
}
