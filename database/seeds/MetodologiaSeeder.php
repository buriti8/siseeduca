<?php

use Illuminate\Database\Seeder;

class MetodologiaSeeder extends Seeder
{
  /**
  * Run the database seeds.
  *
  * @return void
  */
  public function run()
  {
    Eloquent::unguard();
    $path = 'database/master/metodologia.sql';
    DB::unprepared(file_get_contents($path));
  }
}
