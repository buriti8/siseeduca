<?php

use Illuminate\Database\Seeder;

class DocumentoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
     public function run()
     {
       $items = [
         ['codigo' =>1,'documento' => 'Enlace TIC'],
         ['codigo' =>2,'documento' => 'Comité TIC'],
         ['codigo' =>3,'documento' => 'Comité de Cupos'],
         ['codigo' =>4,'documento' => 'JUME'],
      ];


      foreach ($items as $item) {
             \App\Documentos::create($item);
         }
     }
}
