<?php

use Illuminate\Database\Seeder;

class PeriodoPruebaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      $PeriodoSaber = App\PeriodoPrueba::create(['periodo' => 20142, 'Anio' => 2014, 'orden_anio' => 1]);
      $PeriodoSaber = App\PeriodoPrueba::create(['periodo' => 20152, 'Anio' => 2015, 'orden_anio' => 2]);
      $PeriodoSaber = App\PeriodoPrueba::create(['periodo' => 20162, 'Anio' => 2016, 'orden_anio' => 3]);
      $PeriodoSaber = App\PeriodoPrueba::create(['periodo' => 20172, 'Anio' => 2017, 'orden_anio' => 4]);
      $PeriodoSaber = App\PeriodoPrueba::create(['periodo' => 20182, 'Anio' => 2018, 'orden_anio' => 5]);
    }
}
