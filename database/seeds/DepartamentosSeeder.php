<?php

use Illuminate\Database\Seeder;

class DepartamentosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {


      
      $items = [

        ['CodigoDepartamento' =>5,'CodigoDaneDepartamento' =>5000, 'NombreDepartamento' => 'ANTIOQUIA'],
        ['CodigoDepartamento' =>8,'CodigoDaneDepartamento' =>8000, 'NombreDepartamento' => 'ATLÁNTICO'],
        ['CodigoDepartamento' =>11,'CodigoDaneDepartamento' =>11000, 'NombreDepartamento' => 'BOGOTÁ, D.C.'],
        ['CodigoDepartamento' =>13,'CodigoDaneDepartamento' =>13000, 'NombreDepartamento' => 'BOLÍVAR'],
        ['CodigoDepartamento' =>15,'CodigoDaneDepartamento' =>15000, 'NombreDepartamento' => 'BOYACÁ'],
        ['CodigoDepartamento' =>17,'CodigoDaneDepartamento' =>17000, 'NombreDepartamento' => 'CALDAS'],
        ['CodigoDepartamento' =>18,'CodigoDaneDepartamento' =>18000, 'NombreDepartamento' => 'CAQUETÁ'],
        ['CodigoDepartamento' =>19,'CodigoDaneDepartamento' =>19000, 'NombreDepartamento' => 'CAUCA'],
        ['CodigoDepartamento' =>20,'CodigoDaneDepartamento' =>20000, 'NombreDepartamento' => 'CESAR'],
        ['CodigoDepartamento' =>23,'CodigoDaneDepartamento' =>23000, 'NombreDepartamento' => 'CÓRDOBA'],
        ['CodigoDepartamento' =>25,'CodigoDaneDepartamento' =>25000, 'NombreDepartamento' => 'CUNDINAMARCA'],
        ['CodigoDepartamento' =>27,'CodigoDaneDepartamento' =>27000, 'NombreDepartamento' => 'CHOCÓ'],
        ['CodigoDepartamento' =>41,'CodigoDaneDepartamento' =>41000, 'NombreDepartamento' => 'HUILA'],
        ['CodigoDepartamento' =>44,'CodigoDaneDepartamento' =>44000, 'NombreDepartamento' => 'LA GUAJIRA'],
        ['CodigoDepartamento' =>47,'CodigoDaneDepartamento' =>47000, 'NombreDepartamento' => 'MAGDALENA'],
        ['CodigoDepartamento' =>50,'CodigoDaneDepartamento' =>50000, 'NombreDepartamento' => 'META'],
        ['CodigoDepartamento' =>52,'CodigoDaneDepartamento' =>52000, 'NombreDepartamento' => 'NARIÑO'],
        ['CodigoDepartamento' =>54,'CodigoDaneDepartamento' =>54000, 'NombreDepartamento' => 'NORTE DE SANTANDER'],
        ['CodigoDepartamento' =>63,'CodigoDaneDepartamento' =>63000, 'NombreDepartamento' => 'QUINDIO'],
        ['CodigoDepartamento' =>66,'CodigoDaneDepartamento' =>66000, 'NombreDepartamento' => 'RISARALDA'],
        ['CodigoDepartamento' =>68,'CodigoDaneDepartamento' =>68000, 'NombreDepartamento' => 'SANTANDER'],
        ['CodigoDepartamento' =>70,'CodigoDaneDepartamento' =>70000, 'NombreDepartamento' => 'SUCRE'],
        ['CodigoDepartamento' =>73,'CodigoDaneDepartamento' =>73000, 'NombreDepartamento' => 'TOLIMA'],
        ['CodigoDepartamento' =>76,'CodigoDaneDepartamento' =>76000, 'NombreDepartamento' => 'VALLE DEL CAUCA'],
        ['CodigoDepartamento' =>81,'CodigoDaneDepartamento' =>81000, 'NombreDepartamento' => 'ARAUCA'],
        ['CodigoDepartamento' =>85,'CodigoDaneDepartamento' =>85000, 'NombreDepartamento' => 'CASANARE'],
        ['CodigoDepartamento' =>86,'CodigoDaneDepartamento' =>86000, 'NombreDepartamento' => 'PUTUMAYO'],
        ['CodigoDepartamento' =>88,'CodigoDaneDepartamento' =>88000, 'NombreDepartamento' => 'ARCHIPIÉLAGO DE SAN ANDRÉS'],
        ['CodigoDepartamento' =>91,'CodigoDaneDepartamento' =>91000, 'NombreDepartamento' => 'AMAZONAS'],
        ['CodigoDepartamento' =>94,'CodigoDaneDepartamento' =>94000, 'NombreDepartamento' => 'GUAINÍA'],
        ['CodigoDepartamento' =>95,'CodigoDaneDepartamento' =>95000, 'NombreDepartamento' => 'GUAVIARE'],
        ['CodigoDepartamento' =>97,'CodigoDaneDepartamento' =>97000, 'NombreDepartamento' => 'VAUPÉS'],
        ['CodigoDepartamento' =>99,'CodigoDaneDepartamento' =>99000, 'NombreDepartamento' => 'VICHADA'],


     ];


     foreach ($items as $item) {
            \App\Departamento::create($item);
        }
    }
}
