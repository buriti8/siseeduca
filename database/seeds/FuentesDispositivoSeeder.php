<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class FuentesDispositivoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('origen_dispositivos')->insert(['nombre' => 'GOBERNACIÓN DE ANTIOQUIA', 'descripcion' => '']);
        DB::table('origen_dispositivos')->insert(['nombre' => 'INSTITUCIÓN EDUCATIVA', 'descripcion' => '']);
        DB::table('origen_dispositivos')->insert(['nombre' => 'MUNICIPIO', 'descripcion' => '']);
        DB::table('origen_dispositivos')->insert(['nombre' => 'CPE - COMPUTADORES PARA EDUCAR', 'descripcion' => '']);
        DB::table('origen_dispositivos')->insert(['nombre' => 'MEN (MINISTERIO DE EDUCACIÓN)', 'descripcion' => '']);
        DB::table('origen_dispositivos')->insert(['nombre' => 'OTRO', 'descripcion' => '']);
    }
}
