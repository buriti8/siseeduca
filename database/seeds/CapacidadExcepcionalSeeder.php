<?php

use Illuminate\Database\Seeder;

class CapacidadExcepcionalSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      $Estrato = App\CapacidadExcepcional::create(['IdCapacidadExcepcional' => 1, 'Descripcion' => 'CAPACIDADES EXCEPCIONALES']);
      $Estrato = App\CapacidadExcepcional::create(['IdCapacidadExcepcional' => 2, 'Descripcion' => 'TALENTO EN CIENCIA']);
      $Estrato = App\CapacidadExcepcional::create(['IdCapacidadExcepcional' => 3, 'Descripcion' => 'TALENTO EN TECNOLOGIA']);
      $Estrato = App\CapacidadExcepcional::create(['IdCapacidadExcepcional' => 4, 'Descripcion' => 'TALENTO EN ARTES Y/O LETRAS']);
      $Estrato = App\CapacidadExcepcional::create(['IdCapacidadExcepcional' => 5, 'Descripcion' => 'TALENTO EN ACTIVIDAD FISICA, EJERCICIO Y DEPORTE']);
      $Estrato = App\CapacidadExcepcional::create(['IdCapacidadExcepcional' => 6, 'Descripcion' => 'DOBLE EXCEPCIONALIDAD']);
      $Estrato = App\CapacidadExcepcional::create(['IdCapacidadExcepcional' => 7, 'Descripcion' => 'TALENTO EN CIENCIAS SOCIALES O HUMANAS']);
      $Estrato = App\CapacidadExcepcional::create(['IdCapacidadExcepcional' => 9, 'Descripcion' => 'NO APLICA']);
    }
}
