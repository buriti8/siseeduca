<?php

use Illuminate\Database\Seeder;

class TipoSolicitudSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
     public function run()
     {

       $items = [

         ['id' =>1, 'Nombre' =>'AMPLIACIÓN','Descripcion' =>'TIPO SOLICITUD'],
         ['id' =>2, 'Nombre' =>'REPOSICIÓN','Descripcion' =>'TIPO SOLICITUD'],
         ['id' =>3, 'Nombre' =>'MANTENIMIENTO','Descripcion' =>'TIPO SOLICITUD'],
         ['id' =>4, 'Nombre' =>'ADICIÓN','Descripcion' =>'TIPO SOLICITUD']


      ];


      foreach ($items as $item) {
             \App\TipoSolicitud::create($item);
         }
     }
}
