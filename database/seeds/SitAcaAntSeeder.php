<?php

use Illuminate\Database\Seeder;

class SitAcaAntSeeder extends Seeder
{
  /**
  * Run the database seeds.
  *
  * @return void
  */
  public function run()
  {
    $SitAcaAnt = App\SitAcaAnt::create(['IdSitAcaAnt' => 0, 'Descripcion' => 'NO ESTUDIÓ VIGENCIA ANTERIOR']);
    $SitAcaAnt = App\SitAcaAnt::create(['IdSitAcaAnt' => 1, 'Descripcion' => 'APROBÓ']);
    $SitAcaAnt = App\SitAcaAnt::create(['IdSitAcaAnt' => 2, 'Descripcion' => 'REPROBÓ']);
    $SitAcaAnt = App\SitAcaAnt::create(['IdSitAcaAnt' => 4, 'Descripcion' => 'PENDIENTE DE LOGROS']);
    $SitAcaAnt = App\SitAcaAnt::create(['IdSitAcaAnt' => 6, 'Descripcion' => 'VIENE DE OTRA INSTITUCION EDUCATIVA']);
    $SitAcaAnt = App\SitAcaAnt::create(['IdSitAcaAnt' => 7, 'Descripcion' => 'INGRESA POR PRIMERA VEZ AL SISTEMA']);
    $SitAcaAnt = App\SitAcaAnt::create(['IdSitAcaAnt' => 8, 'Descripcion' => 'NO CULMINÓ ESTUDIOS']);
  }
}
