<?php

use Illuminate\Database\Seeder;

class NivelesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      $items = [

        ['IdNivel' =>0,'Descripcion' =>'TRANSICIÓN'],
        ['IdNivel' =>1,'Descripcion' =>'PRIMARIA'],
        ['IdNivel' =>2,'Descripcion' =>'SECUNDARIA'],
        ['IdNivel' =>3,'Descripcion' =>'MEDIA'],
        ['IdNivel' =>4,'Descripcion' =>'CICLOS COMPLEMENTARIOS'],
        ['IdNivel' =>5,'Descripcion' =>'CLEIS'],
        ['IdNivel' =>6,'Descripcion' =>'PRIMERA INFANCIA'],
        ['IdNivel' =>111,'Descripcion' =>'NO APLICA'],


     ];


     foreach ($items as $item) {
            \App\Nivel::create($item);
        }
    }
}
