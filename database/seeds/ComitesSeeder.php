<?php

use Illuminate\Database\Seeder;

class ComitesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
     public function run()
     {



       $items = [
         ['codigo' =>1,'comite_junta' => 'TIC'],
         ['codigo' =>2,'comite_junta' => 'Cupos'],
         ['codigo' =>3,'comite_junta' => 'JUME'],
      ];


      foreach ($items as $item) {
             \App\ComitesJuntas::create($item);
         }
     }
}
