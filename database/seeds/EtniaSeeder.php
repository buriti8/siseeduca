<?php

use Illuminate\Database\Seeder;


class EtniaSeeder extends Seeder
{
  /**
  * Run the database seeds.
  *
  * @return void
  */
  public function run()
  {
    Eloquent::unguard();
    $path = 'database/master/etnias.sql';
    DB::unprepared(file_get_contents($path));

  }
}
