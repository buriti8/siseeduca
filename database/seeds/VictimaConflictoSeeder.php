<?php

use Illuminate\Database\Seeder;

class VictimaConflictoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      $VictimaConflicto = App\VictimaConflicto::create(['IdVictimaConflicto' => 1,'Descripcion' => 'EN SITUACIÓN DE DESPLAZAMIENTO']);
      $VictimaConflicto = App\VictimaConflicto::create(['IdVictimaConflicto' => 2,'Descripcion' => 'DESVINCULADOS DE GRUPOS ARMADOS']);
      $VictimaConflicto = App\VictimaConflicto::create(['IdVictimaConflicto' => 3,'Descripcion' => 'HIJOS DE ADULTOS DESMOVILIZADOS']);
      $VictimaConflicto = App\VictimaConflicto::create(['IdVictimaConflicto' => 4,'Descripcion' => 'VICTIMA DE MINAS']);
      $VictimaConflicto = App\VictimaConflicto::create(['IdVictimaConflicto' => 5,'Descripcion' => 'RESPONSABILIDAD PENAL']);
      $VictimaConflicto = App\VictimaConflicto::create(['IdVictimaConflicto' => 9,'Descripcion' => 'NO APLICA']);
    }
}
