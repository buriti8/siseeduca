CREATE MATERIALIZED VIEW registro_anexos_cargados AS
SELECT historico_matricula.ano_inf,historico_matricula.mes_corte,
max(historico_matricula.created_at) AS fecha,
count(CASE WHEN sector ='NO OFICIAL' AND calendario ='A' THEN 1 END ) as cantidad5AA,
count(CASE WHEN sector ='NO OFICIAL' AND calendario ='B' THEN 1 END ) as cantidad5AB,
count(CASE WHEN sector ='NO OFICIAL' AND calendario ='OTRO' THEN 1 END ) as cantidad5OTRO,
count(CASE WHEN sector ='OFICIAL' AND calendario ='A' THEN 1 END ) as cantidad6AA,
count(CASE WHEN sector ='OFICIAL' AND calendario ='B' THEN 1 END ) as cantidad6AB,
count(CASE WHEN sector ='OFICIAL' AND calendario ='OTRO' THEN 1 END ) as cantidad6OTRO,
count(*) AS cantidadtotal
FROM historico_matricula
GROUP BY historico_matricula.ano_inf, historico_matricula.mes_corte
ORDER BY historico_matricula.ano_inf DESC, historico_matricula.mes_corte;

CREATE MATERIALIZED VIEW vista_matriculas_todas AS
SELECT
historico_matricula.ano_inf,
historico_matricula.mes_corte,
historico_matricula.sector,
historico_matricula.calendario,
historico_matricula.mun_codigo,
historico_matricula.codigo_dane as codigo_establecimiento,
historico_matricula.dane_anterior as codigo_sede,
municipios."CodigoDaneMunicipio",
municipios."CodigoSubregion"        AS subregion,
max(historico_matricula.created_at) AS fecha,
count(*)                            AS cantidad
FROM (historico_matricula
  JOIN municipios ON ((municipios."CodigoMunicipio" = historico_matricula.mun_codigo)))
  GROUP BY historico_matricula.ano_inf, historico_matricula.mes_corte, historico_matricula.sector,
  historico_matricula.calendario, historico_matricula.mun_codigo,historico_matricula.codigo_dane,historico_matricula.dane_anterior,municipios."CodigoDaneMunicipio", municipios."CodigoSubregion"
  ORDER BY historico_matricula.ano_inf DESC, historico_matricula.mes_corte;

  CREATE MATERIALIZED VIEW vista_matriculas_zonas AS SELECT historico_matricula.ano_inf,
  historico_matricula.mes_corte,
  historico_matricula.zon_alu,
  historico_matricula.grado,
  historico_matricula.matricula_contratada,
  historico_matricula.pob_vict_conf,
  historico_matricula.mun_codigo,
  historico_matricula.codigo_dane as codigo_establecimiento,
  historico_matricula.dane_anterior as codigo_sede,
  municipios."CodigoDaneMunicipio",
  municipios."CodigoSubregion" AS subregion,
  max(historico_matricula.created_at) AS fecha,
  count(*) AS cantidad
  FROM (historico_matricula
    JOIN municipios ON ((municipios."CodigoMunicipio" = historico_matricula.mun_codigo)))
    GROUP BY historico_matricula.ano_inf, historico_matricula.mes_corte, historico_matricula.zon_alu, historico_matricula.grado, historico_matricula.matricula_contratada,historico_matricula.pob_vict_conf, historico_matricula.mun_codigo,historico_matricula.codigo_dane,historico_matricula.dane_anterior,municipios."CodigoDaneMunicipio", municipios."CodigoSubregion"
    ORDER BY historico_matricula.ano_inf DESC, historico_matricula.mes_corte;

    CREATE MATERIALIZED VIEW vista_due_todas AS
    SELECT
    historico_matricula.ano_inf,
    historico_matricula.mes_corte,
    historico_establecimientos.codigo_dane_municipio as municipio,
    historico_establecimientos.nombre_establecimiento,
    historico_establecimientos.codigo_establecimiento,
    historico_sedes.nombre_sede,
    historico_sedes.codigo_sede,
    historico_matricula.sector,
    historico_establecimientos.zona,
    municipios."CodigoSubregion"        AS subregion,
    max(historico_matricula.created_at) AS fecha,
    count(*)                            AS cantidad
    FROM (historico_matricula
      JOIN municipios ON ((municipios."CodigoMunicipio" = historico_matricula.mun_codigo))
      JOIN historico_establecimientos ON ((historico_establecimientos.codigo_establecimiento = historico_matricula.codigo_dane))
      JOIN historico_sedes ON ((historico_sedes.codigo_establecimiento = historico_establecimientos.codigo_establecimiento)))
      GROUP BY historico_matricula.ano_inf,  historico_matricula.mes_corte,historico_establecimientos.nombre_establecimiento,
      historico_establecimientos.codigo_establecimiento,historico_sedes.nombre_sede,historico_sedes.codigo_sede, historico_matricula.sector,historico_establecimientos.zona, historico_establecimientos.codigo_dane_municipio, municipios."CodigoSubregion"
      ORDER BY  historico_matricula.ano_inf DESC, historico_matricula.mes_corte;


      CREATE MATERIALIZED VIEW registro_archivos_cargados_due AS
      SELECT historico_establecimientos.anio_corte, historico_establecimientos.mes_corte,
      max(historico_establecimientos.created_at) AS fecha,
      count(CASE WHEN sector ='OFICIAL' AND estado IN ('ANTIGUO-ACTIVO', 'NUEVO-ACTIVO') AND secretaria ='ANTIOQUIA' THEN 1 END) as cantidad_establecimientos
      FROM historico_establecimientos
      GROUP BY historico_establecimientos.anio_corte, historico_establecimientos.mes_corte
      ORDER BY historico_establecimientos.anio_corte DESC, historico_establecimientos.mes_corte;

      CREATE MATERIALIZED VIEW registro_archivos_cargados_due_nacional AS
      SELECT historico_establecimientos_nacional.anio_corte, historico_establecimientos_nacional.mes_corte,
      max(historico_establecimientos_nacional.created_at) AS fecha,
      count(CASE WHEN sector ='OFICIAL' AND estado IN ('ANTIGUO-ACTIVO', 'NUEVO-ACTIVO') THEN 1 END) as cantidad_establecimientos
      FROM historico_establecimientos_nacional
      GROUP BY historico_establecimientos_nacional.anio_corte, historico_establecimientos_nacional.mes_corte
      ORDER BY historico_establecimientos_nacional.anio_corte DESC, historico_establecimientos_nacional.mes_corte;

      CREATE MATERIALIZED VIEW registro_archivos_cargados_pruebas AS
      SELECT historico_pruebas.periodo,
      max(historico_pruebas.created_at) AS fecha,
      count(*) as cantidad_pruebas
      FROM historico_pruebas
      GROUP BY historico_pruebas.periodo
      ORDER BY historico_pruebas.periodo;

      CREATE MATERIALIZED VIEW registro_archivos_cargados_clasificaciones_saber AS
      SELECT historico_clasificaciones.periodo,
      max(historico_clasificaciones.created_at) AS fecha,
      count(*) as cantidad_clasificaciones
      FROM historico_clasificaciones
      GROUP BY historico_clasificaciones.periodo
      ORDER BY historico_clasificaciones.periodo;

      CREATE MATERIALIZED VIEW registro_archivos_cargados_mesa AS
      SELECT historico_mesa.anio_corte, historico_mesa.mes_corte,
      max(historico_mesa.created_at) AS fecha
      FROM historico_mesa
      GROUP BY historico_mesa.anio_corte, historico_mesa.mes_corte
      ORDER BY historico_mesa.anio_corte DESC, historico_mesa.mes_corte;

      CREATE MATERIALIZED VIEW registro_archivos_cargados_conectividad AS
      SELECT historico_conectividad.anio_corte, historico_conectividad.mes_corte,
      max(historico_conectividad.created_at) AS fecha,
      count(*) AS cantidad_conectividad
      FROM historico_conectividad
      GROUP BY historico_conectividad.anio_corte, historico_conectividad.mes_corte
      ORDER BY historico_conectividad.anio_corte DESC, historico_conectividad.mes_corte;

      CREATE MATERIALIZED VIEW registro_archivos_cargados_calidad AS
      SELECT historico_calidad.anio_corte, historico_calidad.mes_corte,
      max(historico_calidad.created_at) AS fecha,
      count(*) AS cantidad_calidad
      FROM historico_calidad
      GROUP BY historico_calidad.anio_corte, historico_calidad.mes_corte
      ORDER BY historico_calidad.anio_corte DESC, historico_calidad.mes_corte;

      CREATE MATERIALIZED VIEW registro_archivos_cargados_dispositivos AS
      SELECT dispositivos.numero_carga,
      max(dispositivos.created_at) AS fecha,
      count(dispositivos.id) AS cantidad
      FROM dispositivos
      GROUP BY dispositivos.numero_carga
      ORDER BY dispositivos.numero_carga;
