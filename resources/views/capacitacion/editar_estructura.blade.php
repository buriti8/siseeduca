
@extends('layouts.app')

@section('htmlheader_title')
Editar Estructura
@endsection


@section('main-content')
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
<section  id="contenido_principal">
  <div class="box-header">
    <h3 class="box-title">Editar Capacitaciones  </h3>
  </div><!-- /.box-header -->


<!-- Caja para estructura de Departamentos -->
  <div class="box box-solid box-primary collapsed-box"  >
      <div class="box-header with-border" >
        <h3 class="box-title" >Tipo de Capacitacion</h3>
        <div class="box-tools pull-right" >
          <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip">
            <i class="fa fa-plus"></i></button>
        </div>
      </div>


      <div class="box-body"  >

        <div class="margin" id="botones_control">
          <a href="javascript:void(0);" class="btn btn-xs btn-primary" onclick="cargar_formulario(22);">Nueva Capacitacion</a>
        </div>

        {{ $capacitacions->links() }}

        @if(count($capacitacions)==0)


        <div class="box box-primary col-xs-12">

        <div class='aprobado' style="margin-top:70px; text-align: center">

        <label style='color:#177F6B'>
                      ... no se encontraron Tipos de capacitaciones ...
        </label>

        </div>

         </div>


        @endif

        <div class="table-responsive" >

          <table  class="table table-hover table-striped" cellspacing="0" width="100%">
            <thead>
                <tr> <th>Código </th>
                    <th>Código DANE </th>
                    <th>Nombre</th>
                    <th>Acción</th>
                </tr>
            </thead>
          <tbody>

          @foreach($departamentos as $departamento)
        <tr role="row" class="odd">
          <td>{{ $departamento->CodigoDepartamento }}</td>
          <td>{{ $departamento->CodigoDaneDepartamento }}</td>
          <td>{{ $departamento->NombreDepartamento }}</td>
          <td>
          <button type="button"  class="btn  btn-default btn-xs" onclick="verinfo_departamento({{  $departamento->id }})" ><i class="fa fa-fw fa-edit"></i></button>
          <button type="button"  class="btn  btn-danger btn-xs"  onclick="borrado_departamento({{  $departamento->id }});"  ><i class="fa fa-fw fa-remove"></i></button>
          </td>
        </tr>
          @endforeach

        </tbody>
        </table>

        </div>

      </div>
      <!-- /.box-body -->

    </div>
    <!-- /.Fin caja Departamentos-->
    <!-- Caja para estructura  Subregion --->


@endsection
