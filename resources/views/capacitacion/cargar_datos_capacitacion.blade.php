@extends('layouts.app')

@section('htmlheader_title')
Carga Masiva de Datos Infraestructura Tecnologica - Capacitacion Docentes
@endsection


@section('main-content')


<section  id="contenido_principal">
  <div class="box box-primary">
                  <div class="box-header">
                    <h3 class="box-title">Carga Masiva Capacitacion Docentes </h3>
                  </div><!-- /.box-header -->

  <div id="notificacion_resul_fci"></div>

<div id="formulario_archivos_capacitacion">
  <form  id="f_subir_archivos_capacitacion" name="f_subir_archivo_capacitacion " method="post"  files=”true” action="{{ url('subir_archivos_capacitacion') }}" class="formarchivocapacitacion" enctype="multipart/form-data" >
{{ csrf_field() }}

    <div class="box-body">

    <div class="form-group col-xs-6"  >

    <label for="mes">Mes :</label>

    <select id="mes" name="mes" class="form-control" value="" required>
    <option selected="selected"></option>

    @foreach($meses as $mes)
    <option value="{{ $mes->id}}">{{ $mes->mes }}</option>
    @endforeach

    </select>

</div>


<div class="form-group col-xs-6"  >

<label for="anio">Año :</label>
  <input name="anio" id="anio" type="number"   class="archivo form-control" min="2000" max="2100" required/><br />

</div>

<div class="form-group col-xs-6"  >

<label for="mes">Tipo de Capacitación :</label>

<select id="nombre" name="nombre" class="form-control" value="" required>
<option selected="selected"></option>

@foreach($nombres as $nombre)
<option value="{{ $nombre->nombre}}">{{ $nombre->nombre }}</option>
@endforeach


</select>
<span class="label label-primary">Recuerda agregar un tipo de capacitación antes de cargar un archivo!! </span>
</div>

  </div>


  <div class="box-body ">

<div class="panel panel-primary ">
   <div class="panel-heading">Capacitacion Docentes </div>
<div class="panel-body">
  <div class="form-group col-xs-6"  >
         <label>Agregar Archivo  <span class="label label-primary">Capacitaciones </span></label>
  <input name="archivo_capacitacion" id="archivo_capacitacion" type="file" files=”true”  enctype=”multipart/form-data”  class="archivo form-control"  required/><br /><br />
  </div>
</div>




</div>



</div>

<div class="box-footer">


  <button type="submit" id="btnsubmit"  class="btn btn-primary pull-right glyphicon glyphicon-open"> Cargar Archivos</button>

  </div>



  </div>

  </form>

</div>





</section>
@endsection
