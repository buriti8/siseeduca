@section('htmlheader_title')
    {{ trans('adminlte_lang::message.servererror') }}
@endsection

@section('main-content')

    <div class="error-page">

        <div class="error-content">
            <h3><i class="fa fa-warning text-red"></i> Oops! Acceso no autorizado</h3>
        </div>
    </div><!-- /.error-page -->
@endsection
