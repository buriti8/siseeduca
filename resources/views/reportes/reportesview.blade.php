
@extends('layouts.app')
@section('htmlheader_title')
  Generar reportes
@endsection

@section('main-content')
  <section  id="contenido_principal">
    <div class="box box-primary">
      <div style="text-align: center;" class="box-header">
        <h3><strong>Generador de reportes</strong></h3>
      </div>

      <div id="infofiltro" class="alert-message alert-message-success" style="overflow: hidden; height:4em; padding-top: 6.86494px; margin-top: 0px; padding-bottom: 6.86494px; margin-bottom: 0px;">
        <h4>¡Recuerda!</h4>
        <p class="text-justify" style="font-size: 16px;">Para poder generar algún reporte, es necesario seleccionar área de interés y opcionalmente: la subregión, municipio, año y mes de corte.</p>
      </div>
      <a href="#" id="clickme" onclick="return false;" style="background-color:#369;color:#fff;text-decoration:none;padding:4px 10px;display:inline-block">Información</a>
      <br><br>
      <div class="box-body">
        <div class="form-group col-xs-6">
          <label for="selArea">Seleccionar área de interés</label>
          <select id="selArea" name="selArea" class="form-control  col-xs-6" onchange="checkGenerator()">
            <option value="">...</option>
            @if(Auth::user()->isRole('administrador_sistema') || Auth::user()->isRole('administrador_matricula'))
              <option value="matricula">Matricula detallada</option>
            @endif
            <option value="matriculapublica">Matricula</option>
            <option value="dueEst">DUE - Establecimientos</option>
            <option value="dueSed">DUE - Sedes</option>
            <option value="historicoConectividad">Histórico conectividad</option>
            <option value="historicoCalidad">Histórico calidad</option>
            @if(Auth::user()->isRole('administrador_sistema') || Auth::user()->isRole('administrador_infraestructura_fisica'))
              <option value="InfraFisica">Infraestructura educativa</option>
            @endif
            @if(Auth::user()->isRole('administrador_sistema') || Auth::user()->isRole('administrador_infraestructura_fisica'))
              <option value="Solicitudes">Solicitudes</option>
            @endif
          </select>
        <!-- <br>
          <label for="selSubregion">Seleccionar subregión</label>
          <select  id="selSubregion" name="selSubregion" class="form-control  col-xs-6" onchange="filtroSubregionMunicipios()">
            <option value="">Todas</option>
            @foreach ($subregiones as $subregion)
              <option value="{{$subregion->CodigoSubregion}}">{{$subregion->NombreSubregion}}</option>
            @endforeach
          </select>
          <br>
          <div id='selMunicipioContenedor' style="display:none">
            <label for="selMunicipio">Seleccionar municipio</label>
            <select  id="selMunicipio" name="selMunicipio" class="form-control  col-xs-6" onchange="filtroMunicipios()">
            </select>
          </div>
          <br>-->
          <div id='selAnioContenedor'>
            <label for="selAnioCorte">Seleccionar año de corte</label>
            <select  id="selAnioCorte" name="selAnioCorte" class="form-control  col-xs-6" onchange="filtroMeses()">
            </select>
          </div>
          <br>
          <div id='selMesContenedor' style="display:none">
            <label for="selMesCorte">Seleccionar mes de corte</label>
            <select  id="selMesCorte" name="selMesCorte" class="form-control  col-xs-6" onchange="selecMeses()">
            </select>
          </div><br>
          <div class="loader" id="loader"  style="margin:auto; display:none;" >
          </div>
        </div>
      </div>
      <div id="CasoEspecialMatricula" class="alert-message-success" style="overflow: hidden; background:white(255, 240, 158); font-size: 1.1em; height:6em; padding-top: 6.86494px; margin-top: 0px; padding-bottom: 6.86494px; margin-bottom: 0px; display:none;">
        <h4 style="font-weight: bold;">¡Advertencia!</h4>
        <p>Debido a la cantidad de registros, si desea generar un reporte de matrícula, este podría tomar algunos minutos en ejecutarse y podría incluso afectar el rendimiento de su equipo.</p>
      </div>
      <div id="infofiltroSeleccinando" class="alert-message-success" style="overflow: hidden; background:white(255, 240, 158); font-size: 1.1em; height:6em; padding-top: 6.86494px; margin-top: 0px; padding-bottom: 6.86494px; margin-bottom: 0px; display:none;">
        <h4 style="font-weight: bold;">¡Aviso!</h4>
        <p>El área de interés seleccionado no posee registros.</p>
      </div>
      <div id="infofiltroMatricula" class="alert-message-success" style="overflow: hidden; background:white(255, 240, 158); font-size: 1.1em; height:6em; padding-top: 6.86494px; margin-top: 0px; padding-bottom: 6.86494px; margin-bottom: 0px; display:none;">
        <h4 style="font-weight: bold;">¡Aviso!</h4>
        <p>No es posible consular las matrículas debido a que el último anexo cargado al sitema fue eliminado por el administrador del módulo.</p>
      </div>

      <div class="panel panel-primary">

        <div class="panel-heading" id="selcamposdiv">
          Seleccionar campos
        </div>
        <div class="panel-body">
          <form id='fmrSelCampos' action="" method="post">

            {{ csrf_field() }}
            <div class="form-group">
              <input type='hidden' id='hdnsubregion' name='hdnsubregion' value=''/>
              <input type='hidden'  id='hdnmunicipio' name='hdnmunicipio' value=''/>
              <input type='hidden'  id='hdnaniocorte' name='hdnaniocorte' value=''/>
              <input type='hidden'  id='hdnmescorte' name='hdnmescorte' value=''/>
              <div id="cargaSelCampos">

              </div>
              <div class="" id="cargaResultados">

              </div>
            </form>
          </div>
        </div>
      </div>
      <input type="button" name="btn" class="btn btn-primary" id="genReport" value="Generar">
      <br><br>
      <div class="panel panel-primary">

        <div class="panel-heading">
          Registros
        </div>
        <div class="panel-body">
          <div class="form-group">
            <div class="col-sm-12 col-md-12 col-lg-12" id="cargaResultados">
              <div class="table-responsive col-sm-12">
                <table id="dt_cliente" style="text-align: center;" class="table display table-hover" cellspacing="0" width="100%">
                  <thead>
                    <tr id="encabezados" style="text-align: center;">
                    </tr>
                  </thead>
                </table>
              </div>
            </div>
          </div>
          <div id="loading" class="col-sm-12 col-md-12 col-lg-12" style="height:22em; text-align:center; display:none;">
            <img src="{{ asset('/img/cargando.gif') }}" style="margin:auto;">
          </div>
          <div id="info" class="alert alert-info col-sm-12 col-md-12 col-lg-12" style="height:3em; text-align:center; display:none;">
            <strong>Información:</strong> La consulta realizada no pudo recuperar registros, inténtelo nuevamente seleccionando otra área de interés.
          </div>
          <div id="danger" class="alert alert-danger col-sm-12 col-md-12 col-lg-12" style="height:3em; text-align:center; display:none;">
            <strong>¡Lo sentimos!</strong> Hemos experimentado un error, recarga la página e inténtalo de nuevo, si el error persiste, comunícate con la oficina central de SeEduca.
          </div>
        </div>

      </div>
    </section>
    <script>
    $(document).ready(function () {
      $(document).ajaxStart(function () {
        $("#loading").show();
      }).ajaxStop(function () {
        $("#loading").hide();
      });
    });

    //Funcion que hace desaparecer el div transcurridos 3000 milisegundos!
    $(document).ready(function() {
      setTimeout(function() {
        // Declaramos la capa mediante una clase para ocultarlo
        $("#infofiltro").fadeOut(1500);
      },8000);
    });

    $(document).ready(function() {
      $("#clickme").click(function() {
        $("#infofiltro").toggle("slow", function() {

        });
      });
    });
    </script>
    <script type="text/javascript">

    </script>
    <style>
    .loader {
      border: 10px solid #f3f3f3;
      border-radius: 50%;
      border-top: 10px solid #337ab7;
      width: 30px;
      height: 30px;
      -webkit-animation: spin 1s linear infinite; /* Safari */
      animation: spin 1s linear infinite;
    }

    /* Safari */
    @-webkit-keyframes spin {
      0% { -webkit-transform: rotate(0deg); }
      100% { -webkit-transform: rotate(360deg); }
    }

    @keyframes spin {
      0% { transform: rotate(0deg); }
      100% { transform: rotate(360deg); }
    }
    </style>
    @extends('layouts.partials.scriptreport')
  @endsection
  @section('scripts')
  @stop
  {{-- @section('scripts')
@endsection --}}
