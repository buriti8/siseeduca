@extends('layouts.app')

@section('htmlheader_title')
  Editar Información
@endsection

@section('main-content')

  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>

  <script type="text/javascript">
  function ocultar_informacion(){

    //función encargada de ocualtar o cerrar el modal de información que se activa al ingresar al sistema.
    document.getElementById('verInformacion').style.display = 'none';
  }
</script>


<div style="background color: white;">
  <div style="text-align: center; margin-bottom: 20px;" class="container">
    <h3><strong>Actualizar información directorio municipal de Antioquia</strong></h2>
    </div>
  </div>
  @if(Auth::user()->isRole('secretarios'))
    <div class="btn-group" style="margin-bottom:10px;">
      <button type="button" class="btn btn-default dropdown-toggle"
      data-toggle="dropdown">
      Ingresar información <span class="caret"></span>
    </button>

    <ul class="dropdown-menu" role="menu">
      <li><a href="javascript:void(0);" onclick="cargar_formulario(43);">Ingresar alcalde, secretarios(a) y administrador(a) SIMAT</a></li>
      <li><a href="javascript:void(0);" onclick="cargar_formulario(44);">Ingresar rector(a)</a></li>
      <li><a href="javascript:void(0);" onclick="cargar_formulario(45);">Ingresar enlace TIC</a></li>
      <li><a href="javascript:void(0);" onclick="cargar_formulario(46);">Ingresar secretario(a) planeación</a></li>
      <li><a href="javascript:void(0);" onclick="cargar_formulario(48);">Ingresar integrante de comité o junta</a></li>
      <li><a href="javascript:void(0);" onclick="cargar_formulario(51);">Ingresar documentos</a></li>
    </ul>
  </div>
@endif
@if(Auth::user()->isRole('administrador_sistema') || Auth::user()->isRole('Administrador_infraestructura_educativa') || Auth::user()->isRole('Administrador_infraestructura_tecnologica') || Auth::user()->isRole('Administrador_matricula') || Auth::user()->isRole('Administrador_due') || Auth::user()->isRole('Administrador_pruebas_saber'))
  <div class="btn-group" style="margin-bottom:10px;">
    <button type="button" class="btn btn-default dropdown-toggle"
    data-toggle="dropdown">
    Ingresar información <span class="caret"></span>
  </button>

  <ul class="dropdown-menu" role="menu">
    <li><a href="javascript:void(0);" onclick="cargar_formulario(43);">Ingresar alcalde, secretarios(a) y administrador(a) SIMAT</a></li>
    <li><a href="javascript:void(0);" onclick="cargar_formulario(44);">Ingresar rector(a)</a></li>
    <li><a href="javascript:void(0);" onclick="cargar_formulario(45);">Ingresar enlace TIC</a></li>
    <li><a href="javascript:void(0);" onclick="cargar_formulario(46);">Ingresar secretario(a) planeación</a></li>
    <li><a href="/form_ingresar_directornucleo" >Ingresar directores(a) de núcleo</a></li>
    <li><a href="javascript:void(0);" onclick="cargar_formulario(48);">Ingresar integrante de comité o junta</a></li>
    <li><a href="javascript:void(0);" onclick="cargar_formulario(51);">Ingresar documentos</a></li>
  </ul>
</div>
@endif
@if(Auth::user()->isRole('rectores'))
  <div class="btn-group" style="margin-bottom:10px;">
    <button type="button" class="btn btn-default dropdown-toggle"
    data-toggle="dropdown">
    Ingresar información <span class="caret"></span>
  </button>
  <ul class="dropdown-menu" role="menu">
    <li><a href="javascript:void(0);" onclick="cargar_formulario(44);">Ingresar rector(a)</a></li>
  </ul>
</div>
@endif
@if(Auth::user()->isRole('enlace_tic'))
  <div class="btn-group" style="margin-bottom:10px;">
    <button type="button" class="btn btn-default dropdown-toggle"
    data-toggle="dropdown">
    Ingresar información <span class="caret"></span>
  </button>
  <ul class="dropdown-menu" role="menu">
    <li><a href="javascript:void(0);" onclick="cargar_formulario(45);">Ingresar enlace TIC</a></li>
    <li><a href="javascript:void(0);" onclick="cargar_formulario(48);">Ingresar comités TIC</a></li>
    <li><a href="javascript:void(0);" onclick="cargar_formulario(51);">Ingresar documentos</a></li>
  </ul>
</div>
@endif

@can('directorio_municipal')
  @if(Auth::user()->isRole('secretarios'))
    <div class="box box-solid box-primary collapsed-box" >
      <div class="box-header with-border" >
        <h3 class="box-title" >Secretarios(a) de educación</h3>
        <div class="box-tools pull-right" >
          <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip">
            <i class="fa fa-plus"></i></button>
          </div>
        </div>
        @if(Auth::user()->isRole('secretarios'))
          <span class="help-block"><span class="glyphicon glyphicon-tags"></span>  *Señor(a) secretario(a) de educación, en este ítem podrás actualizar tu información de contacto.</span>
        @else
          <span class="help-block"><span class="glyphicon glyphicon-tags"></span>    *En este ítem podrás actualizar la información de contacto de los secretarios(a) de educación del departamento de Antioquia.</span>
        @endif
        <div class="box-body"  >
          <div id"tabla-responsive" class="table-responsive" >

            <a href="javascript:void(0);" style="margin-bottom:10px;" onclick="cargar_formulario(43);" class="btn btn-primary">Ingresar secretario(a) de educación</a>

            <table id="tablaSecretarios"  class="mdl-data-table" style="width:100%">
              <thead>
                <tr>
                  <Th>Municipio</Th>
                  <Th>Nombres</Th>
                  <Th>Apellidos</Th>
                  <Th>Celular</Th>
                  <Th>Teléfono</Th>
                  <Th>Correo personal</Th>
                  <Th>Correo secretaría de educación</Th>
                  <Th>Cargo secretario</Th>
                  <Th>Dirección oficina</Th>
                  <Th>Acción</Th>
                </tr>
              </thead>
            </table>

          </div>
        </div>
      </div>
      <script>


      $(document).ready(function() {
        $('#tablaSecretarios').DataTable({
          serverSide: true,
          pageLength: 10,
          language: {
            "url": '{!! asset('/plugins/datatables/latino.json') !!}'
          } ,
          ajax: '{{ route('actualizar.secretarios') }}',
          columns: [
            { data: 'NombreMunicipio', name: 'NombreMunicipio' },
            { data: 'nombre_secretario', name: 'nombre_secretario' },
            { data: 'apellido_secretario', name: 'apellido_secretario' },
            { data: 'celular_secretario', name: 'celular_secretario' },
            { data: 'telefono_secretario', name: 'telefono_secretario' },
            { data: 'correo_secretario', name: 'correo_secretario' },
            { data: 'correo_secretariaeducacion', name: 'correo_secretariaeducacion' },
            { data: 'cargo_secretario', name: 'cargo_secretario' },
            { data: 'direccion_secretario', name: 'direccion_secretario' },
            { data: null,  render: function ( data, type, row )
              {
                return "<button type='button' title='Editar' onclick='verinfo_secretarios("+ data.id +")' class='btn btn-warning btn-xs' ><i class='fa fa-edit'></i></button>"+"&nbsp;"
              }
            }
          ]
        });


      });

      </script>

      <div class="box box-solid box-primary collapsed-box" >
        <div class="box-header with-border" >
          <h3 class="box-title" >Alcaldes </h3>
          <div class="box-tools pull-right" >
            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip">
              <i class="fa fa-plus"></i></button>
            </div>
          </div>
          <span class="help-block"><span class="glyphicon glyphicon-tags"></span>       *En este ítem podrás actualizar la información de contacto de los alcaldes del departamento de Antioquia.</span>
          <div class="box-body"  >
            <div id"tabla-responsive" class="table-responsive" >

              <a href="javascript:void(0);" style="margin-bottom:10px;" onclick="cargar_formulario(43);" class="btn btn-primary">Ingresar alcalde</a>

              <table id="tablaAlcades"  class="mdl-data-table" style="width:100%">
                <thead>
                  <tr>
                    <Th>Municipio</Th>
                    <Th>Nombres</Th>
                    <Th>Apellidos</Th>
                    <Th>Celular</Th>
                    <Th>Teléfono</Th>
                    <Th>Correo personal</Th>
                    <Th>Correo alcaldía</Th>
                    <Th>Dirección</Th>
                    <Th>Acción</Th>
                  </tr>
                </thead>
              </table>
            </div>
          </div>
        </div>

        <script>

        $(document).ready(function() {
          $('#tablaAlcades').DataTable({
            processing: true,
            serverSide: true,
            pageLength: 10,
            language: {
              "url": '{!! asset('/plugins/datatables/latino.json') !!}'
            } ,
            ajax: '{{ route('actualizar.alcalde') }}',
            columns: [
              { data: 'NombreMunicipio', name: 'NombreMunicipio' },
              { data: 'nombre_alcalde', name: 'nombre_alcalde' },
              { data: 'apellido_alcalde', name: 'apellido_alcalde' },
              { data: 'celular_alcalde', name: 'celular_alcalde' },
              { data: 'telefono_alcalde', name: 'telefono_alcalde' },
              { data: 'correo_alcalde', name: 'correo_alcalde' },
              { data: 'correo_alcaldia', name: 'correo_alcaldia' },
              { data: 'direccion_alcaldia', name: 'direccion_alcaldia' },
              { data: null,  render: function ( data, type, row )
                {
                  return "<button type='button' title='Editar' onclick='verinfo_alcaldes("+ data.id +")' class='btn btn-warning btn-xs' ><i class='fa fa-edit'></i></button>"+"&nbsp;"
                }
              }
            ]
          });

        });

        </script>
      @else
        <div class="box box-solid box-primary collapsed-box" >
          <div class="box-header with-border" >
            <h3 class="box-title" >Alcaldes </h3>
            <div class="box-tools pull-right" >
              <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip">
                <i class="fa fa-plus"></i></button>
              </div>
            </div>
            <span class="help-block"><span class="glyphicon glyphicon-tags"></span>       *En este ítem podrás actualizar la información de contacto de los alcaldes del departamento de Antioquia.</span>
            <div class="box-body"  >

              <div id"tabla-responsive" class="table-responsive" >

                <a href="javascript:void(0);" style="margin-bottom:10px;" onclick="cargar_formulario(43);" class="btn btn-primary">Ingresar alcalde</a>

                <table id="tablaAlcades"  class="mdl-data-table" style="width:100%">
                  <thead>
                    <tr>
                      <Th>Municipio</Th>
                      <Th>Nombres</Th>
                      <Th>Apellidos</Th>
                      <Th>Celular</Th>
                      <Th>Teléfono</Th>
                      <Th>Correo personal</Th>
                      <Th>Correo alcaldía</Th>
                      <Th>Dirección</Th>
                      <Th>Acción</Th>
                    </tr>
                  </thead>
                </table>
              </div>
            </div>
          </div>

          <script>

          $(document).ready(function() {
            $('#tablaAlcades').DataTable({
              processing: true,
              serverSide: true,
              pageLength: 10,
              language: {
                "url": '{!! asset('/plugins/datatables/latino.json') !!}'
              } ,
              ajax: '{{ route('actualizar.alcalde') }}',
              columns: [
                { data: 'NombreMunicipio', name: 'NombreMunicipio' },
                { data: 'nombre_alcalde', name: 'nombre_alcalde' },
                { data: 'apellido_alcalde', name: 'apellido_alcalde' },
                { data: 'celular_alcalde', name: 'celular_alcalde' },
                { data: 'telefono_alcalde', name: 'telefono_alcalde' },
                { data: 'correo_alcalde', name: 'correo_alcalde' },
                { data: 'correo_alcaldia', name: 'correo_alcaldia' },
                { data: 'direccion_alcaldia', name: 'direccion_alcaldia' },
                { data: null,  render: function ( data, type, row )
                  {
                    return "<button type='button' title='Editar' onclick='verinfo_alcaldes("+ data.id +")' class='btn btn-warning btn-xs' ><i class='fa fa-edit'></i></button>"+"&nbsp;"+
                    "@role('administrador_sistema')<button type='button'  onclick='borrar_alcalde(" + data.id +")' class='btn  btn-danger btn-xs' ><i class='fa fa-fw fa fa-trash-o' title='Eliminar'></i></button>@endrole"
                  }
                }
              ]
            });

          });

          </script>



          <div class="box box-solid box-primary collapsed-box" >
            <div class="box-header with-border" >
              <h3 class="box-title" >Secretarios(a) de educación</h3>
              <div class="box-tools pull-right" >
                <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip">
                  <i class="fa fa-plus"></i></button>
                </div>
              </div>
              <span class="help-block"><span class="glyphicon glyphicon-tags"></span>    *En este ítem podrás actualizar la información de contacto de los secretarios(a) de educación del departamento de Antioquia.</span>
              <div class="box-body">.
                <a href="javascript:void(0);" style="margin-bottom:10px;" onclick="cargar_formulario(43);" class="btn btn-primary">Ingresar secretario(a) de educación</a>

                <div id"tabla-responsive" class="table-responsive" >
                  <table id="tablaSecretarios"  class="mdl-data-table" style="width:100%">
                    <thead>
                      <tr>
                        <Th>Municipio</Th>
                        <Th>Nombres</Th>
                        <Th>Apellidos</Th>
                        <Th>Celular</Th>
                        <Th>Teléfono</Th>
                        <Th>Correo personal</Th>
                        <Th>Correo secretaría de educación</Th>
                        <Th>Cargo secretario</Th>
                        <Th>Dirección oficina</Th>
                        <Th>Acción</Th>
                      </tr>
                    </thead>
                  </table>

                </div>
              </div>
            </div>

            <script>

            $(document).ready(function() {
              $('#tablaSecretarios').DataTable({
                processing: true,
                serverSide: true,
                pageLength: 10,
                language: {
                  "url": '{!! asset('/plugins/datatables/latino.json') !!}'
                } ,
                ajax: '{{ route('actualizar.secretarios') }}',
                columns: [
                  { data: 'NombreMunicipio', name: 'NombreMunicipio' },
                  { data: 'nombre_secretario', name: 'nombre_secretario' },
                  { data: 'apellido_secretario', name: 'apellido_secretario' },
                  { data: 'celular_secretario', name: 'celular_secretario' },
                  { data: 'telefono_secretario', name: 'telefono_secretario' },
                  { data: 'correo_secretario', name: 'correo_secretario' },
                  { data: 'correo_secretariaeducacion', name: 'correo_secretariaeducacion' },
                  { data: 'cargo_secretario', name: 'cargo_secretario' },
                  { data: 'direccion_secretario', name: 'direccion_secretario' },
                  { data: null,  render: function ( data, type, row )
                    {
                      return "<button type='button' title='Editar' onclick='verinfo_secretarios("+ data.id +")' class='btn btn-warning btn-xs' ><i class='fa fa-edit'></i></button>"+"&nbsp;"
                    }
                  }
                ]
              });

            });

            </script>
          @endif


        @endcan

        @can('directorio_municipal')

          <div class="box box-solid box-primary collapsed-box" >
            <div class="box-header with-border" >
              <h3 class="box-title" >Administrador(a) SIMAT</h3>
              <div class="box-tools pull-right" >
                <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip">
                  <i class="fa fa-plus"></i></button>
                </div>
              </div>
              <span class="help-block"><span class="glyphicon glyphicon-tags"></span>       *En este ítem podrás actualizar la información de contacto de los Administradores(a) SIMAT del departamento de Antioquia.</span>
              <div class="box-body"  >
                <div id"tabla-responsive" class="table-responsive" >

                  <a href="javascript:void(0);" style="margin-bottom:10px;" onclick="cargar_formulario(43);" class="btn btn-primary">Ingresar Administrador(a) SIMAT</a>


                  <table id="tablaAdminsimat"  class="mdl-data-table" style="width:100%">
                    <thead>
                      <tr>
                        <Th>Municipio</Th>
                        <Th>Nombres</Th>
                        <Th>Apellidos</Th>
                        <Th>Celular</Th>
                        <Th>Teléfono</Th>
                        <Th>Correo personal</Th>
                        <Th>Cargo</Th>
                        <Th>Dirección oficina</Th>
                        <Th>Acción</Th>
                      </tr>
                    </thead>
                  </table>

                </div>
              </div>
            </div>

            <script>

            $(document).ready(function() {
              $('#tablaAdminsimat').DataTable({
                processing: true,
                serverSide: true,
                pageLength: 10,
                language: {
                  "url": '{!! asset('/plugins/datatables/latino.json') !!}'
                } ,
                ajax: '{{ route('actualizar.Adminsimat') }}',
                columns: [
                  { data: 'NombreMunicipio', name: 'NombreMunicipio' },
                  { data: 'nombre_adminsimat', name: 'nombre_adminsimat' },
                  { data: 'apellido_adminsimat', name: 'apellido_adminsimat' },
                  { data: 'celular_adminsimat', name: 'celular_adminsimat' },
                  { data: 'telefono_adminsimat', name: 'telefono_adminsimat' },
                  { data: 'correo_adminsimat', name: 'correo_adminsimat' },
                  { data: 'cargo_adminsimat', name: 'cargo_adminsimat' },
                  { data: 'direccion_adminsimat', name: 'direccion_adminsimat' },
                  { data: null,  render: function ( data, type, row )
                    {
                      return "<button type='button' title='Editar' onclick='verinfo_adminsimat("+ data.id +")' class='btn btn-warning btn-xs' ><i class='fa fa-edit'></i></button>"+"&nbsp;"
                    }
                  }
                ]
              });

            });

            </script>
          @endcan

          @can('actualizar_informacion_rectores')
            <div class="box box-solid box-primary collapsed-box"  >
              <div class="box-header with-border" >
                <h3 class="box-title" >Rectores(a)</h3>
                <div class="box-tools pull-right" >
                  <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip">
                    <i class="fa fa-plus"></i></button>
                  </div>
                </div>
                @if(Auth::user()->isRole('rectores'))
                  <span class="help-block"><span class="glyphicon glyphicon-tags"></span>       *Señor rector(a), en este ítem podrás actualizar tu información de contacto.</span>
                @else
                  <span class="help-block"><span class="glyphicon glyphicon-tags"></span>       *En este ítem podrás actualizar la información de contacto de los rectores(a) del departamento de Antioquia.</span>
                @endif
                <div class="box-body"  >
                  <div id"tabla-responsive" class="table-responsive" >

                    <a href="javascript:void(0);"  onclick="cargar_formulario(44);" style="margin-bottom:10px;" class="btn btn-primary">Ingresar rector(a)</a>

                    <table id="tablarectores"  class="mdl-data-table" style="width:100%">
                      <thead>
                        <tr>
                          <Th>Municipio</Th>
                          <Th>Establecimiento</Th>
                          <Th>Nombres</Th>
                          <Th>Apellidos</Th>
                          <Th>Celular</Th>
                          <Th>Teléfono</Th>
                          <Th>Correo personal</Th>
                          <Th>Correo establecimiento</Th>
                          <Th>Acción</Th>
                        </tr>
                      </thead>
                    </table>

                  </div>
                </div>
              </div>
              <script>

              $(document).ready(function() {
                $('#tablarectores').DataTable({
                  processing: true,
                  serverSide: true,
                  pageLength: 10,
                  language: {
                    "url": '{!! asset('/plugins/datatables/latino.json') !!}'
                  } ,
                  ajax: '{{ route('actualizar.rectores') }}',
                  columns: [
                    { data: 'municipio', name: 'municipio' },
                    { data: 'nombre_establecimiento', name: 'nombre_establecimiento' },
                    { data: 'nombre', name: 'nombre' },
                    { data: 'apellido', name: 'apellido' },
                    { data: 'celular', name: 'celular' },
                    { data: 'telefono', name: 'telefono' },
                    { data: 'correo', name: 'correo' },
                    { data: 'correo_establecimiento', name: 'correo_establecimiento' },
                    { data: null,  render: function ( data, type, row )
                      {
                        return "<button type='button' title='Editar' onclick='verinfo_rectores("+ data.id +")' class='btn btn-warning btn-xs' ><i class='fa fa-edit'></i></button>"+"&nbsp;"+
                        "@role('administrador_sistema')<button type='button'  onclick='borrar_rectores(" + data.id +")' class='btn  btn-danger btn-xs' ><i class='fa fa-fw fa fa-trash-o' title='Eliminar'></i></button>@endrole"
                      }
                    }
                  ]
                });

              });

              </script>
            @endcan


            @can('actualizar_informacion_enlacetic')
              <div class="box box-solid box-primary collapsed-box" >
                <div class="box-header with-border" >
                  <h3 class="box-title" >Enlaces TIC</h3>
                  <div class="box-tools pull-right" >
                    <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip">
                      <i class="fa fa-plus"></i></button>
                    </div>
                  </div>
                  @if(Auth::user()->isRole('enlace_tic'))
                    <span class="help-block"><span class="glyphicon glyphicon-tags"></span>        *Señor(a) enlace TIC, en este ítem podrás actualizar tu información de contacto.</span>
                  @else
                    <span class="help-block"><span class="glyphicon glyphicon-tags"></span>        *En este ítem podrás actualizar la información de contacto de los enlaces TIC del departamento de Antioquia.</span>
                  @endif
                  <div class="box-body"  >

                    <a href="javascript:void(0);" style="margin-bottom:10px;"  class="btn btn-primary" onclick="cargar_formulario(45);">Ingresar enlace TIC</a>

                    <div id"tabla-responsive" class="table-responsive" >

                      <table id="tablaEnlacestic"  class="mdl-data-table" style="width:100%">
                        <thead>
                          <tr>
                            <Th>Municipio</Th>
                            <Th>Nombres</Th>
                            <Th>Apellidos</Th>
                            <Th>Celular</Th>
                            <Th>Teléfono</Th>
                            <Th>Correo personal</Th>
                            <Th>Cargo</Th>
                            <Th>Dirección</Th>
                            <Th>Acción</th>
                            </tr>
                          </thead>
                        </table>

                      </div>
                    </div>
                  </div>


                  <script>

                  $(document).ready(function() {
                    $('#tablaEnlacestic').DataTable({
                      processing: true,
                      serverSide: true,
                      pageLength: 10,
                      language: {
                        "url": '{!! asset('/plugins/datatables/latino.json') !!}'
                      } ,
                      ajax: '{{ route('actualizar.enlacestic') }}',
                      columns: [
                        { data: 'NombreMunicipio', name: 'NombreMunicipio' },
                        { data: 'nombre_enlacetic', name: 'nombre_enlacetic' },
                        { data: 'apellido_enlacetic', name: 'apellido_enlacetic' },
                        { data: 'celular_enlacetic', name: 'celular_enlacetic' },
                        { data: 'telefono_enlacetic', name: 'telefono_enlacetic' },
                        { data: 'correo_enlacetic', name: 'correo_enlacetic' },
                        { data: 'cargo_enlacetic', name: 'cargo_enlacetic' },
                        { data: 'direccion_enlacetic', name: 'direccion_enlacetic' },
                        { data: null,  render: function ( data, type, row )
                          {
                            return "<button type='button' title='Editar' onclick='verinfo_enlacetic("+ data.id +")' class='btn btn-warning btn-xs' ><i class='fa fa-edit'></i></button>"+"&nbsp;"+
                            "@role('administrador_sistema')<button type='button'  onclick='borrar_enlacestic(" + data.id +")' class='btn  btn-danger btn-xs' ><i class='fa fa-fw fa fa-trash-o' title='Eliminar'></i></button>@endrole"
                          }
                        }
                      ]
                    });

                  });

                  </script>
                @endcan


                @if(Auth::user()->isRole('administrador_sistema') || Auth::user()->isRole('Administrador_infraestructura_educativa') || Auth::user()->isRole('Administrador_infraestructura_tecnologica') || Auth::user()->isRole('Administrador_matricula') || Auth::user()->isRole('Administrador_due') || Auth::user()->isRole('Administrador_pruebas_saber'))
                  <div class="box box-solid box-primary collapsed-box" >
                    <div class="box-header with-border" >
                      <h3 class="box-title" >Directores(a) de núcleo</h3>
                      <div class="box-tools pull-right" >
                        <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip">
                          <i class="fa fa-plus"></i></button>
                        </div>
                      </div>
                      <span class="help-block"><span class="glyphicon glyphicon-tags"></span>       *En este ítem podrás actualizar la información de contacto de los directores(a) de núcleo del departamento de Antioquia.</span>
                      <div class="box-body"  >

                        <a href="/form_ingresar_directornucleo" style="margin-bottom:10px;"  class="btn btn-primary">Ingresar directores(a) de núcleo</a>

                        <div id"tabla-responsive" class="table-responsive" >

                          <table id="tablaJefesnucleos"  class="mdl-data-table" style="width:100%">
                            <thead>
                              <tr>
                                <Th>Municipio</Th>
                                <Th>Nombres</Th>
                                <Th>Apellidos</Th>
                                <Th>Celular</Th>
                                <Th>Teléfono</Th>
                                <Th>Correo personal</Th>
                                <Th>Cargo</Th>
                                <Th>Dirección</Th>
                                <Th>Acción</Th>
                              </tr>
                            </thead>
                          </table>

                        </div>
                      </div>
                    </div>
                    <script>

                    $(document).ready(function() {
                      $('#tablaJefesnucleos').DataTable({
                        processing: true,
                        serverSide: true,
                        pageLength: 10,
                        language: {
                          "url": '{!! asset('/plugins/datatables/latino.json') !!}'
                        } ,
                        ajax: '{{ route('actualizar.Jefesnucleos') }}',
                        columns: [
                          { data: 'NombreMunicipio', name: 'NombreMunicipio' },
                          { data: 'nombre', name: 'nombre' },
                          { data: 'apellido', name: 'apellido' },
                          { data: 'celular', name: 'celular' },
                          { data: 'telefono', name: 'telefono' },
                          { data: 'correo', name: 'correo' },
                          { data: 'cargo', name: 'cargo' },
                          { data: 'direccion', name: 'direccion' },
                          { data: null,  render: function ( data, type, row )
                            {
                              return "<button type='button' title='Editar' onclick='verinfo_jefesnucleos("+ data.id +")' class='btn btn-warning btn-xs' ><i class='fa fa-edit'></i></button>"+"&nbsp;"+
                              "@role('administrador_sistema')<button type='button'  onclick='borrar_directornucleo(" + data.id +")' class='btn  btn-danger btn-xs' ><i class='fa fa-fw fa fa-trash-o' title='Eliminar'></i></button>@endrole"
                            }
                          }
                        ]
                      });

                    });

                    </script>
                  @endif

                  @can('directorio_municipal')
                    <div class="box box-solid box-primary collapsed-box" >
                      <div class="box-header with-border" >
                        <h3 class="box-title" >Secretarios(a) de planeación</h3>
                        <div class="box-tools pull-right" >
                          <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip">
                            <i class="fa fa-plus"></i></button>
                          </div>
                        </div>
                        <span class="help-block"><span class="glyphicon glyphicon-tags"></span>       *En este ítem podrás actualizar la información de contacto de los Secretarios(a) de planeación del departamento de Antioquia.</span>
                        <div class="box-body">

                          <a href="javascript:void(0);" style="margin-bottom:10px;" onclick="cargar_formulario(46);" class="btn btn-primary">Ingresar secretario(a) de planeación</a>

                          <div id"tabla-responsive" class="table-responsive" >

                            <table id="tablaJefesplaneacion"  class="mdl-data-table" style="width:100%">
                              <thead>
                                <tr>
                                  <Th>Municipio</Th>
                                  <Th>Nombres</Th>
                                  <Th>Apellidos</Th>
                                  <Th>Celular</Th>
                                  <Th>Teléfono</Th>
                                  <Th>Correo personal</Th>
                                  <Th>Cargo</Th>
                                  <Th>Dirección</Th>
                                  <Th>Acción></Th>
                                </tr>
                              </thead>
                            </table>

                          </div>
                        </div>
                      </div>
                      <script>

                      $(document).ready(function() {
                        $('#tablaJefesplaneacion').DataTable({
                          processing: true,
                          serverSide: true,
                          pageLength: 10,
                          language: {
                            "url": '{!! asset('/plugins/datatables/latino.json') !!}'
                          } ,
                          ajax: '{{ route('actualizar.Jefesplaneacion') }}',
                          columns: [
                            { data: 'NombreMunicipio', name: 'NombreMunicipio' },
                            { data: 'nombre', name: 'nombre' },
                            { data: 'apellido', name: 'apellido' },
                            { data: 'celular', name: 'celular' },
                            { data: 'telefono', name: 'telefono' },
                            { data: 'correo', name: 'correo' },
                            { data: 'cargo', name: 'cargo' },
                            { data: 'direccion', name: 'direccion' },
                            { data: null,  render: function ( data, type, row )
                              {
                                return "<button type='button' title='Editar' onclick='verinfo_jefesplaneacion("+ data.id +")' class='btn btn-warning btn-xs' ><i class='fa fa-edit'></i></button>"+"&nbsp;"+
                                "@role('administrador_sistema')<button type='button'  onclick='borrar_jefesplaneacion(" + data.id +")' class='btn  btn-danger btn-xs' ><i class='fa fa-fw fa fa-trash-o' title='Eliminar'></i></button>@endrole"
                              }
                            }
                          ]
                        });

                      });

                      </script>
                    @endcan

                    @can('actualizar_informacion_enlacetic')
                      <div class="box box-solid box-primary collapsed-box" >
                        <div class="box-header with-border" >
                          <h3 class="box-title" >Comités TIC</h3>
                          <div class="box-tools pull-right" >
                            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip">
                              <i class="fa fa-plus"></i></button>
                            </div>
                          </div>
                          <span class="help-block"><span class="glyphicon glyphicon-tags"></span>       *En este ítem podrás actualizar la información de contacto de los comités TIC del departamento de Antioquia.</span>
                          <div class="box-body">


                            <a href="javascript:void(0);" style="margin-bottom:10px;" onclick="cargar_formulario(48);" class="btn btn-primary">Ingresar integrante</a>


                            <div id"tabla-responsive" class="table-responsive" >

                              <table id="tablacomitestic"  class="mdl-data-table" style="width:100%">
                                <thead>
                                  <tr>
                                    <Th>Municipio</Th>
                                    <Th>Nombres</Th>
                                    <Th>Apellidos</Th>
                                    <Th>Celular</Th>
                                    <Th>Teléfono</Th>
                                    <Th>Correo personal</Th>
                                    <Th>Sector</Th>
                                    <Th>Cargo</Th>
                                    <Th>Dirección</Th>
                                    <th>Comité</th>
                                    <Th>Acción</Th>
                                  </tr>
                                </thead>
                              </table>

                            </div>
                          </div>
                        </div>
                        <script>

                        $(document).ready(function() {
                          $('#tablacomitestic').DataTable({
                            processing: true,
                            serverSide: true,
                            pageLength: 10,
                            language: {
                              "url": '{!! asset('/plugins/datatables/latino.json') !!}'
                            } ,
                            ajax: '{{ route('actualizar.comitestic') }}',
                            columns: [
                              { data: 'NombreMunicipio', name: 'NombreMunicipio' },
                              { data: 'nombre', name: 'nombre' },
                              { data: 'apellido', name: 'apellido' },
                              { data: 'celular', name: 'celular' },
                              { data: 'telefono', name: 'telefono' },
                              { data: 'correo', name: 'correo' },
                              { data: 'sector', name: 'sector' },
                              { data: 'cargo', name: 'cargo' },
                              { data: 'direccion', name: 'direccion' },
                              { data: 'comite', name: 'comite' },
                              { data: null,  render: function ( data, type, row )
                                {
                                  return "<button type='button' title='Editar' onclick='verinfo_comitestic("+ data.id +")' class='btn btn-warning btn-xs' ><i class='fa fa-edit'></i></button>"+"&nbsp;"+
                                  "@role('administrador_sistema')<button type='button'  onclick='borrado_comitestic(" + data.id +")' class='btn  btn-danger btn-xs' ><i class='fa fa-fw fa fa-trash-o' title='Eliminar'></i></button>@endrole"
                                }
                              }
                            ]
                          });

                        });

                        </script>
                      @endcan

                      @can('directorio_municipal')
                        <div class="box box-solid box-primary collapsed-box" >
                          <div class="box-header with-border" >
                            <h3 class="box-title" >Junta municipal de educación JUME</h3>
                            <div class="box-tools pull-right" >
                              <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip">
                                <i class="fa fa-plus"></i></button>
                              </div>
                            </div>
                            <span class="help-block"><span class="glyphicon glyphicon-tags"></span>       *En este ítem podrás actualizar la información de contacto de la juntas municipales de educación JUME del departamento de Antioquia.</span>
                            <div class="box-body"  >
                              <div id"tabla-responsive" class="table-responsive" >


                                <a href="javascript:void(0);" style="margin-bottom:10px;" onclick="cargar_formulario(48);" class="btn btn-primary">Ingresar integrante</a>


                                <table id="tablajumes"  class="mdl-data-table" style="width:100%">
                                  <thead>
                                    <tr>
                                      <Th>Municipio</Th>
                                      <Th>Nombres</Th>
                                      <Th>Apellidos</Th>
                                      <Th>Celular</Th>
                                      <Th>Teléfono</Th>
                                      <Th>Correo personal</Th>
                                      <Th>Sector</Th>
                                      <Th>Cargo</Th>
                                      <Th>Dirección</Th>
                                      <th>Comité</th>
                                      <Th>Acción</Th>
                                    </tr>
                                  </thead>
                                </table>

                              </div>
                            </div>
                          </div>
                          <script>

                          $(document).ready(function() {
                            $('#tablajumes').DataTable({
                              processing: true,
                              serverSide: true,
                              pageLength: 10,
                              language: {
                                "url": '{!! asset('/plugins/datatables/latino.json') !!}'
                              } ,
                              ajax: '{{ route('actualizar.jumes') }}',
                              columns: [
                                { data: 'NombreMunicipio', name: 'NombreMunicipio' },
                                { data: 'nombre', name: 'nombre' },
                                { data: 'apellido', name: 'apellido' },
                                { data: 'celular', name: 'celular' },
                                { data: 'telefono', name: 'telefono' },
                                { data: 'correo', name: 'correo' },
                                { data: 'sector', name: 'sector' },
                                { data: 'cargo', name: 'cargo' },
                                { data: 'direccion', name: 'direccion' },
                                { data: 'comite', name: 'comite' },
                                { data: null,  render: function ( data, type, row )
                                  {
                                    return "<button type='button' title='Editar' onclick='verinfo_jume("+ data.id +")' class='btn btn-warning btn-xs' ><i class='fa fa-edit'></i></button>"+"&nbsp;"+
                                    "@role('administrador_sistema')<button type='button'  onclick='borrado_jume(" + data.id +")' class='btn  btn-danger btn-xs' ><i class='fa fa-fw fa fa-trash-o' title='Eliminar'></i></button>@endrole"
                                  }
                                }
                              ]
                            });

                          });

                          </script>

                          <div class="box box-solid box-primary collapsed-box" >
                            <div class="box-header with-border" >
                              <h3 class="box-title" >Comités de cupos</h3>
                              <div class="box-tools pull-right" >
                                <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip">
                                  <i class="fa fa-plus"></i></button>
                                </div>
                              </div>
                              <span class="help-block"><span class="glyphicon glyphicon-tags"></span>       *En este ítem podrás actualizar la información de contacto de los comités de cupos del departamento de Antioquia.</span>
                              <div class="box-body"  >


                                <a href="javascript:void(0);" style="margin-bottom:10px;" onclick="cargar_formulario(48);" class="btn btn-primary">Ingresar integrante</a>


                                <div id"tabla-responsive" class="table-responsive" >

                                  <table id="tablacupos"  class="mdl-data-table" style="width:100%">
                                    <thead>
                                      <tr>
                                        <Th>Municipio</Th>
                                        <Th>Nombres</Th>
                                        <Th>Apellidos</Th>
                                        <Th>Celular</Th>
                                        <Th>Teléfono</Th>
                                        <Th>Correo personal</Th>
                                        <Th>Sector</Th>
                                        <Th>Cargo</Th>
                                        <Th>Dirección</Th>
                                        <th>Comité</th>
                                        <Th>Acción</Th>
                                      </tr>
                                    </thead>
                                  </table>

                                </div>
                              </div>
                            </div>
                            <script>

                            $(document).ready(function() {
                              $('#tablacupos').DataTable({
                                processing: true,
                                serverSide: true,
                                pageLength: 10,
                                language: {
                                  "url": '{!! asset('/plugins/datatables/latino.json') !!}'
                                } ,
                                ajax: '{{ route('actualizar.cupos') }}',
                                columns: [
                                  { data: 'NombreMunicipio', name: 'NombreMunicipio' },
                                  { data: 'nombre', name: 'nombre' },
                                  { data: 'apellido', name: 'apellido' },
                                  { data: 'celular', name: 'celular' },
                                  { data: 'telefono', name: 'telefono' },
                                  { data: 'correo', name: 'correo' },
                                  { data: 'sector', name: 'sector' },
                                  { data: 'cargo', name: 'cargo' },
                                  { data: 'direccion', name: 'direccion' },
                                  { data: 'comite', name: 'comite' },
                                  { data: null,  render: function ( data, type, row )
                                    {
                                      return "<button type='button' title='Editar' onclick='verinfo_comitescupos("+ data.id +")' class='btn btn-warning btn-xs' ><i class='fa fa-edit'></i></button>"+"&nbsp;"+
                                      "@role('administrador_sistema')<button type='button'  onclick='borrado_comitescupos(" + data.id +")' class='btn  btn-danger btn-xs' ><i class='fa fa-fw fa fa-trash-o' title='Eliminar'></i></button>@endrole"
                                    }
                                  }
                                ]
                              });

                            });

                            </script>
                          @endcan

                          @can('actualizar_informacion_enlacetic')
                            <div class="box box-solid box-primary collapsed-box" >
                              <div class="box-header with-border" >
                                <h3 class="box-title" >Documentos</h3>
                                <div class="box-tools pull-right" >
                                  <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip">
                                    <i class="fa fa-plus"></i></button>
                                  </div>
                                </div>
                                <span class="help-block"><span class="glyphicon glyphicon-tags"></span>       *En este ítem podrás actualizar la información de la documentación de los comités: TIC y de Cupos, también, de la junta municipal de educación JUME y de los enlaces TIC.</span>
                                <div class="box-body"  >

                                  <a href="javascript:void(0);" style="margin-bottom:10px;" onclick="cargar_formulario(51);" class="btn btn-primary">Ingresar documentos</a>

                                  <div id"tabla-responsive" class="table-responsive" >

                                    <table id="dtdocumentacion" class="mdl-data-table" style="width:100%">
                                      <thead>
                                        <tr>
                                          <th class="col-md-5 col-xs-5">Municipio</th>
                                          <th class="col-md-3 col-xs-3">Documento</th>
                                          <th class="col-md-3 col-xs-3">Número de documento</th>
                                          <th class="col-md-3 col-xs-3">Fecha documento</th>
                                          <th class="col-md-4 col-xs-4">Ver documento</th>
                                          <th style="float:" class="col-md-4 col-xs-4">Acción</th>
                                        </tr>
                                      </thead>
                                      <tbody>
                                        @foreach ($datos as $dato)
                                          <tr>
                                            <td>{{$dato->nombre_municipio}}</td>
                                            <td>{{$dato->comite_junta}}</td>
                                            <td>{{$dato->numero_documento}}</td>
                                            <td>{{$dato->fecha_documento}}</td>
                                            @if ($dato->documento != "")
                                              <td style="text-align: center;"><a href="{{url("/documento/$dato->documento")}}" title="Ver documento"  target='_blank'><i style="font-size:25px; color:red;" class="fa fa-file-pdf-o"></i></a></td>
                                            @else
                                              <td>Sin Anexo</td>
                                            @endif
                                            <td>
                                              <button type='button' title='Editar' onclick='verinfo_documentos({{$dato->id}})' class='btn btn-warning btn-xs' ><i class='fa fa-edit'></i></button>
                                              @role('administrador_sistema')<button type='button' style="float:right;"  onclick='borrado_documentos({{$dato->id}})' class='btn  btn-danger btn-xs' ><i class='fa fa-fw fa fa-trash-o' title='Eliminar'></i></button>@endrole
                                            </td>
                                          </tr>
                                        @endforeach
                                      </tbody>
                                    </table>
                                    <script type="text/javascript">

                                    $(document).ready(function () {
                                      $('#dtdocumentacion').DataTable({
                                        language: {
                                          "url": '{!! asset('/plugins/datatables/latino.json') !!}'
                                        } ,
                                      });

                                      $('.dataTables_length').addClass('bs-select');
                                    });

                                    </script>

                                  </div>
                                </div>
                              </div>

                            @endcan
                          @endsection
