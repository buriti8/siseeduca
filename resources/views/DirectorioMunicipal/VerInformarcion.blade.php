@extends('layouts.app')

@section('htmlheader_title')
  Ver información
@endsection

@section('main-content')

  <script type="text/javascript">
  function ocultar_informacion(){
    //función encargada de ocualtar o cerrar el modal de información que se activa al ingresar al sistema.
    document.getElementById('verInformacion').style.display = 'none';
  }
</script>

@if(Auth::user()->isRole('rectores'))
<!-- Modal content-->
<!-- modal encargado de mostrar cualquier tipo de información por parte de la gobernación de antioquia
al usuario del sistema. el modal se activa cada vez que el usuario ingrese al submódulo-->
@if($contador>=1)
<div class="modal fade in" id='verInformacion'  role="dialog" style="display: block; padding-right: 17px;">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">
          <strong>Información</strong>
        </h4>
      </div>
      <div align="justify" class="modal-body">
        Señor(a) rector(a), por favor ingrese su información de contacto, con el objetivo de mantener la información del directorio municipal actualizada, para el beneficio de todos.</div>
      <div class="modal-footer">
        <a href="/form_contactos_rector" class="btn btn-primary" value=" ">Ingresar información</a>
        <button onclick="ocultar_informacion()" class="btn btn-danger" data-dismiss="modal" type="button">Cerrar</button>
      </div>
    </div>
  </div>
</div>
@endif
@endif

@if(Auth::user()->isRole('enlace_tic'))
<!-- Modal content-->
<!-- modal encargado de mostrar cualquier tipo de información por parte de la gobernación de antioquia
al usuario del sistema. el modal se activa cada vez que el usuario ingrese al submódulo-->
@if($contador>=1)
<div class="modal fade in" id='verInformacion'  role="dialog" style="display: block; padding-right: 17px;">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">
          <strong>Información</strong>
        </h4>
      </div>
      <div align="justify" class="modal-body">
        Señor(a) enlace TIC, por favor ingrese: <strong>{{$informacionPersonal}}</strong> <strong>{{$Comitestic}}</strong> <strong>{{$documentoEnlaceTIC}}</strong> <strong>{{$documentoTIC}}</strong> con el objetivo de mantener la información del directorio municipal actualizada, para el beneficio de todos.</div>
      <div class="modal-footer">
        <a href="/actualizar_directorio" class="btn btn-primary" value=" ">Ingresar información</a>
        <button onclick="ocultar_informacion()" class="btn btn-danger" data-dismiss="modal" type="button">Cerrar</button>
      </div>
    </div>
  </div>
</div>
@endif
@endif


@if(Auth::user()->isRole('secretarios'))
<!-- Modal content-->
<!-- modal encargado de mostrar cualquier tipo de información por parte de la gobernación de antioquia
al usuario del sistema. el modal se activa cada vez que el usuario ingrese al submódulo-->
@if($contador>=1)
<div class="modal fade in" id='verInformacion'  role="dialog" style="display: block; padding-right: 17px;">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">
          <strong>Información</strong>
        </h4>
      </div>
      <div align="justify" class="modal-body">
        Señor(a) secretario(a) de educación, por favor ingrese la información de: <strong>
        {{$rectores}} {{$enlacetic}} {{$documentoEnlaceTIC}} {{$jefeplaneacion}} {{$Comitestic}} {{$Comitescupos}} {{$JUME}} {{$documentoTIC}} {{$documentocupos}} {{$documentoJUME}} {{$DirectorioMunicipal}} </strong> con el objetivo de mantener la información del directorio municipal actualizada, para el beneficio de todos.</div>
      <div class="modal-footer">
        <a href="/actualizar_directorio" class="btn btn-primary" value=" ">Ingresar información</a>
        <button onclick="ocultar_informacion()" class="btn btn-danger" data-dismiss="modal" type="button">Cerrar</button>
      </div>
    </div>
  </div>
</div>
@endif
@endif


<div style="background color: white;">
  <div style="text-align: center; margin-bottom: 20px;" class="container">
    <h3><strong>Directorio municipal de Antioquia</strong></h2>
    </div>
  </div>

  <section  id="contenido_principal">

    <div class="box box-solid box-primary collapsed-box" >
      <div class="box-header with-border" >
        <h3 class="box-title" >Alcaldes </h3>
        <div class="box-tools pull-right" >
          <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip">
            <i class="fa fa-plus"></i></button>
          </div>
        </div>
        <span class="help-block"><span class="glyphicon glyphicon-tags"></span>       *En este ítem encontrarás la información de contacto de los alcaldes del departamento de Antioquia.</span>
        <div class="box-body"  >

          <div id"tabla-responsive" class="table-responsive" >

            <table id="tablaAlcades"  class="mdl-data-table" style="width:100%">
              <thead>
                <tr>
                  <Th>Subregión</Th>
                  <Th>Municipio</Th>
                  <Th>Nombre</Th>
                  <Th>Apellido</Th>
                  <Th>Celular</Th>
                  <Th>Teléfono</Th>
                  <Th>Correo personal</Th>
                  <Th>Correo alcaldía</Th>
                  <Th>Dirección</Th>
                </tr>
              </thead>
            </table>
          </div>
        </div>
      </div>

      <script>

      $(document).ready(function() {
        $('#tablaAlcades').DataTable({
          serverSide: true,
          pageLength: 10,
          dom: 'Bfrtip',
          buttons: [
            {
              extend: 'excel',
              text: 'Exportar a Excel',
              title: 'Alcaldes',
              filename: 'Alcaldes',
            }, {
              extend: 'pdf',
              text: 'Exportar a PDF',
              orientation: 'landscape',
              title: 'Alcaldes',
              filename: 'Alcaldes',
            }
          ],
          language: {
            "url": '{!! asset('/plugins/datatables/latino.json') !!}'
          } ,
          ajax: '{{ route('datatable.alcalde') }}',
          columns: [
            { data: 'NombreSubregion', name: 'NombreSubregion' },
            { data: 'NombreMunicipio', name: 'NombreMunicipio' },
            { data: 'nombre_alcalde', name: 'nombre_alcalde' },
            { data: 'apellido_alcalde', name: 'apellido_alcalde' },
            { data: 'celular_alcalde', name: 'celular_alcalde' },
            { data: 'telefono_alcalde', name: 'telefono_alcalde' },
            { data: 'correo_alcalde', name: 'correo_alcalde' },
            { data: 'correo_alcaldia', name: 'correo_alcaldia' },
            { data: 'direccion_alcaldia', name: 'direccion_alcaldia' },
          ]
        });

      });

      </script>

      <div class="box box-solid box-primary collapsed-box" >
        <div class="box-header with-border" >
          <h3 class="box-title" >Secretarios(a) de educación</h3>
          <div class="box-tools pull-right" >
            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip">
              <i class="fa fa-plus"></i></button>
            </div>
          </div>
          <span class="help-block"><span class="glyphicon glyphicon-tags"></span>       *En este ítem encontrarás la información de contacto de los secretarios(a) de educación del departamento de Antioquia.</span>
          <div class="box-body"  >
            <div id"tabla-responsive" class="table-responsive" >

              <table id="tablaSecretarios"  class="mdl-data-table" style="width:100%">
                <thead>
                  <tr>
                    <Th>Subregión</Th>
                    <Th>Municipio</Th>
                    <Th>Nombre</Th>
                    <Th>Apellido</Th>
                    <Th>Celular</Th>
                    <Th>Teléfono</Th>
                    <Th>Correo personal</Th>
                    <Th>Correo secretaría de educación</Th>
                    <Th>Cargo secretario</Th>
                    <Th>Dirección oficina</Th>
                  </tr>
                </thead>
              </table>

            </div>
          </div>
        </div>

        <script>

        $(document).ready(function() {
          $('#tablaSecretarios').DataTable({
            processing: true,
            serverSide: true,
            pageLength: 10,
            dom: 'Bfrtip',
            buttons: [
              {
                extend: 'excel',
                text: 'Exportar a Excel',
                title: 'Secretarios(a) de educación',
                filename: 'Secretarios(a) de educación',
              }, {
                extend: 'pdf',
                text: 'Exportar a PDF',
                orientation: 'landscape',
                title: 'Secretarios(a) de educación',
                filename: 'Secretarios(a) de educación',
              }
            ],
            language: {
              "url": '{!! asset('/plugins/datatables/latino.json') !!}'
            } ,
            ajax: '{{ route('datatable.secretarios') }}',
            columns: [
              { data: 'NombreSubregion', name: 'NombreSubregion' },
              { data: 'NombreMunicipio', name: 'NombreMunicipio' },
              { data: 'nombre_secretario', name: 'nombre_secretario' },
              { data: 'apellido_secretario', name: 'apellido_secretario' },
              { data: 'celular_secretario', name: 'celular_secretario' },
              { data: 'telefono_secretario', name: 'telefono_secretario' },
              { data: 'correo_secretario', name: 'correo_secretario' },
              { data: 'correo_secretariaeducacion', name: 'correo_secretariaeducacion' },
              { data: 'cargo_secretario', name: 'cargo_secretario' },
              { data: 'direccion_secretario', name: 'direccion_secretario' },
            ]
          });

        });

        </script>

          <div class="box box-solid box-primary collapsed-box" >
            <div class="box-header with-border" >
              <h3 class="box-title" >Administradores(a) SIMAT</h3>
              <div class="box-tools pull-right" >
                <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip">
                  <i class="fa fa-plus"></i></button>
                </div>
              </div>
              <span class="help-block"><span class="glyphicon glyphicon-tags"></span>       *En este ítem encontrarás la información de contacto de los Administradores(a) SIMAT del departamento de Antioquia.</span>
              <div class="box-body"  >
                <div id"tabla-responsive" class="table-responsive" >

                  <table id="tablaAdminsimat"  class="mdl-data-table" style="width:100%">
                    <thead>
                      <tr>
                        <Th>Subregión</Th>
                        <Th>Municipio</Th>
                        <Th>Nombre</Th>
                        <Th>Apellido</Th>
                        <Th>Celular</Th>
                        <Th>Teléfono</Th>
                        <Th>Correo personal</Th>
                        <Th>Cargo</Th>
                        <Th>Dirección oficina</Th>
                      </tr>
                    </thead>
                  </table>

                </div>
              </div>
            </div>

            <script>

            $(document).ready(function() {
              $('#tablaAdminsimat').DataTable({
                processing: true,
                serverSide: true,
                pageLength: 10,
                dom: 'Bfrtip',
                buttons: [
                  {
                    extend: 'excel',
                    text: 'Exportar a Excel',
                    title: 'Administradores(a) SIMAT',
                    filename: 'Administradores(a) SIMAT',
                  }, {
                    extend: 'pdf',
                    text: 'Exportar a PDF',
                    orientation: 'landscape',
                    title: 'Administradores(a) SIMAT',
                    filename: 'Administradores(a) SIMAT',
                  }
                ],
                language: {
                  "url": '{!! asset('/plugins/datatables/latino.json') !!}'
                } ,
                ajax: '{{ route('datatable.Adminsimat') }}',
                columns: [
                  { data: 'NombreSubregion', name: 'NombreSubregion' },
                  { data: 'NombreMunicipio', name: 'NombreMunicipio' },
                  { data: 'nombre_adminsimat', name: 'nombre_adminsimat' },
                  { data: 'apellido_adminsimat', name: 'apellido_adminsimat' },
                  { data: 'celular_adminsimat', name: 'celular_adminsimat' },
                  { data: 'telefono_adminsimat', name: 'telefono_adminsimat' },
                  { data: 'correo_adminsimat', name: 'correo_adminsimat' },
                  { data: 'cargo_adminsimat', name: 'cargo_adminsimat' },
                  { data: 'direccion_adminsimat', name: 'direccion_adminsimat' },
                ]
              });

            });

            </script>

            <div class="box box-solid box-primary collapsed-box"  >
              <div class="box-header with-border" >
                <h3 class="box-title" >Rectores(a)</h3>
                <div class="box-tools pull-right" >
                  <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip">
                    <i class="fa fa-plus"></i></button>
                  </div>
                </div>
                <span class="help-block"><span class="glyphicon glyphicon-tags"></span>       *En este ítem encontrarás la información de contacto de los rectores(a) del departamento de Antioquia.</span>

                <div class="box-body"  >
                  <div id"tabla-responsive" class="table-responsive" >
                    <table id="tablarectores"  class="mdl-data-table" style="width:100%">
                      <thead>
                        <tr>
                          <Th>Subregión</Th>
                          <Th>Municipio</Th>
                          <Th>Establecimiento</Th>
                          <Th>Nombre</Th>
                          <Th>Apellido</Th>
                          <Th>Celular</Th>
                          <Th>Teléfono</Th>
                          <Th>Correo personal</Th>
                          <Th>Correo establecimiento</Th>
                        </tr>
                      </thead>
                    </table>

                  </div>
                </div>
              </div>

              <script>

              $(document).ready(function() {
                $('#tablarectores').DataTable({
                  processing: true,
                  serverSide: true,
                  pageLength: 10,
                  dom: 'Bfrtip',
                  buttons: [
                    {
                      extend: 'excel',
                      text: 'Exportar a Excel',
                      title: 'Rectores(a)',
                      filename: 'Rectores(a)',
                    }, {
                      extend: 'pdf',
                      text: 'Exportar a PDF',
                      orientation: 'landscape',
                      title: 'Rectores(a)',
                      filename: 'Rectores(a)',
                    }
                  ],
                  language: {
                    "url": '{!! asset('/plugins/datatables/latino.json') !!}'
                  } ,
                  ajax: '{{ route('datatable.rectores') }}',
                  columns: [
                    { data: 'NombreSubregion', name: 'NombreSubregion' },
                    { data: 'municipio', name: 'municipio' },
                    { data: 'nombre_establecimiento', name: 'nombre_establecimiento' },
                    { data: 'nombre', name: 'nombre' },
                    { data: 'apellido', name: 'apellido' },
                    { data: 'celular', name: 'celular' },
                    { data: 'telefono', name: 'telefono' },
                    { data: 'correo', name: 'correo' },
                    { data: 'correo_establecimiento', name: 'correo_establecimiento' },
                  ]
                });

              });

              </script>

            <div class="box box-solid box-primary collapsed-box" >
              <div class="box-header with-border" >
                <h3 class="box-title" >Enlaces TIC</h3>
                <div class="box-tools pull-right" >
                  <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip">
                    <i class="fa fa-plus"></i></button>
                  </div>
                </div>
                <span class="help-block"><span class="glyphicon glyphicon-tags"></span>        *En este ítem encontrarás la información de contacto de los enlaces TIC del departamento de Antioquia.</span>
                <div class="box-body"  >
                  <div id"tabla-responsive" class="table-responsive" >

                    <a href="listado_documentos" style="float: right; margin-bottom:10px;"  class="btn btn-primary">Ver documentos</a>


                    <table id="tablaEnlacestic"  class="mdl-data-table" style="width:100%">
                      <thead>
                        <tr>
                          <Th>Subregión</Th>
                          <Th>Municipio</Th>
                          <Th>Nombre</Th>
                          <Th>Apellido</Th>
                          <Th>Celular</Th>
                          <Th>Teléfono</Th>
                          <Th>Correo personal</Th>
                          <Th>Cargo</Th>
                          <Th>Dirección</Th>
                        </tr>
                      </thead>
                    </table>

                  </div>
                </div>
              </div>


              <script>

              $(document).ready(function() {
                $('#tablaEnlacestic').DataTable({
                  processing: true,
                  serverSide: true,
                  pageLength: 10,
                  dom: 'Bfrtip',
                  buttons: [
                    {
                      extend: 'excel',
                      text: 'Exportar a Excel',
                      title: 'Enlaces TIC',
                      filename: 'Enlaces TIC',
                    }, {
                      extend: 'pdf',
                      text: 'Exportar a PDF',
                      orientation: 'landscape',
                      title: 'Enlaces TIC',
                      filename: 'Enlaces TIC',
                    }
                  ],
                  language: {
                    "url": '{!! asset('/plugins/datatables/latino.json') !!}'
                  } ,
                  ajax: '{{ route('datatable.enlacestic') }}',
                  columns: [
                    { data: 'NombreSubregion', name: 'NombreSubregion' },
                    { data: 'NombreMunicipio', name: 'NombreMunicipio' },
                    { data: 'nombre_enlacetic', name: 'nombre_enlacetic' },
                    { data: 'apellido_enlacetic', name: 'apellido_enlacetic' },
                    { data: 'celular_enlacetic', name: 'celular_enlacetic' },
                    { data: 'telefono_enlacetic', name: 'telefono_enlacetic' },
                    { data: 'correo_enlacetic', name: 'correo_enlacetic' },
                    { data: 'cargo_enlacetic', name: 'cargo_enlacetic' },
                    { data: 'direccion_enlacetic', name: 'direccion_enlacetic' },
                  ]
                });

              });

              </script>



              <div class="box box-solid box-primary collapsed-box" >
                <div class="box-header with-border" >
                  <h3 class="box-title" >Directores(a) de núcleo</h3>
                  <div class="box-tools pull-right" >
                    <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip">
                      <i class="fa fa-plus"></i></button>
                    </div>
                  </div>
                  <span class="help-block"><span class="glyphicon glyphicon-tags"></span>       *En este ítem encontrarás la información de contacto de los directores(a) de núcleo del departamento de Antioquia.</span>
                  <div class="box-body"  >
                    <div id"tabla-responsive" class="table-responsive" >

                      <table id="tablaJefesnucleos"  class="mdl-data-table" style="width:100%">
                        <thead>
                          <tr>
                            <Th>Subregión</Th>
                            <Th>Municipio</Th>
                            <Th>Nombre</Th>
                            <Th>Apellido</Th>
                            <Th>Celular</Th>
                            <Th>Teléfono</Th>
                            <Th>Correo personal</Th>
                            <Th>Cargo</Th>
                            <Th>Dirección</Th>
                          </tr>
                        </thead>
                      </table>

                    </div>
                  </div>
                </div>
                <script>

                $(document).ready(function() {
                  $('#tablaJefesnucleos').DataTable({
                    processing: true,
                    serverSide: true,
                    pageLength: 10,
                    dom: 'Bfrtip',
                    buttons: [
                      {
                        extend: 'excel',
                        text: 'Exportar a Excel',
                        title: 'Directores(a) de núcleo',
                        filename: 'Directores(a) de núcleo',
                      }, {
                        extend: 'pdf',
                        text: 'Exportar a PDF',
                        orientation: 'landscape',
                        title: 'Directores(a) de núcleo',
                        filename: 'Directores(a) de núcleo',
                      }
                    ],
                    language: {
                      "url": '{!! asset('/plugins/datatables/latino.json') !!}'
                    } ,
                    ajax: '{{ route('datatable.Jefesnucleos') }}',
                    columns: [
                      { data: 'NombreSubregion', name: 'NombreSubregion' },
                      { data: 'NombreMunicipio', name: 'NombreMunicipio' },
                      { data: 'nombre', name: 'nombre' },
                      { data: 'apellido', name: 'apellido' },
                      { data: 'celular', name: 'celular' },
                      { data: 'telefono', name: 'telefono' },
                      { data: 'correo', name: 'correo' },
                      { data: 'cargo', name: 'cargo' },
                      { data: 'direccion', name: 'direccion' },
                    ]
                  });

                });

                </script>


                <div class="box box-solid box-primary collapsed-box" >
                  <div class="box-header with-border" >
                    <h3 class="box-title" >Secretarios(a) de planeación</h3>
                    <div class="box-tools pull-right" >
                      <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip">
                        <i class="fa fa-plus"></i></button>
                      </div>
                    </div>
                    <span class="help-block"><span class="glyphicon glyphicon-tags"></span>       *En este ítem encontrarás la información de contacto de los secretario(a) de planeación del departamento de Antioquia.</span>
                    <div class="box-body"  >
                      <div id"tabla-responsive" class="table-responsive" >

                        <table id="tablaJefesplaneacion"  class="mdl-data-table" style="width:100%">
                          <thead>
                            <tr>
                              <Th>Subregión</Th>
                              <Th>Municipio</Th>
                              <Th>Nombre</Th>
                              <Th>Apellido</Th>
                              <Th>Celular</Th>
                              <Th>Teléfono</Th>
                              <Th>Correo personal</Th>
                              <Th>Cargo</Th>
                              <Th>Dirección</Th>
                            </tr>
                          </thead>
                        </table>

                      </div>
                    </div>
                  </div>
                  <script>

                  $(document).ready(function() {
                    $('#tablaJefesplaneacion').DataTable({
                      processing: true,
                      serverSide: true,
                      pageLength: 10,
                      dom: 'Bfrtip',
                      buttons: [
                        {
                          extend: 'excel',
                          text: 'Exportar a Excel',
                          title: 'Secretarios(a) de planeación',
                          filename: 'Secretarios(a) de planeación',
                        }, {
                          extend: 'pdf',
                          text: 'Exportar a PDF',
                          orientation: 'landscape',
                          title: 'Secretarios(a) de planeación',
                          filename: 'Secretarios(a) de planeación',
                        }
                      ],
                      language: {
                        "url": '{!! asset('/plugins/datatables/latino.json') !!}'
                      } ,
                      ajax: '{{ route('datatable.Jefesplaneacion') }}',
                      columns: [
                        { data: 'NombreSubregion', name: 'NombreSubregion' },
                        { data: 'NombreMunicipio', name: 'NombreMunicipio' },
                        { data: 'nombre', name: 'nombre' },
                        { data: 'apellido', name: 'apellido' },
                        { data: 'celular', name: 'celular' },
                        { data: 'telefono', name: 'telefono' },
                        { data: 'correo', name: 'correo' },
                        { data: 'cargo', name: 'cargo' },
                        { data: 'direccion', name: 'direccion' },
                      ]
                    });

                  });

                  </script>

                  <div class="box box-solid box-primary collapsed-box" >
                    <div class="box-header with-border" >
                      <h3 class="box-title" >Comités TIC</h3>
                      <div class="box-tools pull-right" >
                        <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip">
                          <i class="fa fa-plus"></i></button>
                        </div>
                      </div>
                      <span class="help-block"><span class="glyphicon glyphicon-tags"></span>       *En este ítem encontrarás la información de contacto de los comités TIC del departamento de Antioquia.</span>
                      <div class="box-body">

                        <div id"tabla-responsive" class="table-responsive" >
                          <a href="listado_documentos" style="float: right; margin-bottom:10px;"  class="btn btn-primary" onclick="cargar_formulario(31);">Ver documentos</a>

                          <table id="tablacomitestic"  class="mdl-data-table" style="width:100%">
                            <thead>
                              <tr>
                                <Th>Subregión</Th>
                                <Th>Municipio</Th>
                                <Th>Nombre</Th>
                                <Th>Apellido</Th>
                                <Th>Celular</Th>
                                <Th>Teléfono</Th>
                                <Th>Correo personal</Th>
                                <Th>Sector</Th>
                                <Th>Cargo</Th>
                                <Th>Dirección</Th>
                                <th>Comité</th>
                              </tr>
                            </thead>
                          </table>

                        </div>
                      </div>
                    </div>
                    <script>

                    $(document).ready(function() {
                      $('#tablacomitestic').DataTable({
                        processing: true,
                        serverSide: true,
                        pageLength: 10,
                        dom: 'Bfrtip',
                        buttons: [
                          {
                            extend: 'excel',
                            text: 'Exportar a Excel',
                            title: 'Comités TIC',
                            filename: 'Comités TIC',
                          }, {
                            extend: 'pdf',
                            text: 'Exportar a PDF',
                            orientation: 'landscape',
                            title: 'Comités TIC',
                            filename: 'Comités TIC',
                          }
                        ],
                        language: {
                          "url": '{!! asset('/plugins/datatables/latino.json') !!}'
                        } ,
                        ajax: '{{ route('datatable.comitestic') }}',
                        columns: [
                          { data: 'NombreSubregion', name: 'NombreSubregion' },
                          { data: 'NombreMunicipio', name: 'NombreMunicipio' },
                          { data: 'nombre', name: 'nombre' },
                          { data: 'apellido', name: 'apellido' },
                          { data: 'celular', name: 'celular' },
                          { data: 'telefono', name: 'telefono' },
                          { data: 'correo', name: 'correo' },
                          { data: 'sector', name: 'sector' },
                          { data: 'cargo', name: 'cargo' },
                          { data: 'direccion', name: 'direccion' },
                          { data: 'comite', name: 'comite' },
                        ]
                      });

                    });

                    </script>

                    <div class="box box-solid box-primary collapsed-box" >
                      <div class="box-header with-border" >
                        <h3 class="box-title" >Junta municipal de educación JUME</h3>
                        <div class="box-tools pull-right" >
                          <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip">
                            <i class="fa fa-plus"></i></button>
                          </div>
                        </div>
                        <span class="help-block"><span class="glyphicon glyphicon-tags"></span>       *En este ítem encontrarás la información de contacto de los comités jume del departamento de Antioquia.</span>
                        <div class="box-body"  >
                          <div id"tabla-responsive" class="table-responsive" >
                            <a href="listado_documentos" style="float: right; margin-bottom:10px;"  class="btn btn-primary" onclick="cargar_formulario(31);">Ver documentos</a>

                            <table id="tablajumes"  class="mdl-data-table" style="width:100%">
                              <thead>
                                <tr>
                                  <Th>Subregión</Th>
                                  <Th>Municipio</Th>
                                  <Th>Nombre</Th>
                                  <Th>Apellido</Th>
                                  <Th>Celular</Th>
                                  <Th>Teléfono</Th>
                                  <Th>Correo personal</Th>
                                  <Th>Sector</Th>
                                  <Th>Cargo</Th>
                                  <Th>Dirección</Th>
                                  <th>Comité</th>
                                </tr>
                              </thead>
                            </table>

                          </div>
                        </div>
                      </div>
                      <script>

                      $(document).ready(function() {
                        $('#tablajumes').DataTable({
                          processing: true,
                          serverSide: true,
                          pageLength: 10,
                          dom: 'Bfrtip',
                          buttons: [
                            {
                              extend: 'excel',
                              text: 'Exportar a Excel',
                              title: 'Junta municipal de educación JUME',
                              filename: 'JUME',
                            }, {
                              extend: 'pdf',
                              text: 'Exportar a PDF',
                              orientation: 'landscape',
                              title: 'Junta municipal de educación JUME',
                              filename: 'JUME',
                            }
                          ],
                          language: {
                            "url": '{!! asset('/plugins/datatables/latino.json') !!}'
                          } ,
                          ajax: '{{ route('datatable.jumes') }}',
                          columns: [
                            { data: 'NombreSubregion', name: 'NombreSubregion' },
                            { data: 'NombreMunicipio', name: 'NombreMunicipio' },
                            { data: 'nombre', name: 'nombre' },
                            { data: 'apellido', name: 'apellido' },
                            { data: 'celular', name: 'celular' },
                            { data: 'telefono', name: 'telefono' },
                            { data: 'correo', name: 'correo' },
                            { data: 'sector', name: 'sector' },
                            { data: 'cargo', name: 'cargo' },
                            { data: 'direccion', name: 'direccion' },
                            { data: 'comite', name: 'comite' },
                          ]
                        });

                      });

                      </script>

                      <div class="box box-solid box-primary collapsed-box" >
                        <div class="box-header with-border" >
                          <h3 class="box-title" >Comités de cupos</h3>
                          <div class="box-tools pull-right" >
                            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip">
                              <i class="fa fa-plus"></i></button>
                            </div>
                          </div>
                          <span class="help-block"><span class="glyphicon glyphicon-tags"></span>       *En este ítem encontrarás la información de contacto de los comités de cupos del departamento de Antioquia.</span>
                          <div class="box-body"  >
                            <div id"tabla-responsive" class="table-responsive" >

                              <a href="listado_documentos" style="float: right; margin-bottom:10px;"  class="btn btn-primary">Ver documentos</a>

                              <table id="tablacupos"  class="mdl-data-table" style="width:100%">
                                <thead>
                                  <tr>
                                    <Th>Subregión</Th>
                                    <Th>Municipio</Th>
                                    <Th>Nombre</Th>
                                    <Th>Apellido</Th>
                                    <Th>Celular</Th>
                                    <Th>Teléfono</Th>
                                    <Th>Correo personal</Th>
                                    <Th>Sector</Th>
                                    <Th>Cargo</Th>
                                    <Th>Dirección</Th>
                                    <th>Comité</th>
                                  </tr>
                                </thead>
                              </table>

                            </div>
                          </div>
                        </div>
                        <script>

                        $(document).ready(function() {
                          $('#tablacupos').DataTable({
                            processing: true,
                            serverSide: true,
                            pageLength: 10,
                            dom: 'Bfrtip',
                            buttons: [
                              {
                                extend: 'excel',
                                text: 'Exportar a excel',
                                title: 'Comités de cupos',
                                filename: 'Comités de cupos',
                              }, {
                                extend: 'pdf',
                                text: 'Exportar a PDF',
                                orientation: 'landscape',
                                title: 'Comités de cupos',
                                filename: 'Comités de cupos',
                              },
                            ],
                            language: {
                              "url": '{!! asset('/plugins/datatables/latino.json') !!}'
                            } ,
                            ajax: '{{ route('datatable.cupos') }}',
                            columns: [
                              { data: 'NombreSubregion', name: 'NombreSubregion' },
                              { data: 'NombreMunicipio', name: 'NombreMunicipio' },
                              { data: 'nombre', name: 'nombre' },
                              { data: 'apellido', name: 'apellido' },
                              { data: 'celular', name: 'celular' },
                              { data: 'telefono', name: 'telefono' },
                              { data: 'correo', name: 'correo' },
                              { data: 'sector', name: 'sector' },
                              { data: 'cargo', name: 'cargo' },
                              { data: 'direccion', name: 'direccion' },
                              { data: 'comite', name: 'comite' },
                            ]
                          });

                        });

                        </script>

                      </section>

                      @extends('layouts.partials.directoriomunicipal')
                    @endsection
                    @section('scripts')
                    @stop
