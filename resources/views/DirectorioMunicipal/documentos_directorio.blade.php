@extends('layouts.app')
@section('htmlheader_title')
  Documentos directorio municipal
@endsection

@section('main-content')

  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

  <section  id="contenido_principal">
    <div class="box box-primary">
      <div class="box-header">
        <div class="box-header with-border" >
          <center>
            <h3 class="box-title" style=" " ><strong>Documentación de los municipios de Antioquia</strong></h3>
          </center>
        </div>
        <span class="counter pull-right"></span>
        <table id="dtdocumentacion" class="table table-striped table-bordered table-sm" cellspacing="0" width="100%">
          <thead>
            <tr>
              <th class="col-md-5 col-xs-5">Municipio</th>
              <th class="col-md-3 col-xs-3">Documento</th>
              <th class="col-md-3 col-xs-3">Número de documento</th>
              <th class="col-md-3 col-xs-3">Fecha documento</th>
              <th class="col-md-4 col-xs-4">Ver documento</th>
            </tr>
          </thead>
          <tbody>
            @foreach ($datos as $dato)
              <tr>
                <td>{{$dato->nombre_municipio}}</td>
                <td>{{$dato->comite_junta}}</td>
                <td>{{$dato->numero_documento}}</td>
                <td>{{$dato->fecha_documento}}</td>
                @if ($dato->documento != "")
                  <td style="text-align: center;"><a href="{{url("/documento/$dato->documento")}}" title="Ver documento"  target='_blank'><i style="font-size:25px; color:red;" class="fa fa-file-pdf-o"></i></a></td>
                @else
                  <td>Sin Anexo</td>
                @endif
              </tr>
            @endforeach
          </tbody>
        </table>

        <script type="text/javascript">

      $(document).ready(function () {
        $('#dtdocumentacion').DataTable({
          language: {
            "url": '{!! asset('/plugins/datatables/latino.json') !!}'
          } ,
        });

        $('.dataTables_length').addClass('bs-select');
      });

      </script>

      <center>
        <a href="ver_directorio" style="margin-bottom:10px;"  class="btn btn-primary">Regresar</a>
      </center>
    </div>
  </div>
@endsection
