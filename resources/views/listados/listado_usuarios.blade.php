@if(Auth::user()->isRole('administrador_sistema'))
@extends('layouts.app')

@section('htmlheader_title')
  Administración de Usuarios
@endsection


@section('main-content')


  <section  id="contenido_principal">

    <div class="box box-primary box-gris">

      <div class="box-header">

        <div class="margin" id="botones_control">
        <center style="margin-top:5px">

          <h4 class="box-title"><strong>Usuarios</strong></h4>

        </center>
          <form   action="{{ url('buscar_usuario') }}"  method="post"  >
            <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
            <div style="float: right;" class="input-group input-group-sm">
              <input style="width:250px;height:34px" type="text" class="form-control" id="dato_buscado" name="dato_buscado" required>
              <input type="submit" class="btn btn-primary" value="Buscar" >
            </div>

          </form>

          <a href="{{ url("/listado_usuarios") }}"  class="btn btn-primary" >Listado usuarios</a>
          <a href="javascript:void(0);" class="btn btn-primary" onclick="cargar_formulario(1);">Agregar usuario</a>
          <a href="javascript:void(0);" class="btn btn-primary" onclick="cargar_formulario(2);">Roles</a>
          <a href="javascript:void(0);" class="btn btn-primary" onclick="cargar_formulario(3);" >Permisos</a>
        </div>

      </div>

      <div class="box-body box-white">

        <div class="table-responsive" >

          <table  class="table table-hover table-striped" cellspacing="0" width="100%">
            <thead>
              <tr> <th>Código</th>
                <th>Rol</th>
                <th>Nombres</th>
                <th>Usuario</th>
                <th>Teléfono</th>
                <th>Acción</th>
              </tr>
            </thead>
            <tbody>

              @foreach($usuarios as $usuario)
                <tr role="row" class="odd">
                  <td>{{ $usuario->id }}</td>
                  <td>

                    @foreach($usuario->getRoles() as $roles)
                      {{  ucwords(strtolower($roles)).","  }}
                    @endforeach


                  </td>
                  <td class="mailbox-messages mailbox-name">&nbsp;{{ $usuario->name  }} {{ $usuario->lastname  }}</td>

                  <td>{{ $usuario->email }}</td>
                  <td>{{ $usuario->phone }}</td>
                  <td>

                    <button type="button" class="btn  btn-warning btn-xs" data-placement="left" title="Editar"  onclick="verinfo_usuario({{  $usuario->id }})" ><i class="fa fa-fw fa-edit"></i></button>
                    <button type="button"  class="btn  btn-danger btn-xs"  data-placement="left"  title="Eliminar" onclick="borrado_usuario({{  $usuario->id }});"  ><i class="fa fa-fw fa fa-trash-o"></i></button>
                  </td>
                </tr>
              @endforeach

            </tbody>
          </table>

        </div>
      </div>




      {{ $usuarios->links() }}

      @if(count($usuarios)==0)


        <div class="box box-primary col-xs-12">

          <div class='aprobado' style="margin-top:70px; text-align: center">

        <div class="box box-primary col-xs-12">

          <div class='aprobado' style="margin-top:70px; text-align: center">

            <label style='color:#177F6B'>
              No se encontraron resultados para su busqueda.
            </label>

          </div>

        </div>


      @endif

    </div>




  </section>
@endsection
@else
  @extends('errors.acceso')
@endif
