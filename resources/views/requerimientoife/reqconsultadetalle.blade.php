@extends('layouts.app')
@section('htmlheader_title')
  Requerimientos Infraestructura Educativa
@endsection

@section('main-content')
<style type="text/css">
  #tablegeneral th{
    vertical-align: middle;
    border-bottom: none;
    text-align: right;
  }
  #tablegeneral td{
    text-align: left;
    vertical-align: middle;
  }

  #tablegeneral tr{
    border-bottom: 1px solid #ddd;
  }
</style>
  <section  id="contenido_principal">
    <div class="box box-primary">
      <div align="center" class="box-header">
        <h3 class="box-title"><strong>Diagnóstico de necesidades y requerimientos infraestructura educativa</strong></h3>

        @if(Auth::user()->isRole('rectores'))
          <a href='{{url("/requerimientosIFisica/".Auth::user()->name."/establecimiento")}}'>
            <button type="button" class="btn btn-primary pull-right" style="margin-right: 8px;" name="button">Regresar</button>
          </a>
        @elseif (Auth::user()->isRole('secretarios'))
          <a href='{{url("/requerimientosIFisica/".Auth::user()->name."/municipio")}}'>
            <button type="button" class="btn btn-primary pull-right" style="margin-right: 8px;" name="button">Regresar</button>
          </a>
        @else
          <a href="{{url('/requerimientosIFisica/consult')}}">
            <button type="button" class="btn btn-primary pull-right" style="margin-right: 8px;" name="button">Regresar</button>
          </a>
        @endif

      </div>
          <input type="hidden" value="{{ Auth::user()->name }}" name="userSolicitud">
          <div class="box-body ">
            @foreach($SolicitudIF as $solicitud)
            <?php $sed = $solicitud->Sede ?>
            <div class="panel panel-primary ">
              <div class="panel-heading" style="font-size: 16px;">Información general de la sede</div>
              <div class="panel-body">
                <table id="tablegeneral" class="table table-hover table-striped" cellspacing="0" width="100%">
                  <thead>
                    <tr>
                      <th>Departamento:</th>
                      <td>{{$sed->nombre_departamento}}</td>
                      <th>Código municipio:</th>
                      <td>{{$sed->codigo_dane_municipio}}</td>
                      <th>Municipio:</th>
                      <td>{{$sed->nombre_municipio}}</td>
                    </tr>
                    <tr>
                      <th>Código establecimiento:</th>
                      <td>{{$sed->codigo_establecimiento}}</td>
                      <th>Nombre establecimiento:</th>
                      <td>{{$sed->nombre_establecimiento}}</td>
                      <th>Código sede:</th>
                      <td>{{$sed->codigo_sede}}</td>
                    </tr>
                    <tr>
                      <th>Nombre sede:</th>
                      <td>{{$sed->nombre_sede}}</td>
                      <th>Zona:</th>
                      <td>{{$sed->zona}}</td>
                      <th>Dirección:</th>
                      <td>{{$sed->direccion}}</td>
                    </tr>
                    <tr>
                      <th>Teléfono:</th>
                      <td>{{$sed->telefono}}</td>
                      <th>Estado sede:</th>
                      <td>{{$sed->estado_sede}}</td>
                      <th>Niveles:</th>
                      <td>{{$sed->niveles}}</td>
                    </tr>
                    <tr>
                      <th>Modelos:</th>
                      <td>{{$sed->modelos}}</td>
                      <th>Grados:</th>
                      <td>{{$sed->grados}}</td>
                      <th></th>
                      <td></td>
                    </tr>
                  </thead>
                </table>
              </div>
            </div>
          </div>
          <div class="box-body">
            <div class="panel panel-primary ">
              <div class="panel-heading" style="font-size: 16px;">Descripción de diagnóstico de necesidades</div>
              <input type="hidden" value="0" name="nrows">
              <div class="table-responsive" style="padding:2em;">
                <?php $repEspacios = $solicitud->Espacio ?>
                @if(count($repEspacios) >0)
                <table class="table table-hover table-striped" cellspacing="0" width="100%">
                  <thead>
                    <tr>
                      <th>Espacio físico</th>
                      <th>Cantidad</th>
                      <th>Estado</th>
                      <th>Tipo de solicitud</th>
                      <th>Requerimientos</th>
                    </tr>
                  </thead>
                  <tbody  id="tdReqEspacios">
                  @foreach($repEspacios as $espacio)
                  <tr>
                  <td>{{$espacio->TipoEspacios->NombreEspacio}}</td>
                  <td>{{$espacio->Cantidad}}</td>
                  <td>{{$espacio->Estado->Nombre}}</td>
                  <td>{{$espacio->TipoS->Nombre}}</td>
                  <?php
                  $requerimientos="";
                  if($espacio->RedElectrica){$requerimientos =$requerimientos."1";}
                  if($espacio->Acueducto){$requerimientos =$requerimientos."-2";}
                  if($espacio->Alcantarillado){$requerimientos =$requerimientos."-3";}
                  if($espacio->Piso){$requerimientos =$requerimientos."-4";}
                  if($espacio->Pared){$requerimientos =$requerimientos."-5";}
                  if($espacio->Puerta){$requerimientos =$requerimientos."-6";}
                  if($espacio->Cerramiento){$requerimientos =$requerimientos."-7";}
                  if($espacio->Cubierta){$requerimientos =$requerimientos."-8";}
                  ?>
                  <td>{{$requerimientos}}</td>
                  </tr>
                  @endforeach
                </tbody>
                </table>
              </div>
              <div class="panel-body">
                  <div class="form-group col-xs-12" style="border:1px solid #DDD;">
                    <h5><strong>Descripción de requerimientos:</strong></h5>
                    <div class="col-xs-3">
                      <h5>1) Red eléctrica</h5>
                    </div><div class="col-xs-3">
                      <h5>2) Acueducto</h5>
                    </div><div class="col-xs-3">
                      <h5>3) Alcantarillado</h5>
                    </div><div class="col-xs-3">
                      <h5>4) Mejoramiento de pisos</h5>
                    </div><div class="col-xs-3">
                      <h5>5) Mejoramiento en paredes</h5>
                    </div><div class="col-xs-3">
                      <h5>6) Mejoramiento en puertas</h5>
                    </div><div class="col-xs-3">
                      <h5>7) Mejoramiento en cerramiento</h5>
                    </div><div class="col-xs-3">
                      <h5>8) Mejoramiento de cubierta</h5>
                    </div>
                  </div>
                </div>
                @endif

                <div class="panel-body">
                  <br>
                  <div class="col-xs-12" style="border-bottom:1px solid #337ab7; border-top:1px solid #337ab7; padding:0.8em; margin-top:0.5em; margin-bottom:0.5em; text-align:center;"><strong>Observaciones generales de la sede</strong></div>
                    <div class="form-group col-xs-6">
                      <label>Anexo en <span class="label label-primary">PDF</span> / <span class="label label-primary">Imagen</span></label>
                      @if ($solicitud->Anexo != "")
                        <a href="{{url("/down/$solicitud->Anexo")}}"  target='_blank'>Descargar</a>
                      @else
                        Sin Anexo
                      @endif
                      <br>
                      <label for="observacion">Observación:</label>
                      <textarea readonly="true" class="form-control" style="resize:none" rows="5" id="observacion" name="observacion">{{$solicitud->Observacion}}</textarea>
                      <br>
                    </div>
                    <div class="form-group col-xs-6">
                      <h5><strong>Otros requerimientos generales:</strong></h5>
                      @if ($solicitud->Lote)
                        <div class="form-check col-xs-12">
                          <label class="checkbox-formulario">
                            <span class="label-text">Requiere lote</span>
                          </label>
                        </div>
                      @endif
                      @if ($solicitud->DisenioTecnico)
                        <div class="form-check col-xs-12">
                          <label class="checkbox-formulario">
                            <span class="label-text">Requiere estudios y diseños técnicos</span>
                          </label>
                        </div>
                      @endif
                      @if ($solicitud->ReposicionTotal)
                        <div class="form-check col-xs-12">
                          <label class="checkbox-formulario">
                            <span class="label-text">Requiere reposición total</span>
                          </label>
                        </div>
                      @endif
                      @if ($solicitud->TerminacionInconclusa)
                        <div class="form-check col-xs-12">
                          <label class="checkbox-formulario">
                            <span class="label-text">Requiere terminación de obra inconclusa </span>
                          </label>
                        </div>
                      @endif
                    </div>
                    @if (!$solicitud->Atendida && (Auth::user()->isRole('administrador_sistema')  || Auth::user()->isRole('administrador_infraestructura_fisica')))
                    <form method="post" action="{{url('/update/solicitud')}}">
                      {{ csrf_field() }}
                      <input type="hidden" value="{{$solicitud->id}}" name="valueupdate"/>
                      <button type="submit" id="btnsubmit"  class="btn btn-primary pull-right"> Caso evidenciado</button>
                    </form>
                    @if($solicitud->Respuesta == null)

                    <button type="button" id="btnRegRespSolicitud" style="margin-right:0.5em;" class="btn btn-primary pull-right" > Responder</button>

                    @else
                      <button type="button" id="btnEdtRespSolicitud" style="margin-right:0.5em;" class="btn btn-primary pull-right" > Editar Respuesta</button>
                      @endif
                    @endif
                  </div>
              </div>
            </div>
          <div class="box-footer">
            <div class="panel panel-primary" id="Respuesta">
              <input type="hidden" value="{{$solicitud->Respuesta}}" id="exisresponse">
              <div class="panel-heading">Respuesta - Secretaría de Educación, Dirección de Infraestructura Educativa.</div>
              <div class="panel-body">
                <form method="post" action="{{url('requerimientosIFisica/Respuesta')}}">
                  <input type="hidden" value="" name="urlActual" id="urlActual">
                  {{ csrf_field() }}
                  @if($solicitud->Respuesta != null)
                    <label for="observacion">Fecha de respuesta: {{$solicitud->Respuesta->Fecha}}</label>
                    <input type="hidden" name="RespuestaId" id="RespuestaId" value="{{$solicitud->Respuesta->id}}">
                    <textarea readonly="true" class="form-control" style="resize:none" rows="5" id="RespuestaSolicitudIF" placeholder="Escriba aquí su respuesta"  name="RespuestaSolicitudIF">{{$solicitud->Respuesta->Mensaje}}</textarea>
                  @else
                    <textarea readonly="true" class="form-control" style="resize:none" rows="5" id="RespuestaSolicitudIF" placeholder="Escriba aquí su respuesta" name="RespuestaSolicitudIF" required></textarea>
                  @endif
                  <input type="hidden" name="idSolicitud" value="{{$solicitud->id}}">
                  <input type="hidden" name="tipoForm" id="tipoForm" value="">
                  <button type="button" id="btnCancelaRespuestSIF" style="margin-right:0.5em; display:none;" class="btn btn-primary pull-right" > Cancelar</button>
                  <button type="submit" id="btnConfirmEditRespuestaSolicitud" style="margin-right:0.5em; display:none;" class="btn btn-primary pull-right" > Guardar</button>
                </form>
              </div>
            </div>
          </div>
            @endforeach
        </div>

  @endsection
