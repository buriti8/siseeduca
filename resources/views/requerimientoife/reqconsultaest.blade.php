@extends('layouts.app')
@section('htmlheader_title')
Requerimientos Infraestructura Educativa
@endsection

@section('main-content')
<style type="text/css">
#tablegeneral th{
  vertical-align: middle;
  border-bottom: none;
  text-align: right;
}
#tablegeneral td{
  text-align: left;
  vertical-align: middle;
}

#tablegeneral tr{
  border-bottom: 1px solid #ddd;
}
</style>
<section  id="contenido_principal">
  <div  class="box box-primary">
    <div align="center" class="box-header">
      <h3 class="box-title"><strong>Diagnóstico de necesidades por establecimientos</strong></h3>
      @if(Auth::user()->isRole('rectores'))
        <a href='{{url("/requerimientosIFisica/".Auth::user()->name."/establecimiento")}}'>
          <button type="button" class="btn btn-primary pull-right" style="margin-right: 8px;" name="button">Regresar</button>
        </a>
      @elseif (Auth::user()->isRole('secretarios'))
        <a href='{{url("/requerimientosIFisica/".Auth::user()->name."/municipio")}}'>
          <button type="button" class="btn btn-primary pull-right" style="margin-right: 8px;" name="button">Regresar</button>
        </a>
      @else
        <a href="{{url('/requerimientosIFisica/consult')}}">
          <button type="button" class="btn btn-primary pull-right" style="margin-right: 8px;" name="button">Regresar</button>
        </a>
      @endif
    </div>
    <div class="box-body"  >
      <div class="panel panel-primary ">
        <div class="panel-heading" style="font-size: 16px;">Información general del establecimiento</div>
        <div class="table-responsive" >
          @foreach($establecimiento as $est)
          <table id="tablegeneral" class="table table-hover table-striped" cellspacing="0" width="100%">
              <thead>
                <tr>
                  <th>Nombre:</th>
                  <td>{{$est->nombre_establecimiento}}</td>
                  <th>Código DANE:</th>
                  <td>{{$est->codigo_establecimiento}}</td>
                  <th>Dirección:</th>
                  <td>{{$est->direccion}}</td>
                </tr>
                <tr>
                  <th>Teléfono:</th>
                  <td>{{$est->telefono}}</td>
                  <th>Departamento:</th>
                  <td>{{$est->departamento}}</td>
                  <th>Municipio:</th>
                  <td>{{$est->municipio}}</td>
                </tr>
                <tr>
                  <th>Estado:</th>
                  <td>{{$est->estado}}</td>
                  <th>Calendario:</th>
                  <td>{{$est->calendario}}</td>
                  <th>Sector:</th>
                  <td>{{$est->sector}}</td>
                </tr>
                <tr>
                  <th>Zona EE:</th>
                  <td>{{$est->zona}}</td>
                  <th>Rector:</th>
                  <td>{{$est->nombre_rector}}</td>
                  <th>Género</th>
                  <td>{{$est->genero}}</td>
                </tr>
                <tr>
                  <th>Tipo Establecimiento:</th>
                  <td>{{$est->tipo_establecimiento}}</td>
                  <th>Especialidad:</th>
                  <td>{{$est->especialidad}}</td>
                  <th>Licencia:</th>
                  <td>{{$est->licencia}}</td>
                </tr>
                <tr>
                  <th>Prestador de servicio:</th>
                  <td>{{$est->prestador_de_servicio}}</td>
                  <th>Número de sedes:</th>
                  <td>{{$est->numero_de_sedes}}</td>
                  <th>Modelo educativo:</th>
                  <td>{{$est->modelos_educativos}}</td>
                </tr>
              </thead>
            </table>
            @endforeach
        </div>
      </div>

      <div class="panel panel-primary ">
        <div class="panel-heading"style="font-size: 16px;">Dianósticos de necesidades registrados - No evidenciados</div>
        <div class="table-responsive" >
          @if (count($SolicitudIF) >= 1)
          <table  class="table table-hover table-striped" style="text-align:center" cellspacing="0" width="100%">
              <thead>
                <tr>
                  <th>Municipio</th>
                  <th>Establecimiento</th>
                  <th>Código establecimiento</th>
                  <th>Sede</th>
                  <th>Código sede</th>
                  <th>Zona</th>
                  <th>Dirección</th>
                  <th>Teléfono</th>
                  <th>Estado sede</th>
                  <th>Respuesta</th>
                  <th>Anexo</th>
                  <th data-type="date">Fecha solicitud</th>
                  <th>Acción</th>
                </tr>
              </thead>
              <tbody>
                  @foreach($SolicitudIF as $solicitud)
                    <tr role="row" class="odd">
                      <td>{{$solicitud->Sede->nombre_municipio}}</td>
                      <td>{{$solicitud->Establecimiento->nombre_establecimiento}}</td>
                      <td>{{$solicitud->Establecimiento->codigo_establecimiento}}</td>
                      <td><a href='{{url("/requerimientosIFisica/".$solicitud->Sede->codigo_sede."/sede")}}'>{{$solicitud->Sede->nombre_sede}}</a></td>
                      <td>{{$solicitud->Sede->codigo_sede}}</td>
                      <td>{{$solicitud->Sede->zona}}</td>
                      <td>{{$solicitud->Sede->direccion}}</td>
                      <td>{{$solicitud->Sede->telefono}}</td>
                      <td>{{$solicitud->Sede->estado_sede}}</td>
                      <td>
                        <?php
                          if($solicitud->Respuesta != null){
                        ?>
                          @if(Auth::user()->isRole('administrador_sistema') || Auth::user()->isRole('administrador_infraestructura_fisica'))
                            <div  style='background:#B5FEC1; color:#3C763D'>1</div>
                          @elseif(Auth::user()->isRole('secretarios'))
                            @if($solicitud->Respuesta->VistoSecretario)
                              <div  style='color:#3C763D'>1</div>
                            @else
                              <div  style='background:#B5FEC1; color:#3C763D'>1</div>
                            @endif
                          @elseif(Auth::user()->isRole('rectores'))
                            @if($solicitud->Respuesta->VistoRector)
                              <div  style='color:#3C763D'>1</div>
                            @else
                              <div  style='background:#B5FEC1; color:#3C763D'>1</div>
                            @endif
                          @endif
                        <?php

                          }else{
                            echo 0;
                          }
                        ?>
                      </td>
                      @if ($solicitud->Anexo != "")
                        <td><a href="{{url("/down/$solicitud->Anexo")}}"  target='_blank'>Descargar</a></td>
                      @else
                        <td>Sin Anexo</td>
                      @endif
                      <td>{{$solicitud->Fecha}}</td>
                      <td>
                    <div class="row">
                      <a href="{{url('/requerimientosIFisica/'.$solicitud->id.'/view')}}">
                        <button type="button" class="btn btn-info btn-sm" style="margin-left: 5px;" title="Información"
                        ><i class="fa fa fa-info"></i></button>
                      </a>
                        <button type="button" style="margin-left: 5px;"  onclick="elimnaSolicitudIF({{$solicitud->id}})"class="btn  btn-danger btn-xs" title="Eliminar"><i class="fa fa-fw fa fa-trash-o"></i></button>
                    </div>
                    </td>
                    </tr>
                  @endforeach
                </tbody>
              </table>
              {{ $SolicitudIF->links() }}
          @else
          <br>
          <center>
            <h4 style="margin-left:10px;">No hay diagnóstico de necesidades no evidenciados registrados para este establecimiento</h4>
          </center>
          <br>
          @endif
            </div>
        </div>


      <div class="panel panel-primary ">
        <div class="panel-heading" style="font-size: 16px;">Dianósticos de necesidades registrados - Evidenciados</div>
        <div class="table-responsive" >
          @if (count($SolicitudIFAT) >= 1)
          <table  class="table table-hover table-striped" style="text-align:center" cellspacing="0" width="100%">
              <thead>
                <tr>
                  <th>Municipio</th>
                  <th>Establecimiento</th>
                  <th>Código establecimiento</th>
                  <th>Sede</th>
                  <th>Código sede</th>
                  <th>Zona</th>
                  <th>Dirección</th>
                  <th>Teléfono</th>
                  <th>Estado sede</th>
                  <th>Respuesta</th>
                  <th>Anexo</th>
                  <th data-type="date">Fecha solicitud</th>
                  <th>Acción</th>
                </tr>
              </thead>
              <tbody>
                  @foreach($SolicitudIFAT as $solicitud)
                    <tr role="row" class="odd">
                      <td>{{$solicitud->Sede->nombre_municipio}}</td>
                      <td>{{$solicitud->Establecimiento->nombre_establecimiento}}</td>
                      <td>{{$solicitud->Establecimiento->codigo_establecimiento}}</td>
                      <td><a href='{{url("/requerimientosIFisica/".$solicitud->Sede->codigo_sede."/sede")}}'>{{$solicitud->Sede->nombre_sede}}</a></td>
                      <td>{{$solicitud->Sede->codigo_sede}}</td>
                      <td>{{$solicitud->Sede->zona}}</td>
                      <td>{{$solicitud->Sede->direccion}}</td>
                      <td>{{$solicitud->Sede->telefono}}</td>
                      <td>{{$solicitud->Sede->estado_sede}}</td>
                      <td>
                        <?php
                          if($solicitud->Respuesta != null){
                        ?>
                          @if(Auth::user()->isRole('administrador_sistema') || Auth::user()->isRole('administrador_infraestructura_fisica'))
                            <div  style='background:#B5FEC1; color:#3C763D'>1</div>
                          @elseif(Auth::user()->isRole('secretarios'))
                            @if($solicitud->Respuesta->VistoSecretario)
                              <div  style='color:#3C763D'>1</div>
                            @else
                              <div  style='background:#B5FEC1; color:#3C763D'>1</div>
                            @endif
                          @elseif(Auth::user()->isRole('rectores'))
                            @if($solicitud->Respuesta->VistoRector)
                              <div  style='color:#3C763D'>1</div>
                            @else
                              <div  style='background:#B5FEC1; color:#3C763D'>1</div>
                            @endif
                          @endif
                        <?php

                          }else{
                            echo 0;
                          }
                        ?>
                      </td>
                      @if ($solicitud->Anexo != "")
                        <td><a href="{{url("/down/$solicitud->Anexo")}}"  target='_blank'>Descargar</a></td>
                      @else
                        <td>Sin Anexo</td>
                      @endif
                      <td>{{$solicitud->Fecha}}</td>
                      <td>
                        <a href="{{url('/requerimientosIFisica/'.$solicitud->id.'/view')}}">
                          <button type="button" class="btn btn-info" style="margin-left: 5px;" data-placement="left" title="Información"
                          ><i class="fa fa fa-info"></i></button>
                      </a>
                      </td>
                    </tr>
                  @endforeach
                </tbody>
              </table>
              {{ $SolicitudIFAT->links() }}
          @else
            <br>
            <center>
              <h4 style="margin-left:10px;">No hay diagnóstico de necesidades evidenciados registrados para este establecimiento</h4>
            </center>
            <br>
          @endif
            </div>
        </div>
      </div>
    </div>


@endsection
