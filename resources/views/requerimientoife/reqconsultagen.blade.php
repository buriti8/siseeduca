@if(Auth::user()->isRole('administrador_sistema') || Auth::user()->isRole('Administrador_infraestructura_educativa') || Auth::user()->isRole('Rectores') || Auth::user()->isRole('Secretarios'))
@extends('layouts.app')
@section('htmlheader_title')
  Requerimientos Infraestructura Educativa
@endsection

@section('main-content')
  <style type="text/css">
  #tablegeneral th{
    vertical-align: middle;
    border-bottom: none;
    text-align: right;
  }
  #tablegeneral td{
    text-align: left;
    vertical-align: middle;
  }

  #tablegeneral tr{
    border-bottom: 1px solid #ddd;
  }


</style>
<section  id="contenido_principal">
  <div class="box box-primary">
    <div align="center" class="box-header">
      <h3 class="box-title"><strong>Diagnóstico de necesidades generales en Antioquia</strong></h3>
    </div>
    <div class="box-body"  >
      <div class="panel panel-primary ">
        <div style="font-size: 16px;" class="panel-heading">Diagnóstico de necesidades registrados  - No evidenciados</div>
        <div id="notificacion_resul_fci">
          <div class="modal" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <h5 class="modal-title">Modal title</h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="modal-body">
                  <p>Modal body text goes here.</p>
                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-primary">Save changes</button>
                  <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="table-responsive" >
          @if (count($SolicitudIF) >= 1)
            <table  class="table table-hover table-striped" style="text-align:center" cellspacing="0" width="100%">
              <thead>
                <tr>
                  <th>Municipio</th>
                  <th>Establecimiento</th>
                  <th>Código establecimiento</th>
                  <th>Sede</th>
                  <th>Código sede</th>
                  <th>Zona</th>
                  <th>Dirección</th>
                  <th>Teléfono</th>
                  <th>Estado sede</th>
                  <th>Respuesta</th>
                  <th>Anexo</th>
                  <th data-type="date">Fecha solicitud</th>
                  <th>Acción</th>
                </tr>
              </thead>
              <tbody>
                @foreach($SolicitudIF as $solicitud)
                  <tr role="row" class="odd">
                    <td>{{$solicitud->Sede->nombre_municipio}}</td>
                    <td><a href='{{url("/requerimientosIFisica/".$solicitud->Establecimiento->codigo_establecimiento."/establecimiento")}}'>{{ $solicitud->Establecimiento->nombre_establecimiento }}</a></td>
                    <td>{{$solicitud->Establecimiento->codigo_establecimiento}}</td>
                    <td><a href='{{url("/requerimientosIFisica/".$solicitud->Sede->codigo_sede."/sede")}}'>{{ $solicitud->Sede->nombre_sede }}</a></td>
                    <td>{{$solicitud->Sede->codigo_sede}}</td>
                    <td>{{$solicitud->Sede->zona}}</td>
                    <td>{{$solicitud->Sede->direccion}}</td>
                    <td>{{$solicitud->Sede->telefono}}</td>
                    <td>{{$solicitud->Sede->estado_sede}}</td>
                    <td><center>
                      <?php
                      if($solicitud->Respuesta != null){
                        ?>
                        @if(Auth::user()->isRole('administrador_sistema') || Auth::user()->isRole('administrador_infraestructura_fisica'))
                          <div  style='background:#B5FEC1; color:#3C763D'>1</div>
                        @elseif(Auth::user()->isRole('secretarios'))
                          @if($solicitud->Respuesta->VistoSecretario)
                            <div  style='color:#3C763D'>1</div>
                          @else
                            <div  style='background:#B5FEC1; color:#3C763D'>1</div>
                          @endif
                        @elseif(Auth::user()->isRole('rectores'))
                          @if($solicitud->Respuesta->VistoRector)
                            <div  style='color:#3C763D'>1</div>
                          @else
                            <div  style='background:#B5FEC1; color:#3C763D'>1</div>
                          @endif
                        @endif
                        <?php

                      }else{
                        echo 0;
                      }
                      ?>
                      <center>
                    </td>
                    @if ($solicitud->Anexo != "")
                      <td><a href="{{url("/down/$solicitud->Anexo")}}"  target='_blank'>Descargar</a></td>
                    @else
                      <td>Sin Anexo</td>
                    @endif
                    <td>{{$solicitud->Fecha}}</td>
                    <td>
                      <div class="row" style="margin-left:3px;">

                        <a href="{{url('/requerimientosIFisica/'.$solicitud->id.'/view')}}">
                          <button type="button" class="btn btn-warning btn-xs" data-placement="left" title="Editar"
                          ><i class="fa fa-edit"></i></button>
                        </a>
                        <button type="button"  data-placement="left" title="Borrar"
                        class="btn  btn-danger btn-xs" onclick="elimnaSolicitudIF({{$solicitud->id}})"><i class="fa fa fa-trash-o"></i></button>
                      </div>
                    </td>
                  </tr>
                @endforeach
              </tbody>
            </table>
            {{ $SolicitudIF->links() }}
          @else
            <br>
            <center>
            <h4 style="margin-left:10px">No hay diagnóstico de necesidades no evidenciados registrados</h4>
          </center>
            <br>
          @endif
        </div>
      </div>


      <div class="panel panel-primary ">
        <div style="font-size: 16px;" class="panel-heading">Diagnóstico de necesidades registrados - Evidenciados</div>
        <div class="table-responsive" >
          @if (count($SolicitudIFAT) >= 1)
            <table  class="table table-hover table-striped" style="text-align:center" cellspacing="0" width="100%">
              <thead>
                <tr>
                  <th>Municipio</th>
                  <th>Establecimiento</th>
                  <th>Código establecimiento</th>
                  <th>Sede</th>
                  <th>Código sede</th>
                  <th>Zona</th>
                  <th>Dirección</th>
                  <th>Teléfono</th>
                  <th>Estado sede</th>
                  <th>Respuesta</th>
                  <th>Anexo</th>
                  <th data-type="date">Fecha solicitud</th>
                  <th>Acción</th>
                </tr>
              </thead>
              <tbody>
                @foreach($SolicitudIFAT as $solNoatend)
                  <tr role="row" class="odd">
                    <td>{{$solNoatend->Sede->nombre_municipio}}</td>
                    <td><a href='{{url("/requerimientosIFisica/".$solNoatend->Establecimiento->codigo_establecimiento."/establecimiento")}}'>{{$solNoatend->Establecimiento->nombre_establecimiento}}</a></td>
                    <td>{{$solNoatend->Establecimiento->codigo_establecimiento}}</td>
                    <td><a href='{{url("/requerimientosIFisica/".$solNoatend->Sede->codigo_sede."/sede")}}'>{{$solNoatend->Sede->nombre_sede}}</a></td>
                    <td>{{$solNoatend->Sede->codigo_sede}}</td>
                    <td>{{$solNoatend->Sede->zona}}</td>
                    <td>{{$solNoatend->Sede->direccion}}</td>
                    <td>{{$solNoatend->Sede->telefono}}</td>
                    <td>{{$solNoatend->Sede->estado_sede}}</td>
                    <td> <center>
                      <?php
                      if($solNoatend->Respuesta != null){
                        ?>
                        @if(Auth::user()->isRole('administrador_sistema') || Auth::user()->isRole('administrador_infraestructura_fisica'))
                          <div  style='background:#B5FEC1; color:#3C763D'>1</div>
                        @elseif(Auth::user()->isRole('secretarios'))
                          @if($solNoatend->Respuesta->VistoSecretario)
                            <div  style='color:#3C763D'>1</div>
                          @else
                            <div  style='background:#B5FEC1; color:#3C763D'>1</div>
                          @endif
                        @elseif(Auth::user()->isRole('rectores'))
                          @if($solNoatend->Respuesta->VistoRector)
                            <div  style='color:#3C763D'>1</div>
                          @else
                            <div  style='background:#B5FEC1; color:#3C763D'>1</div>
                          @endif
                        @endif
                        <?php

                      }else{
                        echo 0;
                      }
                      ?>
                      <center>
                    </td>
                    @if ($solNoatend->Anexo != "")
                      <td><a href="{{url("/down/$solNoatend->Anexo")}}"  target='_blank'>Descargar</a></td>
                    @else
                      <td>Sin Anexo</td>
                    @endif
                    <td>{{$solNoatend->Fecha}}</td>
                    <td>
                      <a href="{{url('/requerimientosIFisica/'.$solNoatend->id.'/view')}}">
                        <button type="button" class="btn btn-info btn-sm" style="margin-left: 5px;" data-placement="left" title="Información"
                        ><i class="fa fa fa-info"></i></button>
                      </td>
                    </tr>
                  @endforeach
                </tbody>
              </table>
              {{ $SolicitudIFAT->links() }}
            @else
              <br>
              <center>
              <h4 style="margin-left:10px">No hay diagnóstico de necesidades evidenciados registrados</h4>
            </center>
              <br>
            @endif
          </div>
        </div>
      </div>
    </div>
  @endsection
@else
  @extends('errors.acceso')
@endif
