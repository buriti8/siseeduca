@if(Auth::user()->isRole('administrador_sistema') || Auth::user()->isRole('Administrador_infraestructura_educativa') || Auth::user()->isRole('Rectores') || Auth::user()->isRole('Secretarios'))
@extends('layouts.app')
@section('htmlheader_title')
  Requerimientos Infraestructura Educativa
@endsection

@section('main-content')

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

<script type="text/javascript">

//Función que realiza la validación de tamaño del archivo que se desea subir.
$(document).on('change','input[type="file"]',function(){
	// this.files[0].size recupera el tamaño del archivo
	// alert(this.files[0].size);

	var fileName = this.files[0].name;
	var fileSize = this.files[0].size;

	if(fileSize > 5000000){
		alert('El archivo no debe superar los 5 megabytes (MB).');
		this.value = '';
		this.files[0].name = '';
	}else{
		// recuperamos la extensión del archivo
		var ext = fileName.split('.').pop();

		// console.log(ext);
		switch (ext) {
			case 'jpg':
			case 'jpeg':
			case 'png':
			case 'pdf': break;
			default:
				alert('El archivo no tiene la extensión adecuada.');
				this.value = ''; // reset del valor
				this.files[0].name = '';
		}
	}
});


</script>


  <!-- Modal content-->
  <!-- modal encargado de mostrar cualquier tipo de información por parte de la gobernación de antioquia
	al usuario del sistema. el modal se activa cada vez que el usuario ingrese al submódulo diagnóstico de necesidades-->
	<div class="modal fade in" id='verInformacion'  role="dialog" style="display: block; padding-right: 17px;">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<h4 class="modal-title">
						<strong>Información</strong>
					</h4>
				</div>
				<div align="justify" class="modal-body">
			      Este módulo es para el diagnóstico de las necesidades y estado actual de la infraestructura  de la sede educativa.
            Recuerde  que se debe radicar el proyecto con las necesidades por el municipio en la Dirección de Infraestructura Educativa,
            de acuerdo al listado de requisitos de esta dependencia para evaluación técnica y de asignación de recursos.
				</div>
				<div class="modal-footer">
					<button onclick="ocultar_informacion()" class="btn btn-primary" data-dismiss="modal" type="button">Cerrar</button>
				</div>
			</div>
		</div>
	</div>


  <section  id="contenido_principal">
    <div class="box box-primary">
      <div class="box-header">
        <center>
        <h3 class="box-title"><strong>Diagnóstico de necesidades infraestructura educativa</strong></h3>
        <center>
      </div>
      @if ($regSolicitud == 2)
        <div id="notificacion_resul_fci" class="msjNotificacion"><br>
          <div class="rechazado">
            <label style="color:#FA206A">Existen errores</label>
          </div>
          <div class="alert alert-danger">
            <strong></strong> Hay algunos problemas con su entrada.<br><br>
            <ul>
              <li>El archivo seleccionado como anexo no corresponde a un formato válido (pdf o una imagen).</li>
            </ul>
          </div>
        </div>
      @endif

      <div id="ControlEspaciosCantidad">
        <input type="hidden" id="tableAulasPreescolar" value="0">
        <input type="hidden" id="tableAulasPrimaria" value="0">
        <input type="hidden" id="tableAulasSecundaria" value="0">
        <input type="hidden" id="tableAulasEspaciales" value="0">
        <input type="hidden" id="tableBiblioteca" value="0">
        <input type="hidden" id="tableAulasSistemas" value="0">
        <input type="hidden" id="tableAulasBilinguismo" value="0">
        <input type="hidden" id="tableLaboratorio" value="0">
        <input type="hidden" id="tableAulasTalleres" value="0">
        <input type="hidden" id="tableAulasMultiples" value="0">
        <input type="hidden" id="tableCocinas" value="0">
        <input type="hidden" id="tableComedores" value="0">
        <input type="hidden" id="tableSantiriosHombres" value="0">
        <input type="hidden" id="tableSanitariosMujeres" value="0">
        <input type="hidden" id="tableLavamanosHombres" value="0">
        <input type="hidden" id="tableLavamanosMujeres" value="0">
        <input type="hidden" id="tableBReducida" value="0">
        <input type="hidden" id="tableOrinales" value="0">
        <input type="hidden" id="tableVivienda" value="0">
        <input type="hidden" id="tableCanchas" value="0">
        <input type="hidden" id="tablePlacasMulti" value="0">
        <input type="hidden" id="tableJuegosInfantiles" value="0">
        <input type="hidden" id="tableCantidadRegistrada" value="0">
      </div>
      <form  id="formRegReqSolIF"  method="post"  files="true"  enctype="multipart/form-data" action="{{url('/requerimientosIFisica/Create')}}" >
        {{ csrf_field() }}
        <input type="hidden" value="{{ Auth::user()->name }}" name="userSolicitud">
        <div class="box-body ">

          <div class="panel panel-primary ">
            <div class="panel-heading" style="font-size: 16px;">Seleccione una sede educativa:</div>
            <div class="panel-body">

              @if(Auth::user()->isRole('administrador_sistema') || Auth::user()->isRole('administrador_infraestructura_fisica'))
                <div class="form-group col-xs-6"  >
                  <div id='selEstablecimientoContenedor'>
                    <label for="selEstablecimiento">Seleccionar una subregión</label>
                    <select id="selSubregion" name="selSubregion" class="form-control  col-xs-6" onchange="filtroSubregionMunicipios()">
                      <option value="">Elige una</option>
                      <option value="1">BAJO CAUCA</option>
                      <option value="2">MAGDALENA MEDIO</option>
                      <option value="3">NORDESTE</option>
                      <option value="4">NORTE</option>
                      <option value="5">OCCIDENTE</option>
                      <option value="6">ORIENTE</option>
                      <option value="7">SUROESTE</option>
                      <option value="8">URABÁ</option>
                      <option value="9">VALLE DE ABURRÁ</option>
                    </select>
                  </div>
                </div>
                <div class="form-group col-xs-6"  >
                  <div id='selEstablecimientoContenedor'>
                    <label for="selMunicipio">Seleccionar municipio</label>
                    <select  id="selMunicipio" name="selMunicipio" onchange="filtroMunicipioEstablecimiento()" class="form-control  col-xs-6">
                      <option selected="selected" value="">Elige uno</option>
                    </select>
                  </div>
                </div>
              @endif

              <div class="form-group col-xs-6"  >
                <div id='selEstablecimientoContenedor'>
                  <label for="selEstablecimiento">Seleccionar un establecimiento educativo</label>
                  <select  id="selEstablecimiento" name="selEstablecimiento" class="form-control  col-xs-6"  required>
                    <option selected="selected" value="">Elige uno</option>
                    @foreach ($temp_historico_establecimientos as $establecimiento)
                      <option value="{{$establecimiento->codigo_establecimiento}}">{{$establecimiento->nombre_establecimiento}}</option>
                    @endforeach
                  </select>
                </div>
              </div>
              <div class="form-group col-xs-6"  >
                <div id='selEstablecimientoContenedor'>
                  <label for="selSede">Seleccionar una sede educativa</label>
                  <select  id="selSede" name="selSede" class="form-control  col-xs-6" required>
                    <option value="">Elige una</option>
                  </select>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="box-body">
          <div class="panel panel-primary ">
            <div class="panel-heading" style="font-size: 16px;">Descripción de diagnóstico de necesidades</div>
            <input type="hidden" value="0" name="nrows">
            <h5 style="margin-top:10px; margin-left:30px;"><img src="https://img.icons8.com/wired/64/000000/note.png" style="height:35px;"> Los diagnóstico de necesidades se hacen por cada uno de los espacios físicos, añada los Diagnóstico de necesidadess utilizando el botón <b>"Añadir".</b></h5>
            <div class="table-responsive" style="padding:2em;">
              <table class="table table-hover table-striped" cellspacing="0" width="100%">
                <thead>
                  <tr>
                    <th>Espacio físico</th>
                    <th>Cantidad</th>
                    <th>Estado</th>
                    <th>Tipo de solicitud</th>
                    <th>Requerimientos</th>
                    <th>Acción</th>
                  </tr>
                </thead>
                <tbody  id="tdReqEspacios">
                </tbody>
              </table>
            </div>
            <div class="panel-body">
              <div class="form-group col-xs-3"  >
                <label for="EFisicoe">Espacio físico:</label>
                <select id="EFisicoe" name="EFisicoe" class="form-control" value="">
                  @foreach($TipoEspacio as $tipo)
                    <option value="{{ $tipo->id}}" title='{{$tipo->NombreEspacio}}'>{{$tipo->NombreEspacio}}</option>
                  @endforeach
                </select>
              </div>
              <div class="form-group col-xs-1"  >
                <label for="CantidadEfi">Cantidad:</label>
                <input name="CantidadEfi" id="CantidadEfi" type="number"   class="archivo form-control" maxlength="2" min="1" max="99"/>
              </div>
              <div class="form-group col-xs-2"  >
                <label for="EstadoActualefi">Estado actual:</label>
                <select id="EstadoActualefi" name="EstadoActualefi" class="form-control" value="">
                  @foreach($IFisicaEstado as $IEstado)
                    <option value="{{ $IEstado->id}}" title='{{$IEstado->Nombre}}'>{{$IEstado->Nombre}}</option>
                  @endforeach
                </select>
              </div>
              <div class="form-group col-xs-2"  >
                <label for="TipoSolicitudefi">Tipo de solicitud:</label>
                <select id="TipoSolicitudefi" name="TipoSolicitudefi" class="form-control" value="">
                  <option value="">Elige una</option>
                  @foreach($TipoSolicitud as $solicitud)
                    <option value="{{ $solicitud->id}}" title="{{$solicitud->Nombre}}">{{$solicitud->Nombre}}</option>
                  @endforeach
                </select>
              </div>
              <div class="form-group col-xs-12" style="border:1px solid #DDD;" id="reqVarios">
                <h5><strong>Elija requerimientos específicos:</strong></h5>
                <div class="col-xs-3">
                  <label class="checkbox-formulario">
                    <input type="checkbox" name="electricidad" id="electricidad" value="true" title="1" value="1">
                    <span class="label-text">1) Red eléctrica</span>
                  </label>
                </div><div class="col-xs-3">
                  <label class="checkbox-formulario">
                    <input type="checkbox" name="acueducto" id="acueducto" value="true" title="2" value="1">
                    <span class="label-text">2) Acueducto</span>
                  </label>
                </div><div class="col-xs-3">
                  <label class="checkbox-formulario">
                    <input type="checkbox" name="alcantarillado" id="alcantarillado" value="true" title="3" value="1">
                    <span class="label-text">3) Alcantarillado</span>
                  </label>
                </div><div class="col-xs-3">
                  <label class="checkbox-formulario">
                    <input type="checkbox" name="piso" id="piso" value="true"  title="4" value="1">
                    <span class="label-text">4) Mejoramiento de pisos</span>
                  </label>
                </div><div class="col-xs-3">
                  <label class="checkbox-formulario">
                    <input type="checkbox" name="perede" id="perede" value="true"  title="5" value="1">
                    <span class="label-text">5) Mejoramiento en paredes</span>
                  </label>
                </div>
                <div class="col-xs-3">
                  <label class="checkbox-formulario">
                    <input type="checkbox" name="puerta" id="puerta" value="true"  title="6" value="1">
                    <span class="label-text">6) Mejoramiento en puertas</span>
                  </label>
                </div>
                <div class="col-xs-3">
                  <label class="checkbox-formulario">
                    <input type="checkbox" name="cerramiento" id="cerramiento"  value="true" title="7" value="1">
                    <span class="label-text">7) Mejoramiento en cerramiento</span>
                  </label>
                </div>
                <div class="col-xs-3">
                  <label class="checkbox-formulario">
                    <input type="checkbox" name="cubierta" id="cubierta" value="true"  title="8" value="1">
                    <span class="label-text">8) Mejoramiento de cubierta</span>
                  </label>
                </div>
              </div>
              <div class="form-group col-xs-12" >
                <input type="button" name="anadirEfi" class="btn btn-primary" id="anadirEfi"  value="Añadir" style="float: right;">
              </div><br>
              <div id="info" class="alert alert-info col-sm-12 col-md-12 col-lg-12" style="height:3em; text-align:center; display:none;">
                <strong>Información:</strong> Un espacio físico, puede ser agregado más de una vez, sólo sí, el estado en que se desea agregar es diferente a los añadidos anteriormente.
              </div>
              <div id="infoTipoSolicitud" class="alert alert-info col-sm-12 col-md-12 col-lg-12" style="height:3em; text-align:center; display:none;">
                <strong>Información:</strong> Para añadir este espacio físico, debes elegir un tipo de solicitud.
              </div>
              <div id="añadir_requerimiento" class="alert alert-success col-sm-12 col-md-12 col-lg-12" style="height:3em; text-align:center; display:none;">
              <strong>Diagnóstico de necesidades añadido, para guardar el o los diagnóstico de necesidades use el botón "Registar".</strong>
              </div>
              <div class="panel-body">
                <br>
                <div class="col-xs-12" style="border-bottom:1px solid #337ab7; border-top:1px solid #337ab7; padding:0.8em; margin-top:0.5em; margin-bottom:0.5em; text-align:center;">
                  <strong>Observaciones generales de la sede</strong></div>
                <div class="form-group col-xs-6">
                  <label>Agregar archivo  <span class="label label-primary">PDF</span> / <span class="label label-primary">Imagen</span>&nbsp;&nbsp;el archivo no debe superar los 5 megabytes:</label>
                  <input name="anexoEvidencia" id="anexoEvidencia" type="file" files="true" enctype="”multipart/form-data”" accept="application/pdf,image/webp,image/apng,image/*,*/*;q=0.8" class="archivo form-control"><br>
                  <label for="observacion">Observación:</label>
                  <textarea class="form-control" style="resize:none" rows="5" id="observacion" name="observacion" maxlength="300"></textarea>
                  <br>
                </div>
                <div class="form-group col-xs-6">
                  <h5><strong>Otros requerimientos generales:</strong></h5>
                  <div class="form-check col-xs-12">
                    <label class="checkbox-formulario">
                      <input type="checkbox" name="Lote" title="" value="true">
                      <span class="label-text">Requiere lote</span>
                    </label>
                  </div>
                  <div class="form-check col-xs-12">
                    <label class="checkbox-formulario">
                      <input type="checkbox" name="EstudiosDT" title="" value="true">
                      <span class="label-text">Requiere estudios y diseños técnicos</span>
                    </label>
                  </div>
                  <div class="form-check col-xs-12">
                    <label class="checkbox-formulario">
                      <input type="checkbox" name="RepoTotal" title="" value="true">
                      <span class="label-text">Requiere reposición total</span>
                    </label>
                  </div>
                  <div class="form-check col-xs-12">
                    <label class="checkbox-formulario">
                      <input type="checkbox" name="Terminacion" title="" value="true">
                      <span class="label-text">Requiere terminación de obra inconclusa </span>
                    </label>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

        <div class="box-footer">
          <center>
            <button type="submit" id="btnsubmitSolicitudIF" onclick="return confirm('¿Desea guardar la información?')"class="btn btn-primary">Registrar</button>
          </center>
        </div>

      </form>

    </div>
  @endsection
@else
  @extends('errors.acceso')
@endif
