@extends('layouts.app')

@section('htmlheader_title')
Dispositivos
@endsection


@section('main-content')
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>

<!-- Inicio contenido principal -->
<section  id="contenido_principal">

    <div class="box box-primary">
        <div class="box-header" style="text-align:center;">
            <div >
                <div class="container" >
                    <h3><b>Dispositivos existentes</b></h3>
                    <p style="text-align:left;">Aquí encontrará la información de los dispositivos que poseen los establecimientos educativos administrados por la Secretaría de Educación de Antioquia</p>
                </div>
            </div>
        </div><!-- /.box-header -->

         <div class="box box-primary">
            <a href=" {{ url('form_nuevo_dispositivo') }}" type="submit" class="btn btn-primary"  style="margin: 15px 25px">Agregar nuevo dispositivo</a>
         </div>

        <div id="notificacion_resul_fci"></div>
        <div class="box box-primary" style="border: 1px solid #3c8dbc !important;">
        <div class="titleboxes" style="font-size:20px; text-align:center">Listado</div>

    <div class="box-body">
        <div class="table-responsive" >
            <table id="tbl_dispositivo" class="table table-hover table-striped" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th>Id</th>
                        <th>Sede</th>
                        <th>Nombre sede</th>
                        <th>Tipo</th>
                        <th>Marca</th>
                        <th>Serial</th>
                        <th>Estado</th>
                        <th>Acción</th>
                    </tr>
                </thead>

            </table>
        </div>
    </div>

    </div>

    <script>

    $(document).ready(function() {
      activar_tabla_empresas();
      function activar_tabla_empresas() {
         $('#tbl_dispositivo').DataTable({
             processing: true,
             serverSide: true,
             pageLength: 10,
             language: {
                      "url": '{!! asset('/plugins/datatables/latino.json') !!}'
                       } ,
             ajax: '{{ route('datatable.dispositivos') }}',
             columns: [
                 { data: 'id', name: 'id' },
                 { data: 'sede_id', name: 'sede_id' },
                 { data: 'nombre_sede', name: 'nombre_sede' },
                 { data: 'nombre', name: 'nombre' },
                 { data: 'marca', name: 'marca' },
                 { data: 'serial', name: 'serial' },
                 { data: 'estado', name: 'estado' },
                 { data: null,  render: function ( data, type, row )
                  {
                    return "<button type='button' title='Editar'  onclick='ver_editar_dispositivo("+ data.id +")' class='btn  btn-warning btn-xs' ><i class='fa fa-fw fa-edit'></i></button>"+"&nbsp;"+"<button type='button' title='Borrar' onclick='confirmacion_borrado_dispositivo("+ data.id +")' class='btn  btn-danger btn-xs' ><i class='fa fa-fw fa fa-trash-o'></i></button>"+"&nbsp;"+"<button type='button' title='Transladar' onclick='nuevo_traslado_dispositivo("+ data.id +")' class='btn  btn-default btn-xs' ><i class='fa fa-fw fa-mail-reply'></i></button>"
                  }
                  }

             ]

         });

     }
      });

    </script>

</section>
<!-- Fin contenido principal -->

@endsection
