@extends('layouts.app')

@section('htmlheader_title')
Carga Masiva de Datos De Dispositivos
@endsection

@section('main-content')

<section  id="contenido_principal">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
  <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
  <div class="box box-primary">
    <div class="box-header">
      <h3 class="text-center"><b>Carga masiva dispositivos</b></h3>
    </div><!-- /.box-header -->


    <div id="notificacion_resul_fci">
      <div class="modal" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title">Modal title</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <p>Modal body text goes here.</p>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-primary">Save changes</button>
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
          </div>
        </div>
      </div></div>


      <div id="formulario_archivos_dispositivos">
        <form  id="f_subir_archivos_dispositivos" name="f_subir_archivo_dispositivos" method="post"  files="true" action="{{ url('subir_carga_dispositivos') }}" class="formarchivodispositivos" enctype="multipart/form-data" >
          {{ csrf_field() }}
          <div class="box-body">
            <!-- Inicio -->
            <div class="panel panel-primary ">
              <div class="panel-heading">Dispositivos</div>
              <h5 style="margin-left:25px;"><b>Recomendaciones:</b></h5>
              <ol>

                <li><p>Aquí podrá ingresar grandes cantidades de dispositivos por medio del archivo "dispositivos.csv. Llene los requerimientos obligatorios y presione el botón "Cargar archivos".</p></li>
                <li><p>La plantilla para la carga la podrá descargar desde el botón "Descargar plantilla".</p></li>
                <li><p>El orden y el nombre de las columnas debe permanecer igual.</p></li>
                <li><p>Hay que tener en cuenta que las condiciones para una carga masiva son las mismas que las de ingresar un dispositivo individualmente:</p>
                  <ul>
                    <li>Los establecimientos y las sedes deben estar cargados en el DUE.</li>
                    <li>Los datos de las columnas sede_id, tipo_id, serial, origen id (Fuente del dispositivo) y fecha de entrega son obligatorias.</li>
                    <li>Tipo_id y origen_id (Fuente del dispositivo) deben estar creados en <a href="/editar_estructura_inventario">Estructura básica de inventario</a>.</li>
                    <li>No deben haber dos seriales iguales.</li>
                    <li>El formato de tipos de datos de las columnas debe ser el correcto, haciendo énfasis en las columnas sede_id y las relacionadas con las fechas.</li>
                  </ul>
                </li>
                <li><p>Existen columnas que solo se llenan o modifican por medio del sistema en el área de dispositivos.</p>
                <li><p>Se debe convertir el archivo a UTF-8.</p>
                  <!-- /El botón llama el id modal-utf8 para cargar video conversión de archivos a utf8 desde app.blade -->
                  <button class="btn btn-info fa fa-video-camera" style="font-size:15px;" data-target="#modal-utf8-Dispositivos" data-toggle="modal" type="button"> Conversión archivos UTF-8</button>
                  <!-- /Fin botón utf8 -->
                  </ol>

              <div class="panel-body">
                <div class="form-group col-xs-6">
                  <label>Agregar archivo  <span class="label label-primary">Dispositivos</span></label>
                  <input name="archivo_dispositivos" id="archivo_dispositivos" type="file" files="true"  enctype=”multipart/form-data”  class="archivo form-control"  required /><br /><br />
                </div>
              </div>

                </div>
              </div>
              <!-- Final -->

              <div>
              </div>
            </div>

            <div class="box-footer">
              <button type="submit" id="btnsubmit"  class="btn btn-primary fa fa-cloud-upload" style="font-size:15px;"> Cargar archivos</button>
              <a onclick="return confirm('Se debe convertir el archivo a UTF-8.')" href="{{ url('/descargar_plantilla_dispositivos') }}" target="_blank" id="descargar_plantilla" name="descargar_plantilla"  class="btn btn-info fa fa-cloud-download" style="font-size:15px;"> Descargar plantilla</a>




            </div>
          </form>
        </div>


        <div id="anexos" class="box box-primary">

          <h3 class="text-center">Archivos cargados</h3>
          <div class="box-body">

            <div class="table-responsive" >

              <table  class="table table-hover table-striped" cellspacing="0" width="100%">

                <thead>
                  <tr>
                    <th>Número de carga</th>
                    <th>Fecha de carga</th>
                    <th>Registros cargados</th>
                    <th>Acción</th>
                  </tr>
                </thead>
                <tbody>

                  @foreach($lista as $listado)
                  <tr role="row" class="odd">
                    <td>{{ $listado->numero_carga }}</td>
                    <td>{{ $listado->fecha }}</td>
                    <td>{{ $listado->cantidad }}</td>
                    <td>
                      <button type="button"  class="btn  btn-danger btn-xs" title="Eliminar" onclick="borrar_carga_dispositivo({{  $listado->numero_carga }});"  ><i class="fa fa-fw fa fa-trash-o"></i></button>
                    </td>
                  </tr>
                  @endforeach
                </tbody>
              </table>

              {{ $lista->links() }}
            </div>
          </div>
        </div>

        <script type="text/javascript">
        $('#infofiltro').toggle();
        window.setTimeout("mostrar()", 10000);

        function mostrar() {
          $("#infofiltro").slideToggle("slow", function() {
            $('#infofiltro').hide(5000);

          });
        }
        $(document).ready(function() {
          $("#clickme").click(function() {
            $("#infofiltro").toggle("slow", function() {

            });
          });
        });
      </script>

    </section>
    @endsection
