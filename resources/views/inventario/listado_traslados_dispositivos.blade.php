@extends('layouts.app')

@section('htmlheader_title')
Dispositivos
@endsection

@section('main-content')
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>

<!-- Inicio contenido principal -->
<section  id="contenido_principal">

    <div class="box box-primary">
        <div class="box-header" style="text-align:center;">
            <h3><b>Traslados de dispositivos.</b></h3>
        </div><!-- /.box-header -->
        <div id="notificacion_resul_fci"></div>
        <div class="box box-primary" style="border: 1px solid #3c8dbc !important;">
        <div class="titleboxes" style="font-size:20px; text-align:center">Listado</div>

    <div class="box-body">
        <div class="table-responsive">
            <table id="tbl_traslados" class="table table-hover table-striped" cellspacing="0" style="width:100%">
                <thead>
                    <tr>
                        <th>Id</th>
                        <th>Sede origen</th>
                        <th>Nombre origen</th>
                        <th>Sede destino</th>
                        <th>Nombre destino</th>
                        <th>Tipo dispositivo</th>
                        <th>Serial </th>
                        <th>Motivo</th>
                        <th>Fecha</th>
                        <th>Acción</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>

    </div>

<script>

$(document).ready(function() {
$('#tbl_traslados').DataTable({
  processing: true,
  serverSide: true,
  pageLength: 20,
  language: {
    "url": '{!! asset('/plugins/datatables/latino.json') !!}'
  } ,
  ajax: '{{ route('datatable.l_traslados') }}',
  columns: [
    { data: 'id', name: 'id' },
    { data: 'sede_origen_id', name: 'sede_origen_id' },
    { data: 'nombre_sede', name: 'nombre_sede' },
    { data: 'sede_destino_id', name: 'sede_destino_id' },
    { data: 'destino', name: 'destino' },
    { data: 'nombre', name: 'nombre' },
    { data: 'serial', name: 'serial' },
    { data: 'motivo', name: 'motivo' },
    { data: 'created_at', name: 'created_at' },

    { data: null,  render: function ( data, type, row )
      {
         return "<button type='button'  onclick='ver_editar_traslado("+ data.id +")' class='btn  btn-warning btn-xs'  title='Editar' ><i class='fa fa-fw fa-edit'></i></button>"+"&nbsp;"+
         "<button type='button'  onclick='confirmacion_borrado_traslado(" + data.id +")' class='btn  btn-danger btn-xs' ><i class='fa fa-fw fa fa-trash-o' title='Eliminar'></i></button>"

      }
    }

  ]

});

  });

$(document).ready(function(){
  $("#buscar").on("keyup", function() {
    var value = $(this).val().toLowerCase();
    $("#tbl_traslados tr").filter(function() {
      $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
    });
  });
});

</script>

</section>
<!-- Fin contenido principal -->

@endsection
