@if(Auth::user()->isRole('administrador_sistema') || Auth::user()->isRole('Administrador_infraestructura_educativa') || Auth::user()->isRole('Rectores') || Auth::user()->isRole('Secretarios'))
@extends('layouts.app')

@section('htmlheader_title')
Dispositivos
@endsection

@section('main-content')
  <style type="text/css">
  #tablegeneral th{
    vertical-align: middle;
    border-bottom: none;
    text-align: right;
  }
  #tablegeneral td{
    text-align: left;
    vertical-align: middle;
  }

  #tablegeneral tr{
    border-bottom: 1px solid #ddd;
  }


</style>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>

<!-- Inicio contenido principal -->
<section  id="contenido_principal">

    <div class="box box-primary">
      <div align="center" class="box-header">
        <h3 class="box-title"><strong>Diagnóstico de necesidades generales en Antioquia</strong></h3>
      </div>

      <div class="box-body"  >
        <div class="panel panel-primary ">
          <div style="font-size: 16px;" class="panel-heading">Diagnóstico de necesidades registrados</div>
        <div id="notificacion_resul_fci">

        <div class="modal" tabindex="-1" role="dialog">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title">Modal title</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body">
                <p>Modal body text goes here.</p>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-primary">Save changes</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
              </div>
            </div>
          </div>
        </div>
        </div>

        <!--<div class="box box-primary" style="border: 1px solid #3c8dbc !important;">
        <div class="titleboxes" style="font-size:20px; text-align:center">Listado</div>
        <div class="box box-primary">
           <a href=" {{ url('form_nuevo_modal') }}" type="submit" class="btn btn-primary"  style="margin: 15px 25px">Ingresar diagnóstico</a>
        </div>
      -->

        <div class="table-responsive" >
            <table id="tbl_solicitudes" class="table table-hover table-striped" cellspacing="0" style="width:100%">
                <thead>
                    <tr>
                        <th>Id</th>
                        <th>Sede</th>
                        <th>Nombre sede</th>
                        <th>Atendida</th>
                        <th>Fecha solicitud</th>
                        <th>Acción</th>
                    </tr>
                </thead>

            </table>


        </div>


    </div>
    </div>
    </div>

 <script>

    $(document).ready(function() {
      activar_tabla_empresas();
      function activar_tabla_empresas() {
         $('#tbl_solicitudes').DataTable({
             processing: true,
             serverSide: true,
             pageLength: 10,
             language: {
                      "url": '{!! asset('/plugins/datatables/latino.json') !!}'
                       } ,
             ajax: '{{ route('datatable.solicitudes') }}',
             columns: [
                 { data: 'id', name: 'id' },
                 { data: 'sede_id', name: 'sede_id' },
                 { data: 'nombre_sede', name: 'nombre_sede' },
                 { data: 'estado_atencion', name: 'estado_atencion' },
                 { data: 'created_at', name: 'created_at' },





                 { data: null,  render: function ( data, type, row )
                  {
                    return "<button type='button' title='Editar' onclick='ver_editar_solicitud("+ data.id +")' class='btn  btn-warning btn-xs' ><i class='fa fa-fw fa-edit'></i></button>"+"&nbsp;"+"<button type='button' title='Borrar'  onclick='confirmacion_borrado_solicitud("+ data.id +")' class='btn  btn-danger btn-xs' ><i class='fa fa-fw fa fa-trash-o'></i></button>"
                  }
                  }

             ]

         });

     }
      });

    </script>

</section>
<!-- Fin contenido principal -->

@endsection
@else
  @extends('errors.acceso')
@endif
