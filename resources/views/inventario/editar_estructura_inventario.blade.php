@extends('layouts.app')

@section('htmlheader_title')
    Editar Estructura Inventario
@endsection

@section('main-content')

<section  id="contenido_principal">

<!-- Encabezado -->
<div class="box-header">
    <h3 class="text-center"><b>Editar estructura inventario</b></h3>
</div>

<!-- Fuente de dispositivo -->
<div class="box box-solid box-primary collapsed-box">
        <div class="box-header with-border">
            <h3 class="box-title">Fuente de los dispositivos</h3>
            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip">
                <i class="fa fa-plus"></i></button>
            </div>
        </div>

    <!-- Body -->
    <div class="box-body">
        <div class="margin" id="botones_control">
          <a href="javascript:void(0);" class="btn  btn-primary" onclick="cargar_formulario(24);">Nueva fuente</a>
        </div>

    {{ $origenes->links() }}

    @if(count($origenes)==0)
    <div class="box box-primary col-xs-12">
        <div class='aprobado' style="margin-top:70px; text-align: center">
            <label style='color:#177F6B'>
               No se encontraron fuentes de dispositivos.
            </label>
        </div>
    </div>
    @endif

    <div class="table-responsive">
        <table class="table table-hover table-striped" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th>Id</th>
                    <th>Nombre</th>
                    <th>Descripción</th>
                    <th>Acción</th>
                </tr>
            </thead>
            <tbody>
                @foreach($origenes as $origen)
                <tr role="row" class="odd">
                <td>{{ $origen->id }}</td>
                <td>{{ $origen->nombre }}</td>
                <td>{{ $origen->descripcion }}</td>
                <td>
                <button type="button"  class="btn  btn-warning btn-xs" title="Editar" onclick="verinfo_origen({{  $origen->id }})" ><i class="fa fa-fw fa-edit"></i></button>
                <button type="button"  class="btn  btn-danger btn-xs"  title="Eliminar" onclick="borrado_origen({{  $origen->id }});"  ><i class="fa fa-fw fa fa-trash-o"></i></button>
                </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>

    </div>
    <!-- Fin body -->
</div>
<!-- Fin fuente dispositivos -->

<!-- Tipo dispositivos -->
<div class="box box-solid box-primary collapsed-box">
    <div class="box-header with-border">
        <h3 class="box-title" >Tipos de dispositivo</h3>
        <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip">
            <i class="fa fa-plus"></i></button>
        </div>
    </div>

    <!-- Body -->
    <div class="box-body">
        <div class="margin" id="botones_control">
          <a href="javascript:void(0);" class="btn  btn-primary" onclick="cargar_formulario(25);">Nuevo tipo de dispositivo</a>
        </div>

    {{ $tipos->links() }}

    @if(count($tipos)==0)
    <div class="box box-primary col-xs-12">
        <div class='aprobado' style="margin-top:70px; text-align: center">
            <label style='color:#177F6B'>
             No se encontraron tipos de dispositivos.
            </label>
        </div>
    </div>
    @endif

    <div class="table-responsive">
        <table class="table table-hover table-striped" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th>Id</th>
                    <th>Nombre</th>
                    <th>Descripción</th>
                    <th>Acción</th>
                </tr>
            </thead>
            <tbody>
                @foreach($tipos as $tipo)
                <tr role="row" class="odd">
                <td>{{ $tipo->id }}</td>
                <td>{{ $tipo->nombre }}</td>
                <td>{{ $tipo->descripcion }}</td>
                <td>
                <button type="button"  class="btn  btn-warning btn-xs" title="Editar" onclick="verinfo_tipodispositivo({{  $tipo->id }})" ><i class="fa fa-fw fa-edit"></i></button>
                <button type="button"  class="btn  btn-danger btn-xs"  title="Eliminar" onclick="borrado_tipodispositivo({{  $tipo->id }});"  ><i class="fa fa-fw fa fa-trash-o"></i></button>
                </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
    </div>
    <!-- Fin body -->
</div>
<!-- Fin tipo dispositivos -->

<!-- Proyectos -->
@role('administrador_sistema')
<div class="box box-solid box-primary collapsed-box">
    <div class="box-header with-border">
        <h3 class="box-title" >Proyectos</h3>
        <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip">
            <i class="fa fa-plus"></i></button>
        </div>
    </div>

    <!-- Body -->
    <div class="box-body">
        <div class="margin" id="botones_control">
          <a href="javascript:void(0);" class="btn  btn-primary" onclick="cargar_formulario(26);">Nuevo proyecto</a>
        </div>

    {{ $proyectos->links() }}

    @if(count($proyectos)==0)
    <div class="box box-primary col-xs-12">
        <div class='aprobado' style="margin-top:70px; text-align: center">
            <label style='color:#177F6B'>
                 No se encontraron proyectos.
            </label>
        </div>
    </div>
    @endif

    <div class="table-responsive">
        <table class="table table-hover table-striped" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th>Id</th>
                    <th>Nombre</th>
                    <th>Plan de desarrollo</th>
                    <th>Periodo</th>
                    <th>Acción</th>
                </tr>
            </thead>
            <tbody>
                @foreach($proyectos as $proyecto)
                <tr role="row" class="odd">
                <td>{{ $proyecto->id }}</td>
                <td>{{ $proyecto->nombre }}</td>
                <td>{{ $proyecto->plan_desarrollo }}</td>
                <td>{{ $proyecto->periodo }}</td>
                <td>
                <button type="button"  class="btn  btn-warning btn-xs" title="Editar" onclick="verinfo_proyecto({{  $proyecto->id }})" ><i class="fa fa-fw fa-edit"></i></button>
                <button type="button"  class="btn  btn-danger btn-xs"  title="Eliminar"  onclick="borrado_proyecto({{  $proyecto->id }});"  ><i class="fa fa-fw fa fa-trash-o"></i></button>
                </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
    </div>
    <!-- Fin body -->
</div>
@endrole
<!-- Fin proyectos -->

<!-- Contratos -->
<div class="box box-solid box-primary collapsed-box">
    <div class="box-header with-border">
        <h3 class="box-title" >Contratos</h3>
        <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip">
            <i class="fa fa-plus"></i></button>
        </div>
    </div>

    <!-- Body -->
    <div class="box-body">
        <div class="margin" id="botones_control">
          <a href="javascript:void(0);" class="btn  btn-primary" onclick="cargar_formulario(27);">Nuevo contrato</a>
        </div>

    {{ $contratos->links() }}

    @if(count($contratos)==0)
    <div class="box box-primary col-xs-12">
        <div class='aprobado' style="margin-top:70px; text-align: center">
            <label style='color:#177F6B'>
                No se encontraron contratos.
            </label>
        </div>
    </div>
    @endif

    <div class="table-responsive">
        <table class="table table-hover table-striped" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th>Id</th>
                    <th>Número contrato</th>
                    <th>Fecha inicio</th>
                    <th>Teléfono mesa de ayuda</th>
                    <th>Proyecto asociado</th>
                    <th>Acción</th>
                </tr>
            </thead>
            <tbody>
                @foreach($contratos as $contrato)
                <tr role="row" class="odd">
                <td>{{ $contrato->id }}</td>
                <td>{{ $contrato->numero_contrato }}</td>
                <td>{{ $contrato->fecha_acta_inicio }}</td>
                <td>{{ $contrato->telefono_mesa_ayuda }}</td>
                <td>{{ $contrato->proyecto_id }}</td>
                <td>
                <button type="button"  class="btn  btn-warning btn-xs" title="Editar"  onclick="verinfo_contrato({{  $contrato->id }})" ><i class="fa fa-fw fa-edit"></i></button>
                <button type="button"  class="btn  btn-danger btn-xs"  title="Eliminar" onclick="borrado_contrato({{  $contrato->id }});"  ><i class="fa fa-fw fa fa-trash-o"></i></button>
                </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
    </div>
    <!-- Fin body -->
</div>
<!-- Fin contratos -->

<div>

</div>

</section>
@endsection
