@extends('layouts.app')

@section('htmlheader_title')
	Página principal
@endsection


@section('main-content')

<script type="text/javascript">
function ocultar_informacion(){

//función encargada de ocualtar o cerrar el modal de información que se activa al ingresar al sistema.

	document.getElementById('verInformacion').style.display = 'none';
}
</script>




  <!-- Modal content-->
  <!-- modal encargado de mostrar cualquier tipo de información por parte de la gobernación de antioquia
	al usuario del sistema. el modal se activa cada vez que el usuario ingrese al sistema-->
	<div class="modal fade in" id='verInformacion'  role="dialog" style="display: block; padding-right: 17px;">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<h4 class="modal-title">
						<strong>Información</strong>
					</h4>
				</div>
				<div class="modal-body">
			      Mensajes de información por parte de la Gobernación de Antioquia
				</div>
				<div class="modal-footer">
					<button onclick="ocultar_informacion()" class="btn btn-primary" data-dismiss="modal" type="button">Cerrar</button>
				</div>
			</div>
		</div>
	</div>


	<div class="container-fluid spark-screen">
		<div class="row">
			<div class="col-md-8 col-md-offset-2">
				<div class="panel panel-default" style="text-align:center">
					<div class="panel-heading">
						<h4>Sistema de información educativo de la Gobernación de Antioquia</h4>
					</div>
					<div class="panel-body">
						<img src="{{ asset('/img/asd.png') }}" >
					</div>
				</div>
			</div>
		</div>
	</div>

@endsection
