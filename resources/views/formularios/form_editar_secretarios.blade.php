<section  >
  <div class="col-md-12">

    <div class="box box-primary  box-gris">

      <div class="box-header with-border my-box-header">
        <h4 class="box-title"><strong>Editar información del secretario(a) de educación del municipio de {{$nombre}}</strong></h4>
        <button onclick="cerrar_modal()" type="button" class="close" style="font-size: 40px;"  aria-label="Close">
          <span  aria-hidden="true">&times;</span><!-- Botón cerrar para volver a la vista sin necesidad de recargar la página -->
        </button>
        <p style="color:red;">* Obligatorio</p>
      </div><!-- /.box-header -->

      <div class="box-body">

        <form   action="{{ url('editar_secretarios') }}"  method="post" id="f_crear_sedesaulas" class="formentrada"  >

          <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
          <input type="hidden" name="informacion_id" value="{{$informacion->id}}">

          <div class="row" style="padding:20px;">

            <div class="col-md-12">
              <div class="form-group">
                <label class="col-sm-3" for="nombre"> Nombres <span style="color:red;font-size:20px;">*</span></label>
                <div class="col-xs-3" >
                  <input type="text"  onkeyup="javascript:this.value=this.value.toUpperCase();" pattern="[a-zA-ZñÑáéíóúÁÉÍÓÚ\s]+" title="Solo ingrese texto" class="form-control" name="nombres" value="{{$informacion->nombre_secretario}}"  required>
                </div>
              </div><!-- /.form-group -->
            </center>
          </div><!-- /.col -->

          <div class="col-md-12">
            <div class="form-group">
              <label class="col-sm-3" for="nombre"> Apellidos <span style="color:red;font-size:20px;">*</span></label>
              <div class="col-xs-3" >
                <input type="text"  onkeyup="javascript:this.value=this.value.toUpperCase();" pattern="[a-zA-ZñÑáéíóúÁÉÍÓÚ\s]+" title="Solo ingrese texto" class="form-control" name="apellidos" value="{{$informacion->apellido_secretario}}" required>
              </div>
            </div><!-- /.form-group -->
          </div><!-- /.col -->

          <div class="col-md-12">
            <div class="form-group">
              <label class="col-sm-3" for="nombre"> Celulares <span style="color:red;font-size:20px;">*</span></label>
              <div class="col-xs-3" >
                <input type="text" class="form-control" placeholder="No ingrese signo más(+) ni menos(-)"  name="celular" value="{{$informacion->celular_secretario}}"   required>
              </div>
            </div><!-- /.form-group -->
          </div><!-- /.col -->

          <div class="col-md-12">
            <div class="form-group">
              <label class="col-sm-3" for="nombre"> Teléfono <span style="color:red;font-size:20px;">*</span></label>
              <div class="col-xs-3" >
                <input type="text" class="form-control" name="telefono" value="{{$informacion->telefono_secretario}}" required>
              </div>
            </div><!-- /.form-group -->
          </div><!-- /.col -->

          <div class="col-md-12">
            <div class="form-group">
              <label class="col-sm-3" for="nombre"> Correo personal <span style="color:red;font-size:20px;">*</span></label>
              <div class="col-xs-3" >
                <input type="text" pattern="[a-zA-ZàáâäãåąčćęèéêëėįìíîïłńòóôöõøùúûüųūÿýżźñçčšžÀÁÂÄÃÅĄĆČĖĘÈÉÊËÌÍÎÏĮŁŃÒÓÔÖÕØÙÚÛÜŲŪŸÝŻŹÑßÇŒÆČŠŽ∂ð ,.'-_]{2,64}" class="form-control" name="correo_personal" value="{{$informacion->correo_secretario}}"  required>
              </div>
            </div><!-- /.form-group -->
          </div><!-- /.col -->

          <div class="col-md-12">
            <div class="form-group">
              <label class="col-sm-3" for="nombre"> Correo secretaría de educación <span style="color:red;font-size:20px;">*</span></label>
              <div class="col-xs-3" >
                <input type="text" pattern="[a-zA-ZàáâäãåąčćęèéêëėįìíîïłńòóôöõøùúûüųūÿýżźñçčšžÀÁÂÄÃÅĄĆČĖĘÈÉÊËÌÍÎÏĮŁŃÒÓÔÖÕØÙÚÛÜŲŪŸÝŻŹÑßÇŒÆČŠŽ∂ð ,.'-_]{2,64}" class="form-control" name="correo_secretaria" value="{{$informacion->correo_secretariaeducacion}}" required>
              </div>
            </div><!-- /.form-group -->
          </div><!-- /.col -->

          <div class="col-md-12">
            <div class="form-group">
              <label class="col-sm-3" for="nombre"> Cargo <span style="color:red;font-size:20px;">*</span></label>
              <div class="col-xs-3" >
                <input type="text"  onkeyup="javascript:this.value=this.value.toUpperCase();" pattern="[a-zA-ZñÑáéíóúÁÉÍÓÚ\s,]+" title="Solo ingrese texto" class="form-control" name="cargo" value="{{$informacion->cargo_secretario}}"  required>
              </div>
            </div><!-- /.form-group -->
          </div><!-- /.col -->

          <div class="col-md-12">
            <div class="form-group">
              <label class="col-sm-3" for="nombre"> Dirección oficina <span style="color:red;font-size:20px;">*</span></label>
              <div class="col-xs-3" >
                <input type="text" onkeyup="javascript:this.value=this.value.toUpperCase();" class="form-control" name="direccion" value="{{$informacion->direccion_secretario}}"  required>
              </div>
            </div><!-- /.form-group -->
          </div><!-- /.col -->


          <div style="margin-top:15px;" class="box-footer col-xs-12 box-gris ">
            <center>
              <button  type="submit" onclick="return confirm('¿Desea guardar la información?')"  class="btn btn-primary">Enviar</button><!-- Botón para registrar la información de una nueva sede educativa -->
              <a onclick="cerrar_modal()" style="margin-left:15px;" class="btn btn-primary">Cancelar</a><!-- Botón para cancerlar el ingreso de una nueva sede educativa
                y volver a la vista sin necesidad de recargar la página -->
              </center>
            </div>
          </div><!-- /.form-group -->
        </div><!-- /.col -->


      </form>

    </div>
  </div>
</div>

</section>
