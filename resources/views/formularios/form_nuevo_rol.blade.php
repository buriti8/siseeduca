
<section  >
  <div class="col-md-12">

    <div class="box box-primary  box-gris">

      <div class="box-header with-border my-box-header">
        <button onclick="cerrar_modal()" type="button" class="close" style="font-size: 40px; color:#34495E;"  aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button> <!-- Botón cerrar para volver a la vista sin necesidad de recargar la página -->
        <h3 class="box-title"><strong>Ingresar nuevo rol</strong></h3>
        <p style="color:red;">* Obligatorio</p>
      </div><!-- /.box-header -->


      <div class="box-body">

        <form   action="{{ url('crear_rol') }}"  method="post" id="f_crear_rol" class="formentrada"  >
          <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">

          <div class="col-md-12">
            <div class="form-group">
              <label class="col-sm-2" for="apellido">Nombre del rol<span style="color:red;font-size:20px;">*</span></label>
              <div class="col-sm-10" >
                <input style="width:450px;" type="text" class="form-control" id="rol_nombre" name="rol_nombre" required >
              </div>
            </div><!-- /.form-group -->

          </div><!-- /.col -->

          <div class="col-md-12">
            <div class="form-group">
              <label class="col-sm-2" for="apellido">Slug<span style="color:red;font-size:20px;">*</span></label>
              <div class="col-sm-10" >
                <input type="text" style="width:450px;" class="form-control" id="rol_slug" name="rol_slug" required >
              </div>
            </div><!-- /.form-group -->

          </div><!-- /.col -->

          <div class="col-md-12">
            <div class="form-group">
              <label class="col-sm-2" for="apellido">Descripción<span style="color:red;font-size:20px;">*</span></label>
              <div class="col-sm-10" >
                <textarea type="text" style="width:450px; height: 100px;" class="form-control" id="rol_descripcion" name="rol_descripcion"  required></textarea>
              </div>
            </div><!-- /.form-group -->

          </div><!-- /.col -->


          <div class="box-footer col-xs-12 box-gris ">
            <center>
              <button type="submit" class="btn btn-primary">Crear nuevo rol</button>
            </center>
          </div>
        </form>

      </div>

    </div>

  </div>


  <div class="col-md-12">

    <div class="table-responsive" >

      <table  class="table table-hover table-striped" cellspacing="0" width="100%">
        <thead>
          <tr>    <th>Código</th>
            <th>Nombre</th>
            <th>Slug</th>
            <th>Descripción</th>
            <th>Acción</th>
          </tr>
        </thead>
        <tbody>

          @foreach($roles as $rol)
            <tr role="row" class="odd" id="filaR_{{  $rol->id }}">
              <td>{{ $rol->id }}</td>
              <td><span class="label label-default">{{ $rol->name or "Ninguno" }}</span></td>
              <td class="mailbox-messages mailbox-name"><a href="javascript:void(0);" style="display:block"><i class="fa fa-user"></i>&nbsp;&nbsp;{{ $rol->slug  }}</a></td>
              <td>{{ $rol->description }}</td>
              <td>
                <button type="submit"  class="btn  btn-danger btn-xs" title="Eliminar" onclick="borrar_rol({{ $rol->id }});"   ><i class="fa fa-fw fa fa-trash-o"></i></button>
              </td>
            </tr>
          @endforeach

        </tbody>
      </table>
    </div>

    <center>
      <a onclick="cerrar_modal()" style="margin-left:10px;" class="btn btn-primary">Regresar</a>
    </center>

  </div>

</section>
