<style>
div.form-group {
  text-transform: capitalize;
}
</style>
<section >

  <div class="row" >

    <div class="col-md-12">


      <div class="box box-primary box-gris">
        <div class="box-header with-border my-box-header">
          <button onclick="cerrar_modal()" type="button" class="close" style="font-size: 40px;"  aria-label="Close">
             <span  aria-hidden="true">&times;</span><!-- Botón cerrar para volver a la vista sin necesidad de recargar la página -->
          </button>
          <!-- por medio del foreach recorremos el array que nos envío el controlador
          y mostramos el nombre de la sede-->
          @foreach($nombres as $nombre)
            <h5><strong>{{$nombre->nombre_municipio}} &nbsp;&nbsp; - &nbsp;&nbsp; {{$nombre->nombre_establecimiento}} &nbsp;&nbsp; - &nbsp;&nbsp; {{$nombre->nombre_sede}}  &nbsp;&nbsp; - &nbsp;&nbsp; {{$nombre->direccion}} &nbsp;&nbsp; -
            &nbsp;&nbsp; Código Dane: {{ $sedesespacio->DaneSede}}</strong></h5>
          @endforeach

          <p>En este formulario se podrá actualizar la cantidad de espacios de la sede educativa.</p>
          <p style="color:red;">* Obligatorio</p>
        </div><!-- /.box-header -->

        <div id="notificacion_E2" ></div>
        <div class="box-body">


          <form   action="{{ url('editar_sedesaulas') }}"  method="post" id="f_editar_sedesaulas"  class="formentrada"  >
            <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
            <input type="hidden" name="id_sedesespacio" value="{{ $sedesespacio->id }}">
            <input type="hidden" name="sede_id" value="{{ $sedesespacio->DaneSede }}">

            <div class="col-md-12">
              <div class="form-group">
                <label class="col-sm-3" for="nombre">Aulas de preescolar <span style="color:red;font-size:20px;">*</span></label>
                <div class="col-xs-3" >
                  <input type="number" class="form-control" id="aulaspreescolar" name="aulaspreescolar" min="0" max="99"  value="{{ $sedesespacio->AulasPreescolar }}"  required   >
                </div>
              </div><!-- /.form-group -->
            </div><!-- /.col -->

            <div class="col-md-12">
              <div class="form-group">
                <label class="col-sm-3" for="nombre"> Aulas de primaria <span style="color:red;font-size:20px;">*</span></label>
                <div class="col-xs-3" >
                  <input type="number" class="form-control" id="aulasprimaria" name="aulasprimaria" min="0" max="99"  value="{{ $sedesespacio->AulasPrimaria }}"  required   >
                </div>
              </div><!-- /.form-group -->
            </div><!-- /.col -->

            <div class="col-md-12">
              <div class="form-group">
                <label class="col-sm-3" for="nombre"> Aulas de secundaria <span style="color:red;font-size:20px;">*</span></label>
                <div class="col-xs-3" >
                  <input type="number" class="form-control" id="aulassecundaria" name="aulassecundaria" min="0" max="99" value="{{ $sedesespacio->AulasSecundaria }}"  required   >
                </div>
              </div><!-- /.form-group -->
            </div><!-- /.col -->

            <div class="col-md-12">
              <div class="form-group">
                <label class="col-sm-3" for="nombre"> Aulas de especiales <span style="color:red;font-size:20px;">*</span></label>
                <div class="col-xs-3" >
                  <input type="number" class="form-control" id="aulasespaciales" name="aulasespaciales" min="0" max="99"  value="{{ $sedesespacio->AulasEspaciales }}"  required   >
                </div>
              </div><!-- /.form-group -->
            </div><!-- /.col -->

            <div class="col-md-12">
              <div class="form-group">
                <label class="col-sm-3" for="nombre"> Bibliotecas <span style="color:red;font-size:20px;">*</span></label>
                <div class="col-xs-3" >
                  <input type="number" class="form-control" id="biblioteca" name="biblioteca" min="0" max="99" value="{{ $sedesespacio->Biblioteca }}"  required   >
                </div>
              </div><!-- /.form-group -->
            </div><!-- /.col -->

            <div class="col-md-12">
              <div class="form-group">
                <label class="col-sm-3" for="nombre"> Aulas de sistemas <span style="color:red;font-size:20px;">*</span></label>
                <div class="col-xs-3" >
                  <input type="number" class="form-control" id="aulassistemas" name="aulassistemas" min="0" max="99" value="{{ $sedesespacio->AulasSistemas }}"  required   >
                </div>
              </div><!-- /.form-group -->
            </div><!-- /.col -->

            <div class="col-md-12">
              <div class="form-group">
                <label class="col-sm-3" for="nombre"> Aulas de bilingüismo <span style="color:red;font-size:20px;">*</span></label>
                <div class="col-xs-3" >
                  <input type="number" class="form-control" id="aulasbilinguismo" name="aulasbilinguismo" min="0" max="99"  value="{{ $sedesespacio->AulasBilinguismo }}"  required   >
                </div>
              </div><!-- /.form-group -->
            </div><!-- /.col -->

            <div class="col-md-12">
              <div class="form-group">
                <label class="col-sm-3" for="nombre">Laboratorios <span style="color:red;font-size:20px;">*</span></label>
                <div class="col-xs-3" >
                  <input type="number" class="form-control" id="laboratorio" name="laboratorio" min="0" max="99"  value="{{ $sedesespacio->Laboratorio }}"  required   >
                </div>
              </div><!-- /.form-group -->
            </div><!-- /.col -->

            <div class="col-md-12">
              <div class="form-group">
                <label class="col-sm-3" for="nombre"> Aulas de talleres <span style="color:red;font-size:20px;">*</span></label>
                <div class="col-xs-3" >
                  <input type="number" class="form-control" id="aulastalleres" name="aulastalleres" min="0" max="99"  value="{{ $sedesespacio->AulasTalleres }}"  required   >
                </div>
              </div><!-- /.form-group -->
            </div><!-- /.col -->

            <div class="col-md-12">
              <div class="form-group">
                <label class="col-sm-3" for="nombre"> Aulas múltiples <span style="color:red;font-size:20px;">*</span></label>
                <div class="col-xs-3" >
                  <input type="number" class="form-control" id="aulasmultiples" name="aulasmultiples" min="0" max="99"  value="{{ $sedesespacio->AulasMultiples }}"  required   >
                </div>
              </div><!-- /.form-group -->
            </div><!-- /.col -->

            <div class="box-footer col-xs-12 box-gris ">
              <center>
              <button type="submit" class="btn btn-primary">Enviar</button>
              <a onclick="cerrar_modal()" style="margin-left:15px;" class="btn btn-primary">Cancelar</a><!-- Botón para cancerlar el ingreso de una nueva sede educativa
                y volver a la vista sin necesidad de recargar la página -->
  </center>
              </div>

            </form>

          </div>

        </div>

      </div>
    </div>
  </section>
