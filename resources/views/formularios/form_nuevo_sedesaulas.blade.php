<section  >
  <div class="col-md-12">

    <div class="box box-primary  box-gris">

            <div class="box-header with-border my-box-header">
                <h3 class="box-title"><strong>Espacios físicos de la sede educativa</strong></h3>
                <button onclick="cerrar_modal()" type="button" class="close" style="font-size: 40px;"  aria-label="Close">
                  <span  aria-hidden="true">&times;</span><!-- Botón cerrar para volver a la vista sin necesidad de recargar la página -->
                </button>
                <p>En este espacio se especificará la información de la infraestructura física de la sede educativa.</p>
                <p style="color:red;">* Obligatorio</p>
          </div><!-- /.box-header -->

      <div class="box-body">

        <form   action="{{ url('crear_sedesespacio') }}"  method="post" id="f_crear_sedesaulas" class="formentrada"  >
          <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">


          <div class="row" style="padding:20px;">
            <div class="col-md-12">
              <div class="titleboxes" style="font-size:15px; text-align:center;">
              </div>
              <br>

              @role('secretarios')
              @include('partialsroles.secretario')
            @else
              @role('rectores')
              @include('partialsroles.rector')
            @else
              @include('partialsroles.filtro')
              @endrole
              @endrole

            </div>


  <div class="col-md-12">
      <div class="form-group">
            <label class="col-sm-3" for="nombre"> Aulas de preescolar<span style="color:red;font-size:20px;">*</span></label>
            <div class="col-xs-3" >
              <input type="number" class="form-control" id="aulaspreescolar" name="aulaspreescolar" min="0" max="99"   required   >
               </div>
      </div><!-- /.form-group -->
  </div><!-- /.col -->

  <div class="col-md-12">
      <div class="form-group">
            <label class="col-sm-3" for="nombre"> Aulas de primaria<span style="color:red;font-size:20px;">*</span></label>
            <div class="col-xs-3" >
              <input type="number" class="form-control" id="aulasprimaria" name="aulasprimaria" min="0" max="99"   required   >
               </div>
      </div><!-- /.form-group -->
  </div><!-- /.col -->

  <div class="col-md-12">
      <div class="form-group">
            <label class="col-sm-3" for="nombre"> Aulas de secundaria<span style="color:red;font-size:20px;">*</span></label>
            <div class="col-xs-3" >
              <input type="number" class="form-control" id="aulassecundaria" name="aulassecundaria" min="0" max="99"   required   >
               </div>
      </div><!-- /.form-group -->
  </div><!-- /.col -->

  <div class="col-md-12">
      <div class="form-group">
            <label class="col-sm-3" for="nombre"> Aulas de especiales<span style="color:red;font-size:20px;">*</span></label>
            <div class="col-xs-3" >
              <input type="number" class="form-control" id="aulasespaciales" name="aulasespaciales" min="0" max="99"   required   >
               </div>
      </div><!-- /.form-group -->
  </div><!-- /.col -->

  <div class="col-md-12">
      <div class="form-group">
            <label class="col-sm-3" for="nombre"> Bibliotecas<span style="color:red;font-size:20px;">*</span></label>
            <div class="col-xs-3" >
              <input type="number" class="form-control" id="biblioteca" name="biblioteca" min="0" max="99"   required   >
               </div>
      </div><!-- /.form-group -->
  </div><!-- /.col -->

  <div class="col-md-12">
      <div class="form-group">
            <label class="col-sm-3" for="nombre"> Aulas de sistemas<span style="color:red;font-size:20px;">*</span></label>
            <div class="col-xs-3" >
              <input type="number" class="form-control" id="aulassistemas" name="aulassistemas" min="0" max="99"   required   >
               </div>
      </div><!-- /.form-group -->
  </div><!-- /.col -->

  <div class="col-md-12">
      <div class="form-group">
            <label class="col-sm-3" for="nombre"> Aulas de bilingüismo<span style="color:red;font-size:20px;">*</span></label>
            <div class="col-xs-3" >
              <input type="number" class="form-control" id="aulasbilinguismo" name="aulasbilinguismo"  min="0" max="99"   required   >
               </div>
      </div><!-- /.form-group -->
  </div><!-- /.col -->

  <div class="col-md-12">
      <div class="form-group">
            <label class="col-sm-3" for="nombre"> Laboratorios de la sede educativa<span style="color:red;font-size:20px;">*</span></label>
            <div class="col-xs-3" >
              <input type="number" class="form-control" id="laboratorio" name="laboratorio" min="0" max="99"  required   >
               </div>
      </div><!-- /.form-group -->
  </div><!-- /.col -->

  <div class="col-md-12">
      <div class="form-group">
            <label class="col-sm-3" for="nombre"> Aulas de talleres<span style="color:red;font-size:20px;">*</span></label>
            <div class="col-xs-3" >
              <input type="number" class="form-control" id="aulastalleres" name="aulastalleres" min="0" max="99"  required   >
               </div>
      </div><!-- /.form-group -->
  </div><!-- /.col -->

  <div class="col-md-12">
      <div class="form-group">
            <label class="col-sm-3" for="nombre"> Aulas múltiples<span style="color:red;font-size:20px;">*</span></label>
            <div class="col-xs-3" >
              <input type="number" class="form-control" id="aulasmultiples" name="aulasmultiples" min="0" max="99"  required   >
               </div>
      </div><!-- /.form-group -->
  </div><!-- /.col -->

                <div style="margin-top:15px;" class="box-footer col-xs-12 box-gris ">
                  <center>
                    <button  type="submit" onclick="return confirm('¿Desea guardar la información?')"  class="btn btn-primary">Enviar</button><!-- Botón para registrar la información de una nueva sede educativa -->
                   <a onclick="cerrar_modal()" style="margin-left:15px;" class="btn btn-primary">Cancelar</a><!-- Botón para cancerlar el ingreso de una nueva sede educativa
                     y volver a la vista sin necesidad de recargar la página -->
                 </center>
                </div>
              </div><!-- /.form-group -->
            </div><!-- /.col -->


            </form>

          </div>
    </div>
</div>

</section>
