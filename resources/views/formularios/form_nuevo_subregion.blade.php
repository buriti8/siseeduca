<section>

  <div class="row" >

  <div class="col-md-12">

    <div class="box box-primary  box-gris">

      <div class="box-header with-border my-box-header">
        <h3 class="box-title"><strong>Nueva Subregión</strong></h3>
        <button onclick="cerrar_modal()" type="button" class="close" style="font-size: 40px; color:#34495E;"  aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button> <!-- Botón cerrar para volver a la vista sin necesidad de recargar la página -->
        <p style="color:red;">* Obligatorio</p>
      </div>

      <div class="box-body">

        <form   action="{{ url('crear_subregion') }}"  method="post" id="f_crear_subregion" class="formentrada"  >
          <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">

          <div class="col-md-12">
            <div class="form-group">
              <label class="col-sm-2" for="nombre">Código Subregión<span style="color:red;font-size:20px;">*</span> </label>
              <div class="col-xs-3" >
                <input type="number" class="form-control" id="codigo_subregion" name="codigo_subregion" required >
              </div>
            </div><!-- /.form-group -->
          </div><!-- /.col -->

          <div class="col-md-12">
            <div class="form-group">
              <label class="col-sm-2" for="apellido">Nombre Subregión <span style="color:red;font-size:20px;">*</span> </label>
              <div class="col-xs-3" >
                <input type="text" pattern="[a-zA-ZñÑáéíóúÁÉÍÓÚ\s]+" title="Solo ingrese texto" class="form-control" id="nombre_subregion" name="nombre_subregion" required >
              </div>
            </div><!-- /.form-group -->
          </div><!-- /.col -->

          <div class="col-md-12">
            <div class="form-group">
              <label class="col-sm-2" for="apellido">Código Departamento <span style="color:red;font-size:20px;">*</span> </label>
              <div class="col-xs-3" >

                <select  type="text" class="form-control" id="codigo_departamento" name="codigo_departamento" required >
                  <option>Seleccione un Departamento</option>
                  @foreach($departamentos as $departamento)
                  <option value="{{ $departamento->CodigoDepartamento }}">{{ $departamento->NombreDepartamento }}</option>
                  @endforeach

                </select>
              </div>
            </div><!-- /.form-group -->
          </div><!-- /.col -->


          <div class="box-footer col-xs-12 box-gris text-center ">
            <button  type="submit" onclick="return confirm('¿Desea guardar la información?')"  class="btn btn-primary">Enviar</button>
            <a onclick="cerrar_modal()" style="margin-left:15px;" class="btn btn-primary">Cancelar</a>
          </div>
        </form>

      </div>
    </div>
  </div>

</div>
</section>
