<section>

  <div class="row" >

  <div class="col-md-12">

    <div class="box box-primary  box-gris">

      <div class="box-header with-border my-box-header">
        <h3 class="box-title"><strong>Nuevo Grado</strong></h3>
      </div><!-- /.box-header -->

      <div class="box-body">

        <form   action="{{ url('crear_grado') }}"  method="post" id="f_crear_grado" class="formentrada"  >
          <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">

          <div class="col-md-12">
            <div class="form-group">
              <label class="col-sm-2" for="nombre">Código grado<span style="color:red;font-size:20px;">*</span> </label>
              <div class="col-sm-10" >
                <input type="number" class="form-control" id="id_grado" name="id_grado" required >
              </div>
            </div><!-- /.form-group -->
          </div><!-- /.col -->

          <div class="col-md-12">
            <div class="form-group">
              <label class="col-sm-2" for="apellido">Grado <span style="color:red;font-size:20px;">*</span> </label>
              <div class="col-sm-10" >
                <input type="text" class="form-control" id="grado" name="grado" required >
              </div>
            </div><!-- /.form-group -->
          </div><!-- /.col -->

          <div class="col-md-12">
            <div class="form-group">
              <label class="col-sm-2" for="apellido">Limite de edad<span style="color:red;font-size:20px;">*</span> </label>
              <div class="col-sm-10" >
                <input type="text" class="form-control" id="limiteedad" name="limiteedad" required >
              </div>
            </div><!-- /.form-group -->
          </div>

          <div class="col-md-12">
            <div class="form-group">
              <label class="col-sm-2" for="apellido">Id Nivel <span style="color:red;font-size:20px;">*</span> </label>
              <div class="col-sm-8" >

                <select  type="text" class="form-control" id="id_nivel" name="id_nivel" required >
                  <option>--- Seleccione un Nivel--- </option>
                  @foreach($nivels as $nivel)
                  <option value="{{ $nivel->IdNivel }}">{{ $nivel->IdNivel }}</option>
                  @endforeach

                </select>
              </div>
            </div><!-- /.form-group -->
          </div><!-- /.col -->

          <div class="col-md-12">
            <div class="form-group">
              <label class="col-sm-2" for="apellido">Id Nivel Media Total <span style="color:red;font-size:20px;">*</span> </label>
              <div class="col-sm-8" >

                <select  type="text" class="form-control" id="id_nivelmediatotal" name="id_nivelmediatotal" required >
                  <option>--- Seleccione un Nivel Media Total--- </option>
                  @foreach($nivelmediatotals as $nivelmediatotal)
                  <option value="{{ $nivelmediatotal->IdNivelMediaTotal }}">{{ $nivelmediatotal->IdNivelMediaTotal }}</option>
                  @endforeach

                </select>
              </div>
            </div><!-- /.form-group -->
          </div><!-- /.col -->

          <div class="col-md-12">
            <div class="form-group">
              <label class="col-sm-2" for="apellido">Id Nivel Cine <span style="color:red;font-size:20px;">*</span> </label>
              <div class="col-sm-8" >

                <select  type="text" class="form-control" id="id_nivelcine" name="id_nivelcine" required >
                  <option>--- Seleccione un Nivel Cine--- </option>
                  @foreach($nivelcines as $nivelcine)
                  <option value="{{ $nivelcine->IdNivelCine }}">{{ $nivelcine->IdNivelCine }}</option>
                  @endforeach

                </select>
              </div>
            </div><!-- /.form-group -->
          </div><!-- /.col -->

          <div class="box-footer col-xs-12 box-gris text-center ">
            <button  type="submit" onclick="return confirm('¿Desea guardar la información?')"  class="btn btn-primary">Enviar</button>
            <a onclick="cerrar_modal()" style="margin-left:15px;" class="btn btn-primary">Cancelar</a>
          </div>
        </form>

      </div>
    </div>
  </div>

</div>
</section>
