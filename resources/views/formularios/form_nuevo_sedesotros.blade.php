<section  >
<div class="col-md-12">

    <div class="box box-primary  box-gris">

            <div class="box-header with-border my-box-header">
                <h3 class="box-title"><strong>Espacios físicos de la sede educativa </strong></h3>
                <p>En este espacio se especificará la cantidad de cada uno de los datos que se le solicitan</p>
                <p style="color:red;">* Obligatorio</p>

            </div><!-- /.box-header -->


            <div class="box-body">

            <form   action="{{ url('crear_sedesespacio') }}"  method="post" id="f_crear_sedesespacio" class="formentrada"  >
				<input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">

        <div class="col-md-12">
            <div class="form-group">
                  <label class="col-sm-3" for="nombre">Dane del municipio <span style="color:red;font-size:20px;">*</span></label>
                  <div class="col-xs-3" >
                    <input type="number" class="form-control" id="danemunicipio" name="danemunicipio" required  >
                     </div>
            </div><!-- /.form-group -->
        </div><!-- /.col -->

        <div class="col-md-12">
            <div class="form-group">
                  <label class="col-sm-3" for="nombre">Dane del establecimiento <span style="color:red;font-size:20px;">*</span></label>
                  <div class="col-xs-3" >
                    <input type="number" class="form-control" id="daneestablecimiento" name="daneestablecimiento"  required>
                     </div>
            </div><!-- /.form-group -->
        </div><!-- /.col -->

                <div class="col-md-12">
	                <div class="form-group">
							<label class="col-sm-2" for="nombre">Dane de la sede <span style="color:red;font-size:20px;">*</span></label>
	                    <div class="col-xs-3">
							<input type="number" class="form-control" id="danesede" name="danesede" required >
	                    </div>
					</div><!-- /.form-group -->

			    </div><!-- /.col -->


          <div class="col-md-12">
            <div class="form-group">
        <label class="col-sm-2" for="nombre">Nombre de la sede <span style="color:red;font-size:20px;">*</span></label>
                <div class="col-xs-3">
        <input type="number" class="form-control" id="nombresede" name="nombresede" required >
                </div>
    </div><!-- /.form-group -->

    </div><!-- /.col -->


          <div class="col-md-12">
            <div class="form-group">
        <label class="col-sm-2" for="nombre">Cocinas de la sede <span style="color:red;font-size:20px;">*</span></</label>
                <div class="col-xs-3">
        <input type="number" class="form-control" id="cocinas" name="cocinas" required >

                </div>
    </div><!-- /.form-group -->

    </div><!-- /.col -->


    <div class="col-md-12">
      <div class="form-group">
  <label class="col-sm-2" for="nombre">Comedores de la sede <span style="color:red;font-size:20px;">*</span></label>
          <div class="col-xs-3">
  <input type="number" class="form-control" id="comedores" name="comedores" required >

          </div>
</div><!-- /.form-group -->

</div><!-- /.col -->

<div class="col-md-12">
  <div class="form-group">
<label class="col-sm-2" for="nombre">Viviendas de la sede <span style="color:red;font-size:20px;">*</span></label>
      <div class="col-xs-3">
<input type="number" class="form-control" id="vivienda" name="vivienda" required >

      </div>
</div><!-- /.form-group -->

</div><!-- /.col -->

<div class="col-md-12">
  <div class="form-group">
<label class="col-sm-2" for="nombre">Canchas de la sede <span style="color:red;font-size:20px;">*</span></label>
      <div class="col-xs-3">
<input type="number" class="form-control" id="canchas" name="canchas" required >

      </div>
</div><!-- /.form-group -->

</div><!-- /.col -->

<div class="col-md-12">
  <div class="form-group">
<label class="col-sm-2" for="nombre">Placas múltiples de la sede <span style="color:red;font-size:20px;">*</span></label>
      <div class="col-xs-3">
<input type="number" class="form-control" id="placasmulti" name="placasmulti" required >

      </div>
</div><!-- /.form-group -->

</div><!-- /.col -->

<div class="col-md-12">
  <div class="form-group">
<label class="col-sm-2" for="nombre">Parques de juegos infantiles de la sede educativa <span style="color:red;font-size:20px;">*</span></label>
      <div class="col-xs-3">
<input type="number" class="form-control" id="juegosinfantiles" name="juegosinfantiles" required >

      </div>
</div><!-- /.form-group -->

</div><!-- /.col -->



                <div class="box-footer col-xs-12 box-gris ">
                  <center>
                        <button type="submit" onclick="return confirm('¿Desea guardar la información?')"  class="btn btn-success">Crear</button>
                        <a href="/informacion" style="margin-left:10px;" class="btn btn-primary">Regresar</a>
                 </center>
                </div>
            </form>

          </div>
    </div>
</div>



</section>
