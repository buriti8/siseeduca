
<section class="content" >

  <div class="col-md-12">

    <div class="box box-primary  box-gris">

      <div class="box-header with-border my-box-header">
        <button onclick="cerrar_modal()" type="button" class="close" style="font-size: 40px; color:#34495E;"  aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button> <!-- Botón cerrar para volver a la vista sin necesidad de recargar la página -->
        <h3 class="box-title"><strong>Nuevo usuario</strong></h3>
        <p style="color:red;">* Obligatorio</p>
      </div><!-- /.box-header -->


      <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">


      <div class="box-body">

        <form   action="{{ url('crear_usuario') }}"  method="post" id="f_crear_usuario" class="formentrada" >
          <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">

          <div class="col-md-6">
            <div class="form-group">
              <label class="col-sm-2" for="nombre">Código <span style="color:red;font-size:20px;">*</span></label>
              <div class="col-sm-10" >
                <input type="text" class="form-control" id="nombres" name="nombres"  required placeholder='Dane del establecimiento, municipio o un nombre'   >
              </div>
            </div><!-- /.form-group -->



          </div><!-- /.col -->


          <div class="col-md-6">
            <div class="form-group">
              <label class="col-sm-2" for="apellido">Nombre <span style="color:red;font-size:20px;">*</span></label>
              <div class="col-sm-10" >
                <input type="text" class="form-control" id="apellidos" name="apellidos"  required placeholder='Nombre del establecimiento, municipio o un nombre'>
              </div>
            </div><!-- /.form-group -->

          </div><!-- /.col -->

          <div class="col-md-6">
            <div class="form-group">
              <label class="col-sm-2" for="celular">Telefono <span style="color:red;font-size:20px;">*</span></label>

              <div class="col-sm-10" >
                <input type="text" class="form-control" id="telefono" name="telefono" required  >
              </div>

            </div><!-- /.form-group -->

          </div><!-- /.col -->


          <div class="box-header with-border my-box-header col-md-12" style="margin-bottom:15px;margin-top: 15px;">
            <h3 class="box-title">Datos de acceso</h3>
          </div>


          <div class="col-md-6">
            <div class="form-group">
              <label class="col-sm-2" for="email">Usuario <span style="color:red;font-size:20px;">*</span></label>
              <div class="col-sm-10" >
                <input type="text" class="form-control" id="email" name="email"  required placeholder='Dane del establecimiento, municipio o un nombre'>
              </div>

            </div><!-- /.form-group -->

          </div><!-- /.col -->

          <div class="col-md-6">
            <div class="form-group">
              <label class="col-sm-2" for="email">Contraseña <span style="color:red;font-size:20px;">*</span></label>
              <div class="col-sm-10" >
                <input type="password" class="form-control" id="password" name="password"  required >
              </div>

            </div><!-- /.form-group -->

          </div><!-- /.col -->


          <div class="box-footer col-xs-12 box-gris ">
            <center style="margin-top:15px;">
              <button type="submit" class="btn btn-primary">Crear nuevo usuario</button>
              <a onclick="cerrar_modal()" style="margin-left:10px;" class="btn btn-primary">Cancelar</a>
            </center>
          </div>


        </form>

      </div>

    </div>
  </div>
</section>
