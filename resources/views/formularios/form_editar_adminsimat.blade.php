<section  >
  <div class="col-md-12">

    <div class="box box-primary  box-gris">

      <div class="box-header with-border my-box-header">
        <h3 class="box-title"><strong>Editar información del administrador(a) SIMAT del municipio {{$nombre}}</strong></h3>
        <button onclick="cerrar_modal()" type="button" class="close" style="font-size: 40px;"  aria-label="Close">
          <span  aria-hidden="true">&times;</span><!-- Botón cerrar para volver a la vista sin necesidad de recargar la página -->
        </button>
        <p style="color:red;">* Obligatorio</p>
      </div><!-- /.box-header -->

      <div class="box-body">

        <form   action="{{ url('editar_adminsimat') }}"  method="post" id="f_crear_sedesaulas" class="formentrada"  >

          <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
          <input type="hidden" name="informacion_id" value="{{$informacion->id}}">

          <div class="row" style="padding:20px;">

            <div class="col-md-12">
              <div class="form-group">
                <label class="col-sm-3" for="nombre"> Nombres <span style="color:red;font-size:20px;">*</span></label>
                <div class="col-xs-3" >
                  <input type="text" onkeyup="javascript:this.value=this.value.toUpperCase();" class="form-control" pattern="[a-zA-ZñÑáéíóúÁÉÍÓÚ\s]+" title="Solo ingrese texto" name="nombres" value="{{$informacion->nombre_adminsimat}}"  required>
                </div>
              </div><!-- /.form-group -->
            </center>
          </div><!-- /.col -->

          <div class="col-md-12">
            <div class="form-group">
              <label class="col-sm-3" for="nombre"> Apellidos <span style="color:red;font-size:20px;">*</span></label>
              <div class="col-xs-3" >
                <input type="text" onkeyup="javascript:this.value=this.value.toUpperCase();" class="form-control" pattern="[a-zA-ZñÑáéíóúÁÉÍÓÚ\s]+" title="Solo ingrese texto" name="apellidos" value="{{$informacion->apellido_adminsimat}}" required>
              </div>
            </div><!-- /.form-group -->
          </div><!-- /.col -->

          <div class="col-md-12">
            <div class="form-group">
              <label class="col-sm-3" for="nombre"> Celular <span style="color:red;font-size:20px;">*</span></label>
              <div class="col-xs-3" >
                <input type="number" placeholder="No ingrese signo más(+) ni menos(-)"  id="numero1" class="form-control"  name="celular" value="{{$informacion->celular_adminsimat}}"   required>
              </div>
            </div><!-- /.form-group -->
          </div><!-- /.col -->

          <div class="col-md-12">
            <div class="form-group">
              <label class="col-sm-3" for="nombre"> Teléfono <span style="color:red;font-size:20px;">*</span></label>
              <div class="col-xs-3" >
                <input type="text" class="form-control" name="telefono" value="{{$informacion->telefono_adminsimat}}" required>
              </div>
            </div><!-- /.form-group -->
          </div><!-- /.col -->

          <div class="col-md-12">
            <div class="form-group">
              <label class="col-sm-3" for="nombre"> Correo personal <span style="color:red;font-size:20px;">*</span></label>
              <div class="col-xs-3" >
                <input type="text"  pattern="[a-zA-ZàáâäãåąčćęèéêëėįìíîïłńòóôöõøùúûüųūÿýżźñçčšžÀÁÂÄÃÅĄĆČĖĘÈÉÊËÌÍÎÏĮŁŃÒÓÔÖÕØÙÚÛÜŲŪŸÝŻŹÑßÇŒÆČŠŽ∂ð ,.'-_]{2,64}"  class="form-control" name="correo_personal" value="{{$informacion->correo_adminsimat}}"  required>
              </div>
            </div><!-- /.form-group -->
          </div><!-- /.col -->


          <div class="col-md-12">
            <div class="form-group">
              <label class="col-sm-3" for="nombre"> Cargo <span style="color:red;font-size:20px;">*</span></label>
              <div class="col-xs-3" >
                <input type="text" onkeyup="javascript:this.value=this.value.toUpperCase();" class="form-control" pattern="[a-zA-ZñÑáéíóúÁÉÍÓÚ\s,]+" title="Solo ingrese texto" name="cargo" value="{{$informacion->cargo_adminsimat}}"  required>
              </div>
            </div><!-- /.form-group -->
          </div><!-- /.col -->

          <div class="col-md-12">
            <div class="form-group">
              <label class="col-sm-3" for="nombre"> Dirección oficina <span style="color:red;font-size:20px;">*</span></label>
              <div class="col-xs-3" >
                <input type="text" onkeyup="javascript:this.value=this.value.toUpperCase();"  class="form-control" name="direccion" value="{{$informacion->direccion_adminsimat}}"  required>
              </div>
            </div><!-- /.form-group -->
          </div><!-- /.col -->


          <div style="margin-top:15px;" class="box-footer col-xs-12 box-gris ">
            <center>
              <button  type="submit" onclick="return confirm('¿Desea guardar la información?')"  class="btn btn-primary">Enviar</button><!-- Botón para registrar la información de una nueva sede educativa -->
              <a onclick="cerrar_modal()" style="margin-left:15px;" class="btn btn-primary">Cancelar</a><!-- Botón para cancerlar el ingreso de una nueva sede educativa
                y volver a la vista sin necesidad de recargar la página -->
              </center>
            </div>
          </div><!-- /.form-group -->
        </div><!-- /.col -->


      </form>

      <script type="text/javascript">

      var input=  document.getElementById('numero1');
      input.addEventListener('input',function(){
        if (this.value.length > 10)
        this.value = this.value.slice(0,10);
      })

      </script>

    </div>
  </div>
</div>

</section>
