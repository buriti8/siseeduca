@extends('layouts.app')
@section('htmlheader_title')
Nuevo Dispositivo
@endsection


@section('main-content')
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>


<!-- Inicio contenido principal -->
<section  id="contenido_principal">
  <!-- Principal -->

  <div class="box box-primary">
    <div class="box-header">
    </div><!-- /.box-header -->


    <!-- Interior -->
    <div class="box box-primary" style="border: 1px solid #3c8dbc !important;" >



      <div class="titleboxes" style="font-size:20px;text-align:center;">Nuevo dispositivo</div>


      <form action="{{ url('crear_dispositivo') }}" method="post" id="frmdispositivo">
        <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">

        <div class="row" style="padding:20px;">
          <div class="col-md-12">
            <div class="titleboxes" style="font-size:15px; text-align:center;">
            </div>
            <br>

            @role('secretarios')
            @include('partialsroles.secretario')
            @else
            @role('rectores')
            @include('partialsroles.rector')
            @else
            @include('partialsroles.filtro')
            @endrole
            @endrole

          </div>

          <p style="color:red;">* Obligatorio</p>


          <!-- Inicio dispositivo -->
          <div class="col-md-4" style="background: white;">
            <div class="titleboxes" style="font-size:15px; text-align:center;">
            </div>
            <div style="border: 1px solid #3c8dbc !important; padding:20px; 20px;">
              <div class="form-group">
                <label>Tipo de dispositivo <span style="color:red;font-size:20px;">*</span></label>
                <select id="tipo_id" name="tipo_id" class="form-control" required>
                  @foreach($tipos as $tipo)
                  <option value="{{ $tipo->id }}">{{ $tipo->nombre }}</option>
                  @endforeach
                </select>
              </div>

              <div class="form-group">
                <label>Serial <span style="color:red;font-size:20px;">*</span></label>
                <input type="text" id="serial" name="serial" class="form-control" maxlength=100 required>
              </div>

              <div class="form-group">
                <label>Marca <span style="color:red;font-size:20px;">*</span></label>
                <input type="text" id="marca" name="marca" class="form-control" maxlength=100 required>
              </div>

              <div class="form-group">
                <label>Referencia</label>
                <input type="text" id="referencia" name="referencia" class="form-control" maxlength=100>
              </div>

              <div class="form-group">
                <label>Sistema operativo</label>
                <select id="sistema_operativo" name="sistema_operativo" class="form-control">
                  <option value="">No aplica</option>
                  <option value="1">Android</option>
                  <option value="2">Windows</option>
                  <option value="3">iOS</option>
                  <option value="4">MACOS</option>
                  <option value="5">Linux</option>
                </select>
              </div>

              <div class="form-group">
                <label>Procesador</label>
                <input type="text" id="procesador" name="procesador" class="form-control" maxlength=100>
              </div>

              <div class="form-group">
                <label>Memoria RAM(GB)</label>
                <select id="memoria_ram" name="memoria_ram" class="form-control">
                  <option value="">No aplica</option>
                  <option value="1">1</option>
                  <option value="2">2</option>
                  <option value="4">4</option>
                  <option value="6">6</option>
                  <option value="8">8</option>
                  <option value="16">16</option>
                  <option value="32">32</option>
                  <option value="64">64</option>
                </select>
              </div>

              <div class="form-group">
                <label>Disco duro(GB)</label>
                <input type="number" id="disco_duro" name="disco_duro" class="form-control" maxlength=100>
              </div>
            </div>
          </div>
          <!-- Fin dispositivo -->

          <!-- Inicio información principal -->
          <div class="col-md-4" style="background: white;">
            <div class="titleboxes" style="font-size:15px; text-align:center;">
            </div>
            <div style="border: 1px solid #3c8dbc !important; padding:20px; 20px;">
              <div class="form-group">
                <label>Entidad que entregó (FUENTE) <span style="color:red;font-size:20px;">*</span></label>
                <select id="origen_id" name="origen_id" class="form-control" required>
                  @foreach($origenes as $origen)
                  <option value="{{ $origen->id }}">{{ $origen->nombre }}</option>
                  @endforeach
                </select>
              </div>

              <div class="form-group">
                <label>Fecha de entrega <span style="color:red;font-size:20px;">*</span></label>
                <input type="date" id="fecha_entrega" name="fecha_entrega" class="form-control" step="1" min="1970-01-01" max="2040-12-31" maxlength=10 required>
              </div>

              <div class="form-group">
                <label>Fecha final de garantía</label>
                <input type="date" id="fecha_fin_garantia" name="fecha_fin_garantia" class="form-control" step="1" min="1970-01-01" max="2040-12-31" maxlength=10>
              </div>

              <div class="form-group">
                <label>Valor unitario del equipo</label>
                <input type="number" id="inversion" name="inversion" class="form-control" min=0 max=999999999>
              </div>

              <div class="form-group">
                <label>Número factura</label>
                <input type="number" id="numero_factura" name="numero_factura" class="form-control" min=0 max=999999999>
              </div>

              <div id="fueraservicio" style="border: 1px solid #3c8dbc !important; padding:20px; 20px; display:none;">
                <div class="form-check">
                  <label>
                    <input type="checkbox" id="cbfuncionamiento" name="cbfuncionamiento" ><span class="label-text">Fuera de servicio</span>
                  </label>
                </div>
                <div class="form-group">
                  <label>Motivo</label>
                  <input type="text" id="motivo" name="motivo" class="form-control" maxlength=200>
                </div>
                <div class="form-group">
                  <label>Nota para la evidencia</label>
                  <input type="text" id="evidencia" name="evidencia" class="form-control" maxlength=200>
                </div>
              </div>
            </div>
          </div>
          <!-- Fin información principal -->

          <!-- Inicio información principal 2 -->
          <div class="col-md-4" style="background: white;">
            <div class="titleboxes" style="font-size:15px; text-align:center;">
            </div>

            <div style="border: 1px solid #3c8dbc !important; padding:20px; 20px;">
              <div class="form-group">
                <label>Acta de entrega (Si aplica)</label>
                <input type="text" class="form-control" id="acta" name="acta" maxlength=50>
              </div>

              <div class="form-group">
                <label>Radicado (Si aplica)</label>
                <input type="text" class="form-control" id="radicado" name="radicado" maxlength=50>
              </div>

              <div class="form-group">
                <label>Proveedor</label>
                <input type="text" class="form-control" id="proveedor" name="proveedor" maxlength=50>
              </div>

              <div class="form-group">
                <label>Contrato</label>
                <select id="contrato_id" name="contrato_id" class="form-control">
                  <option value="0">No aplica</option>
                  @foreach($contratos as $contrato)
                  <option value="{{ $contrato->id }}">{{ $contrato->numero_contrato }}</option>
                  @endforeach
                </select>
              </div>

            </div>
          </div>
          <!-- Fin información principal 2 -->

          <div class="col-xs-12" style="margin-top:10px;" >
            <div class="form-group" >
              <label>Observaciones</label>
              <textarea rows="4" cols="50" type="text" id="observaciones" name="observaciones" class="form-control" maxlength=230 ></textarea>
            </div>
          </div>


          <div class="col-xs-12" style="margin-top:15px;">
            <center>
              <button  type="submit" onclick="return confirm('¿Desea guardar la información?')"  class="btn btn-primary">Enviar</button><!-- Botón para registrar la información de una nueva sede educativa -->
              <a href="{{ url('listado_dispositivos') }}" class="btn btn-primary">Regresar</a>
          </center>
          </div>
        </div>
      </form>
    </div>
    <!-- Fin interior -->
  </div>
  <!-- Principal -->


</section>
<!-- Fin contenido principal -->


@endsection
