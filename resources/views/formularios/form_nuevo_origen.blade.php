<section class="content" >
  <div class="col-md-12">
    <div class="box box-primary box-gris">

      <div class="box-header with-border my-box-header">

        <button onclick="cerrar_modal()" type="button" class="close" style="font-size: 40px; color:#34495E;"  aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button> <!-- Botón cerrar para volver a la vista sin necesidad de recargar la página -->


        <h3 class="box-title"><strong>Nueva fuente de dispositivo</strong></h3>
      </div><!-- /.box-header -->
      <p style="color:red;">* Obligatorio</p>

      <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">


      <!-- Body -->
      <div class="box-body">

        <form   action="{{ url('crear_origen') }}"  method="post" id="f_crear_origen" class="formentrada" >
          <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">

          <div class="col-md-12">

          </div><!-- /.col -->
          <div class="col-md-12">
            <div class="form-group">
              <label class="col-sm-2" for="nombre">Nombre <span style="color:red;font-size:20px;">*</span></label>
              <div class="col-sm-6" >
                <input type="text" pattern="[a-zA-ZñÑáéíóúÁÉÍÓÚ\s]+" title="Solo ingrese texto" class="form-control" id="nombre_origen" name="nombre_origen" maxlength=25 required>
              </div>
            </div><!-- /.form-group -->

          </div><!-- /.col -->

          <div class="col-md-12">
            <div class="form-group">
              <label class="col-sm-2" for="nombre">Descripción</label>
              <div class="col-sm-6" >
                <input type="text" class="form-control" id="descripcion_origen" name="descripcion_origen" maxlength=25>
              </div>
            </div><!-- /.form-group -->

          </div><!-- /.col -->

          <div class="box-footer col-xs-12 box-gris ">
            <button type="submit" class="btn btn-primary">Crear nueva fuente</button>
            <a onclick="cerrar_modal()" style="margin-left:10px;" class="btn btn-primary">Cancelar</a>
          </div>
        </form>
      </div>
      <!-- Fin body -->

    </div>
  </div>
</section>
