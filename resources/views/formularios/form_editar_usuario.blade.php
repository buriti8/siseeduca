
<section >
    <div class="col-md-12">

      <div class="box box-primary box-gris">
        <div class="box-header with-border my-box-header">
          <h3 class="box-title"><strong>Asignar rol</strong></h3>
          <button onclick="cerrar_modal()" type="button" class="close" style="font-size: 40px;"  aria-label="Close">
            <span  aria-hidden="true">&times;</span><!-- Botón cerrar para volver a la vista sin necesidad de recargar la página -->
          </button>

        </div><!-- /.box-header -->


        <div id="zona_etiquetas_roles" style="margin-left:40px;">
          Roles asignados:
          @foreach($usuario->getRoles() as $rl)
            <span class="label label-warning" style="margin-left:10px;">{{ $rl }} </span>
          @endforeach
        </div>

        <div class="box-body">

          <div class="col-md-12">
            <div class="form-group">
              <label style="margin-top:10px;" class="col-sm-2" for="tipo">Rol a asignar<span style="color:red;font-size:20px;">*</span> </label>
              <div class="col-sm-6" >
                <select style="margin-top:10px;" id="rol1" name="rol1" class="form-control">

                  @foreach($roles as $rol)
                    <option value="{{ $rol->id }}">{{ $rol->name }}</option>
                  @endforeach
                </select>
              </div>
              <div class="col-sm-4" >
                <button type="button" style="margin-top:15px;" class="btn btn-xs btn-primary" onclick="asignar_rol({{ $usuario->id }});" >Asignar rol</button>
              </div>
            </div>
          </div>
          <hr>

          <div class="col-md-12">
            <div class="form-group">
              <label style="margin-top:10px;" class="col-sm-2" for="tipo">Rol quitar<span style="color:red;font-size:20px;">*</span> </label>
              <div class="col-sm-6" >
                <select style="margin-top:10px;" id="rol2" name="rol2" class="form-control">
                  @foreach($roles as $rol)
                    <option value="{{ $rol->id }}">{{ $rol->name }}</option>
                  @endforeach
                </select>
              </div>

              <div class="col-sm-4" >
                <button type="button" style="margin-top:15px;" class="btn btn-xs btn-primary" onclick="quitar_rol({{ $usuario->id }});" >Quitar rol</button>
              </div>
            </div>

          </div>
        </div>

      </div> <!--box -->

      <div class="box box-primary box-gris">

        <div class="box-header with-border my-box-header">
          <h3 class="box-title"><strong>Editar informacion del usuario</strong></h3>
        </div><!-- /.box-header -->

        <div id="notificacion_E2" ></div>
        <div class="box-body">

          <form   action="{{ url('editar_usuario') }}"  method="post" id="f_editar_usuario"  class="formentrada"  >
            <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
            <input type="hidden" name="id_usuario" value="{{ $usuario->id }}">

            <div class="col-md-6">
              <div class="form-group">
                <label class="col-sm-2" for="nombre">Identificador<span style="color:red;font-size:20px;">*</span> </label>
                <div class="col-sm-10" >
                  <input type="text" class="form-control" id="nombres" name="nombres"  value="{{ $usuario->name }}"  required placeholder='Dane del establecimiento, municipio o un nombre'   >
                </div>
              </div><!-- /.form-group -->
            </div><!-- /.col -->

            <div class="col-md-6">
              <div class="form-group">
                <label class="col-sm-2" for="apellido">Nombres<span style="color:red;font-size:20px;">*</span> </label>
                <div class="col-sm-10" >
                  <input type="text" class="form-control" id="apellidos" name="apellidos"   value="{{ $usuario->lastname }}" required placeholder='Dane del establecimiento, municipio o un nombre'>
                </div>
              </div><!-- /.form-group -->
            </div><!-- /.col -->

            <div class="col-md-6">
              <div class="form-group">
                <label class="col-sm-2" for="celular">Telefono<span style="color:red;font-size:20px;">*</span> </label>

                <div class="col-sm-10" >
                  <input type="text" class="form-control" id="telefono" name="telefono"  value="{{ $usuario->phone  }}" required >
                </div>

              </div><!-- /.form-group -->
            </div><!-- /.col -->
            <div class="box-footer col-xs-12 box-gris ">
              <center>
              <button style="margin-bottom: 10px;" type="submit" class="btn btn-primary">Actualizar</button>
            </center>
            </div>
          </form>
        </div>

      </div>

        <div class="box box-primary box-gris">
        <div class="box-header with-border my-box-header">
          <h3 class="box-title"><strong>Acceso al sistema</strong></h3>
        </div><!-- /.box-header -->
        <div id="notificacion_E3" ></div>
        <div class="box-body">

          <form   action="{{ url('editar_acceso') }}"  method="post" id="f_editar_acceso"  class="formentrada"  >
            <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
            <input type="hidden" name="id_usuario" value="{{ $usuario->id }}">

            <div class="col-md-6">
              <div class="form-group">
                <label class="col-sm-2" for="email">Usuario<span style="color:red;font-size:20px;">*</span> </label>
                <div class="col-sm-10" >
                  <input type="text" class="form-control" id="email" name="email"  value="{{ $usuario->email  }}"  required >
                </div>

              </div><!-- /.form-group -->

            </div><!-- /.col -->

            <div class="col-md-6">
              <div class="form-group">
                <label class="col-sm-2" for="email">Nueva contraseña <span style="color:red;font-size:20px;">*</span> </label>
                <div class="col-sm-10" >
                  <input  type="password" class="form-control" id="password" name="password"  required >
                </div>
              </div><!-- /.form-group -->

            </div><!-- /.col -->
            <center>
              <button style="margin-top:25px;" type="submit" class="btn btn-primary">Actualizar Acceso</button>
              <a style="margin-top:25px; margin-left:12px;" onclick="cerrar_modal()" class="btn btn-primary">Cancelar</a>
            </center>
          </form>
        </div>
    </div>
    </div>
</section>
