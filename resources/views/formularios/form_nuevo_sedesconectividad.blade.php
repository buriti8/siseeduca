<section >

  <div class="col-md-12">

    <div class="box box-primary  box-gris">

      <div class="box-header with-border my-box-header">

        <button onclick="cerrar_modal()" type="button" class="close" style="font-size: 40px; color:#34495E;"  aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button> <!-- Botón cerrar para volver a la vista sin necesidad de recargar la página -->


        <h3 class="box-title"><strong>Nueva conectividad</strong></h3>
        <p>En este espacio se especificará ingresar conectividad de las sedes educativas.</p>
        <p style="color:red;">* Obligatorio</p>
        <p><strong>Nota:</strong> Recuerde no reportar varias veces el mismo servicio de conectividad.</p>

      </div><!-- /.box-header -->



      <div class="box-body">

        <form action="{{ url('crear_sedesconectividad') }}" method="post" id="f_crear_sedesconectividad" class="formentrada"  >
          <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">

          <div class="row" style="padding:20px;">
            <div class="col-md-12">
              <div class="titleboxes" style="font-size:15px; text-align:center;">
              </div>
              <br>

              @role('secretarios')
              @include('partialsroles.secretario')
              @else
              @role('rectores')
              @include('partialsroles.rector')
              @else
              @include('partialsroles.filtro')
              @endrole
              @endrole

            </div>


            <div class="col-md-12">
              <div class="form-group">
                <label class="col-sm-2" for="nombre">¿Quién paga la conectividad?<span style="color:red;font-size:20px;" title="No incluya los servicos que pague la gobernación.">*</span></label>
                <div class="col-xs-3">
                  <select class="form-control" id="programaorigen" name="programaorigen" required>
                    <option></option>
                    <option>Ministerio Tic</option>
                    <option>Municipio</option>
                    <option>Establecimiento educativo</option>
                  </select>
                </div>
              </div><!-- /.form-group -->
            </div><!-- /.col -->



            <div class="col-md-12">
              <div class="form-group">
                <label class="col-sm-2" for="nombre">Numero del contrato </label>
                <div class="col-xs-3">
                  <input type="number" class="form-control" class="form-control" id="numerocontrato" name="numerocontrato" >
                </div>
              </div><!-- /.form-group -->
            </div><!-- /.col -->

            <div class="col-md-12">
              <div class="form-group">
                <label class="col-sm-2" for="nombre">Operador prestador del servicio <span style="color:red;font-size:20px;">*</span></label>
                <div class="col-xs-3">
                  <input  type="text" pattern="[a-zA-ZñÑáéíóúÁÉÍÓÚ\s]+" title="Solo ingrese texto" class="form-control" id="operador" name="operador"  required >
                </div>
              </div><!-- /.form-group -->
            </div><!-- /.col -->

            <div class="col-md-12">
              <div class="form-group">
                <label class="col-sm-2" for="nombre">Ancho de banda de la conexión (MB) <span style="color:red;font-size:20px;">*</span></label>
                <div class="col-xs-3">
                  <input type="number" class="form-control" id="anchobanda" name="anchobanda" min="0" max="500" >
                </div>
              </div><!-- /.form-group -->
            </div><!-- /.col -->

            <div class="col-md-12">
              <div class="form-group">
                <label class="col-sm-2" for="nombre">Fecha en la que inicio el contrato <span style="color:red;font-size:20px;">*</span></label>
                <div class="col-xs-3">
                  <input type="date" class="form-control" id="fechainicio" name="fechainicio" step="1" min="1900-01-01" required max=<?php $hoy=date("Y-m-d"); echo $hoy;?>  >


                </div>
              </div><!-- /.form-group -->
            </div><!-- /.col -->


            <div class="col-md-12">
              <div class="form-group">
                <label class="col-sm-2" for="nombre">Fecha que finaliza el contrato </label>
                <div class="col-xs-3">
                  <input type="date" class="form-control" id="fechafin" name="fechafin" step="1" min="1900-01-01" max="2040-12-31" >
                </div>
              </div><!-- /.form-group -->
            </div><!-- /.col -->

            <div class="col-md-12">
              <div class="form-group">
                <label class="col-sm-2" for="nombre">Estado de la conexión <span style="color:red;font-size:20px;">*</span></label>
                <div class="col-xs-3" >
                  <select class="form-control" class id="estado" name="estado" required   >
                    <option></option>
                    <option>Activo</option>
                  </select>
                </div>
              </div><!-- /.form-group -->
            </div><!-- /.col -->

            <div class="col-md-12">
              <div class="form-group">
                <label class="col-sm-2" for="nombre">Tipo de servicio </label>
                <div class="col-xs-3" >
                  <select class="form-control" class id="servicio" name="servicio"   >
                    <option></option>
                    <option>ADSL</option>
                    <option>Fibra óptica</option>
                    <option>Fibra + coaxial</option>
                    <option>LMDS</option>
                    <option>PLC</option>
                    <option>WIMAX</option>
                    <option>GSM</option>
                    <option>GPRS</option>
                    <option>UMTS</option>
                    <option>HSDPA</option>
                    <option>Satélite</option>

                  </select>
                </div>
              </div><!-- /.form-group -->
            </div><!-- /.col -->

            <div class="col-md-12">
              <div class="form-group">
                <label class="col-sm-2" for="nombre">Uso principal del servicio<span style="color:red;font-size:20px;">*</span></label>
                <div class="col-xs-3" >
                  <select class="form-control" class id="uso" name="uso" required   >
                    <option></option>
                    <option>Administrativo</option>
                    <option>Educativo</option>


                  </select>
                </div>
              </div><!-- /.form-group -->
            </div><!-- /.col -->







            <div class="box-footer col-xs-12 box-gris ">
              <center>
                <button  type="submit" onclick="return confirm('¿Desea guardar la información?')"  class="btn btn-primary">Enviar</button><!-- Botón para registrar la información de una nueva sede educativa -->
               <a onclick="cerrar_modal()" style="margin-left:15px;" class="btn btn-primary">Cancelar</a><!-- Botón para cancerlar el ingreso de una nueva sede educativa
                 y volver a la vista sin necesidad de recargar la página -->
            </center>
            </div>
          </form>

        </div>
      </div>

    </div>


    <!-- Inicio dispositivo -->

    <!-- Principal -->


  </section>
  <!-- Fin contenido principal -->
