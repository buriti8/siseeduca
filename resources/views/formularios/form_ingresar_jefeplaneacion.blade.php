<section  >

  <form   action="{{ url('ingresar_jefeplaneacion') }}"  method="post" id="f_crear_sedesaulas" class="formentrada"  >

    <div class="col-md-12">

      <div class="box box-primary  box-gris">

        <div class="box-header with-border my-box-header">
          <h3 class="box-title"><strong>Ingresar información de jefe(a) de planeación</strong></h3>
          <button onclick="cerrar_modal()" type="button" class="close" style="font-size: 40px;"  aria-label="Close">
            <span  aria-hidden="true">&times;</span><!-- Botón cerrar para volver a la vista sin necesidad de recargar la página -->
          </button>
          <p style="color:red;">* Obligatorio</p>
        </div><!-- /.box-header -->

        @if(Auth::user()->isRole('Administrador_sistema'))
        <!--Filtros según el rol del usuario-->
        <div style="margin-left:45px;"  class="form-group col-xs-4"  >

          <div >
            <label  for="selMunicipio">Seleccione un municipio:  </label>
            <select  id="selMunicipio" name="selMunicipio" class="form-control  col-xs-6" required>
              <option selected="selected" value="">Elige uno...</option>
              @foreach ($municipios as $municipio)
                <option value="{{$municipio->CodigoDaneMunicipio}}">{{$municipio->NombreMunicipio}}</option>
              @endforeach
            </select>
          </div>
        </div>
        @endif

        <div class="box-body">

          <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
          <div class="row" style="padding:20px;">
            <div class="col-md-12">
              <br>
              <div class="col-xs-12" style="border-bottom:1px solid #337ab7; border-top:1px solid #337ab7; padding:0.8em; margin-top:0; margin-bottom:10px; text-align:center;">
                <strong>Ingresar información de jefe(a) de planeación</strong></div>

                <div class="col-md-12">
                  <div class="form-group">
                    <label class="col-sm-3" for="nombre"> Nombres <span style="color:red;font-size:20px;">*</span></label>
                    <div class="col-xs-3" >
                      <input type="text" class="form-control" pattern="[a-zA-ZñÑáéíóúÁÉÍÓÚ\s]+" title="Solo ingrese texto" name="nombres" required>
                    </div>
                  </div><!-- /.form-group -->
                </center>
              </div><!-- /.col -->

              <div class="col-md-12">
                <div class="form-group">
                  <label class="col-sm-3" for="nombre"> Apellidos <span style="color:red;font-size:20px;">*</span></label>
                  <div class="col-xs-3" >
                    <input type="text" class="form-control" pattern="[a-zA-ZñÑáéíóúÁÉÍÓÚ\s]+" title="Solo ingrese texto" name="apellidos" required>
                  </div>
                </div><!-- /.form-group -->
              </div><!-- /.col -->

              <div class="col-md-12">
                <div class="form-group">
                  <label class="col-sm-3" for="nombre"> Celular <span style="color:red;font-size:20px;">*</span></label>
                  <div class="col-xs-3" >
                    <input type="number" class="form-control" name="celular"  required>
                  </div>
                </div><!-- /.form-group -->
              </div><!-- /.col -->

              <div class="col-md-12">
                <div class="form-group">
                  <label class="col-sm-3" for="nombre"> Teléfono <span style="color:red;font-size:20px;">*</span></label>
                  <div class="col-xs-3" >
                    <input type="number" class="form-control" name="telefono" required>
                  </div>
                </div><!-- /.form-group -->
              </div><!-- /.col -->

              <div class="col-md-12">
                <div class="form-group">
                  <label class="col-sm-3" for="nombre"> Correo personal <span style="color:red;font-size:20px;">*</span></label>
                  <div class="col-xs-3" >
                    <input type="text" pattern="[a-zñA-ZÑ0-9_]+([.][a-zñA-ZÑ0-9_]+)*@[a-zñA-ZÑñ0-9_]+([.][a-zñA-ZÑ0-9_]+)*[.][a-zñA-ZÑ]{1,5}" class="form-control" name="correo_personal" required>
                  </div>
                </div><!-- /.form-group -->
              </div><!-- /.col -->

              <div class="col-md-12">
                <div class="form-group">
                  <label class="col-sm-3" for="nombre"> Cargo <span style="color:red;font-size:20px;">*</span></label>
                  <div class="col-xs-3" >
                    <input type="text" class="form-control" pattern="[a-zA-ZñÑáéíóúÁÉÍÓÚ\s]+" title="Solo ingrese texto" name="cargo" required>
                  </div>
                </div><!-- /.form-group -->
              </div><!-- /.col -->

              <div class="col-md-12">
                <div class="form-group">
                  <label class="col-sm-3" for="nombre"> Dirección <span style="color:red;font-size:20px;">*</span></label>
                  <div class="col-xs-3" >
                    <input type="text" class="form-control" name="direccion" required>
                  </div>
                </div><!-- /.form-group -->
              </div><!-- /.col -->


              <div style="margin-top:15px;" class="box-footer col-xs-12 box-gris ">
                <center>
                  <button  type="submit" onclick="return confirm('¿Desea guardar la información?')"  class="btn btn-primary">Enviar</button><!-- Botón para registrar la información de una nueva sede educativa -->
                  <a onclick="cerrar_modal()" style="margin-left:15px;" class="btn btn-primary">Cancelar</a><!-- Botón para cancerlar el ingreso de una nueva sede educativa
                    y volver a la vista sin necesidad de recargar la página -->
                  </center>
                </div>
              </div><!-- /.form-group -->
            </div><!-- /.col -->
          </div><!-- /.col -->

        </form>

      </div>
    </div>
  </div>

</section>
