<section  >

	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>


<script type="text/javascript">


$(document).on('change','input[type="file"]',function(){
	// this.files[0].size recupera el tamaño del archivo
	// alert(this.files[0].size);

	var fileName = this.files[0].name;
	var fileSize = this.files[0].size;

	if(fileSize > 3000000){
		alert('El archivo no debe superar los 3 megabytes (MB).');
		this.value = '';
		this.files[0].name = '';
	}else{
		// recuperamos la extensión del archivo
		var ext = fileName.split('.').pop();

		// console.log(ext);
		switch (ext) {
			case 'jpg':
			case 'jpeg':
			case 'png':
			case 'pdf': break;
			default:
				alert('El archivo no tiene la extensión adecuada.');
				this.value = ''; // reset del valor
				this.files[0].name = '';
		}
	}

});

</script>

  <form  action="{{ url('ingresar_documentos') }}" id="f_crear_sedesaulas"   method="post"  files="true"  enctype="multipart/form-data">

  <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">

		<div class="col-md-12">
      <div class="box box-primary  box-gris">
        <div class="box-header with-border my-box-header">
					@if(Auth::user()->isRole('secretarios') || Auth::user()->isRole('enlace_tic'))
					<h3 class="box-title"><strong>Documentación del municipios de {{$municipios}}</strong></h3>
					@else
						<h3 class="box-title"><strong>Documentación de los municipios de Antioquia</strong></h3>
					@endif
          <button onclick="cerrar_modal()" type="button" class="close" style="font-size: 40px;"  aria-label="Close">
            <span  aria-hidden="true">&times;</span><!-- Botón cerrar para volver a la vista sin necesidad de recargar la página -->
          </button>
          <p style="color:red;">* Obligatorio</p>
        </div><!-- /.box-header -->
        <div class="col-md-12" style="font-size:10px; text-align:center;">
        </div>
        <br>

				@if(Auth::user()->isRole('administrador_sistema') || Auth::user()->isRole('Administrador_infraestructura_educativa') || Auth::user()->isRole('Administrador_infraestructura_tecnologica') || Auth::user()->isRole('Administrador_matricula') || Auth::user()->isRole('Administrador_due') || Auth::user()->isRole('Administrador_pruebas_saber'))
	      <!--Filtros según el rol del usuario-->
	      <div style="margin-left:45px;"  class="form-group col-xs-4"  >

	        <div >
	          <label  for="selMunicipio">Seleccione un municipio:  </label>
	          <select  id="selMunicipio" name="selMunicipio" class="form-control  col-xs-6" required>
	            <option selected="selected" value="">Elige uno...</option>
	            @foreach ($municipios as $municipio)
	              <option value="{{$municipio->CodigoDaneMunicipio}}">{{$municipio->NombreMunicipio}}</option>
	            @endforeach
	          </select>
	        </div>
	      </div>
	      @endif


        <div class="box-body">

          <div class="col-xs-12" style="border-bottom:1px solid #337ab7; border-top:1px solid #337ab7; padding:0.8em; margin-top:20px; margin-bottom:10px; text-align:center;">
            <strong>Ingreso de información</strong></div>

          <div class="row" style="padding:20px;">
            <div style="margin-top:20px;"  class="col-md-12">
              <div class="form-group">
                <label class="col-sm-3">Documento <span style="color:red;font-size:20px;">*</span></label>
                <div class="col-xs-3">
                  <select id="selcomite" name="selcomite" class="form-control  col-xs-6" required>
										@if(Auth::user()->isRole('enlace_tic'))
											<option value="">Elige una opción...</option>
											<option value="Enlace TIC">Enlace TIC</option>
	                    <option value="Comité TIC">Comité TIC</option>
										@else
											<option value="">Elige una opción...</option>
											@foreach ($datos as $dato)
												<option value="{{$dato->codigo}}">{{$dato->documento}}</option>
											@endforeach
										@endif
                  </select>
                </div>
              </div><!-- /.form-group -->
            </div><!-- /.col -->

						<div class="col-md-12">
              <div class="form-group">
                <label class="col-sm-3" for="nombre">Número de acta, decreto o carta <span style="color:red;font-size:20px;">*</span></label>
                <div class="col-xs-3">
                  <input type="text" class="form-control" id="numero_documento" name="numero_documento" step="1" min="1900-01-01" max="2019-12-31" required>
                </div>
              </div><!-- /.form-group -->
            </div><!-- /.col -->

						<div class="col-md-12">
              <div class="form-group">
                <label class="col-sm-3" for="nombre">Fecha de acta, decreto o carta <span style="color:red;font-size:20px;">*</span></label>
                <div class="col-xs-3">
                  <input type="date" class="form-control" id="fecha_documento" name="fecha_documento" step="1" min="1900-01-01" max="2019-12-31" required>
                </div>
              </div><!-- /.form-group -->
            </div><!-- /.col -->


              <div class="col-md-12">
                <div class="form-group">
                  <label class="col-sm-3">Agregar archivo  <span class="label label-primary">PDF</span>,&nbsp;&nbsp;el archivo no debe superar los 3 megabytes:</label>
                  <div class="col-xs-3" >
                    <input name="file" id="file" type="file" files="true" enctype="”multipart/form-data”" accept="application/pdf,image/webp,image/apng,image/*,*/*;q=0.8" class="archivo form-control" required>
                  </div>
                </div><!-- /.form-group -->
              </div><!-- /.col -->


              <div class="box-footer col-xs-12 box-gris ">
                <center>
                  <button  type="submit" onclick="return confirm('¿Desea guardar la información?')"  class="btn btn-primary">Enviar</button><!-- Botón para registrar la información -->
                  <a onclick="cerrar_modal()" style="margin-left:15px;" class="btn btn-primary">Cancelar</a><!-- Botón para cancerlar el ingreso de una nueva sede educativa
                    y volver a la vista sin necesidad de recargar la página -->
                  </center>
                </div>

              </div>
            </div>
          </form>
        </section>
