@extends('layouts.app')

@section('htmlheader_title')
Editar Dispositivo
@endsection

@section('main-content')
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>

<!-- Inicio contenido principal -->
<section  id="contenido_principal">
    <!-- Principal -->
    <div class="box box-primary">
        <div class="box-header">
        </div><!-- /.box-header -->

        <!-- Interior -->
        <div class="box box-primary" style="border: 1px solid #3c8dbc !important;" >
           <div class="titleboxes" style="font-size:20px;text-align:center;">Editar dispositivo</div>




            <form action="{{ url('editar_dispositivo', $dispositivo->id) }}" method="post" id="frmdispositivo" enctype="multipart/form-data">
                <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">

                <div class="row" style="padding:20px;">
                    <div class="col-md-12">
                        <div class="titleboxes" style="font-size:15px; text-align:center;">
                        </div>
                            <div class="form-group">
                                <label class="col-md-3">ID DISPOSITIVO</label>
                                <div class="col-md-9">
                                    <label><strong>{{ $dispositivo->id }}</strong></label>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3">SEDE</label>
                                <div class="col-md-9">
                                    <label><strong>{{ $dispositivo->sede_id }} - {{ $nombre_sede }} - {{ $direccion }}</strong></label>
                                </div>
                            </div>
                    </div>

                    <br>

                    <div class="col-md-12">
                      <p style="color:red;"> Obligatorio *</p>
                    </div>

                    <!-- Inicio dispositivo -->
                    <div class="col-md-4" style="background: white;">
                        <div class="titleboxes" style="font-size:15px; text-align:center;">
                        </div>
                        <div style="border: 1px solid #3c8dbc !important; padding:20px; 20px;">
                            <div class="form-group">
                                <label>Tipo de dispositivo <span style="color:red;font-size:20px;">*</span></label>
                                <select id="tipo_id" name="tipo_id" class="form-control">
                                    @foreach($tipos as $tipo)
                                        <option value="{{ $tipo->id }}" @if($dispositivo->tipo_id == $tipo->id) selected @endif >{{ $tipo->nombre }}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="form-group">
                                <label>Serial <span style="color:red;font-size:20px;">*</span></label>
                                <input type="text" id="serial" name="serial" class="form-control" value="{{ $dispositivo->serial }}" maxlength=100>
                            </div>

                            <div class="form-group">
                                <label>Marca</label>
                                <input type="text" id="marca" name="marca" value="{{ $dispositivo->marca }}" class="form-control" maxlength=100>
                            </div>

                            <div class="form-group">
                                <label>Referencia</label>
                                <input type="text" id="referencia" name="referencia" value="{{ $dispositivo->referencia }}" class="form-control" maxlength=100>
                            </div>

                            <div class="form-group">
                                <label>Sistema operativo</label>
                                <select id="sistema_operativo" name="sistema_operativo" class="form-control">
                                    @if($dispositivo->sistema_operativo == 1)
                                            <option value="">No aplica</option>
                                            <option value="1" selected>Android</option>
                                            <option value="2">Windows</option>
                                            <option value="3">IOS</option>
                                            <option value="4">MACOS</option>
                                            <option value="5">Linux</option>
                                    @elseif($dispositivo->sistema_operativo == 2)
                                            <option value="">No aplica</option>
                                            <option value="1">Android</option>
                                            <option value="2" selected>Windows</option>
                                            <option value="3">IOS</option>
                                            <option value="4">MACOS</option>
                                            <option value="5">Linux</option>
                                    @elseif($dispositivo->sistema_operativo == 3)
                                            <option value="">No aplica</option>
                                            <option value="1">Android</option>
                                            <option value="2">Windows</option>
                                            <option value="3" selected>IOS</option>
                                            <option value="4">MACOS</option>
                                            <option value="5">Linux</option>
                                    @elseif($dispositivo->sistema_operativo == 4)
                                            <option value="">No aplica</option>
                                            <option value="1">Android</option>
                                            <option value="2">Windows</option>
                                            <option value="3">IOS</option>
                                            <option value="4" selected>MACOS</option>
                                            <option value="5">Linux</option>
                                    @elseif($dispositivo->sistema_operativo == 5)
                                            <option value="">No aplica</option>
                                            <option value="1">Android</option>
                                            <option value="2">Windows</option>
                                            <option value="3">IOS</option>
                                            <option value="4">MACOS</option>
                                            <option value="5" selected>Linux</option>
                                    @else
                                            <option value="" selected>No aplica</option>
                                            <option value="1">Android</option>
                                            <option value="2">Windows</option>
                                            <option value="3">IOS</option>
                                            <option value="4">MACOS</option>
                                            <option value="5">Linux</option>
                                    @endif
                                    </select>
                            </div>

                            <div class="form-group">
                                <label>Procesador</label>
                                <input type="text" id="procesador" name="procesador" value="{{ $dispositivo->procesador }}" class="form-control" maxlength=100>
                            </div>

                            <div class="form-group">
                                <label>Memoria RAM(GB)</label>
                                <select id="memoria_ram" name="memoria_ram" class="form-control">
                                    <option value="" @if($dispositivo->memoria_ram == "") selected @endif>NO APLICA</option>
                                    <option value="1" @if($dispositivo->memoria_ram == 1) selected @endif>1</option>
                                    <option value="2" @if($dispositivo->memoria_ram == 2) selected @endif>2</option>
                                    <option value="4" @if($dispositivo->memoria_ram == 4) selected @endif>4</option>
                                    <option value="6" @if($dispositivo->memoria_ram == 6) selected @endif>6</option>
                                    <option value="8" @if($dispositivo->memoria_ram == 8) selected @endif>8</option>
                                    <option value="16" @if($dispositivo->memoria_ram == 16) selected @endif>16</option>
                                    <option value="32" @if($dispositivo->memoria_ram == 32) selected @endif>32</option>
                                    <option value="64" @if($dispositivo->memoria_ram == 64) selected @endif>64</option>
                                </select>

                            </div>

                            <div class="form-group">
                                <label>Disco duro(GB)</label>
                                <input type="number" id="disco_duro" name="disco_duro" value="{{ $dispositivo->disco_duro }}" class="form-control" maxlength=100>
                            </div>
                        </div>
                    </div>
                    <!-- Fin dispositivo -->

                    <!-- Inicio información principal -->
                    <div class="col-md-4" style="background: white;">
                    <div class="titleboxes" style="font-size:15px; text-align:center;">
                    </div>
                        <div style="border: 1px solid #3c8dbc !important; padding:20px; 20px;">
                                <div class="form-group">
                                    <label>Entidad que entregó (FUENTE) <span style="color:red;font-size:20px;">*</span></label>
                                    <select id="origen_id" name="origen_id" class="form-control">
                                        @foreach($origenes as $origen)
                                            <option value="{{ $origen->id }}" @if($dispositivo->origen_id == $origen->id) selected @endif >{{ $origen->nombre }}</option>
                                        @endforeach
                                    </select>
                                </div>

                                <div class="form-group">
                                    <label>Fecha de netrega</label>
                                    <input type="date" id="fecha_entrega" name="fecha_entrega" class="form-control" step="1" min="1970-01-01" max="2020-12-31" value="{{ $dispositivo->fecha_entrega }}" maxlength=10 required>
                                </div>

                                <div class="form-group">
                                    <label>Fecha final de garantía</label>
                                    <input type="date" id="fecha_fin_garantia" name="fecha_fin_garantia" class="form-control" step="1" min="1970-01-01" max="2020-12-31" value="{{ $dispositivo->fecha_fin_garantia }}" maxlength=10>
                                </div>

                                <div class="form-group">
                                    <label>Valor unitario de equipo</label>
                                    <input type="number" id="inversion" name="inversion" value="{{ $dispositivo->inversion }}" class="form-control" min=0 max=999999999>
                                </div>

                                <div class="form-group">
                                    <label>Número de factura</label>
                                    <input type="number" id="numero_factura" name="numero_factura" value="{{ $dispositivo->numero_factura }}" class="form-control" min=0 max=999999999>
                                </div>

                            <div class="form-group">
                                <label>Acta de Entrega (Si aplica)</label>
                                <input type="text" class="form-control" id="acta" name="acta" value="{{ $dispositivo->acta }}" maxlength=50>
                            </div>

                            <div class="form-group">
                                <label>Radicado (Si aplica)</label>
                                <input type="text" class="form-control" id="radicado" name="radicado" value="{{ $dispositivo->radicado }}" maxlength=50>
                            </div>

                            <div class="form-group">
                                <label>Proveedor</label>
                                <input type="text" class="form-control" id="proveedor" name="proveedor" value="{{ $dispositivo->proveedor }}" maxlength=50>
                            </div>

                        </div>
                    </div>
                    <!-- Fin información principal -->

                    <!-- Inicio información principal 2 -->
                    <div class="col-md-4" style="background: white;">
                        <div class="titleboxes" style="font-size:15px; text-align:center;">
                        </div>

                        <div style="border: 1px solid #3c8dbc !important; padding:20px; 20px;">
                            <div class="form-group">
                                <label>Contrato</label>
                                <select id="contrato_id" name="contrato_id" class="form-control">
                                        <option value="0">No aplica</option>
                                        @foreach($contratos as $contrato)
                                            <option value="{{ $contrato->id }}" @if($dispositivo->contrato_id == $contrato->id) selected @endif>{{ $contrato->numero_contrato }}</option>
                                        @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="titleboxes" style="font-size:15px; text-align:center; margin-top:10px;">
                        </div>
                        <div style="border: 1px solid #3c8dbc !important; padding:20px; 20px;">
                            <div>
                                <label>Estado</label>
                                <div class="form-check">
                                    <label>
                                            <select id="cbfuncionamiento" name="cbfuncionamiento" class="form-control">
                                                @switch($dispositivo->estado)
                                                    @case(0)
                                                        <option value="0" selected>En operación</option>
                                                        <option value="1">No disponible</option>
                                                        @break
                                                    @case(1)
                                                        <option value="0">En operación</option>
                                                        <option value="1" selected>No disponible</option>
                                                        @break
                                                    @default
                                                        <option value="0" selected>En operación</option>
                                                        <option value="1">No disponible</option>
                                                        @break
                                                @endswitch
                                            </select>
                                    </label>
                                </div>
                            </div>
                            <div id="div_estado" style="width: 100%; display:none;">
                                <div class="form-group">
                                    <select id="motivo" name="motivo" class="form-control">
                                            @switch($dispositivo->motivo)
                                                @case(1)
                                                    <option value="1" selected>Robado</option>
                                                    <option value="2">Dañado</option>
                                                    <option value="3">Obsoleto</option>
                                                    @break
                                                @case(2)
                                                    <option value="1">Robado</option>
                                                    <option value="2" selected>Dañado</option>
                                                    <option value="3">Obsoleto</option>
                                                    @break
                                                @default
                                                    <option value="1">Robado</option>
                                                    <option value="2">Dañado</option>
                                                    <option value="3" selected>Obsoleto</option>
                                                    @break
                                            @endswitch
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Adjunte la evidencia <span style="color:red;font-size:20px;">*</span></label>
                                    <input type="file" id="evidencia" name="evidencia" value="{{ $dispositivo->evidencia }}" maxlength=200>

                                </div>
                                <div class="form-group">
                                @if ($dispositivo->evidencia != "")
                                    <div>
                                        <!-- <div class="form-group">                                     -->
                                            <a href="/down{{$dispositivo->evidencia}}" target="_blank" id="descargar_evidencia" name="descargar_evidencia" class="btn btn-danger pull-left glyphicon glyphicon-download-alt " style="margin-right:50px;"> DESCARGAR EVIDENCIA</a>
                                        <!-- </div>                                                          -->
                                    </div>
                                @else
                                    <div>
                                        <label>No hay archivo adjunto</label>
                                    </div>
                                @endif
                                </div>

                            </div>
                            <!-- Fin estado  -->
                        </div>
                    </div>
                    <!-- Fin información principal 2 -->

                    <div class="col-xs-12" style="margin-top:10px;" >
                        <div class="form-group" >
                            <label>Observaciones</label>
                            <textarea rows="4" cols="50" type="text" id="observaciones" name="observaciones" class="form-control" maxlength=230 >{{ $dispositivo->observaciones }}</textarea>
                        </div>
                    </div>

                    <div class="col-xs-12" style="margin-top:15px;">
                      <center>
                        <button type="submit" class="btn btn-primary">Enviar</button>
                        <a href="{{ url('listado_dispositivos') }}" class="btn btn-primary">Regresar</a>
                      </center>
                    </div>
                </div>
            </form>
        </div>
        <!-- Fin interior -->
    </div>
     <!-- Principal -->


</section>
<!-- Fin contenido principal -->

@endsection
