<section  >
<div class="col-md-12">

    <div class="box box-primary  box-gris">

            <div class="box-header with-border my-box-header">
                <h3 class="box-title"><strong>Espacios físicos de la sede educativa</strong></h3>
                <p>En este espacio se especificará la cantidad de cada uno de los datos que le solicitan.</p>

            </div><!-- /.box-header -->

            <div class="box-body">

            <form   action="{{ url('f_crear_sedesespacio') }}"  method="post" id="f_crear_sedesbaterias" class="formentrada"  >
				<input type="hidden" name="_token" >

        <div class="col-md-12">
            <div class="form-group">
                  <label class="col-sm-3" for="nombre">DANE del municipio <span style="color:red;font-size:20px;">*</span> </label>
                  <div class="col-xs-3" >
                    <input type="number" class="form-control" id="danemunicipio" name="danemunicipio" required  >
                     </div>
            </div><!-- /.form-group -->
        </div><!-- /.col -->

        <div class="col-md-12">
            <div class="form-group">
                  <label class="col-sm-3" for="nombre">DANE del establecimiento <span style="color:red;font-size:20px;">*</span> </label>
                  <div class="col-xs-3" >
                    <input type="number" class="form-control" id="daneestablecimiento" name="daneestablecimiento"  required>
                     </div>
            </div><!-- /.form-group -->
        </div><!-- /.col -->


          <div class="col-md-12">
              <div class="form-group">
                    <label class="col-sm-3" for="nombre">DANE de la sede <span style="color:red;font-size:20px;">*</span> </label>
                    <div class="col-xs-3" >
                      <input type="number" class="form-control" id="danesede" name="danesede"  required>
                       </div>
              </div><!-- /.form-group -->
          </div><!-- /.col -->

          <div class="col-md-12">
              <div class="form-group">
                    <label class="col-sm-3" for="nombre">NOMBRE de la sede <span style="color:red;font-size:20px;">*</span> </label>
                    <div class="col-xs-3" >
                      <input type="text" class="form-control" id="nombresede" name="nombresede"  required  >
                       </div>
              </div><!-- /.form-group -->
          </div><!-- /.col -->

    <div class="col-md-12">
        <div class="form-group">
              <label class="col-sm-3" for="nombre"> Sanitarios para hombres de la sede <span style="color:red;font-size:20px;">*</span> </label>
              <div class="col-xs-3" >
                <input type="number" class="form-control" id="santirioshombres" name="santirioshombres"  required   >
                 </div>
        </div><!-- /.form-group -->
    </div><!-- /.col -->

    <div class="col-md-12">
        <div class="form-group">
              <label class="col-sm-3" for="nombre"> Sanitarios para mujeres de la sede <span style="color:red;font-size:20px;">*</span> </label>
              <div class="col-xs-3" >
                <input type="number" class="form-control" id="sanitariosmujeres" name="sanitariosmujeres"   required   >
                 </div>
        </div><!-- /.form-group -->
    </div><!-- /.col -->

    <div class="col-md-12">
        <div class="form-group">
              <label class="col-sm-3" for="nombre"> Lavamanos para hombres de la sede <span style="color:red;font-size:20px;">*</span> </label>
              <div class="col-xs-3" >
                <input type="number" class="form-control" id="lavamanoshombres" name="lavamanoshombres"  required   >
                 </div>
        </div><!-- /.form-group -->
    </div><!-- /.col -->

    <div class="col-md-12">
        <div class="form-group">
              <label class="col-sm-3" for="nombre"> Lavamanos para mujer de la sede <span style="color:red;font-size:20px;">*</span> </label>
              <div class="col-xs-3" >
                <input type="number" class="form-control" id="lavamanosmujeres" name="lavamanosmujeres"  required   >
                 </div>
        </div><!-- /.form-group -->
    </div><!-- /.col -->

    <div class="col-md-12">
        <div class="form-group">
              <label class="col-sm-3" for="nombre"> Orinales de la sede <span style="color:red;font-size:20px;">*</span> </label>
              <div class="col-xs-3" >
                <input type="number" class="form-control" id="orinales" name="orinales"   required  >
                 </div>
        </div><!-- /.form-group -->
    </div><!-- /.col -->



                <div class="box-footer col-xs-12 box-gris ">
                  <center>
                        <button type="submit" onclick="return confirm('¿Desea guardar la información?')"  class="btn btn-success">Enviar</button>
                        <a href="/informacion" style="margin-left:10px;" class="btn btn-primary">Regresar</a>
                 </center>
                </div>
            </form>

          </div>
    </div>
</div>



</section>
