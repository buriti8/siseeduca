<section class="content" >
  <div class="col-md-12">
    <div class="box box-primary box-gris">
      <div class="box-header with-border my-box-header">
        <button onclick="cerrar_modal()" type="button" class="close" style="font-size: 40px; color:#34495E;"  aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button> <!-- Botón cerrar para volver a la vista sin necesidad de recargar la página -->
        <h3 class="box-title"><strong>Nuevo proyecto</strong></h3>
        <p style="color:red;">* Obligatorio</p>
      </div><!-- /.box-header -->


      <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">


      <!-- Body -->
      <div class="box-body">

        <form   action="{{ url('crear_proyecto') }}"  method="post" id="f_crear_proyecto" class="formentrada" >
          <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">

          <div class="col-md-12">

          </div><!-- /.col -->

          <div class="col-md-12">
            <div class="form-group">
              <label class="col-sm-2" for="nombre">Nombre <span style="color:red;font-size:20px;">*</span></label>
              <div class="col-sm-6" >
                <input type="text" pattern="[a-zA-ZñÑáéíóúÁÉÍÓÚ\s]+" title="Solo ingrese texto" class="form-control" id="nombre_proyecto" name="nombre_proyecto" maxlength=100 required>
              </div>
            </div><!-- /.form-group -->
          </div><!-- /.col -->

          <div class="col-md-12">
            <div class="form-group">
              <label class="col-sm-2" for="plan_desarrollo">Plan de desarrollo <span style="color:red;font-size:20px;">*</span></label>
              <div class="col-sm-6" >
                <input type="text" pattern="[a-zA-ZñÑáéíóúÁÉÍÓÚ\s]+" title="Solo ingrese texto" class="form-control" id="plan_desarrollo" name="plan_desarrollo" maxlength=100 required>
              </div>
            </div><!-- /.form-group -->
          </div><!-- /.col -->

          <div class="col-md-12">
            <div class="form-group">
              <label class="col-sm-2" for="periodo">Inicio del periodo <span style="color:red;font-size:20px;">*</span></label>
              <div class="col-sm-6" >
                <input type="number" class="form-control" id="periodo" name="periodo" min="1900" max="9999" required style="width:15%" placeholder="####">
              </div>
            </div><!-- /.form-group -->
          </div><!-- /.col -->

          <div class="col-md-12">
            <div class="form-group">
              <label class="col-sm-2" for="periodo">Fin del periodo <span style="color:red;font-size:20px;">*</span></label>
              <div class="col-sm-6" >
                <input type="number" class="form-control" id="periodofinalizacion" name="periodofinalizacion" min="1900" max="9999" required style="width:15%" placeholder="####">
              </div>
            </div>
          </div>

          <div class="box-footer col-xs-12 box-gris ">
            <button type="submit" class="btn btn-primary">Crear nuevo proyecto</button>
            <a onclick="cerrar_modal()" style="margin-left:10px;" class="btn btn-primary">Cancelar</a>
          </div>
        </form>
      </div>
      <!-- Fin body -->

    </div>
  </div>
</section>
