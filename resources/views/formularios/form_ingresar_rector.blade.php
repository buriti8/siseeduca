
<section  >

<script type="text/javascript">

function cerrar_modal() {

//función encargada de cerrar el modal o ventanas emergentes de los módulos o súbmodulos.

  var x = document.getElementById('capa_modal');
  if (x.style.display === 'block') {
    x.style.display = 'none';
  } else {
    x.style.display = 'block';
  }

  var z = document.getElementById('capa_formularios');
  if (z.style.display === 'block') {
    z.style.display = 'none';
  } else {
    z.style.display = 'block';
  }

}

</script>


  <form  action="{{ url('ingresar_rectores') }}" id="f_ingresar_alcalde"   method="POST"  class="formentrada">
    <div class="col-md-12">
      <div class="box box-primary  box-gris">
        <div class="box-header with-border my-box-header">
          <h3 class="box-title"><strong>Ingresar información de rectores(a).</strong></h3>
          <button onclick="cerrar_modal()" type="button" class="close" style="font-size: 40px;"  aria-label="Close">
            <span  aria-hidden="true">&times;</span><!-- Botón cerrar para volver a la vista sin necesidad de recargar la página -->
          </button>
          <p style="color:red;">* Obligatorio</p>
        </div><!-- /.box-header -->
        <div class="col-md-12" style="font-size:10px; text-align:center;">
        </div>
        <br>

        <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
        @foreach ($informacion as $informacion)
          <input type="hidden" id="establecimientos" name="establecimientos" value="{{$informacion->codigo_establecimiento}}">
        @endforeach
        <div class="box-body">
          <div class="col-xs-12" style="border-bottom:1px solid #337ab7; border-top:1px solid #337ab7; padding:0.8em; margin-top:0; margin-bottom:10px; text-align:center;">
            <strong>Ingresar información de rector(a)</strong></div>

          <div class="row" style="padding:20px;">
            <div class="col-md-12">
              <div style="margin-top:20px;" class="col-md-12">
                <div class="form-group">
                  <label class="col-sm-3" for="nombre"> Nombres <span style="color:red;font-size:20px;">*</span></label>
                  <div class="col-xs-3" >
                    <input type="text"  onkeyup="javascript:this.value=this.value.toUpperCase();"  pattern="[a-zA-ZñÑáéíóúÁÉÍÓÚ\s]+" title="Solo ingrese texto" class="form-control" name="nombres" required>
                  </div>
                </div><!-- /.form-group -->
              </div><!-- /.col -->

              <div class="col-md-12">
                <div class="form-group">
                  <label class="col-sm-3" for="nombre"> Apellidos <span style="color:red;font-size:20px;">*</span></label>
                  <div class="col-xs-3" >
                    <input type="text"  onkeyup="javascript:this.value=this.value.toUpperCase();"  pattern="[a-zA-ZñÑáéíóúÁÉÍÓÚ\s]+" title="Solo ingrese texto" class="form-control" name="apellidos" required>
                  </div>
                </div><!-- /.form-group -->
              </div><!-- /.col -->

              <div class="col-md-12">
                <div class="form-group">
                  <label class="col-sm-3" for="nombre"> Celular <span style="color:red;font-size:20px;">*</span></label>
                  <div class="col-xs-3" >
                    <input type="number" placeholder="No ingrese signo más(+) ni menos(-)"  id="numero" class="form-control" name="celular" required>
                  </div>
                </div><!-- /.form-group -->
              </div><!-- /.col -->

              <div class="col-md-12">
                <div class="form-group">
                  <label class="col-sm-3" for="nombre"> Teléfono <span style="color:red;font-size:20px;">*</span></label>
                  <div class="col-xs-3" >
                    <input type="text" class="form-control" name="telefono" required>
                  </div>
                </div><!-- /.form-group -->
              </div><!-- /.col -->

              <div class="col-md-12">
                <div class="form-group">
                  <label class="col-sm-3" for="nombre"> Correo personal <span style="color:red;font-size:20px;">*</span></label>
                  <div class="col-xs-3" >
                    <input type="text" class="form-control" pattern="[a-zA-ZàáâäãåąčćęèéêëėįìíîïłńòóôöõøùúûüųūÿýżźñçčšžÀÁÂÄÃÅĄĆČĖĘÈÉÊËÌÍÎÏĮŁŃÒÓÔÖÕØÙÚÛÜŲŪŸÝŻŹÑßÇŒÆČŠŽ∂ð ,.'-_]{2,64}"  name="correo_personal" required>
                  </div>
                </div><!-- /.form-group -->
              </div><!-- /.col -->

              <div class="col-md-12">
                <div class="form-group">
                  <label class="col-sm-3" for="nombre"> Correo establecimiento <span style="color:red;font-size:20px;">*</span></label>
                  <div class="col-xs-3" >
                    <input type="text" class="form-control" pattern="[a-zA-ZàáâäãåąčćęèéêëėįìíîïłńòóôöõøùúûüųūÿýżźñçčšžÀÁÂÄÃÅĄĆČĖĘÈÉÊËÌÍÎÏĮŁŃÒÓÔÖÕØÙÚÛÜŲŪŸÝŻŹÑßÇŒÆČŠŽ∂ð ,.'-_]{2,64}"  name="correo_establecimiento" required>
                  </div>
                </div><!-- /.form-group -->
              </div><!-- /.col -->

              <div class="box-footer col-xs-12 box-gris ">
                <center>
                  <button  type="submit" onclick="return confirm('¿Desea guardar la información?')"  class="btn btn-primary">Enviar</button><!-- Botón para registrar la información de una nueva sede educativa -->
                  <a onclick="cerrar_modal()" style="margin-left:15px;" class="btn btn-primary">Cancelar</a><!-- Botón para cancerlar el ingreso de una nueva sede educativa
                    y volver a la vista sin necesidad de recargar la página -->
                  </center>
                </div>

              </div>
            </div>
          </form>
          <script type="text/javascript">

          var input=  document.getElementById('numero');
          input.addEventListener('input',function(){
            if (this.value.length > 10)
            this.value = this.value.slice(0,10);
          })

          </script>
        </section>
