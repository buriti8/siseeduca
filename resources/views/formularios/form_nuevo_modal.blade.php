@extends('layouts.app')

@section('htmlheader_title')
Ingrese solicitud
@endsection

@section('main-content')
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>


<!-- Inicio contenido principal -->
<section  id="contenido_principal">
    <!-- Principal -->
    <div class="box box-primary">
        <div class="box-header">
        </div><!-- /.box-header -->

        <!-- Interior -->
        <div class="box box-primary" style="border: 1px solid #3c8dbc !important;" >
           <div class="titleboxes" style="font-size:20px;text-align:center;">Ingrese la sede a la cual le va a realizar el diagnóstico de necesidad</div>

                <form  id="forms"  method="post"  files="true"  enctype="multipart/form-data" action="{{url('/modal/Create')}}" >
                <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                <input type="hidden" value="{{ Auth::user()->name }}" name="userSolicitud">

                <div class="row" style="padding:20px;">
                    <!-- Inicio -->
                    <div class="col-md-12">
                        <div class="titleboxes" style="font-size:15px; text-align:center;"></div>
                        <div class="col-md-12" style="padding-top:15px;padding-bottom:15px;">
                          <div class="panel panel-primary ">
                            <div class="panel-heading" style="font-size: 16px;">Seleccione una sede educativa:</div>
                            <div class="panel-body">

                              @if(Auth::user()->isRole('administrador_sistema') || Auth::user()->isRole('administrador_infraestructura_tecnologica'))
                                <div class="form-group col-xs-6"  >
                                  <div id='selEstablecimientoContenedor'>
                                    <label for="selEstablecimiento">Seleccionar una subregión</label>
                                    <select id="selSubregion" name="selSubregion" class="form-control  col-xs-6" onchange="filtroSubregionMunicipios()">
                                      <option value="">Elige una</option>
                                      <option value="1">BAJO CAUCA</option>
                                      <option value="2">MAGDALENA MEDIO</option>
                                      <option value="3">NORDESTE</option>
                                      <option value="4">NORTE</option>
                                      <option value="5">OCCIDENTE</option>
                                      <option value="6">ORIENTE</option>
                                      <option value="7">SUROESTE</option>
                                      <option value="8">URABÁ</option>
                                      <option value="9">VALLE DE ABURRÁ</option>
                                    </select>
                                  </div>
                                </div>
                                <div class="form-group col-xs-6"  >
                                  <div id='selEstablecimientoContenedor'>
                                    <label for="selMunicipio">Seleccionar municipio</label>
                                    <select  id="selMunicipio" name="selMunicipio" onchange="filtroMunicipioEstablecimiento()" class="form-control  col-xs-6">
                                      <option selected="selected" value="">Elige uno</option>
                                    </select>
                                  </div>
                                </div>
                              @endif

                              <div class="form-group col-xs-6"  >
                                <div id='selEstablecimientoContenedor'>
                                  <label for="selEstablecimiento">Seleccionar un establecimiento educativo</label>
                                  <select  id="selEstablecimiento" name="selEstablecimiento" class="form-control  col-xs-6"  required>
                                    <option selected="selected" value="">Elige uno</option>
                                    @foreach ($temp_historico_establecimientos as $establecimiento)
                                      <option value="{{$establecimiento->codigo_establecimiento}}">{{$establecimiento->nombre_establecimiento}}</option>
                                    @endforeach
                                  </select>
                                </div>
                              </div>
                              <div class="form-group col-xs-6"  >
                                <div id='selEstablecimientoContenedor'>
                                  <label for="selSede">Seleccionar una sede educativa</label>
                                  <select  id="selSede" name="selSede" class="form-control  col-xs-6" required>
                                    <option value="">Elige una</option>
                                  </select>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                    </div>



                    <div class="col-md-12">
                        <div class="titleboxes" style="font-size:15px; text-align:center;"></div>
                    </div>
                    <!-- Fin -->

                    <div class="col-xs-12" style="margin-top:15px;">
                      <center>
                          <!-- <button onclick="return confirm('Recuerda:Se le dará respuesta al correo al que tiene inscrito.')" type="submit" class="btn btn-primary">Enviar</button>-->
											  <button  type="submit" onclick="return confirm('¿Desea guardar la información?')"  class="btn btn-primary">Enviar</button>
                        <a href="{{ url('home') }}" class="btn btn-info">Regresar</a>
                      </center>
                    </div>

                </div>
            </form>
        </div>
        <!-- Fin interior -->
    </div>
     <!-- Principal -->

</section>
<!-- Fin contenido principal -->




@endsection
