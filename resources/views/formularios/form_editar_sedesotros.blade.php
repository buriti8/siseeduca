<section >

  <div class="row" >

    <div class="col-md-12">


      <div class="box box-primary box-gris">

        <div class="box-header with-border my-box-header">
          <button onclick="cerrar_modal()" type="button" class="close" style="font-size: 40px;"  aria-label="Close">
            <span  aria-hidden="true">&times;</span><!-- Botón cerrar para volver a la vista sin necesidad de recargar la página -->
          </button>
          <!-- por medio del foreach recorremos el array que nos envío el controlador
          y mostramos el nombre de la sede-->
          @foreach($nombres as $nombre)
            <h5><strong>{{$nombre->nombre_municipio}} &nbsp;&nbsp; - &nbsp;&nbsp; {{$nombre->nombre_establecimiento}} &nbsp;&nbsp; - &nbsp;&nbsp; {{$nombre->nombre_sede}}  &nbsp;&nbsp; - &nbsp;&nbsp; {{$nombre->direccion}} &nbsp;&nbsp; -
              &nbsp;&nbsp; Código Dane: {{ $sedesespacio->DaneSede}}</strong></h5>
            @endforeach
            <p>En este formulario se podrá actualizar la cantidad de espacios de la sede educativa.</p>
            <p style="color:red;">* Obligatorio</p>
          </div><!-- /.box-header -->

          <div id="notificacion_E2" ></div>
          <div class="box-body">


            <form   action="{{ url('editar_sedesotros') }}"  method="post" id="f_editar_sedesotros"  class="formentrada"  >
              <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
              <input type="hidden" name="id_sedesespacio" value="{{ $sedesespacio->id }}">
              <input type="hidden" name="sede_id" value="{{ $sedesespacio->DaneSede }}">

              <div class="col-md-12">
                <div class="form-group">
                  <label class="col-sm-3" for="nombre"> Cocinas <span style="color:red;font-size:20px;">*</span></label>
                  <div class="col-xs-3" >
                    <input type="number" class="form-control" id="cocinas" name="cocinas" min="0" max="99"  value="{{ $sedesespacio->Cocinas }}" required  >
                  </div>
                </div><!-- /.form-group -->
              </div><!-- /.col -->

              <div class="col-md-12">
                <div class="form-group">
                  <label class="col-sm-3" for="nombre"> Comedores <span style="color:red;font-size:20px;">*</span></label>
                  <div class="col-xs-3" >
                    <input type="number" class="form-control" id="comedores" name="comedores" min="0" max="99"  value="{{ $sedesespacio->Comedores }}" required  >
                  </div>
                </div><!-- /.form-group -->
              </div><!-- /.col -->

              <div class="col-md-12">
                <div class="form-group">
                  <label class="col-sm-3" for="nombre"> Viviendas <span style="color:red;font-size:20px;">*</span></label>
                  <div class="col-xs-3" >
                    <input type="number" class="form-control" id="vivienda" name="vivienda" min="0" max="99"  value="{{ $sedesespacio->Vivienda }}" required  >
                  </div>
                </div><!-- /.form-group -->
              </div><!-- /.col -->

              <div class="col-md-12">
                <div class="form-group">
                  <label class="col-sm-3" for="nombre"> Canchas <span style="color:red;font-size:20px;">*</span></label>
                  <div class="col-xs-3" >
                    <input type="number" class="form-control" id="canchas" name="canchas" min="0" max="99"  value="{{ $sedesespacio->Canchas }}" required  >
                  </div>
                </div><!-- /.form-group -->
              </div><!-- /.col -->

              <div class="col-md-12">
                <div class="form-group">
                  <label class="col-sm-3" for="nombre"> Placas múltiples <span style="color:red;font-size:20px;">*</span></label>
                  <div class="col-xs-3" >
                    <input type="number" class="form-control" id="placasmulti" name="placasmulti" min="0" max="99"  value="{{ $sedesespacio->PlacasMulti }}" required  >
                  </div>
                </div><!-- /.form-group -->
              </div><!-- /.col -->


              <div class="col-md-12">
                <div class="form-group">
                  <label class="col-sm-3" for="nombre"> Parques de juegos infantiles <span style="color:red;font-size:20px;">*</span></label>
                  <div class="col-xs-3" >
                    <input type="number" class="form-control" id="juegosinfantiles" name="juegosinfantiles" min="0" max="99"  value="{{ $sedesespacio->JuegosInfantiles }}" required  >
                  </div>
                </div><!-- /.form-group -->
              </div><!-- /.col -->

              

              <div class="box-footer col-xs-12 box-gris ">
                <center>
                  <button type="submit" class="btn btn-primary">Enviar</button>
                  <a onclick="cerrar_modal()" style="margin-left:15px;" class="btn btn-primary">Cancelar</a><!-- Botón para cancerlar el ingreso de una nueva sede educativa
                    y volver a la vista sin necesidad de recargar la página -->
                  </center>
                </div>

              </form>

            </div>
          </div>
        </div>
      </div>
    </section>
