<section  >

  <!--Función para validar que el usuario elija como minímo una opción en los checkbox -->
  <script language="JavaScript">

  function validarRiesgos()
  {
    var riesgos=document.some_form2['riesgos[]'];
    var acumulador=0;

    for(i=0;i<riesgos.length;i++){
      if(riesgos[i].checked){
        acumulador=1;
      }
    }

    if (acumulador==0){
      alert('Si su sede educativa posee riesgos; selecciónelos. Sino, debe seleccionar la opción: "No tiene riesgos".');
      return false;
    }

    document.some_form2.submitted.value='yes';
    return true;

  }

  $('#group1').click(function() {
    // Si esta seleccionado (si la propiedad checked es igual a true)
    if ($(this).prop('checked')) {
      // Selecciona cada input que tenga la clase .checar
      $('.checar').prop('disabled', true);
    } else {
      // Deselecciona cada input que tenga la clase .checar
      $('.checar').prop('disabled', false);
    }
  });

  </script>

  <div class="col-md-12">
    <div class="box box-primary  box-gris">
      <div class="box-header with-border my-box-header">
        <h3 class="box-title"><strong>Riesgos de la sedes educativas</strong></h3>
        <button onclick="cerrar_modal()" type="button" class="close" style="font-size: 40px;"  aria-label="Close">
          <span  aria-hidden="true">&times;</span><!-- Botón cerrar para volver a la vista sin necesidad de recargar la página -->
        </button>
        <p>En este espacio se especificarán los riesgos de las sedes educativas, según lo requerido a continuación.</p>
        <p style="color:red;">* Obligatorio</p>
      </div><!-- /.box-header -->

      <form  name="some_form2"  method="POST" id="f_ingresar_riesgo" class="formentrada">
        <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
        <div class="box-body">

          <div class="row" style="padding:20px;">
            <div class="col-md-12">
              <div class="titleboxes" style="font-size:15px; text-align:center;">
              </div>
              <br>
              <div class="col-xs-12" style="border-bottom:1px solid #337ab7; border-top:1px solid #337ab7; padding:0.8em; margin-top:0.5em; margin-bottom:0.5em; text-align:center;">
                @foreach ($nombres as $nombre)

                  <h5><strong>Ingrese riesgos de la sede educativa &nbsp;&nbsp; {{$nombre->nombre_sede}} &nbsp;&nbsp; - &nbsp;&nbsp; código dane: {{$nombre->codigo_sede}}</strong> <span style="color:red;font-size:20px;">*</span></h5>

                @endforeach
                </div>

                <!--Mostramos al usuario los tipos de riesgos con type="checkbox", pues, se pueden presentar casos
                donde una sede tenga varios tipos de riesgos-->
                @foreach ($nombres as $nombre)
                  <input type="hidden" name="sede_id" value="{{$nombre->codigo_sede}}">
                @endforeach

                <h5 style="margin-top:10px">Seleccione riesgos de la sede educativa:</h5>
                <div class="col-xs-3" style="margin-left:100px; margin-top:10px">
                  <label class="checkbox-formulario">
                    <input type="checkbox" name="riesgos[]"  value="Ninguno" id="group1" title="8" value="1">
                    <span class="label-text"><b>1) No tiene riesgos</b></span>
                  </label>
                </div>
                <div class="col-xs-3" style="margin-left:30px; margin-top:10px">
                  <label class="checkbox-formulario">
                    <input type="checkbox" name="riesgos[]"  value="Inundación" class="checar" title="1" value="1">
                    <span class="label-text"><b>2) Inundación</b></span>
                    <p align="justify">Desbordamiento o subida de aguas, generalmente lentos, sobre pequeñas áreas o vastas regiones, que supera la sección del cauce de los ríos.
                      Inundaciones por mareas en zonas litorales se reportarán bajo el término “marejada”.</p>
                    </label>
                  </div>
                  <div class="col-xs-3" style="margin-left:30px; margin-top:10px">
                    <label class="checkbox-formulario">
                      <input type="checkbox" name="riesgos[]" class="checar" value="Socavación lateral" title="3" value="1">
                      <span class="label-text"><b>3) Socavación lateral de ríos</b></span>
                      <p align="justify">Consiste en la profundización del nivel del fondo del cauce de una corriente causada por el aumento del nivel de agua en las avenidas,
                        modificaciones en la morfología del cauce o por la construcción de estructuras en el cauce como puentes, espigones, etc.
                      </p>
                    </label>
                  </div>
                  <div class="col-xs-3" style="float: left; margin-left:100px; margin-top:10px">
                    <label class="checkbox-formulario">
                      <input type="checkbox" name="riesgos[]" value="Deslizamiento" class="checar"  title="2" value="1">
                      <span class="label-text"><b>4) Deslizamiento</b></span>
                      <p align="justify">Todo movimiento de masa en una ladera, diferente a erosión superficial. Incluye términos otros términos como derrumbe, asentamiento, corrimiento, movimiento de masa, reptación, desplazamiento, hundimiento, colapso de cavernas o minas, caída de rocas, desprendimiento (lento o rápido) sobre vertientes o laderas, de masas de suelo o de rocas.
                        Incluye los reportes de “falla” en cortes o taludes de laderas, vías, canales, excavaciones, etc.</p>
                      </label>
                    </div>
                    <div class="col-xs-3" style="margin-left:30px; margin-top:10px">
                      <label class="checkbox-formulario">
                        <input type="checkbox" name="riesgos[]" class="checar"  value="Remoción de masas" title="4" value="1">
                        <span class="label-text"><b>5) Remoción de masa</b></span>
                        <p align="justify">
                          También conocido como movimiento de inclinación, desplazamiento de masa o movimiento de masa, es el proceso geomorfológico por el cual el suelo,  la capa de materiales no consolidados, alterados,
                          como fragmentos de roca, granos minerales y todos los otros depósitos superficiales, se mueven cuesta abajo por la fuerza de la gravedad.
                        </p>
                      </label>
                    </div>
                    <div>
                      <div class="col-xs-3" style="margin-left:30px; margin-top:10px">
                        <label class="checkbox-formulario">
                          <input type="checkbox" name="riesgos[]" class="checar"  value="Vendavales" title="5" value="1">
                          <span class="label-text"><b>6) Vendavales</b></span>
                          <p align="justify">
                            Son ráfagas de viento muy fuertes por encima de 60 km/h muy comunes durante fuertes aguaceros. Parecidos a los tornados, son como violentos torbellinos, generalmente de corta duración (2 min a 15 min), que se originan en una nube de tormenta hasta el suelo, levantando todo a su paso.
                            Generalmente destechan la infraestructura, tumban árboles, cables de alta tensión y antenas de televisión.
                          </p>
                        </label>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="box-footer col-xs-12 box-gris ">
                  <center>
                    <button type="submit" onclick="this.form.action='/requerimientosIFisica/ingresar_riesgos'; return validarRiesgos()"  class="btn btn-primary">Continuar</button><!-- Botón para registrar la información -->
                    <a onclick="cerrar_modal()" style="margin-left:15px;" class="btn btn-primary">Cancelar</a><!-- Botón para cancerlar el ingreso de una nueva sede educativa
                      y volver a la vista sin necesidad de recargar la página -->
                    </center>
                  </div>
                </form>
              </section>
