<section>
<div class="row" >
    <div class="col-md-12">
        <div class="box box-primary box-gris">
            <div class="box-header with-border my-box-header">
              <button onclick="cerrar_modal()" type="button" class="close" style="font-size: 40px; color:#34495E;"  aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button> <!-- Botón cerrar para volver a la vista sin necesidad de recargar la página -->

                <h3 class="box-title"><strong>Editar Información Tipo de dispositivo</strong></h3>
            </div><!-- /.box-header -->
            <p style="color:red;">* Obligatorio</p>

            <div id="notificacion_E2" ></div>
            <div class="box-body">
                <form   action="{{ url('editar_tipodispositivo') }}"  method="post" id="f_editar_tipodispositivo"  class="formentrada">
                        <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                        <input type="hidden" name="id_tipo" value="{{ $tipo->id }}">

                    <div class="col-md-6">
                        <div class="form-group">
                                <label class="col-sm-2" for="nombre">Nombre <span style="color:red;font-size:20px;">*</span></label>
                                <div class="col-sm-10" >
                                <input type="text" pattern="[a-zA-ZñÑáéíóúÁÉÍÓÚ\s]+" title="Solo ingrese texto" class="form-control" id="nombre_tipo" name="nombre_tipo"  value="{{ $tipo->nombre }}"  maxlength=100 required>
                                </div>
                        </div><!-- /.form-group -->
                    </div><!-- /.col -->

                    <div class="col-md-6">
                        <div class="form-group">
                                <label class="col-sm-2" for="descripcion">Descripción</label>
                                <div class="col-sm-10" >
                                <input type="text" class="form-control" id="descripcion_tipo" name="descripcion_tipo"  value="{{ $tipo->descripcion }}" maxlength=100>
                                </div>
                        </div><!-- /.form-group -->
                    </div><!-- /.col -->

                    <div class="box-footer col-xs-12 box-gris ">
                            <button type="submit" class="btn btn-primary">Actualizar</button>
<a onclick="cerrar_modal()" style="margin-left:15px;" class="btn btn-primary">Cancelar</a>
                    </div>
                </form>
            </div>
            </div>
        </div>
    </div>
</section>
