<section  >

  <form  action="{{ url('ingresar_alcalde') }}" id="f_ingresar_alcalde"   method="POST"  class="formentrada">
  <div class="col-md-12">
    <div class="box box-primary  box-gris">
      <div class="box-header with-border my-box-header">
        @if(Auth::user()->isRole('secretarios') || Auth::user()->isRole('enlace_tic'))
          <h3 class="box-title"><strong>Ingresar información de alcalde, secretario(a) de educación y administrador(a) SIMAT del municipio {{$municipios}}</strong></h3>
        @else
          <h3 class="box-title"><strong>Ingresar información de alcalde, secretario(a) de educación y administrador(a) SIMAT.</strong></h3>
        @endif
        <button onclick="cerrar_modal()" type="button" class="close" style="font-size: 40px;"  aria-label="Close">
          <span  aria-hidden="true">&times;</span><!-- Botón cerrar para volver a la vista sin necesidad de recargar la página -->
        </button>
        <p style="color:red;">* Obligatorio</p>
      </div><!-- /.box-header -->
      <div class="col-md-12" style="font-size:10px; text-align:center;">
      </div>
      <br>

      @if(Auth::user()->isRole('administrador_sistema') || Auth::user()->isRole('Administrador_infraestructura_educativa') || Auth::user()->isRole('Administrador_infraestructura_tecnologica') || Auth::user()->isRole('Administrador_matricula') || Auth::user()->isRole('Administrador_due') || Auth::user()->isRole('Administrador_pruebas_saber'))
      <!--Filtros según el rol del usuario-->
      <div style="margin-left:45px;"  class="form-group col-xs-4"  >
        <div >
          <label  for="selMunicipio">Seleccione un municipio:  </label>
          <select  id="selMunicipio" name="selMunicipio" class="form-control  col-xs-6" required>
            <option selected="selected" value="">Elige uno...</option>
            @foreach ($municipios as $municipio)
              <option value="{{$municipio->CodigoDaneMunicipio}}">{{$municipio->NombreMunicipio}}</option>
            @endforeach
          </select>
        </div>
      </div>
      @endif

        <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
        <div class="box-body">

          <div class="row" style="padding:20px;">
            <div class="col-md-12">

              <br>
              <div class="col-xs-12" style="border-bottom:1px solid #337ab7; border-top:1px solid #337ab7; padding:0.8em; margin-top:0; margin-bottom:10px; text-align:center;">
                <strong>Ingresar información de alcalde</strong></div>

                <div class="col-md-12">
                  <div class="form-group">
                    <label class="col-sm-3" for="nombre"> Nombres <span style="color:red;font-size:20px;">*</span></label>
                    <div class="col-xs-3" >
                      <input type="text" onkeyup="javascript:this.value=this.value.toUpperCase();" class="form-control" name="nombre_alcalde" pattern="[a-zA-ZñÑáéíóúÁÉÍÓÚ\s]+" title="Solo ingrese texto" required>
                    </div>
                  </div><!-- /.form-group -->
                </center>
                </div><!-- /.col -->

                <div class="col-md-12">
                  <div class="form-group">
                    <label class="col-sm-3" for="nombre"> Apellidos <span style="color:red;font-size:20px;">*</span></label>
                    <div class="col-xs-3" >
                      <input type="text" onkeyup="javascript:this.value=this.value.toUpperCase();" class="form-control" pattern="[a-zA-ZñÑáéíóúÁÉÍÓÚ\s]+" title="Solo ingrese texto" name="apellido_alcalde" required>
                    </div>
                  </div><!-- /.form-group -->
                </div><!-- /.col -->

                <div class="col-md-12">
                  <div class="form-group">
                    <label class="col-sm-3" for="nombre"> Celular 1<span style="color:red;font-size:20px;">*</span></label>
                    <div class="col-xs-3" >
                      <input type="number" placeholder="No ingrese signo más(+) ni menos(-)"  id="numero1" class="form-control" name="celularesA[]" required>
                    </div>
                  </div><!-- /.form-group -->
                </div><!-- /.col -->

                <div class="col-md-12">
                  <div class="form-group">
                    <label class="col-sm-3" for="nombre"> Celular 2</label>
                    <div class="col-xs-3" >
                      <input type="number" placeholder="No ingrese signo más(+) ni menos(-)"  id="numero2" class="form-control" name="celularesA[]">
                    </div>
                  </div><!-- /.form-group -->
                </div><!-- /.col -->

                <div class="col-md-12">
                  <div class="form-group">
                    <label class="col-sm-3" for="nombre"> Teléfono <span style="color:red;font-size:20px;">*</span></label>
                    <div class="col-xs-3" >
                      <input type="text" class="form-control" name="telefono_alcalde" required>
                    </div>
                  </div><!-- /.form-group -->
                </div><!-- /.col -->

                <div class="col-md-12">
                  <div class="form-group">
                    <label class="col-sm-3" for="nombre"> Correo personal <span style="color:red;font-size:20px;">*</span></label>
                    <div class="col-xs-3" >
                      <input type="text" pattern="[a-zA-ZàáâäãåąčćęèéêëėįìíîïłńòóôöõøùúûüųūÿýżźñçčšžÀÁÂÄÃÅĄĆČĖĘÈÉÊËÌÍÎÏĮŁŃÒÓÔÖÕØÙÚÛÜŲŪŸÝŻŹÑßÇŒÆČŠŽ∂ð ,.'-_]{2,64}" class="form-control" name="correo_alcalde" required>
                    </div>
                  </div><!-- /.form-group -->
                </div><!-- /.col -->

                <div class="col-md-12">
                  <div class="form-group">
                    <label class="col-sm-3" for="nombre"> Correo alcaldía <span style="color:red;font-size:20px;">*</span></label>
                    <div class="col-xs-3" >
                      <input type="text" pattern="[a-zA-ZàáâäãåąčćęèéêëėįìíîïłńòóôöõøùúûüųūÿýżźñçčšžÀÁÂÄÃÅĄĆČĖĘÈÉÊËÌÍÎÏĮŁŃÒÓÔÖÕØÙÚÛÜŲŪŸÝŻŹÑßÇŒÆČŠŽ∂ð ,.'-_]{2,64}"  class="form-control" name="correo_alcaldia" required>
                    </div>
                  </div><!-- /.form-group -->
                </div><!-- /.col -->

                <div class="col-md-12">
                  <div class="form-group">
                    <label class="col-sm-3" for="nombre"> Dirección <span style="color:red;font-size:20px;">*</span></label>
                    <div class="col-xs-3" >
                      <input type="text" onkeyup="javascript:this.value=this.value.toUpperCase();" class="form-control" name="direccion_alcaldia" required>
                    </div>
                  </div><!-- /.form-group -->
                </div><!-- /.col -->

                <div class="col-xs-12" style="border-bottom:1px solid #337ab7; border-top:1px solid #337ab7; padding:0.8em; margin-top:30px; margin-bottom:10px; text-align:center;">
                  <strong>Ingresar información de secreatrio(a) de educación</strong></div>

                  <div class="col-md-12">
                    <div class="form-group">
                      <label class="col-sm-3" for="nombre"> Nombres <span style="color:red;font-size:20px;">*</span></label>
                      <div class="col-xs-3" >
                        <input type="text" onkeyup="javascript:this.value=this.value.toUpperCase();" pattern="[a-zA-ZñÑáéíóúÁÉÍÓÚ\s]+" title="Solo ingrese texto" class="form-control" name="nombre_secretario" required>
                      </div>
                    </div><!-- /.form-group -->
                  </center>
                  </div><!-- /.col -->

                  <div class="col-md-12">
                  <div class="form-group">
                    <label class="col-sm-3" for="nombre"> Apellidos <span style="color:red;font-size:20px;">*</span></label>
                    <div class="col-xs-3" >
                      <input type="text" onkeyup="javascript:this.value=this.value.toUpperCase();" pattern="[a-zA-ZñÑáéíóúÁÉÍÓÚ\s]+" title="Solo ingrese texto" class="form-control" name="apellido_secretario" required>
                    </div>
                  </div><!-- /.form-group -->
                  </div><!-- /.col -->

                  <div class="col-md-12">
                    <div class="form-group">
                      <label class="col-sm-3" for="nombre"> Celular 1<span style="color:red;font-size:20px;">*</span></label>
                      <div class="col-xs-3" >
                        <input type="number" placeholder="No ingrese signo más(+) ni menos(-)" id="numero3" class="form-control" name="celularesS[]" required>
                      </div>
                    </div><!-- /.form-group -->
                  </div><!-- /.col -->

                  <div class="col-md-12">
                    <div class="form-group">
                      <label class="col-sm-3" for="nombre"> Celular 2</label>
                      <div class="col-xs-3" >
                        <input type="number" placeholder="No ingrese signo más(+) ni menos(-)" id="numero4" class="form-control" name="celularesS[]">
                      </div>
                    </div><!-- /.form-group -->
                  </div><!-- /.col -->

                  <div class="col-md-12">
                  <div class="form-group">
                    <label class="col-sm-3" for="nombre"> Teléfono <span style="color:red;font-size:20px;">*</span></label>
                    <div class="col-xs-3" >
                      <input type="text" class="form-control" name="telefono_secretario" required>
                    </div>
                  </div><!-- /.form-group -->
                  </div><!-- /.col -->

                  <div class="col-md-12">
                  <div class="form-group">
                    <label class="col-sm-3" for="nombre"> Correo personal <span style="color:red;font-size:20px;">*</span></label>
                    <div class="col-xs-3" >
                      <input type="text" pattern="[a-zA-ZàáâäãåąčćęèéêëėįìíîïłńòóôöõøùúûüųūÿýżźñçčšžÀÁÂÄÃÅĄĆČĖĘÈÉÊËÌÍÎÏĮŁŃÒÓÔÖÕØÙÚÛÜŲŪŸÝŻŹÑßÇŒÆČŠŽ∂ð ,.'-_]{2,64}" class="form-control" name="correo_secretario"  required>
                    </div>
                  </div><!-- /.form-group -->
                  </div><!-- /.col -->

                  <div class="col-md-12">
                  <div class="form-group">
                    <label class="col-sm-3" for="nombre"> Correo secretaría de educación <span style="color:red;font-size:20px;">*</span></label>
                    <div class="col-xs-3" >
                      <input type="text" pattern="[a-zA-ZàáâäãåąčćęèéêëėįìíîïłńòóôöõøùúûüųūÿýżźñçčšžÀÁÂÄÃÅĄĆČĖĘÈÉÊËÌÍÎÏĮŁŃÒÓÔÖÕØÙÚÛÜŲŪŸÝŻŹÑßÇŒÆČŠŽ∂ð ,.'-_]{2,64}"  class="form-control" name="correo_secretariaeducacion" required>
                    </div>
                  </div><!-- /.form-group -->
                  </div><!-- /.col -->

                  <div class="col-md-12">
                  <div class="form-group">
                    <label class="col-sm-3" for="nombre"> Cargo <span style="color:red;font-size:20px;">*</span></label>
                    <div class="col-xs-3" >
                      <input type="text" onkeyup="javascript:this.value=this.value.toUpperCase();" pattern="[a-zA-ZñÑáéíóúÁÉÍÓÚ\s,]+" title="Solo ingrese texto" class="form-control" name="cargo_secretario" required>
                    </div>
                  </div><!-- /.form-group -->
                  </div><!-- /.col -->

                  <div class="col-md-12">
                  <div class="form-group">
                    <label class="col-sm-3" for="nombre"> Dirección oficina <span style="color:red;font-size:20px;">*</span></label>
                    <div class="col-xs-3" >
                      <input type="text" onkeyup="javascript:this.value=this.value.toUpperCase();" class="form-control" name="direccion_secretario" required>
                    </div>
                  </div><!-- /.form-group -->
                  </div><!-- /.col -->

                  <div class="col-xs-12" style="border-bottom:1px solid #337ab7; border-top:1px solid #337ab7; padding:0.8em; margin-top:30px; margin-bottom:10px;text-align:center;">
                    <strong>Ingresar información de administrador(a) SIMAT</strong></div>

                    <div class="col-md-12">
                      <div class="form-group">
                        <label class="col-sm-3" for="nombre"> Nombres <span style="color:red;font-size:20px;">*</span></label>
                        <div class="col-xs-3" >
                          <input type="text" onkeyup="javascript:this.value=this.value.toUpperCase();" class="form-control" pattern="[a-zA-ZñÑáéíóúÁÉÍÓÚ\s]+" title="Solo ingrese texto" name="nombre_adminsimat" required>
                        </div>
                      </div><!-- /.form-group -->
                    </center>
                  </div><!-- /.col -->

                  <div class="col-md-12">
                    <div class="form-group">
                      <label class="col-sm-3" for="nombre"> Apellidos <span style="color:red;font-size:20px;">*</span></label>
                      <div class="col-xs-3" >
                        <input type="text" onkeyup="javascript:this.value=this.value.toUpperCase();" class="form-control" pattern="[a-zA-ZñÑáéíóúÁÉÍÓÚ\s]+" title="Solo ingrese texto" name="apellido_adminsimat" required>
                      </div>
                    </div><!-- /.form-group -->
                  </div><!-- /.col -->

                  <div class="col-md-12">
                    <div class="form-group">
                      <label class="col-sm-3" for="nombre"> Celular <span style="color:red;font-size:20px;">*</span></label>
                      <div class="col-xs-3" >
                        <input type="number" placeholder="No ingrese signo más(+) ni menos(-)" id="numero5" class="form-control"  name="celular_adminsimat" required>
                      </div>
                    </div><!-- /.form-group -->
                  </div><!-- /.col -->

                  <div class="col-md-12">
                    <div class="form-group">
                      <label class="col-sm-3" for="nombre"> Teléfono <span style="color:red;font-size:20px;">*</span></label>
                      <div class="col-xs-3" >
                        <input type="text" class="form-control" name="telefono_adminsimat" required>
                      </div>
                    </div><!-- /.form-group -->
                  </div><!-- /.col -->

                  <div class="col-md-12">
                    <div class="form-group">
                      <label class="col-sm-3" for="nombre"> Correo personal <span style="color:red;font-size:20px;">*</span></label>
                      <div class="col-xs-3" >
                        <input type="text" pattern="[a-zA-ZàáâäãåąčćęèéêëėįìíîïłńòóôöõøùúûüųūÿýżźñçčšžÀÁÂÄÃÅĄĆČĖĘÈÉÊËÌÍÎÏĮŁŃÒÓÔÖÕØÙÚÛÜŲŪŸÝŻŹÑßÇŒÆČŠŽ∂ð ,.'-_]{2,64}"  class="form-control" name="correo_adminsimat" required>
                      </div>
                    </div><!-- /.form-group -->
                  </div><!-- /.col -->


                  <div class="col-md-12">
                    <div class="form-group">
                      <label class="col-sm-3" for="nombre"> Cargo <span style="color:red;font-size:20px;">*</span></label>
                      <div class="col-xs-3" >
                        <input type="text" onkeyup="javascript:this.value=this.value.toUpperCase();" class="form-control" pattern="[a-zA-ZñÑáéíóúÁÉÍÓÚ\s,]+" title="Solo ingrese texto" name="cargo_adminsimat" required>
                      </div>
                    </div><!-- /.form-group -->
                  </div><!-- /.col -->

                  <div class="col-md-12">
                    <div class="form-group">
                      <label class="col-sm-3" for="nombre"> Dirección oficina <span style="color:red;font-size:20px;">*</span></label>
                      <div class="col-xs-3" >
                        <input type="text" onkeyup="javascript:this.value=this.value.toUpperCase();" class="form-control" name="direccion_adminsimat" required>
                      </div>
                    </div><!-- /.form-group -->
                  </div><!-- /.col -->

                  <div class="box-footer col-xs-12 box-gris ">
                    <center>
                      <button  type="submit" onclick="return confirm('¿Desea guardar la información?')"  class="btn btn-primary">Enviar</button><!-- Botón para registrar la información de una nueva sede educativa -->
                      <a onclick="cerrar_modal()" style="margin-left:15px;" class="btn btn-primary">Cancelar</a><!-- Botón para cancerlar el ingreso de una nueva sede educativa
                        y volver a la vista sin necesidad de recargar la página -->
                      </center>
                    </div>

                  </div>
                </div>

                <script type="text/javascript">
                var input=  document.getElementById('numero1');
                input.addEventListener('input',function(){
                  if (this.value.length > 10)
                  this.value = this.value.slice(0,10);
                })

                var input=  document.getElementById('numero2');
                input.addEventListener('input',function(){
                  if (this.value.length > 10)
                  this.value = this.value.slice(0,10);
                })

                var input=  document.getElementById('numero3');
                input.addEventListener('input',function(){
                  if (this.value.length > 10)
                  this.value = this.value.slice(0,10);
                })

                var input=  document.getElementById('numero4');
                input.addEventListener('input',function(){
                  if (this.value.length > 10)
                  this.value = this.value.slice(0,10);
                })

                var input=  document.getElementById('numero5');
                input.addEventListener('input',function(){
                  if (this.value.length > 10)
                  this.value = this.value.slice(0,10);
                })


                </script>

              </form>
            </section>
