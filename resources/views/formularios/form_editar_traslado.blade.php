@extends('layouts.app')

@section('htmlheader_title')
Editar Traslado
@endsection

@section('main-content')
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>

<!-- Inicio contenido principal -->
<section  id="contenido_principal">
    <!-- Principal -->
    <div class="box box-primary">
        <div class="box-header">
        </div><!-- /.box-header -->

<div class="box box-primary" style="border: 1px solid #3c8dbc !important;" >
        <div class="titleboxes" style="font-size:20px;text-align:center;">Editar traslado</div>

        <!-- Body -->
        <p style="color:red;">* Obligatorio</p>

        <div class="box-body">
            <form   action="{{ url('editar_traslado', $traslado->id) }}"  method="post" id="frmtraslado">
                <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">

                 <div class="col-md-12">
                    <div class="form-group">
                        <label class="col-sm-3" for="origen">Id Translado</label>
                        <div class="col-sm-6" >
                            <!-- <input type="text" class="form-control" id="traslado_id" name="traslado_id" value='{{ $traslado->id }}'  readonly required style="width:60%;"> -->
                            <label><strong>{{ $traslado->id }}</strong></label>
                        </div>
                    </div>
                </div>

                <div class="col-md-12">
                    <div class="form-group">
                        <label class="col-sm-3" for="origen">Id dispoitivo</label>
                        <div class="col-sm-6" >
                            <input type="number" class="form-control" id="dispositivo_id" name="dispositivo_id" value='{{ $traslado->dispositivo_id }}' maxlength=8 min="1" required readonly style="width:60%;">
                            <!-- <label>{{ $traslado->dispositivo_id }}</label> -->
                        </div>
                    </div>
                </div>

                <div class="col-md-12">
                    <div class="form-group">
                        <label class="col-sm-3" for="serial">Serial dispositivo</label>
                        <div class="col-sm-4" >
                            <label><strong>{{ $serial }}</strong></label>
                        </div>
                    </div>
                </div>

                 <div class="col-md-12">
                    <div class="form-group">
                        <label class="col-sm-3" for="marca">Marca dispositivo</label>
                        <div class="col-sm-4" >
                            <label><strong>{{ $marca }}</strong></label>
                        </div>
                    </div>
                </div>

                <div class="col-md-12">
                    <div class="form-group">
                        <label class="col-sm-3" for="origen">Sede origen</label>
                        <div class="col-sm-6" >
                            <label><strong>{{ $sede_origen }} - {{ $nombre_sede_origen}} - {{ $direccion }}</strong></label>
                        </div>
                    </div>
                </div>

                <div class="col-md-12">
                    <div class="form-group">
                        <label class="col-sm-2" for="destino">Sede destino <span style="color:red;font-size:20px;">*</span></label>
                        <div class="col-sm-12">
                        @role('secretarios')
                            @include('partialsroles.secretario_editar')
                        @else
                            @role('rectores')
                                @include('partialsroles.rector_editar')
                            @else
                                @include('partialsroles.filtro_editar')
                            @endrole
                        @endrole
                        </div>
                    </div>
                </div>

                <div class="col-md-12">
                    <div class="form-group">
                        <label class="col-sm-2" for="motivo">Motivo <span style="color:red;font-size:20px;">*</span></label>
                        <div class="col-sm-6">
                            <select id="motivo" name="motivo" class="form-control" style="width:60%;">
                                    @if($traslado->motivo == 1)
                                        <option value="1" selected>Cierre temporal</option>
                                        <option value="2">Cierre definitivo</option>
                                    @else
                                        <option value="1">Cierre temporal</option>
                                        <option value="2" selected>Cierre definitivo</option>
                                    @endif
                            </select>
                        </div>
                    </div>
                </div>

                <div class="col-md-12">
                    <div class="form-group">
                        <label class="col-sm-2" for="descripcion">Descripción</label>
                        <div class="col-sm-6">
                            <textarea rows="4" cols="50" type="text" id="descripcion" name="descripcion" class="form-control" maxlength=230>{{ $traslado->descripcion }}</textarea>
                        </div>
                    </div>
                </div>

                <div class="col-xs-12" style="margin-top:15px;">
                    <button type="submit" class="btn btn-primary">Editar </button>
                    <a href="{{ url('traslado_dispositivos') }}" class="btn btn-primary pull-right">Regresar</a>
                </div>
            </form>
        </div>
        <!-- Fin body -->
</div>

    </div>
    <!-- Fin principal -->

</section>
@endsection
