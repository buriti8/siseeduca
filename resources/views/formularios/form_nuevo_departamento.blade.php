<section>

  <div class="row" >

  <div class="col-md-12">

    <div class="box box-primary  box-gris">

      <div class="box-header with-border my-box-header">

        <button onclick="cerrar_modal()" type="button" class="close" style="font-size: 40px; color:#34495E;"  aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button> <!-- Botón cerrar para volver a la vista sin necesidad de recargar la página -->
        <h3 class="box-title" ><strong>Nuevo Departamento</strong></h3>
        <p style="color:red;">* Obligatorio</p>
      </div>

      <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">

      <div class="box-body">
        <form   action="{{ url('crear_departamento') }}"  method="post" id="f_crear_departamento" class="formentrada" >
          <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">

          <div class="col-md-12">
            <div class="form-group">
              <label class="col-sm-2" for="nombre">Código Departamento <span style="color:red;font-size:20px;">*</span> </label>
              <div class="col-xs-3" >
                <input type="number" class="form-control" id="codigo_departamento" name="codigo_departamento"  required   >
              </div>
            </div><!-- /.form-group -->
          </div><!-- /.col -->

          <div class="col-md-12">
            <div class="form-group">
              <label class="col-sm-2" for="nombre">Código DANE <span style="color:red;font-size:20px;">*</span> </label>
              <div class="col-xs-3" >
                <input type="number" class="form-control" id="codigo_dane_departamento" name="codigo_dane_departamento"  required   >
              </div>
            </div><!-- /.form-group -->
          </div><!-- /.col -->

          <div class="col-md-12">
            <div class="form-group">
              <label class="col-sm-2" for="nombre">Nombre <span style="color:red;font-size:20px;">*</span> </label>
              <div class="col-xs-3" >
                <input type="text" class="form-control" id="nombre_departamento" name="nombre_departamento"  required   >
              </div>
            </div><!-- /.form-group -->
          </div><!-- /.col -->

          <div class="box-footer col-xs-12 box-gris text-center ">
            <button  type="submit" onclick="return confirm('¿Desea guardar la información?')"  class="btn btn-primary">Enviar</button>
            <a onclick="cerrar_modal()" style="margin-left:15px;" class="btn btn-primary">Cancelar</a>
          </div>

        </form>

      </div>

    </div>

  </div>
  </div>
</section>
