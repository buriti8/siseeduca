<section class="content" >
  <div class="col-md-12">
    <div class="box box-primary box-gris">
      <div class="box-header with-border my-box-header">
        <button onclick="cerrar_modal()" type="button" class="close" style="font-size: 40px; color:#34495E;"  aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button> <!-- Botón cerrar para volver a la vista sin necesidad de recargar la página -->
        <h3 class="box-title"><strong>Nuevo contrato</strong></h3>
      </div><!-- /.box-header -->
      <p style="color:red;">* Obligatorio</p>

      <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">


      <!-- Body -->
      <div class="box-body">

        <form   action="{{ url('crear_contrato') }}"  method="post" id="f_crear_contrato" class="formentrada" >
          <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">

          <div class="col-md-6">
            <div class="form-group">
              <label class="col-sm-4" for="numero_contrato">Número contrato <span style="color:red;font-size:20px;">*</span></label>
              <div class="col-sm-6" >
                <input type="number" class="form-control" id="numero_contrato" name="numero_contrato" maxlength="100" required>
              </div>
            </div>
          </div>

          <div class="col-md-6">
            <div class="form-group">
              <label class="col-sm-4" for="nit_contratista">Nit contratista <span style="color:red;font-size:20px;">*</span></label>
              <div class="col-sm-6" >
                <input type="text" class="form-control" id="nit_contratista" name="nit_contratista" maxlength="100" required>
              </div>
            </div>
          </div>

          <div class="col-md-6">
            <div class="form-group">
              <label class="col-sm-4" for="objeto_contrato">Objeto contrato <span style="color:red;font-size:20px;">*</span></label>
              <div class="col-sm-6" >
                <textarea rows='4' cols='50' class="form-control" id="objeto_contrato" name="objeto_contrato" required></textarea>
              </div>
            </div>
          </div>

          <div class="col-md-6">
            <div class="form-group">
              <label class="col-sm-4" for="nombre_contratista">Nombre contratista <span style="color:red;font-size:20px;">*</span></label>
              <div class="col-sm-6" >
                <input type="text" pattern="[a-zA-ZñÑáéíóúÁÉÍÓÚ\s]+" title="Solo ingrese texto" class="form-control" id="nombre_contratista" name="nombre_contratista" maxlength="230" required>
              </div>
            </div>
          </div>

          <div class="col-md-6">
            <div class="form-group">
              <label class="col-sm-4" for="tipo_contrato">Tipo de contrato <span style="color:red;font-size:20px;">*</span></label>
              <div class="col-sm-6">
                <select class="form-control" id="tipo_contrato" name="tipo_contrato">
                  <option value="1">CONTRATO</option>
                  <option value="2">CONVENIO</option>
                </select>
              </div>
            </div>
          </div>

          <div class="col-md-6">
            <div class="form-group">
              <label class="col-sm-4" for="telefono_contratista">Teléfono contratista <span style="color:red;font-size:20px;">*</span></label>
              <div class="col-sm-6" >
                <input type="number" class="form-control" id="telefono_contratista" name="telefono_contratista" maxlength=200 min=1 required>
              </div>
            </div>
          </div>

          <div class="col-md-6">
            <div class="form-group">
              <label class="col-sm-4" for="proyecto" id="id_proyecto" name="id_proyecto">Proyecto <span style="color:red;font-size:20px;">*</span></label>
              <div class="col-sm-6">
                <select class="form-control" id="id_proyecto" name="id_proyecto" required>
                  <!-- <option>Seleccione...</option> -->
                  @foreach($proyectos as $proyecto)
                  <option value="{{ $proyecto->id }}">{{ $proyecto->nombre }}</option>
                  @endforeach
                </select>
              </div>
            </div>
          </div>

          <div class="col-md-6">
            <div class="form-group">
              <label class="col-sm-4" for="valor_total_contrato">Valor total contrato <span style="color:red;font-size:20px;">*</span></label>
              <div class="col-sm-6" >
                <input type="number" min="1" class="form-control" id="valor_total_contrato" name="valor_total_contrato" maxlength=200 required>
              </div>
            </div>
          </div>

          <div class="col-md-6">
            <div class="form-group">
              <label class="col-sm-4" for="fecha_del_acta_inicio">Fecha de acta de inicio <span style="color:red;font-size:20px;">*</span></label>
              <div class="col-sm-6">
                <input type="date" class="form-control" id="fecha_del_acta_inicio" name="fecha_del_acta_inicio" min="1970-01-01" max="2040-12-31" required placeholder="yyyy-mm-dd">
              </div>
            </div>
          </div>

          <div class="col-md-6">
            <div class="form-group">
              <label class="col-sm-4" for="dependencia_contratante">Dependencia contratante <span style="color:red;font-size:20px;">*</span></label>
              <div class="col-sm-6" >
                <input type="text" pattern="[a-zA-ZñÑáéíóúÁÉÍÓÚ\s]+" title="Solo ingrese texto" class="form-control" id="dependencia_contratante" name="dependencia_contratante" maxlength=230 required>
              </div>
            </div>
          </div>

          <div class="col-md-6">
            <div class="form-group">
              <label class="col-sm-4" for="fecha_terminacion_contrato">Fecha terminación del contrato <span style="color:red;font-size:20px;">*</span></label>
              <div class="col-sm-6" >
                <input type="date" class="form-control" id="fecha_terminacion_contrato" name="fecha_terminacion_contrato" min="1970-01-01" max="2040-12-31" required>
              </div>
            </div>
          </div>

          <div class="col-md-6">
            <div class="form-group">
              <label class="col-sm-4" for="mesa_ayuda">Teléfono mesa de ayuda <span style="color:red;font-size:20px;">*</span></label>
              <div class="col-sm-6" >
                <input type="number" class="form-control" id="mesa_ayuda" name="mesa_ayuda" required>
              </div>
            </div>
          </div>

          <div class="col-md-6">
            <div class="form-group">
              <label class="col-sm-4" for="observaciones">Observaciones</label>
              <div class="col-sm-6" >
                <textarea rows='4' cols='50' class="form-control" id="observaciones" name="observaciones"></textarea>
              </div>
            </div>
          </div>

          <div class="box-footer col-xs-12 box-gris ">
            <button type="submit" class="btn btn-primary">Crear nuevo contrato</button>
            <a onclick="cerrar_modal()" style="margin-left:10px;" class="btn btn-primary">Cancelar</a>
          </div>
        </form>

      </div>
      <!-- Fin body -->

    </div>
  </div>
</section>
