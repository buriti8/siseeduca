<section>

  <div class="row" >

  <div class="col-md-12">

    <div class="box box-primary  box-gris">

      <div class="box-header with-border my-box-header">
        <h3 class="box-title"><strong>Nueva condición del alumno al finalizar el año anterior</strong></h3>
        <button onclick="cerrar_modal()" type="button" class="close" style="font-size: 40px; color:#34495E;"  aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button> <!-- Botón cerrar para volver a la vista sin necesidad de recargar la página -->
        <p style="color:red;">* Obligatorio</p>
      </div>

      <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">


      <div class="box-body">

        <form   action="{{ url('crear_conalunmant') }}"  method="post" id="f_crear_conalunmant" class="formentrada" >
          <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">

          <div class="col-md-12">
            <div class="form-group">
              <label class="col-sm-2" for="nombre">Código condición <span style="color:red;font-size:20px;">*</span> </label>
              <div class="col-xs-3" >
                <input type="number" class="form-control" id="id_conalunmant" name="id_conalunmant"  required   >
              </div>
            </div><!-- /.form-group -->
          </div><!-- /.col -->

          <div class="col-md-12">
            <div class="form-group">
              <label class="col-sm-2" for="nombre">Descripción <span style="color:red;font-size:20px;">*</span> </label>
              <div class="col-xs-3" >
                <input type="text" class="form-control" id="descripcion" name="descripcion"  required   >
              </div>
            </div><!-- /.form-group -->
          </div><!-- /.col -->

          <div class="box-footer col-xs-12 box-gris text-center ">
            <button  type="submit" onclick="return confirm('¿Desea guardar la información?')"  class="btn btn-primary">Enviar</button>
            <a onclick="cerrar_modal()" style="margin-left:15px;" class="btn btn-primary">Cancelar</a>
          </div>

        </form>

      </div>

    </div>

  </div>
</div>
</section>
