<section>

  <!--Estilo CSS para los mensajes correspondientes a la verificación de las contraseñas-->
  <style media="screen">

  .enviar{background:#d25959;box-shadow:none;color:white;margin-bottom:0;width:100px}

  .enviar:hover{text-decoration:underline}

  .confirmacion{background:#C6FFD5;border:1px solid green;}

  .confirmacion_plano{background:#C6FFD5;border:1px solid green;}

  .confirmacion_foto{background:#C6FFD5;border:1px solid green;}

  .negacion{background:#ffcccc;border:1px solid red}

  </style>

  <script language="JavaScript" type="text/JavaScript">

  //Función que permite validar que el área construida no sea mayor que el área del lote.
  $(document).ready(function() {
    //variables
    var pass1 = $('[name=arealote]');
    var pass2 = $('[name=areaconstruida]');
    var confirmacion = " ";
    var negacion = "El área construida no puede ser mayor que el área del lote";
    //oculto por defecto el elemento span
    var span = $('<span></span>').insertAfter(pass2);
    span.hide();

    function verificacion_arealote(){
      var valor1 = pass1.val();
      var valor2 = pass2.val();

      //muestro el span
      span.show().removeClass();

      //condiciones dentro de la función
      if(parseFloat(valor2)>parseFloat(valor1)){
        span.text(negacion).addClass('negacion');
        $('#Cbutton').attr("disabled", true);
      }
      if(parseFloat(valor2)==parseFloat(valor1)){
        $('#Cbutton').attr("disabled", false);
        span.text(confirmacion).removeClass("negacion");
      }
      if(parseFloat(valor1)>parseFloat(valor2)){
        span.text(confirmacion).removeClass("negacion");
        $('#Cbutton').attr("disabled", false);
      }
    }
    //ejecuto la función al soltar la tecla
    pass2.keyup(function(){
      verificacion_arealote();
    });
  });


  function validartipovia()
  {
    var tipovia=document.legal_form['tipovia[]'];
    var acumulador=0;

    for(i=0;i<tipovia.length;i++){
      if(tipovia[i].checked){
        acumulador=1;
      }
    }

    if (acumulador==0){
      alert('Seleccione el o los tipos de vía de acceso a la sede educativa.');
      return false;
    }

    document.legal_form.submitted.value='yes';
    return true;

  }

  </script>


  <div class="col-md-12">

    <div class="box box-primary  box-gris">

      <div class="box-header with-border my-box-header">
        <h3 class="box-title"><strong>Información legal de la sede educativa</strong></h3>
        <button onclick="cerrar_modal()" type="button" class="close" style="font-size: 40px;"  aria-label="Close">
          <span  aria-hidden="true">&times;</span><!-- Botón cerrar para volver a la vista sin necesidad de recargar la página -->
        </button>

        <p>En este espacio se especificará la información legal de las sedes educativas.</p>
        <p style="color:red;">* Obligatorio</p>
      </div><!-- /.box-header -->

      <div class="box-body">

        <form name="legal_form" method="post" id="f_crear_sedeslegal" class="formentrada"  >
          <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">

          <div class="row" style="padding:20px;">
            <div class="col-md-12">
              <div class="titleboxes" style="font-size:15px; text-align:center;">
              </div>
              <br>

              @role('secretarios')
              @include('partialsroles.secretario')
            @else
              @role('rectores')
              @include('partialsroles.rector')
            @else
              @include('partialsroles.filtro')
              @endrole
              @endrole

            </div>

            <div class="col-md-12">
              <div class="form-group">
                <label class="col-sm-2" for="nombre">Tipo de propietario de la sede educativa <span style="color:red;font-size:20px;">*</span></label>
                <div class="col-xs-3">
                  <select class="form-control" class="form-control" id="tipopropietario" name="tipopropietario" required >
                    <option></option>
                    <option>Pública</option>
                    <option>Privada</option>
                  </select>
                </div>
              </div><!-- /.form-group -->
            </div><!-- /.col -->


            <div class="col-md-12">
              <div class="form-group">
                <label class="col-sm-2" for="nombre">Modalidad de sede educativa <span style="color:red;font-size:20px;">*</span></label>
                <div class="col-xs-3">
                  <select class="form-control" id="modalidad" name="modalidad" required >
                    <option></option>
                    <Option>Arrendamiento</Option>
                    <Option>Comodato</Option>
                    <Option>Posesión</Option>
                    <Option>Propio</Option>
                    <Option>Propiedad del municipio</Option>
                    <Option>Baldíos</Option>
                    <Option>Resguardo Indígena</Option>
                    <Option>Cobertura contratada</Option>
                    <Option>Compraventa</Option>
                    <Option>Donación</Option>
                    <Option>Privada</Option>
                    <Option>Sin escritura</Option>
                    <Option>Otro</Option>
                  </select>
                </div>
              </div><!-- /.form-group -->
            </div><!-- /.col -->

            <div class="col-md-12">
              <div class="form-group">
                <label class="col-sm-2" for="nombre">Área del lote (M²) <span style="color:red;font-size:20px;">*</span></label>
                <div class="col-xs-3">
                  <input type="number" step="any"  class="form-control" id="arealote" name="arealote" min="0" required >
                </div>
              </div><!-- /.form-group -->
            </div><!-- /.col -->

            <div class="col-md-12">
              <div class="form-group">
                <label class="col-sm-2" for="nombre">Área construida (M²) <span style="color:red;font-size:20px;">*</span></label>
                <div class="col-xs-3">
                  <input type="number" step="any" class="form-control" id="areaconstruida" name="areaconstruida" min="0" required >
                </div>
              </div><!-- /.form-group -->
            </div><!-- /.col -->

            <div class="col-md-12">
              <div class="form-group">
                <label class="col-sm-2" for="nombre">¿Posee plano de la sede educativa? <span style="color:red;font-size:20px;">*</span></label>
                <div class="col-xs-3">
                  <select class="form-control" class="form-control" id="plano" name="plano" >
                    <option></option>
                    <option>Sí</option>
                    <option>No</option>
                  </select>
                </div>
                Si su respuesta es sí; Por favor enviar el plano al siguiente correo electrónico: david.franco@antioquia.gov.co
              </div><!-- /.form-group -->
            </div><!-- /.col -->

            <div class="col-md-12">
              <div class="form-group">
                <label class="col-sm-2" for="nombre">¿Posee foto de la sede educativa? <span style="color:red;font-size:20px;">*</span></label>
                <div class="col-xs-3">
                  <select class="form-control" class="form-control" id="foto" name="foto">
                    <option></option>
                    <option>Sí</option>
                    <option>No</option>
                  </select>
                </div>
                Si su respuesta es sí; Por favor enviar la foto al siguiente correo electrónico: david.franco@antioquia.gov.co
              </div><!-- /.form-group -->
            </div><!-- /.col -->

            <hr style="border-color:white;" />
            <div class="col-xs-12" style="border-bottom:1px solid #337ab7; border-top:1px solid #337ab7; padding:0.8em; margin-top:0.5em; margin-bottom:0.5em; text-align:center;">
              <strong>Coordenadas de la sede educativa</strong></div>
              <div class="col-md-12">
                <div class="form-group">
                  <label style="margin-top:20px;" class="col-sm-2" for="nombre">Longitud de la sede educativa <span style="color:red;font-size:20px;">*</span></label>
                  <div class="col-xs-3" >
                    <div oninput="coordenadas.value=(parseInt(valor1.value)+(parseInt(valor2.value)/60)+(parseInt(valor3.value)/3600))*parseInt(-1)">
                      <input style="margin-top:20px;"  type="number" id="valor1"  placeholder="Grados" max="90" title="El valor debe ser inferior o igual a 90"> °
                      <input type="number" id="valor2"  placeholder="Minutos" max="60" title="El valor debe ser inferior o igual a 60"> '
                      <input type="number" id="valor3"  placeholder="Segundos" max="60" title="El valor debe ser inferior o igual a 60"> "
                    </div>
                    <input type="text" class="form-control" id="coordenadas" name="coordenadas" step="0.000000000000001" placeholder="Longitud en decimal"  readonly="readonly"  >
                  </div>

                </div><!-- /.form-group -->
              </div><!-- /.col -->

              <div class="col-md-12">
                <div class="form-group">
                  <label class="col-sm-2" for="nombre">Latitud de la sede educativa <span style="color:red;font-size:20px;">*</span></label>
                  <div class="col-xs-3" >
                    <div oninput="latitud.value=parseInt(valor4.value)+(parseInt(valor5.value)/60)+(parseInt(valor6.value)/3600)">
                      <input type="number" id="valor4"  placeholder="Grados" maxlength="90" title="El valor debe ser inferior o igual a 90"> °
                      <input type="number" id="valor5"  placeholder="Minutos" maxlength="60" title="El valor debe ser inferior o igual a 60"> '
                      <input type="number" id="valor6"  placeholder="Segundos" maxlength="60" title="El valor debe ser inferior o igual a 60"> "
                    </div>
                    <input style="margin-bottom:20px;"  type="text" class="form-control" id="latitud" name="latitud" step="0.000000000000001" placeholder="Latitud en decimal"  readonly="readonly" >
                  </div>
                </div><!-- /.form-group -->
              </div><!-- /.col -->


              <div class="col-xs-12" style="border-bottom:1px solid #337ab7; border-top:1px solid #337ab7; padding:0.8em; margin-top:0.5em; margin-bottom:0.5em; text-align:center;">
                <strong>Información adicional</strong></div>

                <div class="row" style="padding:20px;">
                  <div class="col-md-12">
                    <h5>Tipo de vía de acceso a la sede educativa<span style="color:red;font-size:20px;">*</span><strong>:</strong></h5>
                    <div class="col-xs-3" style="margin-left:242px; margin-top:10px">
                      <label class="checkbox-formulario">
                        <input type="checkbox" name="tipovia[]"  value="Camino de herradura" title="8" value="1">
                        <span class="label-text"><b>1) Camino de herradura</b></span>
                      </label>
                    </div>
                    <div class="col-xs-3" style="margin-left:15px; margin-top:10px">
                      <label class="checkbox-formulario">
                        <input type="checkbox" name="tipovia[]"  value="Carretera destapada" title="1" value="1">
                        <span class="label-text"><b>2) Carretera destapada</b></span>
                      </label>
                    </div><div class="col-xs-3" style="margin-left:15px; margin-top:10px">
                      <label class="checkbox-formulario">
                        <input type="checkbox" name="tipovia[]" value="Vía pavimentada"  title="2" value="1">
                        <span class="label-text"><b>3) Vía pavimentada</b></span>
                      </label>
                    </div>
                    <div class="col-xs-3" style="float: left; margin-left:242px; margin-top:10px">
                      <label class="checkbox-formulario">
                        <input type="checkbox" name="tipovia[]" value="Placa huella" title="3" value="1">
                        <span class="label-text"><b>4) Placa huella</b></span>
                      </label>
                    </div>
                    <div class="col-xs-3" style="margin-left:15px; margin-top:10px">
                      <label class="checkbox-formulario">
                        <input type="checkbox" name="tipovia[]" value="Fluvial" title="4" value="1">
                        <span class="label-text"><b>5) Fluvial</b></span>
                      </label>
                    </div>
                  </div>
                </div>

                <div class="col-md-12">
                  <div class="form-group">
                    <h5 style="margin-top:20px;" class="col-sm-2" for="nombre">Distancia de la sede educativa desde la cabecera municipal<span style="color:red;font-size:20px;">*</span></h5>
                    <div class="col-xs-3" >
                      <input placeholder="Kilometros" style="margin-top:20px;" type="number" class="form-control" id="distancia" name="distancia" required >
                    </div>
                  </div><!-- /.form-group -->
                </div><!-- /.col -->

                <div class="box-footer col-xs-12 box-gris ">
                  <center>
                    <button id="Cbutton" type="submit" onclick="this.form.action='crear_sedeslegal'; return validartipovia()"  class="btn btn-primary">Enviar</button><!-- Botón para registrar la información de una nueva sede educativa -->
                    <a onclick="cerrar_modal()" style="margin-left:15px;" class="btn btn-primary">Cancelar</a><!-- Botón para cancerlar el ingreso de una nueva sede educativa
                      y volver a la vista sin necesidad de recargar la página -->
                    </button>
                  </center>
                </div>
              </form>

            </div>
          </div>
        </div>
      </section>
