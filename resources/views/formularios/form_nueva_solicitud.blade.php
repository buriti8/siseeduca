@extends('layouts.app')

@section('htmlheader_title')
Nueva necesidad
@endsection

@section('main-content')
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script type="text/javascript">

$(document).on('change','input[type="file"]',function(){
	// this.files[0].size recupera el tamaño del archivo
	// alert(this.files[0].size);

	var fileName = this.files[0].name;
	var fileSize = this.files[0].size;

	if(fileSize > 5000000){
		alert('El archivo no debe superar los 5 megabytes (MB).');
		this.value = '';
		this.files[0].name = '';
	}else{
		// recuperamos la extensión del archivo
		var ext = fileName.split('.').pop();

		// console.log(ext);
		switch (ext) {
			case 'jpg':
			case 'jpeg':
			case 'png':
			case 'pdf': break;
			default:
				alert('El archivo no tiene la extensión adecuada.');
				this.value = ''; // reset del valor
				this.files[0].name = '';
		}
	}
});

</script>

<!-- Inicio contenido principal -->
<section  id="contenido_principal">
    <!-- Principal -->
    <div class="box box-primary">
        <div class="box-header">
        </div><!-- /.box-header -->

        <!-- Interior -->
        <div class="box box-primary" style="border: 1px solid #3c8dbc !important;" >
           <div class="titleboxes" style="font-size:20px;text-align:center;">Diagnósticos infraestructura tecnológica</div>
           <p style="color:red;">* Obligatorio</p>

            <form action="{{ url('crear_solicitud') }}" method="post" id="frmsolicitud" enctype="multipart/form-data">
                @csrf

                <div class="row" style="padding:20px;">
                    <!-- Inicio -->
                    <div class="col-md-12">
                        <div class="titleboxes" style="font-size:15px; text-align:center;"></div>
                        <div class="col-md-12" style="padding-top:15px;padding-bottom:15px;">
													<div class="row" style="padding:20px;">
								            <!-- Inicio -->
								            <div class="col-md-12">
															<div class="titleboxes" style="font-size:15px; text-align:center;"></div>
              <div class="col-md-12" style="padding-top:15px;padding-bottom:15px;">
                <div class="form-group">
                  <label>Sede <span style="color:red;font-size:20px;">*</span></label>

									<div class="col-xs-12" style="border-bottom:1px solid #337ab7; border-top:1px solid #337ab7; padding:0.8em; margin-top:0.5em; margin-bottom:0.5em; text-align:center;">
										@foreach ($nombres as $nombre)

											<h5><strong>Ingrese la necesidad sede educativa &nbsp;&nbsp; {{$nombre->nombre_sede}} &nbsp;&nbsp; - &nbsp;&nbsp; Código dane: {{$nombre->codigo_sede}}</strong> <span style="color:red;font-size:20px;">*</span></h5>

										@endforeach

			            </div>

			            @foreach ($nombres as $nombre)
			              <input type="hidden" name="sede_id" value="{{$nombre->codigo_sede}}">
			            @endforeach

                </div>
              </div>
            </div>

                    <div class="col-md-12">

					            <!--<div class="table-responsive" style="padding:2em;">
					              <table class="table table-hover table-striped" cellspacing="0" width="100%">
					                <thead>
					                  <tr>
					                    <th>Tipo de solicitud</th>
					                    <th>Tipo de dispositivo</th>
					                    <th>Cantidad</th>
					                    <th>Uso</th>
					                    <th>Acción</th>
					                  </tr>
					                </thead>
					                <tbody  id="tdReqEspacioss">
					                </tbody>
					              </table>
					            </div>-->
                        <div class="titleboxes" style="font-size:15px; text-align:center"></div>
                        <div class="col-md-12" style="padding:0px;">
                            <div class="col-md-3" style="padding-top:15px;padding-bottom:15px;">
                                <div class="tipo_solicitud">
                                    <label>Seleccione el tipo de solicitud <span style="color:red;font-size:20px;">*</span></label>
                                    <select id="tipo_solicitud" name="tipo_solicitud" class="form-control" required>
                                        <option value="1">Dispositivo</option>
                                        <option value="2">Conectividad</option>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-12" style="padding:0px;">
                            <div class="col-md-3">
                                <div class="div_dispositivos" style="padding-top:2px;padding-bottom:10px;">
                                    <div class="div_dispositivos">
                                        <label>Seleccione el tipo de dispositivo <span style="color:red;font-size:20px;">*</span></label>
                                        <select id="tipo_id" name="tipo_id" class="form-control">
                                            @foreach($tipos as $tipo)
                                                <option value="{{ $tipo->id }}">{{ $tipo->nombre }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group "  >
                          <label for="cantidad_d">Cantidad<span style="color:red;font-size:20px;">*</span></label>
                          <input name="cantidad_d" id="cantidad_d" type="number"   class="archivo form-control" maxlength="2" min="1" max="5000"/>
                        </div>




                                </div>
                                <div class="div_conectividad" style="padding-top:2px;padding-bottom:10px;">
                                <div class="div_conectividad">
                                  <label>Seleccione el tipo de uso<span style="color:red;font-size:20px;">*</span></label>
                                  <select id="tipo_uso" name="tipo_uso" class="form-control"  >
																	<option></option>
                                  <option>Educativo</option>

                                  </select>


                                </div>
                              </div>
                              </div>

                            </div>



                    </div>
										<!--<div class="form-group col-xs-12" >
			                <input type="button" name="anadirEfii" class="btn btn-primary" id="anadirEfii"  value="Añadir" style="float: right;">
			              </div>-->

                    <div class="col-md-12">
                        <div class="titleboxes" style="font-size:15px; text-align:center;"></div>
                        <div class="col-md-12" style="padding-top:15px;padding-bottom:15px;">
                        <div class="form-group" >
                            <label>Debe ingresar el motivo de la solicitud <span style="color:red;font-size:20px;">*</span></label>
                            <textarea rows="4" cols="50" type="text" id="descripcion" name="descripcion" class="form-control" maxlength=230 required></textarea>
                        </div>
                        </div>
                    </div>

                    <div class="col-md-12">
                        <div class="titleboxes" style="font-size:15px; text-align:center;"></div>
                        <div class="col-md-12" style="padding-top:15px;padding-bottom:15px;">
                            <div class="form-group">
                                <label><strong>Nota:el archivo no debe superar los 5 megabytes (MB).</strong></label>
                                <label>Agregar archivo <span class="label label-primary">PDF</span> / <span class="label label-primary">Imagen</span></label>
                                <input type="file" id="solicitud_fisica" name="solicitud_fisica" maxlength=200>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12">
                        <div class="titleboxes" style="font-size:15px; text-align:center;"></div>
                    </div>
                    <!-- Fin -->

                    <div class="col-xs-12" style="margin-top:15px;">
                      <center>
                        <button onclick="return confirm('Recuerda:Esta necesidad esta sujeta a verificación técnica y financiera.')" type="submit" class="btn btn-primary">Enviar</button>
                        <a href="{{ url('home') }}" class="btn btn-info">Regresar</a>
                      </center>
                    </div>

                </div>
            </form>
        </div>
        <!-- Fin interior -->
    </div>
     <!-- Principal -->

</section>
<!-- Fin contenido principal -->


@endsection
