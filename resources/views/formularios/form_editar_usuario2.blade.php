
<section >

  <div class="col-md-12">

    <div class="box box-primary box-gris">
      <div class="box-header with-border my-box-header">
        <h3 class="box-title"><strong>Editar información</strong></h3>
        <button onclick="cerrar_modal()" type="button" class="close" style="font-size: 40px;"  aria-label="Close">
          <span  aria-hidden="true">&times;</span><!-- Botón cerrar para volver a la vista sin necesidad de recargar la página -->
        </button>
        <p style="color:red;">* Obligatorio</p>
      </div><!-- /.box-header -->

      <div id="notificacion_E2" ></div>
      <div class="box-body">

        <form   action="{{ url('editar_usuario2') }}"  method="post" id="f_editar_usuario2"  class="formentrada"  >
          <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
          <input type="hidden" name="id_usuario" value="{{ $usuario->id }}">

          <div class="col-md-6">
            <div class="form-group">
              <label class="col-sm-2" for="nombre">Nombres <span style="color:red;font-size:20px;">*</span></label>
              <div class="col-sm-10" >
                <input type="text" class="form-control" id="nombres" name="nombres"  value="{{ $usuario->name }}"  required   >
              </div>
            </div><!-- /.form-group -->
          </div><!-- /.col -->

          <div class="col-md-6">
            <div class="form-group">
              <label class="col-sm-2" for="apellido">Apellidos <span style="color:red;font-size:20px;">*</span></label>
              <div class="col-sm-10" >
                <input type="text" class="form-control" id="apellidos" name="apellidos"   value="{{ $usuario->lastname }}" required >
              </div>
            </div><!-- /.form-group -->
          </div><!-- /.col -->

          <div class="col-md-6">
            <div class="form-group">
              <label class="col-sm-2" for="celular">Teléfono <span style="color:red;font-size:20px;">*</span></label>

              <div class="col-sm-10" >
                <input type="text" class="form-control" id="telefono" name="telefono"  value="{{ $usuario->phone  }}" required >
              </div>

            </div><!-- /.form-group -->
          </div><!-- /.col -->

          <div class="box-footer col-xs-12 box-gris ">
            <center>
            <button style="margin-top:10px" type="submit" class="btn btn-primary">Actualizar</button>
          </center>
          </div>

        </form>
      </div>

    </div>


    <div class="box box-primary box-gris">
      <div class="box-header with-border my-box-header">
        <h3 class="box-title"><strong>Editar datos de acceso al sistema</strong></h3>
      </div><!-- /.box-header -->

      <div id="notificacion_E3" ></div>
      <div class="box-body">

        <form  action="{{ url('editar_acceso') }}" method="post" id="f_editar_acceso"  class="formentrada"  >
          <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
          <input type="hidden" name="id_usuario" value="{{ $usuario->id }}">

          <div class="col-md-6">
            <div class="form-group"> <!-- Edición o cambio de datos de ingreso al sistema por parte del usuario -->
              <label style="margin-top:10px"  for="email">Correo electrónico <span style="color:red;font-size:20px;">*</span></label>
              <input type="text" class="form-control" id="email" name="email"  value="{{ $usuario->email  }}" required >
              <label style="margin-top:10px" for="email">Nueva contraseña <span style="color:red;font-size:20px;">*</span></label>
              <input type="password" class="form-control" id="password" name="password"  required >
              <label style="margin-top:10px" for="email">Confirmar nueva contraseña <span style="color:red;font-size:20px;">*</span></label>
              <input type="password" class="form-control" id="password2" name="password2"  required >
            </div>
          </div>

          <div class=" col-xs-12 box-gris ">
            <center>
            <button style="margin-top:20px" id="Cbutton"   type="submit"  class="btn btn-primary">Actualizar</button>
            <button onclick="cerrar_modal()" style="margin-top:20px; margin-left:10px;" id="Cbutton"   type="submit"  class="btn btn-primary">Cancelar</button>
          </center>
          </div>
        </form>
      </div>
    </div>
  </div>

  <!--Estilo CSS para los mensajes correspondientes a la verificación de las contraseñas-->
  <style media="screen">

  .enviar{background:#d25959;box-shadow:none;color:white;margin-bottom:0;width:100px}

  .enviar:hover{text-decoration:underline}

  .confirmacion{background:#C6FFD5;border:1px solid green;}

  .negacion{background:#ffcccc;border:1px solid red}

  </style>

  <script language="JavaScript" type="text/JavaScript">
  //Función que permite la verificación de las contraseñas ingresasdas por el usuario.
  $(document).ready(function() {
    //variables
    var pass1 = $('[name=password]');
    var pass2 = $('[name=password2]');
    var confirmacion = "Las contraseñas coinciden";
    var negacion = "No coinciden las contraseñas";
    var vacio = "Las contraseñas no pueden estar vacías";
    //oculto por defecto el elemento span
    var span = $('<span></span>').insertAfter(pass2);
    span.hide();
    //función que comprueba las dos contraseñas
    function coincidePassword(){
      var valor1 = pass1.val();
      var valor2 = pass2.val();
      //muestro el span
      span.show().removeClass();
      //condiciones dentro de la función
      if(valor1 != valor2){
        span.text(negacion).addClass('negacion');
        $('#Cbutton').attr("disabled", true);
      }
      if(valor1.length==0 || valor1==""){
        span.text(vacio).addClass('vacio');
        $('#Cbutton').attr("disabled", true);

      }
      if(valor1.length!=0 && valor1==valor2){
        span.text(confirmacion).removeClass("negacion").addClass('confirmacion');
        $('#Cbutton').attr("disabled", false);
      }
    }
    //ejecuto la función al soltar la tecla
    pass2.keyup(function(){
      coincidePassword();
    });
  });
  </script>

</section>
