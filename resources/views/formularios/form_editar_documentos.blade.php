<section  >

  <script type="text/javascript">


  $(document).on('change','input[type="file"]',function(){
    // this.files[0].size recupera el tamaño del archivo
    // alert(this.files[0].size);

    var fileName = this.files[0].name;
    var fileSize = this.files[0].size;

    if(fileSize > 5000000){
      alert('El archivo no debe superar los 5 megabytes (MB).');
      this.value = '';
      this.files[0].name = '';
    }else{
      // recuperamos la extensión del archivo
      var ext = fileName.split('.').pop();

      // console.log(ext);
      switch (ext) {
        case 'jpg':
        case 'jpeg':
        case 'png':
        case 'pdf': break;
        default:
        alert('El archivo no tiene la extensión adecuada.');
        this.value = ''; // reset del valor
        this.files[0].name = '';
      }
    }

  });

</script>

<form  action="{{ url('editar_documentos') }}" id="f_crear_sedesaulas"   method="post"  files="true"  enctype="multipart/form-data">

  <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
  @foreach ($datos as $dato)
    <input type="hidden" name="id" value="{{$dato->id}}">
  @endforeach

  <div class="col-md-12">
    <div class="box box-primary  box-gris">
      <div class="box-header with-border my-box-header">
        @if($dato->comite_junta == 'Comité TIC')
          <h4 class="box-title"><strong>Editar documentación del {{$dato->comite_junta}} del municipio de {{$dato->nombre_municipio}}</h4></strong>
        @elseif ($dato->comite_junta == 'Comité de Cupos')
          <h3 class="box-title"><strong>Editar documentación del {{$dato->comite_junta}} del municipio de {{$dato->nombre_municipio}}</h3></strong>
        @elseif ($dato->comite_junta == 'JUME')
          <h3 class="box-title"><strong>Editar documentación de la Junta municipal de educación {{$dato->comite_junta}} del municipio {{$dato->nombre_municipio}}</h3></strong>
        @elseif ($dato->comite_junta == 'Enlace TIC')
          <h3 class="box-title"><strong>Editar documentación del {{$dato->comite_junta}} del municipio de {{$dato->nombre_municipio}}</h3></strong>
        @endif
        <button onclick="cerrar_modal()" type="button" class="close" style="font-size: 40px;"  aria-label="Close">
          <span  aria-hidden="true">&times;</span><!-- Botón cerrar para volver a la vista sin necesidad de recargar la página -->
        </button>
        <p style="color:red;">* Obligatorio</p>
      </div><!-- /.box-header -->
      <div class="col-md-12" style="font-size:10px; text-align:center;">
      </div>
      <br>



      <div class="col-xs-12" style="border-bottom:1px solid #337ab7; border-top:1px solid #337ab7; padding:0.8em; margin-top:20px; margin-bottom:10px; text-align:center;">
        <strong>Actualizar domuento</strong></div>

        <div class="row" style="padding:20px;">
          <div style="margin-top:20px;"  class="col-md-12">
            <div class="form-group">
              <label class="col-sm-3" for="nombre">Documento <span style="color:red;font-size:20px;">*</span></label>
              <div class="col-xs-3" >
                @foreach ($datos as $dato)
                  <input type="text" class="form-control" name="selcomite" pattern="[a-zA-ZñÑáéíóúÁÉÍÓÚ\s]+" title="Solo ingrese texto" value="{{$dato->comite_junta}}" readonly required>
                @endforeach
              </div>
            </div><!-- /.form-group -->
          </div><!-- /.col -->

          <div class="col-md-12">
            <div class="form-group">
              <label class="col-sm-3" for="nombre">Número de acta, decreto o carta <span style="color:red;font-size:20px;">*</span></label>
              <div class="col-xs-3">
                <input type="text" class="form-control" id="numero_documento" value="{{$dato->numero_documento}}" name="numero_documento" required>
              </div>
            </div><!-- /.form-group -->
          </div><!-- /.col -->

          <div class="col-md-12">
            <div class="form-group">
              <label class="col-sm-3" for="nombre">Fecha de acta, decreto o carta <span style="color:red;font-size:20px;">*</span></label>
              <div class="col-xs-3">
                <input type="date" class="form-control" id="fecha_documento" value="{{$dato->fecha_documento}}"  name="fecha_documento" step="1" min="1900-01-01" max="2019-12-31" required >
              </div>
            </div><!-- /.form-group -->
          </div><!-- /.col -->


          <div class="col-md-12">
            <div class="form-group">
              <label class="col-sm-3">Cambiar documento <span class="label label-primary">PDF</span> anexado, el archivo no debe superar los 5 megabytes:</label>
              <div class="col-xs-3" >
                <input name="file" id="file" type="file" files="true" enctype="”multipart/form-data”" accept="application/pdf,image/webp,image/apng,image/*,*/*;q=0.8" class="archivo form-control" required>
              </div>
            </div><!-- /.form-group -->
          </div><!-- /.col -->


          <div class="box-footer col-xs-12 box-gris ">
            <center>
              <button  type="submit" onclick="return confirm('¿Desea guardar la información?')"  class="btn btn-primary">Enviar</button><!-- Botón para registrar la información -->
              <a onclick="cerrar_modal()" style="margin-left:15px;" class="btn btn-primary">Cancelar</a><!-- Botón para cancerlar el ingreso de una nueva sede educativa
                y volver a la vista sin necesidad de recargar la página -->
              </center>
            </div>

          </div>
        </div>
      </div>

    </form>
  </section>
