<section >



<div class="row" >

<div class="col-md-12">


  <div class="box box-primary box-gris">

      <div class="box-header with-border my-box-header">
        <h3 class="box-title"><strong>Editar Rector Director </strong></h3>
      </div><!-- /.box-header -->

      <div id="notificacion_E2" ></div>
      <div class="box-body">


          <form   action="{{ url('editar_rectordirector') }}"  method="post" id="f_editar_rectordirector"  class="formentrada"  >
                <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                <input type="hidden" name="id_rectordirector" value="{{ $rectordirector->id }}">

          <div class="col-md-6">
              <div class="form-group">
                    <label class="col-sm-2" for="nombre">CC<span style="color:red;font-size:20px;">*</span> </label>
                    <div class="col-sm-10" >
                      <input type="number" class="form-control" id="cc" name="cc"  value="{{ $rectordirector->CC }}"  required   >
                       </div>
              </div><!-- /.form-group -->
          </div><!-- /.col -->

          <div class="col-md-6">
              <div class="form-group">
                    <label class="col-sm-2" for="nombre">Nit<span style="color:red;font-size:20px;">*</span> </label>
                    <div class="col-sm-10" >
                      <input type="text" class="form-control" id="nit" name="nit"  value="{{ $rectordirector->Nit }}"  required   >
                       </div>
              </div><!-- /.form-group -->
          </div><!-- /.col -->


          <div class="col-md-6">
              <div class="form-group">
                    <label class="col-sm-2" for="nombre">Nombre <span style="color:red;font-size:20px;">*</span> </label>
                    <div class="col-sm-10" >
                      <input type="text" class="form-control" id="nombre" name="nombre"  value="{{ $rectordirector->Nombre }}"  required   >
                       </div>
              </div><!-- /.form-group -->
          </div><!-- /.col -->


          <div class="col-md-6">
              <div class="form-group">
                    <label class="col-sm-2" for="nombre">Telefono Fijo<span style="color:red;font-size:20px;">*</span> </label>
                    <div class="col-sm-10" >
                      <input type="text" class="form-control" id="telefonofijo" name="telefonofijo"  value="{{ $rectordirector->TelefonoFijo }}"  required   >
                       </div>
              </div><!-- /.form-group -->
          </div><!-- /.col -->


          <div class="col-md-6">
              <div class="form-group">
                    <label class="col-sm-2" for="nombre">Página Web<span style="color:red;font-size:20px;">*</span> </label>
                    <div class="col-sm-10" >
                      <input type="text" class="form-control" id="paginaweb" name="paginaweb"  value="{{ $rectordirector->PaginaWeb }}"  required   >
                       </div>
              </div><!-- /.form-group -->
          </div><!-- /.col -->

          <div class="col-md-6">
              <div class="form-group">
                    <label class="col-sm-2" for="nombre">Email<span style="color:red;font-size:20px;">*</span> </label>
                    <div class="col-sm-10" >
                      <input type="text" class="form-control" id="email" name="email"  value="{{ $rectordirector->Email }}"  required   >
                       </div>
              </div><!-- /.form-group -->
          </div><!-- /.col -->


          <div class="col-md-12">
             <div class="form-group">
            <label class="col-sm-2" for="apellido">Código Dane Municipio <span style="color:red;font-size:20px;">*</span> </label>
              <div class="col-sm-8" >

            <select  type="text" class="form-control" id="codigo_dane_municipio" name="codigo_dane_municipio" required >
            <option>--- Seleccione un Código DANE Municipio--- </option>
            @foreach($municipios as $municipio)
            <option value="{{ $municipio->CodigoDaneMunicipio }}">{{ $municipio->CodigoDaneMunicipio }}</option>
            @endforeach

                </select>
                  </div>
          </div><!-- /.form-group -->
          </div><!-- /.........................................col -->


           <div class="col-md-12">
               <div class="form-group">
              <label class="col-sm-2" for="apellido">Nombre Del Municipio donde se encuentra ubicado <span style="color:red;font-size:20px;">*</span> </label>
                <div class="col-sm-8" >

              <select  type="text" class="form-control" id="nombre_municipio" name="nombre_municipio" required >
              <option>--- Seleccione el nombre del Municipio--- </option>
              @foreach($municipios as $municipio)
              <option value="{{ $municipio->NombreMunicipio}}">{{ $municipio->NombreMunicipio }}</option>
              @endforeach

                  </select>
                    </div>
            </div><!-- /.form-group -->
          </div><!-- /...................................col -->



          <div class="box-footer col-xs-12 box-gris ">
                <button type="submit" class="btn btn-primary">Actualizar</button>
<a onclick="cerrar_modal()" style="margin-left:15px;" class="btn btn-primary">Cancelar</a>
          </div>

          </form>

      </div>

    </div>

  </div>
</div>
</section>
