<section >

<div class="row" >

<div class="col-md-12">


  <div class="box box-primary box-gris">



<div class="col-md-12">
      @foreach($nombres as $nombre)
      <h5><strong>{{$nombre->nombre_municipio}} &nbsp;&nbsp; - &nbsp;&nbsp; {{$nombre->nombre_establecimiento}} &nbsp;&nbsp; - &nbsp;&nbsp; {{$nombre->nombre_sede}}  &nbsp;&nbsp; - &nbsp;&nbsp; {{$nombre->direccion}}</strong></h5>
      @endforeach

      <p style="color:red;">* Obligatorio</p>
      <p><strong>Nota:</strong> Recuerde no reportar varias veces el mismo servicio de conectividad.</p>
</div>





      <div id="notificacion_E2" ></div>
      <div class="box-body">


          <form   action="{{ url('editar_sedesconectividad') }}"  method="post" id="f_editar_sedesconectividad"  class="formentrada"  >
                <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                <input type="hidden" name="id_sedesconectividad" value="{{ $sedesconectividad->id }}">

                <div class="col-md-12">
                    <div class="form-group">
                          <label class="col-sm-2" for="nombre">Dane de la sede <span style="color:red;font-size:20px;">*</span></label>
                          <div class="col-xs-3" >
                            <input type="number" class="form-control" id="sede_id" name="sede_id"  value="{{ $sedesconectividad->Codigodanesede }}" readonly="readonly" />
                             </div>
                    </div><!-- /.form-group -->
                </div><!-- /.col -->


          <div class="col-md-12">
              <div class="form-group">
                    <label class="col-sm-2" for="nombre">¿Quién paga la conectividad?<span style="color:red;font-size:20px;">*</span></label>
                    <div class="col-xs-3" >
                  <select class="form-control" id="programaorigen" name="programaorigen"  >
                    <option>{{ $sedesconectividad->Programaorigendelosrecursos }}</option>
                    <option>Ministerio Tic</option>
                    <option>Municipio</option>
                    <option>Establecimiento educativo</option>
                    </select>
                    </div>
              </div><!-- /.form-group -->
          </div><!-- /.col -->

          <div class="col-md-12">
              <div class="form-group">
                    <label class="col-sm-2" for="nombre">Numero del contrato</label>
                    <div class="col-xs-3" >
                      <input type="number" class="form-control" id="numerocontrato" name="numerocontrato"  value="{{ $sedesconectividad->Numerodecontrato }}"   >
                       </div>
              </div><!-- /.form-group -->
          </div><!-- /.col -->



          <div class="col-md-12">
              <div class="form-group">
                    <label class="col-sm-2" for="nombre">Operador prestador del servicio<span style="color:red;font-size:20px;">*</span></label>
                    <div class="col-xs-3" >
                      <input type="text" pattern="[a-zA-ZñÑáéíóúÁÉÍÓÚ\s]+" title="Solo ingrese texto" class="form-control" id="operador" name="operador"   value="{{ $sedesconectividad->Operador }}"   >
                       </div>
              </div><!-- /.form-group -->
          </div><!-- /.col -->

          <div class="col-md-12">
              <div class="form-group">
                    <label class="col-sm-2" for="nombre">Ancho de banda de la conexión (MB)<span style="color:red;font-size:20px;">*</span></label>
                    <div class="col-xs-3" >
                      <input type="number" class="form-control" id="anchobanda" name="anchobanda" min="0" max="500" value="{{ $sedesconectividad->Anchodebanda }}"    >

                       </div>
              </div><!-- /.form-group -->
          </div><!-- /.col -->

          <div class="col-md-12">
              <div class="form-group">
                    <label class="col-sm-2" for="nombre">Fecha en la que inicio el contrato<span style="color:red;font-size:20px;">*</span></label>
                    <div class="col-xs-3" >
                    <input type="date" class="form-control" id="fechainicio" name="fechainicio" step="1" min="1900-01-01" max="2040-12-31" value="{{ $sedesconectividad->Fechainicioservicio }}"     >

                       </div>
              </div><!-- /.form-group -->
          </div><!-- /.col -->

          <div class="col-md-12">
              <div class="form-group">
                    <label class="col-sm-2" for="nombre">Fecha que finaliza el contrato</label>
                    <div class="col-xs-3" >
                      <input type="date" class="form-control" id="fechafin" name="fechafin" step="1" min="1900-01-01" max="2040-12-31" value="{{ $sedesconectividad->Fechafinservicio }}"   >

                       </div>
              </div><!-- /.form-group -->
          </div><!-- /.col -->

          <div class="col-md-12">
              <div class="form-group">
                    <label class="col-sm-2" for="nombre">Estado de la conexión<span style="color:red;font-size:20px;">*</span></label>
                    <div class="col-xs-3" >
                    <select class="form-control" class id="estado" name="estado" required   >
                      <option> {{ $sedesconectividad->Estado }}</option>
                      <option>Activo</option>
                      </select>
                       </div>
              </div><!-- /.form-group -->
          </div><!-- /.col -->

          <div class="col-md-12">
              <div class="form-group">
                    <label class="col-sm-2" for="nombre">Tipo de servicio</label>
                    <div class="col-xs-3" >
                    <select class="form-control" class id="servicio" name="servicio"   >
                      <option> {{ $sedesconectividad->Servicio }}</option>
                      <option>ADSL</option>
                      <option>Fibra óptica</option>
                      <option>Fibra + coaxial</option>
                      <option>LMDS</option>
                      <option>PLC</option>
                      <option>WIMAX</option>
                      <option>GSM</option>
                      <option>GPRS</option>
                      <option>UMTS</option>
                      <option>HSDPA</option>
                      <option>Satélite</option>
                      </select>
                       </div>
              </div><!-- /.form-group -->
          </div><!-- /.col -->

          <div class="col-md-12">
              <div class="form-group">
                    <label class="col-sm-2" for="nombre">Uso principal del servicio<span style="color:red;font-size:20px;">*</span></label>
                    <div class="col-xs-3" >
                    <select class="form-control" class id="uso" name="uso" required   >
                      <option> {{ $sedesconectividad->Uso }}</option>
                      <option>Administrativo</option>
                      <option>Educativo</option>
                      </select>
                       </div>
              </div><!-- /.form-group -->
          </div><!-- /.col -->




          <div class="box-footer col-xs-12 box-gris ">
            <center>
                <button type="submit" class="btn btn-primary">Actualizar</button>
                <a onclick="cerrar_modal()" style="margin-left:10px;" class="btn btn-primary">Cancelar</a>
            </center>
          </div>

          </form>

      </div>

    </div>

  </div>
</div>
</section>
