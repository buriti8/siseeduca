<section >

  <!--Función para validar que el usuario elija como minímo una opción en los checkbox -->
  <script language="JavaScript">
  function validar()
  {
    var tipoenergia=document.some_form['tipoenergia[]'];
    var acumulador=0;

    for(i=0;i<tipoenergia.length;i++){
      if(tipoenergia[i].checked){
        acumulador=1;
      }
    }

    if (acumulador==0){
      alert('Si su sede educativa cuenta con energía; seleccione el o los tipos de energia. Sino, debe seleccionar la opción: "No tiene".');
      return false;
    }

    document.some_form.submitted.value='yes';
    return true;

  }

  $('#group1').click(function() {
    // Si esta seleccionado (si la propiedad checked es igual a true)
    if ($(this).prop('checked')) {
      // Selecciona cada input que tenga la clase .checar
      $('.checar').prop('disabled', true);
    } else {
      // Deselecciona cada input que tenga la clase .checar
      $('.checar').prop('disabled', false);
    }
  });

  </script>


  <div class="row" >

    <div class="col-md-12">
      <div class="box box-primary box-gris">

        <div class="box-header with-border my-box-header">
          <button onclick="cerrar_modal()" type="button" class="close" style="font-size: 40px;"  aria-label="Close">
            <span  aria-hidden="true">&times;</span><!-- Botón cerrar para volver a la vista sin necesidad de recargar la página -->
          </button>

          <!-- por medio del foreach recorremos el array que nos envío el controlador
          y mostramos el nombre de la sede-->
          @foreach($nombres as $nombre)
            <h5><strong>{{$nombre->nombre_municipio}} &nbsp;&nbsp; - &nbsp;&nbsp; {{$nombre->nombre_establecimiento}} &nbsp;&nbsp; - &nbsp;&nbsp; {{$nombre->nombre_sede}}  &nbsp;&nbsp; - &nbsp;&nbsp; {{$nombre->direccion}} &nbsp;&nbsp; -
            &nbsp;&nbsp; Código Dane: {{ $sedesservicio->DaneSede}}</strong></h5>
          @endforeach

          <p>En este formulario se especificarán la cantidad de servicios según lo requerido a continuación.</p>
          <p style="color:red;">* Obligatorio</p>

        </div><!-- /.form-group -->

        <div id="notificacion_E2" ></div>
        <div class="box-body">

          <form name="some_form" method="POST" id="f_editar_sedesservicio"  class="formentrada">
            <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
            <input type="hidden" name="id_sedesservicio" value="{{ $sedesservicio->id }}">
            <input type="hidden" name="sede_id" value="{{ $sedesservicio->DaneSede }}">

            <div class="row" style="margin-top:10px;">
              <div class="col-xs-12" style="border-bottom:1px solid #337ab7; border-top:1px solid #337ab7; padding:0.8em; margin-top:0.5em; margin-bottom:0.5em; text-align:center;">
                <strong>Actualizar tipos de energía de la sede educativa <span style="color:red;font-size:20px;">*</span></strong></div>

                <div class="row" style="padding:20px;">
                  <div class="col-md-12">
                    <h5 style="margin-top:20;margin-left:10px;">Los tipos de energía registrados de la sede educativa son: <strong>{{ $sedesservicio->TipoEnergia }}.</strong></h5>
                    <div class="col-xs-3" style="margin-left:242px; margin-top:10px">
                      <label class="checkbox-formulario">
                        <input type="checkbox" name="tipoenergia[]"  value="No tiene" id="group1" title="8" value="1">
                        <span class="label-text"><b>1) No tiene</b></span>
                      </label>
                    </div>
                    <div class="col-xs-3" style="margin-left:15px; margin-top:10px">
                      <label class="checkbox-formulario">
                        <input type="checkbox" name="tipoenergia[]"  value="Eléctrica" class="checar" title="1" value="1">
                        <span class="label-text"><b>2) Eléctrica</b></span>
                      </label>
                    </div><div class="col-xs-3" style="margin-left:15px; margin-top:10px">
                      <label class="checkbox-formulario">
                        <input type="checkbox" name="tipoenergia[]" value="Eólica" class="checar"  title="2" value="1">
                        <span class="label-text"><b>3) Eólica</b></span>
                      </label>
                    </div>
                    <div class="col-xs-3" style="float: left; margin-left:242px; margin-top:10px">
                      <label class="checkbox-formulario">
                        <input type="checkbox" name="tipoenergia[]" class="checar" value="Solar" title="3" value="1">
                        <span class="label-text"><b>4) Solar</b></span>
                      </label>
                    </div>
                    <div class="col-xs-3" style="margin-left:15px; margin-top:10px">
                      <label class="checkbox-formulario">
                        <input type="checkbox" name="tipoenergia[]" class="checar"  value="Térmica" title="4" value="1">
                        <span class="label-text"><b>5) Térmica</b></span>
                      </label>
                    </div>
                    <div>
                      <div class="col-xs-3" style="margin-left:15px; margin-top:10px">
                        <label class="checkbox-formulario">
                          <input type="checkbox" name="tipoenergia[]" class="checar"  value="Planta eléctrica" title="5" value="1">
                          <span class="label-text"><b>6) Planta eléctrica</b></span>
                        </label>
                      </div>
                    </div>
                  </div>
                </div>
              </div>

              <div class="row" style="margin-top:20px;">
                <div class="col-xs-12" style="border-bottom:1px solid #337ab7; border-top:1px solid #337ab7; padding:0.8em; margin-top:0.5em; margin-bottom:0.5em; text-align:center;">
                  <strong>Actualizar información adicional</strong></div>

                  <div style="margin-top:20px;" class="col-md-12">
                    <div class="form-group">
                      <label class="col-sm-2" for="nombre">¿Tiene acueducto? <span style="color:red;font-size:20px;">*</span></label>
                      <div class="col-xs-3">
                        <select  class="form-control" id="acueducto" name="acueducto"   required   >
                          <option>{{ $sedesservicio->Acueducto }} </option>
                          <option>Si</option>
                          <option>No</option>
                        </select>
                      </div>
                    </div><!-- /.form-group -->
                  </div><!-- /.col -->

                  <div class="col-md-12">
                    <div class="form-group">
                      <label class="col-sm-2" for="nombre">¿Posee agua potable? <span style="color:red;font-size:20px;">*</span></label>
                      <div class="col-xs-3">
                        <select class="form-control" id="aguapotable" name="aguapotable" required=" ">
                          <option>{{ $sedesservicio->AguaPotable}} </option>
                          <option>Si</option>
                          <option>No</option>
                        </select>
                      </div>
                    </div><!-- /.form-group -->
                  </div><!-- /.col -->

                  <div class="col-md-12">
                    <div class="form-group">
                      <label class="col-sm-2" for="nombre">¿Posee planta de tratamiento? <span style="color:red;font-size:20px;">*</span></label>
                      <div class="col-xs-3">
                        <select class="form-control" id="plantatratamiento" name="plantatratamiento" required=" ">
                          <option>{{ $sedesservicio->PlantaTratamiento}} </option>
                          <option>Si</option>
                          <option>No</option>
                        </select>
                      </div>
                    </div><!-- /.form-group -->
                  </div><!-- /.col -->


                  <div class="col-md-12">
                    <div class="form-group">
                      <label class="col-sm-2" for="nombre">¿Tiene alcantarillado? <span style="color:red;font-size:20px;">*</span></label>
                      <div class="col-xs-3">
                        <select class="form-control" id="alcantarillado" name="alcantarillado" required >
                          <option>{{ $sedesservicio->Alcantarillado }}</option>
                          <option>Si</option>
                          <option>No</option>
                        </select>
                      </div>
                    </div><!-- /.form-group -->
                  </div><!-- /.col -->


                  <div class="col-md-12">
                    <div class="form-group">
                      <label class="col-sm-2" for="nombre">¿Tiene pozo séptico? <span style="color:red;font-size:20px;">*</span></label>
                      <div class="col-xs-3">
                        <select class="form-control" id="pozoseptico" name="pozoseptico" required   >
                          <option>{{ $sedesservicio->PozoSeptico }}</option>
                          <option>Si</option>
                          <option>No</option>
                        </select>
                      </div>
                    </div><!-- /.form-group -->
                  </div><!-- /.col -->
                </div>

                <div class="box-footer col-xs-12 box-gris ">
                  <center>
                    <button type="submit" onclick="this.form.action='editar_sedesservicio'; return validar()"  class="btn btn-primary">Enviar</button>
                    <a onclick="cerrar_modal()" style="margin-left:15px;" class="btn btn-primary">Cancelar</a><!-- Botón para cancerlar el ingreso de una nueva sede educativa
                      y volver a la vista sin necesidad de recargar la página -->
                    </center>
                  </div>

                </form>

              </div>

            </div>

          </section>
