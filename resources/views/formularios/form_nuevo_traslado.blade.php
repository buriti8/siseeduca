@extends('layouts.app')

@section('htmlheader_title')
Traslado Dispositivo
@endsection

@section('main-content')
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>

<!-- Inicio contenido principal -->
<section  id="contenido_principal">
    <!-- Principal -->
    <div class="box box-primary">
        <div class="box-header">
        </div><!-- /.box-header -->

<div class="box box-primary" style="border: 1px solid #3c8dbc !important;" >
        <div class="titleboxes" style="font-size:20px;text-align:center;">Traslados</div>
        <div class="col-md-12">
        <p style="color:red;">* Obligatorio</p>
      </div>

        <!-- Body -->
        <div class="box-body">
            <form   action="{{ url('crear_traslado') }}"  method="post" id="frmtraslado">
                <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                <input type="hidden" id="dispositivo_id" name="dispositivo_id" value="{{ $id_dispositivo }}">

                <div class="col-md-12">
                    <div class="form-group">
                        <label class="col-sm-3" for="origen">Id dispositio</label>
                        <div class="col-sm-4" >
                            <!-- <input type="number" class="form-control" id="dispositivo_id_1" name="dispositivo_id_1" value="{{ $id_dispositivo }}" min="1" readonly required style="width:60%;"> -->
                            <label>{{ $id_dispositivo }}</label>
                        </div>
                    </div>
                </div>

                <div class="col-md-12">
                    <div class="form-group">
                        <label class="col-sm-3" for="serial">Serial dispositivo</label>
                        <div class="col-sm-4" >
                            <label>{{ $serial }}</label>
                        </div>
                    </div>
                </div>

                <div class="col-md-12">
                    <div class="form-group">
                        <label class="col-sm-3" for="marca">Marca dispositivo</label>
                        <div class="col-sm-4" >
                            <label>{{ $marca }}</label>
                        </div>
                    </div>
                </div>

                <div class="col-md-12">
                    <div class="form-group">
                        <label class="col-sm-3" for="origen">Sede origen</label>
                        <div class="col-sm-6" >
                            <input type="hidden" class="form-control" id="origen" name="origen" value="{{ $sede_origen }}" readonly required>
                            <label>{{ $sede_origen }} - </label>
                            <label>{{ $nombre_sede_origen }}</label>
                            <label> - {{ $direccion }}</label>
                        </div>
                    </div>
                </div>

                <div class="col-md-12">
                    <div class="form-group">
                        <label class="col-sm-2" for="destino">Sede destino <span style="color:red;font-size:20px;">*</span></label>
                        <div class="col-sm-12">
                            @role('secretarios')
                                @include('partialsroles.secretario')
                             @else
                            @role('rectores')
                                @include('partialsroles.rector')
                            @else
                                @include('partialsroles.filtro')
                            @endrole
                        @endrole
                        </div>
                    </div>
                </div>

                <div class="col-md-12">
                    <div class="form-group">
                        <label class="col-sm-2" for="motivo">Motivo <span style="color:red;font-size:20px;">*</span></label>
                        <div class="col-sm-6">
                            <select id="motivo" name="motivo" class="form-control" style="width:60%;">
                                    <option value="1">Cierre temporal</option>
                                    <option value="2">Cierre definitivo</option>
                            </select>
                        </div>
                    </div>
                </div>

                <div class="col-md-12">
                    <div class="form-group">
                        <label class="col-sm-2" for="descripcion">Descripción</label>
                        <div class="col-sm-6">
                            <textarea rows="4" cols="50" type="text" id="descripcion" name="descripcion" class="form-control" maxlength=230 ></textarea>
                        </div>
                    </div>
                </div>

                <div class="col-xs-12" style="margin-top:15px;">
                  <center>
                    <button type="submit" class="btn btn-primary">Enviar</button>
                    <a href="{{ url('listado_dispositivos') }}" class="btn btn-primary">Regresar</a>
                  </center>
                </div>
            </form>
        </div>
        <!-- Fin body -->
</div>

    </div>
    <!-- Fin principal -->

</section>
@endsection
