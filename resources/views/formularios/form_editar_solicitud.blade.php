@extends('layouts.app')

@section('htmlheader_title')
Editar Solicitud
@endsection

@section('main-content')
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>

<!-- Inicio contenido principal -->
<section  id="contenido_principal">
    <!-- Principal -->
    <div class="box box-primary">
        <div class="box-header">
        </div><!-- /.box-header -->

        <!-- Interior -->
        <div class="box box-primary" style="border: 1px solid #3c8dbc !important;" >
           <div class="titleboxes" style="font-size:20px;text-align:center;">Editar petición</div>
           <div class="col-md-12">


           <p style="color:red;">* Obligatorio</p>
         </div>



            <form action="{{ url('editar_solicitud', $solicitud->id) }}" method="post" id="frmsolicitud" enctype="multipart/form-data">

                @csrf

                <div class="row" style="padding:20px;">
                    <!-- Inicio -->
                    <div class="col-md-12">
                        <div class="titleboxes" style="font-size:15px; text-align:center;"></div>
                        <div class="col-md-12" style="padding-top:15px;padding-bottom:15px;">

                        @role('secretarios')
                            @include('partialsroles.secretario_editar_solicitud')
                        @else
                            @role('rectores')
                                @include('partialsroles.rector_editar_solicitud')
                            @else
                                @include('partialsroles.filtro_editar_solicitud')
                            @endrole
                        @endrole

                        </div>
                    </div>

                    <div class="col-md-12">
                        <div class="titleboxes" style="font-size:15px; text-align:center;"></div>
                        <div class="col-md-12" style="padding:0px;">
                            <div class="col-md-3" style="padding-top:15px;padding-bottom:15px;">
                                <div class="form-group">
                                    <label>Tipo de solicitud <span style="color:red;font-size:20px;">*</span></label>
                                    <select id="tipo_solicitud" name="tipo_solicitud" class="form-control" required>
                                        @if($solicitud->tipo_solicitud == 1)
                                            <option value="1" selected>Dispositivo</option>
                                            <option value="2">Conectividad</option>
                                        @else
                                            <option value="1">Dispositivo</option>
                                            <option value="2" selected>Conectividad</option>
                                        @endif
                                    </select>
                                </div>


                            </div>
                        </div>

                        <div class="col-md-12" style="padding:0px;">
                            <div class="col-md-3">
                                <div class="div_dispositivos" style="@if($solicitud->tipo_solicitud == 2) display:none @endif; padding-top:2px;padding-bottom:10px;">
                                    <div class="form-group">
                                        <label>Seleccione el tipo de dispositivo <span style="color:red;font-size:20px;">*</span></label>
                                        <select id="tipo_id" name="tipo_id" class="form-control">
                                            @if($solicitud->tipo_solicitud == 1)
                                                @foreach($tipos as $tipo)
                                                    <option value="{{ $tipo->id }}" @if($solicitud->tipodispositivo_id == $tipo->id) selected @endif>{{ $tipo->nombre }}</option>
                                                @endforeach
                                            @else
                                                @foreach($tipos as $tipo)
                                                    <option value="{{ $tipo->id }}">{{ $tipo->nombre }}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                    </div>
                                    <div class="form-group "  >
                          <label for="cantidad_d">Cantidad de dispositivos</label>
                          <input name="cantidad_d" id="cantidad_d" type="number"   class="archivo form-control" value="{{ $solicitud->cantidad_d }}" maxlength="2" min="1" max="99"/>
                        </div>

                                </div>

                                <div class="div_conectividad" style="padding-top:2px;padding-bottom:10px;">
                                <div class="form-group">
                                  <label>Seleccione el tipo de uso <span style="color:red;font-size:20px;">*</span></label>
                                  <select id="tipo_uso" name="tipo_uso" class="form-control"  >
                                  <option>{{ $solicitud->tipo_uso }}</option>
                                    <option></option>
                                    <option>Educativo</option>

                                  </select>


                                </div>
                              </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12">
                        <div class="titleboxes" style="font-size:15px; text-align:center;"></div>
                        <div class="col-md-12" style="padding-top:15px;padding-bottom:15px;">
                            <div class="form-group" >
                                <label>Debe  el  motivo de la petición <span style="color:red;font-size:20px;">*</span></label>
                                <textarea rows="4" cols="50" type="text" id="descripcion" name="descripcion" class="form-control" maxlength=230 required>{{ $solicitud->descripcion }}</textarea>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12">
                        <div class="titleboxes" style="font-size:15px; text-align:center;"></div>
                        <div class="col-md-12" style="padding-top:15px;padding-bottom:15px;">
                            <div class="form-group">
                              <label><strong>Nota:el archivo no debe superar los 5 megabytes (MB).</strong></label>
                              <label>Agregar archivo <span class="label label-primary">PDF</span> / <span class="label label-primary">Imagen</span></label>
                                <input type="file" id="solicitud_fisica" name="solicitud_fisica" maxlength=200>
                            </div>
                        </div>
                    </div>

                    @if ($solicitud->solicitud_fisica != "")
                        <div class="col-md-12" style="padding-top:15px;padding-bottom:15px;">
                            <div class="form-group">
                                <a href="/down{{$solicitud->solicitud_fisica}}" target="_blank" id="descargar_solicitud" name="descargar_solicitud"  class="btn btn-danger pull-left glyphicon glyphicon-download-alt " style="margin-right:50px;"> DESCARGAR SOLICITUD</a>
                            </div>
                        </div>
                    @else
                        <div class="col-md-12" >
                            <label>No hay archivo djunto</label>
                        </div>
                    @endif

                    @role('administrador_sistema')
                    <div class="col-md-12">
                        <div class="titleboxes" style="font-size:15px; text-align:center;"></div>
                        <div class="col-md-12" style="padding-top:15px;padding-bottom:15px;">
                            <div class="form-group" >
                                <div class="form-check">
                                <label class="checkbox-formulario" style="font-size:14px; color:#3B3938;">
                                    <input type="checkbox" value="1" name="cbestado" id="cbestado" @if($solicitud->estado_atencion == 1) checked @endif > <span class="label-text" >Solicitud atendida</span>
                                </label>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endrole


                    @role('administrador_sistema')
                    <div class="texto">
                        <div class="titleboxes" style="font-size:15px; text-align:center;"></div>
                        <div class="col-md-12" style="padding-top:15px;padding-bottom:15px;">
                            <div class="form-group" >
                                <label>Respuesta - Secretaría de Educación, Dirección de Infraestructura tecnológica. <span style="color:red;font-size:20px;">*</span></label>

                                  @if($solicitud->respuesta != null)
                                    <textarea readonly="true" class="form-control" style="resize:none" rows="5" id="respuesta" placeholder="Escriba aquí su respuesta"  name="respuesta">{{$solicitud->respuesta}}</textarea>
                                      @else
                                <textarea rows="4" cols="50" type="text" id="respuesta" name="respuesta" class="form-control" maxlength=230 required></textarea>
                                @endif
                            </div>
                        </div>
                    </div>

                    @endrole












                    <div class="col-md-12">
                        <div class="titleboxes" style="font-size:15px; text-align:center;"></div>
                    </div>
                    <!-- Fin -->






                    <div class="col-xs-12" style="margin-top:15px;">
                      <center>
                        <button onclick="return confirm('Recuerda:Se le dará respuesta al correo al que tiene inscrito.')" type="submit" class="btn btn-primary">Enviar</button>
                        <a href="{{ url('listado_solicitudes') }}" class="btn btn-primary">Regresar</a>
                      <center>
                    </div>
                </div>
            </form>
        </div>
        <!-- Fin interior -->


    </div>
     <!-- Principal -->

</section>
<!-- Fin contenido principal -->






@endsection
