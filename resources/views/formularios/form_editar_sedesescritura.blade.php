<section >
  <div class="row" >
    <div class="col-md-12">
      <div class="box box-primary box-gris">

        <div class="box-header with-border my-box-header">
          <button onclick="cerrar_modal()" type="button" class="close" style="font-size: 40px;"  aria-label="Close">
            <span  aria-hidden="true">&times;</span><!-- Botón cerrar para volver a la vista sin necesidad de recargar la página -->
          </button>

          <!-- por medio del foreach recorremos el array que nos envío el controlador
          y mostramos el nombre de la sede-->
          @foreach($nombres as $nombre)
            <h5><strong>{{$nombre->nombre_municipio}} &nbsp;&nbsp; - &nbsp;&nbsp; {{$nombre->nombre_establecimiento}} &nbsp;&nbsp; - &nbsp;&nbsp; {{$nombre->nombre_sede}}  &nbsp;&nbsp; - &nbsp;&nbsp; {{$nombre->direccion}} &nbsp;&nbsp; -
              &nbsp;&nbsp; Código Dane: {{ $sedesescritura->DaneSede}}</strong></h5>
            @endforeach
            <p>En este espacio se podrá actualizar la información legal de la sede educativa.</p>
            <p style="color:red;">* Obligatorio</p>
          </div><!-- /.box-header -->

          <div id="notificacion_E2" ></div>
          <div class="box-body">


            <form  action="{{ url('editar_sedesescritura') }}"  method="post" id="f_editar_sedesescritura"  class="formentrada"  >
              <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
              <input type="hidden" name="id_sedesescritura" value="{{ $sedesescritura->id }}">
              <input type="hidden" name="sede_id" value="{{ $sedesescritura->DaneSede}}">

              <div class="col-md-12">
                <div class="form-group">
                  <label class="col-sm-2" for="nombre">Propietario de la sede <span style="color:red;font-size:20px;">*</span></label>
                  <div class="col-xs-3" >
                    <input type="text" pattern="[a-zA-ZñÑáéíóúÁÉÍÓÚ\s]+" title="Solo ingrese texto"class="form-control" id="propietario" name="propietario" onKeyUp="this.value=this.value.toUpperCase();"  value="{{ $sedesescritura->Propietario }}"  required=" ">
                  </div>
                </div><!-- /.form-group -->
              </div><!-- /.col -->

              <div class="col-md-12">
                <div class="form-group">
                  <label class="col-sm-2" for="nombre">Matrícula inmobiliaria <span style="color:red;font-size:20px;">*</span></label>
                  <div class="col-xs-3" >
                    <input type="text" class="form-control" id="matriculainmob" name="matriculainmob"  value="{{ $sedesescritura->MatriculaInmob }}"  required   >
                  </div>
                </div><!-- /.form-group -->
              </div><!-- /.col -->


              <div class="col-md-12">
                <div class="form-group">
                  <label class="col-sm-2" for="nombre">Número de escritura <span style="color:red;font-size:20px;">*</span></label>
                  <div class="col-xs-3" >
                    <input type="number" class="form-control" id="numescritura" name="numescritura"  value="{{ $sedesescritura->NumEscritura }}"  required   >
                  </div>
                </div><!-- /.form-group -->
              </div><!-- /.col -->


              <div class="col-md-12">
                <div class="form-group">
                  <label class="col-sm-2" for="nombre">Fecha escritura <span style="color:red;font-size:20px;">*</span></label>
                  <div class="col-xs-3" >
                    <input type="date" class="form-control" id="fechaescritura" name="fechaescritura" step="1" min="1900-01-01" max="2019-12-31"  value="{{ $sedesescritura->FechaEscritura }}"  required >
                  </div>
                </div><!-- /.form-group -->
              </div><!-- /.col -->


              <div class="box-footer col-xs-12 box-gris ">
                <center>
                  <button type="submit" class="btn btn-primary">Enviar</button>
                  <a onclick="cerrar_modal()" style="margin-left:15px;" class="btn btn-primary">Cancelar</a><!-- Botón para cancerlar el ingreso de una nueva sede educativa
                    y volver a la vista sin necesidad de recargar la página -->
                  </center>
                </div>

              </form>

            </div>

          </div>

        </div>
      </div>
    </section>
