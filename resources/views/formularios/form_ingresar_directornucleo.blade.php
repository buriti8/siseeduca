@extends('layouts.app')
@section('htmlheader_title')
  Información directores(a) núcleo
@endsection

@section('main-content')

  <section>

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/chosen/1.5.1/chosen.min.css">
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/chosen/1.5.1/chosen.jquery.min.js"></script>

    <form   action="{{ url('ingresar_directornucleo') }}"  method="POST"  enctype="multipart/form-data">

      <div class="col-md-12">

        <div class="box box-primary">

          <div class="box-header with-border my-box-header">
            <h3 class="box-title"><strong>Ingresar información de director(a) de núcleo</strong></h3>
            <p style="color:red;">* Obligatorio</p>
          </div><!-- /.box-header -->

          <div class="box-body">
            <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">


              <div style="margin-left:47px;"  class="form-group">
                <div >
                  <label  for="selMunicipio">Seleccione municipios:  </label>
                  <select multiple class="chosen" style="width:500px;" name="municipio[]" class="form-control" required>
                    @foreach ($municipios as $municipio)
                      <option value="{{$municipio->CodigoDaneMunicipio}}">{{$municipio->NombreMunicipio}}</option>
                    @endforeach
                  </select>
                </div>
              </div>

            <div class="row" style="padding:20px;">
              <div class="col-md-12">
                <br>
                <div class="col-xs-12" style="border-bottom:1px solid #337ab7; border-top:1px solid #337ab7; padding:0.8em; margin-top:0; margin-bottom:10px; text-align:center;">
                  <strong>Ingresar información de director(a) de núcleo</strong></div>

                  <div style="margin-top:20px;" class="col-md-12">
                    <div class="form-group">
                      <label class="col-sm-3" for="nombre"> Nombres <span style="color:red;font-size:20px;">*</span></label>
                      <div class="col-xs-3" >
                        <input type="text"  onkeyup="javascript:this.value=this.value.toUpperCase();"  class="form-control" pattern="[a-zA-ZñÑáéíóúÁÉÍÓÚ\s]+" title="Solo ingrese texto" name="nombres" required>
                      </div>
                    </div><!-- /.form-group -->
                  </center>
                </div><!-- /.col -->

                <div class="col-md-12">
                  <div class="form-group">
                    <label class="col-sm-3" for="nombre"> Apellidos <span style="color:red;font-size:20px;">*</span></label>
                    <div class="col-xs-3" >
                      <input type="text"  onkeyup="javascript:this.value=this.value.toUpperCase();"  class="form-control" pattern="[a-zA-ZñÑáéíóúÁÉÍÓÚ\s]+" title="Solo ingrese texto" name="apellidos" required>
                    </div>
                  </div><!-- /.form-group -->
                </div><!-- /.col -->

                <div class="col-md-12">
                  <div class="form-group">
                    <label class="col-sm-3" for="nombre"> Celular <span style="color:red;font-size:20px;">*</span></label>
                    <div class="col-xs-3" >
                      <input type="number" placeholder="No ingrese signo más(+) ni menos(-)"  id="numero" class="form-control" name="celular"  required>
                    </div>
                  </div><!-- /.form-group -->
                </div><!-- /.col -->

                <div class="col-md-12">
                  <div class="form-group">
                    <label class="col-sm-3" for="nombre"> Teléfono <span style="color:red;font-size:20px;">*</span></label>
                    <div class="col-xs-3" >
                      <input type="text" class="form-control" name="telefono" required>
                    </div>
                  </div><!-- /.form-group -->
                </div><!-- /.col -->

                <div class="col-md-12">
                  <div class="form-group">
                    <label class="col-sm-3" for="nombre"> Correo personal <span style="color:red;font-size:20px;">*</span></label>
                    <div class="col-xs-3" >
                      <input type="text" pattern="[a-zA-ZàáâäãåąčćęèéêëėįìíîïłńòóôöõøùúûüųūÿýżźñçčšžÀÁÂÄÃÅĄĆČĖĘÈÉÊËÌÍÎÏĮŁŃÒÓÔÖÕØÙÚÛÜŲŪŸÝŻŹÑßÇŒÆČŠŽ∂ð ,.'-_]{2,64}"  class="form-control" name="correo_personal" required>
                    </div>
                  </div><!-- /.form-group -->
                </div><!-- /.col -->


                <div class="col-md-12">
                  <div class="form-group">
                    <label class="col-sm-3" for="nombre"> Dirección <span style="color:red;font-size:20px;">*</span></label>
                    <div class="col-xs-3" >
                      <input type="text" onkeyup="javascript:this.value=this.value.toUpperCase();" class="form-control" name="direccion" required>
                    </div>
                  </div><!-- /.form-group -->
                </div><!-- /.col -->


                <div style="margin-top:15px;" class="box-footer col-xs-12">
                  <center>
                    <button  type="submit" onclick="return confirm('¿Desea guardar la información?')"  class="btn btn-primary">Enviar</button><!-- Botón para registrar la información de una nueva sede educativa -->
                    <a href="/actualizar_directorio" style="margin-left:15px;" class="btn btn-primary">Cancelar</a><!-- Botón para cancerlar el ingreso de una nueva sede educativa
                      y volver a la vista sin necesidad de recargar la página -->
                    </center>
                  </div>
                </div><!-- /.form-group -->
              </div><!-- /.col -->
            </div><!-- /.col -->

            <script type="text/javascript">
            $(".chosen").chosen();

            var input=  document.getElementById('numero');
            input.addEventListener('input',function(){
              if (this.value.length > 10)
              this.value = this.value.slice(0,10);
            })


            </script>

          </form>
        </section>
      @endsection
