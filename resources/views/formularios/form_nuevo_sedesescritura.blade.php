
<section  >
  <div class="col-md-12">

    <div class="box box-primary  box-gris">

      <div class="box-header with-border my-box-header">
        <h3 class="box-title"><strong>Información legal de la sede educativa</strong></h3>
        <button onclick="cerrar_modal()" type="button" class="close" style="font-size: 40px;"  aria-label="Close">
          <span  aria-hidden="true">&times;</span><!-- Botón cerrar para volver a la vista sin necesidad de recargar la página -->
        </button>
        <p>Si posee las escrituras de la sede; por favor, enviarlas al siguiente correo electrónico: <strong> david.franco@antioquia.gov.co</strong></p>
        <p style="color:red;">* Obligatorio</p>
      </div><!-- /.box-header -->

      <div class="box-body">

        <form   action="{{ url('crear_sedesescritura') }}"  method="post" id="f_crear_sedesescritura" class="formentrada"  >
          <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">

          <div class="row" style="padding:20px;">
            <div class="col-md-12">
              <div class="titleboxes" style="font-size:15px; text-align:center;">
              </div>
              <br>

              @role('secretarios')
              @include('partialsroles.secretario')
            @else
              @role('rectores')
              @include('partialsroles.rector')
            @else
              @include('partialsroles.filtro')
              @endrole
              @endrole

            </div>

            <div class="col-md-12">
              <div class="form-group">
                <label class="col-sm-2" for="nombre">Propietario de la sede educativa  <span style="color:red;font-size:20px;">*</span></label>
                <div class="col-xs-3">
                  <input type="text" pattern="[a-zA-ZñÑáéíóúÁÉÍÓÚ\s]+" title="Solo ingrese texto"
                  class="form-control" id="propietario" name="propietario"  required >

                </div>
              </div><!-- /.form-group -->

            </div><!-- /.col -->

            <div class="col-md-12">
              <div class="form-group">
                <label class="col-sm-2" for="nombre"> Matricula inmobiliaria  <span style="color:red;font-size:20px;">*</span></label>
                <div class="col-xs-3">
                  <input type="text" class="form-control" id="matriculainmob" name="matriculainmob" required >
                </div>
              </div><!-- /.form-group -->
            </div><!-- /.col -->

            <div class="col-md-12">
              <div class="form-group">
                <label class="col-sm-2" for="nombre">Número de escritura  <span style="color:red;font-size:20px;">*</span></label>
                <div class="col-xs-3">
                  <input type="number" title="Solo ingrese valores númericos" class="form-control" id="numescritura" name="numescritura" required >
                </div>
              </div><!-- /.form-group -->
            </div><!-- /.col -->


            <div class="col-md-12">
              <div class="form-group">
                <label class="col-sm-2" for="nombre">Fecha escritura (DD/MM/AA)  <span style="color:red;font-size:20px;">*</span></label>
                <div class="col-xs-3">
                  <input type="date" class="form-control" id="fechaescritura" name="fechaescritura" step="1" min="1900-01-01" max="2019-12-31" >
                </div>
              </div><!-- /.form-group -->
            </div><!-- /.col -->

            <div class="box-footer col-xs-12 box-gris ">
              <center>
                <button  type="submit" onclick="return confirm('¿Desea guardar la información?')"  class="btn btn-primary">Enviar</button><!-- Botón para registrar la información de una nueva sede educativa -->
                <a onclick="cerrar_modal()" style="margin-left:15px;" class="btn btn-primary">Cancelar</a><!-- Botón para cancerlar el ingreso de una nueva sede educativa
                  y volver a la vista sin necesidad de recargar la página -->
                </center>
              </div>
            </form>

          </div>
        </div>
      </div>

    </div>

  </section>

  <script>
