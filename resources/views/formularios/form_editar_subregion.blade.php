<section >



  <div class="row" >

    <div class="col-md-12">


      <div class="box box-primary box-gris">

        <div class="box-header with-border my-box-header">
          <h3 class="box-title"><strong>Editar Subregión </strong></h3>
          <button onclick="cerrar_modal()" type="button" class="close" style="font-size: 40px; color:#34495E;"  aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button> <!-- Botón cerrar para volver a la vista sin necesidad de recargar la página -->
          <p style="color:red;">* Obligatorio</p>
        </div><!-- /.box-header -->

        <div id="notificacion_E2" ></div>
        <div class="box-body">




          <form   action="{{ url('editar_subregion') }}"  method="post" id="f_editar_subregion"  class="formentrada"  >
            <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
            <input type="hidden" name="id_subregion" value="{{ $subregion->id }}">

            <div class="col-md-12">
              <div class="form-group">
                <label class="col-sm-2" for="nombre">Código Subregión <span style="color:red;font-size:20px;">*</span> </label>
                <div class="col-xs-3" >
                  <input type="number" class="form-control" id="codigo_subregion" name="codigo_subregion"  value="{{ $subregion->CodigoSubregion }}"  required   >
                </div>
              </div><!-- /.form-group -->
            </div><!-- /.col -->

            <div class="col-md-12">
              <div class="form-group">
                <label class="col-sm-2" for="nombre">Nombre Subregion <span style="color:red;font-size:20px;">*</span> </label>
                <div class="col-xs-3" >
                  <input type="text" class="form-control" id="nombre_subregion" name="nombre_subregion"  value="{{ $subregion->NombreSubregion }}"  required   >
                </div>
              </div><!-- /.form-group -->
            </div><!-- /.col -->


            <div class="box-footer col-xs-12 box-gris ">
              <button type="submit" class="btn btn-primary">Actualizar</button>
<a onclick="cerrar_modal()" style="margin-left:15px;" class="btn btn-primary">Cancelar</a>
            </div>

          </form>

        </div>

      </div>

    </div>
  </div>
</section>
