<section>
<div class="row" >
    <div class="col-md-12">
        <div class="box box-primary box-gris">
            <div class="box-header with-border my-box-header">

              <button onclick="cerrar_modal()" type="button" class="close" style="font-size: 40px; color:#34495E;"  aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button> <!-- Botón cerrar para volver a la vista sin necesidad de recargar la página -->

                <h3 class="box-title"><strong>Editar Información Proyecto</strong></h3>
                <p style="color:red;">* Obligatorio</p>
            </div>

            <div id="notificacion_E2" ></div>
            <div class="box-body">
                <form   action="{{ url('editar_proyecto') }}"  method="post" id="f_editar_proyecto"  class="formentrada">
                        <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                        <input type="hidden" name="id_proyecto" value="{{ $proyecto->id }}">

                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="col-sm-2" for="nombre">Nombre <span style="color:red;font-size:20px;">*</span></label>
                                <div class="col-sm-6" >
                                <input type="text" pattern="[a-zA-ZñÑáéíóúÁÉÍÓÚ\s]+" title="Solo ingrese texto" class="form-control" id="nombre_proyecto" name="nombre_proyecto" value="{{ $proyecto->nombre }}" maxlength=100 required>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="col-sm-2" for="plan_desarrollo">Plan de desarrollo <span style="color:red;font-size:20px;">*</span></label>
                                <div class="col-sm-6" >
                                <input type="text" pattern="[a-zA-ZñÑáéíóúÁÉÍÓÚ\s]+" title="Solo ingrese texto" class="form-control" id="plan_desarrollo" name="plan_desarrollo" value="{{ $proyecto->plan_desarrollo }}" maxlength=100 required>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="col-sm-2" for="periodo">Inicio del periodo <span style="color:red;font-size:20px;">*</span></label>
                                <div class="col-sm-6" >
                                <input type="number" class="form-control" id="periodo" name="periodo" value="{{ $periodo_inicio }}" min="1900" max="9999" required style="width:15%" placeholder="####">
                                </div>
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="col-sm-2" for="periodo">Fin del periodo <span style="color:red;font-size:20px;">*</span></label>
                                <div class="col-sm-6" >
                                    <input type="number" class="form-control" id="periodofinalizacion" name="periodofinalizacion" value="{{ $periodo_final }}" min="1900" max="9999"  required style="width:15%" placeholder="####">
                                </div>
                            </div>
                        </div>

                    <div class="box-footer col-xs-12 box-gris ">
                            <button type="submit" class="btn btn-primary">Actualizar datos</button>
                            <a onclick="cerrar_modal()" style="margin-left:10px;" class="btn btn-primary">Cancelar</a>
                    </div>
                </form>
            </div>
            </div>
        </div>
    </div>
</section>
