@if(Auth::user()->isRole('administrador_sistema') || Auth::user()->isRole('Administrador_infraestructura_educativa') || Auth::user()->isRole('Rectores') || Auth::user()->isRole('Secretarios'))
@extends('layouts.app')
@section('htmlheader_title')
  Requerimientos Infraestructura Tecnológica
@endsection

@section('main-content')
  <style type="text/css">
  #tablegeneral th{
    vertical-align: middle;
    border-bottom: none;
    text-align: right;
  }
  #tablegeneral td{
    text-align: left;
    vertical-align: middle;
  }

  #tablegeneral tr{
    border-bottom: 1px solid #ddd;
  }


</style>
<section  id="contenido_principal">
  <div class="box box-primary">
    <div align="center" class="box-header">
      <h3 class="box-title"><strong>Diagnóstico de necesidades generales en Antioquia</strong></h3>
    </div>
    <div class="box-body"  >
      <div class="panel panel-primary ">
        <div style="font-size: 16px;" class="panel-heading">Diagnóstico de necesidades registrados  - No evidenciados</div>
        <div id="notificacion_resul_fci">
          <div class="modal" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <h5 class="modal-title">Modal title</h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="modal-body">
                  <p>Modal body text goes here.</p>
                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-primary">Save changes</button>
                  <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="table-responsive" >
          @if (count($SolicitudIF) >= 1)
            <table  class= "table table-hover table-striped" style="text-align:center" cellspacing="0" width="100%">
              <thead>
                <tr>
                  <th>Id</th>
                  <th>Municipio</th>
                  <th>Establecimiento</th>
                  <th>Código establecimiento</th>
                  <th>Sede</th>
                  <th>Código sede</th>
                  <th>Zona</th>
                  <th>Dirección</th>
                  <th>Teléfono</th>
                  <th>Estado sede</th>
                  <th>Respuesta</th>

                  <th>Fecha solicitud</th>
                  <th>Acción</th>
                </tr>
              </thead>
              <tbody>
                @foreach($SolicitudIF as $solicitud)
                  <tr>
                    <td>{{$solicitud->id}}</td>
                    <td>{{$solicitud->temphistoricosedes->nombre_municipio}}</td>
                    <td><a href='{{url("/requerimientos/".$solicitud->Establecimiento->codigo_establecimiento."/establecimiento")}}'>{{ $solicitud->Establecimiento->nombre_establecimiento }}</a></td>
                    <td>{{$solicitud->Establecimiento->codigo_establecimiento}}</td>
                    <td><a href='{{url("/requerimientos/".$solicitud->temphistoricosedes->codigo_sede."/sede")}}'>{{ $solicitud->temphistoricosedes->nombre_sede }}</a></td>
                    <td>{{$solicitud->temphistoricosedes->codigo_sede}}</td>
                    <td>{{$solicitud->temphistoricosedes->zona}}</td>
                    <td>{{$solicitud->temphistoricosedes->direccion}}</td>
                    <td>{{$solicitud->temphistoricosedes->telefono}}</td>
                    <td>{{$solicitud->temphistoricosedes->estado_sede}}</td>
                            <td><center>
                              <?php
                              if($solicitud->Respuesta != null){
                                ?>
                                @if(Auth::user()->isRole('administrador_sistema') || Auth::user()->isRole('administrador_infraestructura_fisica'))
                                  <div  style='background:#B5FEC1; color:#3C763D'>1</div>
                                @elseif(Auth::user()->isRole('secretarios'))
                                  @if($solicitud->respuesta->VistoSecretario)
                                    <div  style='color:#3C763D'>1</div>
                                  @else
                                    <div  style='background:#B5FEC1; color:#3C763D'>1</div>
                                  @endif
                                @elseif(Auth::user()->isRole('rectores'))
                                  @if($solicitud->respuesta->VistoRector)
                                    <div  style='color:#3C763D'>1</div>
                                  @else
                                    <div  style='background:#B5FEC1; color:#3C763D'>1</div>
                                  @endif
                                @endif
                                <?php

                              }else{
                                echo 0;
                              }
                              ?>
                              <center>

                                <td>{{$solicitud->created_at}}</td>

                            <td>
                              <div class="row">
                              <a href="{{url('/requerimientos/'.$solicitud->id.'/view')}}">
                                <button type="button" class="btn btn-warning btn-xs" data-placement="left" title="Editar"
                                ><i class="fa fa-edit"></i></button>
                              </a>
                                <!--<button type="button" onclick="ver_editar_solicitud({{ $solicitud->id }})" class="btn  btn-default btn-xs" data-toggle="tooltip" title="Editar"><i class="fa fa-fw fa-edit"></i></button>-->
                                  <button type="button" onclick="confirmacion_borrado_solicitud({{ $solicitud->id }})" class="btn  btn-danger btn-xs" data-toggle="tooltip" title="Eliminar"><i class="fa fa fa-trash-o"></i></button>
                                  </div>
                            </td>
                        </tr>
                @endforeach
              </tbody>
            </table>
            {{ $SolicitudIF->links() }}
          @else
            <br>
            <center>
            <h4 style="margin-left:10px">No hay diagnóstico de necesidades no evidenciados registrados</h4>
          </center>
            <br>
          @endif
        </div>
      </div>


      <div class="panel panel-primary ">
        <div style="font-size: 16px;" class="panel-heading">Diagnóstico de necesidades registrados - Evidenciados</div>
        <div class="table-responsive" >
          @if (count($SolicitudIFAT) >= 1)
            <table  class="table table-hover table-striped" style="text-align:center" cellspacing="0" width="100%">
              <thead>
                <tr>
                  <th>Id</th>
                  <th>Municipio</th>
                  <th>Establecimiento</th>
                  <th>Código establecimiento</th>
                  <th>Sede</th>
                  <th>Código sede</th>
                  <th>Zona</th>
                  <th>Dirección</th>
                  <th>Teléfono</th>
                  <th>Estado sede</th>
                  <th>Respuesta</th>
                  <th>Fecha solicitud</th>
                  <th>Acción</th>
                </tr>
              </thead>
              <tbody>
                @foreach($SolicitudIFAT as $solNoatend)
                  <tr role="row" class="odd">
                    <td>{{$solNoatend->id}}</td>
                    <td>{{$solNoatend->temphistoricosedes->nombre_municipio}}</td>
                    <td><a href='{{url("/requerimientos/".$solNoatend->Establecimiento->codigo_establecimiento."/establecimiento")}}'>{{$solNoatend->Establecimiento->nombre_establecimiento}}</a></td>
                    <td>{{$solNoatend->Establecimiento->codigo_establecimiento}}</td>
                    <td><a href='{{url("/requerimientos/".$solNoatend->temphistoricosedes->codigo_sede."/sede")}}'>{{$solNoatend->temphistoricosedes->nombre_sede}}</a></td>
                    <td>{{$solNoatend->temphistoricosedes->codigo_sede}}</td>
                    <td>{{$solNoatend->temphistoricosedes->zona}}</td>
                    <td>{{$solNoatend->temphistoricosedes->direccion}}</td>
                    <td>{{$solNoatend->temphistoricosedes->telefono}}</td>
                    <td>{{$solNoatend->temphistoricosedes->estado_sede}}</td>
                    <td> <center>
                      <?php
                      if($solNoatend->Respuesta != null){
                        ?>
                        @if(Auth::user()->isRole('administrador_sistema') || Auth::user()->isRole('administrador_infraestructura_fisica'))
                          <div  style='background:#B5FEC1; color:#3C763D'>1</div>
                        @elseif(Auth::user()->isRole('secretarios'))
                          @if($solNoatend->respuesta->VistoSecretario)
                            <div  style='color:#3C763D'>1</div>
                          @else
                            <div  style='background:#B5FEC1; color:#3C763D'>1</div>
                          @endif
                        @elseif(Auth::user()->isRole('rectores'))
                          @if($solNoatend->respuesta->VistoRector)
                            <div  style='color:#3C763D'>1</div>
                          @else
                            <div  style='background:#B5FEC1; color:#3C763D'>1</div>
                          @endif
                        @endif
                        <?php

                      }else{
                        echo 0;
                      }
                      ?>
                      <center>
                    </td>

                    <td>{{$solNoatend->created_at}}</td>

                    <td>
                      <a href="{{url('/requerimientos/'.$solNoatend->id.'/view')}}">
                        <button type="button" class="btn btn-info btn-sm" style="margin-left: 5px;" data-placement="left" title="Información"
                        ><i class="fa fa fa-info"></i></button>
                      </td>
                    </tr>
                  @endforeach
                </tbody>
              </table>
              {{ $SolicitudIFAT->links() }}
            @else
              <br>
              <center>
              <h4 style="margin-left:10px">No hay diagnóstico de necesidades evidenciados registrados</h4>
            </center>
              <br>
            @endif
          </div>
        </div>
      </div>
    </div>
  @endsection
@else
  @extends('errors.acceso')
@endif
