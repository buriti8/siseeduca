@extends('layouts.app')
@section('htmlheader_title')
  Requerimientos Infraestructura Tecnológica
@endsection

@section('main-content')
  <style type="text/css">
  #tablegeneral th{
    vertical-align: middle;
    border-bottom: none;
    text-align: right;
  }
  #tablegeneral td{
    text-align: left;
    vertical-align: middle;
  }

  #tablegeneral tr{
    border-bottom: 1px solid #ddd;
  }
</style>
<section  id="contenido_principal">
  <div class="box box-primary">
    <div align="center" class="box-header">
      <h3 align="center" class="box-title">
        <strong>Diagnósticos por sedes</strong>
      </h3>
      @if(Auth::user()->isRole('rectores'))
        <a href='{{url("/requerimientos/".Auth::user()->name."/establecimiento")}}'>
          <button type="button" class="btn btn-primary pull-right" style="margin-right: 8px;" name="button">Regresar</button>
        </a>
      @elseif (Auth::user()->isRole('secretarios'))
        <a href='{{url("/requerimientos/".Auth::user()->name."/municipio")}}'>
          <button type="button" class="btn btn-primary pull-right" style="margin-right: 8px;" name="button">Regresar</button>
        </a>
      @else
        <a href="{{url('/requerimientos/consult')}}">
          <button type="button" class="btn btn-primary pull-right" style="margin-right: 8px;" name="button">Regresar</button>
        </a>
      @endif
    </div>
    <div class="box-body"  >
      <div class="panel panel-primary ">
        <div class="panel-heading">Información general de la sede</div>
        <div class="table-responsive" >
          @foreach($sede as $sed)
            <table id="tablegeneral" class="table table-hover table-striped" cellspacing="0" width="100%">
              <thead>
                <tr>
                  <th>Departamento:</th>
                  <td>{{$sed->nombre_departamento}}</td>
                  <th>Código municipio:</th>
                  <td>{{$sed->codigo_dane_municipio}}</td>
                  <th>Municipio:</th>
                  <td>{{$sed->nombre_municipio}}</td>
                </tr>
                <tr>
                  <th>Código establecimiento:</th>
                  <td>{{$sed->codigo_establecimiento}}</td>
                  <th>Nombre establecimiento:</th>
                  <td>{{$sed->nombre_establecimiento}}</td>
                  <th>Código sede:</th>
                  <td>{{$sed->codigo_sede}}</td>
                </tr>
                <tr>
                  <th>Nombre sede:</th>
                  <td>{{$sed->nombre_sede}}</td>
                  <th>Zona:</th>
                  <td>{{$sed->zona}}</td>
                  <th>Dirección:</th>
                  <td>{{$sed->direccion}}</td>
                </tr>
                <tr>
                  <th>Teléfono:</th>
                  <td>{{$sed->telefono}}</td>
                  <th>Estado sede:</th>
                  <td>{{$sed->estado_sede}}</td>
                  <th>Niveles:</th>
                  <td>{{$sed->niveles}}</td>
                </tr>
                <tr>
                  <th>Modelos:</th>
                  <td>{{$sed->modelos}}</td>
                  <th>Grados:</th>
                  <td>{{$sed->grados}}</td>
                  <th></th>
                  <td></td>
                </tr>
              </thead>
            </table>
          @endforeach
        </div>
      </div>

      <div class="panel panel-primary ">
        <div class="panel-heading">Diagnóstico de necesidades registrados  - No evidenciados</div>
        <div class="table-responsive" >
          @if (count($SolicitudIF) >= 1)
            <table  class="table table-hover table-striped" style="text-align:center" cellspacing="0" width="100%">
              <thead>
                <tr>
                  <th>Id</th>
                  <th>Municipio</th>
                  <th>Establecimiento</th>
                  <th>Código establecimiento</th>
                  <th>Sede</th>
                  <th>Código sede</th>
                  <th>Zona</th>
                  <th>Dirección</th>
                  <th>Teléfono</th>
                  <th>Estado sede</th>
                  <th>Respuesta</th>
                  <th>Fecha solicitud</th>
                  <th>Acción</th>
                </tr>
              </thead>
              <tbody>
                @foreach($SolicitudIF as $solicitud)
                    <tr>
                    <td>{{$solicitud->id}}</td>
                    <td>{{$solicitud->temphistoricosedes->nombre_municipio}}</td>
                    <td><a href='{{url("/requerimientos/".$solicitud->Establecimiento->codigo_establecimiento."/establecimiento")}}'>{{$solicitud->Establecimiento->nombre_establecimiento}}</a></td>
                    <td>{{$solicitud->Establecimiento->codigo_establecimiento}}</td>
                    <td>{{$solicitud->temphistoricosedes->nombre_sede}}</td><td>{{$solicitud->temphistoricosedes->codigo_sede}}</td>
                    <td>{{$solicitud->temphistoricosedes->zona}}</td>
                    <td>{{$solicitud->temphistoricosedes->direccion}}</td>
                    <td>{{$solicitud->temphistoricosedes->telefono}}</td>
                    <td>{{$solicitud->temphistoricosedes->estado_sede}}</td>
                    <td>
                      <?php
                      if($solicitud->Respuesta != null){
                        ?>
                        @if(Auth::user()->isRole('administrador_sistema') || Auth::user()->isRole('administrador_infraestructura_fisica'))
                          <div  style='background:#B5FEC1; color:#3C763D'>1</div>
                        @elseif(Auth::user()->isRole('secretarios'))
                          @if($solicitud->Respuesta->VistoSecretario)
                            <div  style='color:#3C763D'>1</div>
                          @else
                            <div  style='background:#B5FEC1; color:#3C763D'>1</div>
                          @endif
                        @elseif(Auth::user()->isRole('rectores'))
                          @if($solicitud->Respuesta->VistoRector)
                            <div  style='color:#3C763D'>1</div>
                          @else
                            <div  style='background:#B5FEC1; color:#3C763D'>1</div>
                          @endif
                        @endif
                        <?php

                      }else{
                        echo 0;
                      }
                      ?>
                    </td>
                    <td>{{$solicitud->created_at}}</td>
                    <td>
                      <div class="row">
                      <a href="{{url('/requerimientos/'.$solicitud->id.'/view')}}">

                      <button type="button" class="btn btn-info btn-sm" style="margin-left: 5px;" title="Información"
                        ><i class="fa fa fa-info"></i></button>
                      </a>
                        <button type="button" onclick="confirmacion_borrado_solicitud({{ $solicitud->id }})" class="btn  btn-danger btn-xs" data-toggle="tooltip" title="Eliminar"><i class="fa fa fa-trash-o"></i></button>

</div>
                    </td>

                    </tr>

                @endforeach
              </tbody>
            </table>
            {{ $SolicitudIF->links() }}
          @else
            <br>
            <h4>No hay solicitudes o requerimientos no solucionados registrados para esta sede</h4>
            <br>
          @endif
        </div>
      </div>


      <div class="panel panel-primary ">
        <div class="panel-heading">Diagnóstico de necesidades registrados - Evidenciados</div>
        <div class="table-responsive" >
          @if (count($SolicitudIFAT) >= 1)
            <table  class="table table-hover table-striped" style="text-align:center" cellspacing="0" width="100%">
              <thead>
                <tr>
                  <th>Id</th>
                  <th>Municipio</th>
                  <th>Establecimiento</th>
                  <th>Código establecimiento</th>
                  <th>Sede</th>
                  <th>Código sede</th>
                  <th>Zona</th>
                  <th>Dirección</th>
                  <th>Teléfono</th>
                  <th>Estado sede</th>
                  <th>Respuesta</th>
                  <th>Fecha solicitud</th>
                  <th>Acción</th>
                </tr>
              </thead>
              <tbody>
                @foreach($SolicitudIFAT as $solNoatend)
                  <tr>
                    <td>{{$solNoatend->id}}</td>
                    <td>{{$solNoatend->temphistoricosedes->nombre_municipio}}</td>
                    <td><a href='{{url("/requerimientos/".$solNoatend->Establecimiento->codigo_establecimiento."/establecimiento")}}'>{{$solNoatend->Establecimiento->nombre_establecimiento}}</a></td>
                    <td>{{$solNoatend->Establecimiento->codigo_establecimiento}}</td>
                    <td>{{$solNoatend->temphistoricosedes->nombre_sede}}</td>
                    <td>{{$solNoatend->temphistoricosedes->codigo_sede}}</td>
                    <td>{{$solNoatend->temphistoricosedes->zona}}</td>
                    <td>{{$solNoatend->temphistoricosedes->direccion}}</td>
                    <td>{{$solNoatend->temphistoricosedes->telefono}}</td>
                    <td>{{$solNoatend->temphistoricosedes->estado_sede}}</td>
                    <td>
                      <?php
                      if($solNoatend->Respuesta != null){
                        ?>
                        @if(Auth::user()->isRole('administrador_sistema') || Auth::user()->isRole('administrador_infraestructura_fisica'))
                          <div  style='background:#B5FEC1; color:#3C763D'>1</div>
                        @elseif(Auth::user()->isRole('secretarios'))
                          @if($solNoatend->Respuesta->VistoSecretario)
                            <div  style='color:#3C763D'>1</div>
                          @else
                            <div  style='background:#B5FEC1; color:#3C763D'>1</div>
                          @endif
                        @elseif(Auth::user()->isRole('rectores'))
                          @if($solNoatend->Respuesta->VistoRector)
                            <div  style='color:#3C763D'>1</div>
                          @else
                            <div  style='background:#B5FEC1; color:#3C763D'>1</div>
                          @endif
                        @endif
                        <?php

                      }else{
                        echo 0;
                      }
                      ?>
                    </td>
                    <td>{{$solNoatend->created_at}}</td>
                    <td>
                      <a href="{{url('/requerimientos/'.$solNoatend->id.'/view')}}">
                        <button type="button" class="btn btn-info" style="margin-left: 5px;" data-placement="left" title="Información"
                        ><i class="fa fa fa-info"></i></button>
                      </a>
                    </td>
</tr>
                @endforeach
              </tbody>
            </table>
            {{ $SolicitudIFAT->links() }}
          @else
            <br>
            <h4>No hay solicitudes o requerimientos solucionados registrados para esta sede</h4>
            <br>
          @endif
        </div>
      </div>
    </div>
  </div>


@endsection
