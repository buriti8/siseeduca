@extends('layouts.app')
@section('htmlheader_title')
  Requerimientos Infraestructura Tecnológica
@endsection

@section('main-content')
<style type="text/css">
  #tablegeneral th{
    vertical-align: middle;
    border-bottom: none;
    text-align: right;
  }
  #tablegeneral td{
    text-align: left;
    vertical-align: middle;
  }

  #tablegeneral tr{
    border-bottom: 1px solid #ddd;
  }
</style>
  <section  id="contenido_principal">
    <div class="box box-primary">
      <div align="center" class="box-header">
        <h3 class="box-title"><strong>Diagnóstico de necesidades y requerimientos Infraestructura Tecnológica</strong></h3>

        @if(Auth::user()->isRole('rectores'))
          <a href='{{url("/requerimientos/".Auth::user()->name."/establecimiento")}}'>
            <button type="button" class="btn btn-primary pull-right" style="margin-right: 8px;" name="button">Regresar</button>
          </a>
        @elseif (Auth::user()->isRole('secretarios'))
          <a href='{{url("/requerimientos/".Auth::user()->name."/municipio")}}'>
            <button type="button" class="btn btn-primary pull-right" style="margin-right: 8px;" name="button">Regresar</button>
          </a>
        @else
          <a href="{{url('/requerimientos/consult')}}">
            <button type="button" class="btn btn-primary pull-right" style="margin-right: 8px;" name="button">Regresar</button>
          </a>
        @endif

      </div>
          <input type="hidden" value="{{ Auth::user()->name }}" name="userSolicitud">
          <div class="box-body ">
            @foreach($SolicitudIF as $solicitud)
            <?php $sed = $solicitud->temphistoricosedes ?>
            <div class="panel panel-primary ">
              <div class="panel-heading" style="font-size: 16px;">Información general de la sede</div>
              <div class="panel-body">
                <table id="tablegeneral" class="table table-hover table-striped" cellspacing="0" width="100%">
                  <thead>
                    <tr>
                      <th>Departamento:</th>
                      <td>{{$sed->nombre_departamento}}</td>
                      <th>Código municipio:</th>
                      <td>{{$sed->codigo_dane_municipio}}</td>
                      <th>Municipio:</th>
                      <td>{{$sed->nombre_municipio}}</td>
                    </tr>
                    <tr>
                      <th>Código establecimiento:</th>
                      <td>{{$sed->codigo_establecimiento}}</td>
                      <th>Nombre establecimiento:</th>
                      <td>{{$sed->nombre_establecimiento}}</td>
                      <th>Código sede:</th>
                      <td>{{$sed->codigo_sede}}</td>
                    </tr>
                    <tr>
                      <th>Nombre sede:</th>
                      <td>{{$sed->nombre_sede}}</td>
                      <th>Zona:</th>
                      <td>{{$sed->zona}}</td>
                      <th>Dirección:</th>
                      <td>{{$sed->direccion}}</td>
                    </tr>
                    <tr>
                      <th>Teléfono:</th>
                      <td>{{$sed->telefono}}</td>
                      <th>Estado sede:</th>
                      <td>{{$sed->estado_sede}}</td>
                      <th>Niveles:</th>
                      <td>{{$sed->niveles}}</td>
                    </tr>
                    <tr>
                      <th>Modelos:</th>
                      <td>{{$sed->modelos}}</td>
                      <th>Grados:</th>
                      <td>{{$sed->grados}}</td>
                      <th></th>
                      <td></td>
                    </tr>
                  </thead>
                </table>
              </div>
            </div>
          </div>
          <div class="box-body">
            <div class="panel panel-primary ">
              <div class="panel-heading" style="font-size: 16px;">Descripción de diagnóstico de necesidades</div>
              <input type="hidden" value="0" name="nrows">
              <div class="table-responsive" style="padding:2em;">
                <div class="col-md-12">
                    <div class="titleboxes" style="font-size:15px; text-align:center;"></div>
                    <div class="col-md-12" style="padding:0px;">
                        <div class="col-md-3" style="padding-top:15px;padding-bottom:15px;">
                            <div class="form-group">
                                <label>Tipo de solicitud <span style="color:red;font-size:20px;">*</span></label>
                                <select id="tipo_solicitud" name="tipo_solicitud" class="form-control" required>
                                    @if($solicitud->tipo_solicitud == 1)
                                        <option value="1" selected>Dispositivo</option>
                                        <option value="2">Conectividad</option>
                                    @else
                                        <option value="1">Dispositivo</option>
                                        <option value="2" selected>Conectividad</option>
                                    @endif
                                </select>
                            </div>


                        </div>
                    </div>

                    <div class="col-md-12" style="padding:0px;">
                        <div class="col-md-3">
                            <div class="div_dispositivos" style="@if($solicitud->tipo_solicitud == 2) display:none @endif; padding-top:2px;padding-bottom:10px;">
                                <div class="form-group">
                                    <label>Seleccione el tipo de dispositivo <span style="color:red;font-size:20px;">*</span></label>
                                    <select id="tipo_id" name="tipo_id" class="form-control">
                                        @if($solicitud->tipo_solicitud == 1)
                                            @foreach($tipos as $tipo)
                                                <option value="{{ $tipo->id }}" @if($solicitud->tipodispositivo_id == $tipo->id) selected @endif>{{ $tipo->nombre }}</option>
                                            @endforeach
                                        @else
                                            @foreach($tipos as $tipo)
                                                <option value="{{ $tipo->id }}">{{ $tipo->nombre }}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                </div>
                                <div class="form-group "  >
                      <label for="cantidad_d">Cantidad de dispositivos</label>
                      <input name="cantidad_d" id="cantidad_d" type="number"   class="archivo form-control" value="{{ $solicitud->cantidad_d }}" maxlength="2" min="1" max="99"/>
                    </div>

                            </div>

                            <div class="div_conectividad" style="padding-top:2px;padding-bottom:10px;">
                            <div class="form-group">
                              <label>Seleccione el tipo de uso <span style="color:red;font-size:20px;">*</span></label>
                              <select id="tipo_uso" name="tipo_uso" class="form-control"  >
                              <option>{{ $solicitud->tipo_uso }}</option>
                                <option></option>
                                <option>Educativo</option>

                              </select>


                            </div>
                          </div>
                        </div>
                    </div>
                </div>
              </div>

              <div class="col-md-12">
                  <div class="titleboxes" style="font-size:15px; text-align:center;"></div>
                  <div class="col-md-12" style="padding-top:15px;padding-bottom:15px;">
                      <div class="form-group" >
                          <label>Debe ingresar el  motivo del diagnóstico de necesidad <span style="color:red;font-size:20px;">*</span></label>
                          <textarea rows="4" cols="50" type="text" id="descripcion" name="descripcion" class="form-control" maxlength=230 required>{{ $solicitud->descripcion }}</textarea>
                      </div>
                  </div>
              </div>
              <div class="col-md-12">
                  <div class="titleboxes" style="font-size:15px; text-align:center;"></div>
                  <div class="col-md-12" style="padding-top:15px;padding-bottom:15px;">
                      <div class="form-group">
                        <label><strong>Nota:el archivo no debe superar los 5 megabytes (MB).</strong></label>
                        <label>Agregar archivo <span class="label label-primary">PDF</span> / <span class="label label-primary">Imagen</span></label>
                          <input type="file" id="solicitud_fisica" name="solicitud_fisica" maxlength=200>
                      </div>
                  </div>
              </div
              @if ($solicitud->solicitud_fisica != "")
                  <div class="col-md-12" style="padding-top:15px;padding-bottom:15px;">
                      <div class="form-group">
                          <a href="/down{{$solicitud->solicitud_fisica}}" target="_blank" id="descargar_solicitud" name="descargar_solicitud"  class="btn btn-danger pull-left glyphicon glyphicon-download-alt " style="margin-right:50px;"> DESCARGAR SOLICITUD</a>
                      </div>
                  </div>
              @else
                  <div class="col-md-12" >
                      <label>No hay archivo djunto</label>
                  </div>
              @endif



              <div class="col-md-12">
                  <div class="titleboxes" style="font-size:15px; text-align:center;"></div>
                    <div class="col-md-12" style="padding-top:15px;padding-bottom:15px;">


                    @if (!$solicitud->respuesta && (Auth::user()->isRole('administrador_sistema')  || Auth::user()->isRole('administrador_infraestructura_tecnologica')))
                    <form method="post" action="{{url('/update/solicitudt')}}">
                      {{ csrf_field() }}
                      <input type="hidden" value="{{$solicitud->id}}" name="valueupdatee"/>
                      <button onclick="return confirm('Tenga en cuenta: DOTACIÓN: Se verificó que la sede no llega al índice de alumnos por dispositivo, que la cantidad de dispositivos no supere la cantidad de alumnos y se verificó la cobertura contratada. CONECTIVIDAD: La sede no cuenta con un servicio de conectividad pagado por la Gobernación de Antioquia.')"  type="submit" id="btnsubmit"  class="btn btn-primary pull-right"> Caso evidenciado</button>

                    </form>
                    @if($solicitud->Respuesta == null)

                    <button type="button" id="btnRegRespSolicitudt" style="margin-right:0.5em;" class="btn btn-primary pull-right" > Responder</button>

                    @else
                      <button type="button" id="btnEdtRespSolicitudt" style="margin-right:0.5em;" class="btn btn-primary pull-right" > Editar Respuesta</button>
                      @endif
                    @endif
                    </div>

                      </div>
                  </div>

                  <div class="box-footer">
                    <div class="panel panel-primary" id="Respuestaa">
                      <input type="hidden" value="{{$solicitud->Respuesta}}" id="exisresponsee">
                      <div class="panel-heading">Respuesta - Secretaría de Educación, Subsecretaría de Innovación.</div>
                      <div class="panel-body">
                        <form method="post" action="{{url('requerimientos/Respuesta')}}">
                          <input type="hidden" value="" name="urlActuall" id="urlActuall">
                          {{ csrf_field() }}
                          @if($solicitud->Respuesta != null)
                            <label for="observacion">Fecha de respuesta: {{$solicitud->Respuesta->Fecha}}</label>
                            <input type="hidden" name="RespuestaId" id="RespuestaId" value="{{$solicitud->Respuesta->id}}">
                            <textarea readonly="true" class="form-control" style="resize:none" rows="5" id="RespuestaSolicitudd" placeholder="Escriba aquí su respuesta"  name="RespuestaSolicitudd">{{$solicitud->Respuesta->Mensaje}}</textarea>
                          @else
                            <textarea readonly="true" class="form-control" style="resize:none" rows="5" id="RespuestaSolicitudd" placeholder="Escriba aquí su respuesta" name="RespuestaSolicitudd" required></textarea>
                          @endif
                          <input type="hidden" name="idSolicitud" value="{{$solicitud->id}}">
                          <input type="hidden" name="tipoFormn" id="tipoFormn" value="">
                          <button type="button" id="btnCancelaRespuestt" style="margin-right:0.5em; display:none;" class="btn btn-primary pull-right"> Cancelar</button>
                          <button type="submit" id="btnConfirmEditRespuestaSolicitudd" style="margin-right:0.5em; display:none;" class="btn btn-primary pull-right" > Guardar</button>
                        </form>



                      </div>
                    </div>
                  </div>
              </div>



            @endforeach
        </div>

  @endsection
