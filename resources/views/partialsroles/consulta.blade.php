<section>

    <div class="panel panel-primary ">
        <div class="panel-heading">Establecimientos</div>
        <div class="panel-body">
            <div class="form-group col-xs-6"  >
                <div id='selEstablecimientoContenedor'>
                    <label for="selEstablecimiento">Seleccionar un establecimiento educativo</label>
                    <select  id="selEstablecimiento" name="selEstablecimiento" class="form-control  col-xs-6"  required>
                    <option selected="selected" value="">Elige uno</option>

                    </select>
                </div>
            </div>
            <div class="form-group col-xs-6"  >
                <div id='selEstablecimientoContenedor'>
                    <label for="selSede">Seleccionar una sede educativa</label>
                    <select  id="selSede" name="selSede" class="form-control  col-xs-6" onchange="" required>
                    <option value="">Elige una</option>
                    </select>
                </div>
            </div>
            <div class="form-group col-xs-12"  >
                <select id="sede_id" name="sede_id" class="form-control" style="width:60%;" required>
                    @foreach($sedes as $sede)
                        <option value="{{ $sede->codigo_sede }}">{{$sede->nombre_sede}}</option>                                    
                    @endforeach
                </select>
            </div>
        </div>

    </div>

</section>
