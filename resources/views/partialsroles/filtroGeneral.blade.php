<label for="selSubregion">Seleccionar subregión</label>
<select  id="selSubregion" name="selSubregion" class="form-control  col-xs-6" onchange="filtroSubregionMunicipios()">
  <option value="">TODAS</option>
  @foreach ($subregiones as $subregion)
    <option value="{{$subregion->CodigoSubregion}}">{{$subregion->NombreSubregion}}</option>
  @endforeach
</select>
<br>
<div id='selMunicipioContenedor' style="display:none">
  <label for="selMunicipio">Seleccionar municipio</label>
  <select  id="selMunicipio" name="selMunicipio" class="form-control  col-xs-6" onchange="filtroMunicipios()">
  </select>
</div>
