<section>

<!-- Filtro editar traslado con rol rector -->
<div class="panel panel-primary ">
<div class="box-body">
    <div class="form-group col-xs-12">
        <div class="form-group col-xs-6">
            <div id='div_sedes'>
                <label for="sedes">SEDES</label>
                <select  id="sede_id" name="sede_id" class="form-control" required>
                <option value="{{ $traslado->sede_destino_id }}">{{ $nombre_sede_destino }}</option>
                </select>
            </div>
        </div>
    </div>
</div>
</div>

<!-- Ver sedes -->
  <script type="text/javascript">
    $(document).ready(function() {

        var select = "codigo_establecimiento";
        var value =  {{ Auth::user()->name }};
        var dependent = $(this).data('dependent');
        var _token = $('input[name="_token"]').val();
        var valor = "";

        if ($(this).val() != 0) {
          valor = "uno";
          $.ajax({
            url: "{{ route('sedes_establecimientos') }}",
            method: "POST",
            data: {
              select: select,
              value: value,
              _token: _token,
              valor: valor,
            },
            success: function(result) {
              $('#sede_id').html(result);
            }
          })
        } else {
          valor = "todos";
          $.ajax({
            url: "{{ route('sedes_establecimientos') }}",
            method: "POST",
            data: {
              select: select,
              value: value,
              _token: _token,
              valor: valor,
            },
            success: function(result) {
              $('#sede_id').html(result);
              document.getElementById("sede_id").value = {{ $traslado->sede_destino_id }};
            }
          })
        }
        //fin else
    });
</script>


</section>
