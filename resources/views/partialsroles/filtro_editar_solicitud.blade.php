<section>

<!-- Filtra en editar solicitud con el rol Admistrador -->
<div class="panel panel-primary ">
<div class="box-body">
    <div class="form-group col-xs-12">
        <div class="form-group col-xs-6">
            <div id='div_subregiones'>
                <label for="subregiones">SUBREGIONES</label>
                <select  id="subregiones" name="subregiones" class="form-control subreg" data-dependent="municipios">
                    <option value="">SELECCIONE UNA SUBREGIÓN...</option>
                    @foreach($subregiones as $subregion)
                        <option value="{{ $subregion->id }}">{{ $subregion->NombreSubregion  }}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class="form-group col-xs-6">
            <div id='div_municipios'>
                <label for="municipios">MUNICIPIOS</label>
                <select  id="municipios" name="municipios" class="form-control munic" data-dependent="establecimientos">
                    <option value="">SELECCIONE UN MUNICIPIO...</option>
                </select>
            </div>
        </div>
    </div>

    <div class="form-group col-xs-12">
         <div class="form-group col-xs-6"  >
            <div id='div_establecimientos'>
                <label for="establecimientos">ESTABLECIMIENTOS</label>
                <select  id="establecimientos" name="establecimientos" class="form-control estab" data-dependent="sede_id">
                    <option value="">SELECCIONE UN ESTABLECIMIENTO...</option>
                </select>
            </div>
        </div>
        <div class="form-group col-xs-6">
            <div id='div_sedes'>
                <label for="sedes">SEDES</label>
                <select  id="sede_id" name="sede_id" class="form-control" required>
                    <option value="{{ $solicitud->sede_id }}">{{ $nombre_sede }}</option>                                    
                </select>
            </div>
        </div>
    </div>

</div>
</div>

<!-- Ver municipios -->
<script type="text/javascript">
      $('.subreg').change(function() {

        var select = $(this).attr("id");
        var value = $(this).val();
        var dependent = $(this).data('dependent');
        var _token = $('input[name="_token"]').val();
        var valor = "";

        if ($(this).val() != 0)
        {
          valor = "uno";
          $.ajax({
            url: "{{ route('municipios_subregion') }}",
            method: "POST",
            data: {
              value: value,
              _token: _token,
              dependent: dependent,
              valor: valor
            },
            success: function(result) {
               $('#' + dependent).html(result);

               $('#establecimientos').html("<option value=''>SELECCIONE UN ESTABLECIMIENTO...</option>");
               $('#sede_id').html("<option value=''>SELECCIONE UNA SEDE...</option>");
            }
          })
        }
        else
        {
          valor = "todos";
          $.ajax({
            url: "{{ route('municipios_subregion') }}",
            method: "POST",
            data: {
              select: select,
              value: value,
              _token: _token,
              dependent: dependent,
              valor: valor
            },
            success: function(result) {
              $('#' + dependent).html(result);

              $('#establecimientos').html("<option value=''>SELECCIONE UN ESTABLECIMIENTO...</option>");
              $('#sede_id').html("<option value=''>SELECCIONE UNA SEDE...</option>");
            }
          })
        }
      });
</script>

<!-- Ver establecimientos -->
<script type="text/javascript">
      $('.munic').change(function() {

        var select = "codigo_dane_municipio";
        var value = $(this).val();
        var location = "matricula";
        var dependent = $(this).data('dependent');
        var _token = $('input[name="_token"]').val();
        var valor = "";

        if ($(this).val() != 0)
        {
          valor = "uno";
          $.ajax({
            url: "{{ route('establecimientos_municipio') }}",
            method: "POST",
            data: {
              value: value,
              _token: _token,
              dependent: dependent,
              valor: valor,
            },
            success: function(result) {
              $('#' + dependent).html(result);
              $('#sede_id').html("<option value=''>SELECCIONE UNA SEDE...</option>");
            }
          })
        }
        else
        {
          valor = "todos";
          $.ajax({
            url: "{{ route('establecimientos_municipio') }}",
            method: "POST",
            data: {
              value: value,
              _token: _token,
              dependent: dependent,
              valor: valor,
            },
            success: function(result) {
              $('#' + dependent).html(result);
              $('#sede_id').html("<option value=''>SELECCIONE UNA SEDE...</option>");
            }

          })
        }
      });
  </script>

  <!-- Ver sedes -->
  <script type="text/javascript">
    $('.estab').change(function() {

        var select = "codigo_establecimiento";
        var value =  $(this).val();
        var dependent = $(this).data('dependent');
        var _token = $('input[name="_token"]').val();
        var valor = "";

        if ($(this).val() != 0) {
          valor = "uno";
          $.ajax({
            url: "{{ route('sedes_establecimientos') }}",
            method: "POST",
            data: {
              select: select,
              value: value,
              _token: _token,
              dependent: dependent,
              valor: valor,
            },
            success: function(result) {
              $('#' + dependent).html(result);
            }
          })
        } else {
          valor = "todos";
          $.ajax({
            url: "{{ route('sedes_establecimientos') }}",
            method: "POST",
            data: {
              select: select,
              value: value,
              _token: _token,
              dependent: dependent,
              valor: valor,
            },
            success: function(result) {
              $('#' + dependent).html(result);
            }
          })
        }
        //fin else
    });
</script>

</section>
