<section>

<!-- Filtro editar traslado con rol secretario -->
<div class="panel panel-primary ">
<div class="box-body">
    <div class="form-group col-xs-12">
         <div class="form-group col-xs-6"  >
            <div id='div_establecimientos'>
                <label for="establecimientos">ESTABLECIMIENTOS</label>
                <select  id="establecimientos" name="establecimientos" class="form-control estab" data-dependent="sede_id">
                    <option value="">SELECCIONE UN ESTABLECIMIENTO...</option>
                </select>
            </div>
        </div>
        <div class="form-group col-xs-6">
            <div id='div_sedes'>
                <label for="sedes">SEDES</label>
                <select  id="sede_id" name="sede_id" class="form-control" required>
                    <option value="{{ $traslado->sede_destino_id }}">{{ $nombre_sede }}</option>                                                 
                </select>
            </div>
        </div>
    </div>

</div>
</div>

<script type="text/javascript">
    $(document).ready(function(){

        // Ver establecimientos
        var select = "codigo_dane_municipio";
        var value = $(this).val();
        var dependent = 'establecimientos';
        var _token = $('input[name="_token"]').val();
        var valor = "";

        $.ajax({
        url: "{{ route('establecimientos_municipio') }}",
        method: "POST",
        data: {
            value: value,
            _token: _token,
            dependent: dependent,
            valor: valor,
        },
        success: function(result) {
            $('#' + dependent).html(result);
            // $('#sede_id').html("<option value=''>SELECCIONE UNA SEDE...</option>");

        }
        })
    });
</script>

  <!-- Ver sedes -->
  <script type="text/javascript">
    $('.estab').change(function() {

        var select = "codigo_establecimiento";
        var value =  $(this).val();
        var dependent = $(this).data('dependent');
        var _token = $('input[name="_token"]').val();
        var valor = "";

        $.ajax({
        url: "{{ route('sedes_establecimientos') }}",
        method: "POST",
        data: {
            select: select,
            value: value,
            _token: _token,
            dependent: dependent,
            valor: valor,
        },
        success: function(result) {
            $('#' + dependent).html(result);
        }
        })
    });
</script>

</section>
