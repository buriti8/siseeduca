<section>

<!-- Filtro de nuevo traslados con el rol de secretario -->

<div class="box-body">
    <div class="form-group col-xs-12">
         <div class="form-group col-xs-6"  >
            <div id='div_establecimientos'>
                <label for="establecimientos">Establecimientos</label>
                <select  id="establecimientos" name="establecimientos" class="form-control estab" data-dependent="sede_id" required>
                    <option value="">Seleccione un establecimiento...</option>
                </select>
            </div>
        </div>
    </div>

</div>


<script type="text/javascript">
    $(document).ready(function(){

        // Ver establecimientos
        var select = "codigo_dane_municipio";
        var value = $(this).val();
        var dependent = 'establecimientos';
        var _token = $('input[name="_token"]').val();
        var valor = "";

        $.ajax({
        url: "{{ route('establecimientos_municipio') }}",
        method: "POST",
        data: {
            value: value,
            _token: _token,
            dependent: dependent,
            valor: valor,
        },
        success: function(result) {
            $('#' + dependent).html(result);
            $('#sede_id').html("<option value=''>Seleccione una sede...</option>");

        }
        })
    });
</script>

  <!-- Ver sedes -->
  <script type="text/javascript">
    $('.estab').change(function() {

        var select = "codigo_establecimiento";
        var value =  $(this).val();
        var dependent = $(this).data('dependent');
        var _token = $('input[name="_token"]').val();
        var valor = "";

        $.ajax({
        url: "{{ route('sedes_establecimientos') }}",
        method: "POST",
        data: {
            select: select,
            value: value,
            _token: _token,
            dependent: dependent,
            valor: valor,
        },
        success: function(result) {
            $('#' + dependent).html(result);
        }
        })
    });
</script>

</section>
