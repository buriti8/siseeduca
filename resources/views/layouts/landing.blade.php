<!DOCTYPE html>

<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="Sistema de información Educativo ">
  <meta property="og:type" content="website" />

  <meta name="twitter:card" content="summary_large_image" />
  <meta name="twitter:site" content="@acachawiki" />
  <meta name="twitter:creator" content="@acacha1" />

  <title>Sistema de Información Educativo</title>

  <!-- Custom styles for this template -->
  <link href="{{ asset('/css/all-landing.css') }}" rel="stylesheet">
  <link rel="icon" href="/img/Gob.png" type="image/png" sizes="16x16">
  <link href='http://fonts.googleapis.com/css?family=Lato:300,400,700,300italic,400italic' rel='stylesheet' type='text/css'>
  <link href='http://fonts.googleapis.com/css?family=Raleway:400,300,700' rel='stylesheet' type='text/css'>

</head>

<body data-spy="scroll" data-offset="0" data-target="#navigation" >

  <div   id="app">
    <!-- Fixed navbar -->
    <div id="navigation" style="background-color:white;" class="navbar navbar-default navbar-fixed-top">

      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
        </div>

        <img src="img/goberna.png" width="225" style="margin-top: 7px;" height="225" align="left"  class="img-responsive"  />

        <ul   class="nav navbar-nav navbar-right" >

          @if (Auth::guest())
            <h3><a href="{{ url('/login') }}">
              <button type="button" style=" margin-top: 10px; background-color:#34495E;" class="btn btn-primary btn-lg">Iniciar sesión</button></a></h3>

            @else


              <div style="margin-top:40px;" class="pull-right">
                <!--Cuando el usuario regresa al landing; los siguientes botones le permiten
                decidir si continua en el sistema o si da la sesión por terminada-->
                <a class="btn btn-primary" style="background-color:#34495E;"  href="{{url('home')}}">Volver a inicio</a>
                &nbsp;&nbsp;&nbsp;&nbsp;
                <a href="{{ url('/logout') }}" class="btn btn-danger"
                onclick="event.preventDefault();
                document.getElementById('logout-form').submit();">
                {{ trans('adminlte_lang::message.signout') }}
              </a>

              <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                {{ csrf_field() }}
                <input type="submit" value="logout" style="display: none;">
              </form>

            </div>

          @endif
        </ul>

      </div><!--/.nav-collapse -->
    </div>
  </div>
</div>
</div>
</div>
</div>


<section id="showcase" name="showcase"></section >
  <div id="showcase">

    <div class="container">

      <div class="row">
        <h1 class="centered" style="color:white" >Bienvenido a SI-SeEduca</h1>
        <h5 class="centered" style="color:white">Sistema de información educativo de la Gobernación de Antioquia </h5>
        <br>
        <div class="col-lg-8 col-lg-offset-2">
          <div id="carousel-example-generic" class="carousel slide" >

            <!-- Indicators -->
            <ol class="carousel-indicators">
              <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
              <li data-target="#carousel-example-generic" data-slide-to="1"></li>
              <li data-target="#carousel-example-generic" data-slide-to="2"></li>
              <li data-target="#carousel-example-generic" data-slide-to="3"></li>
            </ol>

            <!-- Wrapper for slides -->
            <div class="carousel-inner">
              <div class="item active">
                <img src="{{ asset('/img/PANTALLA.png') }}" alt="">
              </div>
              <div class="item">
                <img src="{{ asset('/img/PANTALLA.png') }}" alt="">
              </div>
              <div class="item">
                <img src="{{ asset('/img/PANTALLA.png') }}" alt="">
              </div>
              <div class="item">
                <img src="{{ asset('/img/PANTALLA.png') }}" alt="">
              </div>
              <!-- Controls -->
              <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
                <span class="glyphicon glyphicon-chevron-left"></span>
              </a>
              <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
                <span class="glyphicon glyphicon-chevron-right"></span>
              </a>
              <!-- Carousel -->
            </div>
          </div>
        </div>
      </div>
      <br>
      <br>
      <br>
    </div><!-- /container -->
  </div>

</div>

<section id="desc" name="desc"></section>
<!-- INTRO WRAP -->
<div id="intro">
  <div class="container">
    <div class="row centered">
      <h1>¿Quiénes somos?</h1>
      <br>
      <br>
      <div class="col-lg-4">
        <a href="http://www.antioquia.gov.co/">
          <img src="{{ asset('/img/Gob.png') }}" alt=""height="250" width="250">
        </a>
        <h3>Gobernación de Antioquia</h3>
        <p>Software exclusivo de la <a href="http://antioquia.gov.co/index.php/2014-01-03-13-49-44/2016-02-21-02-28-53">Gobernación de Antioquia.</a>  </p>
      </div>
      <div class="col-lg-4" >
        <img src="{{ asset('/img/Team.png') }}" alt="" height="250" width="250">
        <h3>Equipo desarrollador</h3>
        <p>El equipo está conformado por profesionales en formación, los cuales cuenta con el conocimiento, la motivación y las ganas de aprender.</p>
      </div>
      <div class="col-lg-4">
        <img src="{{ asset('/img/periodico.png') }}" alt="" height="250" width="250">
        <h3>Temas de interés</h3>
        <p>Actualmente en la Gobernación de Antioquia se están llevando a cabo muchos proyectos con la intención de mejorar la calidad de vida de las personas.</p>
      </div>
    </div>
    <br>
    <hr>
  </div> <!--/ .container -->
</div><!--/ #introwrap -->

<!-- FEATURES WRAP -->

<div id="showcase">

  <div class="container">

    <div class="row">
      <h1 class="centered" style ="color:white">¿En qué consiste el sistema? </h1>
      <br>
      <br>
      <div class="col-lg-6 centered">
        <img class="centered" src="{{ asset('/img/sistema6.png') }}" alt="" >
      </div>

      <div class="col-lg-6" style="color:white">

        <br>
        <!-- ACCORDION -->
        <div class="accordion ac" id="accordion2">
          <div class="accordion-group">
            <div class="accordion-heading">
              <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne">
                ¿Cuál es su finalidad?
              </a>
            </div><!-- /accordion-heading -->
            <div id="collapseOne" class="accordion-body collapse in">
              <div class="accordion-inner">
                <p> La finalidad de este gran proyecto, es contribuir al cumplimiento del plan de desarrollo de la Secretaría de Educación de la Gobernación de Antioquia, por medio de la estrategia de ecosistemas de innovación. </p>
              </div><!-- /accordion-inner -->
            </div><!-- /collapse -->
          </div><!-- /accordion-group -->
          <br>

          <div class="accordion-group">
            <div class="accordion-heading">
              <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseTwo">
                ¿En qué consiste?
              </a>
            </div>
            <div id="collapseTwo" class="accordion-body collapse">
              <div class="accordion-inner">
                <p>Consiste en desarrollar un sistema modular, que integre las diferentes fuentes de información de los 117 municipios no certificados, mejorando el flujo entre las diferentes áreas de la secretaria por medio de herramientas de manejo de información y desarrollo de interfaces gráficas </p>
              </div><!-- /accordion-inner -->
            </div><!-- /collapse -->
          </div><!-- /accordion-group -->
          <br>

          <div class="accordion-group">
            <div class="accordion-heading">
              <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseThree">
                ¿Hacia quién está dirigido?
              </a>
            </div>
            <div id="collapseThree" class="accordion-body collapse">
              <div class="accordion-inner">
                <p>Este Proyecto está dirigido a todos los funcionarios publicos del area de la gobernación que necesiten realizar consultas o reportes </p>
              </div><!-- /accordion-inner -->
            </div><!-- /collapse -->
          </div><!-- /accordion-group -->
          <br>

          <div class="accordion-group">
            <div class="accordion-heading">
              <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseFour">
                ¡Esta es la etapa 1!
              </a>
            </div>
            <div id="collapseFour" class="accordion-body collapse">
              <div class="accordion-inner">
                <p>Actualmente se esta desarrollando la primera etapa, la cual consiste en realizar algunos modulos de carga de información y de consulta </p>
              </div><!-- /accordion-inner -->
            </div><!-- /collapse -->
          </div><!-- /accordion-group -->
          <br>
        </div><!-- Accordion -->
      </div>
    </div>
  </div><!--/ .container -->
</div><!--/ #features -->

</div>


<div id="intro">


  <div class="container">

    <div class="row centered">
      <div class="col-lg-12">

      </div>
      <div class="col-lg-2">

        <h5>Linea de atención</h5>

        <p>01 8000 415 221</p>
        <img class="hidden-xs hidden-sm hidden-md" src="{{ asset('/img/arrow1.png') }}">
      </div>
      <div class="col-lg-8">
        <img class="img-responsive" src="{{ asset('/img/contactanos2.png') }}" alt="">
      </div>
      <div class="col-lg-2">
        <br>
        <img class="hidden-xs hidden-sm hidden-md" src="{{ asset('/img/arrow2.png') }}">
        <h5>Atención virtual </h5>
        <p> Envíanos tus sugerencias o dudas <a href="http://antioquia.gov.co/index.php/atencion-a-la-ciudadania">¡Click Aquí!</a> </p>
      </div>
    </div>
  </div> <!--/ .container -->
  <!--/ #headerwrap </div>-->
</div>


<div id="c">

  <div class="container">
    <p>
      <a href="http://www.antioquia.gov.co/"></a><b>Gobernación de Antioquia </b></a>| Secretaría de Educación<br/>
      Creado por<a href="http://www.antioquia.gov.co"> Equipo de Desarrollo de la Gobernación de Antioquia</a>
      <br/>

      2019 <a href="http://www.antioquia.gov.co">.</a>
    </p>

  </div>

</div>
</div>

<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="{{ asset('/js/app.js') }}"></script>
<script src="{{ asset('/js/smoothscroll.js') }}"></script>
<script>
$('.carousel').carousel({
  interval: 3500
})
</script>
</body>
</html>
