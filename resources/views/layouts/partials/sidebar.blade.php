<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">

  <!-- sidebar: style can be found in sidebar.less -->
  <section class="sidebar">
    <br>
    <!-- Sidebar user panel (optional) -->
    @if (! Auth::guest())
      <div class="user-panel">
        <div class="pull-left image">
          <img src="{{ url('img/avatar_plusis.jpg') }}" class="img-circle" alt="Imagen usuario" />
        </div>
        <div class="pull-left info">
          <p>{{ Auth::user()->lastname }}</p>
          <!-- Status -->

        </div>
      </div>
    @endif

    <!-- Sidebar Menu -->

    <ul class="sidebar-menu" data-widget="tree">
      <li class="header" style="text-align:center; color:white; font-size:14px;">Menú Principal</li>

      @can('carga_masiva_matricula')
        <li class="treeview">
          <a href="#">
            <i class="fa fa-book"></i> <span>Matrícula</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            @can('carga_masiva_matricula')
              <li><a href="{{url('cargar_datos')}}"><i class="fa fa-cloud-upload"></i>Cargar anexos</a></li>
            @else
            @endcan
          </ul>
        </li>
      @endcan

      @can('carga_masiva_due')
        <li class="treeview">
          <a href="#">
            <i class="fa fa-university"></i> <span>DUE</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">

            @can('carga_masiva_due')
              <li><a href="{{url('cargar_datos_due')}}"><i class="fa fa-cloud-upload"></i>Cargar anexos</a></li>
            @else
            @endcan
          </ul>
        </li>
      @endcan

      @can('carga_masiva_pruebas')
        <li class="treeview">
          <a href="#">
            <i class="fa fa-area-chart"></i> <span>Pruebas Saber</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            @can('carga_masiva_pruebas')
              <li><a data-dismiss="modal" data-toggle="modal" data-target="#miModal"><i class="fa fa-cloud-upload"></i>Cargar anexos</a></li>
            @else
            @endcan
          </ul>
        </li>
      @endcan

      @can('conectividad_propia')
      <li class="treeview">
        <a href="#">
          <i class="fa fa-desktop"></i> <span>Infraestructura tecnológica</span>
          <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
        </a>
        <ul class="treeview-menu">

          @can('conectividad_propia')
            <a>Conectividad</a>

            <li><a href="{{url('individual')}}"><i class="fa fa-edit"></i>Conectividad propia</a></li>
          @else

          @endcan
          @can('carga_masiva_conectividad')
            <li><a href="{{url('cargar_datos_conectividad')}}"><i class="fa fa-cloud-upload"></i>Cargar conectividad</a></li>

          @else

          @endcan

          @can('carga_masiva_calidad')
            <li><a href="{{url('cargar_datos_calidad')}}"><i class="fa fa-cloud-upload"></i>Cargar calidad</a></li>
          @else
          @endcan

          @can('crear_dispositivos')
            <a>Inventario</a>

            <li><a href="{{url('listado_dispositivos')}}"><i class="fa fa-tablet"></i>Dispositivos</a></li>
          @else
          @endcan

          @can('estructura_basica_inventario')
            <li><a href="{{url('editar_estructura_inventario')}}"><i class="fa fa-edit"></i>Estructura básica</a></li>
          @else
          @endcan

          @can('carga_masiva_dispositivos')
            <li><a href="{{url('carga_masiva_dispositivos')}}"><i class="fa fa-cloud-upload"></i>Carga dispositivos</a></li>
          @else
          @endcan

          @can('traslado_dispositivos')
            <li><a href="{{url('traslado_dispositivos')}}"><i class="fa fa-mail-reply"></i>Traslados realizados</a></li>
          @else
          @endcan

          @can('crear_solicitud_tecnologica')
         <a>Diagnóstico de necesidades</a>
            <li><a href="{{url('form_nuevo_modal')}}"><i class="fa fa-calendar"></i>Diagnóstico</a></li>

            @if(Auth::user()->isRole('rectores'))
              <li><a href='{{url("/requerimientos/".Auth::user()->name."/establecimiento")}}'><i class="fa fa-calendar"></i> <span>Diagnóstico registrados</span></a></li>
            @elseif (Auth::user()->isRole('secretarios'))
              <li><a href='{{url("/requerimientos/".Auth::user()->name."/municipio")}}'><i class="fa fa-calendar"></i> <span>Diagnóstico registrados</span></a></li>
            @else
              <li><a href="{{url('/requerimientos/consult')}}"><i class="fa fa-calendar"></i><span>Diagnóstico registrados</span></a></li>
            @endif
          @else
          @endcan




        </ul>
      </li>
        @endcan

      @can('editar_info_infra_fisica')
      <li class="treeview">
        <a href="#">
          <i class="fa fa-building-o"></i> <span>Infraestructura educativa</span>
          <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
        </a>
        <ul class="treeview-menu">
          @can('editar_info_infra_fisica')
            <li><a href="{{url('informacion')}}"><i class="fa fa-edit"></i>Actualizar información</a></li>
          @else
          @endcan
          @can('solicitudes_infra_fisica')
            <li><a href="{{url('/requerimientosIFisica')}}"><i class="fa fa-warning"></i><span>Diagnóstico de necesidades</span></a></li>
            @if(Auth::user()->isRole('rectores'))
              <li><a href='{{url("/requerimientosIFisica/".Auth::user()->name."/establecimiento")}}'><i class="fa fa-calendar"></i> <span>Diagnósticos registrados</span></a></li>
            @elseif (Auth::user()->isRole('secretarios'))
              <li><a href='{{url("/requerimientosIFisica/".Auth::user()->name."/municipio")}}'><i class="fa fa-calendar"></i> <span>Diagnósticos registrados</span></a></li>
            @else
              <li><a href="{{url('/requerimientosIFisica/consult')}}"><i class="fa fa-calendar"></i><span>Diagnósticos registrados</span></a></li>
            @endif

          @else
          @endcan
        </ul>
      </li>
     @endcan


      @if(Auth::user()->isRole('enlace_tic'))
      @else
      <li class="treeview">
        <a href="#">
          <i class="fa fa-dashboard"></i> <span>Reportes y consultas</span>
          <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
        </a>
        <ul class="treeview-menu">
          <li><a href="{{url('consulta_basica_matricula')}}"><i class="fa fa-area-chart"></i>Consulta matrícula</a></li>
          <li><a href="{{url('consulta_basica_due')}}"><i class="fa fa-area-chart"></i>Consulta DUE</a></li>
          <li><a href="{{url('consulta_basica_saber_antioquia')}}"><i class="fa fa-area-chart"></i>Reportes Pruebas Saber</a></li>
          <li><a href="{{url('reportes')}}"><i class="fa fa-newspaper-o"></i>Reportes dinámicos</a></li>
        </ul>
      </li>
      @endif

      <li class="treeview">
        <a href="#">
          <i class="fa fa-volume-control-phone"></i><span>Directorio municipal</span>
          <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
        </a>
        <ul class="treeview-menu">
        <li><a href="{{url('actualizar_directorio')}}"><i class="fa fa-edit"></i>Actualizar información</a></li>
        <li><a href="{{url('ver_directorio')}}"><i class="fa fa-desktop"></i>Ver información</a></li>
        </ul>
      </li>

      @if(Auth::user()->isRole('administrador_sistema'))
        <li class="treeview">
          <a href="#">
            <i class="fa fa-user" ></i> <span>Admnistración del sistema</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          @can('administracion_usuarios')
          <ul class="treeview-menu">

            <li><a href="{{url('listado_usuarios')}}"><i class="fa fa-universal-access"></i>Usuarios</a></li>

          </ul>
        @else
        @endcan
          <ul class="treeview-menu">
          @can('editar_estructura_matricula')
            <li><a href="{{url('editar_estructura')}}"><i class="fa fa-edit"></i>Editar estructura</a></li>
          @else
          @endcan
          </ul>
        </li>
      @endif


    </ul>
  </section>
  <!-- /.sidebar -->
</aside>
