<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html lang="es">

@section('htmlheader')
  @include('layouts.partials.htmlheader')
@show
<!--
BODY TAG OPTIONS:
=================
Apply one or more of the following classes to get the
desired effect
|---------------------------------------------------------|
| SKINS         | skin-blue                               |
|               | skin-black                              |
|               | skin-purple                             |
|               | skin-yellow                             |
|               | skin-red                                |
|               | skin-green                              |
|---------------------------------------------------------|
|LAYOUT OPTIONS | fixed                                   |
|               | layout-boxed                            |
|               | layout-top-nav                          |
|               | sidebar-collapse                        |
|               | sidebar-mini                            |
|---------------------------------------------------------|
-->
<body class="skin-blue sidebar-mini">

  <div style="display: none;" id="cargador_empresa" class="text-center">
    <br>
    <img src="{{ url('/img/cargando.gif') }}" class="img-responsive center-block" alt="cargador"> &nbsp;<label style="color:#ABB6BA"></label>
    <h4 style="text-align:center;">Cargando, por favor espere un momento.</h4>
    <div class="box-footer col-xs-12">
      <center>
        <button  type="submit" onclick="if (confirm('¿Está seguro de cancelar el proceso de carga masiva?')) javascript:window.location.href=window.location.href;"  class="btn btn-primary">Cancelar</button>
      </center>
    </div>
    <hr style="color:#003" width="100%">
    <br>
  </div>

  <input type="hidden"  id="url_raiz_proyecto" value="{{ url("/") }}" />
  <div id="capa_modal" class="div_modal" style="display: none;"></div>
  <div id="capa_formularios" class="div_contenido" style="display: none;"></div>

  <!-- /Modal para video tutorial conversión UTF-8  DUE-->
  <div aria-hidden="true" aria-labelledby="modal-sample-label" class="modal theme-alt modal-center-vertical" id="modal-utf8-DUE" role="dialog" tabindex="-1">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <button class="close" data-dismiss="modal" type="button" style="font-size: 40px; color:#34495E;" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button> <!-- Botón cerrar para volver a la vista sin necesidad de recargar la página -->
          <h4 class="modal-title text-center" id="modal-sample-label"><strong>Tutorial conversión archivos a UTF-8</strong></h4>
        </div>
        <div class="modal-body">
          <video controls id="video" style="width: 100%; height: auto; margin:0 auto; frameborder:0;" draggable="true">
            <source src="video/UTF-8-DUE.mp4" type="video/mp4; codecs="avc1.42E01E, mp4a.40.2"">
            </video>
          </div>
        </div>
      </div>
    </div>
    <!-- /Fin modal para video tutorial conversión UTF-8 DUE -->

    <!-- /Modal para video tutorial conversión UTF-8 SIMAT -->
    <div aria-hidden="true" aria-labelledby="modal-sample-label" class="modal theme-alt modal-center-vertical" id="modal-utf8-matricula" role="dialog" tabindex="-1">
      <div class="modal-dialog modal-lg">
        <div class="modal-content">
          <div class="modal-header">
            <button class="close" data-dismiss="modal" type="button" style="font-size: 40px; color:#34495E;" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button> <!-- Botón cerrar para volver a la vista sin necesidad de recargar la página -->
            <h4 class="modal-title text-center" id="modal-sample-label"><strong>Tutorial conversión archivos a UTF-8</strong></h4>
          </div>
          <div class="modal-body">
            <video controls id="video2" style="width: 100%; height: auto; margin:0 auto; frameborder:0;" draggable="true">
              <source src="video/UTF-8-Matricula.mp4 " type="video/mp4; codecs="avc1.42E01E, mp4a.40.2"">
              </video>
            </div>
          </div>
        </div>
      </div>
      <!-- /Fin modal para video tutorial conversión UTF-8 SIMAT -->

      <!-- /Modal para video tutorial conversión UTF-8 calidad -->
      <div aria-hidden="true" aria-labelledby="modal-sample-label" class="modal theme-alt modal-center-vertical" id="modal-utf8-Calidad" role="dialog" tabindex="-1">
        <div class="modal-dialog modal-lg">
          <div class="modal-content">
            <div class="modal-header">
              <button class="close" data-dismiss="modal" type="button" style="font-size: 40px; color:#34495E;" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button> <!-- Botón cerrar para volver a la vista sin necesidad de recargar la página -->
              <h4 class="modal-title text-center" id="modal-sample-label"><strong>Tutorial conversión archivos a UTF-8</strong></h4>
            </div>
            <div class="modal-body">
              <video controls id="video2" style="width: 100%; height: auto; margin:0 auto; frameborder:0;" draggable="true">
                <source src="video/UTF-8-Calidad.mp4 " type="video/mp4; codecs="avc1.42E01E, mp4a.40.2"">
                </video>
              </div>
            </div>
          </div>
        </div>
        <!-- /Fin modal para video tutorial conversión UTF-8 calidad -->

        <!-- /Modal para video tutorial conversión UTF-8 dispotivo -->
        <div aria-hidden="true" aria-labelledby="modal-sample-label" class="modal theme-alt modal-center-vertical" id="modal-utf8-Dispositivos" role="dialog" tabindex="-1">
          <div class="modal-dialog modal-lg">
            <div class="modal-content">
              <div class="modal-header">
                <button class="close" data-dismiss="modal" type="button" style="font-size: 40px; color:#34495E;" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button> <!-- Botón cerrar para volver a la vista sin necesidad de recargar la página -->
                <h4 class="modal-title text-center" id="modal-sample-label"><strong>Tutorial conversión archivos a UTF-8</strong></h4>
              </div>
              <div class="modal-body">
                <video controls id="video2" style="width: 100%; height: auto; margin:0 auto; frameborder:0;" draggable="true">
                  <source src="video/UTF-8-Dispositivo.mp4 " type="video/mp4; codecs="avc1.42E01E, mp4a.40.2"">
                  </video>
                </div>
              </div>
            </div>
          </div>
          <!-- /Fin modal para video tutorial conversión UTF-8 dispositivo -->

          <!-- /Modal para video tutorial conversión UTF-8 conectividad -->
          <div aria-hidden="true" aria-labelledby="modal-sample-label" class="modal theme-alt modal-center-vertical" id="modal-utf8-conectividad" role="dialog" tabindex="-1">
            <div class="modal-dialog modal-lg">
              <div class="modal-content">
                <div class="modal-header">
                  <button class="close" data-dismiss="modal" type="button" style="font-size: 40px; color:#34495E;" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button> <!-- Botón cerrar para volver a la vista sin necesidad de recargar la página -->
                  <h4 class="modal-title text-center" id="modal-sample-label"><strong>Tutorial conversión archivos a UTF-8</strong></h4>
                </div>
                <div class="modal-body">
                  <video controls id="video2" style="width: 100%; height: auto; margin:0 auto; frameborder:0;" draggable="true">
                    <source src="video/UTF-8-Conectividad.mp4 " type="video/mp4; codecs="avc1.42E01E, mp4a.40.2"">
                    </video>
                  </div>
                </div>
              </div>
            </div>
            <!-- /Fin modal para video tutorial conversión UTF-8 dispositivo -->

            <!-- Modal de carga masiva para seleccionar resultados o clasificaciones -->
            <div id="miModal" class="modal fade" role="dialog">
              <div class="modal-dialog">
                <!-- Contenido del modal -->
                <div class="modal-content">
                  <div class="modal-header">
                    <p style="text-align:center;">Aquí podrá seleccionar si desea realizar carga masiva de los resultados o clasificaciones de las pruebas Saber</p>
                  </div>
                  <div class="modal-body">
                    <label for="mes">Opciones:</label>
                    <select name="forma" class="form-control" value="" onchange="location = this.options[this.selectedIndex].value;" >
                      <option name="selected" selected="selected"></option>
                      <option value="{{ url('/cargar_datos_saber') }}">Resultados pruebas Saber</option>
                      <option value="{{ url('/clasificaciones_saber') }}">Clasificaciones pruebas Saber</option>
                    </select>
                  </div>
                  <div class="modal-footer">
                  </div>
                </div>
              </div>
            </div>
            <!-- /.Fin Modal de carga masiva para seleccionar el periodo -->




  <div class="wrapper">

    @include('layouts.partials.mainheader')

    @include('layouts.partials.sidebar')

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">

      @include('layouts.partials.contentheader')

      <!-- Main content -->
      <section class="content">
        <!-- Your Page Content Here -->
        @yield('main-content')
      </section><!-- /.content -->
    </div><!-- /.content-wrapper -->



    @include('layouts.partials.footer')

  </div><!-- ./wrapper -->

  @section('scripts')
    @include('layouts.partials.scripts')
  @show

</body>
</html>
