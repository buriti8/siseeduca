@if(Auth::user()->isRole('administrador_sistema') || Auth::user()->isRole('administrador_due'))
@extends('layouts.app')

@section('htmlheader_title')
  Carga masiva de datos DUE
@endsection


@section('main-content')


  <section  id="contenido_principal">
    <div class="box box-primary">
      <div class="box-header">
        <h3 class="text-center"><b>Carga masiva directorio único de establecimientos educativos DUE Nacional</b></h3>
        <br>
        <div>
          <p>Aquí podrá ingresar información de información del Directorio Único de Establecimientos Educativos DUE. Por favor tenga en cuenta las siguientes recomendaciones:</p>
          <ol>
            <li><p>Descargue la plantilla para verificar las columnas que debe incluir en el anexo.</p></li>
            <li><p>El orden y el nombre de las columnas en el anexo debe permanecer igual.</p></li>
            <li><p>Recuerde que antes de cargar el archivo debe convertirlo a UTF-8.</p></li>
            <!-- /El botón llama el id modal-utf8 para cargar video conversión de archivos a utf8 desde app.blade -->
            <button class="btn btn-info fa fa-video-camera" style="font-size:15px;" data-target="#modal-utf8-DUE" data-toggle="modal" type="button"> Conversión archivos UTF-8</button>
            <!-- /Fin botón utf8 -->
            <li><p>Seleccione el mes, año de corte y adjunte los archivos descargados de <a href="https://sineb.mineducacion.gov.co/bcol/app?service=page/BuscandoColegioBasico">Buscando Colegio</a> y presione el botón "Cargar archivos".</p></li>
          </ol>
        </div>
      </div><!-- /.box-header -->

      <div id="notificacion_resul_fci">
        <div class="modal" tabindex="-1" role="dialog">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title">Modal title</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body">
                <p>Modal body text goes here.</p>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-primary">Save changes</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
              </div>
            </div>
          </div>
        </div></div>

        <div id="formulario_archivos_due_nacional">
          <form  id="f_subir_archivos_due_nacional" name="f_subir_archivo_due_nacional" method="post"  files="true" action="{{ url('subir_archivos_due_nacional') }}" class="formarchivoduenacional" enctype="multipart/form-data" >
            {{ csrf_field() }}

            <div class="box-body">

              <div class="form-group col-xs-6" style="width:405px">

                <label for="mes">Mes:</label>

                <select id="mes" name="mes" class="form-control" value="" required>
                  <option selected="selected"></option>

                  @foreach($meses as $mes)
                    <option value="{{ $mes->id}}">{{ $mes->mes }}</option>
                  @endforeach

                </select>

              </div>


              <div class="form-group col-xs-6" style="width:405px ">



                <label for="anio">Año:</label>
                <input name="anio" id="anio" type="number"   class="archivo form-control" min="2018" max="2019" required/><br />

              </div>


            </div>

            <div class="box-body ">

              <div class="panel panel-primary ">
                <div class="panel-heading">Establecimientos educativos</div>
                <div class="panel-body">
                  <div class="form-group col-xs-6"  >
                    <label>Agregar archivo <span class="label label-primary">Establecimientos educativos</span></label>
                    <input name="archivo_establecimientos_nacional" id="archivo_establecimientos_nacional" type="file" files="true"  enctype=”multipart/form-data”  class="archivo form-control"  required /><br /><br />
                  </div>
                </div>

                <div class="panel-heading">Sedes educativas</div>
                <div class="panel-body ">
                  <div class="form-group col-xs-6"  >
                    <label>Agregar archivo <span class="label label-primary">Sedes educativas</span></label>
                    <input name="archivo_sedes_nacional" id="archivo_sedes_nacional" type="file" files="true"  enctype="multipart/form-data"  class="archivo form-control"  required/><br /><br />
                  </div>
                </div>
              </div>



            </div>

            <div class="box-footer">


              <button type="submit" id="btnsubmit"  class="btn btn-primary fa fa-cloud-upload" style="font-size:15px;"> Cargar archivos</button>
              <a onclick="return confirm('Recuerde que cuando vaya a cargar el archivo debe convertirlo a UTF-8.')"  href="{{ url('/descargar_plantilla_establecimientos') }}" target="_blank" id="descargar_plantilla" name="descargar_plantilla"  class="btn btn-info fa fa-cloud-download" style="font-size:15px;"> Descargar plantilla establecimientos</a>
              <a onclick="return confirm('Recuerde que cuando vaya a cargar el archivo debe convertirlo a UTF-8.')" href="{{ url('/descargar_plantilla_sedes') }}" target="_blank" id="descargar_plantilla" name="descargar_plantilla"  class="btn btn-info fa fa-cloud-download" style="font-size:15px;"> Descargar plantilla sedes</a>
            </div>



          </div>

        </form>

      </div>


      <div id="anexos" class="box box-primary" >

        <h3 class="text-center">Archivos cargados</h3>
        <div class="box-body"  >

          <div class="table-responsive" >

            <table  class="table table-hover table-striped" cellspacing="0" width="100%">

              <thead>
                <tr >
                  <th>Año de reporte</th>
                  <th>Mes de reporte</th>
                  <th data-type="date">Fecha</th>
                  <th>Cantidad de registros cargados</th>
                  <th>Acción</th>
                </tr>
              </thead>
              <tbody>

                @foreach($listados as $listado)
                  <tr role="row" class="odd">
                    <td>{{ $listado->anio_corte }}</td>
                    <td>{{ ($listado->mes_corte) ==1 ? "Enero" : "" }}
                      {{ ($listado->mes_corte) ==2 ? "Febrero" : "" }}
                      {{ ($listado->mes_corte) ==3 ? "Marzo " : "" }}
                      {{ ($listado->mes_corte) ==4 ? "Abril" : "" }}
                      {{ ($listado->mes_corte) ==5 ? "Mayo" : "" }}
                      {{ ($listado->mes_corte) ==6 ? "Junio" : "" }}
                      {{ ($listado->mes_corte) ==7 ? "Julio" : "" }}
                      {{ ($listado->mes_corte) ==8 ? "Agosto" : "" }}
                      {{ ($listado->mes_corte) ==9 ? "Septiembre" : "" }}
                      {{ ($listado->mes_corte) ==10 ? "Octubre" : "" }}
                      {{ ($listado->mes_corte) ==11 ? "Noviembre" : "" }}
                      {{ ($listado->mes_corte) ==12 ? "Diciembre" : "" }}
                    </td>
                    <td >{{ Carbon\Carbon::parse($listado->fecha)->format('d-m-Y h:i A') }}</td>

                    <td >{{ $listado->cantidad_establecimientos }}</td>


                    <td >
                      <button type="button"  class="btn  btn-danger btn-xs" title="Borrar"  onclick="borrar_archivos_due_nacional({{  $listado->anio_corte }},{{ $listado->mes_corte }});" ><i class="fa fa-trash-o"></i></button>
                    </td>
                  </tr>
                @endforeach
              </tbody>
            </table>

            {{ $listados->links() }}
          </div>
        </div>
      </div>





    </section>
  @endsection
@else
  @extends('errors.acceso')
@endif
