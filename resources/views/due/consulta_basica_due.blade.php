@extends('layouts.app')

@section('htmlheader_title')
  Consulta Basica DUE
@endsection

@section('main-content')
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
  <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
  <section  id="contenido_principal">
    <div class="box box-success">
      <div class="box-header">
        <center>
        <h3 class="box-title"><strong>Consulta básica DUE</strong></h3>
      </center>
      </div>
      <!-- Filtro Principal -->
      <div class="box box-primary" style="border: 1px solid #3c8dbc !important;" >
        <div class="titleboxes" style="font-size:15px;" align="center">
          Filtro principal
        </div>
        <div class="row" style="padding:10px;">
          <div class="col-md-6">
            <div class="titleboxes" align="center">
              <h5>Año corte, mes corte, subregión y municipios</h5>
            </div>
            <div class="box box-primary" style="border: 1px solid #3c8dbc !important; padding-left:4px;padding-right:4px;">
              <div class="col-md-6">
                <label for="">Año corte</label>
                <div class="dropdown" >
                  <button  class="btn btn-default dropdown-toggle btn-block" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                    Seleccionar
                    <span class="caret"></span>
                  </button>
                  <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                    @foreach($anios as $an)
                      <li><a href="#"><label class="checkbox-formulario">
                        <input type="radio" class="anio_corte" name="anio_corte" value="{{ $an->anio}}" onclick="changemeses(this);" data-dependent="mes_corte"><span class="label-text">{{ $an->anio}}</span>
                      </label></a></li>
                    @endforeach
                  </ul>
                  <script type="text/javascript">
                  $('.dropdown-menu input, .dropdown-menu label').click(function(e) {
                    e.stopPropagation();
                  });
                  </script>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label>Mes corte</label>
                  <select class="form-control" id="mes_corte"  name="mes_corte" class="form-cotrol input-lg" onchange="">
                    <option value="0">Todos</option>
                  </select>
                </div>
              </div>
              <div class="col-md-12">
                <div class="form-group">
                  <label>Subregión</label>
                  <select name="CodigoSubregion" id="CodigoSubregion" class="form-control dynamic" data-dependent="NombreMunicipio"  onchange="_cambiargraficadue()">
                    <option value="0">Todos</option>
                    @foreach($subregions as $sub)
                      <option value="{{ $sub->id}}" onclick="changemeses(this);">{{ $sub->NombreSubregion }}</option>
                    @endforeach
                  </select>
                </div>
              </div>
              <div class="col-md-12">
                <div class="form-group">
                  <label>Municipio</label>
                  <select class="form-control dynamico" id="NombreMunicipio"  name="NombreMunicipio" class="form-cotrol input-lg"  data-dependent="nombre_establecimiento"  onchange="_cambiargraficadue()">
                    <option value="0">Todos</option>
                  </select>
                </div>
              </div>
              <div class="form-group">
                <label><li class="fa fa-filter"></li></label>
              </div>
            </div>
          </div>

          <div class="col-md-6">
            <div class="titleboxes" align="center">
              <h5>Establecimientos</h5>
            </div>
            <div class="box box-primary scrollable-menu" style="border: 1px solid #3c8dbc !important; padding-left:4px;padding-right:4px;">
              <div class="form-group" id="nombre_establecimiento"  name="nombre_establecimiento">
                <div ><a href="#"><label >
                  <input type="radio"  value=""><span class="label-text">Todos</span>
                </label></a></div>
              </div>
            </div>
          </div>

          {{ csrf_field() }}
        </div>
        <div id="infofiltro" class="alert-message alert-message-success" style="display: none;">
          <h4>Recuerda</h4>
          <p>Para poder iniciar los filtros es necesario Seleccionar año y mes de corte, la subregión, municipio y el establecimiento.</p>
        </div>
        <a  href="#" id="clickme" style="background-color:#369;color:#fff;text-decoration:none;padding:4px 10px;display:inline-block">Información</a>
      </div>
    </div>
    <!-- Dividor verde  -->
    <div class="box box-success">
    </div>
    <!-- Caja de informacion principal  -->
    <div class="row"  >
      <div id="mostrardatos1" class="col-md-6 mostrardatos1" >

      </div>
      <div id="mostrardatos2" class="col-md-6 mostrardatos2" align="center" >

      </div>
    </div>
    <div class="row">
      <div class="col-md-12"  align="center">
        <div class="titleboxes">
          <h4>Sedes</h4>
        </div>
        <div class="box box-primary" style="border: 1px solid #3c8dbc !important; padding-left:4px;">
          <div class="box-header">
          </div>
          <table class="table table-striped"  style="width:100%">
            <thead>
              <tr>
                <th>Código</th>
                <th>Nombre de la sede</th>
                <th>Dirección</th>
                <th>Telefono</th>
                <th>Zona</th>
                <th>Municipio</th>
                <th>Estado</th>
              </tr>
            </thead>
            <tbody id="tablesedes">
            </tbody>

          </table>
        </div>
      </div>
    </div>
    <div class="row"  >
      <div id="mostrardatos3" class="col-md-6" >

      </div>
      <div id="mostrardatos4" class="col-md-6" align="center" >

      </div>
    </div>
    <a href="#"></a>
    <script type="text/javascript">
    function activartablalistasedes(estable) {
      var anio_corte = $("input[name='anio_corte']:checked").val();
      var mes_corte = document.getElementById("mes_corte").value;
      var selectestable = estable.value;
      console.log(anio_corte,mes_corte,selectestable);
      $.ajax({
        url: "{{ route('datatable.sedes') }}",
        method: "GET",
        data: {
          anio_corte: anio_corte,
          mes_corte: mes_corte,
          estable: selectestable
        },
        success: function(result) {
          $('#tablesedes').html(result);
          console.log(result);
        }
      });
    }
    </script>

    <!-- Funciones de javascript  -->
    <script type="text/javascript">
    $('#infofiltro').toggle();
    window.setTimeout("mostrar()", 10000);

    function mostrar() {
      $("#infofiltro").slideToggle("slow", function() {
        $('#infofiltro').hide(5000);

      });
    }
    $(document).ready(function() {
      $("#clickme").click(function() {
        $("#infofiltro").toggle("slow", function() {

        });
      });
    });
    </script>
    <script type="text/javascript">
    $('.dynamic').change(function() {
      var select = $(this).attr("id");
      var value = $(this).val();
      var dependent = $(this).data('dependent');
      var _token = $('input[name="_token"]').val();
      var valor = "";
      if ($(this).val() != 0) {
        valor = "uno";
        $.ajax({
          url: "{{ route('ConsultaBasica.fetch') }}",
          method: "POST",
          data: {
            select: select,
            value: value,
            _token: _token,
            dependent: dependent,
            valor: valor
          },
          success: function(result) {
            $('#' + dependent).html(result);
            $('#nombre_establecimiento').html("<div class=''><a href='#'><label ><input type='radio' value='' ><span class='label-text'>Todos</span></label></a></div>");
            $('.mostrardatos1').find('input:text').val('');
            $('.mostrardatos2').find('input:text').val('');
          }
        })
      } else {
        valor = "todos";
        $.ajax({
          url: "{{ route('ConsultaBasica.fetch') }}",
          method: "POST",
          data: {
            select: select,
            value: value,
            _token: _token,
            dependent: dependent,
            valor: valor
          },
          success: function(result) {
            $('#' + dependent).html(result);
            $('#nombre_establecimiento').html("<div class=''><a href='#'><label ><input type='radio' value='' ><span class='label-text'>Todos</span></label></a></div>");
            $('#nombre_sede').html("<div class=''><a href='#'><label ><input type='radio' name='radio' value='' onclick='' ><span class='label-text'>Todos</span></label></a></div>");
          }

        })
      }
    });

    $('#subregion').change(function() {
      $('#municipio').val('');
    });
    </script>
    <script type="text/javascript">
    $('.dynamico').change(function() {
      /*var select = $(this).attr("id");*/
      var select = "codigo_dane_municipio";
      var value = $(this).val();
      var location = "due";
      var dependent = $(this).data('dependent');
      var _token = $('input[name="_token"]').val();
      var anio_corte = $("input[name='anio_corte']:checked").val();
      var mes_corte = document.getElementById("mes_corte").value;
      var valor = "";
      if ($(this).val() != 0) {
        valor = "uno";
        $.ajax({
          url: "{{ route('ConsultaBasica.esta') }}",
          method: "POST",
          data: {
            select: select,
            value: value,
            _token: _token,
            dependent: dependent,
            valor: valor,
            location : location,
            anio_corte : anio_corte,
            mes_corte : mes_corte
          },
          success: function(result) {
            $('#' + dependent).html(result);
            $('#nombre_sede').html("<div class=''><a href='#'><label ><input type='radio' name='radio' value='' onclick='' ><span class='label-text'>Todos</span></label></a></div>");
          }
        })
      } else {
        valor = "todos";
        $.ajax({
          url: "{{ route('ConsultaBasica.esta') }}",
          method: "POST",
          data: {
            select: select,
            value: value,
            _token: _token,
            dependent: dependent,
            valor: valor,
            location : location,
            anio_corte : anio_corte,
            mes_corte : mes_corte
          },
          success: function(result) {
            $('#' + dependent).html(result);
            $('#nombre_sede').html("<div class=''><a href='#'><label ><input type='radio' name='radio' value='' onclick='' ><span class='label-text'>Todos</span></label></a></div>");
          }

        })
      }
    });

    $('#subregion').change(function() {
      $('#municipio').val('');
    });
    </script>
    <script type="text/javascript">
    function changesedes (element) {
      /*var select = $(this).attr("id");*/
      var select = "codigo_establecimiento";
      var value = element.value;
      var location = "due";
      var dependent = $(element).data('dependent');
      var _token = $('input[name="_token"]').val();
      var anio_corte = $("input[name='anio_corte']:checked").val();
      var mes_corte = document.getElementById("mes_corte").value;
      var valor = "";
      if ($(element).val() != 0) {
        valor = "uno";
        $.ajax({
          url: "{{ route('ConsultaBasica.sedes') }}",
          method: "POST",
          data: {
            select: select,
            value: value,
            _token: _token,
            dependent: dependent,
            valor: valor,
            location : location,
            anio_corte : anio_corte,
            mes_corte : mes_corte
          },
          success: function(result) {
            $('#' + dependent).html(result);
          }
        })
      } else {
        valor = "todos";
        $.ajax({
          url: "{{ route('ConsultaBasica.sedes') }}",
          method: "POST",
          data: {
            select: select,
            value: value,
            _token: _token,
            dependent: dependent,
            valor: valor,
            location : location,
            anio_corte : anio_corte,
            mes_corte : mes_corte
          },
          success: function(result) {
            $('#' + dependent).html(result);
          }

        })
      }
    };
    </script>
    <script type="text/javascript">
    function changemeses (element) {
      /*var select = $(this).attr("id");*/
      var select = "anio_corte";
      var value = element.value;
      var dependent = $(element).data('dependent');
      var _token = $('input[name="_token"]').val();
      $.ajax({
        url: "{{ route('ConsultaBasica.meses') }}",
        method: "POST",
        data: {
          select: select,
          value: value,
          _token: _token,
          dependent: dependent
        },
        success: function(result) {
          $('#' + dependent).html(result);
        }
      })
    };
    </script>

  </section>
@endsection
