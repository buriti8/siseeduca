@extends('layouts.app')

@section('htmlheader_title')
	Página principal
@endsection


@section('main-content')
	<div class="container-fluid spark-screen">
		<div class="row">
			<div class="col-md-8 col-md-offset-2">
				<div class="panel panel-default" style="text-align:center">
					<div class="panel-heading">
                    <h2 class="box-title">Acerca de</h2>
					</div>
					<div class="panel-body">
                        <label style="font-weight: bold;">Desarrollado por</label>
					    <h3>Practicantes de excelencia 2018 - I</h3>
						<p>Andrés Cantillo</p>
						<p>Jorge Céspedes</p>
						<p>Javer Uribe Díaz</p>
						<p>Juan David Ortiz</p>
						<p>Jerson David Polo</p>

						<h3>Practicantes de excelencia 2018 - II</h3>
						<p>Mauricio Vélez Castrillón</p>
						<p>Edwin Bedoya</p>
						<p>Jader Gaviria</p>

						<label style="font-weight: bold;">Secretaría de Educación - Gobernacion de Antioquia</label>
						<p style="font-weight: bold;">Derechos reservados</p>

					</div>
				</div>
			</div>
		</div>
	</div>
@endsection
