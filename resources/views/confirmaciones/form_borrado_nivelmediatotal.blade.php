    <!-- modal encargado de mostrar la confirmación de la eliminación del registro-->
  <div class="btn-group" style="margin-left:50px; " >
    <div class="modal fade in" id='id_nivelmediatotal'  role="dialog" style="display: block; padding-right: 17px;">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
          </div>
          <div class="modal-body">
            <center>
              <h3>¿Deseas borrar completamente {{ $nivelmediatotal->Descripcion }}?</h3>
            </center>
          </div>
          <div class="modal-footer">
            <div class="text-center">
              <form method="post" action="{{ url('borrar_nivelmediatotal') }}" id="f_borrar_nivelmediatotal" class="formentrada" >
                <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                <input type="hidden" name="id_nivelmediatotal" value="{{ $nivelmediatotal->id }}">
                <button type="submit" class="btn btn-danger" style="margin-left:20px;" >Borrar</button>
                <button type="button" class="btn btn-primary" onclick="javascript:$('.div_modal').click();" >Cancelar</button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
