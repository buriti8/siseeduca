<!-- modal encargado de mostrar la confirmación de la eliminación del registro-->
<div class="btn-group" style="margin-left:50px; " >
  <div class="modal fade in" id='id_tipodiscapacidad_'  role="dialog" style="display: block; padding-right: 17px;">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
        </div>
        <div class="modal-body">
          <center>
            <h3>¿Deseas borrar completamente {{ $tipodiscapacidad->Descripcion }}?</h3>
          </center>
        </div>
        <div class="modal-footer">
          <div class="text-center">
            <form method="post" action="{{ url('borrar_tipodiscapacidad') }}" id="f_borrar_tipodiscapacidad" class="formentrada" >
              <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
              <input type="hidden" name="id_tipodiscapacidad_" value="{{ $tipodiscapacidad->id }}">
              <button type="submit" class="btn btn-danger" style="margin-left:20px;" >Borrar</button>
              <button type="button" class="btn btn-primary" onclick="javascript:$('.div_modal').click();" >Cancelar</button>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
