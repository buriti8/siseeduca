<div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border my-box-header">
              <h3 class="box-title">Borrar Rector Director</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <div class=" box-body">

            <h3>¿Deseas borrar completamente El Rector Director  {{ $rectordirector->Nombre}}?</h3>

            </div>

              <div class="box-footer">

              <form method="post" action="{{ url('f_borrar_rectordirector') }}" id="f_borrar_rectordirector" class="formentrada" >

               <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                <input type="hidden" name="id_rectordirector" value="{{ $rectordirector->id }}">

                <button type="button" class="btn btn-default" onclick="javascript:$('.div_modal').click();" >Cancelar</button>
                <button type="submit" class="btn btn-danger" style="margin-left:20px;" >Borrar Rector Director</button> </form>
              </div>


          </div>
          <!-- /.box -->

        </div>
