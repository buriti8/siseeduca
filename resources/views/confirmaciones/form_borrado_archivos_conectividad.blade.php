
        <!-- modal encargado de mostrar la confirmación de la eliminación del registro-->
        <div class="btn-group" style="margin-left:50px; " >
        <div class="modal fade in"   role="dialog" style="display: block; padding-right: 17px;">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
              </div>
              <div class="modal-body">
                <center>
                  <h4>¿Se eliminarán los archivos de conectividad correpondiente a la fecha de corte<b> {{ ($mes_corte) ==1?"Enero" : "" }}
                  {{ ($mes_corte) ==2 ? "Febrero" : "" }}
                  {{ ($mes_corte) ==3 ? "Marzo " : "" }}
                  {{ ($mes_corte) ==4 ? "Abril" : "" }}
                  {{ ($mes_corte) ==5 ? "Mayo" : "" }}
                  {{ ($mes_corte) ==6 ? "Junio" : "" }}
                  {{ ($mes_corte) ==7 ? "Julio" : "" }}
                  {{ ($mes_corte) ==8 ? "Agosto" : "" }}
                  {{ ($mes_corte) ==9 ? "Septiembre" : "" }}
                  {{ ($mes_corte) ==10 ? "Octubre" : "" }}
                  {{ ($mes_corte) ==11 ? "Noviembre" : "" }}
                  {{ ($mes_corte) ==12 ? "Diciembre" : "" }} </b>de <b> {{ $ano_info }}</b>?</h4>
                </center>
              </div>
              <div class="modal-footer">
                <div class="text-center">
                  <form method="post" action="{{ url('borrar_archivos_conectividad') }}" id="f_borrar_archivos_conectividad" class="formentrada" >
                    <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                    <input type="hidden" name="ano_info" value="{{ $ano_info }}">
                    <input type="hidden" name="mes_corte" value="{{ $mes_corte}}">
                    <button type="submit" class="btn btn-danger" style="margin-left:20px;" >Borrar</button>
                    <button type="button" class="btn btn-primary" onclick="javascript:$('.div_modal').click();" >Cancelar</button>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
