<div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border my-box-header">
              <h3 class="box-title">Borrar Archivos Mesa</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <div class=" box-body">


        <h3>¿ Se eliminarán los archivos de mesa de ayuda  correpondiente a la fecha de corte:  <b> {{ ($mes_corte) ==1 ? "Enero" : "" }}
        {{ ($mes_corte) ==2 ? "Febrero" : "" }}
        {{ ($mes_corte) ==3 ? "Marzo " : "" }}
        {{ ($mes_corte) ==4 ? "Abril" : "" }}
        {{ ($mes_corte) ==5 ? "Mayo" : "" }}
        {{ ($mes_corte) ==6 ? "Junio" : "" }}
        {{ ($mes_corte) ==7 ? "Julio" : "" }}
        {{ ($mes_corte) ==8 ? "Agosto" : "" }}
        {{ ($mes_corte) ==9 ? "Septiembre" : "" }}
        {{ ($mes_corte) ==10 ? "Octubre" : "" }}
        {{ ($mes_corte) ==11 ? "Noviembre" : "" }}
        {{ ($mes_corte) ==12 ? "Diciembre" : "" }} </b>de  <b>  {{ $ano_info }} </b>?</h3>

            </div>

              <div class="box-footer">

              <form method="post" action="{{ url('borrar_archivos_mesa') }}" id="f_borrar_archivos_mesa" class="formentrada" >

               <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
               <input type="hidden" name="ano_info" value="{{ $ano_info }}">
               <input type="hidden" name="mes_corte" value="{{ $mes_corte}}">


                <button type="button" class="btn btn-default" onclick="javascript:$('.div_modal').click();" >Cancelar</button>
                <button type="submit" class="btn btn-danger" style="margin-left:20px;" >Borrar Archivos</button> </form>
              </div>


          </div>
          <!-- /.box -->





        </div>
