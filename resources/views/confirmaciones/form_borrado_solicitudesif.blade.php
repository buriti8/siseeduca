
<!-- modal encargado de mostrar infromación al usuario, en este caso un mensaje de confirmación-->
<div class=" " style="margin-left:50px;" >
  <div class="modal fade in" id='verInformacion'  role="dialog" style="display: block; padding-right: 10px;">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
        </div>
        <div class="modal-body">
          <center>
            <h4>
              <strong>
                        ¿Deseas borrar el diagnóstico de necesidades?
              </strong>
            </h4>
          </center>
        </div>
        <div class="modal-footer">
          <center>
          <form method="post" action="{{ url('eliminaSolicitudIFDelete') }}" id="elimnasolicitudif" class="formentrada" >
            <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
            <input type="hidden" name="id" value="{{ $id }}">
              <button type="submit" class="btn btn-danger" style="margin-left:10px;" >Borrar</button>
              <button onclick="cerrar_modal()" class="btn btn-primary" data-dismiss="modal" type="button">Cancelar</button>
          </form>
        </center>
        </div>
      </div>
    </div>
  </div>
</div>
