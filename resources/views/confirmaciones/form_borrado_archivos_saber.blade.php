<!-- modal encargado de mostrar la confirmación de la eliminación del registro-->
<div class="btn-group" style="margin-left:50px; " >
  <div class="modal fade in" id='id_subregion'  role="dialog" style="display: block; padding-right: 17px;">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
        </div>
        <div class="modal-body">
          <center>
            <div class=" box-body">
              <h4>¿Desea borrar los resultados del periodo: <b>{{ $periodo }}</b>?
              </h4>
            </div>
          </center>
        </div>
        <div class="modal-footer">
          <div class="text-center">
            <form method="post" action="{{ url('borrar_archivos_saber') }}" id="f_borrar_archivos_saber" class="formentrada" >
              <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
              <input type="hidden" name="periodos" value="{{ $periodo }}">
              <button type="submit" class="btn btn-danger" style="margin-left:20px;" >Borrar</button>
              <button type="button" class="btn btn-primary" onclick="javascript:$('.div_modal').click();" >Cancelar</button>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
