@if(Auth::user()->isRole('administrador_sistema') || Auth::user()->isRole('Administrador_infraestructura_educativa') || Auth::user()->isRole('Rectores') || Auth::user()->isRole('Secretarios'))
  @extends('layouts.app')

  @section('htmlheader_title')
    Editar Información
  @endsection

  @section('main-content')

    <div style="background color: white;">
      <div style="text-align: center; margin-bottom: 20px;" class="container">
        <h3><strong>Infraestructura educativa</strong></h2>
          <h4>El presente submódulo está diseñado para actualizar la información de las sedes educativas del departamento de Antioquia, lo invitamos a mantener la información actualizada. ¡Muchas gracias!
          </h4>
        </div>
      </div>

      <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
      <section  id="contenido_principal">

        <div class="box box-solid box-primary collapsed-box" >
          <div class="box-header with-border" >
            <h3 class="box-title" >Legalidad de las sedes educativas</h3>
            <div class="box-tools pull-right" >
              <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip">
                <i class="fa fa-plus"></i></button>
              </div>
            </div>
            <span class="help-block"><span class="glyphicon glyphicon-tags"></span>       *En este campo encontrarás la información legal de cada una de las sedes educativas.</span>

            <div class="box-body"  >

              <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
              <section  id="contenido_principal">
                <div class="box-header">
                  <h3 class="box-title">Actualizar información  </h3>
                </div><!-- /.box-header -->

                <div class="box box-solid box-default collapsed-box"  >
                  <div class="box-header with-border" >
                    <h3 class="box-title" >Información sedes educativas</h3>
                    <div class="box-tools pull-right" >
                      <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip">
                        <i class="fa fa-plus"></i></button>
                      </div>
                    </div>
                    <div  class="box-body"  >
                      <div class="margin" id="botones_control">

                        <a href="javascript:void(0);" style="margin-bottom:10px;"  class="btn btn-primary" onclick="cargar_formulario(31);">Ingresar información legal</a>

                      </div>

                      <div id"tabla-responsive" class="table-responsive" >

                        <table id="tablasedeslegal"  class="mdl-data-table" style="width:100%">
                          <thead>
                            <tr>
                              <Th>Dane Sede</Th>
                              <Th>Nombre establecimiento</Th>
                              <Th>Nombre sede</Th>
                              <Th>Dirección</Th>
                              <Th>Tipo de propietario</Th>
                              <Th>Modalidad</Th>
                              <Th>Área del lote (M²)</Th>
                              <Th>Área construida (M²)</Th>
                              <Th>Posee plano</Th>
                              <Th>Posee foto</Th>
                              <Th>Longitud</Th>
                              <Th>Latitud</Th>
                              <Th>Distancia</Th>
                              <Th>Tipo de vía</Th>
                              <Th>Acción</Th>
                            </tr>
                          </thead>
                        </table>

                      </div>
                    </div>
                  </div>

                  <script>

                  $(document).ready(function() {
                    activar_tabla_empresas();
                    function activar_tabla_empresas() {
                      $('#tablasedeslegal').DataTable({
                        processing: true,
                        serverSide: true,
                        pageLength: 10,
                        language: {
                          "url": '{!! asset('/plugins/datatables/latino.json') !!}'
                        } ,
                        ajax: '{{ route('datatable.sedeslegal') }}',
                        columns: [
                          { data: 'DaneSede', name: 'DaneSede' },
                          { data: 'nombre_establecimiento', name: 'nombre_establecimiento' },
                          { data: 'nombre_sede', name: 'nombre_sede' },
                          { data: 'direccion', name: 'direccion' },
                          { data: 'TipoPropietario', name: 'TipoPropietario' },
                          { data: 'Modalidad', name: 'Modalidad' },
                          { data: 'AreaLote', name: 'AreaLote' },
                          { data: 'AreaConstruida', name: 'AreaConstruida' },
                          { data: 'Plano', name: 'Plano' },
                          { data: 'Foto', name: 'Foto' },
                          { data: 'Coordenadas', name: 'Coordenadas' },
                          { data: 'Latitud', name: 'Latitud' },
                          { data: 'Distancia', name: 'Distancia' },
                          { data: 'TipoVia', name: 'TipoVia' },

                          { data: null,  render: function ( data, type, row )
                            {
                              return "<button type='button' title='Editar' onclick='verinfo_sedeslegal("+ data.id +")' class='btn btn-warning btn-xs' ><i class='fa fa-edit'></i></button>"+"&nbsp;"+
                              "@role('administrador_sistema')<button type='button'  onclick='borrado_sedeslegal(" + data.id +")' class='btn  btn-danger btn-xs' ><i class='fa fa-fw fa fa-trash-o' title='Eliminar'></i></button>@endrole"
                            }
                          }

                        ]

                      });

                    }
                  });

                  </script>

                </div>

                <div class="box-body"  >

                  <div class="box box-solid box-default collapsed-box"  >
                    <div class="box-header with-border" >
                      <h3 class="box-title" >Información escrituras de las sedes educativas</h3>
                      <div class="box-tools pull-right" >
                        <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip">
                          <i class="fa fa-plus"></i></button>
                        </div>
                      </div>
                      <div class="box-body"  >


                        <div class="margin" id="botones_control">
                          <a href="javascript:void(0);" style="margin-bottom:10px;" class="btn btn-primary" onclick="cargar_formulario(40);">Ingresar información legal</a>
                        </div>


                        <div id"tabla-responsive" class="table-responsive" >

                          <table id="tablaescritura"  class="mdl-data-table" style="width:100%">
                            <thead>
                              <tr>
                                <Th>Dane Sede</Th>
                                <Th>Nombre de Establecimiento</Th>
                                <Th>Nombre sede</Th>
                                <Th>Dirección</Th>
                                <Th>Propietario</Th>
                                <Th>Matricula inmobiliaria</Th>
                                <Th>Número escritura</Th>
                                <Th>Fecha escritura</Th>
                                <Th>Acción</Th>

                              </tr>
                            </thead>
                          </table>

                        </div>
                      </div>

                    </div>


                    <script>

                    $(document).ready(function() {
                      activar_tabla_empresas();
                      function activar_tabla_empresas() {
                        $('#tablaescritura').DataTable({
                          processing: true,
                          serverSide: true,
                          pageLength: 10,
                          language: {
                            "url": '{!! asset('/plugins/datatables/latino.json') !!}'
                          } ,
                          ajax: '{{ route('datatable.sedesescritura') }}',
                          columns: [

                            { data: 'DaneSede', name: 'DaneSede' },
                            { data: 'nombre_establecimiento', name: 'nombre_establecimiento' },
                            { data: 'nombre_sede', name: 'nombre_sede' },
                            { data: 'direccion', name: 'direccion' },
                            { data: 'Propietario', name: 'Propietario' },
                            { data: 'MatriculaInmob', name: 'MatriculaInmob' },
                            { data: 'NumEscritura', name: 'NumEscritura' },
                            { data: 'FechaEscritura', name: 'FechaEscritura' },
                            //{ data: 'updated_at', name: 'updated_at' },
                            { data: null,  render: function ( data, type, row )
                              {
                                return "<button title='Editar' type='button' onclick='verinfo_sedesescritura("+ data.id +")' class='btn btn-warning btn-xs' ><i class='fa fa-fw fa-edit'></i></button>"+"&nbsp;"+
                                "@role('administrador_sistema')<button type='button'  onclick='borrado_sedesescritura(" + data.id +")' class='btn  btn-danger btn-xs' ><i class='fa fa-fw fa fa-trash-o' title='Eliminar'></i></button>@endrole"
                              }
                            }

                          ]

                        });

                      }
                    });

                    </script>

                  </div>



                </div>

                <div class="box box-solid box-primary collapsed-box"  >
                  <div class="box-header with-border" >
                    <h3 class="box-title" >Espacios físicos de las sedes educativas </h3>
                    <div class="box-tools pull-right" >
                      <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip">
                        <i class="fa fa-plus"></i></button>
                      </div>
                    </div>
                    <span class="help-block"><span class="glyphicon glyphicon-tags"></span>       *En este campo encontrarás la información de todos los espacios físicos como aulas, baños, bibliotecas, entre otros.</span>

                    <div class="box-body"  >

                      <div class="box box-solid box-default collapsed-box">
                        <div class="box-header with-border" >
                          <h3 class="box-title" >Aulas</h3>
                          <div class="box-tools pull-right" >
                            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip">
                              <i class="fa fa-plus"></i></button>
                            </div>
                          </div>
                          <div class="box-body"  >

                            <div class="margin" id="botones_control">
                              <a href="javascript:void(0);" style="margin-bottom:10px;" class="btn btn-primary" onclick="cargar_formulario(35);">Ingresar espacios físicos</a>
                            </div>

                            <div id="tabla-responsive" class="table-responsive" >

                              <table id="tblsedesespacio"  class="mdl-data-table" style="width:100%">
                                <thead>
                                  <tr>
                                    <Th>Dane sede</Th>
                                    <Th>Nombre establecimiento</Th>
                                    <Th>Nombre Sede</Th>
                                    <Th>Dirección</Th>
                                    <Th>Preescolar</Th>
                                    <Th>Primaria</Th>
                                    <Th>Secundaria</Th>
                                    <Th>Especiales</Th>
                                    <Th>Biblioteca</Th>
                                    <Th>Sistemas</Th>
                                    <Th>Bilingüismo</Th>
                                    <Th>Laboratorio</Th>
                                    <Th>Talleres</Th>
                                    <Th>Múltiples</Th>
                                    <Th>Acción</Th>
                                  </tr>
                                </thead>
                              </table>

                            </div>

                          </div>
                          <!-- /.box-body -->

                        </div>
                        <script>


                        $(document).ready(function() {
                          activar_tabla_empresas();
                          function activar_tabla_empresas() {
                            $('#tblsedesespacio').DataTable({
                              processing: true,
                              serverSide: true,
                              pageLength: 10,
                              language: {
                                "url": '{!! asset('/plugins/datatables/latino.json') !!}'
                              } ,
                              ajax: '{{ route('datatable.sedesespacio') }}',
                              columns: [
                                { data: 'DaneSede', name: 'DaneSede' },
                                { data: 'nombre_establecimiento', name: 'nombre_establecimiento' },
                                { data: 'nombre_sede', name: 'nombre_sede' },
                                { data: 'direccion', name: 'direccion' },
                                { data: 'AulasPreescolar', name: 'AulasPreescolar' },
                                { data: 'AulasPrimaria', name: 'AulasPrimaria' },
                                { data: 'AulasSecundaria', name: 'AulasSecundaria' },
                                { data: 'AulasEspaciales', name: 'AulasEspaciales' },
                                { data: 'Biblioteca', name: 'Biblioteca' },
                                { data: 'AulasSistemas', name: 'AulasSistemas' },
                                { data: 'AulasBilinguismo', name: 'AulasBilinguismo' },
                                { data: 'Laboratorio', name: 'Laboratorio' },
                                { data: 'AulasTalleres', name: 'AulasTalleres' },
                                { data: 'AulasMultiples', name: 'AulasMultiples' },

                                { data: null,  render: function ( data, type, row )
                                  {
                                    return "<button title='Editar' type='button' onclick='verinfo_sedesaulas("+ data.id +")' class='btn  btn-warning btn-xs' ><i class='fa fa-fw fa-edit'></i></button>"+"&nbsp;"+
                                    "@role('administrador_sistema')<button type='button'  onclick='borrado_sedesespacios(" + data.id +")' class='btn  btn-danger btn-xs' ><i class='fa fa-fw fa fa-trash-o' title='Eliminar'></i></button>@endrole"
                                  }
                                }

                              ]

                            });

                          }
                        });

                        </script>

                      </div>

                      <div class="box-body"  >

                        <div class="box box-solid box-default collapsed-box"  >
                          <div class="box-header with-border" >
                            <h3 class="box-title" >Baterías sanitarias</h3>
                            <div class="box-tools pull-right" >
                              <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip">
                                <i class="fa fa-plus"></i></button>
                              </div>
                            </div>
                            <div class="box-body"  >

                              <div class="margin" id="botones_control">
                              </div>



                              <div id"tabla-responsive" class="table-responsive" >

                                <table id="tablasedesespacio"  class="mdl-data-table" style="width:100%">
                                  <thead>
                                    <tr>
                                      <Th>Dane sede</Th>
                                      <Th>Nombre establecimiento</Th>
                                      <Th>Nombre sede</Th>
                                      <Th>Dirección</Th>
                                      <Th>Sanitarios hombres</Th>
                                      <Th>Sanitarios mujeres</Th>
                                      <Th>Lavamos hombres</Th>
                                      <Th>Lavamanos mujeres</Th>
                                      <Th>Orinales</Th>
                                      <Th>Baños movilidad reducida</Th>
                                      <Th>Acción</Th>
                                    </tr>
                                  </thead>
                                </table>

                              </div>
                            </div>
                            <!-- /.box-body -->
                          </div>

                          <!-- /.Fin caja RectorDirectors-->
                          <script>

                          $(document).ready(function() {
                            activar_tabla_empresas();
                            function activar_tabla_empresas() {
                              $('#tablasedesespacio').DataTable({
                                processing: true,
                                serverSide: true,
                                pageLength: 10,
                                language: {
                                  "url": '{!! asset('/plugins/datatables/latino.json') !!}'
                                } ,
                                ajax: '{{ route('datatable.sedesespacio') }}',
                                columns: [
                                  { data: 'DaneSede', name: 'DaneSede' },
                                  { data: 'nombre_establecimiento', name: 'nombre_establecimiento' },
                                  { data: 'nombre_sede', name: 'nombre_sede' },
                                  { data: 'direccion', name: 'direccion' },
                                  { data: 'SantiriosHombres', name: 'SantiriosHombres' },
                                  { data: 'SanitariosMujeres', name: 'SanitariosMujeres' },
                                  { data: 'LavamanosHombres', name: 'LavamanosHombres' },
                                  { data: 'LavamanosMujeres', name: 'LavamanosMujeres' },
                                  { data: 'Orinales', name: 'Orinales' },
                                  { data: 'SanitariosReducida', name: 'SanitariosReducida' },

                                  { data: null,  render: function ( data, type, row )
                                    {
                                      return "<button title='Editar' type='button' onclick='verinfo_sedesbaterias("+ data.id +")' class='btn  btn-warning btn-xs' ><i class='fa fa-fw fa-edit'></i></button>"+"&nbsp;&nbsp;&nbsp;&nbsp;"
                                    }
                                  }

                                ]

                              });

                            }
                          });

                          </script>

                        </div>

                        <div class="box-body"  >

                          <div class="box box-solid box-default collapsed-box"  >
                            <div class="box-header with-border" >
                              <h3 class="box-title" >Otros espacios</h3>
                              <div class="box-tools pull-right" >
                                <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip">
                                  <i class="fa fa-plus"></i></button>
                                </div>
                              </div>
                              <div class="box-body"  >

                                <div class="margin" id="botones_control">
                                </div>

                                <div id"tabla-responsive" class="table-responsive" >

                                  <table id="tablasedeespacio"  class="mdl-data-table" style="width:100%">
                                    <thead>
                                      <tr>
                                        <th>Dane sede</th>
                                        <th>Nombre establecimiento</th>
                                        <th>Nombre sede</th>
                                        <th>Dirección</th>
                                        <th>Cocinas</th>
                                        <th>Comedores</th>
                                        <th>Vivienda</th>
                                        <th>Canchas</th>
                                        <th>Placas múltiples</th>
                                        <th>Juegos infantiles</th>
                                        <th>Acción</th>
                                      </tr>
                                    </thead>
                                  </table>

                                </div>
                              </div>
                              <!-- /.box-body -->
                            </div>

                            <!-- /.Fin caja RectorDirectors-->
                            <script>

                            $(document).ready(function() {
                              activar_tabla_empresas();
                              function activar_tabla_empresas() {
                                $('#tablasedeespacio').DataTable({
                                  processing: true,
                                  serverSide: true,
                                  pageLength: 10,
                                  language: {
                                    "url": '{!! asset('/plugins/datatables/latino.json') !!}'
                                  } ,
                                  ajax: '{{ route('datatable.sedesespacio') }}',
                                  columns: [
                                    { data: 'DaneSede', name: 'DaneSede' },
                                    { data: 'nombre_establecimiento', name: 'nombre_establecimiento' },
                                    { data: 'nombre_sede', name: 'nombre_sede' },
                                    { data: 'direccion', name: 'direccion' },
                                    { data: 'Cocinas', name: 'Cocinas' },
                                    { data: 'Comedores', name: 'Comedores' },
                                    { data: 'Vivienda', name: 'Vivienda' },
                                    { data: 'Canchas', name: 'Canchas' },
                                    { data: 'PlacasMulti', name: 'PlacasMulti' },
                                    { data: 'JuegosInfantiles', name: 'JuegosInfantiles' },

                                    { data: null,  render: function ( data, type, row )
                                      {
                                        return "<button title='Editar' type='button' onclick='verinfo_sedesotros("+ data.id +")' class='btn  btn-warning btn-xs' ><i class='fa fa-fw fa-edit'></i></button>"+"&nbsp;&nbsp;&nbsp;&nbsp;"
                                      }
                                    }

                                  ]

                                });

                              }
                            });

                            </script>

                          </div>



                        </div>




                        <head>
                          <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />

                          <div class="box box-solid box-primary collapsed-box"  >
                            <div class="box-header with-border" >
                              <h3 class="box-title" >Servicios generales de las sedes educativas</h3>
                              <div class="box-tools pull-right" >
                                <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip">
                                  <i class="fa fa-plus"></i></button>
                                </div>
                              </div>
                              <span class="help-block"><span class="glyphicon glyphicon-tags"></span>       *En este campo encontrarás la información de los servicios que posee cada sede, como energía eléctrica, acueducto, entre otros.</span>

                              <div class="box-body"  >


                                <div class="margin" id="botones_control">
                                  <a href="javascript:void(0);" style="margin-bottom: 10px;" class="btn btn-primary" onclick="cargar_formulario(32);">Ingresar servicios generales</a>
                                </div>




                                <div id"tabla-responsive" class="table-responsive" >

                                  <table id="tablasedesservicios"  class="mdl-data-table" style="width:100%">


                                    <thead>
                                      <tr>
                                        <th>Dane sede</th>
                                        <th>Nombre establecimiento</th>
                                        <th>Nombre sede</th>
                                        <th>Dirección</th>
                                        <th>Tipo de energía</th>
                                        <th>Acueducto</th>
                                        <th>Agua Potable</th>
                                        <th>Planta de tratamiento</th>
                                        <th>Alcantarillado</th>
                                        <th>Pozo séptico</th>
                                        <th>Acción</th>
                                      </tr>
                                    </thead>
                                  </table>

                                </div>
                              </div>
                            </div>



                            <script>

                            $(document).ready(function() {
                              activar_tabla_empresas();
                              function activar_tabla_empresas() {
                                $('#tablasedesservicios').DataTable({
                                  processing: true,
                                  serverSide: true,
                                  pageLength: 10,
                                  language: {
                                    "url": '{!! asset('/plugins/datatables/latino.json') !!}'
                                  } ,
                                  ajax: '{{ route('datatable.sedesservicio') }}',
                                  columns: [
                                    { data: 'DaneSede', name: 'DaneSede' },
                                    { data: 'nombre_establecimiento', name: 'nombre_establecimiento' },
                                    { data: 'nombre_sede', name: 'nombre_sede' },
                                    { data: 'direccion', name: 'direccion' },
                                    { data: 'TipoEnergia', name: 'TipoEnergia' },
                                    { data: 'Acueducto', name: 'Acueducto' },
                                    { data: 'AguaPotable', name: 'AguaPotable' },
                                    { data: 'PlantaTratamiento', name: 'PlantaTratamiento' },
                                    { data: 'Alcantarillado', name: 'Alcantarillado' },
                                    { data: 'PozoSeptico', name: 'PozoSeptico' },

                                    { data: null,  render: function ( data, type, row )
                                      {

                                        return "<button title='Editar' type='button' onclick='verinfo_sedesservicio("+ data.id +")' class='btn  btn-warning btn-xs' ><i class='fa fa-fw fa-edit'></i></button>"+"&nbsp;"+
                                        "@role('administrador_sistema')<button type='button'  onclick='borrado_sedesservicio(" + data.id +")' class='btn  btn-danger btn-xs' ><i class='fa fa-fw fa fa-trash-o' title='Eliminar'></i></button>@endrole"
                                      }
                                    }

                                  ]


                                });

                              }
                            });


                            </script>

                            <head/>

                            <div class="box box-solid box-primary collapsed-box" >
                              <div class="box-header with-border" >
                                <h3 class="box-title" >Riesgos de las sedes educativas</h3>
                                <div class="box-tools pull-right" >
                                  <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip">
                                    <i class="fa fa-plus"></i></button>
                                  </div>
                                </div>
                                <span class="help-block"><span class="glyphicon glyphicon-tags"></span>       *En este campo encontrarás la información de los riegos de cada una de las sedes educativas.</span>
                                <div class="box-body"  >
                                  <div class="margin" id="botones_control">
                                    <a href="javascript:void(0);" style="margin-bottom: 10px;" class="btn btn-primary" onclick="cargar_formulario(41);">Ingresar riesgos</a>
                                  </div>
                                  <div id"tabla-responsive" class="table-responsive" >

                                    <table id="tablariesgos"  class="mdl-data-table" style="width:100%">
                                      <thead>
                                        <tr>
                                          <Th>Dane Sede</Th>
                                          <Th>Nombre establecimiento</Th>
                                          <Th>Nombre sede</Th>
                                          <Th>Riesgos</Th>
                                          <Th>Acción</Th>
                                        </tr>
                                      </thead>
                                    </table>

                                  </div>
                                </div>
                              </div>

                              <script>

                              $(document).ready(function() {
                                activar_tabla_empresas();
                                function activar_tabla_empresas() {
                                  $('#tablariesgos').DataTable({
                                    processing: true,
                                    serverSide: true,
                                    pageLength: 10,
                                    language: {
                                      "url": '{!! asset('/plugins/datatables/latino.json') !!}'
                                    } ,
                                    ajax: '{{ route('datatable.sedesriesgos') }}',
                                    columns: [
                                      { data: 'codigo_dane', name: 'codigo_dane' },
                                      { data: 'nombre_establecimiento', name: 'nombre_establecimiento' },
                                      { data: 'nombre_sede', name: 'nombre_sede' },
                                      { data: 'riesgos', name: 'riesgos' },
                                      { data: null,  render: function ( data, type, row )
                                        {
                                          return "<button type='button' title='Editar' onclick='verinfo_sedesriesgos("+ data.id +")' class='btn btn-warning btn-xs' ><i class='fa fa-edit'></i></button>"+
                                          "@role('administrador_sistema') <button type='button'  onclick='borrado_sedesriesgos(" + data.id +")' class='btn  btn-danger btn-xs' ><i class='fa fa-fw fa fa-trash-o' title='Eliminar'></i></button>@endrole"

                                        }
                                      }

                                    ]

                                  });

                                }
                              });

                              </script>

                            </section>



                          @endsection
                        @else
                          @extends('errors.acceso')
                        @endif
