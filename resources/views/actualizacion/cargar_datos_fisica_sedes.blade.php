@extends('layouts.app')

@section('htmlheader_title')
Carga Masiva de Datos Docentes
@endsection

@section('main-content')


<section  id="contenido_principal">
  <div class="box box-primary">
                  <div class="box-header">
                    <h3 class="box-title">Carga Masiva información sedes educativas</h3>
                  </div><!-- /.box-header -->

  <div id="notificacion_resul_fci"></div>

<div id="formulario_archivos_fisica_sedes">
  <form  id="f_subir_archivos_fisica_sedes" name="f_subir_archivos_fisica_sedes" method="post"  files=”true” action="{{ url('subir_archivos_fisica_sedes') }}" class="formarchivodocentes" enctype="multipart/form-data" >
{{ csrf_field() }}


  <div class="box-body ">

<div class="panel panel-primary ">
   <div class="panel-heading">informacion sedes</div>
<div class="panel-body">
  <div class="form-group col-xs-6"  >
         <label>Agregar Archivo  <span class="label label-primary">PP_Docente</span></label>
  <input name="archivo_fisica_sedes" id="archivo_fisica_sedes" type="file" files=”true”  enctype=”multipart/form-data”  class="archivo form-control"  required/><br /><br />
  </div>
</div>



</div>

</div>

<div class="box-footer">


  <button type="submit" id="btnsubmit"  class="btn btn-primary pull-right glyphicon glyphicon-open"> Cargar Archivos</button>

  </div>

  </div>

  </form>
  </div>

</div>





</section>
@endsection
