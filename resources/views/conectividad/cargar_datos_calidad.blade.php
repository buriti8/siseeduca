@if(Auth::user()->isRole('administrador_sistema') || Auth::user()->isRole('administrador_infraestructura_tecnologica'))
@extends('layouts.app')

@section('htmlheader_title')
  Carga Masiva de Datos Infraestructura Tecnológica - Calidad de servicio
@endsection


@section('main-content')


  <section  id="contenido_principal">
    <div class="box box-primary">
      <div class="box-header">
        <h3 class="text-center"><b>Carga masiva calidad de servicio</b></h3>
      </div><!-- /.box-header -->

      <div id="notificacion_resul_fci"></div>

      <div id="formulario_archivos_calidad">
        <form  id="f_subir_archivos_calidad" name="f_subir_archivo_calidad" method="post"  files=”true” action="{{ url('subir_archivos_calidad') }}" class="formarchivocalidad" enctype="multipart/form-data" >
          {{ csrf_field() }}

          <h5 style="margin-left:25px;"><b>Recomendaciones:</b></h5>
          <ol>
            <li><p>Aquí podrá ingresar información de la calidad del servicio por medio del archivo "calidad.csv.Llene los requerimientos obligatorios y presione el botón "Cargar archivos".</p>
            <li><p>La plantilla para verificar el encabezado de la carga la podrá descargar desde el botón "Descargar plantilla".</p>
              <li><p>El orden y el nombre de las columnas debe permanecer igual.</p>
               <li><p>Recuerde que antes de cargar el archivo debe convertirlo a UTF-8.</p>
                 <!-- /El botón llama el id modal-utf8 para cargar video conversión de archivos a utf8 desde app.blade -->
                 <button class="btn btn-info fa fa-video-camera" style="font-size:15px;" data-target="#modal-utf8-Calidad" data-toggle="modal" type="button"> Conversión archivos UTF-8</button>
                 <!-- /Fin botón utf8 -->

              </ol>


          <div class="box-body">

            <div class="form-group col-xs-6"  style="width:380px">

              <label for="mes">Mes :</label>

              <select id="mes" name="mes" class="form-control" value="" required>
                <option selected="selected"></option>

                @foreach($meses as $mes)
                  <option value="{{ $mes->id}}">{{ $mes->mes }}</option>
                @endforeach

              </select>

            </div>


            <div class="form-group col-xs-6"  style="width:380px">

              <label for="anio">Año :</label>
              <input name="anio" id="anio" type="number"   class="archivo form-control" min="2018" max="2019" required/><br />

            </div>


          </div>

          <div class="box-body ">

            <div class="panel panel-primary ">
              <div class="panel-heading">Calidad </div>
              <div class="panel-body">
                <div class="form-group col-xs-6" style="width:745px" >
                  <label>Agregar archivo  <span class="label label-primary">Calidad </span></label>
                  <input name="archivo_calidad" id="archivo_calidad" type="file" files=”true”  enctype=”multipart/form-data”  class="archivo form-control"  required/><br /><br />
                </div>
              </div>


            </div>



          </div>

          <div class="box-footer">


            <button type="submit" id="btnsubmit"  class="btn btn-primary fa fa-cloud-upload" style="font-size:15px;"> Cargar archivos</button>
            <a onclick="return confirm('Se debe convertir el archivo a UTF-8.')" href="{{ url('/descargar_plantilla_calidad') }}" target="_blank" id="descargar_plantilla" name="descargar_plantilla"  class="btn btn-info fa fa-cloud-download" style="font-size:15px;"> Descargar plantilla</a>
          </div>



        </div>

      </form>

    </div>

    <div id="anexos" class="box box-primary">

      <h3 class="text-center">Archivos cargados</h3>
      <div class="box-body"  >

        <div class="table-responsive" >

          <table  class="table table-hover table-striped" cellspacing="0" width="100%">

            <thead>
              <tr>
                <th>Año de reporte</th>
                <th>Mes de reporte</th>
                <th data-type="date">Fecha</th>
                <th>Cantidad filas ingresadas</th>
                <th>Acción</th>

              </tr>
            </thead>
            <tbody>

              @foreach($listados as $listado)
                <tr role="row" class="odd">
                  <td>{{ $listado->anio_corte }}</td>
                  <td>{{ ($listado->mes_corte) ==1 ? "Enero" : "" }}
                    {{ ($listado->mes_corte) ==2 ? "Febrero" : "" }}
                    {{ ($listado->mes_corte) ==3 ? "Marzo " : "" }}
                    {{ ($listado->mes_corte) ==4 ? "Abril" : "" }}
                    {{ ($listado->mes_corte) ==5 ? "Mayo" : "" }}
                    {{ ($listado->mes_corte) ==6 ? "Junio" : "" }}
                    {{ ($listado->mes_corte) ==7 ? "Julio" : "" }}
                    {{ ($listado->mes_corte) ==8 ? "Agosto" : "" }}
                    {{ ($listado->mes_corte) ==9 ? "Septiembre" : "" }}
                    {{ ($listado->mes_corte) ==10 ? "Octubre" : "" }}
                    {{ ($listado->mes_corte) ==11 ? "Noviembre" : "" }}
                    {{ ($listado->mes_corte) ==12 ? "Diciembre" : "" }}
                  </td>
                  <td >{{ Carbon\Carbon::parse($listado->fecha)->format('d-m-Y h:i A') }}</td>

                  <td >{{ $listado->cantidad_calidad }}</td>


                  <td >
                      <button type="button"   class="btn  btn-danger btn-xs"  title="Eliminar" onclick="borrar_archivos_calidad({{  $listado->anio_corte }},{{ $listado->mes_corte }});"  ><i class="fa fa-fw fa fa-trash-o"></a></i></button>
                    </span>
                  </td>

                </tr>
              @endforeach
            </tbody>
          </table>

          {{ $listados->links() }}
        </div>
      </div>
    </div>





  </section>
@endsection
@else
  @extends('errors.acceso')
@endif
