@extends('layouts.app')

@section('htmlheader_title')
Carga Masiva de Datos Infraestructura Tecnologica - Conectividad
@endsection


@section('main-content')


<section  id="contenido_principal">
  <div class="box box-primary">
                  <div class="box-header">
                    <h3 class="box-title">Carga masiva mesa de ayuda </h3>
                  </div><!-- /.box-header -->

  <div id="notificacion_resul_fci"></div>

<div id="formulario_archivos_mesa">
  <form  id="f_subir_archivos_mesa" name="f_subir_archivo_mesa" method="post"  files=”true” action="{{ url('subir_archivos_mesa') }}" class="formarchivomesa" enctype="multipart/form-data" >
{{ csrf_field() }}

    <div class="box-body">

    <div class="form-group col-xs-6"  >

    <label for="mes">Mes :</label>

    <select id="mes" name="mes" class="form-control" value="" required>
    <option selected="selected"></option>

    @foreach($meses as $mes)
    <option value="{{ $mes->id}}">{{ $mes->mes }}</option>
    @endforeach

    </select>

</div>


<div class="form-group col-xs-6"  >

<label for="anio">Año :</label>
  <input name="anio" id="anio" type="number"   class="archivo form-control" min="2000" max="2100" required/><br />

</div>


  </div>

  <div class="box-body ">

<div class="panel panel-primary ">
   <div class="panel-heading">Incidentes abiertos </div>
<div class="panel-body">
  <div class="form-group col-xs-6"  >
         <label>Agregar archivo  <span class="label label-primary">Incidentes abiertos </span></label>
  <input name="archivo_mesa" id="archivo_mesa" type="file" files=”true”  enctype=”multipart/form-data”  class="archivo form-control"  required/><br /><br />
  </div>
</div>

<div class="panel-heading">Incidentes cerrados</div>
<div class="panel-body ">
<div class="form-group col-xs-6"  >
      <label>Agregar archivo  <span class="label label-primary">Incidentes cerrados</span></label>
<input name="archivo_mesa2" id="archivo_mesa2" type="file" files=”true”  enctype=”multipart/form-data”  class="archivo form-control"  required/><br /><br />
</div>
</div>


</div>



</div>

<div class="box-footer">


  <button type="submit" id="btnsubmit"  class="btn btn-primary fa fa-cloud-upload" style="font-size:15px;"> Cargar archivos</button>

  </div>



  </div>

  </form>

</div>

<div id="anexos" class="box box-primary">

  <h3 class="box-title">Archivos cargados</h3>
  <div class="box-body"  >

  <div class="table-responsive" >

    <table  class="table table-hover table-striped" cellspacing="0" width="100%">

        <thead>
            <tr>
              <th>Año de reporte</th>
              <th>Mes de reporte</th>
              <th data-type="date">Fecha</th>
              <th>Acción</th>
            </tr>
        </thead>
      <tbody>

      @foreach($listados as $listado)
    <tr role="row" class="odd">
      <td>{{ $listado->anio_corte }}</td>
      <td>{{ ($listado->mes_corte) ==1 ? "Enero" : "" }}
          {{ ($listado->mes_corte) ==2 ? "Febrero" : "" }}
          {{ ($listado->mes_corte) ==3 ? "Marzo " : "" }}
          {{ ($listado->mes_corte) ==4 ? "Abril" : "" }}
          {{ ($listado->mes_corte) ==5 ? "Mayo" : "" }}
          {{ ($listado->mes_corte) ==6 ? "Junio" : "" }}
          {{ ($listado->mes_corte) ==7 ? "Julio" : "" }}
          {{ ($listado->mes_corte) ==8 ? "Agosto" : "" }}
          {{ ($listado->mes_corte) ==9 ? "Septiembre" : "" }}
          {{ ($listado->mes_corte) ==10 ? "Octubre" : "" }}
          {{ ($listado->mes_corte) ==11 ? "Noviembre" : "" }}
          {{ ($listado->mes_corte) ==12 ? "Diciembre" : "" }}
</td>
      <td >{{ Carbon\Carbon::parse($listado->fecha)->format('d-m-Y h:i A') }}</td>


<td >
      <button type="button"  class="btn  btn-danger btn-xs"  title="Eliminar" onclick="borrar_archivos_mesa({{  $listado->anio_corte }},{{ $listado->mes_corte }});"  ><i class="fa fa-fw fa fa-trash-o"></i></button>
</td>
    </tr>
      @endforeach
    </tbody>
    </table>

  {{ $listados->links() }}
  </div>
</div>
</div>





</section>
@endsection
