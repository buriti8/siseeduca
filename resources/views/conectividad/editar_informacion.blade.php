@extends('layouts.app')

@section('htmlheader_title')
Editar Información
@endsection


@section('main-content')
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
<section  id="contenido_principal">
  <div class="box-header">
    <h3 class="box-title">Editar información  </h3>
  </div><!-- /.box-header -->


  <!-- Caja para estructura de RectorDirectors -->
    <div class="box box-solid box-primary collapsed-box"  >
        <div class="box-header with-border" >
          <h3 class="box-title" >Rector director</h3>
          <div class="box-tools pull-right" >
            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip">
              <i class="fa fa-plus"></i></button>
          </div>
        </div>


        <div class="box-body"  >

          <div class="margin" id="botones_control">
            <a href="javascript:void(0);" class="btn btn-xs btn-primary" onclick="cargar_formulario(23);">Nuevo Rector director</a>
          </div>

          {{ $rectordirectors->links() }}

          @if(count($rectordirectors)==0)


          <div class="box box-primary col-xs-12">

          <div class='aprobado' style="margin-top:70px; text-align: center">

          <label style='color:#177F6B'>
                        ... No se encontraron Rectores directores ...
          </label>

          </div>

           </div>


          @endif

          <div id"tabla-responsive" class="table-responsive" >

            <table id="tablarectordirector"  class="mdl-data-table" style="width:100%">
              <thead>
                  <tr><th>CC</th>
                      <th>Nit</th>
                      <th>Nombre</th>
                      <th>TelefonoFijo</th>
                      <th>PaginaWeb</th>
                      <th>Email</th>
                      <th>Código Dane Municipio</th>
                      <th>Nombre del Municipio</th>
                      <th>Acciones</th>
                  </tr>
              </thead>
          </table>

          </div>
        </div>
        <!-- /.box-body -->
      </div>

      <!-- /.Fin caja RectorDirectors-->
      <script>

      $(document).ready(function() {
        activar_tabla_empresas();
        function activar_tabla_empresas() {
           $('#tablarectordirector').DataTable({
               processing: true,
               serverSide: true,
               pageLength: 10,
               language: {
                        "url": '{!! asset('/plugins/datatables/latino.json') !!}'
                         } ,
               ajax: '{{ route('datatable.rectordirector') }}',
               columns: [
                   { data: 'CC', name: 'CC' },
                   { data: 'Nit', name: 'Nit' },
                   { data: 'Nombre', name: 'Nombre' },
                   { data: 'TelefonoFijo', name: 'TelefonoFijo' },
                   { data: 'PaginaWeb', name: 'PaginaWeb' },
                   { data: 'Email', name: 'Email' },
                   { data: null,  render: function ( data, type, row )
                    {
                      return "<button type='button'  onclick='verinfo_rectordirector("+ data.id +")' class='btn  btn-warning btn-xs' ><i class='fa fa-fw fa-edit'></i></button>"+"&nbsp;&nbsp;&nbsp;&nbsp;"+"<button type='button' onclick='borrado_rectordirector("+ data.id +")' class='btn  btn-danger btn-xs' ><i class='fa fa-fw fa fa-trash-o'></i></button>"
                    }
                    }

               ]

           });

       }
        });

      </script>


      <!-- /.Fin caja MUNICIPIO-->
    </section>

    @endsection
