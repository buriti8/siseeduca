@extends('layouts.app')

@section('htmlheader_title')
  Editar Información
@endsection

@section('main-content')
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
  <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>

  <!-- Inicio contenido principal -->
  <section  id="contenido_principal">

    <div class="box box-primary">
      <div class="box-header" style="text-align:center;">
        <h3><b>Conectividad propia de las sedes</b></h3>
      </div><!-- /.box-header -->


      <div id="notificacion_resul_fci"></div>
      <div class="box box-primary" style="border: 1px solid #3c8dbc !important;">
          <div class="titleboxes" style="font-size:20px; text-align:center">Listado</div>



        <div class="box-body"  >



          <div class="margin" id="botones_control">
            <a href="javascript:void(0);" class="btn btn btn-primary" style="margin-bottom:10px;" onclick="cargar_formulario(33);">Ingresar conectividad</a>
          </div>



          <div class="table-responsive" >

            <table id="tablasede_conectividads"  class="table table-hover table-striped" cellspacing="0" width="100%">
              <thead>
                <tr>
                  <th> <span class="tool-tip" data-toggle="tooltip" data-placement="top" title="Municipio al que pertence">  Municipio  </span></th>
                  <th> <span class="tool-tip" data-toggle="tooltip" data-placement="top" title="Código dane de la sede educativa">  Código DANE  </span></th>
                  <th> <span class="tool-tip" data-toggle="tooltip" data-placement="top" title="Nombre establecimiento educativa">  Nombre establecimiento  </span></th>
                  <th> <span class="tool-tip" data-toggle="tooltip" data-placement="top" title="Nombre sede educativa">  Nombre sede  </span></th>
                  <th> <span class="tool-tip" data-toggle="tooltip" data-placement="top" title="Dirección de la sede educativa">  Dirección  </span></th>
                  <th> <span class="tool-tip" data-toggle="tooltip" data-placement="top" title="Programa por el cual se lleva acabo el contrato de internet de la sede"> Programa  </span></th>
                  <th> <span class="tool-tip" data-toggle="tooltip" data-placement="top" title="Numero de contrato por el cual se lleva acabo la conexión de la sede educativa">  Numero de contrato   </span></th>
                  <th> <span class="tool-tip" data-toggle="tooltip" data-placement="top" title="Operador con el cual se contrata la conexión de la sede educativa"> Operador  </span></th>
                  <th> <span class="tool-tip" data-toggle="tooltip" data-placement="top" title="Ancho de banda de la conexión a internet en megabyte, de la sede educativa"> Ancho de banda  </span></th>
                  <th> <span class="tool-tip" data-toggle="tooltip" data-placement="top" title="fecha de inicio del contrato"> Fecha inicio  </span></th>
                  <th> <span class="tool-tip" data-toggle="tooltip" data-placement="top" title="fecha de finalizacion del contrato"> Fecha fin  </span></th>
                  <th> <span class="tool-tip" data-toggle="tooltip" data-placement="top" title="Estado de la conexión de la sede"> Estado </span></th>
                  <th> <span class="tool-tip" data-toggle="tooltip" data-placement="top" title="Tipo de servicio que requiere la sede"> Servicio </span></th>
                  <th> <span class="tool-tip" data-toggle="tooltip" data-placement="top" title="Tipo de uso del servicio"> Uso </span></th>

                  <th>Acción</th>
                </tr>
              </thead>
            </table>
          </div>
        </div>

        <script>

        $(document).ready(function() {
          activar_tabla_empresas();
          function activar_tabla_empresas() {
            $('#tablasede_conectividads').DataTable({
              processing: true,
              serverSide: true,
              pageLength: 10,
              language: {
                "url": '{!! asset('/plugins/datatables/latino.json') !!}'
              } ,
              ajax: '{{ route('datatable.sede_conectividads') }}',
              columns: [
                { data: 'nombre_municipio', name: 'nombre_municipio' },
                { data: 'Codigodanesede', name: 'Codigodanesede' },
                { data: 'nombre_establecimiento', name: 'nombre_establecimiento' },
                { data: 'nombre_sede', name: 'nombre_sede' },
                { data: 'direccion', name: 'direccion' },
                { data: 'Programaorigendelosrecursos', name: 'Programaorigendelosrecursos' },
                { data: 'Numerodecontrato', name: 'Numerodecontrato' },
                { data: 'Operador', name: 'Operador' },
                { data: 'Anchodebanda', name: 'Anchodebanda' },
                { data: 'Fechainicioservicio', name: 'Fechainicioservicio' },
                { data: 'Fechafinservicio', name: 'Fechafinservicio' },
                { data: 'Estado', name: 'Estado' },
                { data: 'Servicio', name: 'Servicio' },
                { data: 'Uso', name: 'Uso' },


                { data: null,  render: function ( data, type, row )
                  {
                    return "<button type='button' title='Editar' onclick='verinfo_sedesconectividad("+ data.id +")' class='btn btn-warning btn-xs' ><i class='fa fa-edit'></i></button>"+"&nbsp;"
                    +"<button type='button' title='Borrar' onclick='borrado_sedesconectividad("+ data.id +")' class='btn btn-danger btn-xs' ><i class='fa fa-trash'></i></button>"

                  }
                }

              ]

            });

          }
        });

        </script>

      </section>
      <!-- Fin contenido principal -->

    @endsection
