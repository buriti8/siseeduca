@extends('layouts.app')

@section('htmlheader_title')
Carga Masiva de Datos Infraestructura Tecnologica - Dotacion
@endsection


@section('main-content')


<section  id="contenido_principal">
  <div class="box box-primary">
                  <div class="box-header">
                    <h3 class="box-title">Carga Masiva Inventario de Equipos por Sede </h3>
                  </div><!-- /.box-header -->

  <div id="notificacion_resul_fci"></div>

<div id="formulario_archivos_dotacion">
  <form  id="f_subir_archivos_dotacion" name="f_subir_archivo_dotacion" method="post"  files=”true” action="{{ url('subir_archivos_dotacion') }}" class="formarchivodotacion" enctype="multipart/form-data" >
{{ csrf_field() }}

    <div class="box-body">

    <div class="form-group col-xs-6"  >

    <label for="mes">Mes :</label>

    <select id="mes" name="mes" class="form-control" value="" required>
    <option selected="selected"></option>

    @foreach($meses as $mes)
    <option value="{{ $mes->id}}">{{ $mes->mes }}</option>
    @endforeach

    </select>

</div>


<div class="form-group col-xs-6"  >

<label for="anio">Año :</label>
  <input name="anio" id="anio" type="number"   class="archivo form-control" min="2000" max="2100" required/><br />

</div>


  </div>

  <div class="box-body ">

<div class="panel panel-primary ">
   <div class="panel-heading">Inventario de Equipos </div>
<div class="panel-body">
  <div class="form-group col-xs-6"  >
         <label>Agregar Archivo  <span class="label label-primary">Dotacion </span></label>
  <input name="archivo_dotacion" id="archivo_dotacion" type="file" files=”true”  enctype=”multipart/form-data”  class="archivo form-control"  required/><br /><br />
  </div>
</div>




</div>



</div>

<div class="box-footer">


  <button type="submit" id="btnsubmit"  class="btn btn-primary pull-right glyphicon glyphicon-open"> Cargar Archivos</button>

  </div>



  </div>

  </form>

</div>





</section>
@endsection
