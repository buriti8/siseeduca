@extends('layouts.app')

@section('htmlheader_title')
Carga Masiva de Datos Docentes
@endsection

@section('main-content')


<section  id="contenido_principal">
  <div class="box box-primary">
                  <div class="box-header">
                    <h3 class="box-title">Carga Masiva información Docentes</h3>
                  </div><!-- /.box-header -->

  <div id="notificacion_resul_fci"></div>

<div id="formulario_archivos_docentes">
  <form  id="f_subir_archivos_docentes" name="f_subir_archivo_docentes" method="post"  files=”true” action="{{ url('subir_archivos_docentes') }}" class="formarchivodocentes" enctype="multipart/form-data" >
{{ csrf_field() }}

<div class="panel panel-primary ">
   <div class="panel-heading">Fecha de Corte</div>
    <div class="box-body">


    <div class="form-group col-xs-6"  >

    <label for="mes">Mes :</label>

    <select id="mes" name="mes" class="form-control" value="" required>
    <option selected="selected"></option>

    @foreach($meses as $mes)
    <option value="{{ $mes->id}}">{{ $mes->mes }}</option>
    @endforeach

    </select>

</div>


<div class="form-group col-xs-6"  >

<label for="anio">Año :</label>
  <input name="anio" id="anio" type="number"   class="archivo form-control" min="2000" max="2100" required/><br />

</div>
</div>
</div>


  <div class="box-body ">

<div class="panel panel-primary ">
   <div class="panel-heading">Planta de docentes de aula</div>
<div class="panel-body">
  <div class="form-group col-xs-6"  >
         <label>Agregar Archivo  <span class="label label-primary">PP_Docente</span></label>
  <input name="archivo_pp_docente" id="archivo_pp_docente" type="file" files=”true”  enctype=”multipart/form-data”  class="archivo form-control"  required/><br /><br />
  </div>
</div>

<div class="panel-heading">Planta de personal docente</div>
<div class="panel-body">
<div class="form-group col-xs-6"  >
      <label>Agregar Archivo  <span class="label label-primary">EO_PCARGOS</span></label>
<input name="archivo_eo_pcargos" id="archivo_eo_pcargos" type="file" files=”true”  enctype=”multipart/form-data”  class="archivo form-control" required /><br /><br />
</div>
</div>

<div class="panel-heading">Planta de directivos docentes</div>
<div class="panel-body">
<div class="form-group col-xs-6"  >
      <label>Agregar Archivo  <span class="label label-primary">EO_DIRDOC</span></label>
<input name="archivo_eo_dirdoc" id="archivo_eo_dirdoc" type="file" files=”true”  enctype=”multipart/form-data”  class="archivo form-control" required/><br /><br />
</div>
</div>

</div>

</div>

<div class="box-footer">


  <button type="submit" id="btnsubmit"  class="btn btn-primary pull-right glyphicon glyphicon-open"> Cargar Archivos</button>

  </div>

  </div>

  </form>
  </div>

</div>





</section>
@endsection
