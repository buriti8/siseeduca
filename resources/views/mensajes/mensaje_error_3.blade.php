@extends('layouts.app')

@section('htmlheader_title')
Error
@endsection

@section('main-content')

 <br/><div class='rechazado'><label style='color:#FA206A'><?php  echo $msj; ?></label>  </div>

<div class="col-xs-12" style="margin-top:15px;">    
    <a href="{{ url('traslado_dispositivos') }}" class="btn btn-info pull-right">VER LISTADO DE TRASLADOS</a>
</div> 

@if (count($errors) > 0)
       <div class="alert alert-danger">
           <strong>Whoops!</strong> {{ trans('adminlte_lang::message.someproblems') }}<br><br>
           <ul>
               @foreach ($errors->all() as $error)
                   <li>{{ $error }}</li>
               @endforeach
           </ul>
       </div>
 @endif

 @endsection
