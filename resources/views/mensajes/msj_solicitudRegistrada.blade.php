@extends('layouts.app')

@section('htmlheader_title')
Solicitudes Infraestructura Fisica
@endsection

@section('main-content')

  <!-- modal encargado de mostrar la confirmación de diagnóstico de necesidadess registrados al usuario-->
  <div class="btn-group" style="margin-left:50px; " >
    <div class="modal fade in" id='verInformacion'  role="dialog" style="display: block; padding-right: 17px;">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
          </div>
          <div class="modal-body">
            <center>
              <h4>
                <strong>
                          Diagnóstico de necesidades registrado
                </strong>
              </h4>
            </center>
          </div>
          <div class="modal-footer">
            @if(Auth::user()->isRole('rectores'))
              <a href='{{url("/requerimientosIFisica/".Auth::user()->name."/establecimiento")}}' class="btn btn-primary">Ver diagnóstico de necesidades registrados</a>
            @elseif (Auth::user()->isRole('secretarios'))
              <a href='{{url("/requerimientosIFisica/".Auth::user()->name."/municipio")}}' class="btn btn-primary">Ver diagnóstico de necesidades registrados</a>
            @else
              <a href="{{url('/requerimientosIFisica/consult')}}" class="btn btn-primary">Ver diagnóstico de necesidades registrados</a>
            @endif
            <a href="/home" type="button" class="btn btn-primary">Cerrar</a>
          </div>
        </div>
      </div>
    </div>
  </div>

 @endsection
