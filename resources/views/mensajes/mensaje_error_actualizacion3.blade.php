
<!-- modal encargado de mostrar la confirmación de la carga del archivo al usuario-->
<div class="btn-group" style="margin-left:50px; " >
  <div class="modal fade in" role="dialog" style="display: block; padding-right: 17px;">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
        </div>
        <div class="modal-body">
          <center>
            @foreach ($nombres as $nombre)
              <h4>La información de los riesgos de la sede educativa <b>{{$nombre->nombre_sede}}</b>, ya fue ingresada. Por favor, ingrese los servicios.</h4>
            @endforeach

          </center>
        </div>
          <div class="modal-footer">
            <div class="text-center">
                <a href="javascript:void(0);" style="margin-bottom: 10px;" class="btn btn-primary" onclick="cargar_formulario(43);">Ingresar los servicios</a>
                <a onclick="cerrar_modal()" style="margin-bottom:10px;" class="btn btn-primary">Cancelar</a><!-- Botón para cancerlar el ingreso de una nueva sede educativa
                  y volver a la vista sin necesidad de recargar la página -->
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
