@extends('layouts.app')
@section('htmlheader_title')
  Requerimientos Infraestructura Educativa
@endsection
@section('main-content')

  <!-- modal encargado de notificar al usuario, que la información de los riesgos no ha sido ingresada-->
  <div class="btn-group" style="margin-left:50px; " >
    <form  action="{{ url('eliminaSolicitudIF') }}" method="POST">
      <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
      @foreach ($nombres as $nombre)
        <input type="hidden" id="selSede" name="selSede" value="{{ $nombre->codigo_sede}}">
      @endforeach

      <div class="modal fade in" role="dialog" style="display: block; padding-right: 17px;">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
            </div>
            <div class="modal-body">
              <center>
                @foreach ($nombres as $nombre)
                  <h4>La información de los riesgos de la sede educativa <b>{{$nombre->nombre_sede}}</b>, no ha sido ingresada. Por favor, ingrese la información para continuar con el registro del diagnóstico de necesidades.</h4>
                @endforeach
              </center>
            </div>
            <div class="modal-footer">
              <div class="text-center">
              <a href="javascript:void(0);" style="margin-bottom: 10px;" class="btn btn-primary" onclick="cargar_formulario(42);">Ingresar riesgos</a>
              <button type="submit" style="margin-bottom: 10px;" onclick="return confirm('Si cancela el proceso; se eliminará el diagnóstico de necesidades ingresado. ¿Desea cancelar el proceso?')"  class="btn btn-primary">Cancelar</button>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="container-fluid spark-screen">
    <div class="row">
      <div class="col-md-8 col-md-offset-2">
        <div class="panel panel-default" style="text-align:center">
          <div class="panel-heading">
            <h4>Sistema de información educativo de la Gobernación de Antioquia</h4>
          </div>
          <div class="panel-body">
            <img src="{{ asset('/img/asd.png') }}" >
          </div>
        </div>
      </div>
    </div>
  </div>
</form>
@endsection
