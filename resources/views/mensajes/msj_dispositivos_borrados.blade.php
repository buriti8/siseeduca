<@extends('layouts.app')

  @section('htmlheader_title')
  Borrado
  @endsection

  @section('main-content')

  <!-- modal encargado de mostrar la confirmación de la carga del archivo al usuario-->
  <div class="btn-group" style="margin-left:50px; " >
    <div class="modal fade in" role="dialog" style="display: block; padding-right: 17px;">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
          </div>
          <div class="modal-body">
            <center>
              <h4><b>Borrado correctamente.</b></h4>
            </center>
          </div>
            <div class="modal-footer">
              <div class="text-center">
                <a href="/carga_masiva_dispositivos" class="btn btn-primary" value=" ">Regresar</a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>


  @endsection
