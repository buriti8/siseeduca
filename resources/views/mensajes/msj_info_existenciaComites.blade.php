@extends('layouts.app')
@section('htmlheader_title')
  Directorio municipal
@endsection
@section('main-content')


<div class="btn-group" style="margin-left:50px; " >
  <div class="modal fade in" id='verInformacion'  role="dialog" style="display: block; padding-right: 17px;">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
        </div>
        <div class="modal-body">
          <center>
            <h4>
                    @foreach ($municipios as $municipio)
                      El documento <strong>{{$informacion}}</strong> del municipio de <strong>{{$municipio->NombreMunicipio}}</strong>, ya fue anexado. por favor elija la opción editar.
                    @endforeach
            </h4>
          </center>
        </div>
        <div class="modal-footer">
          <center>
          <a href="/actualizar_directorio" class="btn btn-primary" value=" ">Regresar</a>
        </center>
        </div>
      </div>
    </div>
  </div>
</div>
