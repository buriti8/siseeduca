@extends('layouts.app')

@section('htmlheader_title')
Creado
@endsection

@section('main-content')

<!-- modal encargado de mostrar la confirmación de la actualización de datos al usuario-->
<div class="btn-group" style="margin-left:50px; " >
  <div class="modal fade in" id='verInformacion'  role="dialog" style="display: block; padding-right: 17px;">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
        </div>
        <div class="modal-body">
          <center>
            <h4>
              <strong>
                          Proyecto ingresado
              </strong>
            </h4>
          </center>
        </div>
        <div class="modal-footer">
          <a href="{{ url('editar_estructura_inventario') }}" class="btn btn-primary" value=" ">Refrescar</a>
        </div>
      </div>
    </div>
  </div>
</div>
 @endsection
