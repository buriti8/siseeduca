
<!-- modal encargado de mostrar la confirmación de la carga del archivo al usuario-->
<div class="btn-group" style="margin-left:50px; " >
  <div class="modal fade in" role="dialog" style="display: block; padding-right: 17px;">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
        </div>
        <div class="modal-body">
          <center>
            @foreach ($nombres as $nombre)
              <h4>La información de la sede educativa <b>{{$nombre->nombre_sede}}</b>, ya fue ingresada.</h4>
            @endforeach

          </center>
        </div>
          <div class="modal-footer">
            <div class="text-center">
              <a href="javascript:window.location.href=window.location.href" class="btn btn-primary" value=" ">Regresar</a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
