<div class="box box-primary col-xs-12">
    <div class='aprobado' style="margin-top:70px; text-align: center">
        <span class="label label-success">Borrado<i class="fa fa-check"></i></span><br/>
        <label style='color:#177F6B'>
                    <?php  echo $msj; ?>
        </label>
    </div>

    <div class="margin" style="margin-top:50px; text-align:center;margin-bottom: 50px;">
        <div class="btn-group" style="margin-left:50px;">
            <a href="{{ url('traslado_dispositivos') }}" class="btn btn-info pull-right">VER LISTADO DE TRASLADOS</a>
        </div>
    </div>
 </div>

 <@extends('layouts.app')

  @section('htmlheader_title')
  Borrado
  @endsection

  @section('main-content')

  <!-- modal encargado de mostrar la confirmación de la carga del archivo al usuario-->
  <div class="btn-group" style="margin-left:50px; " >
    <div class="modal fade in" role="dialog" style="display: block; padding-right: 17px;">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
          </div>
          <div class="modal-body">
            <center>
              <h4><b>Borrado correctamente.</b></h4>
            </center>
          </div>
            <div class="modal-footer">
              <div class="text-center">
                <a href="/traslado_dispositivos" class="btn btn-primary" value=" ">Regresar</a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>


  @endsection
