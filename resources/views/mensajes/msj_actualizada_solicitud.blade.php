@extends('layouts.app')

@section('htmlheader_title')
Actualizar
@endsection

@section('main-content')

<div class="btn-group" style="margin-left:50px; " >
  <div class="modal fade in" role="dialog" style="display: block; padding-right: 17px;">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
        </div>
        <div class="modal-body">
          <center>
            <h4><strong>Actualizado correctamente</strong></h4>
          </center>
        </div>
          <div class="modal-footer">
            <div class="text-center">
              <a href="/listado_solicitudes" class="btn btn-primary" value=" ">Regresar</a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

@endsection
