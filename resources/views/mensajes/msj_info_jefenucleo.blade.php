@extends('layouts.app')
@section('htmlheader_title')
  Directorio municipal
@endsection
@section('main-content')

  <!-- modal encargado de notificar al usuario, que la información de los riesgos no ha sido ingresada-->
  <div class="btn-group" style="margin-left:50px; " >
    <div class="modal fade in" role="dialog" style="display: block; padding-right: 17px;">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
          </div>
          <div class="modal-body">
            <center>
              <h4>
                  @if ($guardados==1)
                    La información del director(a) de núcleo de <strong>{{$array}}</strong>, fue ingresada.
                  @elseif ($guardados>=2)
                    La información del director(a) de núcleo de: <strong>{{$array}}</strong>, fue ingresada.
                  @endif
                  @if ($noguardados==1)
                    No fue ingresada la información del director(a) de núcleo de <strong>{{$array2}}</strong>, ya que esta, ya había sido ingresada. En este caso, elija la opción editar.
                  @elseif($noguardados>=2)
                    No fue ingresada la información del director(a) de núcleo de: <strong>{{$array2}}</strong>, ya que esta, ya había sido ingresada. En estos casos, elija la opción editar.
                  @endif

              </h4>
            </center>
          </div>
          <div class="modal-footer">
            <div class="text-center">
              <a href="/actualizar_directorio" class="btn btn-primary" value=" ">Regresar</a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
