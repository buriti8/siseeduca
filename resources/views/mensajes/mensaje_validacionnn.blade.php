@extends('layouts.app')
@section('htmlheader_title')
Nueva necesidad
@endsection
@section('main-content')

  <!-- modal encargado de notificar al usuario, que la información de los riesgos no ha sido ingresada-->
  <div class="btn-group" style="margin-left:50px; " >
    <form  action="{{ url('modal') }}" method="POST">
      <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
      @foreach ($nombres as $nombre)
        <input type="hidden" id="selSede" name="selSede" value="{{ $nombre->codigo_sede}}">
      @endforeach

      <div class="modal fade in" role="dialog" style="display: block; padding-right: 17px;">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
            </div>
            <div class="modal-body">
              <center>
                @foreach ($nombres as $nombre)
                  <h4>La sede educativa <b>{{$nombre->nombre_sede}}</b>, ya cuenta con conectividad. Esta necesidad va estar sujeta a revisión</h4>
                @endforeach
              </center>
            </div>
            <div class="modal-footer">
              <div class="text-center">
              <a href="{{ url('form_nueva_solicitud') }}" class="btn btn-primary" value=" ">Aceptar</a>
              <a href="{{ url('home') }}" class="btn btn-primary" value=" ">Cancelar</a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="container-fluid spark-screen">
    <div class="row">
      <div class="col-md-8 col-md-offset-2">
        <div class="panel panel-default" style="text-align:center">
          <div class="panel-heading">
            <h4>Sistema de información educativo de la Gobernación de Antioquia</h4>
          </div>
          <div class="panel-body">
            <img src="{{ asset('/img/asd.png') }}" >
          </div>
        </div>
      </div>
    </div>
  </div>
</form>
@endsection
