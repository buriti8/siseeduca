
<section>
  <!-- modal encargado de mostrar la confirmación de los riesgos y Diagnósticos registrados al usuario-->
  <div class="btn-group" style="margin-left:50px; " >
    <div class="modal fade in" id='verInformacion'  role="dialog" style="display: block; padding-right: 17px;">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
          </div>
          <div class="modal-body">
            <br>
            <center>
              <h4>
                <strong>
                          Registro de servicios y diagnóstico exitoso
                </strong>
              </h4>
            </center>
            <br>
            <br>
          </div>
          <div class="modal-footer">
            @if(Auth::user()->isRole('rectores'))
              <a href='{{url("/form_ingresar_sedesservicio/".Auth::user()->name."/establecimiento")}}' class="btn btn-primary">Ver diagnósticos registrados</a>
            @elseif (Auth::user()->isRole('secretarios'))
              <a href='{{url("/form_ingresar_sedesservicio/".Auth::user()->name."/municipio")}}' class="btn btn-primary">Ver diagnósticos registrados</a>
            @else
              <a href="{{url('/form_ingresar_sedesservicio/consult')}}" class="btn btn-primary">Ver diagnósticos registrados</a>
            @endif
            <a href="/home" type="button" class="btn btn-primary">Cerrar</a>
          </div>
        </div>
      </div>
    </div>
  </div>

</section>
