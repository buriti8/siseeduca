
<!-- modal encargado de mostrar la confirmación de la actualización de contraseña al usuario-->
<div class="btn-group" style="margin-left:50px; " >
  <div class="modal fade in" id='verInformacion'  role="dialog" style="display: block; padding-right: 17px;">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
        </div>
        <div class="modal-body">
          <center>
            <h4>
              <strong>
                          Contraseña actualizada
              </strong>
            </h4>
          </center>
        </div>
        <div class="modal-footer">
          <a href="{{ url('/home') }}" class="btn btn-primary" value=" ">Regresar</a>
        </div>
      </div>
    </div>
  </div>
</div>
