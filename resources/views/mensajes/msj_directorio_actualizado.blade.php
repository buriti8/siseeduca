<!-- modal encargado de mostrar la confirmación de la actualización de datos al usuario-->
<div class="btn-group" style="margin-left:50px; " >
  <div class="modal fade in" id='verInformacion'  role="dialog" style="display: block; padding-right: 17px;">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
        </div>
        <div class="modal-body">
          <center>
            <h4>
              <strong>
                          Información actualizada
              </strong>
            </h4>
          </center>
        </div>
        <div class="modal-footer">
          <center>
          <a href="{{ url('actualizar_directorio') }}" class="btn btn-primary" value=" ">Cerrar</a>
          <center>
        </div>
      </div>
    </div>
  </div>
</div>
