
@extends('layouts.app')

@section('htmlheader_title')
  Editar Estructura
@endsection


@section('main-content')
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
  <section  id="contenido_principal">
    <div class="box-header">
      <h3 class="text-center"><b>Editar estructura</b></h3>
    </div><!-- /.box-header -->

    <!-- Caja para Departamentos -->
    <div class="box box-solid box-primary collapsed-box" >
      <div class="box-header with-border" >
        <h3 class="box-title">Departamentos</h3>
        <div class="box-tools pull-right" >
          <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip">
            <i class="fa fa-plus"></i></button>
          </div>
        </div>


        <div class="box-body">

          <div class="margin" id="botones_control">
            <a href="javascript:void(0);" class="btn btn btn-primary" onclick="cargar_formulario(4);">Nuevo Departamento</a>
          </div>

          <div id="tabla-responsive" class="table-responsive" >

            <table  id="tbldepartamentos"  class="mdl-data-table" style="width:100%">
              <thead style="width:100%">
                <tr>
                  <th>Código Departamento</th>
                  <th>Código DANE</th>
                  <th>Nombre</th>
                </tr>
              </thead>

            </table>

          </div>

        </div>
        <!-- /.box-body -->

      </div>
      <script>

      $(document).ready(function() {
        activar_tabla_empresas();
        function activar_tabla_empresas() {
          $('#tbldepartamentos').DataTable({
            processing: true,
            serverSide: true,
            pageLength: 10,
            language: {
              "url": '{!! asset('/plugins/datatables/latino.json') !!}'
            } ,
            ajax: '{{ route('datatable.departamentos') }}',
            columns: [
              { data: 'CodigoDepartamento', name: 'CodigoDepartamento' },
              { data: 'CodigoDaneDepartamento', name: 'CodigoDaneDepartamento' },
              { data: 'NombreDepartamento', name: 'NombreDepartamento' }

            ]

          });

        }
      });

      </script>
      <!-- /.Fin caja Departamentos-->

      <!-- Caja para estructura  Subregion --->
      <div class="box box-solid box-primary collapsed-box" >
        <div class="box-header with-border" >
          <h3 class="box-title">Subregiones</h3>
          <div class="box-tools pull-right" >
            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip">
              <i class="fa fa-plus"></i></button>
            </div>
          </div>


          <div class="box-body">

            <div class="margin" id="botones_control">
              <a href="javascript:void(0);" class="btn btn btn-primary" onclick="cargar_formulario(20);">Nueva Subregion</a>
            </div>

            <div id="tabla-responsive" class="table-responsive" >

              <table  id="tblsubregiones"  class="mdl-data-table" style="width:100%">
                <thead>
                  <tr>
                    <th>Código Subregión</th>
                    <th>Nombre Subregión</th>
                    <th>Nombre del Departamento</th>
                  </tr>
                </thead>

              </table>

            </div>

          </div>
          <!-- /.box-body -->

        </div>
        <script>


        $(document).ready(function() {
          activar_tabla_empresas();
          function activar_tabla_empresas() {
            $('#tblsubregiones').DataTable({
              processing: true,
              serverSide: true,
              pageLength: 10,
              language: {
                "url": '{!! asset('/plugins/datatables/latino.json') !!}'
              } ,
              ajax: '{{ route('datatable.subregiones') }}',
              columns: [
                { data: 'CodigoSubregion', name: 'CodigoSubregion' },
                { data: 'NombreSubregion', name: 'NombreSubregion' },
                { data: 'CodigoDepartamento', name: 'CodigoDepartamento' }


              ]

            });

          }
        });

        </script>
        <!-- /.Fin caja SUBREGION-->

        <!-- Caja para estructura  MUNICIPIO --->
        <div class="box box-solid box-primary collapsed-box" >
          <div class="box-header with-border" >
            <h3 class="box-title">Municipio</h3>
            <div class="box-tools pull-right" >
              <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip">
                <i class="fa fa-plus"></i></button>
              </div>
            </div>


            <div class="box-body" >

              <div class="margin" id="botones_control">
                <a href="javascript:void(0);" class="btn btn btn-primary" onclick="cargar_formulario(22);">Nuevo Municipio</a>
              </div>

              <div id="tabla-responsive" class="table-responsive" >

                <table id="tblmunicipios"  class="mdl-data-table" style="width:100%">
                  <thead>
                    <tr>
                      <th>Código Municipio</th>
                      <th>Código Dane Municipio</th>
                      <th>Nombre Municipio</th>
                      <th>Código Subregion</th>
                    </tr>
                  </thead>
                </table>

              </div>

            </div>
            <!-- /.box-body -->

          </div>
          <script>


          $(document).ready(function() {
            activar_tabla_empresas();
            function activar_tabla_empresas() {
              $('#tblmunicipios').DataTable({
                processing: true,
                serverSide: true,
                pageLength: 10,
                language: {
                  "url": '{!! asset('/plugins/datatables/latino.json') !!}'
                } ,
                ajax: '{{ route('datatable.municipios') }}',
                columns: [
                  { data: 'CodigoMunicipio', name: 'CodigoMunicipio' },
                  { data: 'CodigoDaneMunicipio', name: 'CodigoDaneMunicipio' },
                  { data: 'NombreMunicipio', name: 'NombreMunicipio' },
                  { data: 'CodigoSubregion', name: 'CodigoSubregion' }

                ]

              });

            }
          });

          </script>
          <!-- /.Fin caja MUNICIPIO-->

          <!-- /caja TipoDocumento-->
          <div class="box box-solid box-primary collapsed-box" >
            <div class="box-header with-border" >
              <h3 class="box-title">Tipo de documento</h3>
              <div class="box-tools pull-right" >
                <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip">
                  <i class="fa fa-plus"></i></button>
                </div>
              </div>


              <div class="box-body" >

                <div class="margin" id="botones_control">
                  <a href="javascript:void(0);" class="btn btn btn-primary" onclick="cargar_formulario(5);">Nuevo tipo de documento</a>
                </div>

                <div id="tabla-responsive" class="table-responsive" >

                  <table id="tbltipodoc"  class="mdl-data-table" style="width:100%">
                    <thead>
                      <tr>
                        <th>Código tipo documento</th>
                        <th>Descripción</th>
                        <th>Abreviado</th>
                        <th>Acción</th>
                      </tr>
                    </thead>
                  </table>

                </div>

              </div>
              <!-- /.box-body -->

            </div>
            <script>


            $(document).ready(function() {
              activar_tabla_empresas();
              function activar_tabla_empresas() {
                $('#tbltipodoc').DataTable({
                  processing: true,
                  serverSide: true,
                  pageLength: 10,
                  language: {
                    "url": '{!! asset('/plugins/datatables/latino.json') !!}'
                  } ,
                  ajax: '{{ route('datatable.tipodoc') }}',
                  columns: [
                    { data: 'IdTipoDocumento', name: 'IdTipoDocumento' },
                    { data: 'Descripcion', name: 'Descripcion' },
                    { data: 'Abreviado', name: 'Abreviado' },
                    { data: null,  render: function ( data, type, row )
                      {
                        return "<button type='button' title='Editar' onclick='verinfo_tipodocumento("+ data.id +")' class='btn  btn-warning btn-xs' ><i class='fa fa-edit'></i></button>"+"&nbsp;"+"<button type='button' title='Borrar' onclick='borrado_tipodocumento("+ data.id +")' class='btn  btn-danger btn-xs' ><i class='fa fa-trash-o'></i></button>"
                      }
                    }

                  ]

                });

              }
            });

            </script>
            <!-- /.Fin caja TipoDocumento-->

            <!-- /caja Tipo de comité o junta-->
            <div class="box box-solid box-primary collapsed-box" >
              <div class="box-header with-border" >
                <h3 class="box-title">Comités o juntas de los municipios</h3>
                <div class="box-tools pull-right" >
                  <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip">
                    <i class="fa fa-plus"></i></button>
                  </div>
                </div>


                <div class="box-body" >

                  <div class="margin" id="botones_control">
                    <a href="javascript:void(0);" class="btn btn btn-primary" onclick="cargar_formulario(52);">Nuevo comité o junta</a>
                  </div>

                  <div id="tabla-responsive" class="table-responsive" >

                    <table id="tblcomitesJuntas"  class="mdl-data-table" style="width:100%">
                      <thead>
                        <tr>
                          <th>Código</th>
                          <th>Comité o junta</th>
                          <th>Acción</th>
                        </tr>
                      </thead>
                    </table>

                  </div>

                </div>
                <!-- /.box-body -->

              </div>
              <script>


              $(document).ready(function() {
                activar_tabla_empresas();
                function activar_tabla_empresas() {
                  $('#tblcomitesJuntas').DataTable({
                    processing: true,
                    serverSide: true,
                    pageLength: 10,
                    language: {
                      "url": '{!! asset('/plugins/datatables/latino.json') !!}'
                    } ,
                    ajax: '{{ route('datatable.comiteJunta') }}',
                    columns: [
                      { data: 'codigo', name: 'codigo' },
                      { data: 'comite_junta', name: 'comite_junta' },
                      { data: null,  render: function ( data, type, row )
                        {
                          return "<button type='button' title='Editar' onclick='verinfo_comiteJunta("+ data.id +")' class='btn  btn-warning btn-xs' ><i class='fa fa-edit'></i></button>"+"&nbsp;"+"<button type='button' title='Borrar' onclick='borrado_comiteJunta("+ data.id +")' class='btn  btn-danger btn-xs' ><i class='fa fa-trash-o'></i></button>"
                        }
                      }

                    ]

                  });

                }
              });

              </script>

              <!-- /caja Tipo de documento de los municipios-->
              <div class="box box-solid box-primary collapsed-box" >
                <div class="box-header with-border" >
                  <h3 class="box-title">Tipo de documento de los municipios</h3>
                  <div class="box-tools pull-right" >
                    <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip">
                      <i class="fa fa-plus"></i></button>
                    </div>
                  </div>


                  <div class="box-body" >

                    <div class="margin" id="botones_control">
                      <a href="javascript:void(0);" class="btn btn btn-primary" onclick="cargar_formulario(53);">Nuevo tipo de documento</a>
                    </div>

                    <div id="tabla-responsive" class="table-responsive" >

                      <table id="tbldocumentos"  class="mdl-data-table" style="width:100%">
                        <thead>
                          <tr>
                            <th>Código</th>
                            <th>Nombre documento</th>
                            <th>Acción</th>
                          </tr>
                        </thead>
                      </table>

                    </div>

                  </div>
                  <!-- /.box-body -->

                </div>
                <script>


                $(document).ready(function() {
                  activar_tabla_empresas();
                  function activar_tabla_empresas() {
                    $('#tbldocumentos').DataTable({
                      processing: true,
                      serverSide: true,
                      pageLength: 10,
                      language: {
                        "url": '{!! asset('/plugins/datatables/latino.json') !!}'
                      } ,
                      ajax: '{{ route('datatable.documento_editarEstructura') }}',
                      columns: [
                        { data: 'codigo', name: 'codigo' },
                        { data: 'documento', name: 'documento' },
                        { data: null,  render: function ( data, type, row )
                          {
                            return "<button type='button' title='Editar' onclick='verinfo_comiteDocumento("+ data.id +")' class='btn  btn-warning btn-xs' ><i class='fa fa-edit'></i></button>"+"&nbsp;"+"<button type='button' title='Borrar' onclick='borrado_documento_estructura("+ data.id +")' class='btn  btn-danger btn-xs' ><i class='fa fa-trash-o'></i></button>"
                          }
                        }

                      ]

                    });

                  }
                });

                </script>


            <!-- /caja Condición del alumno al finalizar el año anterior-->
            <div class="box box-solid box-primary collapsed-box" >
              <div class="box-header with-border" >
                <h3 class="box-title">Condición del alumno al finalizar el año anterior</h3>
                <div class="box-tools pull-right" >
                  <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip">
                    <i class="fa fa-plus"></i></button>
                  </div>
                </div>


                <div class="box-body" >

                  <div class="margin" id="botones_control">
                    <a href="javascript:void(0);" class="btn btn btn-primary" onclick="cargar_formulario(6);">Nueva condición</a>
                  </div>

                  <div id="tabla-responsive" class="table-responsive" >

                    <table id="tblconaluant"  class="mdl-data-table" style="width:100%">
                      <thead>
                        <tr>
                          <th>Código condición</th>
                          <th>Descripción</th>
                          <th>Acción</th>
                        </tr>
                      </thead>
                    </table>

                  </div>

                </div>
                <!-- /.box-body -->

              </div>
              <script>


              $(document).ready(function() {
                activar_tabla_empresas();
                function activar_tabla_empresas() {
                  $('#tblconaluant').DataTable({
                    processing: true,
                    serverSide: true,
                    pageLength: 10,
                    language: {
                      "url": '{!! asset('/plugins/datatables/latino.json') !!}'
                    } ,
                    ajax: '{{ route('datatable.conaluant') }}',
                    columns: [
                      { data: 'IdConAlunmAnt', name: 'IdConAlunmAnt' },
                      { data: 'Descripcion', name: 'Descripcion' },
                      { data: null,  render: function ( data, type, row )
                        {
                          return "<button type='button' title='Editar' onclick='verinfo_conalunmant("+ data.id +")' class='btn  btn-warning btn-xs' ><i class='fa fa-edit'></i></button>"+"&nbsp;"+"<button type='button' title='Borrar' onclick='borrado_conalunmant("+ data.id +")' class='btn  btn-danger btn-xs' ><i class='fa fa-trash-o'></i></button>"
                        }
                      }

                    ]

                  });

                }
              });

              </script>
              <!-- /.Fin Condición del alumno al finalizar el año anterior-->

              <!-- /Inicio caja sitacion académica anterior-->
              <div class="box box-solid box-primary collapsed-box" >
                <div class="box-header with-border" >
                  <h3 class="box-title">Situación académica año anterior</h3>
                  <div class="box-tools pull-right" >
                    <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip">
                      <i class="fa fa-plus"></i></button>
                    </div>
                  </div>


                  <div class="box-body" >

                    <div class="margin" id="botones_control">
                      <a href="javascript:void(0);" class="btn btn btn-primary" onclick="cargar_formulario(7);">Nueva situación</a>
                    </div>

                    <div id="tabla-responsive" class="table-responsive" >

                      <table id="tblsitacant"  class="mdl-data-table" style="width:100%">
                        <thead>
                          <tr>
                            <th>Código situación</th>
                            <th>Descripción</th>
                            <th>Acción</th>
                          </tr>
                        </thead>
                      </table>

                    </div>

                  </div>
                  <!-- /.box-body -->

                </div>
                <script>


                $(document).ready(function() {
                  activar_tabla_empresas();
                  function activar_tabla_empresas() {
                    $('#tblsitacant').DataTable({
                      processing: true,
                      serverSide: true,
                      pageLength: 10,
                      language: {
                        "url": '{!! asset('/plugins/datatables/latino.json') !!}'
                      } ,
                      ajax: '{{ route('datatable.sitacant') }}',
                      columns: [
                        { data: 'IdSitAcaAnt', name: 'IdSitAcaAnt' },
                        { data: 'Descripcion', name: 'Descripcion' },
                        { data: null,  render: function ( data, type, row )
                          {
                            return "<button type='button' title='Editar' onclick='verinfo_sitacaant("+ data.id +")' class='btn  btn-warning btn-xs' ><i class='fa fa-edit'></i></button>"+"&nbsp;"+"<button type='button' title='Borrar' onclick='borrado_sitacaant("+ data.id +")' class='btn  btn-danger btn-xs' ><i class='fa fa-trash-o'></i></button>"
                          }
                        }

                      ]

                    });

                  }
                });

                </script>
                <!-- /.Fin caja sitacion académica anterior-->

                <!-- Inicio caja zona -->
                <div class="box box-solid box-primary collapsed-box" >
                  <div class="box-header with-border" >
                    <h3 class="box-title">Zona</h3>
                    <div class="box-tools pull-right" >
                      <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip">
                        <i class="fa fa-plus"></i></button>
                      </div>
                    </div>


                    <div class="box-body" >

                      <div class="margin" id="botones_control">
                        <a href="javascript:void(0);" class="btn btn btn-primary" onclick="cargar_formulario(8);">Nueva zona</a>
                      </div>

                      <div id="tabla-responsive" class="table-responsive" >

                        <table id="tblzonas"  class="mdl-data-table" style="width:100%">
                          <thead>
                            <tr>
                              <th>Código zona</th>
                              <th>Descripción</th>
                              <th>Acción</th>
                            </tr>
                          </thead>
                        </table>

                      </div>

                    </div>
                    <!-- /.box-body -->

                  </div>
                  <script>


                  $(document).ready(function() {
                    activar_tabla_empresas();
                    function activar_tabla_empresas() {
                      $('#tblzonas').DataTable({
                        processing: true,
                        serverSide: true,
                        pageLength: 10,
                        language: {
                          "url": '{!! asset('/plugins/datatables/latino.json') !!}'
                        } ,
                        ajax: '{{ route('datatable.zonas') }}',
                        columns: [
                          { data: 'IdZona', name: 'IdZona' },
                          { data: 'Descripcion', name: 'Descripcion' },
                          { data: null,  render: function ( data, type, row )
                            {
                              return "<button type='button' title='Editar' onclick='verinfo_zona("+ data.id +")' class='btn  btn-warning btn-xs' ><i class='fa fa-edit'></i></button>"+"&nbsp;"+"<button type='button' title='Borrar' onclick='borrado_zona("+ data.id +")' class='btn  btn-danger btn-xs' ><i class='fa fa-trash-o'></i></button>"
                            }
                          }

                        ]

                      });

                    }
                  });

                  </script>
                  <!-- Fin caja zonas-->

                  <!-- Inicio caja tipo de discapacidad-->
                  <div class="box box-solid box-primary collapsed-box" >
                    <div class="box-header with-border" >
                      <h3 class="box-title">Tipo de discapacidad</h3>
                      <div class="box-tools pull-right" >
                        <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip">
                          <i class="fa fa-plus"></i></button>
                        </div>
                      </div>


                      <div class="box-body" >

                        <div class="margin" id="botones_control">
                          <a href="javascript:void(0);" class="btn btn btn-primary" onclick="cargar_formulario(9);">Nuevo tipo de discapacidad</a>
                        </div>

                        <div id="tabla-responsive" class="table-responsive" >

                          <table id="tbltipodis"  class="mdl-data-table" style="width:100%">
                            <thead>
                              <tr>
                                <th>Código tipo discapacidad</th>
                                <th>Descripción</th>
                                <th>Acción</th>
                              </tr>
                            </thead>
                          </table>

                        </div>

                      </div>
                      <!-- /.box-body -->

                    </div>
                    <script>


                    $(document).ready(function() {
                      activar_tabla_empresas();
                      function activar_tabla_empresas() {
                        $('#tbltipodis').DataTable({
                          processing: true,
                          serverSide: true,
                          pageLength: 10,
                          language: {
                            "url": '{!! asset('/plugins/datatables/latino.json') !!}'
                          } ,
                          ajax: '{{ route('datatable.tipodis') }}',
                          columns: [
                            { data: 'IdTipoDiscapacidad', name: 'IdTipoDiscapacidad' },
                            { data: 'Descripcion', name: 'Descripcion' },
                            { data: null,  render: function ( data, type, row )
                              {
                                return "<button type='button' title='Editar' onclick='verinfo_tipodiscapacidad("+ data.id +")' class='btn  btn-warning btn-xs' ><i class='fa fa-edit'></i></button>"+"&nbsp;"+"<button type='button' title='Borrar' onclick='borrado_tipodiscapacidad("+ data.id +")' class='btn  btn-danger btn-xs' ><i class='fa fa-trash-o'></i></button>"
                              }
                            }

                          ]

                        });

                      }
                    });

                    </script>
                    <!-- Fin caja tipo discapacidad-->

                    <!-- Inicio caja metodología-->
                    <div class="box box-solid box-primary collapsed-box" >
                      <div class="box-header with-border" >
                        <h3 class="box-title">Metodología</h3>
                        <div class="box-tools pull-right" >
                          <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip">
                            <i class="fa fa-plus"></i></button>
                          </div>
                        </div>


                        <div class="box-body" >

                          <div class="margin" id="botones_control">
                            <a href="javascript:void(0);" class="btn btn btn-primary" onclick="cargar_formulario(10);">Nueva metodología</a>
                          </div>

                          <div id="tabla-responsive" class="table-responsive" >

                            <table id="tblmetodologia"  class="mdl-data-table" style="width:100%">
                              <thead>
                                <tr>
                                  <th>Código metodología</th>
                                  <th>Descripción</th>
                                  <th>Acción</th>
                                </tr>
                              </thead>
                            </table>

                          </div>

                        </div>
                        <!-- /.box-body -->

                      </div>
                      <script>


                      $(document).ready(function() {
                        activar_tabla_empresas();
                        function activar_tabla_empresas() {
                          $('#tblmetodologia').DataTable({
                            processing: true,
                            serverSide: true,
                            pageLength: 10,
                            language: {
                              "url": '{!! asset('/plugins/datatables/latino.json') !!}'
                            } ,
                            ajax: '{{ route('datatable.metodologia') }}',
                            columns: [
                              { data: 'IdMetodologia', name: 'IdMetodologia' },
                              { data: 'Descripcion', name: 'Descripcion' },
                              { data: null,  render: function ( data, type, row )
                                {
                                  return "<button type='button' title='Editar' onclick='verinfo_metodologia("+ data.id +")' class='btn  btn-warning btn-xs' ><i class='fa fa-edit'></i></button>"+"&nbsp;"+"<button type='button' title='Borrar' onclick='borrado_metodologia("+ data.id +")' class='btn  btn-danger btn-xs' ><i class='fa fa-trash-o'></i></button>"
                                }
                              }

                            ]

                          });

                        }
                      });

                      </script>
                      <!-- Fin caja metodologia-->

                      <!-- Inicio caja estrato-->
                      <div class="box box-solid box-primary collapsed-box" >
                        <div class="box-header with-border" >
                          <h3 class="box-title">Estrato</h3>
                          <div class="box-tools pull-right" >
                            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip">
                              <i class="fa fa-plus"></i></button>
                            </div>
                          </div>


                          <div class="box-body" >

                            <div class="margin" id="botones_control">
                              <a href="javascript:void(0);" class="btn btn btn-primary" onclick="cargar_formulario(11);">Nuevo estrato</a>
                            </div>

                            <div id="tabla-responsive" class="table-responsive" >

                              <table id="tblestrato"  class="mdl-data-table" style="width:100%">
                                <thead>
                                  <tr>
                                    <th>Código estrato</th>
                                    <th>Descripción</th>
                                    <th>Acción</th>
                                  </tr>
                                </thead>
                              </table>

                            </div>

                          </div>
                          <!-- /.box-body -->

                        </div>
                        <script>


                        $(document).ready(function() {
                          activar_tabla_empresas();
                          function activar_tabla_empresas() {
                            $('#tblestrato').DataTable({
                              processing: true,
                              serverSide: true,
                              pageLength: 10,
                              language: {
                                "url": '{!! asset('/plugins/datatables/latino.json') !!}'
                              } ,
                              ajax: '{{ route('datatable.estrato') }}',
                              columns: [
                                { data: 'IdEstratos', name: 'IdEstratos' },
                                { data: 'Descripcion', name: 'Descripcion' },
                                { data: null,  render: function ( data, type, row )
                                  {
                                    return "<button type='button' title='Editar' onclick='verinfo_estrato("+ data.id +")' class='btn  btn-warning btn-xs' ><i class='fa fa-edit'></i></button>"+"&nbsp;"+"<button type='button' title='Borrar' onclick='borrado_estrato("+ data.id +")' class='btn  btn-danger btn-xs' ><i class='fa fa-trash-o'></i></button>"
                                  }
                                }

                              ]

                            });

                          }
                        });

                        </script>
                        <!-- Fin caja estrato-->

                        <!-- Inicio caja Etnia -->
                        <div class="box box-solid box-primary collapsed-box" >
                          <div class="box-header with-border" >
                            <h3 class="box-title">Etnia</h3>
                            <div class="box-tools pull-right" >
                              <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip">
                                <i class="fa fa-plus"></i></button>
                              </div>
                            </div>


                            <div class="box-body" >

                              <div class="margin" id="botones_control">
                                <a href="javascript:void(0);" class="btn btn btn-primary" onclick="cargar_formulario(16);">Nueva etnia</a>
                              </div>

                              <div id="tabla-responsive" class="table-responsive" >

                                <table id="tbletnias"  class="mdl-data-table" style="width:100%">
                                  <thead>
                                    <tr>
                                      <th>Código etnia</th>
                                      <th>Descripción</th>
                                      <th>Acción</th>
                                    </tr>
                                  </thead>
                                </table>

                              </div>

                            </div>
                            <!-- /.box-body -->

                          </div>
                          <script>


                          $(document).ready(function() {
                            activar_tabla_empresas();
                            function activar_tabla_empresas() {
                              $('#tbletnias').DataTable({
                                processing: true,
                                serverSide: true,
                                pageLength: 10,
                                language: {
                                  "url": '{!! asset('/plugins/datatables/latino.json') !!}'
                                } ,
                                ajax: '{{ route('datatable.etnias') }}',
                                columns: [
                                  { data: 'IdEtnias', name: 'IdEtnias' },
                                  { data: 'Descripcion', name: 'Descripcion' },
                                  { data: null,  render: function ( data, type, row )
                                    {
                                      return "<button type='button' title='Editar' onclick='verinfo_etnia("+ data.id +")' class='btn  btn-warning btn-xs' ><i class='fa fa-edit'></i></button>"+"&nbsp;"+"<button type='button' title='Borrar' onclick='borrado_etnia("+ data.id +")' class='btn  btn-danger btn-xs' ><i class='fa fa-trash-o'></i></button>"
                                    }
                                  }

                                ]

                              });

                            }
                          });

                          </script>
                          <!-- Fin caja Etnia -->

                        <!-- Inicio caja resguardo-->
                        <div class="box box-solid box-primary collapsed-box" >
                          <div class="box-header with-border" >
                            <h3 class="box-title">Resguardo</h3>
                            <div class="box-tools pull-right" >
                              <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip">
                                <i class="fa fa-plus"></i></button>
                              </div>
                            </div>


                            <div class="box-body" >

                              <div class="margin" id="botones_control">
                                <a href="javascript:void(0);" class="btn btn btn-primary" onclick="cargar_formulario(12);">Nuevo resguardo</a>
                              </div>

                              <div id="tabla-responsive" class="table-responsive" >

                                <table id="tblresguardos"  class="mdl-data-table" style="width:100%">
                                  <thead>
                                    <tr>
                                      <th>Código resguardo</th>
                                      <th>Descripción</th>
                                      <th>Acción</th>
                                    </tr>
                                  </thead>
                                </table>

                              </div>

                            </div>
                            <!-- /.box-body -->

                          </div>
                          <script>


                          $(document).ready(function() {
                            activar_tabla_empresas();
                            function activar_tabla_empresas() {
                              $('#tblresguardos').DataTable({
                                processing: true,
                                serverSide: true,
                                pageLength: 10,
                                language: {
                                  "url": '{!! asset('/plugins/datatables/latino.json') !!}'
                                } ,
                                ajax: '{{ route('datatable.resguardos') }}',
                                columns: [
                                  { data: 'IdResguardos', name: 'IdResguardos' },
                                  { data: 'Nombre', name: 'Nombre' },
                                  { data: null,  render: function ( data, type, row )
                                    {
                                      return "<button type='button' title='Editar' onclick='verinfo_resguardo("+ data.id +")' class='btn  btn-warning btn-xs' ><i class='fa fa-edit'></i></button>"+"&nbsp;"+"<button type='button' title='Borrar' onclick='borrado_resguardo("+ data.id +")' class='btn  btn-danger btn-xs' ><i class='fa fa-trash-o'></i></button>"
                                    }
                                  }

                                ]

                              });

                            }
                          });

                          </script>
                          <!-- Fin caja resguardo-->

                          <!-- Inicio caja jornada -->
                          <div class="box box-solid box-primary collapsed-box" >
                            <div class="box-header with-border" >
                              <h3 class="box-title">Jornada</h3>
                              <div class="box-tools pull-right" >
                                <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip">
                                  <i class="fa fa-plus"></i></button>
                                </div>
                              </div>


                              <div class="box-body" >

                                <div class="margin" id="botones_control">
                                  <a href="javascript:void(0);" class="btn btn btn-primary" onclick="cargar_formulario(13);">Nueva jornada</a>
                                </div>

                                <div id="tabla-responsive" class="table-responsive" >

                                  <table id="tbljornada"  class="mdl-data-table" style="width:100%">
                                    <thead>
                                      <tr>
                                        <th>Código jornada</th>
                                        <th>Descripción</th>
                                        <th>Acción</th>
                                      </tr>
                                    </thead>
                                  </table>

                                </div>

                              </div>
                              <!-- /.box-body -->

                            </div>
                            <script>


                            $(document).ready(function() {
                              activar_tabla_empresas();
                              function activar_tabla_empresas() {
                                $('#tbljornada').DataTable({
                                  processing: true,
                                  serverSide: true,
                                  pageLength: 10,
                                  language: {
                                    "url": '{!! asset('/plugins/datatables/latino.json') !!}'
                                  } ,
                                  ajax: '{{ route('datatable.jornada') }}',
                                  columns: [
                                    { data: 'IdJornada', name: 'IdJornada' },
                                    { data: 'Descripcion', name: 'Descripcion' },
                                    { data: null,  render: function ( data, type, row )
                                      {
                                        return "<button type='button' title='Editar' onclick='verinfo_jornada("+ data.id +")' class='btn  btn-warning btn-xs' ><i class='fa fa-edit'></i></button>"+"&nbsp;"+"<button type='button' title='Borrar' onclick='borrado_jornada("+ data.id +")' class='btn  btn-danger btn-xs' ><i class='fa fa-trash-o'></i></button>"
                                      }
                                    }

                                  ]

                                });

                              }
                            });

                            </script>
                            <!-- Fin caja jornada -->

                            <!-- Caja para estructura  victimaconflicto --->
                            <div class="box box-solid box-primary collapsed-box" >
                              <div class="box-header with-border" >
                                <h3 class="box-title">Víctima conflicto</h3>
                                <div class="box-tools pull-right" >
                                  <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip">
                                    <i class="fa fa-plus"></i></button>
                                  </div>
                                </div>


                                <div class="box-body" >

                                  <div class="margin" id="botones_control">
                                    <a href="javascript:void(0);" class="btn btn btn-primary" onclick="cargar_formulario(14);">Nueva víctima conflicto</a>
                                  </div>

                                  <div id="tabla-responsive" class="table-responsive" >

                                    <table id="tblvictcon"  class="mdl-data-table" style="width:100%">
                                      <thead>
                                        <tr>
                                          <th>Código víctima conflicto</th>
                                          <th>Descripción</th>
                                          <th>Acción</th>
                                        </tr>
                                      </thead>
                                    </table>

                                  </div>

                                </div>
                                <!-- /.box-body -->

                              </div>
                              <script>


                              $(document).ready(function() {
                                activar_tabla_empresas();
                                function activar_tabla_empresas() {
                                  $('#tblvictcon').DataTable({
                                    processing: true,
                                    serverSide: true,
                                    pageLength: 10,
                                    language: {
                                      "url": '{!! asset('/plugins/datatables/latino.json') !!}'
                                    } ,
                                    ajax: '{{ route('datatable.victcon') }}',
                                    columns: [
                                      { data: 'IdVictimaConflicto', name: 'IdVictimaConflicto' },
                                      { data: 'Descripcion', name: 'Descripcion' },
                                      { data: null,  render: function ( data, type, row )
                                        {
                                          return "<button type='button' title='Editar' onclick='verinfo_victimaconflicto("+ data.id +")' class='btn  btn-warning btn-xs' ><i class='fa fa-edit'></i></button>"+"&nbsp;"+"<button type='button' title='Borrar' onclick='borrado_victimaconflicto("+ data.id +")' class='btn  btn-danger btn-xs' ><i class='fa fa-trash-o'></i></button>"
                                        }
                                      }

                                    ]

                                  });

                                }
                              });

                              </script>
                              <!-- Fin caja victimaconflicto-->

                              <!-- Inicio caja capacidad excepcional -->
                              <div class="box box-solid box-primary collapsed-box" >
                                <div class="box-header with-border" >
                                  <h3 class="box-title">Capacidad excepcional</h3>
                                  <div class="box-tools pull-right" >
                                    <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip">
                                      <i class="fa fa-plus"></i></button>
                                    </div>
                                  </div>


                                  <div class="box-body" >

                                    <div class="margin" id="botones_control">
                                      <a href="javascript:void(0);" class="btn btn btn-primary" onclick="cargar_formulario(15);">Nueva capacidad</a>
                                    </div>

                                    <div id="tabla-responsive" class="table-responsive" >

                                      <table id="tblcapacidadexcepcionals"  class="mdl-data-table" style="width:100%">
                                        <thead>
                                          <tr>
                                            <th>Código capacidad excepcional</th>
                                            <th>Descripción</th>
                                            <th>Acción</th>
                                          </tr>
                                        </thead>
                                      </table>

                                    </div>

                                  </div>
                                  <!-- /.box-body -->

                                </div>
                                <script>


                                $(document).ready(function() {
                                  activar_tabla_empresas();
                                  function activar_tabla_empresas() {
                                    $('#tblcapacidadexcepcionals').DataTable({
                                      processing: true,
                                      serverSide: true,
                                      pageLength: 10,
                                      language: {
                                        "url": '{!! asset('/plugins/datatables/latino.json') !!}'
                                      } ,
                                      ajax: '{{ route('datatable.capacidadexcepcionals') }}',
                                      columns: [
                                        { data: 'IdCapacidadExcepcional', name: 'IdCapacidadExcepcional' },
                                        { data: 'Descripcion', name: 'Descripcion' },
                                        { data: null,  render: function ( data, type, row )
                                          {
                                            return "<button type='button' title='Editar' onclick='verinfo_capacidadexcepcional("+ data.id +")' class='btn  btn-warning btn-xs' ><i class='fa fa-edit'></i></button>"+"&nbsp;"+"<button type='button' title='Borrar' onclick='borrado_capacidadexcepcional("+ data.id +")' class='btn  btn-danger btn-xs' ><i class='fa fa-trash-o'></i></button>"
                                          }
                                        }

                                      ]

                                    });

                                  }
                                });

                                </script>
                                <!-- Fin caja Capacidad excepcional -->

                                  <!-- Inicio caja nivel -->
                                  <div class="box box-solid box-primary collapsed-box" >
                                    <div class="box-header with-border" >
                                      <h3 class="box-title">Nivel</h3>
                                      <div class="box-tools pull-right" >
                                        <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip">
                                          <i class="fa fa-plus"></i></button>
                                        </div>
                                      </div>


                                      <div class="box-body" >

                                        <div class="margin" id="botones_control">
                                          <a href="javascript:void(0);" class="btn btn btn-primary" onclick="cargar_formulario(17);">Nuevo nivel</a>
                                        </div>

                                        <div id="tabla-responsive" class="table-responsive" >

                                          <table id="tblnivel"  class="mdl-data-table" style="width:100%">
                                            <thead>
                                              <tr>
                                                <th>Código nivel</th>
                                                <th>Descripción</th>
                                                <th>Acción</th>
                                              </tr>
                                            </thead>
                                          </table>

                                        </div>

                                      </div>
                                      <!-- /.box-body -->

                                    </div>
                                    <script>


                                    $(document).ready(function() {
                                      activar_tabla_empresas();
                                      function activar_tabla_empresas() {
                                        $('#tblnivel').DataTable({
                                          processing: true,
                                          serverSide: true,
                                          pageLength: 10,
                                          language: {
                                            "url": '{!! asset('/plugins/datatables/latino.json') !!}'
                                          } ,
                                          ajax: '{{ route('datatable.nivel') }}',
                                          columns: [
                                            { data: 'IdNivel', name: 'IdNivel' },
                                            { data: 'Descripcion', name: 'Descripcion' },
                                            { data: null,  render: function ( data, type, row )
                                              {
                                                return "<button type='button' title='Editar' onclick='verinfo_nivel("+ data.id +")' class='btn  btn-warning btn-xs' ><i class='fa fa-edit'></i></button>"+"&nbsp;"+"<button type='button' title='Borrar' onclick='borrado_nivel("+ data.id +")' class='btn  btn-danger btn-xs' ><i class='fa fa-trash-o'></i></button>"
                                              }
                                            }

                                          ]

                                        });

                                      }
                                    });

                                    </script>
                                    <!-- /.Fin caja Nivel-->

                                    <!-- Inicio caja nivel total -->
                                    <div class="box box-solid box-primary collapsed-box" >
                                      <div class="box-header with-border" >
                                        <h3 class="box-title">Nivel media total</h3>
                                        <div class="box-tools pull-right" >
                                          <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip">
                                            <i class="fa fa-plus"></i></button>
                                          </div>
                                        </div>


                                        <div class="box-body" >

                                          <div class="margin" id="botones_control">
                                            <a href="javascript:void(0);" class="btn btn btn-primary" onclick="cargar_formulario(18);">Nuevo nivel media total</a>
                                          </div>

                                          <div id="tabla-responsive" class="table-responsive" >

                                            <table id="tblniveltotal"  class="mdl-data-table" style="width:100%">
                                              <thead>
                                                <tr>
                                                  <th>Código nivel total</th>
                                                  <th>Descripción</th>
                                                  <th>Límite edad</th>
                                                  <th>Acción</th>
                                                </tr>
                                              </thead>
                                            </table>

                                          </div>

                                        </div>
                                        <!-- /.box-body -->

                                      </div>
                                      <script>


                                      $(document).ready(function() {
                                        activar_tabla_empresas();
                                        function activar_tabla_empresas() {
                                          $('#tblniveltotal').DataTable({
                                            processing: true,
                                            serverSide: true,
                                            pageLength: 10,
                                            language: {
                                              "url": '{!! asset('/plugins/datatables/latino.json') !!}'
                                            } ,
                                            ajax: '{{ route('datatable.nivelmedia') }}',
                                            columns: [
                                              { data: 'IdNivelMediaTotal', name: 'IdNivelMediaTotal' },
                                              { data: 'Descripcion', name: 'Descripcion' },
                                              { data: 'LimiteEdad', name: 'LimiteEdad' },
                                              { data: null,  render: function ( data, type, row )
                                                {
                                                  return "<button type='button' title='Editar' onclick='verinfo_nivelmediatotal("+ data.id +")' class='btn  btn-warning btn-xs' ><i class='fa fa-edit'></i></button>"+"&nbsp;"+"<button type='button' title='Borrar' onclick='borrado_nivelmediatotal("+ data.id +")' class='btn  btn-danger btn-xs' ><i class='fa fa-trash-o'></i></button>"
                                                }
                                              }

                                            ]

                                          });

                                        }
                                      });

                                      </script>
                                      <!-- Fin caja Nivel media total -->

                                      <!-- Inicio caja nivel cine -->
                                      <div class="box box-solid box-primary collapsed-box" >
                                        <div class="box-header with-border" >
                                          <h3 class="box-title">Nivel cine</h3>
                                          <div class="box-tools pull-right" >
                                            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip">
                                              <i class="fa fa-plus"></i></button>
                                            </div>
                                          </div>


                                          <div class="box-body" >

                                            <div class="margin" id="botones_control">
                                              <a href="javascript:void(0);" class="btn btn btn-primary" onclick="cargar_formulario(19);">Nuevo nivel media total</a>
                                            </div>

                                            <div id="tabla-responsive" class="table-responsive" >

                                              <table id="tblnivelcine"  class="mdl-data-table" style="width:100%">
                                                <thead>
                                                  <tr>
                                                    <th>Código nivel cine</th>
                                                    <th>Descripción</th>
                                                    <th>Acción</th>
                                                  </tr>
                                                </thead>
                                              </table>

                                            </div>

                                          </div>
                                          <!-- /.box-body -->

                                        </div>
                                        <script>


                                        $(document).ready(function() {
                                          activar_tabla_empresas();
                                          function activar_tabla_empresas() {
                                            $('#tblnivelcine').DataTable({
                                              processing: true,
                                              serverSide: true,
                                              pageLength: 10,
                                              language: {
                                                "url": '{!! asset('/plugins/datatables/latino.json') !!}'
                                              } ,
                                              ajax: '{{ route('datatable.nivelcine') }}',
                                              columns: [
                                                { data: 'IdNivelCine', name: 'IdNivelCine' },
                                                { data: 'Descripcion', name: 'Descripcion' },
                                                { data: null,  render: function ( data, type, row )
                                                  {
                                                    return "<button type='button' title='Editar' onclick='verinfo_nivelcine("+ data.id +")' class='btn  btn-warning btn-xs' ><i class='fa fa-edit'></i></button>"+"&nbsp;"+"<button type='button' title='Borrar' onclick='borrado_nivelcine("+ data.id +")' class='btn  btn-danger btn-xs' ><i class='fa fa-trash-o'></i></button>"
                                                  }
                                                }

                                              ]

                                            });

                                          }
                                        });

                                        </script>
                                        <!-- Fin caja nivel cine -->

                                        <!-- Inicio caja grado -->
                                        <div class="box box-solid box-primary collapsed-box" >
                                          <div class="box-header with-border" >
                                            <h3 class="box-title">Grado</h3>
                                            <div class="box-tools pull-right" >
                                              <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip">
                                                <i class="fa fa-plus"></i></button>
                                              </div>
                                            </div>


                                            <div class="box-body" >

                                              <div class="margin" id="botones_control">
                                                <a href="javascript:void(0);" class="btn btn btn-primary" onclick="cargar_formulario(19);">Nuevo grado</a>
                                              </div>

                                              <div id="tabla-responsive" class="table-responsive" >

                                                <table id="tblgrado"  class="mdl-data-table" style="width:100%">
                                                  <thead>
                                                    <tr>
                                                      <th>Código grado</th>
                                                      <th>Grado</th>
                                                      <th>Edad límite</th>
                                                      <th>Código nivel</th>
                                                      <th>Código nivel media total</th>
                                                      <th>Código nivel cine</th>
                                                      <th>Acción</th>
                                                    </tr>
                                                  </thead>
                                                </table>

                                              </div>

                                            </div>
                                            <!-- /.box-body -->

                                          </div>
                                          <script>


                                          $(document).ready(function() {
                                            activar_tabla_empresas();
                                            function activar_tabla_empresas() {
                                              $('#tblgrado').DataTable({
                                                processing: true,
                                                serverSide: true,
                                                pageLength: 10,
                                                language: {
                                                  "url": '{!! asset('/plugins/datatables/latino.json') !!}'
                                                } ,
                                                ajax: '{{ route('datatable.grado') }}',
                                                columns: [
                                                  { data: 'IdGrado', name: 'IdGrado' },
                                                  { data: 'Grado', name: 'Grado' },
                                                  { data: 'LimiteEdad', name: 'LimiteEdad' },
                                                  { data: 'IdNivel', name: 'IdNivel' },
                                                  { data: 'IdNivelMediaTotal', name: 'IdNivelMediaTotal' },
                                                  { data: 'IdNivelCine', name: 'IdNivelCine' },
                                                  { data: null,  render: function ( data, type, row )
                                                    {
                                                      return "<button type='button' title='Editar' onclick='verinfo_grado("+ data.id +")' class='btn  btn-warning btn-xs' ><i class='fa fa-edit'></i></button>"+"&nbsp;"+"<button type='button' title='Borrar' onclick='borrado_grado("+ data.id +")' class='btn  btn-danger btn-xs' ><i class='fa fa-trash-o'></i></button>"
                                                    }
                                                  }

                                                ]

                                              });

                                            }
                                          });

                                          </script>
                                          <!-- Fin caja grado -->

                                        </section>

                                      @endsection
