@if(Auth::user()->isRole('administrador_sistema') || Auth::user()->isRole('administrador_matricula'))
@extends('layouts.app')

@section('htmlheader_title')
  Carga masiva de datos matrícula
@endsection


@section('main-content')


  <section  id="contenido_principal">
    <div class="box box-primary">
      <div class="box-header">
        <h3 class="text-center"><b>Carga masiva de anexos matrícula SIMAT</b></h3>
        <br>
        <div>
          <p>Aquí podrá ingresar información de matrícula SIMAT. Por favor tenga en cuenta las siguientes recomendaciones:</p>
          <ol>
            <li><p>Descargue la plantilla para verificar las columnas que debe incluir en el anexo.</p></li>
            <li><p>El orden y el nombre de las columnas en el anexo debe permanecer igual.</p></li>
            <li><p>Recuerde que antes de cargar el archivo debe convertirlo a UTF-8.</p></li>
            <!-- /El botón llama el id modal-utf8 para cargar video conversión de archivos a utf8 desde app.blade -->
            <button class="btn btn-info fa fa-video-camera" style="font-size:15px;" data-target="#modal-utf8-matricula" data-toggle="modal" type="button"> Conversión archivos UTF-8</button>
            <!-- /Fin botón utf8 -->
            <li><p>Seleccione el mes de corte y adjunte los archivos descargados mensualmente de SIMAT y presione el botón "Cargar archivos".</p></li>
          </ol>
        </div>
      </div><!-- /.box-header -->

      <div id="notificacion_resul_fci"></div>

      <div id="formulario_anexos">
        <form  id="f_subir_anexo" name="f_subir_anexo" method="post"  files=”true” action="{{ url('subir_anexo') }}" class="formarchivo" enctype="multipart/form-data" >
          {{ csrf_field() }}

          <div class="box-body">

            <div class="form-group col-xs-6"  style="width:380px">

              <label for="mes">Mes:</label>

              <select id="mes" name="mes" class="form-control" value="" required>
                <option selected="selected"></option>
                <option value="1">Enero</option>
                <option value="2">Febrero</option>
                <option value="3">Marzo</option>
                <option value="4">Abril</option>
                <option value="5">Mayo</option>
                <option value="6">Junio</option>
                <option value="7">Julio</option>
                <option value="8">Agosto</option>
                <option value="9">Septiembre</option>
                <option value="10">Octubre</option>
                <option value="11">Noviembre</option>
                <option value="12">Diciembre</option>

              </select>

            </div>
          </div>

          <div class="box-body">

            <div class="panel panel-primary">
              <div class="panel-heading">Sector no oficial</div>
              <div class="panel-body">
                <div class="form-group col-xs-6"  >
                  <label>Agregar anexo <span class="label label-primary">5 AA</span></label>
                  <input name="anexo1" id="archivo_anexo" type="file" files=”true”  enctype=”multipart/form-data”  class="archivo form-control"  required/><br /><br />
                </div>

                <div class="form-group col-xs-6"  >
                  <label>Agregar anexo <span class="label label-primary">5 AB</span></label>
                  <input name="anexo2" id="archivo_anexo" type="file" files=”true”  enctype=”multipart/form-data”  class="archivo form-control"  required/><br /><br />
                </div>

                <div class="form-group col-xs-6"  >
                  <label>Agregar anexo <span class="label label-primary">5 OTRO</span></label>
                  <input name="anexo3" id="archivo_anexo" type="file" files=”true”  enctype=”multipart/form-data”  class="archivo form-control"  required/><br /><br />
                </div>
              </div>
            </div>

            <div class="panel panel-primary">
              <div class="panel-heading">Sector oficial</div>
              <div class="panel-body">
                <div class="form-group col-xs-6"  >
                  <label>Agregar anexo <span class="label label-primary">6 AA</span></label>
                  <input name="anexo4" id="archivo_anexo" type="file" files=”true”  enctype=”multipart/form-data”  class="archivo form-control"  required/><br /><br />
                </div>

                <div class="form-group col-xs-6"  >
                  <label>Agregar anexo <span class="label label-primary">6 AB</span></label>
                  <input name="anexo5" id="archivo_anexo" type="file" files=”true”  enctype=”multipart/form-data”  class="archivo form-control"  required/><br /><br />
                </div>

                <div class="form-group col-xs-6"  >
                  <label>Agregar anexo <span class="label label-primary">6 OTRO</span></label>
                  <input name="anexo6" id="archivo_anexo" type="file" files=”true”  enctype=”multipart/form-data”  class="archivo form-control"  required/><br /><br />
                </div>
              </div>
            </div>
          </div>

          <div class="box-footer">


            <button type="submit" id="btnsubmit"  class="btn btn-primary fa fa-cloud-upload"> Cargar archivos</button>
            <a onclick="return confirm('Recuerde que cuando vaya a cargar el archivo debe convertirlo a UTF-8.')" href="{{ url('/descargar_plantilla_matricula') }}" target="_blank" id="descargar_plantilla" name="descargar_plantilla"  class="btn btn-info fa fa-cloud-download" style="font-size:15px;"> Descargar plantilla</a>
          </div>



        </div>

      </form>

    </div>

    <div id="anexos" class="box box-primary">

      <h3 class="text-center">Anexos cargados</h3>
      <div class="box-body"  >

        <div class="table-responsive" >

          <table  class="table table-hover table-striped" cellspacing="0" width="100%">

            <thead>
              <tr>
                <th>Año de reporte</th>
                <th>Mes de reporte</th>
                <th data-type="date">Fecha</th>
                <th>5 AA</th>
                <th>5 AB</th>
                <th>5 OTRO</th>
                <th>6 AA</th>
                <th>6 AB</th>
                <th>6 OTRO</th>
                <th>Total</th>
                <th>Acción</th>
              </tr>
            </thead>
            <tbody>

              @foreach($listados as $listado)
                <tr role="row" class="odd">
                  <td>{{ $listado->ano_inf }}</td>
                  <td>{{ ($listado->mes_corte) ==1 ? "Enero" : "" }}
                    {{ ($listado->mes_corte) ==2 ? "Febrero" : "" }}
                    {{ ($listado->mes_corte) ==3 ? "Marzo " : "" }}
                    {{ ($listado->mes_corte) ==4 ? "Abril" : "" }}
                    {{ ($listado->mes_corte) ==5 ? "Mayo" : "" }}
                    {{ ($listado->mes_corte) ==6 ? "Junio" : "" }}
                    {{ ($listado->mes_corte) ==7 ? "Julio" : "" }}
                    {{ ($listado->mes_corte) ==8 ? "Agosto" : "" }}
                    {{ ($listado->mes_corte) ==9 ? "Septiembre" : "" }}
                    {{ ($listado->mes_corte) ==10 ? "Octubre" : "" }}
                    {{ ($listado->mes_corte) ==11 ? "Noviembre" : "" }}
                    {{ ($listado->mes_corte) ==12 ? "Diciembre" : "" }}
                  </td>
                  <td >{{ Carbon\Carbon::parse($listado->fecha)->format('d-m-Y h:i A') }}</td>

                  <td >{{ $listado->cantidad5aa }}</td>
                  <td >{{ $listado->cantidad5ab }}</td>
                  <td >{{ $listado->cantidad5otro }}</td>
                  <td >{{ $listado->cantidad6aa }}</td>
                  <td >{{ $listado->cantidad6ab }}</td>
                  <td >{{ $listado->cantidad6otro }}</td>
                  <td >{{ $listado->cantidadtotal }}</td>
                  <td >
                    <button type="button" title="Borrar" class="btn  btn-danger btn-xs"  onclick="borrar_anexo({{  $listado->ano_inf }},{{ $listado->mes_corte }});"  ><i class="fa fa-trash"></i></button>
                  </td>
                </tr>
              @endforeach
            </tbody>
          </table>

          {{ $listados->links() }}
        </div>
      </div>
    </div>



  </section>
@endsection
@else
  @extends('errors.acceso')
@endif
