@extends('layouts.app')

@section('htmlheader_title')
Consulta Basica
@endsection


@section('main-content')
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<section  id="contenido_principal">
  <div class="box box-primary">
                    <div class="box-header">
                      <center>
                      <h3 style="margin-top: 10px;" class="box-title"><strong>Consulta básica SIMAT</strong></h3>
                     </center>
                    </div><!-- /.box-header -->

    <div id="notificacion_resul_fci"></div>
    <div class="box box-primary" style="border: 1px solid #3c8dbc !important;" >
      <div class="titleboxes" style="font-size:15px;" align="center">
      Filtro principal
      </div>
      <div class="row" style="padding:10px;">
      <div class="col-md-4">
        <!-- inicio de Años -->
        <label for="">Año</label>
        <div class="dropdown" >
          <button  class="btn btn-default dropdown-toggle btn-block" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
            Seleccionar
            <span class="caret"></span>
          </button>
          <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
            @foreach($anios as $an)
            <li><a href="#"><label class="checkbox-formulario">
              <input type="checkbox" name="ano" value="{{ $an->anio}}" onclick="anios(this);_cambiargrafica();_cambiarzonasgrafica();"><span class="label-text">{{ $an->anio}}</span>
            </label></a></li>
            @endforeach
          </ul>
            <script type="text/javascript">
            $('.dropdown-menu input, .dropdown-menu label').click(function(e) {
              e.stopPropagation();
            });
            </script>
        </div>
        <!-- Fin de año -->
        <!-- Incio de SUBREGION -->
        <div class="form-group">
        <label>Subregión</label>
        <select name="CodigoSubregion" id="CodigoSubregion" class="form-control dynamic" data-dependent="NombreMunicipio" onclick="_cambiargrafica();_cambiarzonasgrafica();">
         <option value="0">Todos</option>
         @foreach($subregions as $sub)
         <option value="{{ $sub->id}}">{{ $sub->NombreSubregion }}</option>
         @endforeach
        </select>
       </div>
       <!-- Fin de SUBREGION -->
       <!-- inio de MUNICIPIO -->
       <div class="form-group">
       <label>Municipio</label>
       <select class="form-control dynamicmuni" id="NombreMunicipio"  name="NombreMunicipio" data-dependent="nombre_establecimiento" onclick="_cambiargrafica();_cambiarzonasgrafica();">
         <option value="0">Todos</option>
       </select>
       </div>
       <!-- Fin de MUNICIPIO -->
      </div>
      <div class="col-md-4">
        <div class="titleboxes">
          <h5 align="center">Establecimientos</h5>
        </div>
        <div class="box box-primary scrollable-menu" style="border: 1px solid #3c8dbc !important; padding-left:4px;padding-right:4px;">
            <div class="form-group" id="nombre_establecimiento"  name="nombre_establecimiento">
            <div ><a href="#"><label >
                <input type="radio"  value=""><span class="label-text">Todos</span>
              </label></a></div>
            </div>
        </div>
      </div>
      <div class="col-md-4">
        <div class="titleboxes" >
          <h5 align="center">Sedes</h5>
        </div>
        <div class="box box-primary" style="border: 1px solid #3c8dbc !important; padding-left:4px;padding-right:4px;">
            <div class="scrollable-menu" id="nombre_sede"  name="nombre_sede">
              <div class="">
                <a href="#"><label >
                    <input type="radio" name="radio" value="" onclick="" ><span class="label-text">Todos</span>
                  </label></a>
              </div>
            </div>
        </div>
      </div>
        {{ csrf_field() }}
      </div>
      <div id="infofiltro" class="alert-message alert-message-success" style="display: none;">
          <h4>Recuerda</h4>
          <p>Para poder iniciar los filtros es necesario seleccionar el año.</p>
      </div>
      <a  href="#" id="clickme" style="background-color:#369;color:#fff;text-decoration:none;padding:4px 10px;display:inline-block">Información</a>
    </div>
  </div>
  <div class="box box-primary">
  </div>
  <div  class="row"  >
    <div class="col-md-2" >
      <div class="titleboxes">
      <h5 align="center">Sectores</h5>
      </div>
      <div class="box box-primary" style="border: 1px solid #3c8dbc !important; padding-left:4px;">
        <div class="box-header">
        </div>
      <div class="form-group " name="forms">
        <div class="form-check">
					<label class="checkbox-formulario">
						<input type="checkbox" name="cbtodossectores" id="cbtodossectores" value=""  onchange="sector(this);_cambiargrafica();" > <span class="label-text" onclick="_cambiargrafica();">Todos</span>
					</label>
				</div>
        <div class="form-check">
					<label class="checkbox-formulario">
						<input type="checkbox" value="OFICIAL" name="cboficial" id="cboficial" class="cbsectores" onchange="sector(this);_cambiargrafica();"> <span class="label-text" >Oficial</span>
					</label>
				</div>
        <div class="form-check">
					<label class="checkbox-formulario">
						<input type="checkbox" value="NO OFICIAL" name="cbnooficial" id="cbnooficial" class="cbsectores" onchange="sector(this);_cambiargrafica();"> <span class="label-text" >No oficial</span>
					</label>
				</div>
      </div>
    </div>
    </div>
    <div class="col-md-2">
      <div class="titleboxes">
      <h5 align="center">Calendario</h5>
      </div>
      <div class="box box-primary" style="border: 1px solid #3c8dbc !important; padding-left:4px;">
        <div class="box-header">
        </div>
      <div class="form-group " name="forms">
        <div class="form-check">
					<label class="checkbox-formulario">
						<input type="checkbox" name="todoscal" id="cbtodoscalendario" value="" onchange="calendario(this);_cambiargrafica();" > <span class="label-text" onclick="_cambiargrafica();">Todos</span>
					</label>
				</div>
        <div class="form-check">
					<label class="checkbox-formulario">
						<input type="checkbox" name="cbcala" id="cbcala" class="cbcalendarios" value="A" onchange="calendario(this);_cambiargrafica();"> <span class="label-text" >A</span>
					</label>
				</div>
        <div class="form-check">
					<label class="checkbox-formulario">
						<input type="checkbox" name="cbcalb" id="cbcalb" class="cbcalendarios" value="B" onchange="calendario(this);_cambiargrafica();"> <span class="label-text" onclick="_cambiargrafica();">B</span>
					</label>
				</div>
        <div class="form-check">
					<label class="checkbox-formulario">
						<input type="checkbox" name="cbcalotro" id="cbcalotro" class="cbcalendarios" value="OTRO" onchange="calendario(this);_cambiargrafica();"> <span class="label-text" onclick="_cambiargrafica();">Otro</span>
					</label>
				</div>
      </div>
    </div>
    </div>
    <div class="col-md-8" >
      <div class="titleboxes">
        <h5 align="center">Matriculas totales por mes</h5>
      </div>
      <div class="box box-primary" style="border: 1px solid #3c8dbc !important; padding-left:4px;">
        <div class="box-header">
        </div>
        <div class="chart-container">
          <canvas id="myChartma" width="200" height="100"></canvas>
        </div>
          <div class="box-footer">
        </div>
    </div>
    </div>
  </div>

  <div class="row">
    <div class="col-md-4" style="padding-left:0px;padding-right:0px;">
      <div class="col-md-5">
        <div class="titleboxes">
        <h5 align="center">Zona</h5>
        </div>
        <div class="box box-primary" style="border: 1px solid #3c8dbc !important; padding-left:4px;">
          <div class="box-header">
          </div>
          <div class="form-group " name="forms">
            <label class="checkbox-formulario">
              <input type="checkbox" name="cbtodoszr" id="cbtodoszr" value="" onchange="zonasresi(this);_cambiarzonasgrafica();"  > <span class="label-text" >Todos</span>
            </label>
            <label class="checkbox-formulario">
              <input type="checkbox" name="cbzonasres" value="1" onchange="zonasresi(this);_cambiarzonasgrafica();"  id="cbruralzr" class="cbzonasres"  > <span class="label-text" >Rural</span>
            </label>
            <label class="checkbox-formulario">
              <input type="checkbox" name="cbzonasres" value="2" onchange="zonasresi(this);_cambiarzonasgrafica();" class="cbzonasres" > <span class="label-text">Urbana</span>
            </label>
          </div>
        </div>
      </div>
      <div class="col-md-7">
        <div class="titleboxes">
          <h5 align="center">Contratación</h5>
        </div>
        <div class="box box-primary" style="border: 1px solid #3c8dbc !important; padding-left:4px;">
          <div class="box-header">
          </div>
          <div class="form-group " name="forms">
            <label class="checkbox-formulario">
              <input type="checkbox" name="cbtodoscontra" id="cbtodoscontra" value="" onchange="contrataciones(this);_cambiarzonasgrafica();"  > <span class="label-text" onclick="">Todos</span>
            </label>
            <label class="checkbox-formulario">
              <input type="checkbox" value="S" name="cbmatcontrata"    value="N" onchange="contrataciones(this);_cambiarzonasgrafica();" class="cbmatcontrata"> <span class="label-text" >Contratada</span>
            </label>
            <label class="checkbox-formulario">
              <input type="checkbox" value="N" name="cbmatcontrata"  value="S"   onchange="contrataciones(this);_cambiarzonasgrafica();" class="cbmatcontrata"> <span class="label-text" >No contratada</span>
            </label>
          </div>
        </div>
      </div>
      <div class="col-md-5">
        <div class="titleboxes">
          <h5 align="center">Grados</h5>
        </div>
        <div class="box box-primary" style="border: 1px solid #3c8dbc !important; padding-left:4px;">
          <div class="box-header">
          </div>
          <div class="form-group " name="forms">
            <div class="dropdown" id="dropmenugrados" >
              <button  class="btn btn-default dropdown-toggle btn-block" type="button" id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">Grados
                <span class="caret"></span>
              </button>
              <ul class="dropdown-menu scrollable-menu" aria-labelledby="dropdownMenu2">
                <li><a href="#"><label class="checkbox-formulario">
                  <input type="checkbox" name="grado" id="cbtodosgrados" value="" onclick="grados(this);_cambiarzonasgrafica();"><span class="label-text">Todos</span>
                </label></a></li>
                @foreach($grados as $gra)
                <li><a href="#"><label class="checkbox-formulario">
                  <input type="checkbox" name="grado" value="{{ $gra->grado}}" onclick="grados(this);_cambiarzonasgrafica();" class="cbgrados"><span class="label-text">{{ $gra->Grado}}</span>
                </label></a></li>
                @endforeach
              </ul>
              <script type="text/javascript">
          $('.dropdown-menu input, .dropdown-menu label').click(function(e) {
              e.stopPropagation();
          });
      </script>
          </div>
        </div>
        </div>
      </div>
      <div class="col-md-7">
        <div class="titleboxes">
          <h5 align="center">Victima de Conflicto</h5>
        </div>
        <div class="box box-primary" style="border: 1px solid #3c8dbc !important; padding-left:4px;">
          <div class="box-header">
          </div>
          <div class="form-group " name="forms">
            <div class="dropdown" >
              <button  class="btn btn-default dropdown-toggle btn-block" type="button" id="dropdownMenu3" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                <span class="caret"></span>
              </button>
              <ul class="dropdown-menu" aria-labelledby="dropdownMenu3">
                <li><a href="#"><label class="checkbox-formulario">
                  <input type="checkbox" name="cbvictimaconf" class="cbvictimaconf"  id="" value="1" onchange="victimas(this);_cambiarzonasgrafica();"><span class="label-text">En situación de desplazamiento</span>
                </label></a>
              </li>
                <li><a href="#"><label class="checkbox-formulario">
                  <input type="checkbox" name="cbvictimaconf" class="cbvictimaconf" id="" value="2"  onchange="victimas(this);_cambiarzonasgrafica();"><span class="label-text">Desvinculados de grupos armados</span>
                </label></a>
              </li>
                <li><a href="#"><label class="checkbox-formulario">
                  <input type="checkbox" name="cbvictimaconf" class="cbvictimaconf" id="" value="3"  onchange="victimas(this);_cambiarzonasgrafica();"><span class="label-text">Hijos de adultos desmovilizados</span>
                </label></a>
              </li>
                <li><a href="#"><label class="checkbox-formulario">
                  <input type="checkbox" name="cbvictimaconf" class="cbvictimaconf" id="" value="4"  onchange="victimas(this);_cambiarzonasgrafica();"><span class="label-text">Victimas de minas</span>
                </label></a>
              </li>
                <li><a href="#"><label class="checkbox-formulario">
                  <input type="checkbox" name="cbvictimaconf" class="cbvictimaconf" id="" value="5"  onchange="victimas(this);_cambiarzonasgrafica();"><span class="label-text">Responsabilidad penal</span>
                </label></a>
              </li>
                <li><a href="#"><label class="checkbox-formulario">
                  <input type="checkbox" name="cbvictimaconf" class="cbvictimaconf" id="" value="9"  onchange="victimas(this);_cambiarzonasgrafica();"><span class="label-text">No aplica</span>
                </label></a>
              </li>
              </ul>
              <script type="text/javascript">
                  $('.dropdown-menu input, .dropdown-menu label').click(function(e) {
                      e.stopPropagation();
                  });
              </script>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="col-md-8" style="padding-left:0px;padding-right:0px;">
      <div class="col-md-12">
        <div class="titleboxes">
          <h5 align="center">Matriculas totales por mes</h5>
        </div>
        <div class="box box-primary" style="border: 1px solid #3c8dbc !important; padding-left:4px;">
          <div class="box-header">
          </div>
          <div id="piechart_3d" style=" height: 400px;padding-right:0px;" ></div>
      </div>
      </div>
    </div>
  </div>
  </div>

  <script type="text/javascript">
      $('#infofiltro').toggle();
      window.setTimeout("mostrar()", 10000);

      function mostrar() {
        $("#infofiltro").slideToggle("slow", function() {
          $('#infofiltro').hide(5000);

        });
      }
      $(document).ready(function() {
        $("#clickme").click(function() {
          $("#infofiltro").toggle("slow", function() {

          });
        });
      });
  </script>
  <script type="text/javascript">
      $('.dynamic').change(function() {

        var select = $(this).attr("id");
        var value = $(this).val();
        var dependent = $(this).data('dependent');
        var _token = $('input[name="_token"]').val();
        var valor = "";
        if ($(this).val() != 0) {
          valor = "uno";
          $.ajax({
            url: "{{ route('ConsultaBasica.fetch') }}",
            method: "POST",
            data: {
              select: select,
              value: value,
              _token: _token,
              dependent: dependent,
              valor: valor
            },
            success: function(result) {
              $('#' + dependent).html(result);
              $('#nombre_establecimiento').html("<div class=''><a href='#'><label ><input type='radio' value='' ><span class='label-text'>TODOS</span></label></a></div>");
              $('#nombre_sede').html("<div class=''><a href='#'><label ><input type='radio' name='radio' value='' onclick='' ><span class='label-text'>TODOS</span></label></a></div>");
            }
          })
        } else {
          valor = "todos";
          $.ajax({
            url: "{{ route('ConsultaBasica.fetch') }}",
            method: "POST",
            data: {
              select: select,
              value: value,
              _token: _token,
              dependent: dependent,
              valor: valor
            },
            success: function(result) {
              $('#' + dependent).html(result);
                $('#nombre_establecimiento').html("<div class=''><a href='#'><label ><input type='radio' value='' ><span class='label-text'>TODOS</span></label></a></div>");
                $('#nombre_sede').html("<div class=''><a href='#'><label ><input type='radio' name='radio' value='' onclick='' ><span class='label-text'>TODOS</span></label></a></div>");
            }

          })
        }
      });

      $('#subregion').change(function() {
        $('#municipio').val('');
      });
  </script>
  <script type="text/javascript">
      $('.dynamicmuni').change(function() {
        /*var select = $(this).attr("id");*/
        var select = "codigo_dane_municipio";
        var value = $(this).val();
        var location = "matricula";
        var dependent = $(this).data('dependent');
        var _token = $('input[name="_token"]').val();
        var valor = "";
        if ($(this).val() != 0) {
          valor = "uno";
          $.ajax({
            url: "{{ route('ConsultaBasica.esta') }}",
            method: "POST",
            data: {
              select: select,
              value: value,
              _token: _token,
              dependent: dependent,
              valor: valor,
              location : location
            },
            success: function(result) {
              $('#' + dependent).html(result);
              $('#nombre_sede').html("<div class=''><a href='#'><label ><input type='radio' name='radio' value='' onclick='' ><span class='label-text'>TODOS</span></label></a></div>");
            }
          })
        } else {
          valor = "todos";
          $.ajax({
            url: "{{ route('ConsultaBasica.esta') }}",
            method: "POST",
            data: {
              select: select,
              value: value,
              _token: _token,
              dependent: dependent,
              valor: valor,
              location : location
            },
            success: function(result) {
              $('#' + dependent).html(result);
                $('#nombre_sede').html("<div class=''><a href='#'><label ><input type='radio' name='radio' value='' onclick='' ><span class='label-text'>TODOS</span></label></a></div>");
            }

          })
        }
      });

      $('#subregion').change(function() {
        $('#municipio').val('');
      });
  </script>
  <script type="text/javascript">
      function changesedes (element) {
        /*var select = $(this).attr("id");*/
        var select = "codigo_establecimiento";
        var value = element.value;
        var location = "matricula";
        var dependent = $(element).data('dependent');
        var _token = $('input[name="_token"]').val();
        var valor = "";
        if ($(element).val() != 0) {
          valor = "uno";
          $.ajax({
            url: "{{ route('ConsultaBasica.sedes') }}",
            method: "POST",
            data: {
              select: select,
              value: value,
              _token: _token,
              dependent: dependent,
              valor: valor,
              location : location
            },
            success: function(result) {
              $('#' + dependent).html(result);
            }
          })
        } else {
          valor = "todos";
          $.ajax({
            url: "{{ route('ConsultaBasica.sedes') }}",
            method: "POST",
            data: {
              select: select,
              value: value,
              _token: _token,
              dependent: dependent,
              valor: valor,
              location : location
            },
            success: function(result) {
              $('#' + dependent).html(result);
            }

          })
        }
      };
  </script>
  <script type="text/javascript">
      $('#cbtodossectores').click(function() {
        if ($(this).prop('checked')) {
          $('.cbsectores').prop('checked', true);
        } else {
          $('.cbsectores').prop('checked', false);
        }
      });
      $('#cbtodoscalendario').click(function() {
        if ($(this).prop('checked')) {
          $('.cbcalendarios').prop('checked', true);
        } else {
          $('.cbcalendarios').prop('checked', false);
        }
      });

      $('#cbtodoszr').click(function() {
        if ($(this).prop('checked')) {
          $('.cbzonasres').prop('checked', true);
        } else {
          $('.cbzonasres').prop('checked', false);
        }
      });
      $('#cbtodoscontra').click(function() {
        if ($(this).prop('checked')) {
          $('.cbmatcontrata').prop('checked', true);
        } else {
          $('.cbmatcontrata').prop('checked', false);
        }
      });
      $('#cbtodosgrados').click(function() {
        if ($(this).prop('checked')) {
          $('.cbgrados').prop('checked', true);
        } else {
          $('.cbgrados').prop('checked', false);
        }
      });

      let sect = null;
      for (let CheckBox of document.getElementsByClassName('cbsectores')) {
        CheckBox.onclick = function() {
          if (sect != null) {
            sect.checked = false;

            sect = CheckBox;
          }
          sect = CheckBox;
        }
      }
      let cal = null;
      for (let CheckBox of document.getElementsByClassName('cbcalendarios')) {
        CheckBox.onclick = function() {
          if (cal != null) {
            cal.checked = false;

            cal = CheckBox;
          }
          cal = CheckBox;
        }
      }
      let zonares = null;
      for (let CheckBox of document.getElementsByClassName('cbzonasres')) {
        CheckBox.onclick = function() {
          if (zonares != null) {
            zonares.checked = false;

            zonares = CheckBox;
          }
          zonares = CheckBox;
        }
      }
      let matcontra = null;
      for (let CheckBox of document.getElementsByClassName('cbmatcontrata')) {
        CheckBox.onclick = function() {
          if (matcontra != null) {
            matcontra.checked = false;

            matcontra = CheckBox;
          }
          matcontra = CheckBox;
        }
      }
      let victima = null;
      for (let CheckBox of document.getElementsByClassName('cbvictimaconf')) {
        CheckBox.onclick = function() {
          if (victima != null) {
            victima.checked = false;

            victima = CheckBox;
          }
          victima = CheckBox;
        }
      }
  </script>
  <script type="text/javascript">
      $(document).on('ready', function() {
        $("input:checkbox").on('click', function() {
          // in the handler, 'this' refers to the box clicked on
          var $box = $(this);
          if ($box.is(":checked")) {
            // the name of the box is retrieved using the .attr() method
            // as it is assumed and expected to be immutable
            var group = "input:checkbox[name='" + $box.attr("name") + "']";
            // the checked state of the group/box on the other hand will change
            // and the current value is retrieved using .prop() method
            $(group).prop("checked", false);
            $box.prop("checked", true);
          } else {
            $box.prop("checked", false);
          }
        });
      })
  </script>

</section>
@endsection
