@extends('layouts.app')

@section('htmlheader_title')
  Pruebas Saber
@endsection

@section('main-content')

  <section  id="contenido_principal">
    <!-- /.box-primary -->
    <div class="box box-primary">
      <div class="box-header">
        <h3 class="text-center"><b>Carga masiva resultados Pruebas Saber</b></h3>
        <br>
        <div>
          <p>Si presione el botón "Cargar archivos" podrá seleccionar el periodo para ingresar información de los resultados de las Pruebas Saber.</p>
          <!-- Enlace para abrir el modal de carga masiva del id #miModal cargado desde el layout app -->
          <button type="button" class="btn btn-primary" data-dismiss="modal" type="button" data-toggle="modal" data-target="#miModal">Cargar archivos</button>
          <!-- Fin enlace para abrir el modal de carga masiva del id #miModal cargado desde el layout app -->
        </div>

      </div><!-- /.box-header -->
    </div>
    <!-- /.box-primary -->

    <div id="formulario_archivos_pruebas">
      <form  id="f_subir_archivos_pruebas" name="f_subir_archivos_pruebas" method="post"  files="true" action="{{ url('subir_archivos_pruebas') }}" class="formarchivopruebas" enctype="multipart/form-data" >
        {{ csrf_field() }}

        <div id="anexos" class="box box-primary" >
          <h3 class="text-center">Archivos cargados</h3>
          <div class="box-body"  >
            <div class="table-responsive" >
              <table  class="table table-hover table-striped" cellspacing="0" width="100%">
                <thead>
                  <tr >
                    <th>Periodo</th>
                    <th data-type="date">Fecha de carga</th>
                    <th>Cantidad de registros</th>
                    <th>Acción</th>
                  </tr>
                </thead>
                <tbody>

                  @foreach($listados as $listado)
                    <tr role="row" class="odd">
                      <td>{{ $listado->periodo }}</td>
                      <td >{{ Carbon\Carbon::parse($listado->fecha)->format('d-m-Y h:i A') }}</td>
                      <td >{{ $listado->cantidad_pruebas }}</td>
                      <td >
                        <button type="button"  class="btn  btn-danger btn-xs" title="Borrar"  onclick="borrar_archivos_saber({{  $listado->periodo }});" ><i class="fa fa-trash-o"></i></button>
                      </td>
                    </tr>
                  @endforeach
                </tbody>
              </table>
              {{ $listados->links() }}
            </div>
          </div>
        </div>
      </section>
    @endsection
