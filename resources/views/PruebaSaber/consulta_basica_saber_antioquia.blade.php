@extends('layouts.app')

@section('htmlheader_title')
  Pruebas Saber Antioquia
@endsection


@section('main-content')

  <section  id="contenido_principal">
    <div class="box box-primary">
      <div class="box-header">
        <h3 class="text-center"><b>Consulta resultados Pruebas Saber 11° de Antioquia</b></h3>
        <br>
        <div>

          <iframe src="https://app.powerbi.com/view?r=eyJrIjoiZWU2NTBhNTAtMGI1OC00MzgxLTllMDgtYTQzMjUxMzFiZjFiIiwidCI6IjY0MmYxNTllLThmMTItNDMwOS1iODdjLWNiYzU0MzZlYzY5MSIsImMiOjR9" style="height:1200px; width:100%;" frameborder="0" allowFullScreen="true"></iframe>
        </div>
      </div><!-- /.box-header -->

      <div class="box-footer">

      </div>
    </section>
  @endsection
