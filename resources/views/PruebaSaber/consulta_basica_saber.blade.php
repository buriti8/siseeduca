@extends('layouts.app')

@section('htmlheader_title')
  Carga masiva de datos matrícula
@endsection


@section('main-content')

  <!-- Styles -->
  <style>
  #Matematicas {
    width: 48%;
    height: 500px;

  }

</style>

<style>
#Sociales {
  width: 48%;
  height: 500px;

}

</style>

<style>
#Naturales {
  width: 48%;
  height: 500px;
}

</style>

<style>
#Lectura {
  width: 48%;
  height: 500px;
}

</style>

<style>
#Materias {
  width: 48%;
  height: 600px;
}

</style>

<style>
#Ingles {
  width: 48%;
  height: 500px;
}

</style>

<!-- Resources -->
<script src="https://www.amcharts.com/lib/4/core.js"></script>
<script src="https://www.amcharts.com/lib/4/charts.js"></script>
<script src="https://www.amcharts.com/lib/4/themes/animated.js"></script>

<!-- Chart code -->
<!-- HTML -->


<section  id="contenido_principal">
  <div class="box box-primary">
    <div class="box-header">
      <h3 class="text-center"><b>Consulta resultados Pruebas Saber 11°</b></h3>
      <br>
      <div>

      </div>
    </div><!-- /.box-header -->

    <div class="box-body">

      <div class="panel panel-primary ">
        <div class="panel-heading" style="font-size: 16px;">Seleccione una sede educativa:</div>
        <div class="panel-body">

          @if(Auth::user()->isRole('administrador_sistema') || Auth::user()->isRole('administrador_pruebas_saber'))
            <div class="form-group col-xs-6"  >
              <div id='selEstablecimientoContenedor'>
                <label for="selEstablecimiento">Seleccionar una subregión</label>
                <select id="selSubregion" name="selSubregion" class="form-control  col-xs-6" onchange="filtroSubregionMunicipios()">
                  <option value="">Elige una</option>
                  <option value="1">BAJO CAUCA</option>
                  <option value="2">MAGDALENA MEDIO</option>
                  <option value="3">NORDESTE</option>
                  <option value="4">NORTE</option>
                  <option value="5">OCCIDENTE</option>
                  <option value="6">ORIENTE</option>
                  <option value="7">SUROESTE</option>
                  <option value="8">URABÁ</option>
                  <option value="9">VALLE DE ABURRÁ</option>
                </select>
              </div>
            </div>
            <div class="form-group col-xs-6"  >
              <div id='selEstablecimientoContenedor'>
                <label for="selMunicipio">Seleccionar municipio</label>
                <select  id="selMunicipio" name="selMunicipio" onchange="filtroMunicipioEstablecimiento()" class="form-control  col-xs-6">
                  <option selected="selected" value="">Elige uno</option>
                </select>
              </div>
            </div>
          @endif

          <div class="form-group col-xs-6"  >
            <div id='selEstablecimientoContenedor'>
              <label for="selEstablecimiento">Seleccionar un establecimiento educativo</label>
              <select  id="selEstablecimiento" name="selEstablecimiento" class="form-control  col-xs-6"  required>
                <option selected="selected" value="">Elige uno</option>
                @foreach ($temp_historico_establecimientos as $establecimiento)
                  <option value="{{$establecimiento->codigo_establecimiento}}">{{$establecimiento->nombre_establecimiento}}</option>
                @endforeach
              </select>
            </div>
          </div>
          <div class="form-group col-xs-6"  >
            <div id='selEstablecimientoContenedor'>
              <label for="selSede">Seleccionar una sede educativa</label>
              <select  id="selSede" name="selSede" class="form-control  col-xs-6" required>
                <option value="">Elige una</option>
              </select>
            </div>
          </div>
        </div>
      </div>

    </div>

    <div class="box-body">
      <h3 class="text-center">Puntajes promedio por área</h3>
      <div class="form-group col-xs-6" id="Materias"></div>
    </div>

    <div class="box-body" >
      <h3 class="text-center">Porcentaje de estudiantes por niveles de desempeño</h3>
      <div class="form-group col-xs-6" id="Matematicas"></div>
      <div class="form-group col-xs-6" id="Naturales"></div>
      <div class="form-group col-xs-6" id="Lectura"></div>
      <div class="form-group col-xs-6" id="Ingles"></div>
      <div class="form-group col-xs-6" id="Sociales"></div>
    </div>




    <div class="box-footer">

    </div>



  </section>
@endsection
