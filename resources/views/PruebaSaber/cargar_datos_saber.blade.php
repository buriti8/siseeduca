@extends('layouts.app')

@section('htmlheader_title')
Pruebas Saber
@endsection

@section('main-content')

<section  id="contenido_principal">
  <!-- /.box-primary -->
  <div class="box box-primary">
    <div class="box-header">
      <h3 class="text-center"><b>Carga masiva resultados Pruebas Saber</b></h3>
      <br>
      <div>
        <p>Aquí podrá ingresar información de los resultados de las Pruebas Saber. Por favor tenga en cuenta las siguientes recomendaciones:</p>
        <ol>
          <li><p>Descargue la plantilla para verificar las columnas que debe incluir en el anexo.</p></li>
          <li><p>El orden y el nombre de las columnas en el anexo debe permanecer igual.</p></li>
          <li><p>Recuerde que antes de cargar el archivo debe convertirlo a UTF-8.</p></li>
          <!-- /El botón llama el id modal-utf8 para cargar video conversión de archivos a utf8 desde app.blade -->
          <button class="btn btn-info fa fa-video-camera" style="font-size:15px;" data-target="#modal-utf8-DUE" data-toggle="modal" type="button"> Conversión archivos UTF-8</button>
          <!-- /Fin botón utf8 -->
          <li><p>Adjunte el archivo y presione el botón "Cargar archivos".</p></li>
        </ol>
      </div>
    </div><!-- /.box-header -->

    <!-- Modal que muestra el spinner de carga -->
    <div id="notificacion_resul_fci">
      <div class="modal" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title">Modal title</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <p>Modal body text goes here.</p>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-primary">Save changes</button>
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
          </div>
        </div>
      </div></div>
      <!-- /. Fin Modal que muestra el spinner de carga -->

      <!-- /.box-primary -->

      <div id="formulario_archivos_pruebas">
        <form  id="f_subir_archivos_pruebas" name="f_subir_archivos_pruebas" method="post"  files="true" action="{{ url('subir_archivos_pruebas') }}" class="formarchivopruebas" enctype="multipart/form-data" >
          {{ csrf_field() }}

          <div class="box-body ">
            <div class="panel panel-primary ">
              <div class="panel-heading">Resultados pruebas Saber</div>
              <div class="panel-body">
                <div class="form-group col-xs-6"  >
                  <label>Agregar archivo <span class="label label-primary">Resultados Pruebas Saber</span></label>
                  <input name="archivo_pruebas" id="archivo_pruebas" type="file" files="true"  enctype=”multipart/form-data”  class="archivo form-control" /><br /><br />
                </div>
              </div>
            </div>

          </form>
          <button type="submit" id="btnsubmit"  class="btn btn-primary fa fa-cloud-upload" style="font-size:15px;"> Cargar archivos</button>
          <a onclick="return confirm('Recuerde que cuando vaya a cargar el archivo debe convertirlo a UTF-8.')" href="{{ url('/descargar_plantilla_saber') }}" target="_blank" id="descargar_plantilla" name="descargar_plantilla"  class="btn btn-info fa fa-cloud-download" style="font-size:15px;"> Descargar plantilla</a>
        </div>
      </div>
    </div>

    <div id="anexos" class="box box-primary" >
      <h3 class="text-center">Archivos cargados</h3>
      <div class="box-body"  >
        <div class="table-responsive" >
          <table  class="table table-hover table-striped" cellspacing="0" width="100%">
            <thead>
              <tr >
                <th>Periodo</th>
                <th data-type="date">Fecha de carga</th>
                <th>Cantidad de registros</th>
                <th>Acción</th>
              </tr>
            </thead>
            <tbody>

              @foreach($listados as $listado)
              <tr role="row" class="odd">
                <td>{{ $listado->periodo }}</td>
                <td >{{ Carbon\Carbon::parse($listado->fecha)->format('d-m-Y h:i A') }}</td>
                <td >{{ $listado->cantidad_pruebas }}</td>
                <td >
                  <button type="button"  class="btn  btn-danger btn-xs" title="Borrar"  onclick="borrar_archivos_saber({{  $listado->periodo }});" ><i class="fa fa-trash-o"></i></button>
                </td>
              </tr>
              @endforeach
            </tbody>
          </table>
          {{ $listados->links() }}
        </div>
      </div>
    </div>
</section>
@endsection
